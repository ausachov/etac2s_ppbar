from ROOT import *

cutTrigger = "(Jpsi_L0HadronDecision_TOS || Jpsi_L0Global_TIS)"
cutTrack   = "ProtonP_TRACK_CHI2NDOF<3 && ProtonM_TRACK_CHI2NDOF<3 && PiLamP_TRACK_CHI2NDOF<3 && PiLamM_TRACK_CHI2NDOF<3 && PiFreeP_TRACK_CHI2NDOF<3 && PiFreeM_TRACK_CHI2NDOF<3"
cutVtx = "Lambda_ENDVERTEX_CHI2<9 && antiLambda_ENDVERTEX_CHI2<9 && Xip_ENDVERTEX_CHI2<9 && Xim_ENDVERTEX_CHI2<9 && Jpsi_ENDVERTEX_CHI2<9"
cutPID = "ProtonP_ProbNNp>0.2 && ProtonM_ProbNNp>0.2"
cutIP = "ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16 && PiLamP_IPCHI2_OWNPV>16 && PiLamM_IPCHI2_OWNPV>16 && PiFreeP_IPCHI2_OWNPV>16 && PiFreeM_IPCHI2_OWNPV>16" 
cutP = "ProtonP_P>10000 && ProtonM_P>10000"

cutPhiVeto = ""

cutDef_v0 = cutTrigger+" && "+cutTrack+" && "+cutVtx+" && "+cutPID+" && "+cutIP+" && "+cutP

ch = TChain("XiXi/DecayTree")
ch.Add("/eos/user/a/ausachov/Data_XiXi/750/Merged.root")
ch.Add("/eos/user/a/ausachov/Data_XiXi/752/Merged.root")
ch.Add("/eos/user/a/ausachov/Data_XiXi/802/Merged.root")
ch.Add("/eos/user/a/ausachov/Data_XiXi/829/Merged.root")

f = TFile("XiXi_RunII_cutDef_v0.root","recreate")
tree = ch.CopyTree(cutDef_v0)

f.Write()
f.Close()

