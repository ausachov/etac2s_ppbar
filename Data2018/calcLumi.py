merged_filepath_prefix = '/eos/lhcb/wg/BandQ/PRODPOL/etac2s_2018/ppbar'
input_tree_nameLumi = 'GetIntegratedLuminosity/LumiTuple'

import os, sys, re
from ROOT import TTree, TFile
from array import array


def caclLumi():

  tree = TTree()
  intLumi = 0.

  luminosity = array('d',[0])
  for ii in range(300):
    fileName = merged_filepath_prefix+"/1002/"+str(ii)+"/Tuple.root"
    if os.path.isfile(fileName):
      file = TFile(fileName,"read")
      tree = file.Get("GetIntegratedLuminosity/LumiTuple")
      tree.SetBranchAddress("IntegratedLuminosity",luminosity)
      for iii in range(tree.GetEntries()):
        tree.GetEntry(iii)
        intLumi += luminosity[0]

    fileName = merged_filepath_prefix+"/1003/"+str(ii)+"/Tuple.root"
    if os.path.isfile(fileName):
      file = TFile(fileName,"read")
      tree = file.Get("GetIntegratedLuminosity/LumiTuple")

      tree.SetBranchAddress("IntegratedLuminosity",luminosity)
      for iii in range(tree.GetEntries()):
        tree.GetEntry(iii)
        intLumi += luminosity[0]

  print("IntegratedLuminosity is ",intLumi)


if __name__ == '__main__':
  caclLumi()
