def makeSingleTuple(year,polarity,mass):

    yearShort = str(int(year)-2000)

    myJobName = 'Jpsi2PPbar_'+mass+'_Turbo05_Mag'+polarity+'_'+year


    davinciVer = 'v44r10p1'
    davinciDir = '/afs/cern.ch/user/v/vazhovko/cmtuser/DaVinciDev_'+davinciVer
    import os
    if os.path.exists(davinciDir):
        myApplication = GaudiExec()
        myApplication.directory = davinciDir
    else:
        myApplication = prepareGaudiExec('DaVinci',davinciVer, myPath='/afs/cern.ch/user/v/vazhovko/cmtuser/')

    #myApplication = GaudiExec()
    #myApplication.directory = "$HOME/cmtuser/DaVinciDev_v44r3"

    myApplication.options = ['DaVinci_Jpsi2pp_'+mass+'.py']

    data  = BKQuery('/LHCb/Collision'+yearShort+'/Beam6500GeV-VeloClosed-Mag'+polarity+'/Real Data/Turbo05/94000000/LEPTONS.MDST', dqflag=['OK','UNCHECKED']).getDataset()

    validData = LHCbDataset(files=['LFN:'+lfn for lfn, rep in data.getReplicas().iteritems() if len(rep)])

    mySplitter = SplitByFiles( filesPerJob = 20, maxFiles = -1, ignoremissing = True, bulksubmit = False )

    myBackend = Dirac()
    j = Job (
             name         = myJobName,
             application  = myApplication,
             splitter     = mySplitter,
             outputfiles  = [ DiracFile('Tuple.root'),
                              DiracFile('DVHistos.root')],
             backend      = myBackend,
             inputdata    = validData,
             do_auto_resubmit = True,
             parallel_submit = True
             )
    queues.add(j.submit)


years      = ['2018']
polarities = ['Down','Up']
masses     = ['High','Low']

for mass in masses:
    for year in years:
        for polarity in polarities:
            print('PPbar for ', year, polarity, mass, ' submitting below! \n')
            makeSingleTuple(year,polarity,mass)
