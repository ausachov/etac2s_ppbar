def fillTuple( tuple, myTriggerList ):
#def fillTuple( tuple, myBranches, myTriggerList ):

    #tuple.Branches = myBranches

    tuple.ToolList = [
                      "TupleToolAngles",
                      "TupleToolEventInfo",
                      "TupleToolGeometry",
                      "TupleToolKinematic",
                      #"TupleToolANNPID",
                      "TupleToolPid",
                      "TupleToolPrimaries",
                      #"TupleToolRecoStats",
                      "TupleToolTrackInfo"
                      ]


    # RecoStats for filling SpdMult, etc
    from Configurables import TupleToolRecoStats
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True


    from Configurables import TupleToolANNPID
    tuple.addTool(TupleToolANNPID, name = 'TupleToolANNPID')
    tuple.TupleToolANNPID.Verbose = True


    from Configurables import TupleToolTISTOS, TupleToolDecay
    tuple.addTool(TupleToolDecay, name = 'Jpsi')

    # TISTOS for Jpsi
    tuple.Jpsi.ToolList+=[ "TupleToolTISTOS" ]
    tuple.Jpsi.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.Jpsi.TupleToolTISTOS.Verbose=True
    tuple.Jpsi.TupleToolTISTOS.TriggerList = myTriggerList


    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled" : "DTF_FUN ( M , False )",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
     }

    #tuple.addTool(TupleToolDecay, name="Jpsi")
    #tuple.Jpsi.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Jpsi"]
    tuple.Jpsi.addTool(LoKi_Jpsi)




    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
    }
    #tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
    tuple.addTool(LoKi_All)


# DecayTreeTuple
from Configurables import DecayTreeTuple

myTriggerList = [
                 # L0
                 "L0ElectronDecision",
                 "L0PhotonDecision",
                 "L0HadronDecision",
                 # L0 Muon
                 "L0MuonDecision",
                 "L0MuonHighDecision",
                 "L0DiMuonDecision",



                 # Hlt1 track
                 "Hlt1B2PhiPhi_LTUNBDecision",
                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackPhotonDecision",

                 "Hlt1TrackMVADecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1B2HH_LTUNB_KPiDecision",
                 "Hlt1B2HH_LTUNB_KKDecision",
                 "Hlt1B2HH_LTUNB_PiPiDecision",
                 "Hlt1IncPhiDecision",
                 "Hlt1DiProtonDecision",
                 "Hlt1DiProtonLowMultDecision",
                 "Hlt1LowMultVeloCut_HadronsDecision",
                 "Hlt1LowMultPassThroughDecision",




                 # Hlt2 Topo
                 "Hlt2PhiIncPhiDecision",
                 "Hlt2PhiPromptPhi2EETurboDecision",
                 "Hlt2PhiBs2PhiPhiDecision",
                 "Hlt2B2HH_B2HHDecision",
                 "Hlt2CcDiHadronDiProtonDecision",
                 "Hlt2CcDiHadronDiProtonLowMultDecision",
                 "Hlt2CcDiHadronDiPhiDecision",
                 "Hlt2PhiIncPhiDecision",
                 "Hlt2PhiBs2PhiPhiDecision",

                 "Hlt2Topo2BodyDecision",
                 "Hlt2Topo3BodyDecision",
                 "Hlt2Topo4BodyDecision"
                 ]


year = "2018"
stream = "CharmCompleteEvent"

Jpsi2ppLocation = "Phys/Ccbar2PpbarDetachedLine/Particles"
Jpsi2ppDecay = "J/psi(1S) -> ^p+ ^p~-"
Jpsi2ppBranches = {
    "ProtonP"  :  "J/psi(1S) ->^p+ p~-"
    ,"ProtonM"  :  "J/psi(1S) -> p+^p~-"
    ,"Jpsi"     : "(J/psi(1S) -> p+ p~-)"
}



Jpsi2ppTuple          = DecayTreeTuple("Jpsi2ppTuple")
Jpsi2ppTuple.Decay    = Jpsi2ppDecay
Jpsi2ppTuple.Branches = Jpsi2ppBranches
Jpsi2ppTuple.Inputs   = ['/Event/{0}/{1}'.format(stream, Jpsi2ppLocation)]
#Jpsi2ppTuple.Inputs = [ Jpsi2ppLocation ]
fillTuple( Jpsi2ppTuple, myTriggerList )
#fillTuple( Jpsi2ppTuple, Jpsi2ppBranches, myTriggerList )


#from PhysConf.Selections import AutomaticData, MomentumScaling, TupleSelection
#inputData = AutomaticData(Jpsi2ppLocation)
#inputData = MomentumScaling(inputData)
#inputData = MomentumScaling(inputData, Turbo  = False, Year = year)

#Jpsi2ppTuple = TupleSelection("Jpsi2ppTuple", inputData, Decay = Jpsi2ppDecay, Branches = Jpsi2ppBranches)
#fillTuple( Jpsi2ppTuple, myTriggerList )


from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('DAQkiller')
eventNodeKiller.Nodes = ['/Event/DAQ',
                         '/Event/pRec']



from Configurables import TrackScaleState
StateScale = TrackScaleState("StateScale", RootInTES = '/Event/{0}'.format(stream))

#from Configurables import StoreExplorerAlg




"""
    Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Jpsi2ppFilters = LoKi_Filters (
                               STRIP_Code = "HLT_PASS('StrippingCcbar2PpbarDetachedLineDecision')"
                               )

from Configurables import DaVinci, CondDB
CondDB( LatestGlobalTagByDataType = year )
DaVinci().EventPreFilters = Jpsi2ppFilters.filters ('Jpsi2ppFilters')
DaVinci().EvtMax = -1                          # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000
DaVinci().DataType = year
DaVinci().Simulation    = False
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().TupleFile = "Tuple.root"             # Ntuple
#DaVinci().UserAlgorithms = [ eventNodeKiller,StateScale,Jpsi2ppTuple ]        # The algorithms
DaVinci().UserAlgorithms = [ Jpsi2ppTuple ]        # The algorithms

DaVinci().InputType = "DST"
DaVinci().RootInTES = '/Event/{0}'.format(stream)
# MDST
#DaVinci().InputType = "MDST"
#DaVinci().RootInTES = "/Event/CharmCompleteEvent"

# Get Luminosity
DaVinci().Lumi = True

#DaVinci().DDDBtag   = "dddb-20190206-3"
#DaVinci().CondDBtag = "cond-20180202"



from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]

#from GaudiConf import IOHelper
#IOHelper().inputFiles([
                     #'/afs/cern.ch/work/v/vazhovko/private/etac2s_ppbar/00077434_00051244_1.charmcompleteevent.dst'
                     #], clear=True)
                     #'/afs/cern.ch/work/v/vazhovko/private/etac2s_ppbar/00079436_00000334_1.charm.mdst',
                     #'/afs/cern.ch/work/v/vazhovko/private/etac2s_ppbar/00079436_00004236_1.charm.mdst'
                     #'/afs/cern.ch/work/v/vazhovko/private/etac2s_ppbar/00092361_00007935_1.charm.mdst'
                     #'/afs/cern.ch/work/v/vazhovko/private/etac2s_ppbar/00077432_00025279_1.leptons.mdst'
