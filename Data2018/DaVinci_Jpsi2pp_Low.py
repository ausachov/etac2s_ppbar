from DaVinci.Configuration import *
from DecayTreeTuple.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, DecayTreeTuple, HltSelReportsDecoder,HltVertexReportsDecoder,HltDecReportsDecoder,LoKi__Hybrid__TupleTool, FilterDesktop

from Configurables import CombineParticles
from PhysSelPython.Wrappers import (
    RebuildSelection,
    AutomaticData,
    Selection,
    SelectionSequence,
    TupleSelection
)


def fillTuple( tuple, myTriggerList ):

    tuple.ToolList = ["TupleToolAngles",
                      "TupleToolEventInfo",
                      "TupleToolGeometry",
                      "TupleToolKinematic",
                      "TupleToolPid",
                      "TupleToolPrimaries",
                      "TupleToolTrackInfo",
                      "TupleToolRecoStats",
                      "TupleToolANNPID"
                     ]
    # tuple.TupleToolRecoStats.Verbose=True

    from Configurables import TupleToolTISTOS, TupleToolDecay
    tuple.addTupleTool(TupleToolDecay, name = 'Jpsi')

    tuple.Jpsi.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForJpsi" ]
    tuple.Jpsi.addTool(TupleToolTISTOS, name="TupleToolTISTOSForJpsi" )
    tuple.Jpsi.TupleToolTISTOSForJpsi.Verbose=True
    tuple.Jpsi.TupleToolTISTOSForJpsi.TriggerList = myTriggerList


    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.Jpsi.addTupleTool(LoKi_Jpsi)


    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
        }
    tuple.addTool(LoKi_All)

myTriggerList = [
                 # L0
                 "L0ElectronDecision",
                 "L0PhotonDecision",
                 "L0HadronDecision",
                 # L0 Muon
                 "L0MuonDecision",
                 "L0MuonHighDecision",
                 "L0DiMuonDecision",

                 # Hlt1 track
                 "Hlt1B2PhiPhi_LTUNBDecision",
                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackPhotonDecision",

                 "Hlt1TrackAllL0Decision",
                 "Hlt1TrackMVADecision",
                 "Hlt1TrackMVALooseDecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1TwoTrackMVALooseDecision"
                 "Hlt1B2HH_LTUNB_KPiDecision",
                 "Hlt1B2HH_LTUNB_KKDecision",
                 "Hlt1B2HH_LTUNB_PiPiDecision",
                 "Hlt1IncPhiDecision",
                 "Hlt1DiProtonDecision",
                 "Hlt1DiProtonLowMultDecision",
                 "Hlt1LowMultVeloCut_HadronsDecision",
                 "Hlt1LowMultPassThroughDecision",

                 "Hlt1DiProtonDecision",
                 "Hlt1DiProtonHighDecision"

                 ]







year = "2018"
Jpsi2ppDecay = "J/psi(1S) -> ^p+ ^p~-"
Jpsi2ppBranches = {
     "ProtonP"  :  "J/psi(1S) ->^p+ p~-"
    ,"ProtonM"  :  "J/psi(1S) -> p+^p~-"
    ,"Jpsi"     : "(J/psi(1S) -> p+ p~-)"
}

Jpsi2ppLocation = "Hlt2CcDiHadronDiProtonTurbo/Particles"
from PhysConf.Selections import AutomaticData, MomentumScaling, TupleSelection
inputData = AutomaticData(Jpsi2ppLocation)
inputData = MomentumScaling(inputData, Turbo  = True, Year = year)

Jpsi2ppTuple = TupleSelection("Jpsi2ppTuple", inputData, Decay = Jpsi2ppDecay, Branches = Jpsi2ppBranches)
fillTuple( Jpsi2ppTuple, myTriggerList )

#Jpsi2ppTuple = DecayTreeTuple('Jpsi2ppTuple')
#Jpsi2ppTuple.Inputs   = [Jpsi2ppLocation]
#Jpsi2ppTuple.Decay    = Jpsi2ppDecay
#Jpsi2ppTuple.Branches = Jpsi2ppBranches
#fillTuple( Jpsi2ppTuple, myTriggerList )


from Configurables import DataOnDemandSvc
dod = DataOnDemandSvc()
from Configurables import Gaudi__DataLink as Link
rawEvt1 = Link ( 'LinkRawEvent1', What   =  '/Event/DAQ/RawEvent' , Target = '/Event/Trigger/RawEvent' )
dod.AlgMap [ rawEvt1  . Target ] = rawEvt1


Jpsi2ppTuple.Jpsi.addTupleTool( 'TupleToolSubMass' )
Jpsi2ppTuple.Jpsi.TupleToolSubMass.Substitution += ["p+ => pi+"]
Jpsi2ppTuple.Jpsi.TupleToolSubMass.Substitution += ["p+ => K+"]

Jpsi2ppTuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => pi+/pi-"]
Jpsi2ppTuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => K+/pi-"]
Jpsi2ppTuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => pi+/K-"]
Jpsi2ppTuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => K+/K-"]


from Configurables import DstConf, TurboConf
TurboConf().PersistReco=False
DstConf().Turbo=True




"""
Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Jpsi2ppFilters = LoKi_Filters (
     HLT2_Code = """
                  HLT_PASS('Hlt2CcDiHadronDiProtonTurboDecision')
                 """
    )



from Configurables import CondDB
CondDB( LatestGlobalTagByDataType = year )
DaVinci().EventPreFilters = Jpsi2ppFilters.filters('Jpsi2ppFilters')
DaVinci().EvtMax         = -1                        # Number of events
DaVinci().SkipEvents     = 0                       # Events to skip
DaVinci().PrintFreq      = 1000
DaVinci().DataType       = year
DaVinci().Simulation     = False
DaVinci().HistogramFile  = "DVHistos.root"      # Histogram file
DaVinci().TupleFile      = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms = [Jpsi2ppTuple]       # The algorithms
# MDST
DaVinci().InputType = "MDST"
DaVinci().RootInTES = '/Event/Leptons/Turbo'
DaVinci().Turbo     = True

DaVinci().Lumi = True

from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]

#from GaudiConf import IOHelper
#IOHelper().inputFiles([
                      #'00075226_00000078_1.leptons.mdst'
                      #], clear=True)




