#merged_filepath_prefix = '/eos/lhcb/wg/BandQ/PRODPOL/etac2s_2018/ppbar'
merged_filepath_prefix = '/eos/user/v/vazhovko/etac2s_ppbar'
# merged_filepath_prefix = '/sps/lhcb/zhovkovska/etac2s_ppbar/Data_2017/'

input_tree_name     = 'Jpsi2ppTuple/DecayTree'
input_tree_nameLumi = 'GetIntegratedLuminosity/LumiTuple'

import os, sys, re
import ROOT


def merge_root_output(id, merged_filepath_prefix):

    access_urls = []
    thefile = open('Job{}_URLs.txt'.format(id), 'r')
   #  thefile = open('Jobs2017_URLs.txt', 'r')

    for line in thefile:
       # line = line.translate(None, '\'[]')
       line = line.translate({ord(i): None for i in "\'[]"})
       line = line.replace('?svcClass=lhcbUser','')
       line = line.replace('\n','')
       if "Tuple.root" in line:
          access_urls.append(line)
          print(line)
    thefile.close()

    import ROOT
    tchain = ROOT.TChain(input_tree_name)
    tchainLumi = ROOT.TChain(input_tree_nameLumi)
    for url in access_urls:
       tchain.Add(url)
       tchainLumi.Add(url)
       print(tchain.GetEntries())

    sjDir = merged_filepath_prefix+"/"+str(id)
    try:
       os.system('mkdir '+sjDir)
    except:
       pass

   #  newfile = ROOT.TFile(sjDir+"/Etac2sDiProton_High_2018.root","recreate")
    newfile = ROOT.TFile(sjDir+"/Etac2sDiProton_Low_2018.root","recreate")
   #  newfile = ROOT.TFile(sjDir+"/EtacDiProton_2017.root","recreate")
    newtree = ROOT.TTree()
    newtree.SetMaxTreeSize(500000000)

    newtree = tchain.CopyTree("")
    newtree.Print()
    newtree.GetCurrentFile().Write()
    newtree.GetCurrentFile().Close()

    del tchain, newtree, newfile





    sjDirLumi = merged_filepath_prefix+"/Lumi"
    try:
       os.system('mkdir '+sjDirLumi)
    except:
       pass
    sjDirLumi = sjDirLumi+"/"+str(id)
    try:
       os.system('mkdir '+sjDirLumi)
    except:
       pass

    newfileLumi = ROOT.TFile(sjDir+"/LumiTuple_pp2018.root","recreate")
   #  newfileLumi = ROOT.TFile(sjDir+"/LumiTuple_pp2017.root","recreate")
    newtreeLumi = ROOT.TTree()
    newtreeLumi.SetMaxTreeSize(500000000)

    newtreeLumi = tchainLumi.CopyTree("")
    newtreeLumi.GetCurrentFile().Write()
    newtreeLumi.GetCurrentFile().Close()

    del tchainLumi, newtreeLumi, newfileLumi



merge_root_output(152, merged_filepath_prefix)
merge_root_output(153, merged_filepath_prefix)
# merge_root_output(0, merged_filepath_prefix)


# jid=1003
# for sj in jobs(jid).subjobs:
#   sjDir = "/eos/lhcb/wg/BandQ/PRODPOL/etac2s_2018/ppbar/"+str(jid)+"/"+str(sj.id)
#   try:
#     os.system('mkdir '+sjDir)
#   except:
#     pass
#   if sj.status=='completed':
#     sj.outputfiles[0].localDir = sjDir
#     sj.outputfiles[0].get()
