import os, sys
def getURLs(job):
    # Treat a job with subjobs the same as a job with no subjobs
    JOBS = job.subjobs
    print "treating job ", job.id
    if len(JOBS) == 0:
        JOBS = [job]

    access_urls = []
    for j in JOBS:
        if j.status != 'completed':
            print 'Skipping #{0}'.format(j.id)
            continue
        # THIS WAS FOR NORMAL JOBS ACCORDING SECOND ANALYSIS STEPS?????

        try:
           df = j.outputfiles[0]
           access_urls.append(df.accessURL())
        except:
           pass
        print j.id,"  added"

    thefile = open('Job'+str(job.id)+'_URLs.txt', 'w')
    for item in access_urls:
       thefile.write("%s\n" % item)
    thefile.close()

getURLs(job)

#if __name__ == "__main__":
    #job = sys.argv[1]
    #getURLs(job)

