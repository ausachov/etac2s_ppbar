
input_tree_name = 'GetIntegratedLuminosity/LumiTuple'
merged_filepath_prefix = '/eos/user/a/ausachov/Data_etac2s_ppbar_2018/Lumi'

import os, sys, re
import ROOT 


def merge_root_output(id, input_tree_name, merged_filepath_prefix):

    access_urls = []
    thefile = open('Job{}_URLs.txt'.format(id), 'r')

    for line in thefile:        
       line = line.translate(None, '\'[]')
       line = line.replace('?svcClass=lhcbUser','')
       line = line.replace('\n','')
       if "Tuple.root" in line:
          access_urls.append(line)
          print(line)
    thefile.close() 

    import ROOT
    tchain = ROOT.TChain(input_tree_name)
    for url in access_urls:
       tchain.Add(url)
       print(tchain.GetEntries())

    sjDir = merged_filepath_prefix+"/"+str(id) 
    try:
       os.system('mkdir '+sjDir)
    except:
       pass

    newfile = ROOT.TFile(sjDir+"/mergedLumi_pp2018.root","recreate")
    newtree = ROOT.TTree()
    newtree.SetMaxTreeSize(500000000)

    newtree = tchain.CopyTree("")
    newtree.Print()
    newtree.GetCurrentFile().Write() 
    newtree.GetCurrentFile().Close()

    del tchain, newtree


merge_root_output(1002, input_tree_name, merged_filepath_prefix)
merge_root_output(1003, input_tree_name, merged_filepath_prefix)


