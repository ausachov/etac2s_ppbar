from ROOT import *
from ROOT.TObject import kOverwrite

def merge_files(ccbar,source):

    # f = TFile("/sps/lhcb/zhovkovska/etac2s_ppbar/MC/NoTurbo/{}DiProton_{}_2018_AddBr_oldDec.root".format(ccbar,source),"RECREATE")
    # nt = TChain("MCDecayTree")
    # nt.Add("/sps/lhcb/zhovkovska/etac2s_ppbar/MC/NoTurbo/{}DiProton_{}_Mag*_2018_AddBr_oldDec.root".format(ccbar,source))
    # f = TFile("/sps/lhcb/zhovkovska/etac2s_ppbar/MC/NoTurbo/{}DiProton_{}_MagDown_2018.root".format(ccbar,source),"RECREATE")
    f = TFile("/sps/lhcb/zhovkovska/etac2s_ppbar/MC/Trigger/{}DiProton_{}_MagUp_2018_sel.root".format(ccbar,source),"RECREATE")
    nt = TChain("DecayTree")
    # nt.Add("/sps/lhcb/zhovkovska/etac2s_ppbar/MC/NoTurbo/{}DiProton_{}_MagDown_2018_*.root".format(ccbar,source))
    # nt.Add("/sps/lhcb/zhovkovska/etac2s_ppbar/MC/Trigger/{}DiProton_{}_MagUp_2018_Sim*_AddBr.root".format(ccbar,source))
    nt.Add("/sps/lhcb/zhovkovska/etac2s_ppbar/MC/Trigger/{}DiProton_{}_MagUp_2018_Sim09*_sel.root".format(ccbar,source))
    t = nt.CopyTree("")
    t.Write()
    f.Close()

    print(ccbar)

if __name__ == "__main__":

    kSources = ["fromB", "prompt"]
    kSources = ["prompt"]
    kRanges = ["jpsi"]
    #kRanges = ["etac","etac2S","chic0","chic1","chic2","etac","jpsi","etac2S","psi2S",]

    for kRange in kRanges:
        for kSource in kSources:
            merge_files(kRange, kSource)

