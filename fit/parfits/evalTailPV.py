from ROOT import gStyle, gROOT, TCanvas, TFile, TChain, TH1D, TF1, TKDE, TLegend, TLatex
from array import array
import logging
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

# homeDir = "/users/LHCb/zhovkovska/scripts/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/MC/Trigger/"
expDir = "/exp/LHCb/zhovkovska/"
#def EvalTail()
#{

#    RooRealVar Jpsi_Tz("Jpsi_Tz","Jpsi_Tz",-10.,10.) 
#    Jpsi_Tz.setBins(100)


#    nt=TChain("DecayTree")
#    nt.Add("Reduced_Snd_2012.root")



#    RooDataSet ds_tail("ds_tail","ds_tail",RooArgSet(Jpsi_Tz),StoreError(RooArgSet(Jpsi_Tz)))
#    const Int_t nEntries = nt.GetEntries()
#    Jpsi_ENDVERTEX_Z
#    Jpsi_OWNPV_Z
#    Jpsi_MM
#    Jpsi_PZ
#    nt.SetBranchAddress("Jpsi_ENDVERTEX_Z",&Jpsi_ENDVERTEX_Z)
#    nt.SetBranchAddress("Jpsi_OWNPV_Z",&Jpsi_OWNPV_Z)
#    nt.SetBranchAddress("Jpsi_MM",&Jpsi_MM)
#    nt.SetBranchAddress("Jpsi_PZ",&Jpsi_PZ)

#    for(Int_t iEn=1 iEn<=nEntries iEn++)
#    {

#        nt.GetEntry((iEn+1)%nEntries)
#        Jpsi_OWNPV_Z_next = Jpsi_OWNPV_Z
#        nt.GetEntry(iEn)
#        if(TMath::Abs(3.3* (Jpsi_ENDVERTEX_Z - Jpsi_OWNPV_Z_next)*Jpsi_MM/Jpsi_PZ) <= 10.)
#        {    Jpsi_Tz = (3.3* (Jpsi_ENDVERTEX_Z - Jpsi_OWNPV_Z_next)*Jpsi_MM/Jpsi_PZ)
#            Jpsi_Tz.setRange(-10.,10.)
#            ds_tail.add(RooArgSet(Jpsi_Tz)) 
#        }
#    }



##    TFile f("test.root","RECREATE")
##    tree.Write()
##    f.Close()
#    ds_tail.Print()


#    RooKeysPdf kest("kest","kestPdf",Jpsi_Tz,ds_tail,RooKeysPdf::MirrorBoth)

#    RooPlot *frame = Jpsi_Tz.frame(Title("KeyPdf")) 
#    ds_tail.plotOn(frame)
#    kest.plotOn(frame,LineColor(2))

#     c = TCanvas("Signal_Fit","Signal Fit",800,500)
#     gPad.SetLeftMargin(0.15)  frame.GetXaxis().SetTitle("t_{z} [ps]")  frame.Draw()
##     c.SetLogy()
##     frame.SetMinimum(1.)

#     Jpsi_Tz = 5.
#     print kest.getValV(/*RooArgSet(Jpsi_Tz)*/)<<std::endl


#}



def EvalTail():

    gStyle.SetOptStat(0)

    nt = TChain("DecayTree")
#    nt.Add("CharmSelect_BMes.root")
    nt.Add(homeDir+"Reduced_Snd_2012.root")


    nEntries = nt.GetEntries()
    Jpsi_ENDVERTEX_Z = array("d",[0])
    Jpsi_OWNPV_Z = array("d",[0])
    Jpsi_MM = array("d",[0])
    Jpsi_PZ = array("d",[0])
#    Bool_t prompt
#    Bool_t sec
    nt.SetBranchAddress("Jpsi_ENDVERTEX_Z",Jpsi_ENDVERTEX_Z)
    nt.SetBranchAddress("Jpsi_OWNPV_Z",Jpsi_OWNPV_Z)
    nt.SetBranchAddress("Jpsi_MM",Jpsi_MM)
    nt.SetBranchAddress("Jpsi_PZ",Jpsi_PZ)
#    nt.SetBranchAddress("Jpsi_prompt",prompt)
#    nt.SetBranchAddress("Jpsi_sec",sec)

    tz = array("d",nEntries*[0])
    Jpsi_OWNPV_Z_next = array("d",[0])
    hTail = TH1D("hTail","hTail",100,-10.,10.)
    hTailPr = TH1D("hTailPr","hTailPr",100,-10.,10.)
    for iEn in range(nEntries):

        nt.GetEntry((iEn+1)/nEntries)
        Jpsi_OWNPV_Z_next = Jpsi_OWNPV_Z
        nt.GetEntry(iEn)
        tz[iEn] = (3.3* (Jpsi_ENDVERTEX_Z - Jpsi_OWNPV_Z_next)*Jpsi_MM/Jpsi_PZ)
        hTail.Fill(tz[iEn])


#    f  = TFile("evalTail.root","RECREATE")

    kdeTail = TKDE(nEntries, tz[0], -10., 10., "kerneltype:gaussian", 3.0)
    f = kdeTail.GetFunction(1000)

#    hTail.Scale(1/hTail.Integral(1,100)/hTail.GetBinWidth(1))
#    hTail.Scale(1/hTail.GetEntries()/hTail.GetBinWidth(1))
    hTail.Scale(1/hTail.Integral(hTail.FindBin(-10.0),hTail.FindBin(10.0))/hTail.GetBinWidth(1))
#    hTailPr.Scale(1/hTailPr.Integral(hTailPr.FindBin(-10.),hTailPr.FindBin(10.))/hTailPr.GetBinWidth(1))
#    hTailPr.Fit(f)

    c = TCanvas("TailEval","Tail Evaluation",800,500)
#    c.cd()

    hTail.Draw()
    hTail.GetXaxis().SetTitle("t_{z} [ps]")
    f.Draw("same")
#    hTailPr.SetLineColor(kRed)
#    hTailPr.DrawClone("SAME")

#    print "nSec"<<hTailPr.Integral(hTailPr.FindBin(-10.),hTailPr.FindBin(10.))<<std::endl
#    kdeTail.Draw("SAME")
#    kdeTail.SaveAs("evalTail.root")
#    f.Write("kdeTail")
#    f.Close()
    leg = TLegend(0.7, 0.65, 0.85, 0.85)
    leg.AddEntry(hTail,"MC Mismatch","lep")
    leg.AddEntry(f,"Mismatch evaluation","l")
    leg.SetBorderSize(0)
    leg.Draw()


def ReadEval():

    f  = TFile(f"{homeDir}/evalTail.root","READ")
#    kdeTail = (TKDE*)((TKDE*)f.Get("kdeTail")).Clone()
    kdeTail = TKDE()
    kdeTail.Read("")
    kdeTail.DrawClone()
    f.Close()

def evalTail(nPT=0):

  gStyle.SetOptTitle(0)
  gStyle.SetOptFit(0)
  gStyle.SetOptStat(0)

  ptBins = [5000., 6500., 8000., 10000., 12000., 14000., 20000.]  
  nt = TChain("DecayTree")
#   nt.Add("MC/Etac_MC_Tz_Tail.root")
#   nt.Add("MC/Jpsi_MC_Tz_Tail.root")

  #nt.Add(expDir+"Reduced_Mismatch.root")
  nt.Add(f"{homeDir}/Reduced_Mismatch.root")
  
  if nPT == 0:
    ntCut = nt.CopyTree("otz>=-10.0 && otz<=10.0")
  else:
    ptName = "%s < p_{T} < %s GeV/c"%(ptBins[nPT-1]/1000.,ptBins[nPT]/1000.)  
    print(f"otz>=-10.0 && otz<=10.0 && Jpsi_PT>{ptBins[nPT-1]/1000.} && Jpsi_PT<{ptBins[nPT]/1000.}")
    ntCut = nt.CopyTree(f"otz>=-10.0 && otz<=10.0 && Jpsi_PT>{ptBins[nPT-1]/1000.} && Jpsi_PT<{ptBins[nPT]/1000.}")
      
  logging.info(f"Get {nt.GetEntries()} entries")
  logging.info(f"Get {ntCut.GetEntries()} entries after cut")
  logging.info("Tail tree: OK \n")
  
  nEntries = ntCut.GetEntries()
  nEntriesUsed = nEntries
  Jpsi_Tz = array("d",[0])
  ntCut.SetBranchAddress("otz",Jpsi_Tz)
  
  tz = array("d",nEntriesUsed*[0])
  hTail = TH1D("hTail","h",100, -10.0, 10.0)

  for iEn in range(nEntriesUsed):

    ntCut.GetEntry(iEn)
    tz[iEn] = Jpsi_Tz[0]
    hTail.Fill(tz[iEn])

  logging.info("DataSet fill status: OK \n")

  
  #  rho : Factor which can be used to tune the smoothing.
  #  It is used as multiplicative factor for the fixed and adaptive bandwidth.
  #  A value < 1 will reproduce better the tails but oversmooth the peak
  #  while a factor > 1 will overestimate the tail
  rho = 2.0
  kdeTail = TKDE(nEntriesUsed, tz, -10., 10., "kerneltype:gaussian", rho)
  fTail = kdeTail.GetFunction(1000, -10., 10.)
  # fTail  = TF1("fTail",kdeTail,-10.0,10.0,0)
  fTail.Print()
  
  logging.info("KeysPdf fill status: OK\n")
  
  c = TCanvas("Masses_Fit","Masses Fit",800,500)
  # logging.debug(f"fTail integral {fTail.Integral(-10.0,10.0,1e-12)} \n")
  # logging.debug(f"hTail integral {hTail.Integral(hTail.FindBin(-10.0),hTail.FindBin(10.0))} \n")
  sc = 1/hTail.Integral(hTail.FindBin(-10.0),hTail.FindBin(10.0))/hTail.GetBinWidth(1)*fTail.Integral(-10.0,10.0)
  hTail.Scale(sc)
  # logging.info(f"Scale {sc} \n")
  # hTail.Scale(1/hTail.Integral(hTail.FindBin(-10.0),hTail.FindBin(10.0))/hTail.GetBinWidth(1)*fTail.Integral(-10.0,10.0))
#   hTail.Scale(1/hTail.Integral(1,100)/hTail.GetBinWidth(1))
#   hTail.Scale(1/hTail.GetEntries()/hTail.GetBinWidth(1))

  logging.info(hTail.Integral(hTail.FindBin(-10.0),hTail.FindBin(10.0)))
  logging.info(fTail.Integral(-10.0,10.0))
  logging.info(fTail.GetXmin())
  logging.info(fTail.GetXmax())
  logging.info(fTail.Integral(fTail.GetXmin(),fTail.GetXmax()))
  
  texMC = TLatex()
  texMC.SetNDC()
  
  hTail.Draw()
  hTail.GetXaxis().SetTitle("t_{z}, ps")
  hTail.SetMinimum(0.)
  hTail.SetMaximum(0.145)
  fTail.Draw("same")
  fTail.SetLineColor(2)
  fTail.SetNpx(5000)
  texMC.DrawLatex(0.18, 0.85, "LHCb")
  texMC.DrawLatex(0.18, 0.79, "#sqrt{s} = 13 TeV")
  if nPT!=0:
    texMC.DrawLatex(0.18, 0.73, ptName)
      
  c.SaveAs(f"Mismatch_PT{nPT}.pdf")
  
  fOut = TFile(f"{homeDir}/Mismatch_func_PT{nPT}.root","RECREATE")
  fTail.Write("fTail")
  fOut.Close()
  

if __name__ == "__main__":
    
    gROOT.LoadMacro("../libs/lhcbStyle.C")  
    evalTail(0)  
