from ROOT import gROOT, gPad, TFile, TH1D, TF1, TCanvas, TLatex, TLegend, TGaxis, TKDE, kFullCircle, kOpenCircle 
from array import array
import numpy as np

gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+");
gROOT.LoadMacro("../libs/libBifurcatedCB/BifurcatedCB.cxx+");
# homeDir = "/sps/lhcb/zhovkovska/Results_ppbar_2018/MC/MassFit/"
# homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/MC/etac/MassFit/"
# homeDir = "/sps/lhcb/zhovkovska/etac_ppbar/results/MC/MassFit/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/MC/etac/mass_fit/"


par_dict = {
    True     : ["sigma_etac_1","r_ref_etac","r_G1ToG2","r_NToW"],
    False    : ["sigma_etac_1","r_ref_etac","alpha_1","nCB_1"]
}
title_dict = {
    "PT"            : "p_{T}, GeV/c",
    "Y"             : "y",
    "sigma_etac_1"  : "#sigma_{#eta_{c}}, MeV/c^{2}",
    "r_ref_etac"    : "#sigma_{#eta_{c}}/#sigma_{J/#psi}",
    "r_G1ToG2"      : "#sigma_{n}/#sigma_{w}",
    "r_NToW"        : "f_{n}",
    "alpha_1"       : "#alpha",
    "nCB_1"         : "n^{CB}"
}

binning_dict = {
    "PT"    : np.array([5.0, 6.5, 8.0, 10.0, 12.0, 14.0, 20.0]),
    "Y"     : np.array([2.0, 2.5, 3.0, 3.5, 4.0])
}
def mass_par_fit(bins:str="PT", gauss:bool=True, kMC:bool = True):


    gROOT.ProcessLine('gStyle->SetOptStat(000000000)')
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')

    x_arr = binning_dict[bins]
    xMax = x_arr[-1]
    xMin = x_arr[0]
    nBins = x_arr.size - 1

    if gauss: add = ""
    else: add = "_CB"

    par_names = par_dict[gauss]
    n_par = len(par_names)

    f = TFile(f"{homeDir}/MC_MassResolution{add}_wksp.root","READ")
    # f = TFile("{}/Wksp_M_res_all_jpsi_sim{}.root".format(homeDir,add),"READ")
    wMC = f.Get("w")
    f.Close()

    vals_0 = dict()
    errs_0 = dict()
    funcs = dict()
    hist0 = dict()
    hists = dict()
    fits  = dict()

    for par in par_names:
        vals_0[par] = wMC.var(par).getVal()
        errs_0[par] = wMC.var(par).getError()
        hist0[par] = TH1D(f"h0_{par}", f"h0_{par}", 1, xMin, xMax)
        hist0[par].SetBinContent(1,vals_0[par])
        hist0[par].SetBinError(1,errs_0[par])
        funcs[par] = TF1("f_{}".format(par),"pol0", xMin, xMax)
        funcs[par].SetParameter(0, vals_0[par])
        funcs[par].SetLineColor(2)

        hists[par] = TH1D("h_{}".format(par),title_dict[par],nBins,x_arr)
        fits[par] = TF1("fit_{}".format(par),"pol1", xMin, xMax)
        fits[par].SetParName(0,f"p0_{par}")
        fits[par].SetParName(1,f"p1_{par}")
        fits[par].SetLineColor(1)

    for iBin in range(1, nBins+1):

        f = TFile("{}/MC_MassResolution{}_wksp_{}{}.root".format(homeDir,add,bins,iBin),"READ")
        wMC = f.Get("w")
        f.Close()

        for par in par_names:

            hists[par].SetBinContent(iBin,wMC.var(par).getValV())
            hists[par].SetBinError(iBin,wMC.var(par).getError())

    leg = TLegend(0.5,0.25,0.88,0.45)
    leg.AddEntry(0, "LHCb preliminary", "")
    leg.AddEntry(0, "#sqrt{s}=13 TeV", "")
    leg.SetLineWidth(0)


    fout = TFile("{}/parFit{}_GeV_{}{}.root".format(homeDir,add,bins,nBins), "recreate")

    c = TCanvas("Masses_Fit MC","Masses Fit MC",1200,900)
    c.Divide(int(n_par/2),2)
    n_pad = 1

    texJpsiPiMC1 = TLatex()
    texJpsiPiMC1.SetNDC()

    for par in par_names:
        c.cd(n_pad)
        n_pad+=1
        hists[par].Fit(fits[par],"I")

        gPad.SetLeftMargin(0.15)
        gPad.SetBottomMargin(0.15)
        hists[par].GetXaxis().SetTitle(title_dict[bins])
        hists[par].GetYaxis().SetTitle(title_dict[par])
        hists[par].SetLineWidth(2)
        hists[par].SetLineColor(1)
        hists[par].SetMarkerColor(2)
        hists[par].SetMarkerStyle(kFullCircle)
        #hists[par].SetMinimum(6.9)
        #hists[par].SetMaximum(8.2)
        hists[par].GetXaxis().SetNdivisions(510)
        # if par == "sigma_etac_1" or par=="nCB_1":
        if par=="nCB_1":
            hists[par].SetMaximum(1.175*hists[par].GetMaximum())
            hists[par].SetMinimum(0.825*hists[par].GetMinimum())
        else:
            hists[par].SetMaximum(1.1*hists[par].GetMaximum())
            hists[par].SetMinimum(0.9*hists[par].GetMinimum())
        hists[par].DrawCopy("0E1")
        # texJpsiPiMC1.DrawLatex(0.25, 0.80, "LHCb simulation")
        # texJpsiPiMC1.DrawLatex(0.25, 0.75, "#sqrt{s}=13 TeV")
        texJpsiPiMC1.DrawLatex(0.60, 0.25, "LHCb simulation")
        texJpsiPiMC1.DrawLatex(0.60, 0.20, "#sqrt{s}=13 TeV")
        hist0[par].SetMarkerSize(0)
        hist0[par].SetFillColor(2)
        hist0[par].SetFillStyle(3002)
        hist0[par].Draw("E2 same")
        # funcs[par].Draw("same")

        hists[par].Write()
        funcs[par].Write()
        fits[par].Write()

    fout.Write()
    fout.Close()

    c.SaveAs("{}{}_dist_mass{}.pdf".format(bins,nBins,add))

def mass_pars(gauss=True):


    gROOT.ProcessLine('gStyle->SetOptStat(000000000)')
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')

    nPTBins = 6

    if gauss: add = ""
    else: add = "_CB"

    pt = array( 'f',[5.0, 6.5, 8.0, 10.0, 12.0, 14.0, 20.0])
    #pt = array( 'f',[2.0, 2.4, 2.8, 3.3, 4.5])
    #pt = array( 'f',[0.0, 100.0, 175.0, 250.0, 300.0])
    ptMax = pt[-1]
    ptMin = pt[0]


    f = TFile("{}/MC_MassResolution{}_wksp.root".format(homeDir,add),"READ")
    wMC = f.Get("w")
    f.Close()

    sigma = wMC.var("sigma_etac_1").getValV()
    relEtac2Jpsi = wMC.var("r_ref_etac").getValV()
    relG12G2 = wMC.var("r_G1ToG2").getValV()
    relN2W = wMC.var("r_NToW").getValV()

    f_sigma_mean = TF1("f_sigma_mean","pol0", ptMin, ptMax)
    f_sigma_mean.SetParameter(0, sigma)
    f_sigma_mean.SetLineColor(2)
    f_rel_mean = TF1("f_rel_mean","pol0", ptMin, ptMax)
    f_rel_mean.SetParameter(0, relEtac2Jpsi)
    f_rel_mean.SetLineColor(2)
    f_relRes_mean = TF1("f_relRes_mean","pol0", ptMin, ptMax)
    f_relRes_mean.SetParameter(0, relN2W)
    f_relRes_mean.SetLineColor(2)
    f_relYield_mean = TF1("f_relYield_mean","pol0", ptMin, ptMax)
    f_relYield_mean.SetParameter(0, relG12G2)
    f_relYield_mean.SetLineColor(2)


    h_sigma_mass = TH1D("h_sigma_mass","#sigma_{mass #etac_{c}} DATA",nPTBins, pt)

    h_sigma_mass_MC = TH1D("h_sigma_mass_MC","#sigma_{mass #etac_{c}} MC",nPTBins,pt)
    h_sigma_mass_MC_fromB = TH1D("h_sigma_mass_MC_fromB","#sigma_{mass #etac_{c}} MC from-b",nPTBins,pt)
    h_rEtacToJpsi = TH1D("h_rEtacToJpsi","#sigma_{#etac_{c}}/#sigma_{J/#psi} MC",nPTBins,pt)
    h_r_NToW = TH1D("h_r_NToW","#sigma_{Narr}/#sigma_{W} MC",nPTBins,pt)
    h_r_G1ToG2 = TH1D("h_r_G1ToG2","N_{Narr}/N_{W} MC",nPTBins,pt)


    for iPT in range(1, nPTBins+1):

        f = TFile("{}/MC_MassResolution{}_wksp_PT{}.root".format(homeDir,add,iPT),"READ")
        wMC = f.Get("w")
        f.Close()

        h_sigma_mass_MC.SetBinContent(iPT,wMC.var("sigma_etac_1").getValV())
        h_sigma_mass_MC.SetBinError(iPT,wMC.var("sigma_etac_1").getError())

        h_rEtacToJpsi.SetBinContent(iPT,wMC.var("r_ref_etac").getValV())
        h_r_NToW.SetBinContent(iPT,wMC.var("r_NToW").getValV())
        h_r_G1ToG2.SetBinContent(iPT,wMC.var("r_G1ToG2").getValV())
        h_rEtacToJpsi.SetBinError(iPT,wMC.var("r_ref_etac").getError())
        h_r_NToW.SetBinError(iPT,wMC.var("r_NToW").getError())
        h_r_G1ToG2.SetBinError(iPT,wMC.var("r_G1ToG2").getError())

        #f = TFile(homeDir+"Results/MassFit/fromB/Jpsi_PT/m_scaled/Wksp_MassFit_PT{}_Tz0_C0.root".format(iPT),"READ")
        #wMC = f.Get("w")
        #f.Close()

        #h_sigma_mass.SetBinContent(iPT,wMC.var("sigma_etac_1").getValV())
        #h_sigma_mass.SetBinError(iPT,wMC.var("sigma_etac_1").getError())


        #f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution_fromB_cuted_2016_wksp_PT{}.root".format(iPT),"READ")
        #wMC = f.Get("w")
        #f.Close()

        #h_sigma_mass_MC_fromB.SetBinContent(iPT,wMC.var("sigma_etac_1").getValV())
        #h_sigma_mass_MC_fromB.SetBinError(iPT,wMC.var("sigma_etac_1").getError())


    fSigmaDATA = TF1("fSigmaDATA","pol1", ptMin, ptMax)
    fSigmaMC = TF1("fSigmaMC","pol1", ptMin, ptMax)
    fRel = TF1("fRel","pol1", ptMin, ptMax)
    fRelRes = TF1("fRelRes","pol1", ptMin, ptMax)
    fRelYield = TF1("fRelYield","pol1", ptMin, ptMax)

    fSigmaMC.SetLineColor(2)
    fRel.SetLineColor(2)

    h_sigma_mass.Fit(fSigmaDATA, "I")
    h_sigma_mass_MC.Fit(fSigmaMC, "I")
    h_rEtacToJpsi.Fit(fRel, "I")
    h_r_NToW.Fit(fRelRes, "I")
    h_r_G1ToG2.Fit(fRelYield, "I")

    leg = TLegend(0.5,0.25,0.88,0.45)
    leg.AddEntry(0, "LHCb preliminary", "")
    leg.AddEntry(0, "#sqrt{s}=13 TeV", "")
    leg.SetLineWidth(0)

    c0 = TCanvas("Masses_Fit","Masses Fit",1200,900)

    c0.cd(1)
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    h_sigma_mass.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_sigma_mass.GetYaxis().SetTitle("#sigma_{#etac_{c}} / [GeV/c^{2}]")
    h_sigma_mass.SetLineWidth(2)
    h_sigma_mass.SetLineColor(1)
    h_sigma_mass.SetMarkerColor(2)
    h_sigma_mass.SetMarkerStyle(kFullCircle)
    h_sigma_mass.GetXaxis().SetTitleSize(0.07)
    h_sigma_mass.GetYaxis().SetTitleSize(0.07)
    h_sigma_mass.GetXaxis().SetTitleOffset(0.90)
    h_sigma_mass.GetYaxis().SetTitleOffset(0.90)
    h_sigma_mass.GetXaxis().SetTitleFont(12)
    h_sigma_mass.GetYaxis().SetTitleFont(12)
    h_sigma_mass.GetXaxis().SetLabelSize(0.05)
    h_sigma_mass.GetYaxis().SetLabelSize(0.05)
    h_sigma_mass.GetXaxis().SetLabelFont(62)
    h_sigma_mass.GetYaxis().SetLabelFont(62)
    h_sigma_mass.GetXaxis().SetNdivisions(510)
    h_sigma_mass.DrawCopy("E1")

    TGaxis.SetMaxDigits(3)
    fSigmaDATA.Draw("same")
    fSigmaDATA.SetLineColor(2)
    leg.Draw()

    texJpsiPiMC1 = TLatex()
    texJpsiPiMC1.SetNDC()

    c0.SaveAs("PT6_dist_mass_DATA.pdf")

    c = TCanvas("Masses_Fit MC","Masses Fit MC",1200,900)
    c.Divide(2,2)

    c.cd(1)
    #h_sigma_mass_MC.GetXaxis().SetTitle("p_{T}, MeV/c")
    #h_sigma_mass_MC.GetYaxis().SetTitle("#sigma_{#etac_{c}}, MeV/c^{2}")
    #h_sigma_mass_MC_fromB.SetLineWidth(2)
    #h_sigma_mass_MC_fromB.SetLineColor(1)
    #h_sigma_mass_MC_fromB.SetMarkerColor(4)
    #h_sigma_mass_MC_fromB.SetMarkerStyle(kFullCircle)
    #h_sigma_mass_MC_fromB.DrawCopy("E1")
    #fSigmaMC.Draw("same")
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    h_sigma_mass_MC.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_sigma_mass_MC.GetYaxis().SetTitle("#sigma_{#etac_{c}}, MeV/c^{2}")
    h_sigma_mass_MC.SetLineWidth(2)
    h_sigma_mass_MC.SetLineColor(1)
    h_sigma_mass_MC.SetMarkerColor(2)
    h_sigma_mass_MC.SetMarkerStyle(kFullCircle)
    h_sigma_mass_MC.SetMinimum(6.9)
    h_sigma_mass_MC.SetMaximum(8.5)
    h_sigma_mass_MC.GetXaxis().SetNdivisions(510)
    h_sigma_mass_MC.DrawCopy("0E1")
    texJpsiPiMC1.DrawLatex(0.25, 0.80, "LHCb simulation")
    texJpsiPiMC1.DrawLatex(0.25, 0.75, "#sqrt{s}=13 TeV")
    #f_sigma_mean.Draw("same")

    c.cd(2)
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    h_rEtacToJpsi.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_rEtacToJpsi.GetYaxis().SetTitle("#sigma_{#etac_{c}}/#sigma_{J/#psi}")
    h_rEtacToJpsi.SetLineWidth(2)
    h_rEtacToJpsi.SetLineColor(1)
    h_rEtacToJpsi.SetMarkerColor(2)
    h_rEtacToJpsi.SetMinimum(0.87)
    h_rEtacToJpsi.SetMaximum(1.1)
    h_rEtacToJpsi.SetMarkerStyle(kFullCircle)
    h_rEtacToJpsi.DrawCopy("E1  0")
    #fRel.SetLineColor()
    #fRel.Draw("same")
    texJpsiPiMC1.DrawLatex(0.25, 0.80, "LHCb simulation")
    texJpsiPiMC1.DrawLatex(0.25, 0.75, "#sqrt{s}=13 TeV")
    f_rel_mean.Draw("same")

    c.cd(3)
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    h_r_NToW.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_r_NToW.GetYaxis().SetTitle("#sigma_{n}/#sigma_{w}")
    h_r_NToW.SetLineWidth(2)
    h_r_NToW.SetLineColor(1)
    h_r_NToW.SetMarkerColor(2)
    h_r_NToW.SetMinimum(0.125)
    h_r_NToW.SetMaximum(0.26)
    h_r_NToW.SetMarkerStyle(kFullCircle)
    h_r_NToW.DrawCopy("E1 0")
    #fRelRes.Draw("same")
    texJpsiPiMC1.DrawLatex(0.25, 0.35, "LHCb simulation")
    texJpsiPiMC1.DrawLatex(0.25, 0.3, "#sqrt{s}=13 TeV")
    f_relRes_mean.Draw("same")

    c.cd(4)
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    h_r_G1ToG2.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_r_G1ToG2.GetYaxis().SetTitle("f_{n}")
    h_r_G1ToG2.SetLineWidth(2)
    h_r_G1ToG2.SetLineColor(1)
    h_r_G1ToG2.SetMarkerColor(2)
    h_r_G1ToG2.SetMinimum(0.915)
    h_r_G1ToG2.SetMaximum(0.97)
    h_r_G1ToG2.SetMarkerStyle(kFullCircle)
    h_r_G1ToG2.DrawCopy("E1")
    #fRelYield.Draw("same")
    texJpsiPiMC1.DrawLatex(0.25, 0.35, "LHCb simulation")
    texJpsiPiMC1.DrawLatex(0.25, 0.3, "#sqrt{s}=13 TeV")
    f_relYield_mean.Draw("same")

    fout = TFile("{}/parFit{}_GeV_PT{}.root".format(homeDir,add,nPTBins), "recreate")
    h_sigma_mass.Write()
    h_rEtacToJpsi.Write()
    h_r_NToW.Write()
    h_r_G1ToG2.Write()
    fSigmaDATA.Write()
    fSigmaMC.Write()
    fRel.Write()
    #fRelRes.Write()
    #fRelYield.Write()
    c.Write()
    fout.Close()

    c.SaveAs("PT6_dist_mass{}.pdf".format(add))


def gamma_pars():

    nPTBins = 5

    sigma_mass = nPTBins*[0]
    gamma = nPTBins*[0]

    sigma_mass_err = nPTBins*[0]
    gamma_err = nPTBins*[0]

#    ptMass = [6.5,7.0, 8.0,9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 16.0, 18.]
#    pt = [6.5, 8.0, 10.0, 12.0, 14.0, 18.]
    pt = [2.0, 3.0, 4.0, 5.0, 6.5, 18.]
    ptMax = 18.
    ptMin = 6.5

    h_sigma_mass = TH1D("h_sigma_mass","#sigma_{mass J/#psi}",nPTBins,pt)
    h_gamma = TH1D("h_gamma","#Gamma_{#etac_{c}}",nPTBins,pt)


    for iPT in range(1, nPTBins+1):

#        sprintf(nameWksp, "TotFitResult/massEtac/PT_distributions/Reduced_Pmt_2016_wksp_gamma_PT%d.root",iPT)
        f = TFile("TotFitResult/massEtac/ProtonPT_distributions/Reduced_Pmt_2016_wksp_gamma_proton_PT{}.root".format(iPT),"READ")
        wMC = f.Get("w1")
        f.Close()

        h_gamma.SetBinContent(iPT,wMC.var("gamma_eta").getValV())
        h_gamma.SetBinError(iPT,wMC.var("gamma_eta").getError())
        gamma[iPT-1] = wMC.var("gamma_eta").getValV()
        gamma_err[iPT-1] = wMC.var("gamma_eta").getError()

#        sprintf(nameWksp, "TotFitResult/massEtac/PT_distributions/Reduced_Pmt_2016_wksp_sep_PT%d.root",iPT)
        f = TFile("TotFitResult/massEtac/ProtonPT_distributions/Reduced_Pmt_2016_wksp_sep_proton_PT{}.root".format(iPT),"READ")
        wMC = f.Get("w1")
        f.Close()

        h_sigma_mass.SetBinContent(iPT,wMC.var("sigma_Jpsi_1").getValV())
        h_sigma_mass.SetBinError(iPT,wMC.var("sigma_Jpsi_1").getError())
        sigma_mass[iPT-1] = wMC.var("sigma_Jpsi_1").getValV()
        sigma_mass_err[iPT-1] = wMC.var("sigma_Jpsi_1").getError()



    fSigma = TF1("fSigma","pol1",ptMin,ptMax)
    fGamma = TF1("fRel","pol1",ptMin,ptMax)

    h_sigma_mass.Fit(fSigma, "I")
    h_gamma.Fit(fGamma, "I")

    c = TCanvas("Masses_Fit","Masses Fit",600,800)
    c.Divide(1,2)
    #  c.cd(1).SetPad(.005, .405, .995, .995)
    #  gPad.SetLeftMargin(0.15)  frame.GetXaxis().SetTitle("J/#psi mass / [MeV/c^{2]")  frame.Draw()
    #  c.cd(2).SetPad(.005, .005, .995, .395)
    #  gPad.SetLeftMargin(0.15)  frame_res.GetXaxis().SetTitle("J/#psi mass / [MeV/c^{2]")  frame_res.Draw()

    c.cd(1)
    h_sigma_mass.GetXaxis().SetTitle("Proton p_{T}, MeV/c")
    h_sigma_mass.GetYaxis().SetTitle("#sigma_{mass J/#psi}, MeV/c^{2}")
    h_sigma_mass.SetLineWidth(2)
    h_sigma_mass.SetLineColor(1)
    h_sigma_mass.SetMarkerColor(2)
    h_sigma_mass.SetMarkerStyle(kFullCircle)
    h_sigma_mass.DrawCopy("E1")
    fSigma.Draw("same")
    gPad.SetLogx()


    c.cd(2)
    h_gamma.GetXaxis().SetTitle("Proton p_{T}, MeV/c")
    h_gamma.GetYaxis().SetTitle("#Gamma_{#etac_{c}}, MeV")
    h_gamma.SetLineWidth(2)
    h_gamma.SetLineColor(1)
    h_gamma.SetMarkerColor(2)
    h_gamma.SetMarkerStyle(kFullCircle)
    h_gamma.DrawCopy("E1")
    fGamma.Draw("same")
    gPad.SetLogx()

    fout = TFile (homeDir+"Results/Data/parFit_gamma_protonPT.root", "recreate")
    h_sigma_mass.Write()
    h_gamma.Write()
    fSigma.Write()
    fGamma.Write()
    c.Write()
    fout.Close()

    c.SaveAs("PT_dist_gamma_protonPT.pdf")



def nRel_gamma():

    nGamma = 10

    NRel = nGamma*[0]
    NRel_err = nGamma*[0]

    gamma = [15.0, 18.0, 21.0, 24.0, 27.0, 30.0, 33.0, 36.0, 39.0, 42.0, 45.0]
    gammaMax = 45.0

    h_NRel = TH1D("h_NRel","N_{#etac_{c/N_{J/#psi",nGamma,gamma)
    h_NRel_mean = TH1D("h_NRel_mean","#mu total",1,15.0, gammaMax)
#    TH1Dh_mu_mean_MC("h_mu_mean_MC","#mu MC total",1,6.5, ptMax)


#    sprintf(nameWksp, homeDir+"Results/MC/TzFit/JpsiVsEtac/MC_2016_wksp.root")
#    f = TFile(nameWksp,"READ")
#    wMC =  f.Get("w1")
#    f.Close()
#
#    h_mu_mean.SetBinContent(1,-0.00154759)
#    h_mu_mean.SetBinError(1,0.00167596)

    for iGamma in range(1, nGamma+1):

        f = TFile("TotFitResult/massEtac/Reduced_Pmt_2016_wksp_gamma_{}.root".format(iGamma),"READ")
        wMC =  f.Get("w1")
        f.Close()

        h_NRel.SetBinContent(iGamma,wMC.var("nEtacRel").getValV())
        h_NRel.SetBinError(iGamma,wMC.var("nEtacRel").getError())

        NRel[iGamma-1] = wMC.var("nEtacRel").getValV()
        NRel_err[iGamma-1] = wMC.var("nEtacRel").getError()


    kdeSigma = TKDE(nGamma, NRel[0], 15.0, gammaMax, "kerneltype:gaussian", 1.0)

    #  fSigma = kdeSigma.GetFunction(10000)
    #  fTauB = kdeTauB.GetFunction(10000)

    fYield = TF1("fYield","pol1",15.0,gammaMax)

    h_NRel.Fit(fYield,"I")

    legend = TLegend(0.1,0.7,0.48,0.9)
    legend.SetHeader("relative yield of #etac_{c","C") #option "C" allows to center the header
    legend.AddEntry(h_NRel,"N_{#etac_{c/N_{J/#psi","lep")
#    legend.AddEntry(h_NRel_mean,"#mu_{data","f")
#    legend.AddEntry(h_mu_mean_MC,"#mu_{MC","f")

    c = TCanvas("Masses_Fit","Masses Fit",800,500)

    c.cd()
#    h_NRel_mean.GetYaxis().SetRangeUser(-0.004, 0.004)
#    h_NRel_mean.SetFillStyle(3002)
#    h_NRel_mean.SetFillColor(15)
#    h_NRel_mean.DrawCopy("E2")
#
#    h_NRel_mean_MC.SetFillStyle(3003)
#    h_NRel_mean_MC.SetFillColor(4)
#    h_NRel_mean_MC.DrawCopy("E2 SAME")

    h_NRel.GetXaxis().SetTitle("gamma_{#etac_{c}}, MeV/c")
    h_NRel.SetLineWidth(2)
    h_NRel.SetLineColor(1)
    h_NRel.SetMarkerColor(2)
    h_NRel.SetMarkerStyle(kOpenCircle)
    h_NRel.DrawCopy("")

#    legend.Draw()

    fout = TFile ("TotFitResult/massEtac/yieldFit.root", "recreate")
    h_NRel.Write()
#    fYield.Write()
#    h_NRelmean.Write()
    c.Write()
    fout.Close()


    c.SaveAs("gamma_dist_yields.pdf")

if __name__ == "__main__":
    gROOT.LoadMacro("../libs/lhcbStyle.C")
    TGaxis.SetMaxDigits(2)
    #mass_pars()
    # mass_par_fit("PT", gauss=True)
    # mass_par_fit("Y", gauss=True)
    mass_par_fit("PT", gauss=False)
    mass_par_fit("Y", gauss=False)
        #mass_pars(False)
    #mass_parsCB()
