merged_filepath_prefix = '/eos/lhcb/wg/BandQ/PRODPOL/etac2s_2018/ppbar'

input_tree_name     = 'Jpsi2ppTuple/DecayTree'
input_tree_nameLumi = 'GetIntegratedLuminosity/LumiTuple'

import os, sys, re
from ROOT import * 




cuts = {
  "Hlt1": "Jpsi_Hlt1DiProtonHighDecision_TOS",
  "PT65only": "Jpsi_Hlt1DiProtonHighDecision_TOS && Jpsi_PT>6500",
  "Def": "Jpsi_Hlt1DiProtonHighDecision_TOS && Jpsi_PT>6500 && ProtonP_PIDp>20 && ProtonM_PIDp>20 && (ProtonP_PIDp-ProtonP_PIDK)>15 && ProtonM_PIDp>20 && (ProtonM_PIDp-ProtonM_PIDK)>15",
  "DefPIDonly": "Jpsi_Hlt1DiProtonHighDecision_TOS && ProtonP_PIDp>20 && ProtonM_PIDp>20 && (ProtonP_PIDp-ProtonP_PIDK)>15 && ProtonM_PIDp>20 && (ProtonM_PIDp-ProtonM_PIDK)>15",
  "SoftPIDonly": "Jpsi_Hlt1DiProtonHighDecision_TOS && ProtonP_PIDp>15 && ProtonM_PIDp>15 && (ProtonP_PIDp-ProtonP_PIDK)>10 && ProtonM_PIDp>15 && (ProtonM_PIDp-ProtonM_PIDK)>10",
  "SoftPIDPT65": "Jpsi_Hlt1DiProtonHighDecision_TOS && ProtonP_PIDp>15 && ProtonM_PIDp>15 && (ProtonP_PIDp-ProtonP_PIDK)>10 && ProtonM_PIDp>15 && (ProtonM_PIDp-ProtonM_PIDK)>10 && Jpsi_PT>6500",
  "TightProbNNonly": "Jpsi_Hlt1DiProtonHighDecision_TOS && ProtonP_ProbNNp>0.97 && ProtonM_ProbNNp>0.97",
  "TightProbNN_PT65": "Jpsi_Hlt1DiProtonHighDecision_TOS && ProtonP_ProbNNp>0.97 && ProtonM_ProbNNp>0.97 && Jpsi_PT>6500",
  "DefPID_PT8": "Jpsi_Hlt1DiProtonHighDecision_TOS && Jpsi_PT>8000 && ProtonP_PIDp>20 && ProtonM_PIDp>20 && (ProtonP_PIDp-ProtonP_PIDK)>15 && ProtonM_PIDp>20 && (ProtonM_PIDp-ProtonM_PIDK)>15",
  "Def_FD_IP": "Jpsi_Hlt1DiProtonHighDecision_TOS && Jpsi_PT>6500 && ProtonP_PIDp>20 && ProtonM_PIDp>20 && (ProtonP_PIDp-ProtonP_PIDK)>15 && ProtonM_PIDp>20 && (ProtonM_PIDp-ProtonM_PIDK)>15 && Jpsi_FDCHI2_OWNPV>100 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16"
}





def plotHistos():

  ch = TChain(input_tree_name)
  for ii in range(300):
    ch.Add(merged_filepath_prefix+"/1002/"+str(ii)+"/Tuple.root")
    ch.Add(merged_filepath_prefix+"/1003/"+str(ii)+"/Tuple.root")

  M_hist = TH1F("M_hist","M_hist",250,3300.,3800.)

  for key in cuts.keys():
    ch.Draw("Jpsi_M>>M_hist",cuts[key],"E")
    M_hist.SaveAs("../Histos/"+key+".root")

  LumiCh = TChain(input_tree_nameLumi)
  Lumi_hist = TH1F("Lumi_hist","Lumi_hist",100,0.,10.)
  LumiCh.Draw("IntegratedLuminosity>>Lumi_hist")
  Lumi_hist.SaveAs("../Histos/intLumi.root")

plotHistos()


