from ROOT import TChain, TFile, TH1D
from ROOT.TMath import Abs, Log
from array import array
import logging
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"
#dataDir = "/eos/user/v/vazhovko/etac2s_ppbar/"

pid = {
    "Etac":   441,
    "etac":   441,      #from-b
    # "etac":    443,     #prompt
    "Jpsi":    443,
    "jpsi":    443,
    "pppi0":   443,
    "etac2S": 100441,   #from-b
    # "etac2S": 100443,   #prompt
    "chic0":  10441,
    "chic1":  20443,
    "chic2":  445,
    "Psi2S":  100443,
    "psi2S":  100443,
    "proton": 2212

}

bMesID = [5, 511, 521, 531, 541, -511, -521, -531, -541, 513, 523, 533, -513, -523, -533]
bHadrID = [5, 511, 521, 531, 541, 513, 523, 533, 515, 525, 535, 10511, 10521, 10531, 5122, 5112, 5212, 5222, 5132, 5232, 5332, 5242, 5114, 5214, 5224, 5424, 20553, 555]#, -5, -511, -521, -531, -541, -513, -523, -533, -515, -525, -535, -10511, -10521, -10531, -5122, -5112, -5212, -5222, -5132, -5232, -5332]
charmID = [ 441, 10441, 100441, 443, 10443, 20443, 100443, 30443, 9000443, 9010443, 9020443, 445, 100445]
dirCgarmID = [0, 4, -4, 21]

def selectAcc(kRange: str):
    ''' Select events from MC generator tuple, add new branches and save to new file '''
    nt = TChain("MCDecayTreeTuple/MCDecayTree")
    kMag = "*"
    nt.Add(f"{dataDir}/MC/Generator/Tuple-{kRange}-Total*.root")

    #new branches
    prompt     = array( "l", [False])
    sec        = array( "l", [False])
    Jpsi_TRUEY = array("d", [0.])

    f = TFile(f"{dataDir}/MC/Generator/Tuple-{kRange}-Total_AddBr.root", "RECREATE")

    tree = nt.CloneTree(0)
    tree.Branch("Jpsi_prompt",prompt,"prompt/O")
    tree.Branch("Jpsi_sec",sec,"sec/O")
    tree.Branch("Jpsi_TRUEY",Jpsi_TRUEY,"Jpsi_TRUEY/D")

    nEntries = nt.GetEntries()

    print("OK")

    for event in nt:

        prompt[0] = False
        sec[0] = False

        if (nt.Jpsi_MC_MOTHER_ID in dirCgarmID ):

            prompt[0] = True
            sec[0] = False

        elif (nt.Jpsi_MC_MOTHER_ID in charmID):

            if (((nt.Jpsi_MC_GD_MOTHER_ID) in dirCgarmID)  or ((nt.Jpsi_MC_GD_MOTHER_ID in charmID) and ((nt.Jpsi_MC_GD_GD_MOTHER_ID in dirCgarmID) or (nt.Jpsi_MC_GD_GD_MOTHER_ID in charmID)))):

                prompt[0] = True
                sec[0] = False

            elif ( nt.Jpsi_MC_GD_MOTHER_ID in charmID and Abs(nt.Jpsi_MC_GD_GD_MOTHER_ID) in bHadrID ):

                prompt[0] = False
                sec[0] = True

            else:
                if( Abs(nt.Jpsi_MC_GD_MOTHER_ID) in bHadrID):
                    prompt[0] = False
                    sec[0] = True
                else:
                    print(nt.Jpsi_MC_MOTHER_ID, nt.Jpsi_MC_GD_MOTHER_ID, nt.Jpsi_MC_GD_GD_MOTHER_ID)

        else:

            if( Abs(nt.Jpsi_MC_MOTHER_ID) in bHadrID):

                prompt[0] = False
                sec[0] = True
            else:
                print(nt.Jpsi_MC_MOTHER_ID, nt.Jpsi_MC_GD_MOTHER_ID, nt.Jpsi_MC_GD_GD_MOTHER_ID)

        Jpsi_TRUEY[0] = 0.5*Log((nt.Jpsi_TRUEP_E + nt.Jpsi_TRUEP_Z)/(nt.Jpsi_TRUEP_E-nt.Jpsi_TRUEP_Z))
        tree.Fill()



    #c = TCanvas("Signal_Fit","Signal Fit",1600,1000)
    #gPad.SetLeftMargin(0.15)

    #pad = c.cd(1)
    #tree.Draw("Jpsi_TRUEPV_X")
    #pad.RangeAxis(-100, 0, 100, 1e6)
    #pad.SetLogy()

    #c.SaveAs("%s_prompt.pdf"%(kRange))
    tree.Write()
    f.Close()
    print(kRange)
    #del tree, nt

def selectGen(kSource: str, kRange: str, kMag: str = "*", kTree: bool=True):
    ''' Select events from reconstructed tuple without selection, add new branches and save to new file '''
    postfix=""
    # postfix="_Sim09j"
    nt = TChain("MCDecayTree")
    # nt.Add("{}/MC/NoTrigger/{}DiProton_{}_Mag{}_2018*.root".format(dataDir, kRange, kSource, kMag))
    nt.Add(f"{dataDir}/MC/NoTrigger/{kRange}DiProton_{kSource}_Mag{kMag}_2018{postfix}.root")

    #new branches
    prompt = array( "l", [False])
    sec = array( "l", [False])
    Jpsi_TRUEPV_X = array( "d", [0])
    Jpsi_TRUEPV_Y = array( "d", [0])
    Jpsi_TRUEPV_Z = array( "d", [0])
    Jpsi_TRUEY = array("d", [0.])

    if kTree:
        if kMag in ["Up","Down"]:
            f = TFile("{dataDir}/MC/NoTrigger/{kRange}DiProton_{kSource}_Mag{kMag}_2018{postfix}_AddBr.root", "RECREATE")
        else:
            f = TFile("{dataDir}/MC/NoTrigger/{kRange}DiProton_{kSource}_2018{postfix}_AddBr.root", "RECREATE")

        tree = nt.CloneTree(0)
        tree.Branch("Jpsi_prompt",prompt,"prompt/O")
        tree.Branch("Jpsi_sec",sec,"sec/O")

        tree.Branch("Jpsi_TRUEPV_X",Jpsi_TRUEPV_X,"Jpsi_TRUEPV_X/D")
        tree.Branch("Jpsi_TRUEPV_Y",Jpsi_TRUEPV_Y,"Jpsi_TRUEPV_Y/D")
        tree.Branch("Jpsi_TRUEPV_Z",Jpsi_TRUEPV_Z,"Jpsi_TRUEPV_Z/D")
        tree.Branch("Jpsi_TRUEY",Jpsi_TRUEY,"Jpsi_TRUEY/D")

    nEntries = nt.GetEntries()


    for event in nt:

        prompt[0] = False
        sec[0] = False

        if (nt.Jpsi_MC_MOTHER_ID in dirCgarmID ):

            prompt[0] = True
            sec[0] = False
            Jpsi_TRUEPV_X[0] = nt.Jpsi_TRUEENDVERTEX_X
            Jpsi_TRUEPV_Y[0] = nt.Jpsi_TRUEENDVERTEX_Y
            Jpsi_TRUEPV_Z[0] = nt.Jpsi_TRUEENDVERTEX_Z

        elif (nt.Jpsi_MC_MOTHER_ID in charmID):

            if (((nt.Jpsi_MC_GD_MOTHER_ID) in dirCgarmID)  or ((nt.Jpsi_MC_GD_MOTHER_ID in charmID) and ((nt.Jpsi_MC_GD_GD_MOTHER_ID in dirCgarmID) or (nt.Jpsi_MC_GD_GD_MOTHER_ID in charmID)))):

                prompt[0] = True
                sec[0] = False
                Jpsi_TRUEPV_X[0] = nt.Jpsi_MC_MOTHER_TRUEV_X
                Jpsi_TRUEPV_Y[0] = nt.Jpsi_MC_MOTHER_TRUEV_Y
                Jpsi_TRUEPV_Z[0] = nt.Jpsi_MC_MOTHER_TRUEV_Z

            elif ( nt.Jpsi_MC_GD_MOTHER_ID in charmID and Abs(nt.Jpsi_MC_GD_GD_MOTHER_ID) in bHadrID ):

                prompt[0] = False
                sec[0] = True
                Jpsi_TRUEPV_X[0] = nt.Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_X
                Jpsi_TRUEPV_Y[0] = nt.Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Y
                Jpsi_TRUEPV_Z[0] = nt.Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Z

            else:
                if( Abs(nt.Jpsi_MC_GD_MOTHER_ID) in bHadrID):
                    prompt[0] = False
                    sec[0] = True
                    Jpsi_TRUEPV_X[0] = nt.Jpsi_MC_GD_GD_MOTHER_TRUEV_X
                    Jpsi_TRUEPV_Y[0] = nt.Jpsi_MC_GD_GD_MOTHER_TRUEV_Y
                    Jpsi_TRUEPV_Z[0] = nt.Jpsi_MC_GD_GD_MOTHER_TRUEV_Z
                else:
                    print(nt.Jpsi_MC_MOTHER_ID, nt.Jpsi_MC_GD_MOTHER_ID, nt.Jpsi_MC_GD_GD_MOTHER_ID)

        else:

            if( Abs(nt.Jpsi_MC_MOTHER_ID) in bHadrID):

                prompt[0] = False
                sec[0] = True
                Jpsi_TRUEPV_X[0] = nt.Jpsi_MC_GD_MOTHER_TRUEV_X
                Jpsi_TRUEPV_Y[0] = nt.Jpsi_MC_GD_MOTHER_TRUEV_Y
                Jpsi_TRUEPV_Z[0] = nt.Jpsi_MC_GD_MOTHER_TRUEV_Z
            else:
                print(nt.Jpsi_MC_MOTHER_ID, nt.Jpsi_MC_GD_MOTHER_ID, nt.Jpsi_MC_GD_GD_MOTHER_ID)

        Jpsi_TRUEY[0] = 0.5*Log((nt.Jpsi_TRUEP_E + nt.Jpsi_TRUEP_Z)/(nt.Jpsi_TRUEP_E-nt.Jpsi_TRUEP_Z))
        if kTree:
            tree.Fill()


    #c = TCanvas("Signal_Fit","Signal Fit",1600,1000)
    #gPad.SetLeftMargin(0.15)

    #pad = c.cd(1)
    #tree.Draw("Jpsi_TRUEPV_X")
    #pad.RangeAxis(-100, 0, 100, 1e6)
    #pad.SetLogy()

    #c.SaveAs("%s_prompt.pdf"%(kRange))
    if kTree:
        tree.Write()

        f.Close()
    print(kRange, kSource)
    # del tree, nt



cut_accept  = "ProtonP_TRUETHETA>0.01 && ProtonP_TRUETHETA<0.4 && ProtonM_TRUETHETA>0.01 && ProtonM_TRUETHETA<0.4"#" && 0.5*TMath::Log((Jpsi_TRUEP_E + Jpsi_TRUEP_Z)/(Jpsi_TRUEP_E-Jpsi_TRUEP_Z))>2. && 0.5*TMath::Log((Jpsi_TRUEP_E + Jpsi_TRUEP_Z)/(Jpsi_TRUEP_E-Jpsi_TRUEP_Z))<4.5 "#" && nSPDHits < 300" #
cut_true_v  = "Jpsi_TRUEPT > 5000 && Jpsi_TRUEPT < 20000 && Jpsi_TRUEY > 2.0 && Jpsi_TRUEY < 4.0"
cut_reco_v  = "Jpsi_PT > 5000 && Jpsi_PT < 20000 && Jpsi_Y > 2.0 && Jpsi_Y < 4.0"
# cut_ProbNN  = "ProtonP_ProbNNk < 0.6 && ProtonM_ProbNNk < 0.6 && ProtonP_ProbNNp > 0.6 && ProtonM_ProbNNp > 0.6 && ProtonP_ProbNNpi < 0.6 && ProtonM_ProbNNpi < 0.6 "
# cut_ProbNN  = "ProtonP_ProbNNp > 0.8 && ProtonM_ProbNNp > 0.8"
probnn = "0.6"
cut_ProbNN  = f"ProtonP_ProbNNp > {probnn} && ProtonM_ProbNNp > {probnn}"
cut_PID     = "(ProtonP_PIDp-ProtonP_PIDK)>0 && (ProtonM_PIDp-ProtonM_PIDK)>0 && ProtonP_PIDp>5 && ProtonM_PIDp>5"
cut_Trigger = "Jpsi_L0HadronDecision_TOS && (Jpsi_Hlt1DiProtonDecision_TOS || Jpsi_Hlt1DiProtonHighDecision_TOS)"


cut_select  = "     nSPDHits < 300 && \
                    ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
                    ProtonP_PT>2000 && ProtonM_PT>2000 && \
                    ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366"

# cut_total = "{} && {} && {} && {}".format(cut_reco_v, cut_select, cut_Trigger, cut_ProbNN)
cut_total = "{} && {} && {}".format(cut_accept, cut_reco_v, cut_select)

def select(kSource: str, kRange: str, kMag: str = "*", kSel: bool=False, kTree: bool=True):
    ''' Select events from MC, add new branches and save to a new file '''
    postfix = "Sim09j_AddBr"
    nt = TChain("DecayTree")

    if kSel: 
        # postfix="Sim09j_sel"
        postfix="sel"

        nt0 = TChain("DecayTree")
        # nt0.Add("{}/MC/Trigger/{}DiProton_{}_Mag{}_2018.root".format(dataDir, kRange, kSource, kMag))
        nt0.Add("{}/MC/NoTurbo/{}DiProton_{}_Mag{}_2018.root".format(dataDir, kRange, kSource, kMag))

        nt = nt0.CopyTree(cut_total)
        logging.info("Creating a tree with selection ......................")
    else:
        # nt.Add("{}/MC/Trigger/{}DiProton_{}_Mag{}_2018.root".format(dataDir, kRange, kSource, kMag))
        # nt.Add("{}/MC/Trigger/{}DiProton_{}_Mag{}_2018_Sim09j.root".format(dataDir, kRange, kSource, kMag))
        nt.Add("{}/MC/NoTurbo/{}DiProton_{}_Mag{}_2018.root".format(dataDir, kRange, kSource, kMag))
        logging.info("Creating a tree ......................")

    #new branches
    prompt = array( "l", [False])
    sec = array( "l", [False])
    Jpsi_M_res = array( "d", [0])
    Jpsi_TRUEM = array( "d", [0])
    Jpsi_Tz = array( "d", [0])
    Jpsi_Tz_res = array( "d", [0])
    Jpsi_Dist_CHI2 = array( "d", [0])
    Jpsi_TRUEY = array( "d", [0])
    Jpsi_TRUEPV_X = array( "d", [0])
    Jpsi_TRUEPV_Y = array( "d", [0])
    Jpsi_TRUEPV_Z = array( "d", [0])
    Jpsi_TRUETz = array( "d", [0])
    nScaledTracks = array( "i", [0])
    ProtonM_ETA = array( "d", [0])
    ProtonP_ETA = array( "d", [0])

    # #true variables
    # Jpsi_TRUEP_E = array( "d", [0])
    # Jpsi_TRUEP_X = array( "d", [0])
    # Jpsi_TRUEP_Y = array( "d", [0])
    # Jpsi_TRUEP_Z = array( "d", [0])
    # Jpsi_TRUE_Tz = array( "d", [0])
    # Jpsi_TRUEENDVERTEX_X = array( "d", [0])
    # Jpsi_TRUEENDVERTEX_Y = array( "d", [0])
    # Jpsi_TRUEENDVERTEX_Z = array( "d", [0])

    nLT = array( "i", [0])
    nDT = array( "i", [0])
    nT  = array( "i", [0])

    if kTree:
        if kMag in ["Up","Down"]:
            f = TFile("{}/MC/NoTurbo/{}DiProton_{}_Mag{}_2018_{}.root".format(dataDir, kRange, kSource, kMag, postfix), "RECREATE")
            # f = TFile("{}/MC/Trigger/{}DiProton_{}_Mag{}_2018_{}.root".format(dataDir, kRange, kSource, kMag, postfix), "RECREATE")
        else:
            f = TFile("{}/MC/NoTurbo/{}DiProton_{}_2018_{}.root".format(dataDir, kRange, kSource, postfix), "RECREATE")
            # f = TFile("{}/MC/Trigger/{}DiProton_{}_2018_{}.root".format(dataDir, kRange, kSource, postfix), "RECREATE")

        tree = nt.CloneTree(0)
        tree.Branch("Jpsi_prompt",prompt,"prompt/O")
        tree.Branch("Jpsi_sec",sec,"sec/O")
        tree.Branch("Jpsi_M_res",Jpsi_M_res,"Jpsi_M_res/D")
        tree.Branch("Jpsi_TRUEY",Jpsi_TRUEY,"Jpsi_TRUEY/D")
        #tree.Branch("Jpsi_TRUEM",Jpsi_TRUEM,"Jpsi_TRUEM/D")
        tree.Branch("Jpsi_Tz",Jpsi_Tz,"Jpsi_Tz/D")
        tree.Branch("Jpsi_Tz_res",Jpsi_Tz_res,"Jpsi_Tz_res/D")
        tree.Branch("Jpsi_Dist_CHI2",Jpsi_Dist_CHI2,"Jpsi_Dist_CHI2/D")

        tree.Branch("Jpsi_TRUEPV_X",Jpsi_TRUEPV_X,"Jpsi_TRUEPV_X/D")
        tree.Branch("Jpsi_TRUEPV_Y",Jpsi_TRUEPV_Y,"Jpsi_TRUEPV_Y/D")
        tree.Branch("Jpsi_TRUEPV_Z",Jpsi_TRUEPV_Z,"Jpsi_TRUEPV_Z/D")
        tree.Branch("Jpsi_TRUETz",Jpsi_TRUETz,"Jpsi_TRUETz/D")
        tree.Branch("nScaledTracks",nScaledTracks,"nScaledTracks/I")

        tree.Branch("ProtonM_ETA",ProtonM_ETA,"ProtonM_ETA/D")
        tree.Branch("ProtonP_ETA",ProtonP_ETA,"ProtonP_ETA/D")
        logging.info("New tree with branches was created ......................")


    nEntries = nt.GetEntries()

    mother_pid = pid[kRange]
    if "etac" in kRange and kSource=="prompt":
        mother_pid += 2

    proton_pid = pid["proton"]
    logging.info(f"Evaluating {nEntries} entries ......................")

    for event in nt:
        prompt[0] = False
        sec[0] = False


        if (nt.ProtonM_P - nt.ProtonM_PZ)>1e-3:
            ProtonM_ETA[0] = 0.5*Log( (nt.ProtonM_P + nt.ProtonM_PZ)/(nt.ProtonM_P - nt.ProtonM_PZ) )
        else:
            ProtonM_ETA[0] = 0.

        if (nt.ProtonP_P - nt.ProtonP_PZ)>1e-3:
            ProtonP_ETA[0] = 0.5*Log( (nt.ProtonP_P + nt.ProtonP_PZ)/(nt.ProtonP_P - nt.ProtonP_PZ) )
        else:
            ProtonP_ETA[0] = 0.

        if nt.Jpsi_TRUEID==mother_pid and Abs(nt.ProtonP_TRUEID)==proton_pid and Abs(nt.ProtonM_TRUEID)==proton_pid:

            Jpsi_Tz[0] = (3.3* (nt.Jpsi_ENDVERTEX_Z - nt.Jpsi_OWNPV_Z)*nt.Jpsi_MM/nt.Jpsi_PZ)
            #Jpsi_TRUEM[0] = Sqrt(nt.Jpsi_TRUEP_E*nt.Jpsi_TRUEP_E - (nt.Jpsi_TRUEP_X*nt.Jpsi_TRUEP_X + nt.Jpsi_TRUEP_Y*nt.Jpsi_TRUEP_Y + nt.Jpsi_TRUEP_Z*nt.Jpsi_TRUEP_Z))
            Jpsi_M_res[0] = nt.Jpsi_M - nt.Jpsi_TRUEM

            if (nt.Jpsi_MC_MOTHER_ID in dirCgarmID ):

                prompt[0] = True
                sec[0] = False
                logging.debug(f"prompt:::::: Jpsi_MC_MOTHER_ID: {nt.Jpsi_MC_MOTHER_ID}")
            elif (nt.Jpsi_MC_MOTHER_ID in charmID):

                if (((nt.Jpsi_MC_GD_MOTHER_ID) in dirCgarmID)  or ((nt.Jpsi_MC_GD_MOTHER_ID in charmID) and ((nt.Jpsi_MC_GD_GD_MOTHER_ID in dirCgarmID) or (nt.Jpsi_MC_GD_GD_MOTHER_ID in charmID)))):

                    prompt[0] = True
                    sec[0] = False
                    logging.debug(f"prompt:::::: Jpsi_MC_MOTHER_ID: {nt.Jpsi_MC_MOTHER_ID}, Jpsi_MC_GD_MOTHER_ID: {nt.Jpsi_MC_GD_MOTHER_ID}, Jpsi_MC_GD_GD_MOTHER_ID: {nt.Jpsi_MC_GD_GD_MOTHER_ID}")
                elif ( nt.Jpsi_MC_GD_MOTHER_ID in charmID and Abs(nt.Jpsi_MC_GD_GD_MOTHER_ID) in bHadrID ):

                    prompt[0] = False
                    sec[0] = True
                    logging.debug(f"sec:::::: Jpsi_MC_MOTHER_ID: {nt.Jpsi_MC_MOTHER_ID}, Jpsi_MC_GD_MOTHER_ID: {nt.Jpsi_MC_GD_MOTHER_ID}, Jpsi_MC_GD_GD_MOTHER_ID: {nt.Jpsi_MC_GD_GD_MOTHER_ID}")
                else:
                    if( Abs(nt.Jpsi_MC_GD_MOTHER_ID) in bHadrID):
                        prompt[0] = False
                        sec[0] = True
                        logging.debug(f"sec:::::: Jpsi_MC_MOTHER_ID: {nt.Jpsi_MC_MOTHER_ID}, Jpsi_MC_GD_MOTHER_ID: {nt.Jpsi_MC_GD_MOTHER_ID}, Jpsi_MC_GD_GD_MOTHER_ID: {nt.Jpsi_MC_GD_GD_MOTHER_ID}")
                    else:
                        logging.info(f"failed::::::: Jpsi_MC_MOTHER_ID: {nt.Jpsi_MC_MOTHER_ID}, Jpsi_MC_GD_MOTHER_ID: {nt.Jpsi_MC_GD_MOTHER_ID}, Jpsi_MC_GD_GD_MOTHER_ID: {nt.Jpsi_MC_GD_GD_MOTHER_ID}")

            else:

                if( Abs(nt.Jpsi_MC_MOTHER_ID) in bHadrID):

                    prompt[0] = False
                    sec[0] = True
                    logging.debug(f"sec:::::: Jpsi_MC_MOTHER_ID: {nt.Jpsi_MC_MOTHER_ID}")
                else:
                    logging.info(f"failed::::::: Jpsi_MC_MOTHER_ID: {nt.Jpsi_MC_MOTHER_ID}, Jpsi_MC_GD_MOTHER_ID: {nt.Jpsi_MC_GD_MOTHER_ID}, Jpsi_MC_GD_GD_MOTHER_ID: {nt.Jpsi_MC_GD_GD_MOTHER_ID}")

            #Dist_CHI2 = ( TMath.Abs(float(Jpsi_TRUEPV_X[0])-PVX[0])/(PVXERR[0]) + TMath.Abs(float(Jpsi_TRUEPV_Y[0])-PVY[0])/(PVYERR[0]) + TMath.Abs(float(Jpsi_TRUEPV_Z[0])-PVZ[0])/(PVZERR[0]))
            #Jpsi_Dist_CHI2[0] = ( Abs(float(nt.Jpsi_TRUEENDVERTEX_X)-nt.PVX)/(nt.PVXERR) + Abs(float(nt.Jpsi_TRUEENDVERTEX_Y)-nt.PVY)/(nt.PVYERR) + Abs(float(nt.Jpsi_TRUEENDVERTEX_Z)-nt.PVZ)/nt.PVZERR)
            Jpsi_Dist_CHI2[0] = -1000.
            if nt.Jpsi_TRUEP_Z!=0:
                Jpsi_TRUETz[0] = (3.3* (nt.Jpsi_TRUEENDVERTEX_Z - nt.Jpsi_TRUEP_Z)*nt.Jpsi_TRUEM/nt.Jpsi_TRUEP_Z)
            else:
                Jpsi_TRUETz[0] = -3e8
            Jpsi_Tz_res[0] = Jpsi_Tz[0] - nt.Jpsi_TRUE_Tz
            nScaledTracks[0] = int(3.25*nt.nTracks + 0.5)  #+0.5 to round off correctly
            Jpsi_TRUEY[0] = 0.5*Log((nt.Jpsi_TRUEP_E + nt.Jpsi_TRUEP_Z)/(nt.Jpsi_TRUEP_E-nt.Jpsi_TRUEP_Z))
            if (not prompt[0]) and (not sec[0]):
                print(f"Non-matched ccbar: ID:{nt.Jpsi_MC_MOTHER_ID}, mID:{nt.Jpsi_MC_GD_MOTHER_ID}, gmID{nt.Jpsi_MC_GD_GD_MOTHER_ID}")
            if kTree:
                tree.Fill()



    #c = TCanvas("Signal_Fit","Signal Fit",1600,1000)
    #gPad.SetLeftMargin(0.15)

    #pad = c.cd(1)
    #tree.Draw("Jpsi_TRUEPV_X")
    #pad.RangeAxis(-100, 0, 100, 1e6)
    #pad.SetLogy()

    #c.SaveAs("%s_prompt.pdf"%(kRange))
    if kTree:
        tree.Write()
        f.Close()
    print(kRange, kSource)
    #del tree, nt

def add_branch(kSource: str, kRange: str, kMag: str):
    ''' Add a branch to the MC tree with the L0Hadron efficiency'''
    # kMag = "*"

    nt = TChain("DecayTree")
    nt.Add("{}/MC/Trigger/{}DiProton_{}_Mag{}_2018_Ecal.root".format(dataDir, kRange, kSource, kMag))
    # nt.Add("{}/MC/NoTrigger/{}DiProton_{}_2018_AddBr_oldDec.root".format(dataDir, kRange, kSource, kMag))

    f_L0 = TFile("{}/MC/Trigger/L0Calib/{}DiProton_{}_Mag{}_L0.root".format(dataDir, kRange, kSource, kMag), "READ")
    h_L0 = f_L0.Get("effJpsiL0Had")
    n_bins = h_L0.GetNbinsX()

    #new branch to add
    # Jpsi_TRUEY        = array( "d", [0])
    Jpsi_L0Hadron_eff     = array( "d", [0])
    Jpsi_L0Hadron_eff_err = array( "d", [0])

    f    = TFile("{}/MC/Trigger/{}DiProton_{}_Mag{}_2018_L0.root".format(dataDir, kRange, kSource, kMag), "RECREATE")
    # f = TFile("{}/MC/Trigger/{}DiProton_{}_2018_oldDec_AddBr.root".format(dataDir, kRange, kSource), "RECREATE")

    tree = nt.CloneTree(0)
    # tree.Branch("Jpsi_TRUEY",Jpsi_TRUEY,"Jpsi_TRUEY/D")
    tree.Branch("Jpsi_L0Hadron_eff",Jpsi_L0Hadron_eff,"Jpsi_L0Hadron_eff/D")
    tree.Branch("Jpsi_L0Hadron_eff_err",Jpsi_L0Hadron_eff_err,"Jpsi_L0Hadron_eff_err/D")

    for event in nt:

        bin_PT = h_L0.FindBin(nt.Jpsi_PT/1e3)
        Jpsi_L0Hadron_eff[0] = h_L0.GetBinContent(bin_PT)
        Jpsi_L0Hadron_eff_err[0] = h_L0.GetBinError(bin_PT)
        # print(Jpsi_L0Hadron_eff, Jpsi_L0Hadron_eff_err)
        # Jpsi_TRUEY[0] = 0.5*Log((nt.Jpsi_TRUEP_E + nt.Jpsi_TRUEP_Z)/(nt.Jpsi_TRUEP_E-nt.Jpsi_TRUEP_Z))
        tree.Fill()

    tree.Write()
    f.Close()
    print(kRange, kSource)

def get_binning(var, nbins, kSource="prompt", kRange="etac"):
    ''' Get the "optimal" binning for a given variable'''
    #f = TFile("{}/MC/Trigger/{}DiProton_{}_2018_AddBr.root".format(dataDir, kRange, kSource))
    #t = f.Get("DecayTree")
    nt = TChain("DecayTree")
    nt.Add("{}/MC/Trigger/{}DiProton_{}_2018_AddBr.root".format(dataDir, kRange, kSource))

    nEntries = nt.GetEntries()
    print("Tree is created.......................................... \n")

    nbins_scan = 2600
    h = TH1D("h","h", nbins_scan, 0., 260000.)

    nt.Draw("{}>>h".format(var))

    #bins = []
    bins = [h.GetBinLowEdge(1)]
    print(bins, nEntries/nbins, h.GetNbinsX())
    ibin_scan = 1
    input()
    for ibin in range(nbins):
        x = 0.
        while (x < float(nEntries/nbins) and ibin_scan<nbins_scan):
            x+= h.GetBinContent(ibin_scan)
            ibin_scan += 1
            #print(ibin_scan, x)
        bins.append(h.GetBinLowEdge(ibin_scan))

    print(bins)
    del nt

if __name__ == "__main__":

    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)
    kSources = ["prompt","fromB"]
    # kSources = ["prompt"]
    # kRanges  = ["jpsi","Etac"]
    kMags    = ["Up","Down"]
    # kRanges = ["etac","etac2S"]
    kRanges = ["etac"]
    kRanges = ["jpsi","etac","chic0","chic1","chic2","psi2S","etac","etac2S"]
    for kRange in kRanges:
        for kSource in kSources:
            # select(kSource,kRange)    
            # selectGen(kSource,kRange)
            for kMag in kMags:
                # add_branch(kSource,kRange,kMag)
                # selectGen(kSource,kRange,kMag,False)
                select(kSource,kRange,kMag,True)
                # select(kSource,kRange,kMag,False,False)
            # select(kSource,kRange)
            # selectGen(kSource,kRange)

    # files = [10132010, 10132000, 10132080, 10132060, 10132030, 10132040]
    # files = [24102002, 24102011, 24102013, 28102000, 28102001, 28102002, 28102033, 28102043, 28102053]
    # files = [24102402]
    # for kRange in files:
    #     selectAcc(kRange)
    
    #get_binning("ProtonP_PT", 10)
    #get_binning("ProtonP_PX", 10)
    #get_binning("ProtonP_PY", 10)
    #get_binning("ProtonM_PX", 10)
    #get_binning("ProtonM_PY", 10)
    #get_binning("ProtonP_PZ", 10)
    #get_binning("ProtonM_PZ", 10)