from ROOT import *
from ROOT.TMath import Abs
from array import array

dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"
#dataDir = "/eos/user/v/vazhovko/etac2s_ppbar/"

def num_sim(m, h_in):

        binN = h_in.FindBin(m)
        ##cout<<m<<" "<<bin<<" "<<h_in.GetBinContent(bin)<<endl
        #print(m, binN)
        return float(h_in.GetBinContent(binN))

def weightVar(ccbar, ccbar_ref, var, src):

    #gROOT.Reset()
    #ccbar1 = "etac"
    #ccbar2 = "jpsi"

    cut_string = ""
    if ccbar_ref!="data":
        nt_base = TChain("DecayTree")
        nt_base.Add("{}/MC/Trigger/{}DiProton_{}_2018_AddBr.root".format(dataDir, ccbar_ref, src))
        cut_string = "Jpsi_L0HadronDecision_TOS && Jpsi_Hlt1DiProtonDecision_TOS"
    else:
        nt_base = TChain("Jpsi2ppTuple/DecayTree")
        nt_name_base = "{}/Tuple_Data_ccbar2ppbar_Low.root".format(dataDir)
        nt_base.Add(nt_name_base)
        
        if src=="prompt":
            cut_FD  = '(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08'
        else:
            cut_FD  = '(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16'

        cut_string = "Jpsi_L0HadronDecision_TOS && Jpsi_Hlt1DiProtonDecision_TOS && " + cut_FD

    # h_new = TH1D("h{}".format(var),"h{}".format(var), 820, 4000., 45000.)
    h_new = TH1D("h{}".format(var),"h{}".format(var), 60, 0., 300.)
    nt_base.Draw("{0}>>h{0}".format(var),cut_string)
    h_new.Scale(1./float(h_new.Integral()))
    # input()

    nt = TChain("DecayTree")
    nt_name_old = "{}/MC/Trigger/{}DiProton_{}_2018_AddBr.root".format(dataDir, ccbar, src)
    nt.Add(nt_name_old)

        
    # nt.Add("{}/MC/Trigger/{}DiProton_{}_2018_AddBr.root".format(dataDir, ccbar2, kSource))

# 
    # h_old = TH1D("h{}_old".format(var),"h{}_old".format(var), 820, 4000., 45000.)
    h_old = TH1D("h{}_old".format(var),"h{}_old".format(var), 60, 0., 300.)
    nt.Draw("{0}>>h{0}_old".format(var))
    h_old.Scale(1./float(h_old.Integral()))
    # input()

    src_arr = array( "l", [False])
    evt_arr = array( "i", [0])
    # evt_arr = array( "d", [0])
    
    # if src=="prompt": src_name = "Jpsi_prompt"
    # else:             src_name = "Jpsi_sec"

    # nt.SetBranchAddress(src_name,src_arr)
    nt.SetBranchAddress(var,evt_arr)

    nt_name_new = nt_name_old.replace(".root", "_w_{}.root".format(ccbar_ref))
    f = TFile(nt_name_new, "RECREATE")

    #new branches
    weight = array( "d", [0])

    tree = nt.CloneTree(0)
    tree.Branch("{}_w".format(var),weight,"{}_w/D".format(var))

    nEntries = nt.GetEntries()

    for i_en in range(nEntries):

        nt.GetEntry(i_en)
        R_new = num_sim(evt_arr[0], h_new)
        R_old = num_sim(evt_arr[0], h_old)
        print(evt_arr[0],R_old, R_new)
        # if (src_arr[0] and R_old!=0.): 
        if R_old!=0.: 
            weight[0] = R_new/R_old
        else:
            weight[0] = 0.

        tree.Fill()


    #c = TCanvas("Signal_Fit","Signal Fit",1600,1000)
    #gPad.SetLeftMargin(0.15)

    #pad = c.cd(1)
    #tree.Draw("Jpsi_TRUEPV_X")
    #pad.RangeAxis(-100, 0, 100, 1e6)
    #pad.SetLogy()

    #c.SaveAs("%s_prompt.pdf"%(kRange))
    tree.Write()

    f.Close()
    print("reweighted {} {} var {} to {}".format(src, var, ccbar, ccbar_ref))
    del tree, nt

if __name__ == "__main__":

    kSources = ["prompt" ]
    kRanges = ["etac2S"]
    #kRanges = ["chic0","chic1","chic2","etac","etac2S"]
    #kRanges = ["psi2S","chic0","chic1","chic2","etac","jpsi","etac2S"]

    src = "prompt"
    # var = "Jpsi_TRUEPT"
    # var = "Jpsi_PT"
    var = "nSPDHits"
    # ccbar1 = "etac2S"
    # ccbar2 = "psi2S"
    # weightVar(ccbar1, ccbar2, var, src)
    # ccbar1 = "etac"
    ccbar1 = "jpsi"
    ccbar2 = "data"
    weightVar(ccbar1, ccbar2, var, src)

