from ROOT import TH1F, TChain, TCut
from ROOT.RooFit import *
import os


# histosDir = "/users/LHCb/zhovkovska/scripts/Histos/m_scaled/"
histosDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/Histos/m_scaled/"
tuples2015 = "/sps/lhcb/zhovkovska/Pmt2015_scaled/Reduced_Pmt_2015*.root"
tuples2016 = "/sps/lhcb/zhovkovska/Pmt2016_scaled/Reduced_Pmt_2016*.root"
tuples2018 = "/sps/lhcb/zhovkovska/etac2s_ppbar/Data_Low_NoPID/Etac2sDiProton_Low_2018*.root"


cutApriori2015_Dict = {"DiProton":"Jpsi_Hlt1DiProtonDecision_TOS && Jpsi_Hlt2DiProtonDecision_TOS && \
                                  ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
                                  ProtonP_PT>2000 && ProtonM_PT>2000 && \
                                  ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366 && \
                                  ProtonP_PIDp>20 && ProtonM_PIDp>20 && \
                                  (ProtonP_PIDp-ProtonP_PIDK)>15 && (ProtonM_PIDp-ProtonM_PIDK)>15 && \
                                  ProtonP_TRACK_CHI2NDOF<2.5 && ProtonM_TRACK_CHI2NDOF<2.5 && \
                                  Jpsi_ENDVERTEX_CHI2<3.5 && \
                                  Jpsi_Y>2 && Jpsi_Y<4.5 && \
                                  Jpsi_PT>6500 && \
                                  ProtonP_TRACK_GhostProb<0.2 && ProtonP_TRACK_GhostProb<0.2 && \
                                  ProtonP_TRACK_CloneDist<0 && ProtonM_TRACK_CloneDist<0",
                       "Topo":    "Jpsi_L0HadronDecision_TOS && \
                                  (Jpsi_Hlt1TrackMVADecision_TOS || Jpsi_Hlt1TwoTrackMVADecision_TOS) && \
                                  (Jpsi_Hlt2Topo2BodyDecision_TOS || Jpsi_Hlt2Topo3BodyDecision_TOS || Jpsi_Hlt2Topo4BodyDecision_TOS) && \
                                  ProtonP_P>-2e3 && ProtonM_P>-2e3 && \
                                  ProtonP_PT>1000 && ProtonM_PT>1000 && \
                                  ProtonP_PIDp>15 && ProtonM_PIDp>15 && \
                                  (ProtonP_PIDp-ProtonP_PIDK)>10 && (ProtonM_PIDp-ProtonM_PIDK)>10 && \
                                  ProtonP_TRACK_CHI2NDOF<5 && ProtonM_TRACK_CHI2NDOF<5 && \
                                  ProtonP_IPCHI2_OWNPV>9 && ProtonM_IPCHI2_OWNPV>9 && \
                                  Jpsi_ENDVERTEX_CHI2<9 && \
                                  Jpsi_Y>2 && Jpsi_Y<4.5 && \
                                  Jpsi_FDCHI2_OWNPV>25 && \
                                  nSPDHits<600"
                      }

cutApriori2016_Dict = {"DiProton":"Jpsi_Hlt1DiProtonDecision_TOS && Jpsi_Hlt2CcDiHadronDiProtonDecision_TOS && \
                                  ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
                                  ProtonP_PT>2000 && ProtonM_PT>2000 && \
                                  ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366 && \
                                  ProtonP_PIDp>20 && ProtonM_PIDp>20 && \
                                  (ProtonP_PIDp-ProtonP_PIDK)>15 && (ProtonM_PIDp-ProtonM_PIDK)>15 && \
                                  ProtonP_TRACK_CHI2NDOF<2.5 && ProtonM_TRACK_CHI2NDOF<2.5 && \
                                  Jpsi_ENDVERTEX_CHI2<3.5 && \
                                  Jpsi_Y>2 && Jpsi_Y<4.5 && \
                                  Jpsi_PT>6500 && \
                                  ProtonP_TRACK_GhostProb<0.2 && ProtonP_TRACK_GhostProb<0.2 && \
                                  ProtonP_TRACK_CloneDist<0 && ProtonM_TRACK_CloneDist<0",
                       "Topo":    "Jpsi_L0HadronDecision_TOS && \
                                  (Jpsi_Hlt1TrackMVADecision_TOS || Jpsi_Hlt1TwoTrackMVADecision_TOS) && \
                                  (Jpsi_Hlt2Topo2BodyDecision_TOS || Jpsi_Hlt2Topo3BodyDecision_TOS || Jpsi_Hlt2Topo4BodyDecision_TOS) && \
                                  ProtonP_P>-2e3 && ProtonM_P>-2e3 && \
                                  x>1000 && ProtonM_PT>1000 && \
                                  ProtonP_PIDp>15 && ProtonM_PIDp>15 && \
                                  (ProtonP_PIDp-ProtonP_PIDK)>10 && (ProtonM_PIDp-ProtonM_PIDK)>10 && \
                                  ProtonP_TRACK_CHI2NDOF<5 && ProtonM_TRACK_CHI2NDOF<5 && \
                                  ProtonP_IPCHI2_OWNPV>9 && ProtonM_IPCHI2_OWNPV>9 && \
                                  Jpsi_ENDVERTEX_CHI2<9 && \
                                  Jpsi_Y>2 && Jpsi_Y<4.5 && \
                                  Jpsi_FDCHI2_OWNPV>25 && \
                                  nSPDHits<600"
                      }
cutApriori2018_Dict = {"DiProton":"Jpsi_Hlt1DiProtonDecision_TOS && \
                                  ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
                                  ProtonP_PT>2000 && ProtonM_PT>2000 && \
                                  ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366 && \
                                  ProtonP_ProbNNp>0.6 && ProtonM_ProbNNp>0.6 && \
                                  ProtonP_TRACK_CHI2NDOF<2.5 && ProtonM_TRACK_CHI2NDOF<2.5 && \
                                  Jpsi_ENDVERTEX_CHI2<4.0 && \
                                  Jpsi_Y>2 && Jpsi_Y<4.5 && \
                                  Jpsi_PT>5000 && \
                                  ProtonP_TRACK_GhostProb<0.2 && ProtonP_TRACK_GhostProb<0.2 && \
                                  ProtonP_TRACK_CloneDist<0 && ProtonM_TRACK_CloneDist<0",
                        "DiProtonOldPID":"Jpsi_Hlt1DiProtonDecision_TOS && \
                                  ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
                                  ProtonP_PT>2000 && ProtonM_PT>2000 && \
                                  ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366 && \
                                  ProtonP_PIDp>20 && ProtonM_PIDp>20 && \
                                  (ProtonP_PIDp-ProtonP_PIDK)>15 && (ProtonM_PIDp-ProtonM_PIDK)>15 && \
                                  ProtonP_TRACK_CHI2NDOF<2.5 && ProtonM_TRACK_CHI2NDOF<2.5 && \
                                  Jpsi_ENDVERTEX_CHI2<4.0 && \
                                  Jpsi_Y>2 && Jpsi_Y<4.5 && \
                                  Jpsi_PT>5000 && \
                                  ProtonP_TRACK_GhostProb<0.2 && ProtonP_TRACK_GhostProb<0.2 && \
                                  ProtonP_TRACK_CloneDist<0 && ProtonM_TRACK_CloneDist<0",
                       "Topo":    "Jpsi_L0HadronDecision_TOS && \
                                  (Jpsi_Hlt1TrackMVADecision_TOS || Jpsi_Hlt1TwoTrackMVADecision_TOS) && \
                                  (Jpsi_Hlt2Topo2BodyDecision_TOS || Jpsi_Hlt2Topo3BodyDecision_TOS || Jpsi_Hlt2Topo4BodyDecision_TOS) && \
                                  ProtonP_P>-2e3 && ProtonM_P>-2e3 && \
                                  x>1000 && ProtonM_PT>1000 && \
                                  ProtonP_ProbNNp>0.6 && ProtonM_ProbNNp>0.6 && \
                                  ProtonP_TRACK_CHI2NDOF<5 && ProtonM_TRACK_CHI2NDOF<5 && \
                                  ProtonP_IPCHI2_OWNPV>9 && ProtonM_IPCHI2_OWNPV>9 && \
                                  Jpsi_ENDVERTEX_CHI2<9 && \
                                  Jpsi_Y>2 && Jpsi_Y<4.5 && \
                                  Jpsi_FDCHI2_OWNPV>25 && \
                                  nSPDHits<600"
                      }


tzVar = "(3.3)*(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ"
# tzVar = "Jpsi_Tz"

cutsDict = {
    "all"       : "1",
    "all_l0TOS" : "Jpsi_L0HadronDecision_TOS",
    "all_l0TIS" : "Jpsi_L0HadronDecision_TIS",
    "secondary" : "("+tzVar + ">0.08) && (ProtonP_IPCHI2_OWNPV > 16) && (ProtonM_IPCHI2_OWNPV > 16)",
    "secondary_l0TOS" : "("+tzVar+">0.08) && (Jpsi_L0HadronDecision_TOS) && (ProtonP_IPCHI2_OWNPV > 16) && (ProtonM_IPCHI2_OWNPV > 16)",
    "secondary_tight_l0TOS" : "("+tzVar+">2.00) && (Jpsi_L0HadronDecision_TOS) && (ProtonP_IPCHI2_OWNPV > 16) && (ProtonM_IPCHI2_OWNPV > 16)",
    "secondary_middle_l0TOS" : "("+tzVar+">1.00) && (Jpsi_L0HadronDecision_TOS) && (ProtonP_IPCHI2_OWNPV > 16) && (ProtonM_IPCHI2_OWNPV > 16)",
    "secondary_soft_l0TOS" : "("+tzVar+">2.00) && (Jpsi_L0HadronDecision_TOS)",
    # "secondary_l0TIS" : "("+tzVar+">0.08) && (Jpsi_L0HadronDecision_TIS)",
    "prompt"    : tzVar + "<0.08",
    "prompt_l0TOS" : "("+tzVar+"<0.08) && (Jpsi_L0HadronDecision_TOS)",
    "prompt_l0TOS_Inv" : "("+tzVar+">0.08) && (Jpsi_L0HadronDecision_TOS) && (ProtonP_IPCHI2_OWNPV < 16) && (ProtonM_IPCHI2_OWNPV < 16)",
    "secondary_l0TOS_Inv" : "("+tzVar+"<0.08) && (Jpsi_L0HadronDecision_TOS) && (ProtonP_IPCHI2_OWNPV > 16) && (ProtonM_IPCHI2_OWNPV > 16)",
    # "prompt_l0TIS" : "("+tzVar+"<0.08) && (Jpsi_L0HadronDecision_TIS)",
    # "IPCHI2_9"  : "1",
    # "IPCHI2_16" : "1",
    # "FD_25"  : "1",
    # "FD_100" : "1",
    # "secondary_RunI" : "("+tzVar + ">0.08)",
    # "secondary_RunI_l0TOS" : "("+tzVar+">0.08) && (Jpsi_L0HadronDecision_TOS)",
    # "secondary_RunI_l0TIS" : "("+tzVar+">0.08) && (Jpsi_L0HadronDecision_TIS)",
    # "prompt_RunI"    : tzVar + "<0.08",
    # "prompt_RunI_l0TOS" : "("+tzVar+"<0.08) && (Jpsi_L0HadronDecision_TOS)",
    # "prompt_RunI_l0TIS" : "("+tzVar+"<0.08) && (Jpsi_L0HadronDecision_TIS)"
    }


binningDict = {
    "Jpsi_PT":                      [5000, 6500, 8000, 10000, 12000, 14000, 20000],
    # "Jpsi_PT":                      [6500, 8000, 10000, 12000, 14000],
    # "Jpsi_ETA":                     [2.0, 2.4, 2.8, 3.3, 4.5],
    "Jpsi_ETA":                     [2.0, 2.5, 3.0, 3.5, 4.0, 4.5],
    "TMath::Abs(ProtonM_CosTheta)": [0.0, 0.15, 0.3, 0.50, 1.0],
    "nSPDHits":                     [0., 100., 175., 250., 300.],
    #"nSPDHits":                     [0, 100, 150, 200, 250, 300],
}

charmDict = {
    "Jpsi":     0,
    "Etac":     1,
}

tzTotalBinning =  [-10., -0.125, -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10]
# tzTotalBinning = [-10.0, -0.125, -0.025, 0.,        0.200,  2., 4., 10.]

#tzInBinsBinning = [[-1., -0.025, 0., 0.025, 0.1, 1., 4., 10.],
                   #[-1., -0.025, 0.,        0.200,   2., 10.]]

tzInBinsBinning = [[-10.0, -0.125, -0.025, 0.,        0.200,  2., 4., 10.],
                   [-10.0, -0.125, -0.025, 0.,        0.200,  2., 4., 10.]]


tzInBinsBinningAlt = [[-10.0, -0.15, -0.05, 0.,        0.100,   4., 10.],
                      [-10.0, -0.15, -0.05, 0.,        0.100,   4., 10.]]

#tzTotalBinning = [-10., -0.125, -0.025]

minMass = 2850.
maxMass = 3250.
#binWidth = 1.
binWidth = 0.2


bold = "\033[1m"
reset = "\033[0;0m"
def makeHistos(nt, year="2016", trigger="DiProton", inCutKey="secondary", var="Jpsi_PT", iPT=0, iTz=0, charm='Etac'):
    # gROOT.Reset()
    inCut = cutsDict[inCutKey]

    nMassBins = int((maxMass-minMass)/binWidth)
    Jpsi_M = TH1F("Jpsi_M","Jpsi_M" , nMassBins, minMass, maxMass)
    Jpsi_M.SetLineColor(4)
    Jpsi_M.GetXaxis().SetTitle("J/#psi_Mass / MeV ")
    Jpsi_M.GetYaxis().SetTitle("Entries")
    Jpsi_M.SetMinimum(0.0)

    cutMass = f"(Jpsi_m_scaled>{minMass}) && (Jpsi_m_scaled<{maxMass})"
    #cutMass = "(Jpsi_M>" + str(minMass) + ") && (Jpsi_M<" + str(maxMass) + ")"                   #!!!!!!!!!!!!!!!!!!!!!

    cutDoubleCount = "totCandidates<2"

    isEtac = charmDict[charm]

    if year=="2015": cutApriori = cutApriori2015_Dict[trigger]
    if year=="2016": cutApriori = cutApriori2016_Dict[trigger]
    if year=="2018": cutApriori = cutApriori2018_Dict[trigger]

    if iPT!=0:
        binningFile = "binning.txt"
# !!!!!!!!
        if iTz!=0:
            if (iTz >= len(tzInBinsBinning[isEtac])):
                return -1
            print(f"{bold} \t I\'m doing", charm, " Tz binning. Bin No {iTz} {reset}")
            tzLowEdge = tzInBinsBinning[isEtac][iTz-1]
            tzHighEdge = tzInBinsBinning[isEtac][iTz]
            cutTz = f"({tzVar}>{tzLowEdge}) && ({tzVar}<{tzHighEdge})"

            varBinning = binningDict[var]
            nVarBins =  len(varBinning)-1

            print(f"{bold} \t\t Now I\'m doing", var, "binning. Bin No", (iPT), reset)
            varLowEdge = varBinning[iPT-1]
            varHighEdge = varBinning[iPT]
            cutVar = f"({var}>{varLowEdge}) && ({var}<{varHighEdge})"

            sumCut = cutMass    +" && "+ \
                     cutApriori +" && "+ \
                     inCut      +" && "+ \
                     cutTz      +" && "+ \
                     cutVar     
                     # cutVar     +" && "+ \
                     # cutDoubleCount

            dirName = f"{histosDir}/{year}/{trigger}/{inCutKey}/{var}/{charm}"  #+ "/CutedBins" #!!!!!!!!!!!!!!!!!!!!!!!!
            if not os.path.exists(dirName):
                os.makedirs(dirName)
            fileName = f"{dirName}/bin{iPT}_Tz{iTz}.root"
            nt.Draw("Jpsi_m_scaled>>Jpsi_M", sumCut)
            Jpsi_M.SaveAs(fileName)
        else:
            varBinning = binningDict[var]
            nVarBins =  len(varBinning)-1
            print(f"{bold}I DON\'T do Tz binning {reset}")
            for i in range(nVarBins):
                print(bold+"    Now I\'m doing", var, "binning. Bin No", (i+1), ", Tz integrated", reset)
                varLowEdge = varBinning[i]
                varHighEdge = varBinning[i+1]
                cutVar = f"({var}>{varLowEdge}) && ({var}<{varHighEdge})"

                sumCut = cutMass    +" && "+ \
                         cutApriori +" && "+ \
                         inCut      +" && "+ \
                         cutVar

                dirName = f"{histosDir}/{year}/{trigger}/{inCutKey}/{var}"
                if not os.path.exists(dirName):
                    os.makedirs(dirName)
                fileName = f"{dirName}/bin{(i+1)}_Tz0.root"
                nt.Draw("Jpsi_m_scaled>>Jpsi_M", sumCut)
                Jpsi_M.SaveAs(fileName)
    else:
        print(f"{bold}I\'m doing Total histos {reset}")
        if iTz!=0:    
            if (iTz >= len(tzTotalBinning)):
                return -2    
            print(f"{bold}\t I\'m doing Tz binning for Total histos. Bin No {iTz} {reset}")
            tzLowEdge = tzTotalBinning[iTz-1]
            tzHighEdge = tzTotalBinning[iTz]
            cutTz = f"({tzVar}>{tzLowEdge}) && ({tzVar}<{tzHighEdge})"
            # cutPTHigh = "(Jpsi_PT < 20000)"
            cutPTHigh = f"(Jpsi_PT > {binningDict['Jpsi_PT'][0]} && Jpsi_PT < {binningDict['Jpsi_PT'][-1]})"
            
            sumCut = cutMass    +" && "+ \
                     cutApriori +" && "+ \
                     inCut      +" && "+ \
                     cutPTHigh  +" && "+ \
                     cutTz

            dirName = f"{histosDir}/{year}/{trigger}/{inCutKey}/Total"
            if not os.path.exists(dirName):
                os.makedirs(dirName)
            # fileName = f"{dirName}/Tz{iTz}_7Bins.root"  #!!!!!!!!!!!!!!!!!!!!!
            fileName = f"{dirName}/Tz{iTz}.root"
            nt.Draw("Jpsi_m_scaled>>Jpsi_M", sumCut)
            #nt.Draw("Jpsi_M>>Jpsi_M", sumCut)           #!!!!!!!!!!!!!!!!!!!!!
            Jpsi_M.SaveAs(fileName)
        else:
            print(f"{bold} \t I DON\'T do Tz binning for Total histos {reset}")
            sumCut = cutMass    +" && "+ \
                     cutApriori +" && "+ \
                     inCut

            cut = TCut(sumCut)

            dirName = f"{histosDir}/{year}/{trigger}/{inCutKey}/Total"
            if not os.path.exists(dirName):
                os.makedirs(dirName)
            fileName = f"{dirName}/Tz0.root"   
            nt.Draw("Jpsi_m_scaled>>Jpsi_M", cut)
            Jpsi_M.SaveAs(fileName)
    del Jpsi_M
    return 0



from multiprocessing import Pool
from functools import partial
from contextlib import closing

def makeAllHistos():

    inCutKey= "all_l0TOS"
    #inCutKey= "secondary_l0TOS"
    var     = "Jpsi_PT"
    year    = "2016"
    trigger = "DiProton"


    years = ["2016"]
    #variables = ["Jpsi_PT","Jpsi_ETA","TMath::Abs(ProtonM_CosTheta)","nSPDHits"]
    variables = ["Jpsi_PT"]
    cutKeys = ["all_l0TOS", "secondary_l0TOS"]
    
    #nt = TChain("DecayTree")
    #if (year=="2016"): 
        #nt.Add(tuples2016)
    #if (year=="2015"): 
        #nt.Add(tuples2015)
    #makeHistos(nt,year,trigger,inCutKey,var,False,False,'Etac')
    #makeHistos(nt,year,trigger,inCutKey,var,True,True,'Etac')
            
    for var in variables:
        for year in years:
            #for inCutKey in cutKeys:
                #print "Setup Cuts: ", bold+inCutKey, reset
                #print "      year: ", bold+year, reset,"\n"

                nt = TChain("DecayTree")
                if (year=="2016"): 
                    nt.Add(tuples2016)
                if (year=="2015"): 
                    nt.Add(tuples2015)
                if (year=="2018"): 
                    nt.Add(tuples2018)

                #makeHistos(nt,year,trigger,inCutKey,var,False,False)
                #makeHistos(nt,year,trigger,inCutKey,var,True,False)
                makeHistos(nt,year,trigger,inCutKey,var,True,True)
                #makeHistos(nt,year,trigger,inCutKey,var,False,True)
                #makeHistos(nt,year,trigger,inCutKey,var,True,True,'Jpsi')




def multHistos(list):
    year = list[0]
    #trigger = list[1]
    # trigger = "DiProton"
    trigger = "DiProtonOldPID"
    #trigger = "Topo"
    inCutKey = list[1]
    var = list[2]
    isPT = list[3]
    isTz = list[4]
    charm = list[5]
    nt = TChain("DecayTree")
    if (year=="2016"): 
        nt.Add(tuples2016)
    if (year=="2015"): 
        nt.Add(tuples2015)
    if (year=="2018"): 
        nt.Add(tuples2018)

    return  makeHistos(nt,year,trigger,inCutKey,var,isPT,isTz,charm)


def proc():

    # years = ["2015","2016"]
    years = ["2018"]
    #variables = ["Jpsi_PT","Jpsi_ETA","TMath::Abs(ProtonM_CosTheta)","nSPDHits"]
    #variables = ["nSPDHits"]
    variables = ["Jpsi_PT"]
    # cutKeys = ["secondary_l0TOS", "prompt_l0TOS"]
    cutKeys = [ "all_l0TOS"]
    # cutKeys = [ "prompt_l0TOS_Inv"]
    #doPT = [True,False]
    # doPT = [i for i in range(1,7)]
    doPT = [0]
    doTz = [i for i in range(1,12)]
    # doTz = [i for i in range(1,8)]
    # doTz = [0]

    a = [[year, inCutKey, var, isDoPT, isDoTz, "Jpsi"] for year in years for inCutKey in cutKeys for var in variables for isDoPT in doPT for isDoTz in doTz]
#print(a);

    if __name__ == '__main__':
        with closing(Pool(100)) as p:
            (p.map(multHistos, a))
            p.terminate()


#makeAllHistos()
proc()