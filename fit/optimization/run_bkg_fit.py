from ROOT import *
from ROOT.RooFit import *
from ROOT.RooStats import *
gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")

import ROOT.RooFit as RF
import numpy as np
#from math import abs

import sys
sys.path.insert(1, "../mass_fit/")
from setup_model import fillWksp_Low, fillWksp_High #imported from an old module which was modified for this purpose

sys.path.insert(1, "../libs/")
from charmonium import Particle

binWidth = 0.25
binWidthDraw = 5.0

minM_Low = 2850
maxM_Low = 3250
nBins_Low = int((maxM_Low-minM_Low)/binWidth)
binN_Low  = int((maxM_Low-minM_Low)/binWidthDraw)

minM_High = 3320
maxM_High = 3780
nBins_High = int((maxM_High-minM_High)/binWidth)
binN_High  = int((maxM_High-minM_High)/binWidthDraw)

binN_Tot     = int((maxM_High-minM_Low)/binWidthDraw)
binN_Tot_Fit = int((maxM_High-minM_Low)/binWidth)

binning      = RF.Binning(binN_Tot, minM_Low, maxM_High)
binning_low  = RF.Binning(binN_Low, minM_Low, maxM_Low)
binning_high = RF.Binning(binN_High, minM_High, maxM_High)

range_tot  = RF.Range(minM_Low,maxM_High)
range_low  = RF.Range(minM_Low,maxM_Low)
range_high = RF.Range(minM_High,maxM_High)

mrkSize = RooFit.MarkerStyle(7)
lineWidth1 = RooFit.LineWidth(1)
lineWidth2 = RooFit.LineWidth(2)
lineStyle1 = RooFit.LineStyle(2)
lineStyle2 = RooFit.LineStyle(9)
lineColor1 = RooFit.LineColor(6)
lineColor2 = RooFit.LineColor(8)
name = RooFit.Name
cut = RooFit.Cut

var_list = ['Jpsi_M', 'Jpsi_MM','Jpsi_ENDVERTEX_Z','Jpsi_OWNPV_Z','Jpsi_PT',
               'Jpsi_PE','Jpsi_PZ',
               #'Jpsi_ENDVERTEX_CHI2','Jpsi_FDCHI2_OWNPV',
               'ProtonP_PT','ProtonM_PT',
               'ProtonP_ProbNNp','ProtonM_ProbNNp',
               #'ProtonP_TRACK_CHI2NDOF','ProtonM_TRACK_CHI2NDOF',
               #'ProtonP_PIDK', 'ProtonP_PIDp', 'ProtonM_PIDK', 'ProtonM_PIDp',
               #'ProtonP_IPCHI2_OWNPV', 'ProtonM_IPCHI2_OWNPV',
               #'ProtonP_TRACK_GhostProb', 'ProtonM_TRACK_GhostProb', #'ProtonP_TRACK_CloneDist', 'ProtonM_TRACK_CloneDist',

]

# homeDir = '/afs/cern.ch/user/v/vazhovko/private/results/etac2s_ppbar/'
dataDir = '/eos/user/v/vazhovko/etac2s_ppbar/'
histosDir = "/afs/cern.ch/user/v/vazhovko/private/results/etac2s_ppbar/Histos/m_scaled/"
dataDir = '/sps/lhcb/zhovkovska/etac2s_ppbar/'
homeDir = '/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/'

def get_tuple(kSource):

    dirName_Low  = dataDir +"Data_Low_Loose_Prompt/"

    nt  =  TChain("DecayTree")
    nt.Add(dirName_Low+'/Etac2sDiProton_Low*.root')
    nt.Add(dirName_Low+'/Etac2sDiProton_Low_2018_1*.root')
    #nt.Add(dirName_Low+'/Etac2sDiProton_Low_2018_2*.root')
    #nt.Add(dirName_Low+'/Etac2sDiProton_Low_2018_3*.root')
    #nt.Add(dirName_Low+'/Etac2sDiProton_Low_2018_4*.root')
    #nt.Add(dirName_Low+'/Etac2sDiProton_Low_2018_5*.root')
    print('OK')

    nt.SetBranchStatus("*",0)
    for var in var_list:
        nt.SetBranchStatus("{}".format(var),1);

    #cut_Jpsi_Y = TCut('0.5*log((Jpsi_PE + Jpsi_PZ)/(Jpsi_PE-Jpsi_PZ)) > 2 & 0.5*log((Jpsi_PE + Jpsi_PZ)/(Jpsi_PE-Jpsi_PZ)) < 4.5')
    cut_FD = ''
    if kSource=='prompt':
        cut_FD = TCut('(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08')# & Jpsi_FDCHI2_OWNPV > 49'
    else:
        cut_FD = TCut('(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16')


    #totCut = TCut(cut_Jpsi_Y + cut_Ghost + cut_FD)
    totCut = TCut(cut_FD)

    tree = TTree()
    tree = nt.CopyTree(totCut.GetTitle())


    return tree

def get_data_h(w, vars_list, idxs, kSource="prompt", kRange="Low"):

    vars_name = vars_list[0]
    for var_name in vars_list[1:]:
        vars_name+= '_' + var_name

    idx = str(idxs[0]+1)
    for i in idxs[1:]:
        idx+= '_{}'.format(i+1)
    print(idx)

    newfile = TFile("{}/Data_{}/{}/Etac2sDiProton_{}_2018_Bin_{}_PT1.root".format(dataDir, vars_name, kSource, kRange, idx),"read")
    if kRange=="Low":
        binN, minM, maxM = binN_Low, minM_Low, maxM_Low
    else:
        binN, minM, maxM = binN_High, minM_High, maxM_High

    hh = TH1D('hh_{}'.format(kRange),'hh_{}'.format(kRange), binN, minM, maxM)
    hh = newfile.Get("Jpsi_M")

    Jpsi_M = RooRealVar('Jpsi_M',"Jpsi_M", minM, maxM)

    dh = RooDataHist("dh_{}".format(kRange),"dh_{}".format(kRange), RooArgList(Jpsi_M), hh)
    getattr(w,'import')(dh)

    hh.Print()
    print("RooDataHist was imported")


def get_MC_h(w, cut_opt, ccbar="jpsi", kSource="prompt"):


    newfile = TFile("{}/MC/Trigger/{}DiProton_{}_2018_AddBr.root".format(dataDir, ccbar, kSource),"read")
    tree_MC = newfile.Get("DecayTree")

    cut = TCut(cut_opt)

    cut_PT = TCut("Jpsi_PT>5000 && Jpsi_PT<6500")
    cut = TCut(cut+cut_PT)

    if ccbar in ["jpsi","etac"]:
        binN, minM, maxM = binN_Low, minM_Low, maxM_Low
    else:
        binN, minM, maxM = binN_High, minM_High, maxM_High

    hh_MC = TH1D('hh_MC','hh_MC', binN, minM, maxM)

    #tree.Draw('Jpsi_M>>hh_Low',cut_opt,'goff')
    tree_MC.Draw('Jpsi_M>>hh_MC',cut.GetTitle(),'goff')

    # Jpsi_M_MC = RooRealVar('Jpsi_M',"Jpsi_M", -100., 100.)
    Jpsi_M = w.var("Jpsi_M")

    dh_MC = RooDataHist("dh_MC","dh_MC", RooArgList(Jpsi_M), hh_MC)
    getattr(w,'import')(dh_MC)

    hh_MC.Print()
    print("MC was imported")




def choose_bkg(optList, regName, argList):
    return {

        'prompt cheb' : RooChebychev ("bkg_{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2),argList.at(3))),
        'fromB cheb'  : RooChebychev ("bkg_{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2))),
        'prompt exp'  : RooGenericPdf("bkg_{}".format(regName),"background","TMath::Exp(-(@0-3550.)/200.*@1)*(1.+@2*(@0-3550.)/200.+@3*(@0-3550.)*(@0-3550.)/200./200.)", RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3))) ,
        'fromB exp'   : RooGenericPdf("bkg_{}".format(regName),"background","TMath::Exp(-(@0-3550.)/200.*@1)*(1.+@2*(@0-3550.)/200)", RooArgList(argList.at(0), argList.at(1),argList.at(2)))

    }[optList[0] + ' ' + optList[1]]


def fill_workspace(w, name, ref="etac", cb=False):

    Jpsi_M = w.var("Jpsi_M")
    Jpsi_M.setBins(2000,"cache")

    ccbar = Particle(name)
    proton = Particle("p")

    bw = True
    if ccbar.width < 1e-3: bw=False 

    ratio_sigma = 0.30
    ratio_ref = 1.1 #ratio sigma_ccbar 2 sigma_ref
    ratio_area = 0.90
    m_win = 50.

    #ratioNtoW,ratioEtaTo_etac2s,ratioArea, gamma_etac, eff = getConstPars()

    r_ref = RooRealVar("r_ref_{}".format(name),"r_ref", ratio_ref, 0., 3.)
    #r_NToW = RooRealVar("r_NToW_{}".format(name),"rNarToW",ratio_sigma, 0., 1.)
    #r_G1ToG2 = RooRealVar("r_G1ToG2_{}".format(name),"rG1toG2",ratio_area, 0., 1.)
    n_ccbar = RooRealVar("n_{}_MC".format(ccbar.name),"num of etac", 1e3, 10., 5.e5)

    if ref==ccbar.name:
        r_NToW = RooRealVar("r_NToW","rNarToW",ratio_sigma, 0., 1.)
        r_G1ToG2 = RooRealVar("r_G1ToG2","rG1toG2",ratio_area, 0., 1.)
        sigma_1 = RooRealVar("sigma_{}_1".format(ccbar.name),"width of gaussian", 9., 0.1, 30.)
    else:
        r_NToW = w.var("r_NToW")
        r_G1ToG2 = w.var("r_G1ToG2")
        sigma_ref = w.var("sigma_{}_1".format(ref))
        sigma_1 = RooFormulaVar("sigma_{}_1".format(ccbar.name),"width of gaussian","@0*@1",RooArgList(RooArgSet(sigma_ref,r_ref)))

    sigma_2 = RooFormulaVar("sigma_{}_2".format(ccbar.name),"width of gaussian","@0/@1",RooArgList(sigma_1,r_NToW))

    n_ccbar_1 = RooFormulaVar("n_{}_1_MC".format(ccbar.name),"num of ","@0*@1",RooArgList(n_ccbar,r_G1ToG2))
    n_ccbar_2 = RooFormulaVar("n_{}_2_MC".format(ccbar.name),"num of ","@0-@1",RooArgList(n_ccbar,n_ccbar_1))

    mass  = RooRealVar("mass_{}".format(ccbar.name),"mean of gaussian", ccbar.mass, ccbar.mass-m_win, ccbar.mass+m_win)
    #mass.setConstant(True)
    gamma = RooRealVar("gamma_{}".format(ccbar.name),"width of Br-W", ccbar.width, 10., 50. )
    gamma.setConstant(True)

    spin = RooRealVar("spin_{}".format(ccbar.name),"spin", ccbar.spin )
    radius = RooRealVar("radius","radius", 1.)
    proton_m = RooRealVar("proton_m","proton mass", proton.mass )


    #Fit etac

    if bw: mean = RooFit.RooConst(0)
    else: mean = mass

    brw = RooRelBreitWigner("brw_{}_MC".format(ccbar.name), "brw",Jpsi_M, mass, gamma, spin,radius,proton_m,proton_m)

    if cb:
        if ref==ccbar.name:
            alpha_1 = RooRealVar("alpha_1","alpha of CB", 1., 0.0, 10.)
            alpha_2 = RooRealVar("alpha_2","alpha of CB", 1., 0.0, 10.)
            nCB_1 = RooRealVar("nCB_1","n of CB", 1., 0.0, 100.)
            nCB_2 = RooRealVar("nCB_2","n of CB", 1., 0.0, 100.)
        else:
            alpha_1 = w.var("alpha_1")
            alpha_2 = w.var("alpha_2")
            nCB_1 = w.var("nCB_1")
            nCB_2 = w.var("nCB_2")
        # alpha_1 = RooRealVar("alpha_{}_1".format(ccbar.name),"alpha of CB", 1., 0.0, 10.)
        # alpha_2 = RooRealVar("alpha_{}_2".format(ccbar.name),"alpha of CB", 1., 0.0, 10.)
        # nCB_1 = RooRealVar("nCB_{}_1".format(ccbar.name),"n of CB", 1., 0.0, 100.)
        # nCB_2 = RooRealVar("nCB_{}_2".format(ccbar.name),"n of CB", 1., 0.0, 100.)
        resolution_1 = BifurcatedCB("cb_{}_1".format(ccbar.name), "Cystal Ball Function", Jpsi_M, mean, sigma_1, alpha_1, nCB_1, alpha_1, nCB_1)
    else:
        resolution_1 = RooGaussian("gauss_{}_1_MC".format(ccbar.name),"gauss_1 PDF", Jpsi_M, mean, sigma_1) #mass -> mean2s
        resolution_2 = RooGaussian("gauss_{}_2_MC".format(ccbar.name),"gauss_2 PDF", Jpsi_M, mean, sigma_2) #mass -> mean2s


    # a0 = RooRealVar("a0","a0",0.4,-1.,1.)
    # a1 = RooRealVar("a1","a1",0.05,-1.,1.)
    # a2 = RooRealVar("a2","a2",-0.005,-1.,1.)
    # bkg = RooChebychev ("bkg","Background",Jpsi_M,RooArgList(a0,a1))

    # n_bkg = RooRealVar("n_bkg","num of etac", 1e3, 10, 5.e5)


    model = RooAddPdf()
    if bw:
        pdf_1 = RooFFTConvPdf("bwxg_{}_1_MC".format(ccbar.name),"breit-wigner (X) gauss", Jpsi_M, brw, resolution_1)
        pdf_2 = RooFFTConvPdf("bwxg_{}_2_MC".format(ccbar.name),"breit-wigner (X) gauss", Jpsi_M, brw, resolution_2)
    else:
        pdf_1 = resolution_1
        pdf_2 = resolution_2
        #pdf = RooGaussian("gauss_{}_1".format(ccbar.name),"gauss_1 PDF", Jpsi_M, mass, sigma_1) #mass -> mean2s

    #model = RooAddPdf("model_{}".format(ccbar.name),"{} signal".format(ccbar.name), RooArgList(pdf_1), RooArgList(n_ccbar))
    model = RooAddPdf("model_{}_MC".format(ccbar.name),"{} signal".format(ccbar.name), RooArgList(pdf_1,pdf_2), RooArgList(n_ccbar_1,n_ccbar_2))
    getattr(w,"import")(model,RooFit.RecycleConflictNodes())

    return model, n_ccbar



def perform_fit(w, ccbar, kSource, postfix, pad_gen=None):

    #gStyle.SetOptTitle(0)
    # if kRange=="Low":
    #     states = ["jpsi", "etac"]
    kRange = "High"   # !!! this if doesn't work 

    if ccbar in ["jpsi", "etac"]:
        kRange="Low"
        binning_data = binning_low
        kNorm = "Jpsi"
        fillWksp_Low(w, kSource)
    else:
        kRange="High"
        states = ["chic0","chic1","chic2","psi2S"]
        binning_data = binning_high
        kNorm = "psi2S"
        fillWksp_High(w, kSource, kNorm=kNorm)

    fill_workspace(w, ccbar, ccbar)
    # fillRelWorkspace(w, kSource, kBkg)

    Jpsi_M = w.var("Jpsi_M")

    model = w.pdf('model_{}'.format(kRange))
    modelSignal = w.pdf('modelSignal_{}'.format(kRange))
    modelBkg = w.pdf('modelBkg_{}'.format(kRange))
    model_MC = w.pdf('model_{}_MC'.format(ccbar))

    data = w.data('dh_{}'.format(kRange))
    data_MC = w.data('dh_MC')

    #if kSource=='prompt':
        #f = TFile(homeDir+"/opt_fit/FitMass_wksp_{}_fromB_{}.root".format(kNorm, kBkg),"READ")
        #w_fromB = f.Get("w")
        #f.Close()
        #sigma = w_fromB.var('sigma_Jpsi').getValV()
        #w.var('sigma_Jpsi').setVal(sigma)


    if kNorm=="psi2S":
        w.var('sigma_{}'.format(kNorm)).setVal(8.3958/0.754)
    else:
        w.var('sigma_{}'.format(kNorm)).setVal(8.3958)
    w.var('sigma_{}'.format(kNorm)).setConstant(True)

    modelBkg.fitTo(data, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.NumCPU(48))
    model.fitTo(data, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.NumCPU(48))
    # w.var('n_Jpsi').setVal(w.var('n_Jpsi').getValV())
    r = model.fitTo(data, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.NumCPU(48))
    #r = model.fitTo(data, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.NumCPU(48))
    #r = model.fitTo(data, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.NumCPU(48))

    model_MC.fitTo(data_MC, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.NumCPU(48))
    r_MC = model_MC.fitTo(data_MC, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.NumCPU(48))

    ##draw low and high separately

    frame1 = Jpsi_M.frame(RooFit.Title('J/#psi to p #bar{p}'))

    data.plotOn(frame1, binning, mrkSize, lineWidth1, name('data'))

    model.plotOn(frame1, lineWidth1)
    modelBkg.plotOn(frame1, lineWidth1, lineColor1, lineStyle1, name('bkg'), RooFit.Normalization(1.0, RooAbsReal.RelativeExpected))


    h_resid = frame1.residHist("data","bkg")
    frame_res1 = Jpsi_M.frame(RooFit.Bins(100))
    frame_res1.addPlotable(h_resid,"P")
    modelSignal.plotOn(frame_res1, lineWidth1, lineColor2, name('sig'))#, RooFit.Normalization(8.83177e+07,RooAbsReal.NumEvent))

    if pad_gen==None:
        pad_gen = TCanvas('c','c', 3600, 2000)
    #c = TCanvas('c','c', 3600, 2000)
    #c.Divide(1,2)
    #pad = c.cd(1)

    pad_gen.Divide(2,2)
    pad = pad_gen.cd(1)
    #xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
    #yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
    #pad.SetPad(xl,yl-0.2,xh,yh)
    #pad.SetBottomMargin(0.15)
    #pad.SetLeftMargin(0.15)
    frame1.GetYaxis().SetTitleOffset(1.5)
    frame1.GetXaxis().SetTitleOffset(0.75)
    frame1.GetXaxis().SetTitle("M(pp) / [MeV/c^{2}]")
    frame1.GetXaxis().SetTitleSize(0.045)
    frame1.GetXaxis().SetTitleFont(12)
    frame1.Draw()

    #pad = c.cd(2)
    pad = pad_gen.cd(2)
    #xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
    #yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
    #pad.SetPad(xl,yl,xh,yh-0.2)
    #pad.SetBottomMargin(0.15)
    #pad.SetLeftMargin(0.15)
    frame_res1.GetXaxis().SetTitle("M(pp) / [MeV/c^{2}]")
    frame_res1.GetXaxis().SetTitleFont(12)
    frame_res1.Draw()


    frame2 = Jpsi_M.frame(RooFit.Title('J/#psi to p #bar{p}'))

    data_MC.plotOn(frame2, binning, mrkSize, lineWidth1, name('data'))

    model_MC.plotOn(frame2, lineWidth1, name('sig'))


    h_resid2 = frame2.residHist("data","sig")
    frame_res2 = Jpsi_M.frame(RooFit.Bins(100))
    frame_res2.addPlotable(h_resid2,"P")
    model_MC.plotOn(frame_res2, lineWidth1, lineColor2, name('sig'), RooFit.Normalization(0,RooAbsReal.RelativeExpected))

    #c = TCanvas('c','c', 3600, 2000)
    #c.Divide(1,2)
    #pad = c.cd(1)

    pad = pad_gen.cd(3)
    #xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
    #yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
    #pad.SetPad(xl,yl-0.2,xh,yh)
    #pad.SetBottomMargin(0.15)
    #pad.SetLeftMargin(0.15)
    frame2.GetYaxis().SetTitleOffset(1.5)
    frame2.GetXaxis().SetTitleOffset(0.75)
    frame2.GetXaxis().SetTitle("M(pp) / [MeV/c^{2}]")
    frame2.GetXaxis().SetTitleSize(0.045)
    frame2.GetXaxis().SetTitleFont(12)
    frame2.Draw()

    #pad = c.cd(2)
    pad = pad_gen.cd(4)
    #xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
    #yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
    #pad.SetPad(xl,yl,xh,yh-0.2)
    #pad.SetBottomMargin(0.15)
    #pad.SetLeftMargin(0.15)
    frame_res2.GetXaxis().SetTitle("M(pp) / [MeV/c^{2}]")
    frame_res2.GetXaxis().SetTitleFont(12)
    frame_res2.Draw()

    # homeDir = '/afs/cern.ch/user/v/vazhovko/private/results/etac2s_ppbar/opt_fit/'

    namePic = homeDir + "/mass_fit/ProbNN_check/Plot_{}_{}_{}_{}_PT1.pdf".format(ccbar,kSource,kBkg,postfix)
    nameTxt = homeDir + "/mass_fit/ProbNN_check/Result_{}_{}_{}_{}_PT1.txt".format(ccbar,kSource,kBkg,postfix)
    nameWksp = homeDir + "/mass_fit/ProbNN_check/Wksp_{}_{}_{}_{}_PT1.root".format(ccbar,kSource,kBkg,postfix)


    pad_gen.SaveAs(namePic)
    w.writeToFile(nameWksp)

    import os, sys
    save = os.dup( sys.stdout.fileno() )
    newout = open(nameTxt, 'a' )
    os.dup2( newout.fileno(), sys.stdout.fileno() )
    r.Print("v")
    r.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() )
    newout.close()


    print('fit performed successfully')


def get_scale():

    w = RooWorkspace("w",True)

    newfile = TFile(histosDir+"/2018/DiProton_Loose/prompt_l0TOS/Low/Total/Tz0.root","read")
    hh_data_Low = TH1D('hh_data_Low','hh_data_Low', 1600, minM_Low, maxM_Low)
    hh_data_Low = newfile.Get("Jpsi_M")

    Jpsi_M = RooRealVar('Jpsi_M',"Jpsi_M", minM_Low, maxM_Low)

    dh_Low = RooDataHist("dh_Low","dh_Low", RooArgList(Jpsi_M), hh_data_Low)
    getattr(w,'import')(dh_Low)

    get_MC_h(w, "1")

    c = TCanvas('c','c', 600, 400)
    perform_fit(w, kSource, kBkg, c)
    c.Draw()
    c.SaveAs(homeDir + "/FitMass_prompt_no_cuts_result.pdf")


    n_etac_data = w.var("n_etac").getValV()
    n_etac_MC = w.var("n_etac_MC").getValV()
    sigma_MC = w.var("sigma_etac_MC")
    #n_etac_MC = w.data("dh_Low_MC").sumEntries()

    return n_etac_data/n_etac_MC, sigma_MC


def one_dim_opt(var1,ccbar,kSource):

    kRange = "Low" if ccbar in ["jpsi","etac"] else "High"

    len_x1 = cuts[var1][1].size

    c = TCanvas('c','c', 7200, 4000)
    c.Divide(len_x1)

    # w = RooWorkspace("w",True)

    signif_Jpsi = np.zeros((len_x1))
    signif_etac = np.zeros((len_x1))
    sigmas = np.zeros((len_x1))

    for cut1 in cuts[var1][1]:

        idx1 = list(cuts[var1][1]).index(cut1)

        # pad = c.cd(idx1+len_x1*idx2+1)

        cut_string = ''

        for var_name1 in link_list[var1]:
            if cut_string == '':
                cut_string += '{} {} {:.2f}'.format(var_name1, cuts[var1][0], cut1)
            else:
                cut_string += ' && {} {} {:.2f}'.format(var_name1, cuts[var1][0], cut1)
            
        print(cut_string)

        postfix = "{}".format(idx1+1)
        print(postfix)

        w = RooWorkspace("w",True)
        get_data_h(w, [var1], [idx1], kSource, kRange)
        get_MC_h(w, cut_string, ccbar, kSource)
        # perform_fit(w, ccbar, kSource, postfix, pad)
        perform_fit(w, ccbar, kSource, postfix)

        # signif_etac[idx1][idx2] = w.var('n_etac').getValV() / w.var('n_etac').getError()
        # signif_Jpsi[idx1][idx2] = w.var('n_Jpsi').getValV() / w.var('n_Jpsi').getError()
        # sigmas[idx1][idx2] = w.var('sigma_Jpsi').getValV()
        #masses[idx1][idx2] = w.var('mass_etac').getValV()


    import os
    try:
        os.system('mkdir {}/mass_fit/ProbNN_check/{}'.format(homeDir, var1))
    except:
        pass

    try:
        os.system('mv {}/mass_fit/ProbNN_check/Wksp*   {}/mass_fit/ProbNN_check/{}/.'.format(homeDir, homeDir, var1))
        os.system('mv {}/mass_fit/ProbNN_check/Plot*   {}/mass_fit/ProbNN_check/{}/.'.format(homeDir, homeDir, var1))
        os.system('mv {}/mass_fit/ProbNN_check/Result* {}/mass_fit/ProbNN_check/{}/.'.format(homeDir, homeDir, var1))
    except:
        pass

    # c.Draw()
    # c.SaveAs(homeDir + '/mass_fit/ProbNN_check/{0}_{1}/Fit.pdf'.format(var1,var2))
    # return signif_Jpsi, signif_etac


def two_dim_opt(var1,var2,ccbar,kSource):

    kRange = "Low" if ccbar in ["jpsi","etac"] else "High"

    len_x1 = cuts[var1][1].size
    len_x2 = cuts[var2][1].size

    c = TCanvas('c','c', 7200, 4000)
    c.Divide(len_x1, len_x2)

    # w = RooWorkspace("w",True)

    signif_Jpsi = np.zeros((len_x1,len_x2))
    signif_etac = np.zeros((len_x1,len_x2))
    sigmas = np.zeros((len_x1,len_x2))

    for cut1 in cuts[var1][1]:
        for cut2 in cuts[var2][1]:

            idx1 = list(cuts[var1][1]).index(cut1)
            idx2 = list(cuts[var2][1]).index(cut2)

            # pad = c.cd(idx1+len_x1*idx2+1)

            cut_string = ''

            for var_name1 in link_list[var1]:
                if cut_string == '':
                    cut_string += '{} {} {:.2f}'.format(var_name1, cuts[var1][0], cut1)
                else:
                    cut_string += ' && {} {} {:.2f}'.format(var_name1, cuts[var1][0], cut1)
                for var_name2 in link_list[var2]:
                    cut_string += ' && {} {} {:.2f}'.format(var_name2, cuts[var2][0], cut2)
            print(cut_string)

            postfix = "{}_{}".format(idx1+1, idx2+1)
            print(postfix)

            w = RooWorkspace("w",True)
            get_data_h(w, [var1, var2], [idx1, idx2], kSource, kRange)
            get_MC_h(w, cut_string, ccbar, kSource)
            # perform_fit(w, ccbar, kSource, postfix, pad)
            perform_fit(w, ccbar, kSource, postfix)

            # signif_etac[idx1][idx2] = w.var('n_etac').getValV() / w.var('n_etac').getError()
            # signif_Jpsi[idx1][idx2] = w.var('n_Jpsi').getValV() / w.var('n_Jpsi').getError()
            # sigmas[idx1][idx2] = w.var('sigma_Jpsi').getValV()
            #masses[idx1][idx2] = w.var('mass_etac').getValV()


    import os
    try:
        os.system('mkdir {}/mass_fit/ProbNN_check/{}_{}'.format(homeDir, var1, var2))
    except:
        pass

    try:
        os.system('mv {}/mass_fit/ProbNN_check/Wksp*   {}/mass_fit/ProbNN_check/{}_{}/.'.format(homeDir, homeDir, var1, var2))
        os.system('mv {}/mass_fit/ProbNN_check/Plot*   {}/mass_fit/ProbNN_check/{}_{}/.'.format(homeDir, homeDir, var1, var2))
        os.system('mv {}/mass_fit/ProbNN_check/Result* {}/mass_fit/ProbNN_check/{}_{}/.'.format(homeDir, homeDir, var1, var2))
    except:
        pass

    # c.Draw()
    # c.SaveAs(homeDir + '/mass_fit/ProbNN_check/{0}_{1}/Fit.pdf'.format(var1,var2))
    # return signif_Jpsi, signif_etac


def opt_1dim_file(var):


    homeDir = '/afs/cern.ch/user/v/vazhovko/private/results/etac2s_ppbar/opt_fit/'
    print(var, ' optimization begins')
    len_x = cuts[var][1].size
    signif_Jpsi = np.zeros(len_x)

    c_gr = TCanvas('c_gr','c_gr', 1800, 1000)
    c_gr.cd()

    for idx in range(len_x):
        nameWksp = homeDir + "/FitMass_wksp_prompt_cheb_{}_{}.root".format(var,idx+1)
        file_w = TFile(nameWksp)
        w = file_w.Get('w')

        signif_Jpsi[idx] = w.var('n_Jpsi').getValV() / w.var('n_Jpsi').getError()
        print(w.var('n_Jpsi').getValV() , w.var('n_Jpsi').getError())

    print(signif_Jpsi)
    gr = TGraph(len_x,cuts[var][1],signif_Jpsi)
    #gr.SetMarkerStyle(kCircle);
    gr.SetName(var);
    gr.SetTitle(var);
    gr.SetLineColor(2);
    gr.SetLineWidth(4);
    gr.SetMarkerColor(4);
    gr.SetMarkerSize(1.5);
    gr.SetMarkerStyle(21);
    gr.Draw("ACP")
    #gr.SetMinimum(25.)
    #gr.SetMaximum(35.)

    c_gr.Draw()
    homeDir = '/afs/cern.ch/user/v/vazhovko/private/results/etac2s_ppbar/opt_fit/'
    c_gr.SaveAs(homeDir + '/{}_opt.png'.format(var))




#kBkgs = ['exp','cheb']
#kSources = ['fromB','prompt']
kBkg = 'cheb'
kSource = 'prompt'


link_list = {
                'Jpsi_PT'             : ['Jpsi_PT'],
                'Proton_PT'           : ['ProtonP_PT','ProtonM_PT'],
                'Proton_ProbNNp'      : ['ProtonP_ProbNNp','ProtonM_ProbNNp'],
                'Proton_ProbNNk'      : ['ProtonP_ProbNNk','ProtonM_ProbNNk'],
                'Proton_ProbNNpi'     : ['ProtonP_ProbNNpi','ProtonM_ProbNNpi'],
                'Proton_PIDp'         : ['ProtonP_PIDp','ProtonM_PIDp'],
                'Proton_PIDpK'        : ['ProtonP_PIDp-ProtonP_PIDK', 'ProtonM_PIDp-ProtonM_PIDK']

               }

opt_var_list = [
                'Jpsi_PT',
                'Proton_PT',
                'Proton_ProbNNp',
                'Proton_PIDp',
                'Proton_PIDpK'
               ]

opt_var_list_2d = [
                ['Proton_PIDp','Proton_PIDdiff'],
               ]



cuts = {'Jpsi_PT':              ['>', np.arange(5000., 10000., 500)],
        'Proton_PT':            ['>', np.arange(0., 8000., 1000)],
        'Proton_ProbNNp':       ['>', np.arange(0.0, 1.0, 0.1)],
        'Proton_ProbNNpi':      ['<', np.arange(0.1, 1.01, 0.1)],
        'Proton_ProbNNk':       ['<', np.arange(0.1, 1.01, 0.1)],
        # 'Proton_ProbNNp':       ['>', np.append(np.arange(0.0, 0.9, 0.05), np.arange(0.9, 1.00, 0.01))],
        'Proton_PIDp':          ['>', np.arange(0, 35, 5)],
        'Proton_PIDpK':         ['>', np.arange(0, 25, 5)],

       }

if __name__ == "__main__":
    
    kSources = ["prompt"]
    kSources = ["prompt", "fromB"]
    # ccbar_list = ["jpsi","psi2S"]
    ccbar_list = ["jpsi"]
    for ccbar in ccbar_list:
        for kSource in kSources:
            one_dim_opt('Proton_ProbNNp',ccbar,kSource)
            # two_dim_opt('Proton_ProbNNp','Proton_ProbNNk',ccbar,kSource)
            # two_dim_opt('Proton_ProbNNp','Proton_ProbNNpi',ccbar,kSource)
            # two_dim_opt('Proton_ProbNNpi','Proton_ProbNNk',ccbar,kSource)
    #tree = get_tuple(kSource)
    # tree = []

    #print get_scale()
    # for var in opt_var_list:
        # optim_1dim(tree, var)

    #opt_1dim_file('Jpsi_PT')
    #opt_1dim_file('Proton_PT')
    #opt_1dim_file('Proton_ProbNNp')
    # opt_1dim_file('Proton_PIDp')

