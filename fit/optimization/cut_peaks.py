from ROOT import *
import numpy as np

homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

binWigth = 0.5

minM_ppbar = 3250.
maxM_ppbar = 3850.
minM_misID = 500.
maxM_misID = 4000.

#minM_ppbar = 2750.
#maxM_ppbar = 3350.
#minM_misID = 0.
#maxM_misID = 3500.

nBins_ppbar = int((maxM_ppbar-minM_ppbar)/binWigth)
nBins_misID = int((maxM_misID-minM_misID)/binWigth)

var_list = ['Jpsi_m_scaled', 'Jpsi_MM',
            #'Jpsi_ENDVERTEX_CHI2',
            'Jpsi_ENDVERTEX_Z','Jpsi_OWNPV_Z',
            'Jpsi_PT',
            #'Jpsi_FDCHI2_OWNPV',
            'Jpsi_PZ',#'Jpsi_PE','Jpsi_P',
            'ProtonP_ProbNNp','ProtonM_ProbNNp',
            'ProtonP_ProbNNpi','ProtonM_ProbNNpi',
            'ProtonP_ProbNNk','ProtonM_ProbNNk',
            #'ProtonP_P','ProtonM_P',
            #'ProtonP_PT','ProtonM_PT',
            #'ProtonP_TRACK_CHI2NDOF','ProtonM_TRACK_CHI2NDOF',
            #'ProtonP_PIDK', 'ProtonP_PIDp', 'ProtonM_PIDK', 'ProtonM_PIDp',
            'ProtonP_IPCHI2_OWNPV', 'ProtonM_IPCHI2_OWNPV',
            #'ProtonP_TRACK_GhostProb', 'ProtonM_TRACK_GhostProb', 'ProtonP_TRACK_CloneDist', 'ProtonM_TRACK_CloneDist',
            #'Jpsi_M01_Subst0_p2pi','Jpsi_M01_Subst1_p2pi',
            #'Jpsi_M01_Subst0_p2K','Jpsi_M01_Subst1_p2K',
            'Jpsi_M01_Subst01_pp~2KK','Jpsi_M01_Subst01_pp~2pipi',
            'Jpsi_M01_Subst01_pp~2Kpi','Jpsi_M01_Subst01_pp~2piK',

            ]

var_dict = {
    "KK"    : "Jpsi_M01_Subst01_pp~2KK",
    "Kpi"   : "Jpsi_M01_Subst01_pp~2Kpi",
    "piK"   : "Jpsi_M01_Subst01_pp~2piK",
    "pipi"  : "Jpsi_M01_Subst01_pp~2pipi",
    "pK"    : "Jpsi_M01_Subst0_p2K",
    "Kp"    : "Jpsi_M01_Subst1_p2K",
    "ppi"   : "Jpsi_M01_Subst0_p2pi",
    "pip"   : "Jpsi_M01_Subst1_p2pi",
}

def draw_2D_mass(kSource, kRange, var):

    true_var = var_dict[var]
    treeName = dataDir + "/Data_{0}_NoPID/Etac2sDiProton_{0}_2018*.root".format(kRange)

    nt = TChain("DecayTree")
    nt.Add(treeName)
    #nt.SetBranchStatus("*",0)
    #for var in var_list:
        #nt.SetBranchStatus("{}".format(var),1);

    cut_FD = ""
    if kSource=="prompt":
        cut_FD = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08"# & Jpsi_FDCHI2_OWNPV > 49"
    else:
        cut_FD = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16"# & Jpsi_FDCHI2_OWNPV > 49"

    cut_PT     = "Jpsi_PT > 5000 && Jpsi_PT < 20000"
    cut_PID    = "ProtonP_ProbNNk < 0.6 && ProtonM_ProbNNk < 0.6 && ProtonP_ProbNNp > 0.4 && ProtonM_ProbNNp > 0.4 && ProtonP_ProbNNpi < 0.6 && ProtonM_ProbNNpi < 0.6 "
    cut_PIDk   = "ProtonP_ProbNNk < 0.6 && ProtonM_ProbNNk < 0.6"
    cut_PIDpi  = "ProtonP_ProbNNpi < 0.6 && ProtonM_ProbNNpi < 0.6 "

    cut_misID_Kpi = "(Jpsi_M01_Subst01_pp~2Kpi>1895 || Jpsi_M01_Subst01_pp~2Kpi<1835) && (Jpsi_M01_Subst01_pp~2piK>1895 || Jpsi_M01_Subst01_pp~2piK<1835)"

    cut_misID_pipi = "(Jpsi_M01_Subst01_pp~2pipi>1805 || Jpsi_M01_Subst01_pp~2piK<1770)"

    cut_misID_KK = "(Jpsi_M01_Subst01_pp~2KK>(1245 + 0.3196*Jpsi_m_scaled) || Jpsi_M01_Subst01_pp~2KK<(1225 + 0.3196*Jpsi_m_scaled))"


    totCut_mass = cut_FD + " && " + cut_misID_Kpi


    newfile = TFile(homeDir+"misID/plot_Mpp_vs_M{}_{}_{}_ProbNN06.root".format(var, kSource, kRange),"RECREATE")

    hh_2D     = TH2D("hh_2D","hh_2D", nBins_misID, minM_misID, maxM_misID, nBins_ppbar, minM_ppbar, maxM_ppbar)
    hh_2D_Kpi = TH2D("hh_2D_cutKpi","hh_2D_cutKpi", nBins_misID, minM_misID, maxM_misID, nBins_ppbar, minM_ppbar, maxM_ppbar)
    hh_2D_k   = TH2D("hh_2D_cutProbNNk","hh_2D_cutProbNNk", nBins_misID, minM_misID, maxM_misID, nBins_ppbar, minM_ppbar, maxM_ppbar)
    hh_2D_pi  = TH2D("hh_2D_cutProbNNpi","hh_2D_cutProbNNpi", nBins_misID, minM_misID, maxM_misID, nBins_ppbar, minM_ppbar, maxM_ppbar)
    hh_2D_tot = TH2D("hh_2D_cutProbNNkpi","hh_2D_cutProbNNkpi", nBins_misID, minM_misID, maxM_misID, nBins_ppbar, minM_ppbar, maxM_ppbar)

    plot_masses = "Jpsi_m_scaled:{}>>hh_2D".format(true_var)
    nt.Draw(plot_masses, cut_FD, "colz")
    plot_masses = "Jpsi_m_scaled:{}>>hh_2D_cutKpi".format(true_var)
    nt.Draw(plot_masses, cut_FD + " && " + cut_misID_Kpi, "colz")
    plot_masses = "Jpsi_m_scaled:{}>>hh_2D_cutProbNNk".format(true_var)
    nt.Draw(plot_masses, cut_FD + " && " + cut_PIDk, "colz")
    plot_masses = "Jpsi_m_scaled:{}>>hh_2D_cutProbNNpi".format(true_var)
    nt.Draw(plot_masses, cut_FD + " && " + cut_PIDpi, "colz")
    plot_masses = "Jpsi_m_scaled:{}>>hh_2D_cutProbNNkpi".format(true_var)
    nt.Draw(plot_masses, cut_FD + " && " + cut_PID, "colz")

    hh_2D.Write()
    hh_2D_Kpi.Write()
    hh_2D_k.Write()
    hh_2D_pi.Write()
    hh_2D_tot.Write()
    newfile.Close()


def fit_peaks(kSource):

    particle1, particle2 = "K","K"

    newfile = TFile(homeDir+"misID/out_{}_{}{}.root".format(kSource, particle1, particle2),"READ")

    hh_2D = TH2D()
    hh_2D = newfile.Get("hh_Reco_vs_misID")


    #newfile_fit = TFile(homeDir+"misID/fit_{}_{}{}.root".format(kSource, particle1, particle2),"RECREATE")
    #func = TF1("func","gaus",2250,2450);
    #func.SetParLimits(1, 2270, 2400)
    #func.SetParLimits(2, 0, 100)

    #aSlices = TObjArray()
    #hh_2D.FitSlicesX(func, 0, -1, 0, "QNR", aSlices);

    #aSlices.Write()
    #newfile_fit.Close()
    #newfile.Close()

    newfile_fit = TFile(homeDir+"misID/fit_{}_{}{}.root".format(kSource, particle1, particle2),"RECREATE")
    func = TF1("func","gaus",2250,2450);



    n_points = 50
    bins       = np.arange(3300.,3800.,10)+5.
    bins_err   = 5.*np.ones(50)
    means      = np.zeros(50)
    means_err  = np.zeros(50)
    width      = np.zeros(50)
    width_err  = np.zeros(50)

    for i in range(n_points):
        i_bin = (100/n_points)*i + 560
        shift = i*3.0
        func.SetRange(2265+shift, 2315+shift)
        #func.SetParameter(1, 2300)
        #func.SetParLimits(1, 2270, 2400)
        func.SetParLimits(2, 0, 100)
        hh_1D = hh_2D.ProjectionX("KK_mass", i_bin, i_bin+2)
        hh_1D.Fit(func,"R")
        means[i] = func.GetParameter(1)
        means_err[i] = func.GetParError(1)
        width[i] = func.GetParameter(2)
        width_err[i] = func.GetParError(2)
        hh_1D.Write()


    means_err[i] = means_err[i] * (means_err[i]<1000)
    gr_mean  = TGraphErrors(n_points,bins,means,bins_err,means_err)
    gr_width = TGraphErrors(n_points,bins,width,bins_err,width_err)


    c_fb = TCanvas('c_fb','c_fb', 5600, 2000)
    c_fb.Divide(2, 1)
    c_fb.cd(1)
    gr_mean.Draw("AP")
    gr_mean.SetTitle("mean")
    gr_mean.SetLineColor(2);
    gr_mean.SetLineWidth(1);
    gr_mean.SetMarkerColor(4);
    gr_mean.SetMarkerSize(1.5);
    gr_mean.SetMarkerStyle(21);
    gr_mean.GetXaxis().SetTitle("M_{ppbar}");
    gr_mean.GetYaxis().SetTitle("Peak mean");
    gr_mean.GetYaxis().SetTitleOffset(1.5);
    c_fb.cd(2)
    gr_width.Draw("AP")
    gr_width.SetTitle("sigma")
    gr_width.SetLineColor(2);
    gr_width.SetLineWidth(1);
    gr_width.SetMarkerColor(4);
    gr_width.SetMarkerSize(1.5);
    gr_width.SetMarkerStyle(21);
    gr_width.GetXaxis().SetTitle("M_{ppbar}");
    gr_width.GetYaxis().SetTitle("Peak width");
    gr_width.GetYaxis().SetTitleOffset(1.5);
    c_fb.SaveAs("plot.pdf")

    gr_mean.Write()
    gr_width.Write()
    newfile_fit.Close()
    newfile.Close()

if __name__ == '__main__':

    kSource = "fromB"
    kRanges = ["High","Low"]
    kRange = "High"
    particles = ["KK", "Kpi", "piK", "pipi", "pK", "Kp", "ppi", "pip"]
    particles = ["pipi", "pK", "Kp", "ppi", "pip"]
    #var = "pipi"
    for var in particles:
        #for kRange in kRanges:
            draw_2D_mass(kSource, kRange, var)
    #fit_peaks(kSource)