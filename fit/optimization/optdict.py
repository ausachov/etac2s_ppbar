import numpy as np

link_list = {
                "Jpsi_PT"             : ["Jpsi_PT"],
                "Jpsi_FDCHI2"         : ["Jpsi_FDCHI2_OWNPV"],
                "Proton_PT"           : ["ProtonP_PT","ProtonM_PT"],
                "Proton_ProbNNp"      : ["ProtonP_ProbNNp","ProtonM_ProbNNp"],          # add _corr in case of PID corrections
                "Proton_ProbNNk"      : ["ProtonP_ProbNNk","ProtonM_ProbNNk"],
                "Proton_ProbNNpi"     : ["ProtonP_ProbNNpi","ProtonM_ProbNNpi"],
                # "Proton_ProbNNp"      : ["ProtonP_ProbNNp_corr","ProtonM_ProbNNp_corr"],          # add _corr in case of PID corrections
                # "Proton_ProbNNk"      : ["ProtonP_ProbNNk_corr","ProtonM_ProbNNk_corr"],
                # "Proton_ProbNNpi"     : ["ProtonP_ProbNNpi_corr","ProtonM_ProbNNpi_corr"],
                "Proton_PIDp"         : ["ProtonP_PIDp","ProtonM_PIDp"],
                "Proton_PIDpK"        : ["ProtonP_PIDp-ProtonP_PIDK", "ProtonM_PIDp-ProtonM_PIDK"]

               }

opt_var_list = [
                #"Jpsi_PT",
                #"Jpsi_FDCHI2",
                #"Proton_PT",
                "Proton_ProbNNp",
                "Proton_ProbNNpi",
                "Proton_ProbNNk",
                #"Proton_PIDp",
                #"Proton_PIDpK"
               ]

opt_var_list_2d = [
                ["Proton_PIDp","Proton_PIDpK"],
                ["Proton_ProbNNp","Proton_ProbNNk"],
                ["Proton_ProbNNp","Proton_ProbNNpi"],
               ]

# cut list for ProbNN vars has one additional point for 2D optimization
cuts = {"Jpsi_PT":              [np.arange(5000., 10000., 500), 500.*np.ones(10), ">"],
        "Jpsi_FDCHI2":          [np.arange(0., 11., 1.)**2, 1.0*np.ones(11), ">"],
        "Proton_PT":            [np.arange(2000., 11000., 1000), 1000*np.ones(9), ">"],
        #for 2D optimization
        "Proton_ProbNNp":       [np.arange(0.0, 1.01, 0.1), 0.1*np.ones(10), ">"],
        # "Proton_ProbNNk":       [np.arange(0.0, 1.01, 0.1), 0.1*np.ones(10), "<"],
        # "Proton_ProbNNpi":      [np.arange(0.0, 1.01, 0.1), 0.1*np.ones(10), "<"],
        "Proton_ProbNNk":       [np.arange(0.0, 1.01, 0.2), 0.2*np.ones(6), "<"],
        "Proton_ProbNNpi":      [np.arange(0.0, 1.01, 0.2), 0.2*np.ones(6), "<"],
        # "Proton_ProbNNp":       [np.arange(0.0, 1.01, 0.1), 0.1*np.ones(9), ">"],
        # "Proton_ProbNNk":       [np.arange(0.0, 1.0, 0.1), 0.1*np.ones(9), "<"],
        # "Proton_ProbNNpi":      [np.arange(0.0, 1.0, 0.1), 0.1*np.ones(9), "<"],
        # "Proton_ProbNNpi":      [np.arange(0.0, 0.7, 0.1), 0.1*np.ones(6), "<"],
        'Proton_PIDp':          [np.arange(5., 27., 2.), 2*np.ones(6), ">"],
        'Proton_PIDpK':         [np.arange(0., 23., 2.), 2*np.ones(11), ">"],
        #for 1D optimization
        #"Proton_ProbNNp":       [np.append(np.arange(0.0, 0.9, 0.10), np.arange(0.9, 1.01, 0.02)), np.append(0.05*np.ones(9),0.01*np.ones(5)),  ">"],
        #"Proton_ProbNNk":       [np.append(np.arange(0.0, 0.4, 0.05), np.arange(0.4, 1.01, 0.10)), np.append(0.025*np.ones(8),0.01*np.ones(6)), "<"],
        #"Proton_ProbNNpi":      [np.append(np.arange(0.0, 0.4, 0.05), np.arange(0.4, 1.01, 0.10)), np.append(0.025*np.ones(8),0.01*np.ones(6)), "<"],
        #"Proton_PIDp":          [np.append(np.arange(5, 17, 2),np.arange(16, 25, 1)), np.append(1.0*np.ones(6),0.5*np.ones(9)), ">"],
        #"Proton_PIDpK":         [np.append(np.arange(0, 12, 2),np.arange(11, 20, 1)), np.append(1.0*np.ones(6),0.5*np.ones(9)), ">"],

       }

