from ROOT import *
import numpy as np
gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")

from optdict import *

binWidth = 0.2
binWidthDraw = 0.2

minM_Low = 2850
maxM_Low = 3250
binN_Low = int((maxM_Low-minM_Low)/binWidth)

minM_High = 3320
maxM_High = 3780
binN_High = int((maxM_High-minM_High)/binWidth)



binning = RooFit.Binning(93, minM_Low, maxM_High)
binning_Jpsi = RooFit.Binning(40, minM_Low, maxM_Low)
binning_etac2s = RooFit.Binning(46, minM_High, maxM_High)
mrkSize = RooFit.MarkerStyle(7)
lineWidth1 = RooFit.LineWidth(1)
lineWidth2 = RooFit.LineWidth(2)
lineStyle1 = RooFit.LineStyle(2)
lineStyle2 = RooFit.LineStyle(9)
lineColor1 = RooFit.LineColor(6)
lineColor2 = RooFit.LineColor(8)
name = RooFit.Name
cut = RooFit.Cut

#homeDir = "/afs/cern.ch/user/v/vazhovko/private/etac2s_ppbar/"
#dataDir = "/eos/user/v/vazhovko/etac2s_ppbar/"
homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"


kBkg = "cheb"
kSources = ["fromB","prompt"]

n_etac_fit = {
              "fromB":  [2.4976e+04, 2.13e+02], #jpsi
              "prompt": [1.0649e+05, 9.29e+03], #jpsi
              # "fromB": [1.3920e+03, 8.29e+01], #psi2S
              # "prompt": [8.9344e+03, 3.73e+03], #psi2S
              # "prompt": [7.3663e+04, 3.00e+03],
             }
n_etac_MC = {
              "fromB": [22923.], #jpsi
              "prompt": [17109.], #jpsi
              # "fromB": [24678.], #psi2S
              # "prompt": [6177.], #psi2S
              # "prompt": [7.3663e+04, 3.00e+03],
             }
titleDict = {
    "Proton_ProbNNpi": "ProbNN(pi)",
    "Proton_ProbNNk" : "ProbNN(K)",
    "Proton_ProbNNp" : "ProbNN(p)",
}


def check_ProbNN_1D(ccbar, kSource, var_x, kMC=False, postfix=None):

    kRange = "High" if ccbar=="psi2S" else "Low" 
    gStyle.SetOptTitle(0)
    gStyle.SetOptStat(0)

    var_x_val = cuts[var_x][0]
    var_x_err = cuts[var_x][1]

    n_points_x = var_x_val.size-1

    n_var_prompt = 0
    n_var_fromB = 0


    sign_list = {
                # "n_Jpsi": np.zeros([n_points_x, n_points_y]),
                # "n_{}".format(ccbar): np.zeros([n_points_x, n_points_y]),
                "n_{}_MC".format(ccbar): np.zeros([n_points_x]),
                 #"n_etac": np.zeros([n_points_x, n_points_y])
                 }
    sign_list_err = {
                # "n_Jpsi": np.zeros([n_points_x, n_points_y]),
                # "n_{}".format(ccbar): np.zeros([n_points_x, n_points_y]),
                "n_{}_MC".format(ccbar): np.zeros([n_points_x]),
                 #"n_etac": np.zeros([n_points_x, n_points_y])
                 }

    hh = TH1D("hh","hh",n_points_x,var_x_val)
    hh_MC = TH1D()

    if kMC:
        # nameMC = "{}/optimization/MC_yield_{}_{}_{}{}.root".format(homeDir,ccbar,kSource, var_x, postfix)
        nameMC = "{}/optimization/PIDCalib_yield_{}_{}_{}{}.root".format(homeDir,ccbar,kSource, var_x, postfix)
        file_MC = TFile(nameMC)
        hh_MC = file_MC.Get("hh_MC")
        hh_MC.SetDirectory(0)
        file_MC.Close()

    for idx_cut in range(n_points_x):

        # nameWksp = homeDir + "/mass_fit/ProbNN_check/2D/Wksp_MassFit_{}_{}_{}_{}_{}.root".format(kSource,var_x,var_y,idx_cut+1,idy_cut+1)
        nameWksp = homeDir + "/mass_fit/ProbNN_check/{}/Wksp_{}_{}_cheb_{}.root".format(var_x,ccbar,kSource,idx_cut+1)
        file_w = TFile(nameWksp)
        w = file_w.Get("w")
        # w.Print()

        for var_name in sign_list:

            if kMC:
                n_jpsi = hh_MC.GetBinContent(idx_cut+1) * n_etac_fit[kSource][0]
                err_jpsi = hh_MC.GetBinError(idx_cut+1) * n_etac_fit[kSource][0]
            else:
                # n_jpsi_scale = w.var("n_Jpsi").getValV()/n_etac_fit[kSource][0]
                n_jpsi_scale = n_etac_fit[kSource][0]/n_etac_MC[kSource][0]
                # err_jpsi_fit = w.var("n_Jpsi").getError()
                n_jpsi = w.var(var_name).getValV()*n_jpsi_scale
                err_jpsi = w.var(var_name).getError()*n_jpsi_scale

            if ccbar=="jpsi":
                sigma = w.var("sigma_Jpsi").getValV() #data
                m_jpsi = w.var("mass_Jpsi").getValV()
            else:
                sigma = w.var("sigma_psi2S").getValV() #data
                m_jpsi = w.var("mass_psi2S").getValV()
            # m_etac = w.var("mass_etac").getValV()
            # m_res = w.var("mass_res").getValV()
            bkg = w.pdf("bkg_{}".format(kRange))

            gamma = 32.0
            Jpsi_M = w.var("Jpsi_M")
            Jpsi_M.setRange("my_range_{}".format(idx_cut), m_jpsi-3*sigma, m_jpsi+3*sigma)
            # Jpsi_M.setRange("my_range_{}".format(idx_cut), m_jpsi-m_res-1.5*gamma, m_jpsi-m_res+1.5*gamma)
            argset = RooArgSet(Jpsi_M)
            i = bkg.createIntegral(argset, RooFit.NormSet(argset), RooFit.Range("my_range_{}".format(idx_cut)))
            # print(i.getValV(), w.var("nBckgr_{}".format(kRange)).getValV())
            n_bkg = i.getValV() * w.var("nBckgr_{}".format(kRange)).getValV()

            err_jpsi_upd =  (err_jpsi * (1/n_jpsi - 1/2./(n_jpsi + n_bkg)))**2
            err_bkg =  ( i.getValV() * w.var("nBckgr_{}".format(kRange)).getError() / 2 / (n_jpsi + n_bkg))**2

            sign_list[var_name][idx_cut] = abs(n_jpsi/(n_jpsi + n_bkg)**0.5)
            # sign_list[var_name][idx_cut] = abs(n_jpsi/(n_bkg)**0.5)
            sign_list_err[var_name][idx_cut] = sign_list[var_name][idx_cut] * (err_jpsi_upd + err_bkg)**0.5

            hh.SetBinContent(idx_cut+1, sign_list[var_name][idx_cut])
            hh.SetBinError(idx_cut+1, sign_list_err[var_name][idx_cut])
            # print(idx_cut+1, sign_list[var_name][idx_cut])
            # print(idx_cut+1, sign_list_err[var_name][idx_cut])
            # print("sig: {}; bkg: {}".format(n_jpsi , n_bkg))

        file_w.Close()

    canv = TCanvas("canv_{}".format(kSource),"canv", 3600, 2000)

    gr   = {}
    line = {}
    n = 1

    for var_name in sign_list:

        canv.cd(n)
        hh.Draw("E1")
        hh.SetTitle(var_name)
        hh.SetLineColor(2)
        hh.SetLineWidth(1)
        hh.SetMarkerColor(2)
        hh.GetXaxis().SetTitle(titleDict[var_x])
        hh.GetYaxis().SetTitle("N_{sig}/#sqrt{N_{sig}+N_{bg}}")
        #hh.GetYaxis().SetTitleSize(0.05)
        #hh.GetYaxis().SetTitleOffset(1.0)
        #hh.GetXaxis().SetRangeUser(0.4, 1.0)
        #hh.GetYaxis().SetRangeUser(0.4, 0.9)
        #hh.GetXaxis().SetRangeUser(pid_p_val[0]-1, pid_p_val[-1]+1)
        #hh.GetYaxis().SetRangeUser(sign_list[var_name][1:].min()*0.8, sign_list[var_name][1:].max()*1.15)
        #hh.GetYaxis().SetRangeUser(100,125)
        #line[var_name]  = TLine(0.4,sign_list_0["n_Jpsi"],1.0,sign_list_0["n_Jpsi"])
        #line[var_name].SetLineColor(kYellow)
        #line[var_name].SetLineWidth(2)
        #line[var_name].Draw("same")
        n+=1

    if kMC: postfix += '_MC'
    canv.SaveAs("{}_{}_{}_bg{}_PIDCalib.pdf".format(kSource,var_x, ccbar, postfix))
    #c.SaveAs("{}_{}_{}_Jpsi_etac_bg.pdf".format(kSource,var_x,var_y))
    del canv

def check_ProbNN_2D(ccbar, kSource, var_x, var_y, kMC=False, postfix=None):

    kRange = "High" if ccbar=="psi2S" else "Low" 
    gStyle.SetOptTitle(0)
    gStyle.SetOptStat(0)
    gStyle.SetPalette(57)

    var_x_val = cuts[var_x][0]
    var_x_err = cuts[var_x][1]
    var_y_val = cuts[var_y][0]
    var_y_err = cuts[var_y][1]

    n_points_x = var_x_val.size-1
    n_points_y = var_y_val.size-1

    n_var_prompt = 0
    n_var_fromB = 0


    sign_list = {
                # "n_Jpsi": np.zeros([n_points_x, n_points_y]),
                # "n_{}".format(ccbar): np.zeros([n_points_x, n_points_y]),
                "n_{}_MC".format(ccbar): np.zeros([n_points_x, n_points_y]),
                 #"n_etac": np.zeros([n_points_x, n_points_y])
                 }
    sign_list_err = {
                # "n_Jpsi": np.zeros([n_points_x, n_points_y]),
                # "n_{}".format(ccbar): np.zeros([n_points_x, n_points_y]),
                "n_{}_MC".format(ccbar): np.zeros([n_points_x, n_points_y]),
                 #"n_etac": np.zeros([n_points_x, n_points_y])
                 }

    hh = TH2D("hh","hh",n_points_x,var_x_val,n_points_y,var_y_val)
    hh_MC = TH2D()

    if kMC:
        # nameMC = "{}/optimization/MC_yield_{}_{}_{}_{}{}.root".format(homeDir,ccbar,kSource, var_x, var_y, postfix)
        nameMC = "{}/optimization/PIDCalib_yield_{}_{}_{}_{}{}.root".format(homeDir,ccbar,kSource, var_x, var_y, postfix)
        file_MC = TFile(nameMC)
        hh_MC = file_MC.Get("hh_MC")

    for idx_cut in range(n_points_x):
        for idy_cut in range(n_points_y):

            # nameWksp = homeDir + "/mass_fit/ProbNN_check/2D/Wksp_MassFit_{}_{}_{}_{}_{}.root".format(kSource,var_x,var_y,idx_cut+1,idy_cut+1)
            nameWksp = homeDir + "/mass_fit/ProbNN_check/{}_{}/Wksp_{}_{}_cheb_{}_{}.root".format(var_x,var_y,ccbar,kSource,idx_cut+1,idy_cut+1)
            file_w = TFile(nameWksp)
            w = file_w.Get("w")
            # w.Print()

            for var_name in sign_list:

                if kMC:
                    n_jpsi = hh_MC.GetBinContent(idx_cut+1, idy_cut+1) * n_etac_fit[kSource][0]
                    err_jpsi = hh_MC.GetBinError(idx_cut+1, idy_cut+1) * n_etac_fit[kSource][0]
                else:
                    # n_jpsi_scale = w.var("n_Jpsi").getValV()/n_etac_fit[kSource][0]
                    n_jpsi_scale = n_etac_fit[kSource][0]/n_etac_MC[kSource][0]
                    # err_jpsi_fit = w.var("n_Jpsi").getError()
                    n_jpsi = w.var(var_name).getValV()*n_jpsi_scale
                    err_jpsi = w.var(var_name).getError()*n_jpsi_scale

                if ccbar=="jpsi":
                    sigma = w.var("sigma_Jpsi").getValV() #data
                    m_jpsi = w.var("mass_Jpsi").getValV()
                else:
                    sigma = w.var("sigma_psi2S").getValV() #data
                    m_jpsi = w.var("mass_psi2S").getValV()
                # m_etac = w.var("mass_etac").getValV()
                # m_res = w.var("mass_res").getValV()
                bkg = w.pdf("bkg_{}".format(kRange))
    
                gamma = 32.0
                Jpsi_M = w.var("Jpsi_M")
                Jpsi_M.setRange("my_range_{}".format(idx_cut), m_jpsi-3*sigma, m_jpsi+3*sigma)
                # Jpsi_M.setRange("my_range_{}".format(idx_cut), m_jpsi-m_res-1.5*gamma, m_jpsi-m_res+1.5*gamma)
                argset = RooArgSet(Jpsi_M)
                i = bkg.createIntegral(argset, RooFit.NormSet(argset), RooFit.Range("my_range_{}".format(idx_cut)))
                # print(i.getValV(), w.var("nBckgr_{}".format(kRange)).getValV())
                n_bkg = i.getValV() * w.var("nBckgr_{}".format(kRange)).getValV()

                err_jpsi_upd =  (err_jpsi * (1/n_jpsi - 1/2./(n_jpsi + n_bkg)))**2
                err_bkg =  ( i.getValV() * w.var("nBckgr_{}".format(kRange)).getError() / 2 / (n_jpsi + n_bkg))**2


                sign_list[var_name][idx_cut][idy_cut] = abs(n_jpsi/(n_jpsi + n_bkg)**0.5)
                # sign_list[var_name][idx_cut][idy_cut] = abs(n_jpsi/(n_bkg)**0.5)
                sign_list_err[var_name][idx_cut][idy_cut] = sign_list[var_name][idx_cut][idy_cut] * (err_jpsi_upd + err_bkg)**0.5
                #sign_list[var_name][idx_cut] = abs(n_jpsi/err_jpsi)
                #sign_list_err[var_name][idx_cut] = 1.0
                hh.SetBinContent(idx_cut+1, idy_cut+1, sign_list[var_name][idx_cut][idy_cut])
                hh.SetBinError(idx_cut+1, idy_cut+1, sign_list_err[var_name][idx_cut][idy_cut])
                # print(idx_cut+1, idy_cut+1, sign_list[var_name][idx_cut][idy_cut])
                # print(idx_cut+1, idy_cut+1, sign_list_err[var_name][idx_cut][idy_cut])
                # print("sig: {}; bkg: {}".format(n_jpsi , n_bkg))

            file_w.Close()

    c = TCanvas("c","c", 3600, 2000)

    gr   = {}
    line = {}
    n = 1

    for var_name in sign_list:

        pad = c.cd(n)
        pad.SetRightMargin(0.15)
        pad.SetTopMargin(0.075)
        hh.Draw("colz")
        hh.SetTitle(var_name)
        hh.SetLineColor(2)
        hh.SetLineWidth(1)
        hh.SetMarkerColor(4)
        hh.GetXaxis().SetTitle(titleDict[var_x])
        hh.GetYaxis().SetTitle(titleDict[var_y])
        #hh.GetYaxis().SetTitleSize(0.05)
        #hh.GetYaxis().SetTitleOffset(1.0)
        #hh.GetXaxis().SetRangeUser(0.4, 1.0)
        #hh.GetYaxis().SetRangeUser(0.4, 0.9)
        #hh.GetXaxis().SetRangeUser(pid_p_val[0]-1, pid_p_val[-1]+1)
        #hh.GetYaxis().SetRangeUser(sign_list[var_name][1:].min()*0.8, sign_list[var_name][1:].max()*1.15)
        #hh.GetYaxis().SetRangeUser(100,125)
        #line[var_name]  = TLine(0.4,sign_list_0["n_Jpsi"],1.0,sign_list_0["n_Jpsi"])
        #line[var_name].SetLineColor(kYellow)
        #line[var_name].SetLineWidth(2)
        #line[var_name].Draw("same")
        n+=1

    if kMC: postfix += '_MC'
    c.SaveAs("{}_{}_{}_{}_bg{}_PIDCalib.pdf".format(kSource,var_x,var_y, ccbar, postfix))
    # c.SaveAs("{}_{}_{}_{}_bg{}.pdf".format(kSource,var_x,var_y, ccbar, postfix))


if __name__ == '__main__':

    gROOT.LoadMacro("../libs/lhcbStyle.C")
    #check_ProbNN("fromB")
    #check_ProbNN("prompt")
    postfixes = ["_pc_Lb","_pg_Lb","_pg_Lc", "_pidgen"]
    postfixes = ["_AddBr","_Lb_pidGen"]
    postfixes = ["_sel"]
    #postfix = "_pidcorr"
    #for pid in opt_var_list:
        #for postfix in postfixes:
            #check_ProbNN_1D("prompt",pid, True, postfix)
            #check_ProbNN_1D("fromB", pid, True, postfix)

    # ccbar = "jpsi"
    ccbar = "psi2S"
    kSources = ["prompt","fromB"]
    # kSources = ["fromB"]
    mcFlag = True
    for postfix in postfixes:
        for kSource in kSources:
            # check_ProbNN_1D(ccbar, kSource,"Proton_ProbNNp", mcFlag, postfix)
            check_ProbNN_2D(ccbar, kSource,"Proton_ProbNNp","Proton_ProbNNk", mcFlag, postfix)
            check_ProbNN_2D(ccbar, kSource,"Proton_ProbNNp","Proton_ProbNNpi", mcFlag, postfix)
            # check_ProbNN_2D(ccbar, kSource,"Proton_ProbNNpi","Proton_ProbNNk", mcFlag, postfix)


