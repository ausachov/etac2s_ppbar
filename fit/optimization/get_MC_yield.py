from ROOT import *
import numpy as np

homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

kBkg = "cheb"
kSources = ["fromB","prompt"]
from optdict import *

def get_MC_signal_2D(ccbar, kSource, var_x, var_y, postfix=""):

    print("cutting", var_x, var_y)

    # dirName = dataDir +"/MC/Trigger/PIDGen/"
    dirName = dataDir +"/MC/Trigger/"

    nt  =  TChain("DecayTree")
    nt.Add(dirName + "/{}DiProton_{}_*_2018{}.root".format(ccbar,kSource,postfix))


    var_x_val = cuts[var_x][0]
    var_y_val = cuts[var_y][0]
    len_x = var_x_val.size
    len_y = var_y_val.size

    # newfile = TFile("{}/optimization/MC_yield_{}_{}_{}_{}{}_pidGen.root".format(homeDir,ccbar,kSource, var_x, var_y, postfix),"RECREATE")
    newfile = TFile("{}/optimization/MC_yield_{}_{}_{}_{}{}.root".format(homeDir,ccbar,kSource, var_x, var_y, postfix),"RECREATE")

    hh_MC = TH2D("hh_MC","hh_MC",len_x-1,var_x_val,len_y-1,var_y_val)

    if kSource=="prompt":
        cut_FD = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08"
    else:
        cut_FD = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16"

    cut_Protons = "ProtonP_P>12.5e3 && ProtonM_P>12.5e3                    && \
                    ProtonP_PT>2000 && ProtonM_PT>2000                          && \
                    ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366  \
                  "

    n_tot = float(nt.GetEntries(cut_FD + " && " + cut_Protons))

    if cuts[var_x][2] == '>': var_x_val = var_x_val[:-1]
    else:                     var_x_val = var_x_val[1:]

    if cuts[var_y][2] == '>': var_y_val = var_y_val[:-1]
    else:                     var_y_val = var_y_val[1:]

    for cut_val_x in var_x_val:
        idx = list(var_x_val).index(cut_val_x) + 1

        for cut_val_y in var_y_val:
            idy = list(var_y_val).index(cut_val_y) + 1

            cut_string = ""

            for var_name in link_list[var_x]:
                cut_string += "{} {} {:.2f} && ".format(var_name,cuts[var_x][2],cut_val_x)

                #if cut_string != "":
                    #cut_string += "{} {} {:.2f}".format(var_name,cuts[var_x][2],cut_val_x)
                #else:
                    #cut_string += " && {} {} {:.2f}".format(var_name,cuts[var_x][2],cut_val_x)


            for var_name in link_list[var_y]:
                cut_string += "{} {} {:.2f}  && ".format(var_name,cuts[var_y][2],cut_val_y)

            cut_string += cut_FD + " && " + cut_Protons

            n_signal = nt.GetEntries(cut_string)/n_tot
            hh_MC.SetBinContent(idx, idy, n_signal)
            hh_MC.SetBinError(idx, idy, n_signal**0.5)

    hh_MC.Write()
    newfile.Write()
    newfile.Close()

def get_MC_signal_1D(kSource, var_x, postfix=""):

    print("cutting", var_x)

    dirName = dataDir +"/MC/Trigger/PIDGen/"

    nt  =  TChain("DecayTree")
    nt.Add(dirName + "/jpsiDiProton_{}_2018{}.root".format(kSource,postfix))


    var_x_val = cuts[var_x][0]
    len_x = var_x_val.size


    newfile = TFile("{}/optimization/MC_yield_jpsi_{}_{}{}.root".format(homeDir,kSource, var_x, postfix),"RECREATE")

    hh_MC = TH1D("hh_MC","hh_MC",len_x-1,var_x_val)

    if kSource=="prompt":
        cut_FD = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08"
    else:
        cut_FD = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16"

    cut_Protons = "ProtonP_P>12.5e3 && ProtonM_P>12.5e3                    && \
                    ProtonP_PT>2000 && ProtonM_PT>2000                          && \
                    ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366  \
                  "

    n_tot = float(nt.GetEntries(cut_FD + " && " + cut_Protons))

    if cuts[var_x][2] == '>': var_x_val = var_x_val[:-1]
    else:                     var_x_val = var_x_val[1:]

    for cut_val_x in var_x_val:
        idx = list(var_x_val).index(cut_val_x) + 1

        cut_string = ""

        for var_name in link_list[var_x]:
            cut_string += "{} {} {:.2f} && ".format(var_name,cuts[var_x][2],cut_val_x)

        cut_string += cut_FD + " && " + cut_Protons

        n_signal = nt.GetEntries(cut_string)/n_tot
        hh_MC.SetBinContent(idx, n_signal)
        hh_MC.SetBinError(idx, n_signal**0.5)

    hh_MC.Write()
    #newfile.Write()
    newfile.Close()

def get_PIDCalib_signal_1D(ccbar, kSource, var_x, postfix=""):

    print("cutting", var_x)

    # dirName = dataDir +"/MC/Trigger/PIDGen/"
    dirName = dataDir +"/MC/Trigger/PIDOpt/{}/".format( var_x)

    var_x_val = cuts[var_x][0]
    len_x = var_x_val.size


    newfile = TFile("{}/optimization/PIDCalib_yield_{}_{}_{}{}.root".format(homeDir,ccbar,kSource, var_x, postfix),"RECREATE")
    # print("{}/optimization/PIDCalib_yield_{}_{}_{}{}.root".format(homeDir,ccbar,kSource, var_x, postfix))
    hh_MC = TH1D("hh_MC","hh_MC",len_x-1,var_x_val)

    if cuts[var_x][2] == '>': var_x_val = var_x_val[:-1]
    else:                     var_x_val = var_x_val[1:]

    for cut_val_x in var_x_val:
        idx = list(var_x_val).index(cut_val_x) + 1

        nt_PID  =  TChain("PIDCalibTree")
        nt_PID.Add(dirName + "/jpsiDiProton_{}_Mag*_2018{}_PID_eff_{:.1f}.root".format(kSource,postfix,cut_val_x))
        # nt_PID = TChain("DecayTree")
        # nt_PID.Add(dirName + "/jpsiDiProton_{}_Mag*_2018{}_{:.1f}.root".format(kSource,postfix,cut_val_x))
        # nt_PID.Print()
        # nt = TChain("DecayTree")
        # nt.Add("{}/MC/Trigger/{}DiProton_{}_Mag*_2018_{}.root".format(dataDir, ccbar, kSource, postfix))
        # nt.Print()

        # nt_PID.AddFriend(nt)

        h_eff = TH1D("h_eff","h_eff",100,0.,1.)
        h_err = TH1D("h_err","h_err",100,0.,0.5)

        nt_PID.Draw("PIDCalibEff>>h_eff","","goff")
        nt_PID.Draw("PIDCalibErr>>h_err","","goff")

        # nt_PID.Draw("PIDCalibEff>>h_eff","Jpsi_PT<6500","goff")
        # nt_PID.Draw("PIDCalibErr>>h_err","Jpsi_PT<6500","goff")

        n_signal = h_eff.GetMean()
        n_s_err  = h_err.GetRMS()

        print(n_signal,n_s_err)
        hh_MC.SetBinContent(idx, n_signal)
        hh_MC.SetBinError(idx, n_s_err)

        del nt_PID, h_eff, h_err

    newfile.cd()
    hh_MC.Write()
    # hh_MC.Draw("E1")
    # newfile.Write()
    newfile.Close()


def get_PIDCalib_signal_2D(ccbar, kSource, var_x, var_y, postfix=""):

    print("cutting", var_x, var_y)

    # dirName = dataDir +"/MC/Trigger/PIDGen/"
    dirName = dataDir +"/MC/Trigger/PIDOpt/{}_{}".format( var_x, var_y)

    var_x_val = cuts[var_x][0]
    var_y_val = cuts[var_y][0]
    len_x = var_x_val.size
    len_y = var_y_val.size


    newfile = TFile("{}/optimization/PIDCalib_yield_{}_{}_{}_{}{}.root".format(homeDir,ccbar,kSource, var_x, var_y, postfix),"RECREATE")
    print("{}/optimization/PIDCalib_yield_{}_{}_{}_{}{}.root".format(homeDir,ccbar,kSource, var_x, var_y, postfix))
    hh_MC = TH2D("hh_MC","hh_MC",len_x-1,var_x_val,len_y-1,var_y_val)

    if cuts[var_x][2] == '>': var_x_val = var_x_val[:-1]
    else:                     var_x_val = var_x_val[1:]
    if cuts[var_y][2] == '>': var_y_val = var_y_val[:-1]
    else:                     var_y_val = var_y_val[1:]

    for cut_val_x in var_x_val:
        idx = list(var_x_val).index(cut_val_x) + 1
        for cut_val_y in var_y_val:
            idy = list(var_y_val).index(cut_val_y) + 1

            nt_PID  =  TChain("PIDCalibTree")
            nt_PID.Add(dirName + "/jpsiDiProton_{}_Mag*_2018{}_PID_eff_{:.1f}_{:.1f}.root".format(kSource,postfix,cut_val_x,cut_val_y))

            h_eff = TH1D("h_eff","h_eff",100,0.,1.)
            h_err = TH1D("h_err","h_err",100,0.,0.5)

            nt_PID.Draw("PIDCalibEff>>h_eff","","goff")
            nt_PID.Draw("PIDCalibErr>>h_err","","goff")

            n_signal = h_eff.GetMean()
            n_s_err  = h_err.GetRMS()

            print(n_signal,n_s_err)
            hh_MC.SetBinContent(idx, idy, n_signal)
            hh_MC.SetBinError(idx, idy, n_s_err)

            del nt_PID, h_eff, h_err

    newfile.cd()
    hh_MC.Write()
    # hh_MC.Draw("E1")
    # newfile.Write()
    newfile.Close()

if __name__ == '__main__':
    
    # postfixes = ["_pc_Lb", "_pg_Lb", "_pg_Lc", "_pidgen"]
    # postfixes = ["_Lb"]
    # postfixes = ["_AddBr"]
    postfixes = ["_sel"]
    postfix = ""
    ccbar = "psi2S"
    for postfix in postfixes:
        for kSource in ["prompt","fromB"]:
            # get_PIDCalib_signal_1D(ccbar, kSource, "Proton_ProbNNp", postfix)
            get_PIDCalib_signal_2D(ccbar, kSource, "Proton_ProbNNp", "Proton_ProbNNk", postfix)
            get_PIDCalib_signal_2D(ccbar, kSource, "Proton_ProbNNp", "Proton_ProbNNpi", postfix)
            # get_MC_signal_2D(ccbar, kSource, "Proton_ProbNNp","Proton_PIDp", postfix)
            # get_MC_signal_2D(ccbar, kSource, "Proton_ProbNNp","Proton_ProbNNk", postfix)
            # get_MC_signal_2D(ccbar, kSource, "Proton_ProbNNpi","Proton_ProbNNk", postfix)
            # get_MC_signal_2D(ccbar, kSource, "Proton_ProbNNp","Proton_ProbNNpi", postfix)
            # get_MC_signal_1D(ccbar, kSource,"Proton_ProbNNp",postfix)
        # get_MC_signal_2D("prompt","Proton_ProbNNp","Proton_PIDp", postfix)
        # get_MC_signal_2D("fromB","Proton_ProbNNp","Proton_ProbNNk", postfix)
        # get_MC_signal_2D("prompt","Proton_ProbNNp","Proton_ProbNNk", postfix)
        # get_MC_signal_2D("fromB","Proton_ProbNNp","Proton_ProbNNpi", postfix)
        # get_MC_signal_2D("prompt","Proton_ProbNNp","Proton_ProbNNpi", postfix)
        # get_MC_signal_2D("fromB","Proton_ProbNNk","Proton_ProbNNpi", postfix)
        # get_MC_signal_2D("prompt","Proton_ProbNNk","Proton_ProbNNpi", postfix)

    #for pid in opt_var_list:
        #for postfix in postfixes:
            #get_MC_signal_1D("fromB",pid,postfix)
            #get_MC_signal_1D("prompt",pid,postfix)

