from ROOT import TF1, TH1D, TCanvas, TGraphAsymmErrors, TGraphErrors, TLatex, TLegend, gPad, gROOT
#from array import array
import numpy as np

homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"
nPTBins = 5


def get_thPlot(charm="etac1S",add=""):


    n_points = nPTBins
    cs_th_val       = np.zeros(n_points)
    #cs_th_sc_error  = np.array((2, n_points)
    #cs_th_PDF_error = np.array((2, n_points)

    cs_th_sc_eh  = np.zeros(n_points)
    cs_th_sc_el  = np.zeros(n_points)

    pt_val  = np.zeros(n_points)
    pt_err  = np.zeros(n_points)
    pt_low  = np.zeros(n_points)
    pt_high = np.zeros(n_points)

    fi = open("{}/results/{}{}_dsigmadpt.dat".format(homeDir, charm, add),"r")

    for i_p in range(n_points):

        line = fi.readline().split()
        pt_low[i_p]     = float(line[0])
        pt_high[i_p]    = float(line[1])

        pt_val[i_p] = (pt_high[i_p] + pt_low[i_p])/2.
        pt_err[i_p] = (pt_high[i_p] - pt_low[i_p])/2.

        cs_th_val[i_p]   = float(line[2])
        cs_th_sc_eh[i_p] = float(line[3]) - cs_th_val[i_p]
        cs_th_sc_el[i_p] = cs_th_val[i_p] - float(line[4])


    #print(pt_val)
    #print(cs_th_val)
    #print(cs_th_PDF_eh)
    #print(cs_th_PDF_el)
    #print(cs_th_sc_eh)
    #print(cs_th_sc_el)

    cs_theory_CS  = TGraphAsymmErrors(n_points, pt_val, cs_th_val, pt_err, pt_err, cs_th_sc_el, cs_th_sc_eh)
    #cs_theory_CO = TGraphAsymmErrors(n_points, pt_val, cs_th_val, pt_err, pt_err, cs_th_PDF_el, cs_th_PDF_eh)


    #return cs_theory_CS
    return cs_th_val, cs_th_sc_el, cs_th_sc_el


def get_rel_CS(key="",rel=False):

    n_points = nPTBins

    etac_val, etac_el, etac_eh = get_thPlot("etac1S",key)        #Color Singlet
    jpsi_val, jpsi_el, jpsi_eh = np.ones(n_points), np.zeros(n_points), np.zeros(n_points)
    if rel:
        jpsi_val, jpsi_el, jpsi_eh = get_thPlot("jpsi")
    rel_val = etac_val/jpsi_val

    rel_el  = rel_val*((etac_el/etac_val)**2 + (jpsi_el/jpsi_val)**2)**0.5
    rel_eh  = rel_val*((etac_eh/etac_val)**2 + (jpsi_eh/jpsi_val)**2)**0.5

    pt_val  = np.array([5.75, 7.25, 9.0, 11.0, 13.0, 17.0])
    pt_err = np.array([0.75, 0.75, 1.0, 1.0, 1.0, 3.0])
    cs_theory_CS  = TGraphAsymmErrors(n_points, pt_val, rel_val, pt_err, pt_err, rel_el, rel_eh)

    return cs_theory_CS

def get_csMeas(rel=False):

    cs_val      = np.zeros(nPTBins)
    cs_val_new  = np.zeros(2)
    cs_stat_e   = np.zeros(nPTBins)
    cs_syst_u_e = np.zeros(nPTBins)
    cs_syst_c_e = np.zeros(nPTBins)
    cs_BRs_e    = np.zeros(nPTBins)
    cs_ss_e   = np.zeros(nPTBins)
    cs_tot_e    = np.zeros(nPTBins)

    pt_val = np.zeros(nPTBins)
    pt_err = np.zeros(nPTBins)
    pt = np.array([5.0, 6.5, 8.0, 10.0, 12.0, 14.0, 20.])
    cs_tot  = TH1D("hh","hh", nPTBins, pt)

    add = ""
    if rel: add="_rel"

    fi = open("{}/results/etac1S_dsigmadpt_meas{}.dat".format(homeDir,add),"r")

    for i_p in range(nPTBins):

        line = fi.readline().split()
        pt_val[i_p]  = float(line[0])
        pt_err[i_p]  = float(line[1])

        cs_val[i_p]      = float(line[2])
        cs_stat_e[i_p]   = float(line[3])
        cs_syst_u_e[i_p] = float(line[4])
        cs_syst_c_e[i_p] = float(line[5])
        cs_BRs_e[i_p]    = float(line[6])

        cs_ss_e[i_p]   = (cs_stat_e[i_p]**2 + cs_syst_u_e[i_p]**2 + cs_syst_c_e[i_p]**2)**0.5
        cs_tot_e[i_p]  = (cs_stat_e[i_p]**2 + cs_syst_u_e[i_p]**2 + cs_syst_c_e[i_p]**2 + cs_BRs_e[i_p]**2)**0.5

        cs_tot.SetBinContent(i_p+1,cs_val[i_p])
        cs_tot.SetBinError(i_p+1,cs_tot_e[i_p])

    #cs_tot  = TGraphErrors(nPTBins, pt_val, cs_val, pt_err, cs_tot_e)
    cs_stat = TGraphErrors(nPTBins, pt_val, cs_val, pt_err, cs_stat_e)
    cs_ss   = TGraphErrors(nPTBins, pt_val, cs_val, pt_err, cs_ss_e)
    cs_val_new = np.array([cs_val[0],cs_val[-1]])
    cs_err_new = np.array([cs_tot_e[0],cs_tot_e[-1]])
    pt_val_new = np.array([pt_val[0],pt_val[-1]])
    pt_err_new = np.array([pt_err[0],pt_err[-1]])
    cs_tot_new  = TGraphErrors(2, pt_val_new, cs_val_new, pt_err_new, cs_err_new)

    return cs_tot, cs_stat, cs_ss, cs_tot_new


def draw_CS(rel=False, logsc=False):

    texCS = TLatex()
    texCS.SetNDC()

    #cs_theory_CO, cs_theory_CS = get_thPlot()
    if rel:
        cs_theory_mNRQCD = get_rel_CS("_mNRQCD_rel",False)
    else:
        cs_theory_mNRQCD = get_rel_CS("_mNRQCD",rel)
    cs_theory_CS = get_rel_CS("_CSM",rel)
    cs_theory_CO = get_rel_CS("_COM",rel)
    cs_theory = get_rel_CS("",rel)
    cs_tot, cs_stat, cs_ss, cs_tot_new = get_csMeas(rel)

    canv_pr = TCanvas("canv_pr","J/Psi_Mass",55,55,550,400)
    rangeL, rangeR  = 4.0, 21.0

    canv_pr.cd()
    if logsc:
        canv_pr.SetLogy()
    gPad.SetLeftMargin(0.20)

    fitCSEtacPr = TF1("fitCSEtacPr","[0]+[1]*x",5.0, 20.0)
    fitCSEtacPr.SetParameters(1e6,0.2)
    cs_tot.Fit(fitCSEtacPr,"IMER0")
    #sigmaJpsiPr.Fit(fitCSJpsiPr,"I0")

    cs_tot.Draw("E1")
    cs_stat.Draw("SAME 1")
    cs_ss.Draw("SAME 1")
    #fitCSEtacPr.Draw("SAME")
    #sigmaJpsiPr.Draw("SAME P")
    #fitCSJpsiPr.Draw("SAME")

    #cs_theory_CS.Draw("A3")
    cs_theory.Draw("SAME 2")
    # cs_theory_CO.Draw("SAME 2")
    cs_theory_CS.Draw("SAME 2")
    cs_theory_mNRQCD.Draw("SAME E1")

    cs_tot.SetLineWidth(2)
    cs_tot.SetLineColor(2)
    cs_tot.SetFillStyle(0)
    #cs_tot.SetFillColor(2)
    cs_tot.SetMarkerStyle(4)
    cs_tot.SetMarkerColor(2)
    cs_tot.SetMarkerSize(0)
    cs_tot.GetXaxis().SetTitle("p_{T}, GeV/c")
    if rel:
        cs_tot.SetMaximum(1.e1)
        cs_tot.SetMinimum(0.0)
        cs_tot.GetYaxis().SetTitle("#frac{#it{d#sigma_{#eta_{c}}}/ #it{dp_{T}} }{ #it{d#sigma_{J/#psi}}/ #it{dp_{T}}}")
        cs_tot.GetXaxis().SetRangeUser(4.0, 21.0)
    else:
        # canv_pr.SetLogy()
        # cs_tot.SetMaximum(1.e4)
        if logsc:
            cs_tot.SetMaximum(7.5e3)
        else:
            cs_tot.SetMaximum(4.5e3)
        cs_tot.SetMinimum(1.0)
        cs_tot.GetYaxis().SetTitle("#it{d#sigma_{#eta_{c}}}/%s [nb / GeV/c]"%("#it{dp}_{T}"))
        cs_tot.GetXaxis().SetRangeUser(4.0,15.0)
    
    # cs_tot.GetXaxis().SetLimits(rangeL,rangeR)
    cs_tot.GetYaxis().SetTitleOffset(1.2)
    cs_tot.GetYaxis().SetTitleSize(0.065)
    #cs_totStat.SetLineColor(2)
    #cs_totStat.SetMarkerStyle(4)
    #cs_totStat.SetMarkerColor(2)
    #cs_totSS.SetLineColor(2)
    #cs_totSS.SetMarkerStyle(4)
    #sigmaJpsiPr.SetLineWidth(2)
    #sigmaJpsiPr.SetLineColor(4)
    #sigmaJpsiPr.SetMarkerStyle(4)
    #sigmaJpsiPr.SetMarkerColor(4)
    fitCSEtacPr.SetLineColor(1)
    #fitCSJpsiPr.SetLineColor(4)

    cs_stat.SetLineColor(2)
    cs_stat.SetMarkerColor(2)
    cs_ss.SetLineColor(2)
    cs_ss.SetMarkerColor(2)
    cs_tot_new.SetLineColor(4)
    cs_tot_new.SetMarkerColor(4)
    cs_tot_new.SetMarkerSize(0)

    cs_theory_CO.SetLineColor(7)
    #cs_theory_CO.SetLineWidth(1100)
    #cs_theory_CO.SetFillColor(4)
    cs_theory_CO.SetFillColorAlpha(7, 0.3)
    cs_theory_CO.SetFillStyle(1002)
    cs_theory_CO.SetMarkerSize(1)

    cs_theory_CS.SetLineColor(6)
    cs_theory_CS.SetFillColorAlpha(6, 0.3)
    #cs_theory_CS.SetFillColor(3)
    cs_theory_CS.SetFillStyle(1002)
    cs_theory_CS.SetMarkerSize(1)

    cs_theory_mNRQCD.SetLineColor(1)
    cs_theory_mNRQCD.SetMarkerColor(1)
    cs_theory_mNRQCD.SetMarkerSize(1)
    cs_theory_mNRQCD.SetMarkerStyle(2)

    cs_theory.SetLineColor(8)
    cs_theory.SetFillColorAlpha(8, 0.3)
    cs_theory.SetFillStyle(1002)
    cs_theory.SetMarkerSize(1)

    #fitCSEtacPr.SetLineStyle(2)
    #fitCSJpsiPr.SetLineStyle(2)
    cs_tot.Draw("SAME 1")
    #cs_tot_new.Draw("SAME 1")

    leg1 = TLegend(0.36,0.25,0.65,0.45)
    if rel:
        leg1 = TLegend(0.24,0.75,0.50,0.91)
    else:
        if logsc:
            leg1 = TLegend(0.26,0.25,0.55,0.45)
        else:
            leg1  = TLegend(0.56,0.45,0.85,0.65)

    leg1.SetBorderSize(0)
    leg1.AddEntry(cs_tot,"LHCb measurement","lep")
    #leg1.AddEntry(cs_tot,"LHCb measurement","lep")
    #leg1.AddEntry(fitCSEtacPr,"Linear fit","l")
    leg1.AddEntry(cs_theory_CS,"NLO NRQCD CS","f")
    # leg1.AddEntry(cs_theory_CO,"NLO NRQCD CO","f")
    leg1.AddEntry(cs_theory,"NLO NRQCD CS+CO","f")
    leg1.AddEntry(cs_theory_mNRQCD,"Modified NRQCD","l")
    leg1.Draw()

    texCS.DrawLatex(0.60, 0.86, "LHCb preliminary")
    texCS.DrawLatex(0.60, 0.80, "#sqrt{s} = 13 TeV")
    texCS.DrawLatex(0.60, 0.74, "2.0 < y < 4.0")

    if rel: add="rel"
    else:   add="abs"
    if logsc: add = f"{add}_log"
    canv_pr.Draw()
    canv_pr.SaveAs("{}/results/CS/CSEtacPrompt_th_{}_test.pdf".format(homeDir,add))
    canv_pr.SaveAs("{}/results/CS/CSEtacPrompt_th_{}_test.root".format(homeDir,add))


if __name__=='__main__':
    gROOT.LoadMacro("../libs/lhcbStyle.C")
    # draw_CS(True)
    draw_CS(False, True)
