import numpy as np
from extrapolateJpsi import unc, yield2cs, uncPT
from ROOT import TFile, gROOT

gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../libs/libBifurcatedCB/BifurcatedCB.cxx+")
from ROOT import RooRelBreitWigner, BifurcatedCB

import logging

import sys
sys.path.insert(1,"../libs/")
from crosstalk import crosstalk

homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/"
CUTKEY = "_ProbNNp_06_etac2S"
# CUTKEY = "_ProbNNp_06"
# CUTKEY = "_5-14_ProbNNp_06"

BR = {"jpsi": {
        "val": 2.120e-3,
        "err": 0.029e-3},
    "psi2S": {
        "val": 2.94e-4,
        "err": 0.08e-4},
    "chic0": {
        "val": 2.21e-4,
        "err": 0.08e-4},
    "chic1": {
        "val": 7.60e-5,
        "err": 0.34e-5},
    "chic2": {
        "val": 7.33e-5,
        "err": 0.33e-5}
}

def getEff(state:str,norm:str="jpsi",src:str="prompt"):

    
    filename = ""
    if src=="fromB":
        if state != "hc":
            filename = "{0}/eff/plots_{1}/ratio_{3}2{2}_{1}_PT_tot.root".format(homeDir, "sec", norm, state)
        else:
            filename = "{0}/eff/plots_{1}/ratio_{3}2{2}_{1}_PT_tot.root".format(homeDir, "sec", norm, "chic1")
    else:
        if state != "hc":
            filename = "{0}/eff/plots_{1}/ratio_{3}2{2}_{1}_PT_tot.root".format(homeDir, "prompt", norm, state)
        else:
            filename = "{0}/eff/plots_{1}/ratio_{3}2{2}_{1}_PT_tot.root".format(homeDir, "prompt", norm, "chic1")


    feff = TFile(filename,"READ")
    eff_val = 1./feff.Get("eff_val").GetVal()
    eff_err = feff.Get("eff_err").GetVal()*eff_val**2
    feff.Close()

    logging.debug(f"state: {state}, eff_v = {eff_val:.3f}, eff_e = {eff_err:.3f}")
    return eff_val, eff_err

def getEffAbs(state:str,src:str="prompt"):

    
    filename = ""
    if src=="fromB":
        if state != "hc":
            filename = "{0}/eff/plots_{1}/eff_{2}_{1}_PT_tot.root".format(homeDir, "sec", state)
        else:
            filename = "{0}/eff/plots_{1}/eff_{2}_{1}_PT_tot.root".format(homeDir, "sec", "chic1")
    else:
        if state != "hc":
            filename = "{0}/eff/plots_{1}/eff_{2}_{1}_PT_tot.root".format(homeDir, "prompt", state)
        else:
            filename = "{0}/eff/plots_{1}/eff_{2}_{1}_PT_tot.root".format(homeDir, "prompt", "chic1")


    feff = TFile(filename,"READ")
    eff_val = feff.Get("eff_val").GetVal()
    eff_err = feff.Get("eff_err").GetVal()
    feff.Close()

    logging.debug(f"state: {state}, eff_v = {eff_val:.3f}, eff_e = {eff_err:.3f}")
    return eff_val, eff_err


def calcTrueYieldCT(state:str="psi2S", norm:str="jpsi", syst:str="Base"):

    nameWksp =  f"{homeDir}/mass_fit/one_hist/CB/Wksp_{norm}_sim_{syst}{CUTKEY}.root"
    file_w = TFile(nameWksp)
    w = file_w.Get("w").Clone()
    file_w.Close()


    src = "prompt"
    n1_pr_val = w.var(f"n_{state}_{src}").getVal() 
    n1_pr_err = w.var(f"n_{state}_{src}").getError()
    n2_pr_val = w.var(f"n_{norm}_{src}").getVal() 
    n2_pr_err = w.var(f"n_{norm}_{src}").getError()

    src = "fromB"
    n1_fb_val = w.var(f"n_{state}_{src}").getVal() 
    n1_fb_err = w.var(f"n_{state}_{src}").getError()
    n2_fb_val = w.var(f"n_{norm}_{src}").getVal() 
    n2_fb_err = w.var(f"n_{norm}_{src}").getError()


    effPP, effPB, effBP, effBB, _, _, _, _  = crosstalk(state=state,cutkey=CUTKEY)    

    N1_pr_val = (n1_pr_val*effBB - n1_fb_val*effBP) / (effPP*effBB - effPB*effBP)
    N1_fb_val = (n1_fb_val*effPP - n1_pr_val*effPB) / (effPP*effBB - effPB*effBP)

    N1_pr_err = ((n1_pr_err/n1_pr_val)**2 + (n1_fb_err/n1_fb_val)**2)**0.5
    N1_fb_err = ((n1_fb_err/n1_fb_val)**2 + (n1_pr_err/n1_pr_val)**2)**0.5

    effPP, effPB, effBP, effBB, _, _, _, _  = crosstalk(state=norm,cutkey=CUTKEY)    

    N2_pr_val = (n2_pr_val*effBB - n2_fb_val*effBP) / (effPP*effBB - effPB*effBP)
    N2_fb_val = (n2_fb_val*effPP - n2_pr_val*effPB) / (effPP*effBB - effPB*effBP)

    N2_pr_err = ((n2_pr_err/n2_pr_val)**2 + (n2_fb_err/n2_fb_val)**2)**0.5
    N2_fb_err = ((n2_fb_err/n2_fb_val)**2 + (n2_pr_err/n2_pr_val)**2)**0.5

    # NRel_pr = (n1_pr_val*effBB - n1_fb_val*effBP) / (n2_pr_val*effBB - n2_fb_val*effBP)
    # NRel_fb = (n1_fb_val*effPP - n1_pr_val*effPB) / (n2_fb_val*effPP - n2_pr_val*effPB)

    # dNRel_pr = NRel_pr * ( ((n1_pr_err*effBB)**2 + (n1_fb_err*effBP)**2)/(n1_pr_val*effBB - n1_fb_val*effBP)**2 + ((n2_pr_err*effBB)**2 + (n2_fb_err*effBP)**2)/(n2_pr_val*effBB - n2_fb_val*effBP)**2 )**0.5
    # dNRel_fb = NRel_fb * ( ((n1_fb_err*effPP)**2 + (n1_pr_err*effPB)**2)/(n1_fb_val*effPP - n1_pr_val*effPB)**2 + ((n2_fb_err*effPP)**2 + (n2_pr_err*effPB)**2)/(n2_fb_val*effPP - n2_pr_val*effPB)**2 )**0.5

    NRel_pr = N1_pr_val/N2_pr_val
    NRel_fb = N1_fb_val/N2_fb_val

    dNRel_pr = NRel_pr * ( (N1_pr_err)**2 + (N2_pr_err)**2 )**0.5
    dNRel_fb = NRel_fb * ( (N1_fb_err)**2 + (N2_fb_err)**2 )**0.5
    
    logging.debug(f"n1_pr_val = {n1_pr_val:.1f} +- {n1_pr_err:.1f}")
    logging.debug(f"n2_pr_val = {n2_pr_val:.1f} +- {n2_pr_err:.1f}")
    logging.debug(f"n1_fb_val = {n1_fb_val:.1f} +- {n1_fb_err:.1f}")
    logging.debug(f"n2_fb_val = {n2_fb_val:.1f} +- {n2_fb_err:.1f}")

    logging.debug(f"N1_pr_val = {N1_pr_val:.1f} +- {N1_pr_err*N1_pr_val:.1f}")
    logging.debug(f"N2_pr_val = {N2_pr_val:.1f} +- {N2_pr_err*N2_pr_val:.1f}")
    logging.debug(f"N1_fb_val = {N1_fb_val:.1f} +- {N1_fb_err*N1_fb_val:.1f}")
    logging.debug(f"N2_fb_val = {N2_fb_val:.1f} +- {N2_fb_err*N2_fb_val:.1f}")

    return NRel_pr, NRel_fb, dNRel_pr, dNRel_fb


def calcTrueYield(state:str="psi2S", norm:str="jpsi", syst:str="Base"):

    nameWksp =  f"{homeDir}/mass_fit/one_hist/CB/Wksp_{norm}_sim_{syst}{CUTKEY}.root"
    file_w = TFile(nameWksp)
    w = file_w.Get("w").Clone()
    file_w.Close()

    src = "prompt"
    n1_pr_val = w.var(f"n_{state}_{src}").getVal() 
    n1_pr_err = w.var(f"n_{state}_{src}").getError()
    n2_pr_val = w.var(f"n_{norm}_{src}").getVal() 
    n2_pr_err = w.var(f"n_{norm}_{src}").getError()

    src = "fromB"
    n1_fb_val = w.var(f"n_{state}_{src}").getVal() 
    n1_fb_err = w.var(f"n_{state}_{src}").getError()
    n2_fb_val = w.var(f"n_{norm}_{src}").getVal() 
    n2_fb_err = w.var(f"n_{norm}_{src}").getError()


    effPP, effPB, effBP, effBB, _, _, _, _  = crosstalk(state=state,cutkey=CUTKEY)    

    N1_pr_val = (n1_pr_val) / (effPP)
    N1_fb_val = (n1_fb_val) / (effBB)

    N1_pr_err = (n1_pr_err/n1_pr_val)
    N1_fb_err = (n1_fb_err/n1_fb_val)

    effPP, effPB, effBP, effBB, _, _, _, _  = crosstalk(state=norm,cutkey=CUTKEY)    

    N2_pr_val = (n2_pr_val) / (effPP)
    N2_fb_val = (n2_fb_val) / (effBB)

    N2_pr_err = (n2_pr_err/n2_pr_val)
    N2_fb_err = (n2_fb_err/n2_fb_val)

    # NRel_pr = (n1_pr_val*effBB - n1_fb_val*effBP) / (n2_pr_val*effBB - n2_fb_val*effBP)
    # NRel_fb = (n1_fb_val*effPP - n1_pr_val*effPB) / (n2_fb_val*effPP - n2_pr_val*effPB)

    # dNRel_pr = NRel_pr * ( ((n1_pr_err*effBB)**2 + (n1_fb_err*effBP)**2)/(n1_pr_val*effBB - n1_fb_val*effBP)**2 + ((n2_pr_err*effBB)**2 + (n2_fb_err*effBP)**2)/(n2_pr_val*effBB - n2_fb_val*effBP)**2 )**0.5
    # dNRel_fb = NRel_fb * ( ((n1_fb_err*effPP)**2 + (n1_pr_err*effPB)**2)/(n1_fb_val*effPP - n1_pr_val*effPB)**2 + ((n2_fb_err*effPP)**2 + (n2_pr_err*effPB)**2)/(n2_fb_val*effPP - n2_pr_val*effPB)**2 )**0.5

    NRel_pr = N1_pr_val/N2_pr_val
    NRel_fb = N1_fb_val/N2_fb_val

    dNRel_pr = NRel_pr * ( (N1_pr_err)**2 + (N2_pr_err)**2 )**0.5
    dNRel_fb = NRel_fb * ( (N1_fb_err)**2 + (N2_fb_err)**2 )**0.5
    
    logging.debug(f"n1_pr_val = {n1_pr_val:.1f} +- {n1_pr_err:.1f}")
    logging.debug(f"n2_pr_val = {n2_pr_val:.1f} +- {n2_pr_err:.1f}")
    logging.debug(f"n1_fb_val = {n1_fb_val:.1f} +- {n1_fb_err:.1f}")
    logging.debug(f"n2_fb_val = {n2_fb_val:.1f} +- {n2_fb_err:.1f}")

    logging.debug(f"N1_pr_val = {N1_pr_val:.1f} +- {N1_pr_err*N1_pr_val:.1f}")
    logging.debug(f"N2_pr_val = {N2_pr_val:.1f} +- {N2_pr_err*N2_pr_val:.1f}")
    logging.debug(f"N1_fb_val = {N1_fb_val:.1f} +- {N1_fb_err*N1_fb_val:.1f}")
    logging.debug(f"N2_fb_val = {N2_fb_val:.1f} +- {N2_fb_err*N2_fb_val:.1f}")

    return NRel_pr, NRel_fb, dNRel_pr, dNRel_fb

def getCS(state:str="psi2S", syst:str="Base"):

    norm = "jpsi"
    logging.info(f"Calculating CS for {state} relative to {norm}")
        
    # n1_pr_val, n1_fb_val, n1_pr_err, n1_fb_err = calcTrueYieldCT(state=state, norm=norm, syst=syst)
    n1_pr_val, n1_fb_val, n1_pr_err, n1_fb_err = calcTrueYield(state=state, norm=norm, syst=syst)

    n1 = unc()
    n2 = unc()
    cs = unc()

    brjpsi = unc()
    brjpsi.d["val"] = BR[norm]["val"]
    brjpsi.d["stat"] = BR[norm]["err"]

    brpsi2S = unc()
    # to calculate BR(b->chicX)
    brpsi2S.d["val"] = BR[state]["val"]
    brpsi2S.d["stat"] = BR[state]["err"]
    # # to calculate BR(b->chicX)xBR(chicX->ppbar)
    # brpsi2S.d["val"] = 1.0e-5
    # brpsi2S.d["stat"] = 0.0

    src = "prompt"
    eff_v, eff_e = getEff(state=state, norm=norm, src=src)

    cs.d["val"], cs.d["stat"], cs.d["uncorr"], cs.d["corr"] = 1372.950, 4.908, 7.232, 57.091
    cs.calculateErrors()
    n2.d["val"], n2.d["stat"], n2.d["uncorr"], n2.d["corr"] = 1.0, 0., 0., 0.
    n1.d["val"], n1.d["stat"], n1.d["uncorr"], n1.d["corr"] = n1_pr_val, n1_pr_err, 0., 0.
    cs_calc = yield2cs(n1, n2, brjpsi, brpsi2S, cs, eff_v, eff_e).calcCS()
    cs_calc()

    # calculate CS from yield
    src = "fromB"
    eff_v, eff_e = getEff(state=state, norm=norm, src=src)
    cs.d["val"], cs.d["stat"], cs.d["uncorr"], cs.d["corr"] = 366.600, 2.510, 2.804, 19.782 
    cs.calculateErrors()
    n2.d["val"], n2.d["stat"], n2.d["uncorr"], n2.d["corr"] = 1.0, 0., 0., 0.
    n1.d["val"], n1.d["stat"], n1.d["uncorr"], n1.d["corr"] = n1_fb_val, n1_fb_err, 0., 0.
    cs_calc = yield2cs(n1, n2, brjpsi, brpsi2S, cs, eff_v, eff_e).calcCS()
    cs_calc()

    # calculate BR from yield
    cs.d["val"], cs.d["stat"], cs.d["uncorr"], cs.d["corr"] = 1.16, 0.01, 0., 0. # b->JpsiX BR; to calculate BR(b->chicX)
    cs.calculateErrors()
    cs_calc = yield2cs(n1, n2, brjpsi, brpsi2S, cs, eff_v, eff_e).calcCS()
    for key, val in cs_calc.d.items():
        print(f"{key} = {val:.9f}")
    # cs_calc()


def calcTrueYieldJpsi(state:str="jpsi", syst:str="Base"):

    # nameWksp =  f"{homeDir}/mass_fit/one_hist/CB/Wksp_{state}_sim_{syst}{CUTKEY}.root"
    nameWksp =  f"{homeDir}/mass_fit/etac/CB/Wksp_PT0_C{syst}{CUTKEY}.root"
    file_w = TFile(nameWksp)
    w = file_w.Get("w").Clone()
    file_w.Close()

    # src = "prompt"
    # n1_pr_val = w.var(f"n_{state}_{src}").getVal() 
    # n1_pr_err = w.var(f"n_{state}_{src}").getError()

    # src = "fromB"
    # n1_fb_val = w.var(f"n_{state}_{src}").getVal() 
    # n1_fb_err = w.var(f"n_{state}_{src}").getError()

    src = "Prompt"
    n1_pr_val = w.var(f"n_{state}_PT0_{src}").getVal() 
    n1_pr_err = w.var(f"n_{state}_PT0_{src}").getError()

    src = "FromB"
    n1_fb_val = w.var(f"n_{state}_PT0_{src}").getVal() 
    n1_fb_err = w.var(f"n_{state}_PT0_{src}").getError()


    effPP, effPB, effBP, effBB, _, _, _, _  = crosstalk(state=state,cutkey=CUTKEY)    

    N1_pr_val = (n1_pr_val*effBB - n1_fb_val*effBP) / (effPP*effBB - effPB*effBP)
    N1_fb_val = (n1_fb_val*effPP - n1_pr_val*effPB) / (effPP*effBB - effPB*effBP)

    N1_pr_err = ((n1_pr_err/n1_pr_val)**2 + (n1_fb_err/n1_fb_val)**2)**0.5*N1_pr_val
    N1_fb_err = ((n1_fb_err/n1_fb_val)**2 + (n1_pr_err/n1_pr_val)**2)**0.5*N1_fb_val

    logging.debug(f"n1_pr_val = {n1_pr_val:.1f} +- {n1_pr_err:.1f}")
    logging.debug(f"n1_fb_val = {n1_fb_val:.1f} +- {n1_fb_err:.1f}")

    logging.debug(f"N1_pr_val = {N1_pr_val:.1f} +- {N1_pr_err*N1_pr_val:.1f}")
    logging.debug(f"N1_fb_val = {N1_fb_val:.1f} +- {N1_fb_err*N1_fb_val:.1f}")

    return N1_pr_val, N1_fb_val, N1_pr_err, N1_fb_err

def getCSAbs(syst:str="Base"):

    state = "jpsi"
    logging.info(f"Calculating CS for {state} from luminosity and {state} yield")
        
    n1_pr_val, n1_fb_val, n1_pr_err, n1_fb_err = calcTrueYieldJpsi(state=state, syst=syst)
    lumi = 2.1e6 # nb-1, approx. 2018

    brjpsi = unc()
    brjpsi.d["val"] = BR[state]["val"]
    brjpsi.d["stat"] = BR[state]["err"]

    src = "prompt"
    eff_v, eff_e = getEffAbs(state=state, src=src)
    logging.debug(f"Efficiency for {state} {src} = {eff_v:.3f} +- {eff_e:.3f}")

    # calculate CS from yield
    cs_val = n1_pr_val/lumi/eff_v/brjpsi.d["val"]
    cs_err = cs_val*((n1_pr_err/n1_pr_val)**2 + (eff_e/eff_v)**2 + (brjpsi.d["stat"]/brjpsi.d["val"])**2)**0.5
    logging.info(f"CS prompt = {cs_val:.3f} +- {cs_err:.3f} nb")

    # calculate CS from yield
    src = "fromB"
    eff_v, eff_e = getEffAbs(state=state, src=src)
    logging.debug(f"Efficiency for {state} {src} = {eff_v:.3f} +- {eff_e:.3f}")
    cs_val = n1_fb_val/lumi/eff_v/brjpsi.d["val"]
    cs_err = cs_val*((n1_fb_err/n1_fb_val)**2 + (eff_e/eff_v)**2 + (brjpsi.d["stat"]/brjpsi.d["val"])**2)**0.5
    logging.info(f"CS fromB = {cs_val:.3f} +- {cs_err:.3f} nb")



def calcBR():

    brjpsi = unc()
    brjpsi.d["val"] = 1.16e-2
    brjpsi.d["stat"] = 0.10-2

    bretac = unc()
    cs = unc()

if __name__=="__main__":
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

    # getCS(syst="Base")
    getCS(state="chic0",syst="Base")
    getCS(state="chic1",syst="Base")
    getCS(state="chic2",syst="Base")
    # getCSAbs(syst="Base")
    # calcBR()
    