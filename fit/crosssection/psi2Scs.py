import numpy as np
from extrapolateJpsi import uncPT

def readCS(src:str, bins:str="PT"):
    pt_l = np.array([])
    pt_h = np.array([])
    cs_val = np.array([])
    cs_err_st = np.array([]) 
    cs_err_ss = np.array([]) 
    cs_err_br = np.array([]) 
    with open("psi2SCS_{}.txt".format(src),"r") as f:
        for line in f:
            if line.strip()==bins:
                break
        for line in f:
            if "-" in line.strip():
                break
            l = line.strip().split()
            pt_l = np.append(pt_l,float(l[0]))
            pt_h = np.append(pt_h,float(l[1]))
            cs_val = np.append(cs_val,float(l[2]))
            cs_err_st = np.append(cs_err_st,float(l[3]))
            cs_err_ss = np.append(cs_err_ss,float(l[4]))
            cs_err_br = np.append(cs_err_br,float(l[5]))

    return pt_l, pt_h, cs_val, cs_err_st, cs_err_ss, cs_err_br


def compute_sum(src:str, bins:str, bmin:float, bmax:float):

    pt_min, pt_max, cs_val, cs_err_st, cs_err_ss, cs_err_br = readCS(src, bins)
    l = np.where(pt_min==bmin)[0][0]
    h = np.where(pt_max==bmax)[0][0]+1
    cs_tot_val = np.sum(cs_val[l:h])
    cs_tot_st  = np.sum((cs_err_st[l:h]/cs_val[l:h])**2)**0.5*cs_tot_val
    cs_tot_ss  = np.sum((cs_err_ss[l:h]/cs_val[l:h])**2)**0.5*cs_tot_val
    cs_tot_br  = np.sum((cs_err_br[l:h]/cs_val[l:h])**2)**0.5*cs_tot_val

    print(f"Integrated CS for {src} psi2S in range [{bmin}-{bmax}]:")
    # print(f"{cs_tot_val:5.3f} \u00B1 {cs_tot_st:5.3f} \u00B1 {cs_tot_ss:5.3f} \u00B1 {cs_tot_br:5.3f}")
    print(f"{cs_tot_val:5.3f} \\pm {cs_tot_st:5.3f} \\pm {cs_tot_ss:5.3f} \\pm {cs_tot_br:5.3f}")


if __name__ == '__main__':
    '''
    Correct code to calculate psi2S CS in bins of PT
    For Y needs computation in bins of PT
    '''
    compute_sum("prompt", "PT", 5, 20)
    compute_sum("fromB", "PT", 5, 20)
    compute_sum("prompt", "PT", 5, 14)
    compute_sum("fromB", "PT", 5, 14)
    # compute_sum("prompt", "Y", 2., 4.)
    # compute_sum("fromB", "Y", 2., 4.)