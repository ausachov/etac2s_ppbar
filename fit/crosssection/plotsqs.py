from ROOT import gStyle, gROOT, gPad, TGraphErrors, TLatex, TLegend, TCanvas

from array import array


en_jpsi     = array("d", [2.76, 7., 8., 12.9, 13.])
en_jpsi_err = array("d", 5*[0])
en_etac     = array("d", [7., 8., 12.9, 13.])
en_etac_err = array("d", 4*[0])

#homeDir = "../"
homeDir = "/users/LHCb/zhovkovska/scripts/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"


def plotsqs():
    
    npetac = len(en_jpsi)
    npjpsi = len(en_etac)


    #TGaxis.SetMaxDigits(4)
    gStyle.SetPaintTextFormat("3.1f")
    gStyle.SetStripDecimals(False)

    # CSJpsi          = array("d", [0.0393, 0.2839, 0.3714, 0.7490, 0.5620])
    # CSJpsiErrStat   = array("d", [0.0039, 0.0017, 0.0014, 0.0238, 0.0026])
    # CSJpsiErrSyst   = array("d", [0.0049, 0.0153, 0.0272, 0.0188, 0.0237])
    CSJpsi          = array("d", [0.0393, 0.2839, 0.3714, 0.5620, 0.5620])
    CSJpsiErrStat   = array("d", [0.0039, 0.0017, 0.0014, 0.0026, 0.0026])
    CSJpsiErrSyst   = array("d", [0.0049, 0.0153, 0.0272, 0.0237, 0.0237])
    CSJpsiErr       = array("d", 5*[0])


    CSEtacRel          = array("d", [1.74, 1.60, 1.69, 1.22])
    CSEtacRelErrStat   = array("d", [0.29, 0.29, 0.15, 0.14])
    CSEtacRelErrSyst   = array("d", [0.28, 0.25, 0.10, 0.09])
    CSEtacRelErrBR     = array("d", [0.18, 0.17, 0.18, 0.17])
    CSEtacRelErr       = array("d", npetac*[0])
    CSEtacRelErrTot    = array("d", npetac*[0])

    CSEtac             = array("d", [0.52, 0.59, 0.94, 0.75])
    CSEtacErrStat      = array("d", [0.09, 0.11, 0.07, 0.08])
    CSEtacErrSyst      = array("d", [0.08, 0.09, 0.04, 0.04])
    CSEtacErrBR        = array("d", [0.06, 0.08, 0.06, 0.05])
    CSEtacErr          = array("d", npetac*[0])
    CSEtacErrTot       = array("d", npetac*[0])



    for i in range(len(CSEtac)):

        CSEtacErr[i]    = ((CSEtacErrStat[i])**2 + (CSEtacErrSyst[i])**2)**0.5
        CSEtacRelErr[i] = ((CSEtacRelErrStat[i])**2 + (CSEtacRelErrSyst[i])**2)**0.5

        CSJpsiErr[i]       = ((CSJpsiErrStat[i])**2 + (CSJpsiErrSyst[i])**2)**0.5

        CSEtacErrTot[i]    = ((CSEtacErr[i])**2    + (CSEtacErrBR[i])**2)**0.5
        CSEtacRelErrTot[i] = ((CSEtacRelErr[i])**2 +(CSEtacRelErrBR[i])**2)**0.5

    sigma          = TGraphErrors(npetac,en_etac,CSEtac,en_etac_err,CSEtacErr)
    sigmaStat      = TGraphErrors(npetac,en_etac,CSEtac,en_etac_err,CSEtacErrStat)
    sigmaErrTot    = TGraphErrors(npetac,en_etac,CSEtac,en_etac_err,CSEtacErrTot)

    sigmaJpsiStat  = TGraphErrors(npjpsi,en_jpsi,CSJpsi,en_jpsi_err,CSJpsiErrStat)
    sigmaJpsi      = TGraphErrors(npjpsi,en_jpsi,CSJpsi,en_jpsi_err,CSJpsiErr)


    sigmaRel       = TGraphErrors(npetac,en_etac,CSEtacRel,en_etac_err,CSEtacRelErr)
    sigmaRelStat   = TGraphErrors(npetac,en_etac,CSEtacRel,en_etac_err,CSEtacRelErrStat)
    sigmaRelErrTot = TGraphErrors(npetac,en_etac,CSEtacRel,en_etac_err,CSEtacRelErrTot)


    texCS = TLatex()
    texCS.SetNDC()

    leg1 = TLegend(0.30,0.53,0.4,0.67)
    leg1.SetBorderSize(0)
    leg1.AddEntry(sigma," #it{#eta_{c}}","lep")
    leg1.AddEntry(sigmaJpsi," #it{J/#psi}","lep")

    canv = TCanvas("canv","CS",55,55,500,400)
    gPad.SetLeftMargin(0.2)

    sigma.Draw("AP")
    sigmaStat.Draw("SAME P")
    sigmaErrTot.Draw("SAME P")
    sigmaJpsi.Draw("SAME P")
    sigmaJpsiStat.Draw("SAME P")
    #sigma.SetMarkerStyle(2)
    sigma.GetYaxis().SetRangeUser(0.0, 1.8)
    sigma.GetXaxis().SetLimits(0., 14)
    #sigma.GetYaxis().SetLimits(0.0, 2.0)
    sigma.GetXaxis().SetTitle("#it{#sqrt{s}}  [TeV]")
    sigma.GetYaxis().SetTitle("#it{#sigma_{#eta_{c}}}  [#mub]")
    sigmaJpsi.SetMarkerStyle(4)
    sigmaJpsi.SetMarkerColor(4)
    sigmaJpsi.SetMarkerSize(1)
    sigmaJpsi.SetLineColor(4)
    sigmaJpsiStat.SetMarkerStyle(4)
    sigmaJpsiStat.SetMarkerSize(1)
    sigmaJpsiStat.SetMarkerColor(4)
    sigmaJpsiStat.SetLineColor(4)
    leg1.Draw()

    # texCS.DrawLatex(0.3, 0.81,"LHCb ")
    # texCS.DrawLatex(0.3, 0.75,"6.5 < #it{p}_{T} < 14.0 GeV")
    # texCS.DrawLatex(0.3, 0.69,"2.0 < #it{y} < 4.5")

    canv.SaveAs(f"{homeDir}/results/CS/CSSqs.pdf")

    canvRel = TCanvas("canvRel","CS",55,55,500,400)
    gPad.SetLeftMargin(0.2)

    sigmaRel.Draw("AP")
    sigmaRelStat.Draw("SAME P")
    sigmaRelErrTot.Draw("SAME P")
    sigmaRel.SetMarkerStyle(2)
    sigmaRel.GetYaxis().SetRangeUser(0.0, 2.5)
    sigmaRel.GetXaxis().SetLimits(0., 14)
    #sigma.GetYaxis().SetLimits(0.0, 2.0)
    sigmaRel.GetXaxis().SetTitle("#it{#sqrt{s}}  [TeV]")
    sigmaRel.GetYaxis().SetTitle("#it{#sigma_{#eta_{c}}}/#it{#sigma_{J/#psi}}")
    texCS.DrawLatex(0.3, 0.41,"LHCb ")
    texCS.DrawLatex(0.3, 0.35,"6.5 < #it{p}_{T} < 14.0 GeV")
    texCS.DrawLatex(0.3, 0.29,"2.0 < #it{y} < 4.5")

    canvRel.SaveAs(f"{homeDir}/results/CS/CSRelSqs.pdf")

if __name__=="__main__":

    gROOT.LoadMacro("../libs/lhcbStyle.C")
    plotsqs()