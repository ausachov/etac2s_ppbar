from extrapolateJpsi import *
from cstools import uncPT

from ROOT import TF1, TH1F, TFile, TCanvas, TLatex, TGraphErrors, TLegend
from ROOT import gStyle, gPad, gROOT
from ROOT import kBlue, kFullCircle, kOpenCircle
from array import array
import logging

from cstools import uncPT

#homeDir = "../"
# homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/"

NB = { 
    "PT" : 6,
    # "PT" : 5,
    # "PT" : 4,
    "Y"  : 4,
    "TOT": 1    
}
BHW = { 
    "PT" : array("d", [0.75, 0.75, 1.0, 1.0, 1.0, 3.0]),
    # "PT" : array("d", [0.75, 0.75, 1.0, 1.0, 1.0]),
    # "PT" : array("d", [0.75, 1.0, 1.0, 1.0]),
    "Y"  : array("d", [0.25, 0.25, 0.25, 0.25]),
    "TOT": array("d",[0.5])
}
BINNING = { 
    "PT" : array("d", [5.0, 6.5, 8.0, 10.0, 12.0, 14.0, 20.0]),
    # "PT" : array("d", [5.0, 6.5, 8.0, 10.0, 12.0, 14.0]),
    # "PT" : array("d", [6.5, 8.0, 10.0, 12.0, 14.0]),
    "Y"  : array("d", [2.0, 2.5, 3.0, 3.5, 4.0]),
    "TOT": array("d",[0.0, 1.0])
}
L   = 2.0
#eff = 0.99
#effErr  = 0.02
eff = 0.907
effErr  = 0.008
PTBins  = array("d", [5.75, 7.25, 9.0, 11.0, 13.0, 17.0])
# PTBins  = array("d", [7.25, 9.0, 11.0, 13.0, 17.0])
PTBinHW = array("d", [0.75, 0.75, 1.0, 1.0, 1.0, 3.0])
# ptBinning = array("d", [5.0, 6.5, 8.0, 10.0, 12.0, 14.0, 20.0])
#PTBins  = array("d", [2.25, 2.75, 3.25, 3.75])
YBinHW = array("d", [0.25, 0.25, 0.25, 0.25])
yBinning = array("d", [2.0, 2.5, 3.0, 3.5, 4.0])

# BREtacToPP    = 1.44e-3 # PDG 2022
BREtacToPP    = 1.45e-3
BREtacToPPErr = 0.14e-3

# BRJpsiToPP    = 2.120e-3 # PDG 2022
BRJpsiToPP    = 2.121e-3
BRJpsiToPPErr = 0.029e-3

errBRrel = ((BRJpsiToPPErr/BRJpsiToPP)**2 + (BREtacToPPErr/BREtacToPP)**2)**0.5 # 0.098

rangeL, rangeR  = 5.0, 20.
#rangeL, rangeR  = 2.0, 4.0

mcDir = "../MC/tuples/selected/"

title_dict = {
    "PT"   : ["#it{dp}_{T}", "2.0 < #it{y} < 4.0",      "[nb / GeV/c]", "#it{p}_{T}  [GeV/c]"],
    "Y"    : ["#it{dy}",     "5.0 < #it{p}_{T} < 20.0 GeV/c", "[nb]",       "#it{y}"],
    "TOT"  : ["#sqrt{s}",    "5.0 < #it{p}_{T} < 20.0 GeV/c \n 2.0 < #it{y} < 4.0", "[nb]", "#sqrt{s}"],
}


def getJpsi(bins:str="PT",total:bool=False):

    nBins = NB[bins]
    BinHW = BHW[bins]

    CSJpsiPrD  = uncPT(nBins)
    CSJpsiFbD = uncPT(nBins)

    fiS = open(f"{homeDir}/CSJpsi_{bins}_Y4.txt","r")
    # fiS = open(f"{homeDir}/CSJpsi_{bins}_PT4.txt","r")
    # fiS = open(f"{homeDir}/CSJpsi_{bins}.txt","r")
    for iB in range(nBins):
        i=0
        line = fiS.readline()
        for key in ["val","stat","uncorr","corr","tot"]:
            CSJpsiPrD.d[key][iB] = float(line.split()[i])/(2*BinHW[iB])
            i+=1
        CSJpsiPrD.calculateErrors(iB)

    line = fiS.readline()
    for iB in range(nBins):
        i=0
        line = fiS.readline()
        for key in ["val","stat","uncorr","corr","tot"]:
            CSJpsiFbD.d[key][iB] = float(line.split()[i])/(2*BinHW[iB])
            i+=1
        CSJpsiFbD.calculateErrors(iB)
    fiS.close()

    CSJpsiPrD.calculateTotalCS(nBins,BinHW)
    CSJpsiFbD.calculateTotalCS(nBins,BinHW)

    if total:
        CSJpsiPrTot  = uncPT(1)
        CSJpsiFbTot = uncPT(1)
        CSJpsiPrTot[0] = CSJpsiPrD.IntegralXsec
        CSJpsiFbTot[0] = CSJpsiFbD.IntegralXsec
        return CSJpsiPrTot, CSJpsiFbTot
    else:
        return CSJpsiPrD, CSJpsiFbD


def printCS(CSList:uncPT, nBins:int, TypePrefix:str):

    print('-------------------------------------------')
    # print(f'prompt ratio    {CSList["RelPr"].getCS()}')
    # print(f'fromB ratio     {CSList["RelFb"].getCS()}')
    # print(f'prompt eta_c    {CSList["EtacPr"].getCS()} nb')
    # print(f'fromB eta_c     {CSList["EtacFb"].getCS()} nb')
    # print(f'prompt J/psi    {CSList["JpsiPr"].getCS()} nb')
    # print(f'prompt J/psi    {CSList["JpsiFb"].getCS()} nb')

    fo = open(f"{homeDir}/CS/CSTotal_new_{TypePrefix}.txt","w")

    fo.write("Etac Prompt dSigma/dPT [nb/ GeV] \n")
    for iBin in range(nBins):
        # fo.write( f'{CSList["EtacPr"].getCS(iBin,["val","stat","uncorr","corr","norm"])} \n' )
        fo.write( f'{CSList["EtacPr"].getCS(iBin,["val","tot"])} \n' )

    fo.write("\n Jpsi Prompt dSigma/dPT [nb/ GeV] (ANA 2015) \n")
    for iBin in range(nBins):
        fo.write( f'{CSList["JpsiPr"].getCS(iBin,["val","stat","uncorr","corr"])} \n' )

    fo.write("\n Etac from-b dSigma/dPT [nb/ GeV] \n")
    for iBin in range(nBins):
        fo.write( f'{CSList["EtacFb"].getCS(iBin,["val","stat","uncorr","corr","norm"])} \n' )

    fo.write("\n Jpsi from-b dSigma/dPT [nb/ GeV] (ANA 2015) \n")
    for iBin in range(nBins):
        fo.write( f'{CSList["JpsiFb"].getCS(iBin,["val","stat","uncorr","corr"])} \n' )

    fo.write("\n Etac Rel Prompt dSigma/dPT [nb/ GeV] \n")
    for iBin in range(nBins):
        fo.write( f'{CSList["RelPr"].getCS(iBin,["val","stat","uncorr","corr","norm"])} \n' )

    fo.write("\n Etac Rel from-b dSigma/dPT [nb/ GeV] \n")
    for iBin in range(nBins):
        fo.write( f'{CSList["RelFb"].getCS(iBin,["val","stat","uncorr","corr","norm"])} \n' )
    fo.close()



def createHisto(hName:str = "", vals:list=[], errors:list=[], bins:str="PT"):
    nBins = NB[bins]
    hist = TH1F(hName,hName,nBins,BINNING[bins])
    for ii in range(nBins):
        hist.SetBinContent(ii+1, vals[ii])
        hist.SetBinError(ii+1, errors[ii])
    return hist

def get_eff(bins:str="PT", source:str="prompt"):

    if bins!="Y":
        bins="PT"
    # if bins=="Y":
    #     nBins = NB[bins]
    # else:
    #     bins="PT"
    #     nBins = NB["PT"]
    nBins = NB[bins]

    eff_v = array("f",(nBins+1)*[0.])
    eff_e = array("f",(nBins+1)*[0.])
    # eff_v = np.zeros(nBins)
    # eff_e = np.zeros(nBins)
    # f = TFile("{}/CS/eff_total_{}.root".format(homeDir, bins), "READ")
    f = TFile("{homeDir}/eff/plots_{src}/ratio_jpsi2etac_{src}_{bins}_tot.root".format(homeDir=homeDir, src=source, bins=bins), "READ")
    eff_v[0] = f.Get("eff_val").GetVal()
    # eff_e[0] = f.Get("eff_err").GetVal()
    eff_e[0] = 0.
    hh = f.Get("hist_tot")
    for iB in range(1,nBins+1):
        eff_v[iB] = hh.GetBinContent(iB)
        # eff_e[iB] = hh.GetBinError(iB)
        eff_e[iB] = 0.
        # eff_v[iB] = eff
        # eff_e[iB] = effErr


    return eff_v, eff_e

def crosssection(CSfilename:str="rel_PT_sim.txt", bins:str="PT", total:bool=False):

    nBins = NB[bins]
    BinHW = BHW[bins]
    
    if total:
        nBins = 1
        BinHW = array("d", [0.5])
        
    CSList = {}

    NJpsiPrDict  = uncPT(nBins)
    NJpsiFbDict = uncPT(nBins)

    NEtacPrDict  = uncPT(nBins)
    NEtacFbDict = uncPT(nBins)

    eff_pr_val, eff_pr_err = get_eff(bins,"prompt")
    eff_fb_val, eff_fb_err = get_eff(bins,"sec")

    if total:
        eff_pr_val, eff_pr_err = eff_pr_val[0:1], eff_pr_err[0:1]
        eff_fb_val, eff_fb_err = eff_fb_val[0:1], eff_fb_err[0:1]
        print(f"pr eff: {eff_pr_val} \\pm {eff_pr_err}")
        print(f"fb eff: {eff_fb_val} \\pm  {eff_fb_err}")
    else:
        eff_pr_val, eff_pr_err = eff_pr_val[1:], eff_pr_err[1:]
        eff_fb_val, eff_fb_err = eff_fb_val[1:], eff_fb_err[1:]

    keys = ["val","stat","uncorr","corr","tot"]
    with open(f"{homeDir}/{CSfilename}","r") as fi:
        for iB in range(nBins):
            for key in keys:
                line = fi.readline()
                l = line.split()
                NJpsiPrDict.d[key][iB], NJpsiFbDict.d[key][iB], NEtacPrDict.d[key][iB], NEtacFbDict.d[key][iB] = float(l[0]), float(l[1]), float(l[2]), float(l[3])
                NJpsiPrDict.calculateErrors(iB)
                NJpsiFbDict.calculateErrors(iB)
                NEtacPrDict.calculateErrors(iB)
                NEtacPrDict.calculateErrors(iB)
            line = fi.readline()


    CSJpsiPrDict, CSJpsiFbDict = getJpsi(bins, total)

    CSRelPrDict  = uncPT(nBins)
    CSRelFbDict = uncPT(nBins)

    CSEtacPrDict  = uncPT(nBins)
    CSEtacFbDict = uncPT(nBins)

    for iB in range(nBins):

        valRelPrXsec = NEtacPrDict.d["val"][iB]*BRJpsiToPP/BREtacToPP/eff_pr_val[iB]
        CSRelPrDict.copyRelUncts(NEtacPrDict,iB,valRelPrXsec)
        CSRelPrDict.addRelUnct("uncorr",iB, eff_pr_err[iB]/eff_pr_val[iB])
        CSRelPrDict.addRelUnct("norm",  iB, errBRrel)
        CSRelPrDict.calculateErrors(iB)
        # CSRelPrDict(iB)


        valRelFbXsec = NEtacFbDict.d["val"][iB]*BRJpsiToPP/BREtacToPP/eff_fb_val[iB]
        CSRelFbDict.copyRelUncts(NEtacFbDict,iB,valRelFbXsec)
        CSRelFbDict.addRelUnct("uncorr",iB,eff_fb_err[iB]/eff_fb_val[iB])
        CSRelFbDict.addRelUnct("norm",  iB, errBRrel)
        CSRelFbDict.calculateErrors(iB)
        # CSRelFbDict(iB)


        valEtacPrXsec = CSRelPrDict.d["val"][iB]*CSJpsiPrDict.d["val"][iB]
        CSEtacPrDict.copyRelUncts(CSRelPrDict,iB,valEtacPrXsec)
        ErrJpsiProdRel = CSJpsiPrDict.d["tot"][iB]/CSJpsiPrDict.d["val"][iB]
        CSEtacPrDict.addRelUnct("norm",iB,ErrJpsiProdRel)
        CSEtacPrDict.calculateErrors(iB)
        CSEtacPrDict(iB)


        valEtacFbXsec = CSRelFbDict.d["val"][iB]*CSJpsiFbDict.d["val"][iB]
        CSEtacFbDict.copyRelUncts(CSRelFbDict,iB,valEtacFbXsec)
        ErrJpsiProdRel = CSJpsiFbDict.d["tot"][iB]/CSJpsiFbDict.d["val"][iB]
        CSEtacFbDict.addRelUnct("norm",iB,ErrJpsiProdRel)
        CSEtacFbDict.calculateErrors(iB)
        # CSEtacFbDict(iB)

    NEtacPrDict.calculateTotalCS(nBins,BinHW)
    NEtacFbDict.calculateTotalCS(nBins,BinHW)

    logging.debug(f" prompt relative yeild: {NEtacPrDict.getIntegralXsec()}")
    logging.debug(f" from-b relative yeild: {NEtacFbDict.getIntegralXsec()}")
    logging.debug(".............................................")

    CSRelPrDict.calculateTotalCS(nBins,BinHW)
    CSRelFbDict.calculateTotalCS(nBins,BinHW)

    logging.debug(f" prompt relative cross-section: {CSRelPrDict.getIntegralXsec(['val','stat','uncorr', 'corr', 'systTot','norm'])}")
    logging.debug(f" from-b relative cross-section: {CSRelFbDict.getIntegralXsec(['val','stat','uncorr', 'corr', 'systTot','norm'])}")
    logging.debug(".............................................")

    CSJpsiPrDict.calculateTotalCS(nBins,BinHW)
    CSJpsiFbDict.calculateTotalCS(nBins,BinHW)

    logging.debug(f" prompt jpsi cross-section: {CSJpsiPrDict.getIntegralXsec(['val','stat','uncorr', 'corr','systTot','tot'])} \\nb")
    logging.debug(f" from-b jpsi cross-section: {CSJpsiFbDict.getIntegralXsec(['val','stat','uncorr', 'corr','systTot','tot'])} \\nb")

    CSEtacPrDict.calculateTotalCS(nBins,BinHW)
    CSEtacFbDict.calculateTotalCS(nBins,BinHW)

    logging.debug(f" prompt etac cross-section: {CSEtacPrDict.getIntegralXsec(['val','stat','uncorr', 'corr','systTot','norm'])} \\nb")
    logging.debug(f" from-b etac cross-section: {CSEtacFbDict.getIntegralXsec(['val','stat','uncorr', 'corr','systTot','norm'])} \\nb")
    logging.debug(".............................................")

    CSList["EtacPr"] = CSEtacPrDict
    CSList["EtacFb"] = CSEtacFbDict
    CSList["JpsiPr"] = CSJpsiPrDict
    CSList["JpsiFb"] = CSJpsiFbDict
    CSList["RelPr"] = CSRelPrDict
    CSList["RelFb"] = CSRelFbDict
    CSList["NPr"] = NEtacPrDict
    CSList["NFb"] = NEtacFbDict

    # if total:
    #     TypePrefix = f'{bb}_{TypePrefix}'
    # printCS(CSList, nBins, TypePrefix)

    return  CSList

def makeYieldPlots(empty:bool,bins:str,TypePrefix:str,\
    NEtacPrDict, NEtacFbDict):

    texCS = TLatex()
    texCS.SetNDC()

    rangeL, rangeR = BINNING[bins][0], BINNING[bins][-1]

    fitNRelPr   = TF1("firNRelPr_"+TypePrefix,  "[0]+[1]*x", rangeL, rangeR)
    fitNRelFb   = TF1("firNRelFb_"+TypePrefix, "[0]+[1]*x", rangeL, rangeR)

    if empty:
        gStyle.SetOptFit(0)
        gStyle.SetOptStat(0)
        gStyle.SetOptTitle(0)
    else:
        gStyle.SetOptFit(1)
        gStyle.SetOptStat(1)
        gStyle.SetOptTitle(0)

    gStyle.SetPaintTextFormat("3.1f")
    
    canv_pr  = TCanvas(f"canv_Pr_{TypePrefix}","N Rel",55,55,550,400)
    canv_fb = TCanvas(f"canv_Fb_{TypePrefix}","N Rel",55,55,550,400)

    canv_pr.cd()
    gPad.SetLeftMargin(0.20)
    #gPad.SetBottomMargin(0.125)

    hn_pr     = createHisto("hn_pr",NEtacPrDict.d["val"],NEtacPrDict.d["tot"],bins)
    hn_pr_stat = createHisto("hn_pr_stat",NEtacPrDict.d["val"],NEtacPrDict.d["stat"],bins)

    # hn_pr.Fit(fitNRelPr,"IMER")
    hn_pr.Draw("E1")
    hn_pr_stat.Draw("E1 SAME")
    # fitNRelPr.Draw("E1 SAME")
    hn_pr.SetTitle("prompt")
    # hn_pr.SetLineColor(6)
    hn_pr.SetMinimum(0.0)
    hn_pr.SetMaximum(2.0)
    hn_pr.GetYaxis().SetTitle("#frac{#it{#sigma_{#eta_{c}} #times BR(#eta_{c} #rightarrow p#bar{p})}}{#it{#sigma_{J/#psi} #times BR(J/#psi #rightarrow p#bar{p})}}")
    hn_pr.GetXaxis().SetTitleOffset(0.90)
    # hn_pr.GetYaxis().SetTitleOffset(1.10)
    hn_pr.GetYaxis().SetTitleOffset(1.40)
    hn_pr.GetYaxis().SetTitleSize(0.06)
    # hn_pr_stat.SetLineColor(4)
    # hn_pr_stat.SetMarkerStyle(23)


    if not empty:
        texCS.DrawLatex(0.25, 0.86, "LHCb")
        texCS.DrawLatex(0.25, 0.80, "#it{#sqrt{s}} = 13 TeV")
        texCS.DrawLatex(0.25, 0.74, "{}".format(title_dict[bins][1]))

    canv_fb.cd()
    gPad.SetLeftMargin(0.20)
    #gPad.SetBottomMargin(0.125)

    hn_fb     = createHisto("hn_fb",NEtacFbDict.d["val"],NEtacFbDict.d["tot"],bins)
    hn_fb_stat = createHisto("hn_fb_stat",NEtacFbDict.d["val"],NEtacFbDict.d["stat"],bins)

    # hn_fb.Fit(fitNRelFb, "IMER")
    hn_fb.Draw("E1")
    hn_fb_stat.Draw("E1 SAME")
    # fitNRelFb.Draw("E1 SAME")
    hn_fb.SetTitle("from-b")
    # hn_fb.SetLineColor(6)
    hn_fb.SetMinimum(0.0)
    hn_fb.SetMaximum(0.5)
    hn_fb.GetYaxis().SetTitle("#frac{#it{#sigma_{b#rightarrow#eta_{c}X} #times BR(#eta_{c} #rightarrow p#bar{p})}}{#it{#sigma_{b#rightarrowJ/#psiX} #times BR(J/#psi #rightarrow p#bar{p})}}")
    hn_fb.GetXaxis().SetTitleOffset(0.90)
    hn_fb.GetYaxis().SetTitleOffset(1.40)
    hn_fb.GetYaxis().SetTitleSize(0.06)
    # hn_fb_stat.SetLineColor(4)

    if not empty:
        texCS.DrawLatex(0.25, 0.86, "LHCb")
        texCS.DrawLatex(0.25, 0.8, "#it{#sqrt{s}} = 13 TeV")
        texCS.DrawLatex(0.25, 0.74, "{}".format(title_dict[bins][1]))

    for graph in [hn_pr_stat,hn_pr,hn_fb_stat,hn_fb]:
        graph.GetXaxis().SetLimits(rangeL,rangeR)
        graph.GetXaxis().SetTitle("{}".format(title_dict[bins][3]))
        graph.SetLineWidth(2)
        graph.SetMarkerSize(0.75)
        graph.SetMarkerColor(602)
        graph.SetLineColor(602)
        # graph.SetMarkerStyle(23)

    canv_pr.SaveAs(f"{homeDir}/CS/NRelPrompt_{TypePrefix}.pdf")
    canv_fb.SaveAs(f"{homeDir}/CS/NRelFromB_{TypePrefix}.pdf")


    f = TFile(f"{homeDir}/CS/NRel_{TypePrefix}.root","RECREATE")
    hn_pr.Write()
    hn_fb.Write()
    f.Write()
    f.Close()

def makeCSRelPlots(empty:bool,bins:str,TypePrefix:str,\
    CSRelPrDict, CSRelFbDict):

    if empty:
        gStyle.SetOptFit(0)
        gStyle.SetOptStat(0)
        gStyle.SetOptTitle(0)
    else:
        gStyle.SetOptFit(1)
        gStyle.SetOptStat(1)
        gStyle.SetOptTitle(0)

    gStyle.SetPaintTextFormat("3.1f");
    #TGaxis.SetMaxDigits(4)

    texCS = TLatex()
    texCS.SetNDC()

    rangeL, rangeR = BINNING[bins][0], BINNING[bins][-1]

    fitCSRelPr   = TF1("fitCSJpsiPr_"+TypePrefix, "[0]+[1]*x", rangeL, rangeR)
    fitCSRelPr.SetParNames("Const","LinSlope")
    fitCSRelPr.SetParameters(1e6,0.2)

    fitopt = "IMER"
    if bins!="PT":
        fitopt+="0"

    canv_pr = TCanvas(f"canv_pr_rel_{TypePrefix}","1",55,55,550,400)
    canv_fb = TCanvas(f"canv_fb_rel_{TypePrefix}","2",55,55,550,400)

    canv_pr.cd()
    gPad.SetLeftMargin(0.2)
    #gPad.SetBottomMargin(0.125)

    sigmaPrRel     = createHisto("sigmaPrRel",CSRelPrDict.d["val"],CSRelPrDict.d["tot"],bins)
    sigmaPrRelStat = createHisto("sigmaPrRelStat",CSRelPrDict.d["val"],CSRelPrDict.d["stat"],bins)
    sigmaPrRelSS   = createHisto("sigmaPrRelSS",CSRelPrDict.d["val"],CSRelPrDict.d["SS"],bins)


    sigmaPrRelSS.Fit(fitCSRelPr,fitopt)
    # fitCSRelPr.SetLineColor(2)

    sigmaPrRel.Draw("E2")
    sigmaPrRelSS.Draw("E2 SAME")
    sigmaPrRelStat.Draw("E2 SAME")
    sigmaPrRel.Draw("E1 SAME")
    sigmaPrRelSS.Draw("E1 SAME")
    sigmaPrRelStat.Draw("E1 SAME")

    sigmaPrRel.SetMinimum(0.0)
    sigmaPrRel.SetTitle("#frac{#it{d#sigma_{#eta_{c}}}/%s}{#it{d#sigma_{J/#psi}}/%s}"%(title_dict[bins][0],title_dict[bins][0]))
    sigmaPrRel.GetYaxis().SetTitle("#frac{#it{d#sigma_{#eta_{c}}}/%s}{#it{d#sigma_{J/#psi}}/%s}"%(title_dict[bins][0],title_dict[bins][0]))
    sigmaPrRel.GetYaxis().SetTitleOffset(1.1)

    if bins=="Y" and not empty:
        texCS.DrawLatex(0.25, 0.35, "LHCb")
        texCS.DrawLatex(0.25, 0.30, "#it{#sqrt{s}} = 13 TeV")
        texCS.DrawLatex(0.25, 0.25, "{}".format(title_dict[bins][1]))
    elif not empty:
        texCS.DrawLatex(0.25, 0.85, "LHCb")
        texCS.DrawLatex(0.25, 0.80, "#it{#sqrt{s}} = 13 TeV")
        texCS.DrawLatex(0.25, 0.75, "{}".format(title_dict[bins][1]))

    canv_fb.cd()
    gPad.SetLeftMargin(0.2)
    #gPad.SetBottomMargin(0.125)

    sigmaFbRel     = createHisto("sigmaFbRel",CSRelFbDict.d["val"],CSRelFbDict.d["tot"],bins)
    sigmaFbRelStat = createHisto("sigmaFbRelStat",CSRelFbDict.d["val"],CSRelFbDict.d["stat"],bins)
    sigmaFbRelSS   = createHisto("sigmaFbRelSS",CSRelFbDict.d["val"],CSRelFbDict.d["SS"],bins)


    sigmaFbRel.Draw("E2")
    sigmaFbRelStat.Draw("SAME E2")
    sigmaFbRelSS.Draw("SAME E2")
    sigmaFbRel.Draw("E1 same")
    sigmaFbRelStat.Draw("E1 SAME")
    sigmaFbRelSS.Draw("E1 SAME")

    sigmaFbRel.SetMinimum(0.0)
    sigmaFbRel.SetTitle("#frac{#it{d#sigma_{b#rightarrow#eta_{c}X}}/%s}{#it{d#sigma_{b#rightarrowJ/#psiX}}/%s}"%(title_dict[bins][0],title_dict[bins][0]))
    sigmaFbRel.GetYaxis().SetTitle("#frac{#it{d#sigma_{b#rightarrow#eta_{c}X}}/%s}{#it{d#sigma_{b#rightarrowJ/#psiX}}/%s}"%(title_dict[bins][0],title_dict[bins][0]))
    sigmaFbRel.GetYaxis().SetTitleOffset(1.1)


    if not empty:
        texCS.DrawLatex(0.25, 0.35, "LHCb")
        texCS.DrawLatex(0.25, 0.30, "#it{#sqrt{s}} = 13 TeV")
        texCS.DrawLatex(0.25, 0.25, "{}".format(title_dict[bins][1]))

 
    for i, graph in enumerate([sigmaPrRel,sigmaPrRelStat,sigmaPrRelSS, \
        sigmaFbRel,sigmaFbRelStat,sigmaFbRelSS]):
        graph.SetFillColorAlpha(632+i%3, 0.1)
        graph.SetFillStyle(1000)
        # graph.SetLineColor(632)
        # graph.SetMarkerColor(632)
        graph.GetXaxis().SetLimits(BINNING[bins][0],BINNING[bins][-1])
        graph.GetXaxis().SetTitle("{}".format(title_dict[bins][3]))

    # canv_pr.Update()
    # canv_fb.Update()
    canv_pr.Draw()
    canv_fb.Draw()
    canv_pr.SaveAs(f"{homeDir}/CS/CSRelPrompt_{TypePrefix}.pdf")
    canv_fb.SaveAs(f"{homeDir}/CS/CSRelFromB_{TypePrefix}.pdf")

def makeCSAbsPlots(empty:bool,bins:str,TypePrefix:str,\
    CSEtacPrDict, CSEtacFbDict, CSJpsiPrDict, CSJpsiFbDict, logsc:bool=False):

    rangeL, rangeR = BINNING[bins][0], BINNING[bins][-1]
    if bins=="PT":
        rangeR = 14.0

    fitCSJpsiPr  = TF1("fitCSJpsiPr_"+TypePrefix,  "[0]*exp(-[1]*x)", rangeL, rangeR)
    fitCSJpsiPr.SetParNames("AmpJpsiPr","ExpJpsiPr")
    fitCSJpsiPr.SetParameters(1e6,0.2)
    fitCSJpsiPr.SetLineColor(4)

    fitCSJpsiFb = TF1("fitCSJpsiFb_"+TypePrefix, "[0]*exp(-[1]*x)", rangeL, rangeR)

    fitCSEtacPr  = TF1("fitCSEtacPr_"+TypePrefix,   "[0]*exp(-[1]*x)", rangeL, rangeR)
    fitCSEtacPr.SetParNames("AmpEtacPr","ExpEtacPr")
    fitCSEtacPr.SetParameters(1e6,0.2)
    fitCSEtacPr.SetLineColor(2)

    fitCSEtacFb = TF1("fitCSEtacFb_"+TypePrefix, "[0]*exp(-[1]*x)",  rangeL, rangeR)

    fitopt = "IMER"
    if bins!="PT":
        fitopt+="0"

    if empty:
        gStyle.SetOptFit(0)
        gStyle.SetOptStat(0)
        gStyle.SetOptTitle(0)
    else:
        gStyle.SetOptFit(1)
        gStyle.SetOptStat(1)
        gStyle.SetOptTitle(0)

    gStyle.SetPaintTextFormat("3.1f");
    #TGaxis.SetMaxDigits(4)


    texCS = TLatex()
    texCS.SetNDC()

    canv_pr = TCanvas(f"canv_pr_{TypePrefix}","J/Psi_Mass",55,55,550,400)
    canv_fb = TCanvas(f"canv_fb_{TypePrefix}","J/Psi_Mass",55,55,550,400)

    canv_pr.cd()
    if logsc:
        canv_pr.SetLogy()
    gPad.SetLeftMargin(0.20)
    #gPad.SetBottomMargin(0.125)

    sigmaPr     = createHisto("sigmaPr",CSEtacPrDict.d["val"],CSEtacPrDict.d["tot"],bins)
    sigmaPrStat = createHisto("sigmaPrStat",CSEtacPrDict.d["val"],CSEtacPrDict.d["stat"],bins)
    sigmaPrSS   = createHisto("sigmaPrSS",CSEtacPrDict.d["val"],CSEtacPrDict.d["SS"],bins)
    sigmaJpsiPr = createHisto("sigmaJpsiPr",CSJpsiPrDict.d["val"],CSJpsiPrDict.d["tot"],bins)

    sigmaPrSS.Fit(fitCSEtacPr,fitopt)
    fitCSEtacPr.SetLineColor(633)

    sigmaJpsiPr.Fit(fitCSJpsiPr,fitopt)
    fitCSJpsiPr.SetLineColor(601)
    #fitCSEtacPr.SetLineColor(kBlack)
    #fitCSEtacPr.SetLineStyle(2)
    #fitCSJpsiPr.SetLineStyle(2)


    sigmaPr.Draw("E2")
    sigmaPr.Draw("E1 SAME")
    sigmaPrSS.Draw("E1 SAME")
    sigmaPrStat.Draw("E1 SAME")
    sigmaJpsiPr.Draw("E2 SAME")
    sigmaJpsiPr.Draw("E1 SAME")

    sigmaPr.SetTitle("#it{d#sigma_{p}}/%s"%(title_dict[bins][0]))
    sigmaPr.SetMinimum(1.0)
    if logsc:
        if bins=="PT":
            sigmaPr.SetMaximum(2.e3)
        else:
            sigmaPr.SetMaximum(2.e4)
    else:
        sigmaPr.SetMinimum(0)
        if bins=="PT":
            sigmaPr.SetMaximum(7.e2)
        else:
            sigmaPr.SetMaximum(3.5e3)
    sigmaPr.GetXaxis().SetRangeUser(rangeL,rangeR)
    sigmaPr.GetYaxis().SetTitle("#it{d#sigma}/%s %s"%(title_dict[bins][0],title_dict[bins][2]))
    sigmaPr.GetYaxis().SetLimits(1, 500)
    sigmaJpsiPr.SetLineWidth(2)
    sigmaJpsiPr.SetLineColor(4)


    leg1 = TLegend(0.80,0.72,0.90,0.90)
    if logsc:
        leg1 = TLegend(0.25,0.22,0.35,0.40)
    leg1.SetBorderSize(0)
    leg1.AddEntry(sigmaPr,"#it{#eta_{c}}","lepf")
    leg1.AddEntry(sigmaJpsiPr,"#it{J/#psi}","lepf")
    leg1.Draw()

    if not empty:
        texCS.DrawLatex(0.25, 0.36, "LHCb")
        texCS.DrawLatex(0.25, 0.3, "#it{#sqrt{s}} = 13 TeV")
        texCS.DrawLatex(0.25, 0.24, "{}".format(title_dict[bins][1]))


    canv_fb.cd()
    if logsc:
        canv_fb.SetLogy()
    gPad.SetLeftMargin(0.20)
    #gPad.SetBottomMargin(0.125)

    sigmaFb     = createHisto("sigmaFb",CSEtacFbDict.d["val"],CSEtacFbDict.d["tot"],bins)
    sigmaFbStat = createHisto("sigmaFbStat",CSEtacFbDict.d["val"],CSEtacFbDict.d["stat"],bins)
    sigmaFbSS   = createHisto("sigmaFbSS",CSEtacFbDict.d["val"],CSEtacFbDict.d["SS"],bins)
    sigmaJpsiFb = createHisto("sigmaJpsiFb",CSJpsiFbDict.d["val"],CSJpsiFbDict.d["tot"],bins)

    #sigmaFb.Fit(fitCSEtacFb,"I")
    #sigmaJpsiFb.Fit(fitCSJpsiFb,"I")

    sigmaFb.Draw("E2")
    sigmaFb.Draw("E1 SAME")
    sigmaFbStat.Draw("E1 SAME")
    sigmaFbSS.Draw("E1 SAME")
    sigmaJpsiFb.Draw("E2 SAME")
    sigmaJpsiFb.Draw("E1 SAME")
    sigmaFb.SetTitle("d#sigma_{s}/dp_{t}")
    sigmaFb.SetLineWidth(2)

    sigmaFb.SetMinimum(1.0)
    if logsc:
        if bins=="PT":
            sigmaFb.SetMaximum(2.5e2)
        else:
            sigmaFb.SetMaximum(2.5e3)
    else:
        sigmaPr.SetMinimum(0)
        if bins=="PT":
            sigmaFb.SetMaximum(1.5e2)
        else:
            sigmaFb.SetMaximum(6e2)
    sigmaFb.GetXaxis().SetRangeUser(rangeL,rangeR)
    sigmaFb.GetYaxis().SetLimits(0.3, 100)
    sigmaFb.GetYaxis().SetTitle("#it{d#sigma_{b#rightarrow#eta_{c}X}}/%s %s"%(title_dict[bins][0],title_dict[bins][2]))


    if not empty:
        texCS.DrawLatex(0.25, 0.36, "LHCb")
        texCS.DrawLatex(0.25, 0.3, "#it{#sqrt{s}} = 13 TeV")
        texCS.DrawLatex(0.25, 0.24, "{}".format(title_dict[bins][1]))
    leg1.Draw()


    for graph in [sigmaJpsiPr, sigmaJpsiFb]:
        graph.SetLineColor(601)
        graph.SetMarkerStyle(kFullCircle)
        graph.SetMarkerSize(0.5)
        graph.SetMarkerColor(601)
        graph.SetFillColorAlpha(601, 0.2)
        graph.SetFillStyle(1000)
        

    for graph in [sigmaPr, sigmaPrStat, sigmaPrSS, \
                sigmaFb, sigmaFbStat, sigmaFbSS]:
        graph.SetLineColor(633)
        graph.SetMarkerStyle(kFullCircle)
        graph.SetMarkerSize(0.5)
        graph.SetMarkerColor(633)
        graph.GetXaxis().SetLimits(rangeL,rangeR)
        graph.GetXaxis().SetTitle("{}".format(title_dict[bins][3]))
        graph.SetFillColorAlpha(633, 0.2)
        graph.SetFillStyle(1000)

    LogPrefix = ""
    if logsc:
        LogPrefix = "_log"

    canv_pr.SaveAs(f"{homeDir}/CS/CSEtacPrompt_{TypePrefix}{LogPrefix}.pdf")
    canv_fb.SaveAs(f"{homeDir}/CS/CSEtacFromB_{TypePrefix}{LogPrefix}.pdf")


def makeCSPlots(empty:bool,bins:str,TypePrefix:str,\
    CSRelPrDict,CSRelFbDict, CSEtacPrDict, CSEtacFbDict, CSJpsiPrDict, CSJpsiFbDict, NEtacPrDict, NEtacFbDict, logsc:bool=False):

    makeYieldPlots(empty, bins, TypePrefix, NEtacPrDict, NEtacFbDict)
    makeCSRelPlots(empty, bins, TypePrefix, CSRelPrDict,CSRelFbDict)
    makeCSAbsPlots(empty, bins, TypePrefix, CSEtacPrDict, CSEtacFbDict, CSJpsiPrDict, CSJpsiFbDict, logsc)

def makePlotsTzFit_vs_TzCut(empty, bins, CSRelPrDict_tzFit, CSRelFbDict_tzFit, CSRelPrDict_tzCut,CSRelFbDict_tzCut):

    if empty:
        gStyle.SetOptFit(0)
        gStyle.SetOptStat(0)
        gStyle.SetOptTitle(0)
    else:
        gStyle.SetOptFit(1)
        gStyle.SetOptStat(1)
        gStyle.SetOptTitle(0)

    leg = TLegend(0.6, 0.2, 0.9, 0.4)
    texCS = TLatex()
    texCS.SetNDC()

    canv_1 = TCanvas("canv_pr_rel_comp", "1",55,55,550,400)
    canv_2 = TCanvas("canv_fb_rel_comp","2",55,55,550,400)

    canv_1.cd()

    sigmaPrRel_tzFit     = TGraphErrors(NB["PT"],PTBins,CSRelPrDict_tzFit.d["val"],PTBinHW,CSRelPrDict_tzFit.d["tot"])
    sigmaPrRelStat_tzFit = TGraphErrors(NB["PT"],PTBins,CSRelPrDict_tzFit.d["val"],PTBinHW,CSRelPrDict_tzFit.d["stat"])
    sigmaPrRelSS_tzFit   = TGraphErrors(NB["PT"],PTBins,CSRelPrDict_tzFit.d["val"],PTBinHW,CSRelPrDict_tzFit.d["SS"])


    sigmaPrRel_tzCut     = TGraphErrors(NB["PT"],PTBins,CSRelPrDict_tzCut.d["val"],PTBinHW,CSRelPrDict_tzCut.d["tot"])
    sigmaPrRelStat_tzCut = TGraphErrors(NB["PT"],PTBins,CSRelPrDict_tzCut.d["val"],PTBinHW,CSRelPrDict_tzCut.d["stat"])
    sigmaPrRelSS_tzCut   = TGraphErrors(NB["PT"],PTBins,CSRelPrDict_tzCut.d["val"],PTBinHW,CSRelPrDict_tzCut.d["SS"])

    gPad.SetLeftMargin(0.20)
    #gPad.SetBottomMargin(0.125)


    sigmaPrRel_tzCut.Draw("AE2")
    sigmaPrRelSS_tzCut.Draw("E2 same")
    sigmaPrRelStat_tzCut.Draw("E2 same")

    sigmaPrRel_tzFit.Draw("SAME P")
    sigmaPrRelSS_tzFit.Draw("SAME P")
    sigmaPrRelStat_tzFit.Draw("SAME P")

    leg.AddEntry(sigmaPrRel_tzCut, "DLL_{p\pi}>20 & DLL_{pK}>15", "f")
    leg.AddEntry(sigmaPrRel_tzFit, "ProbNNp>0.6", "lep")

    leg.Draw()
    
    texCS.DrawLatex(0.3, 0.8, "LHCb")
    texCS.DrawLatex(0.3, 0.75, "#it{#sqrt{s}} = 13 TeV")
    texCS.DrawLatex(0.3, 0.7, "{}".format(title_dict[bins][1]))

    canv_2.cd()
    #gPad.SetBottomMargin(0.125)

    # sigmaFbRel_tzFit     = TGraphErrors( 4,PTBins,CSRelFbDict_tzFit.d["val"],PTBinHW,CSRelFbDict_tzFit.d["tot"])
    # sigmaFbRelStat_tzFit = TGraphErrors( 4,PTBins,CSRelFbDict_tzFit.d["val"],PTBinHW,CSRelFbDict_tzFit.d["stat"])
    # sigmaFbRelSS_tzFit   = TGraphErrors( 4,PTBins,CSRelFbDict_tzFit.d["val"],PTBinHW,CSRelFbDict_tzFit.d["SS"])
    sigmaFbRel_tzFit     = TGraphErrors( NB["PT"],PTBins,CSRelFbDict_tzFit.d["val"],PTBinHW,CSRelFbDict_tzFit.d["tot"])
    sigmaFbRelStat_tzFit = TGraphErrors( NB["PT"],PTBins,CSRelFbDict_tzFit.d["val"],PTBinHW,CSRelFbDict_tzFit.d["stat"])
    sigmaFbRelSS_tzFit   = TGraphErrors( NB["PT"],PTBins,CSRelFbDict_tzFit.d["val"],PTBinHW,CSRelFbDict_tzFit.d["SS"])


    # sigmaFbRel_tzCut     = TGraphErrors( 4,PTBins,CSRelFbDict_tzCut.d["val"],PTBinHW,CSRelFbDict_tzCut.d["tot"])
    # sigmaFbRelStat_tzCut = TGraphErrors( 4,PTBins,CSRelFbDict_tzCut.d["val"],PTBinHW,CSRelFbDict_tzCut.d["stat"])
    # sigmaFbRelSS_tzCut   = TGraphErrors( 4,PTBins,CSRelFbDict_tzCut.d["val"],PTBinHW,CSRelFbDict_tzCut.d["SS"])
    sigmaFbRel_tzCut     = TGraphErrors( NB["PT"],PTBins,CSRelFbDict_tzCut.d["val"],PTBinHW,CSRelFbDict_tzCut.d["tot"])
    sigmaFbRelStat_tzCut = TGraphErrors( NB["PT"],PTBins,CSRelFbDict_tzCut.d["val"],PTBinHW,CSRelFbDict_tzCut.d["stat"])
    sigmaFbRelSS_tzCut   = TGraphErrors( NB["PT"],PTBins,CSRelFbDict_tzCut.d["val"],PTBinHW,CSRelFbDict_tzCut.d["SS"])

    gPad.SetLeftMargin(0.25)

    sigmaFbRel_tzCut.Draw("AE2")
    sigmaFbRelStat_tzCut.Draw("E2 same")
    sigmaFbRelSS_tzCut.Draw("E2 same")

    sigmaFbRel_tzFit.Draw("SAME P")
    sigmaFbRelStat_tzFit.Draw("SAME P")
    sigmaFbRelSS_tzFit.Draw("SAME P")

    leg.Draw()

    texCS.DrawLatex(0.3, 0.35, "LHCb")
    texCS.DrawLatex(0.3, 0.30, "#it{#sqrt{s}} = 13 TeV")
    texCS.DrawLatex(0.3, 0.25, "{}".format(title_dict[bins][1]))




    for graph in [sigmaPrRel_tzFit,sigmaPrRelStat_tzFit,sigmaPrRelSS_tzFit, \
              sigmaFbRel_tzFit,sigmaFbRelStat_tzFit,sigmaFbRelSS_tzFit]:
        graph.SetMinimum(0.0)
        graph.GetYaxis().SetTitleOffset(1.1)
        graph.SetLineColor(1)
        graph.SetMarkerStyle(8)
        graph.SetMarkerColor(1)
        graph.GetXaxis().SetLimits(rangeL,rangeR)
        graph.GetXaxis().SetTitle("{}".format(title_dict[bins][3]))
        graph.GetYaxis().SetLimits(0,4.)

    for graph in [sigmaPrRel_tzCut,sigmaPrRelStat_tzCut,sigmaPrRelSS_tzCut, \
              sigmaFbRel_tzCut,sigmaFbRelStat_tzCut,sigmaFbRelSS_tzCut]:
        graph.SetMinimum(0.0)
        graph.GetYaxis().SetTitleOffset(1.1)
        # graph.SetLineColor(2)
        # graph.SetMarkerStyle(4)
        # graph.SetMarkerColor(2)
        graph.SetFillColor(2)
        graph.SetFillStyle(3002)
        graph.GetXaxis().SetLimits(rangeL,rangeR)
        graph.GetXaxis().SetTitle("{}".format(title_dict[bins][3]))
        graph.GetYaxis().SetLimits(0,4.)

    for graph in [sigmaPrRel_tzFit,sigmaPrRelStat_tzFit,sigmaPrRelSS_tzFit,\
                  sigmaPrRel_tzCut,sigmaPrRelStat_tzCut,sigmaPrRelSS_tzCut]:
        graph.GetYaxis().SetTitle("#frac{#it{d#sigma_{#eta_{c}}}/%s}{#it{d#sigma_{J/#psi}}/%s}"%(title_dict[bins][0],title_dict[bins][0]))

    for graph in [sigmaFbRel_tzCut,sigmaFbRelStat_tzCut,sigmaFbRelSS_tzCut,\
                  sigmaFbRel_tzFit,sigmaFbRelStat_tzFit,sigmaFbRelSS_tzFit]:
        graph.GetYaxis().SetTitle("#frac{#it{d#sigma_{b#rightarrow#eta_{c}X}}/%s}{#it{d#sigma_{b#rightarrowJ/#psiX}}/%s}"%(title_dict[bins][0],title_dict[bins][0]))



    canv_1.SaveAs(f"{homeDir}/CS/CSRelPrompt_comp_PID.pdf")
    canv_2.SaveAs(f"{homeDir}/CS/CSRelFromB_comp_PID.pdf")




if __name__=='__main__':

    gROOT.LoadMacro("../libs/lhcbStyle.C")
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)

    # bb = "TOT"
    bb = "PT"
    # bb = "Y"

    total = (bb=="TOT")
    #CSRelPrDict_tzFit,  CSRelFbDict_tzFit, \
    #CSEtacPrDict_tzFit, CSEtacFbDict_tzFit, \
    #CSJpsiPrDict_tzFit, CSJpsiFbDict_tzFit, \
    #NEtacPrDict_tzFit,  NEtacFbDict_tzFit = crosssection(CSfilename="/rel_PT_sim.txt")

    # CSList = crosssection(CSfilename=f"/CB/rel_{bb}_RunIMethod_ProbNNp_06.txt",bins="PT",total=True)
    # printCS(CSList, 1, "_RunIMethod_CB")
    # CSEtacPrDTot_tzCut = CSList["EtacPr"]
    # CSEtacFbDTot_tzCut = CSList["EtacFb"]
    # CSJpsiPrDTot_tzCut = CSList["JpsiPr"]
    # CSJpsiFbDTot_tzCut = CSList["JpsiFb"]
    # CSRelPrDTot_tzCut = CSList["RelPr"]
    # CSRelFbDTot_tzCut = CSList["RelFb"]
    # NEtacPrDTot_tzCut = CSList["NPr"]
    # NEtacFbDTot_tzCut = CSList["NFb"]

    # makeCSPlots(True, "TOT", "TOT"+"_RunIMethod", \
    #             CSEtacPrDTot_tzCut,  CSRelFbDTot_tzCut, \
    #             CSEtacPrDTot_tzCut, CSEtacFbDTot_tzCut, \
    #             CSJpsiPrDTot_tzCut, CSJpsiFbDTot_tzCut, \
    #             NEtacPrDTot_tzCut,  NEtacFbDTot_tzCut)

    ''' For calculating CS from the average values, one has to set eff errors to 0'''
    # fileKey = f"{bb}_RunIMethod_CB_4NRQCD"
    # fileKey = f"{bb}_RunIMethod_avg"
    fileKey = f"{bb}_RunIMethod_avg_6Bins"
    # fileKey = f"{bb}_RunIMethod"
    # CSList_old = crosssection(CSfilename=f"/CB/rel_{bb}_RunIMethod_old_ProbNNp_06.txt",bins=bb)
    # CSList_old = crosssection(CSfilename=f"/CB/rel_{bb}_RunIMethod_oldPID.txt",bins=bb)
    # CSList_new = crosssection(CSfilename=f"/CB/rel_{bb}_RunIMethod_ProbNNp_06.txt",bins=bb)
    # CSList_new = crosssection(CSfilename=f"/CB/rel_{bb}_RunIMethod_new_ProbNNp_06.txt",bins=bb)
    # CSList_new = crosssection(CSfilename=f"/CB/rel_{bb}_RunIMethod_oldPID.txt",bins=bb)
    # CSList = crosssection(CSfilename=f"/CB/rel_{bb}_RunIMethod_ProbNNp_06_4Bins.txt",bins=bb)
    # CSList = crosssection(CSfilename=f"/CB/rel_{bb}_RunIMethod_oldPID.txt",bins=bb)
    # CSList = crosssection(CSfilename=f"/CB/rel_{bb}_RunIMethod_ProbNNp_06.txt",bins=bb, total=total)
    # CSList = crosssection(CSfilename=f"/CB/rel_{bb}_RunIMethod_5-14_ProbNNp_06.txt",bins=bb,total=total)
    # CSList = crosssection(CSfilename=f"/CB/rel_{bb}_RunIMethod_6-14_ProbNNp_06.txt",bins=bb,total=total)
    # CSList = crosssection(CSfilename=f"/CB/rel_{bb}_RunIMethod_old_ana.txt",bins=bb)
    # CSList = crosssection(CSfilename=f"/CB/rel_{bb}_RunIMethod_average.txt",bins=bb, total=total)
    CSList = crosssection(CSfilename=f"/CB/rel_{bb}_RunIMethod_average_6Bins.txt",bins=bb, total=total)
    # printCS(CSList, NB[bb], fileKey)
    
    CSEtacPrDict_tzCut = CSList["EtacPr"]
    CSEtacFbDict_tzCut = CSList["EtacFb"]
    CSJpsiPrDict_tzCut = CSList["JpsiPr"]
    CSJpsiFbDict_tzCut = CSList["JpsiFb"]
    CSRelPrDict_tzCut = CSList["RelPr"]
    CSRelFbDict_tzCut = CSList["RelFb"]
    NEtacPrDict_tzCut = CSList["NPr"]
    NEtacFbDict_tzCut = CSList["NFb"]

    #makeCSPlots(True, "sim", \
                #CSRelPrDict_tzFit,  CSRelFbDict_tzFit, \
                #CSEtacPrDict_tzFit, CSEtacFbDict_tzFit, \
                #CSJpsiPrDict_tzFit, CSJpsiFbDict_tzFit, \
                #NEtacPrDict_tzFit, NEtacFbDict_tzFit)

    # makeCSPlots(True, bb, bb+"_RunIMethod", \
    #             CSRelPrDict_tzCut,  CSRelFbDict_tzCut, \
    #             CSEtacPrDict_tzCut, CSEtacFbDict_tzCut, \
    #             CSJpsiPrDict_tzCut, CSJpsiFbDict_tzCut, \
    #             NEtacPrDict_tzCut, NEtacFbDict_tzCut, \
    #             True )

    # makeYieldPlots(True, bb, fileKey, NEtacPrDict_tzCut, NEtacFbDict_tzCut)
    makeCSRelPlots(True, bb, fileKey, CSRelPrDict_tzCut, CSRelFbDict_tzCut)
    # makeCSAbsPlots(True, bb, fileKey, CSEtacPrDict_tzCut, CSEtacFbDict_tzCut, \
    #             CSJpsiPrDict_tzCut, CSJpsiFbDict_tzCut, True)

    # CSRelPrDict_tzCut = CSList_old["RelPr"]
    # CSRelFbDict_tzCut = CSList_old["RelFb"]
    # CSRelPrDict_tzFit = CSList_new["RelPr"]
    # CSRelFbDict_tzFit = CSList_new["RelFb"]
    # makePlotsTzFit_vs_TzCut(True,bb,CSRelPrDict_tzFit, CSRelFbDict_tzFit, \
    #                         CSRelPrDict_tzCut,CSRelFbDict_tzCut)

