from ROOT import gStyle, TFile
from array import array
import numpy as np
from cstools import uncPT, nPTBins, nYBins
import logging
import csv

homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"


#calculation of J/psi CS in bins of Y/PT 
#y bins = number of bin
#pt bins = range
def calc_y_bins(src:str="prompt",bins:str="PT", ymin:int=1, ymax:int=5, pmin:float=5., pmax:float=20.0):

    # binningY = [2., 2.5, 3., 4., 4.5]
    binningY = 0.25*np.ones(5)
    gStyle.SetOptFit(0)
    gStyle.SetOptStat(0)

    pr = 1 if src=="prompt" else 2

    filename = f"{dataDir}/results/HEPData/HEPData-ins1391511-v2-Table_{pr}.root"
    f = TFile(filename,"READ")

    cs_jpsi = np.array([])
    cs_jpsi_int = uncPT(0)

    import os, sys
    # sys.stdout = open(f"{homeDir}/CS/CSJpsi_{bins}_{src}.txt", 'w')
    fout = open(f"{homeDir}/CS/CSJpsi_{bins}_{src}.txt", 'w')


    if bins=="PT":
        nBins = nPTBins
    else:
        nBins = nYBins
    logging.info(f"Integrating CS for Y bins {ymin}-{ymax} over pT {pmin}-{pmax} GeV") 
    for iY in range(ymin-1,ymax):

        h = f.Get(f"Table {pr}/Hist1D_y{iY+1}")
        h_st = f.Get(f"Table {pr}/Hist1D_y{iY+1}_e1")  #stat
        h_sc = f.Get(f"Table {pr}/Hist1D_y{iY+1}_e2")  #syst corr
        h_su = f.Get(f"Table {pr}/Hist1D_y{iY+1}_e3")  #syst uncorr

        ptlow = h.FindBin(pmin)
        pthigh =  h.FindBin(pmax)
        # print(ptlow, pthigh)

        # fill PT bins array for requested range
        binning = np.array([])
        s = h.GetXaxis().GetXbins().GetSize()
        b = h.GetXaxis().GetXbins().GetArray()
        for i in range(ptlow-1,pthigh):
            binning = np.append(binning, b[i])
        binning = (binning[1:] - binning[:-1])/2. # HW of pt bins
        # print(iY, binning)

        cs_jpsi = np.append(cs_jpsi, uncPT(pthigh-ptlow))
        # print(pthigh-ptlow)
        for iPT in range(pthigh-ptlow):
            # print(iPT)
            cs_jpsi[iY].d["val"][iPT] = h.GetBinContent(iPT+ptlow)
            cs_jpsi[iY].d["stat"][iPT] = h_st.GetBinContent(iPT+ptlow)
            cs_jpsi[iY].d["uncorr"][iPT] = h_su.GetBinContent(iPT+ptlow)
            cs_jpsi[iY].d["corr"][iPT] = h_sc.GetBinContent(iPT+ptlow)
            #cs_jpsi[iY]["norm"][iPT] = 0.
            cs_jpsi[iY].calculateErrors(iPT)

            # print(cs_jpsi[iY].d["val"][iPT])
        cs_jpsi[iY].calculateTotalCS(pthigh-ptlow, binning)
        
        cs_jpsi_tot = cs_jpsi[iY].IntegralXsec
        logging.debug(f'\t Y Bin {iY+1} {cs_jpsi_tot["val"]:.3f}, \
        {cs_jpsi_tot["stat"]:.3f}, \
        {cs_jpsi_tot["uncorr"]:.3f}, \
        {cs_jpsi_tot["corr"]:.3f}, \
        {(cs_jpsi_tot["stat"]**2 + cs_jpsi_tot["uncorr"]**2 + cs_jpsi_tot["corr"]**2)**0.5:.3f}')

        cs_jpsi_int.append(cs_jpsi_tot)
        # cs_jpsi_int(iY)
        # print("\n")
    
    cs_jpsi_int.calculateTotalCS(ymax-ymin+1, binningY[ymin-1:ymax])
    logging.info(f'\t sigma = {cs_jpsi_int.getIntegralXsec(["val","stat","uncorr","corr","tot"])} \\nb')
    #PTBins_pr = np.array([4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5, 12.5, 13.5])

    #YBins_pr = np.array([2.25, 2.75, 3.25, 3.75])



    # sys.stdout.close()
    f.Close()

    return cs_jpsi_int.IntegralXsec

def calc_y_int(src:str="prompt", ymin:int=1, ymax:int=5, pmin:float=5., pmax:float=14.0):

    csv_columns = ['ptmin', 'ptmax', 'val','stat','uncorr', 'corr', 'norm', 'tot']
    csv_file = f"{homeDir}/CS_jpsi_{src}_int_inY{ymax-ymin+1}.csv"

    cs_jpsi = uncPT(0)
    ptlist = np.linspace(pmin, pmax, int(pmax-pmin+1))

    try:
        with open(csv_file, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns, quoting=csv.QUOTE_NONNUMERIC)
            writer.writeheader()
            for i_pt, pt in enumerate(ptlist[:-1]):
                cs_bin = calc_y_bins(src,"PT", ymin, ymax, pt, ptlist[i_pt+1])
                logging.debug(cs_bin)
                cs_bin["ptmin"] = pt
                cs_bin["ptmax"] = ptlist[i_pt+1]
                cs_jpsi.append(cs_bin)
                cs_bin.pop("SS")
                cs_bin.pop("systTot")
                # writer.writerow(cs_jpsi.get(i_pt,csv_columns))
                writer.writerow(cs_bin)
        logging.info(f"File {csv_file} is successfully created")
    except IOError:
        logging.error("I/O error")
    cs_jpsi.calculateTotalCS(ptlist.size-1, 0.5*np.ones(ptlist.size))
    logging.info(f"Integrated CS in pT [{pmin},{pmax}] is {cs_jpsi.getIntegralXsec()}")


if __name__=="__main__":

    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
    # calc_y_bins("prompt",bins="PT", ymin=1, ymax=4, pmin=3., pmax=14.0)
    # calc_y_bins("fromB",bins="PT", ymin=1, ymax=4, pmin=3., pmax=14.0)
    # calc_y_bins("prompt",bins="PT", ymin=1, ymax=4, pmin=5., pmax=14.0)
    # calc_y_bins("fromB",bins="PT", ymin=1, ymax=4, pmin=5., pmax=14.0)
    # calc_y_int("prompt",ymin=1, ymax=4, pmin=0.)
    calc_y_int("fromB",ymin=1, ymax=4, pmin=0.)
