import numpy as np
from array import array
homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = '/sps/lhcb/zhovkovska/etac2s_ppbar/'

nPTBins = 5 #up to 14
nYBins = 4

nPTBins_tot = 5                  # nPTBins in Jpsi->mumu  - nPTBins up to 5 GeV
PTBins  = np.arange(5.5, 13.51)     # bin centers from 5 to 14 GeV
PTBinHW = np.ones(nPTBins)

ptBins = np.array([5., 6.5, 8., 10.0, 12.0, 14.0])

class unc():
    def __init__(self):
        self.d = {}
        for key in ["val","stat","uncorr","corr","systTot","SS","norm","tot"]:
            self.d[key] = 0. if key!="val" else 1.

    def __call__(self, keys=["val","stat","uncorr","corr","tot"]):
        print([ key for key in keys])
        for key in keys:
            print(f"{self.d[key]:.3f}",end=" ")
        print("")
        return self.d

    def __getitem__(self, keys=["val","stat","uncorr","corr","tot"]):
        d = {}
        for key in keys:
            d[key] = self.d[key]
        return d

    def __setitem__(self, d):
        for key in d:
            self.d[key] = d[key]
        

    def calculateErrors(self):
        Val    = self.d["val"]
        Stat   = self.d["stat"]
        Uncorr = self.d["uncorr"]
        Corr   = self.d["corr"]
        Norm   = self.d["norm"]

        self.d["systTot"] = (Uncorr**2 + Corr**2)**0.5
        self.d["SS"]      = (Uncorr**2 + Corr**2 + Stat**2)**0.5
        self.d["tot"]     = (Uncorr**2 + Corr**2 + Stat**2 + Norm**2)**0.5

    def copyRelUncts(self,origin,val):
        oldVal  = origin.d["val"]
        self.d["val"] = val
        for key in ["stat","stat","uncorr","corr","systTot","SS","norm"]:
            self.d[key] = val*origin.d[key]/oldVal


    def addRelUnct(self,key,valRel):
        otherKeys = []
        if key=="uncorr" or key=="corr":
            otherKeys = ["systTot","SS","tot"]
        if key=="norm" :
            otherKeys = ["tot"]

        otherKeys.append(key)
        for kkey in otherKeys:
            oldUnc = self.d[kkey]
            val    = self.d["val"]
            self.d[kkey] = val*((oldUnc/val)**2 + valRel**2)**0.5


class yield2cs():
    '''
    n1 = yeild 1
    n2 = yeild 2
    br1 = branching ratio 1
    br2 = branching ratio 2
    cs = cross section reference
    eps = efficiency ratio
    '''
    def __init__(self, n1:unc=None, n2:unc=None, br1:unc=None, br2:unc=None, cs:unc=None, eps=1.,epsErr=0.):
        keys = ["stat","uncorr","corr","systTot","SS","norm","tot"]
        if n1!=None:
            self.n1 = n1
            self.n1.calculateErrors()
        else:
            self.n2 = unc()

        if cs!=None:
            self.cs = cs
            self.cs.calculateErrors()
        else:
            self.cs = unc()

        if n2!=None:
            self.n2 = n2
            self.n2.calculateErrors()
        else:
            self.n2 = unc()

        if br1!=None:
            self.br1 = br1
            self.br1.calculateErrors()
        else:
            self.br1 = unc()

        if br2!=None:
            self.br2 = br2
            self.br2.calculateErrors()
        else:
            self.br2 = unc()

        self.eps = eps
        self.epsErr = epsErr

    
    def calcCS(self):
        cs_new = unc()
        cs_new.d["val"] = self.n1.d["val"] / self.n2.d["val"]*self.cs.d["val"]*self.eps *  self.br1.d["val"]/ self.br2.d["val"]

        cs_new.addRelUnct("stat",self.n1.d["stat"]/self.n1.d["val"])
        cs_new.addRelUnct("stat",self.n2.d["stat"]/self.n2.d["val"])
        cs_new.addRelUnct("uncorr",self.n1.d["uncorr"]/self.n1.d["val"])
        cs_new.addRelUnct("uncorr",self.n2.d["uncorr"]/self.n2.d["val"])
        cs_new.addRelUnct("corr", self.n1.d["corr"]/self.n1.d["val"])
        cs_new.addRelUnct("corr", self.n2.d["corr"]/self.n2.d["val"])
        cs_new.addRelUnct("norm", self.cs.d["tot"]/self.cs.d["val"])
        cs_new.addRelUnct("norm", self.epsErr/self.eps)
        cs_new.addRelUnct("norm", self.br1.d["tot"]/self.br1.d["val"])
        cs_new.addRelUnct("norm", self.br2.d["tot"]/self.br2.d["val"])
        cs_new.d["tot"] = 0.
        cs_new.calculateErrors()
        return cs_new

class uncPT():
    def __init__(self, bins=nPTBins):
        self.d = {}
        self.bins = bins
        # self.binning = array("d", (bins+1)*[0])
        for key in ["val","stat","uncorr","corr","systTot","SS","norm","tot"]:
            self.d[key] = array("d", bins*[0])

    def __call__(self, iB, keys=["val","stat","uncorr","corr","norm","tot"]):
        print([ key for key in keys])
        for key in keys:
            print(f"{self.d[key][iB]:.3f}",end=" ")
        print("")
        # return self.d

    def __getitem__(self, iB):
        keys=["val","stat","uncorr","corr","norm","tot"]
        d = {}
        for key in keys:
            d[key] = self.d[key][iB]
        return d

    def __setitem__(self, iB, d):
        for key in d:
            self.d[key][iB] = d[key]
        self.calculateErrors(iB)

    def append(self, d):
        self.bins += 1
        for key in self.d:
            if key in d:
                self.d[key].append(d[key])
            else:
                self.d[key].append(0.)
        # self.d[-1] = d
        
    def get(self, iB, keys=["val","stat","uncorr","corr","norm","tot"]):
        d = {}
        for key in keys:
            d[key] = self.d[key][iB]
        return d

    def calculateErrors(self,iB):
        Val    = self.d["val"][iB]
        Stat   = self.d["stat"][iB]
        Uncorr = self.d["uncorr"][iB]
        Corr   = self.d["corr"][iB]
        Norm   = self.d["norm"][iB]

        self.d["systTot"][iB]    = (Uncorr**2 + Corr**2)**0.5
        self.d["SS"][iB]         = (Uncorr**2 + Corr**2 + Stat**2)**0.5
        self.d["tot"][iB]        = (Uncorr**2 + Corr**2 + Stat**2 + Norm**2)**0.5

    def copyRelUncts(self,origin,iB,val):
        oldVal  = origin.d["val"][iB]
        self.d["val"][iB] = val
        for key in ["stat","stat","uncorr","corr","systTot","SS","norm"]:
            self.d[key][iB] = val*origin.d[key][iB]/oldVal


    def addRelUnct(self,key,iB,valRel):
        if key=="uncorr" or key=="corr":
            otherKeys = ["systTot","SS","tot"]
        if key=="norm" :
            otherKeys = ["tot"]

        # self.d[key][iB] = self.d["val"][iB] * valRel

        otherKeys.append(key)
        for kkey in otherKeys:
            oldUnc = self.d[kkey][iB]
            val    = self.d["val"][iB]
            self.d[kkey][iB] = val*((oldUnc/val)**2 + valRel**2)**0.5


    def calculateTotalCS(self, bins=None, binning=PTBinHW):

        # IntCS = unc()
        totCS      = 0
        stat       = 0
        systUncorr = 0
        systCorr   = 0
        expUnc     = 0
        syst       = 0
        norm       = 0
        tot        = 0
        if bins == None:
            bins = self.bins

        for iB in range(bins):
            valPT = self.d["val"][iB]*binning[iB]*2
            totCS      += valPT
            stat       += (self.d["stat"][iB]*binning[iB]*2)**2     # sqrt(sum(dsigma**2)/n)
            systUncorr += (self.d["uncorr"][iB]*binning[iB]*2)**2 # sqrt(sum((dsigma/sigma)**2)) good for jpsi
            # systUncorr += (self.d["uncorr"][iB]*binning[iB]*2) # sum((dsigma/sigma)**2)
            # systUncorr += (self.d["uncorr"][iB]*2)**2 # sqrt(sum((dsigma/sigma)**2))

            # relCorr     = (self.d["corr"][iB]*binning[iB]*2/valPT)**2 #sqr average relative between bins
            relCorr     = self.d["corr"][iB]*binning[iB]*2/valPT #average relative between bins
            systCorr   += relCorr/float(bins)

            norm = self.d["norm"][iB]*binning[iB]*2/valPT

        # stat = (stat/bins)**0.5
        stat = (stat)**0.5
        norm     = norm * totCS

        # systCorr = 1.11 * systCorr * totCS # magic factor to have correct jpsi uncert.
        # systCorr = 1.5**0.5 * systCorr * totCS # magic factor to have correct jpsi uncert.
        systCorr = systCorr * totCS 
        # systCorr = (systCorr)**0.5 * totCS

        systUncorr = (systUncorr)**0.5 
        # systUncorr = systUncorr 

        syst = (systUncorr**2 + systCorr**2)**0.5

        expUnc = (systUncorr**2 + systCorr**2 + stat**2)**0.5

        tot = (stat**2 + systUncorr**2 + systCorr**2 + norm**2 )**0.5
        self.IntegralXsec = {"val"     :totCS,
                             "stat"    :stat,
                             "uncorr"  :systUncorr,
                             "corr"    :systCorr,
                             "systTot" :syst,
                             "norm"    :norm,
                             "SS"      :expUnc,
                             "tot"     :tot}

    def getCS(self, iB=None, keys=["val","stat","uncorr","corr","norm"]):
        strCS = ""
        csDict = {}
        if iB==None:
            csDict = self.IntegralXsec
        else:
            csDict = self[iB]
        # print(csDict)            
        for key in keys[:-1]:
                # print(f"{self.IntegralXsec[key]:.3f}",end=" ")
            strCS += f"{csDict[key]:.2f} \\pm "
        strCS += f"{csDict[keys[-1]]:.2f}"
        # print("")
        return strCS

    def getIntegralXsec(self, keys=["val","stat","uncorr","corr","norm"]):
        strCS = ""
        for key in keys[:-1]:
            # print(f"{self.IntegralXsec[key]:.3f}",end=" ")
            strCS += f"{self.IntegralXsec[key]:.3f} \\pm "
        strCS += f"{self.IntegralXsec[keys[-1]]:.3f}"
        # print("")
        return strCS

class uncPTAlt():
    def __init__(self, bins=nPTBins):
        self.d = []
        for i in range(nPTBins):
            self.d.append(unc())

    def __call__(self, iB, keys=["val","stat","uncorr","corr","tot"]):
        self.d[iB]()
        # return self.d

    def __getitem__(self, iB, keys=["val","stat","uncorr","corr","tot"]):
        d = self.d[iB]
        return d

    def __setitem__(self, iB, d):
        self.d[iB] = d

    def append(self, d):
        self.d.append(d)
        # self.d[-1] = d
        
    def calculateErrors(self,iB):
        self.d[iB].calculateErrors()

    def copyRelUncts(self,origin,iB,val):
        self.d[iB].copyRelUncts(origin, val)


    def addRelUnct(self,key,iB,valRel):
        self.d[iB].addRelUnct(key,valRel)


    def calculateTotalCS(self, bins=nPTBins, binning=PTBinHW):
        totCS      = 0
        stat       = 0
        systUncorr = 0
        systCorr   = 0
        syst       = 0
        norm       = 0
        tot        = 0
        for iB in range(bins):
            valPT = self.d[iB]["val"]*binning[iB]*2
            totCS      += valPT
            stat       += (self.d[iB]["stat"]*binning[iB]*2)**2     # sqrt(sum(dsigma**2)/n)
            systUncorr += (self.d[iB]["uncorr"]*binning[iB]*2)**2 # sqrt(sum((dsigma/sigma)**2))
            # systUncorr += (self.d["uncorr"][iB]*2)**2 # sqrt(sum((dsigma/sigma)**2))

            relCorr     = self.d[iB]["corr"]*binning[iB]*2/valPT #average relative between bins
            systCorr   += relCorr/float(bins)

            norm = self.d[iB]["norm"]*binning[iB]*2/valPT

        # stat = (stat/bins)**0.5
        stat = (stat)**0.5
        systCorr = systCorr * totCS
        norm     = norm * totCS

        systUncorr = (systUncorr)**0.5 

        systTot = (systUncorr**2 + systCorr**2)**0.5
        tot = (stat**2 + systUncorr**2 + systCorr**2 + norm**2 )**0.5
        self.IntegralXsec = {"val"     :totCS,
                             "stat"    :stat,
                             "uncorr"  :systUncorr,
                             "corr"    :systCorr,
                             "systTot" :systTot,
                             "norm"    :norm,
                             "tot"     :tot}

    def getIntegralXsec(self, keys=["val","stat","uncorr","corr","tot"]):
        strCS = ""
        for key in keys:
            # print(f"{self.IntegralXsec[key]:.3f}",end=" ")
            strCS += f"{self.IntegralXsec[key]:.3f} "
        # print("")
        return strCS