from ROOT import gStyle, gROOT, TFile, TH1F, TH1D, TGraph, TCanvas, TF1, TLegend, kBlue
import ROOT.Math as m
from ROOT.TMath import Sq, Sqrt
from array import array
import numpy as np
import sys 
from cstools import *
import logging
import csv

homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

'''
# #calculation of J/psi CS in bins of Y (PT 5.0-14.0 GeV)
def calc_y_bins():

    gStyle.SetOptFit(0)
    gStyle.SetOptStat(0)

    f_pr = TFile(dataDir+"../etacToPpbar/Results/HEPData-ins1391511-v2-Table_1.root","READ")
    f_fb = TFile(dataDir+"../etacToPpbar/Results/HEPData-ins1391511-v2-Table_2.root","READ")

    cs_jpsi_pr = np.array([])
    cs_jpsi_fb = np.array([])

    import os, sys
    sys.stdout = open(homeDir+"/CSJpsi_Y.txt", 'w' )
    #fo = open(homeDir+"/CSJpsi_Y.txt","w")

    #h_pr, h_pr_st, h_pr_su, h_pr_sc = TH1D(), TH1D(), TH1D(), TH1D()
    #h_fb, h_fb_st, h_fb_su, h_fb_sc = TH1D(), TH1D(), TH1D(), TH1D()

    for iY in range(nYBins):

        cs_jpsi_pr = np.append(cs_jpsi_pr, uncPT())
        cs_jpsi_fb = np.append(cs_jpsi_fb, uncPT())

        h_pr = f_pr.Get("Table {pr}/Hist1D_y{}".format(iY+1))
        h_pr_st = f_pr.Get("Table {pr}/Hist1D_y{}_e1".format(iY+1))  #stat
        h_pr_sc = f_pr.Get("Table {pr}/Hist1D_y{}_e2".format(iY+1))  #syst corr
        h_pr_su = f_pr.Get("Table {pr}/Hist1D_y{}_e3".format(iY+1))  #syst uncorr


        h_fb = f_fb.Get("Table 2/Hist1D_y{}".format(iY+1))
        h_fb_st = f_fb.Get("Table 2/Hist1D_y{}_e1".format(iY+1))  #stat
        h_fb_sc = f_fb.Get("Table 2/Hist1D_y{}_e2".format(iY+1))  #syst corr
        h_fb_su = f_fb.Get("Table 2/Hist1D_y{}_e3".format(iY+1))  #syst uncorr

        for iPT in range(nPTBins_tot):

            cs_jpsi_pr[iY].d["val"][iPT] = h_pr.GetBinContent(iPT+6)
            cs_jpsi_pr[iY].d["stat"][iPT] = h_pr_st.GetBinContent(iPT+6)
            cs_jpsi_pr[iY].d["uncorr"][iPT] = h_pr_su.GetBinContent(iPT+6)
            cs_jpsi_pr[iY].d["corr"][iPT] = h_pr_sc.GetBinContent(iPT+6)
            #cs_jpsi_pr[iY]["norm"][iPT] = 0.
            cs_jpsi_pr[iY].calculateErrors(iPT)

            cs_jpsi_fb[iY].d["val"][iPT] = h_fb.GetBinContent(iPT+6)
            cs_jpsi_fb[iY].d["stat"][iPT] = h_fb_st.GetBinContent(iPT+6)
            cs_jpsi_fb[iY].d["uncorr"][iPT] = h_fb_su.GetBinContent(iPT+6)
            cs_jpsi_fb[iY].d["corr"][iPT] = h_fb_sc.GetBinContent(iPT+6)
            #cs_jpsi_fb[iY]["norm"][iPT] = 0.
            cs_jpsi_fb[iY].calculateErrors(iPT)

        cs_jpsi_pr[iY].calculateTotalCS()
        cs_jpsi_fb[iY].calculateTotalCS()

        cs_jpsi_pr_tot = cs_jpsi_pr[iY].IntegralXsec
        print(cs_jpsi_pr_tot["val"], \
              cs_jpsi_pr_tot["stat"], \
              cs_jpsi_pr_tot["uncorr"], \
              cs_jpsi_pr_tot["corr"], \
              (cs_jpsi_pr_tot["stat"]**2 + cs_jpsi_pr_tot["uncorr"]**2 + cs_jpsi_pr_tot["corr"]**2)**0.5)

        print("\n")

        cs_jpsi_fb_tot = cs_jpsi_fb[iY].IntegralXsec
        print(cs_jpsi_fb_tot["val"], \
              cs_jpsi_fb_tot["stat"], \
              cs_jpsi_fb_tot["uncorr"], \
              cs_jpsi_fb_tot["corr"], \
              (cs_jpsi_fb_tot["stat"]**2 + cs_jpsi_fb_tot["uncorr"]**2 + cs_jpsi_fb_tot["corr"]**2)**0.5)

    #PTBins_pr = np.array([4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5, 12.5, 13.5])
    #PTBins_fb = np.array([4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5, 12.5, 13.5])

    #YBins_pr = np.array([2.25, 2.75, 3.25, 3.75])
    #YBins_fb = np.array([2.25, 2.75, 3.25, 3.75])



    sys.stdout.close()
    f_pr.Close()
    f_fb.Close()
'''

'''
#calculation of J/psi CS in bin of PT (6.5-8 GeV)
def extrapol(src="prompt", sqrtS:int=13):

    gStyle.SetOptFit(0)
    gStyle.SetOptStat(0)

    PTBins1 = array("d",[4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5, 12.5, 13.5])
    PTBins2 = array("d",[4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5, 12.5, 13.5])

    if sqrtS==13:
        cSJpsiPr = array("d",[ 1273, 703.7, 376.8, 199.7, 113.8, 66.5, 39.9, 25.1, 15.4, 10.1])
        cSJpsiSec = array("d",[ 242.7, 146.3, 94.6, 57.7, 35.8, 25.5, 17.0, 12.1, 8.1, 5.9])
        cSJpsiPrErrSystUncorr = array("d",[ 9, 6, 3.7, 2.4, 1.6, 1.2, 0.9, 0.7, 0.5, 0.4])
        cSJpsiSecErrSystUncorr  = array("d",[  1.9, 1.3, 0.9, 0.7, 0.5, 0.5, 0.4, 0.3, 0.3, 0.2])
        cSJpsiPrErrSystCorr = array("d",[ 72, 38.9, 20.5, 10.8, 6.1, 3.6, 2.1, 1.3, 0.8, 0.5])
        cSJpsiSecErrSystCorr = array("d",[  13.8, 8.1, 5.1, 3.1, 1.9, 1.4, 0.9, 0.6, 0.4, 0.3])
        cSJpsiPrErrStat = array("d",[ 5, 3.8, 2.6, 1.7, 1.2, 0.9, 0.7, 0.6, 0.4, 0.3 ])
        cSJpsiSecErrStat = array("d",[ 2.5, 1.8, 1.3, 1.0, 0.7, 0.6, 0.5, 0.4, 0.3, 0.3])
    if sqrtS==8:
        cSJpsiPr = array("d",[ 1273, 703.7, 376.8, 199.7, 113.8, 66.5, 39.9, 25.1, 15.4, 10.1])
        cSJpsiSec = array("d",[ 242.7, 146.3, 94.6, 57.7, 35.8, 25.5, 17.0, 12.1, 8.1, 5.9])
        cSJpsiPrErrSystUncorr = array("d",[ 9, 6, 3.7, 2.4, 1.6, 1.2, 0.9, 0.7, 0.5, 0.4])
        cSJpsiSecErrSystUncorr  = array("d",[  1.9, 1.3, 0.9, 0.7, 0.5, 0.5, 0.4, 0.3, 0.3, 0.2])
        cSJpsiPrErrSystCorr = array("d",[ 72, 38.9, 20.5, 10.8, 6.1, 3.6, 2.1, 1.3, 0.8, 0.5])
        cSJpsiSecErrSystCorr = array("d",[  13.8, 8.1, 5.1, 3.1, 1.9, 1.4, 0.9, 0.6, 0.4, 0.3])
        cSJpsiPrErrStat = array("d",[ 5, 3.8, 2.6, 1.7, 1.2, 0.9, 0.7, 0.6, 0.4, 0.3 ])
        cSJpsiSecErrStat = array("d",[ 2.5, 1.8, 1.3, 1.0, 0.7, 0.6, 0.5, 0.4, 0.3, 0.3])


    hJpsiPr = TH1D("hJpsiPr", "hJpsiPr", 6, 4.0, 10.0)
    hJpsiSec = TH1D("hJpsiSec", "hJpsiSec", 6, 4.0, 10.0)

    binValPr = 0.
    binValSec = 0.
    binValPrErrStat = 0.
    binValPrErrSystUncorr = 0.
    binValPrErrSystCorr = 0.
    binValSecErrStat = 0.
    binValSecErrSystUncorr = 0.
    binValSecErrSystCorr = 0.

    yD1 = []
    yD2 = []

    for iB in range(6):

      hJpsiPr.SetBinContent(iB+1,cSJpsiPr[iB])
      hJpsiPr.SetBinError(iB+1, m.Sqrt(cSJpsiPrErrSystCorr[iB]*cSJpsiPrErrSystCorr[iB] + cSJpsiPrErrSystUncorr[iB]*cSJpsiPrErrSystUncorr[iB] + cSJpsiPrErrStat[iB]*cSJpsiPrErrStat[iB]))
      hJpsiSec.SetBinContent(iB+1,cSJpsiSec[iB])
      hJpsiSec.SetBinError(iB+1,m.Sqrt(cSJpsiSecErrSystCorr[iB]*cSJpsiSecErrSystCorr[iB] + cSJpsiSecErrSystUncorr[iB]*cSJpsiSecErrSystUncorr[iB] + cSJpsiSecErrStat[iB]*cSJpsiSecErrStat[iB]))

      yD1.append(cSJpsiPr[iB])
      yD2.append(cSJpsiSec[iB])

    x1 = array("f",30*[0])
    y1 = array("f",30*[0])
    x2 = array("f",30*[0])
    y2 = array("f",30*[0])



    inter1 = m.Interpolator(6, m.Interpolation.kPOLYNOMIAL)
    inter2 = m.Interpolator(6, m.Interpolation.kPOLYNOMIAL)
    inter1.SetData(6, PTBins1, cSJpsiPr)
    inter2.SetData(6, PTBins2, cSJpsiSec)

    nInt = 30
    for i in range(nInt):

      x1[i] = ( i*(9.5-4.5)/(nInt-1.) + 4.5)
      y1[i] = (inter1.Eval(x1[i]))
      #print(y1[i] + "\n")
      x2[i] = (i*(9.5-4.5)/(nInt-1.) + 4.5)
      y2[i] = (inter2.Eval(x2[i]))


    fitFunc1 = TF1("fitFunc1", "expo(0)", 4.0, 10.0)
    fitFunc2 = TF1("fitFunc2", "expo(0)", 4.0, 10.0)

    cs_pr = uncPT()

    rPr = hJpsiPr.Fit(fitFunc1,"S")
    binValPr = fitFunc1.Integral(6.5, 7.0)
    binValPrErrStat = (binValPr/fitFunc1.Integral(6.0, 7.0))*cSJpsiPrErrStat[2]
    binValPrErrSystUncorr = (binValPr/fitFunc1.Integral(6.0, 7.0))*cSJpsiPrErrSystUncorr[2]
    binValPrErrSystCorr = (binValPr/fitFunc1.Integral(6.0, 7.0))*cSJpsiPrErrSystCorr[2]
    #   binValPrErr = fitFunc1.IntegralError(6.5,7.0, rPr.GetParams(), rPr.GetCovarianceMatrix().GetMatrixArray())


    rSec = hJpsiSec.Fit(fitFunc2,"S")
    binValSec = fitFunc2.Integral(6.5, 7.0)
    binValSecErrStat = (binValSec/fitFunc2.Integral(6.0, 7.0))*cSJpsiSecErrStat[2]
    binValSecErrSystUncorr = (binValSec/fitFunc2.Integral(6.0, 7.0))*cSJpsiSecErrSystUncorr[2]
    binValSecErrSystCorr = (binValSec/fitFunc2.Integral(6.0, 7.0))*cSJpsiSecErrSystCorr[2]
    #    binValSecErr = fitFunc2.IntegralError(6.5,7.0, rSec.GetParams(), rSec.GetCovarianceMatrix().GetMatrixArray())

    canv = TCanvas("canv","J/Psi cross-section",55,55,600,800)
    canv.Divide(1,2)

    canv.cd(1)
    hJpsiPr.Draw()
    hJpsiPr.SetTitle("J/#psi prompt cross-section")
    hJpsiPr.GetXaxis().SetTitle("p_{T} [GeV]")
    hJpsiPr.GetYaxis().SetTitle("#sigma [nb/ GeV/c]")
    fitFunc1.Draw("SAME")

    gi1 = TGraph(30, x1, y1)
    gi1.SetLineColor(kBlue)
    gi1.SetLineWidth(2)
    gi1.SetTitle("Integral")
    gi1.Draw("SAME L")

    leg = TLegend(0.55, 0.7, 0.85, 0.85)
    leg.AddEntry(hJpsiPr,"J/#psi cross-section","lep")
    leg.AddEntry(fitFunc1,"e^{ax+b} fit","l")
    leg.AddEntry(gi1,"pol3 interpolation","l")
    leg.SetBorderSize(0)
    leg.Draw()

    canv.cd(2)
    hJpsiSec.Draw()
    hJpsiSec.SetTitle("J/#psi from-b cross-section")
    hJpsiSec.GetXaxis().SetTitle("p_{T}, GeV/c")
    hJpsiSec.GetYaxis().SetTitle("#sigma [nb/ GeV/c]")
    fitFunc2.Draw("SAME")

    gi2 = TGraph(30, x2, y2)
    gi2.SetLineColor(kBlue)
    gi2.SetLineWidth(2)
    gi2.SetTitle("Integral")
    gi2.Draw("SAME L")


    leg.Draw()


    canv.SaveAs(homeDir+"/CS/CSfit.pdf")

    if sqrtS==13:
        fo = open(homeDir+"/CS/CSJpsi13_new.txt","w")
    if sqrtS==8:
        fo = open(homeDir+"/CS/CSJpsi8_new.txt","w")

    fo.write( str(binValPr+cSJpsiPr[3]) + " ")
    fo.write( str(m.Sqrt(m.Sq(binValPrErrStat)+m.Sq(cSJpsiPrErrStat[3]))) + "  ")
    fo.write( str(m.Sqrt(m.Sq(binValPrErrSystUncorr)+m.Sq(cSJpsiPrErrSystUncorr[3]))) + "  ")
    fo.write( str(m.Sqrt(m.Sq(binValPrErrSystCorr)+m.Sq(cSJpsiPrErrSystCorr[3]))) + "  ")
    fo.write( str(m.Sqrt(m.Sq(binValPrErrStat)+m.Sq(cSJpsiPrErrStat[3]) + m.Sq(binValPrErrSystUncorr)+m.Sq(cSJpsiPrErrSystUncorr[3]) + m.Sq(binValPrErrSystCorr)+m.Sq(cSJpsiPrErrSystCorr[3]))) + "\n")
    for i in range(2, 5):
        fo.write( str(cSJpsiPr[2*i]+cSJpsiPr[2*i+1]) + "  ")
        fo.write( str(m.Sqrt(m.Sq(cSJpsiPrErrStat[2*i])+m.Sq(cSJpsiPrErrStat[2*i+1]))) + "  ")
        fo.write( str(m.Sqrt(m.Sq(cSJpsiPrErrSystUncorr[2*i])+m.Sq(cSJpsiPrErrSystUncorr[2*i+1]))) + "  ")
        fo.write( str(m.Sqrt(m.Sq(cSJpsiPrErrSystCorr[2*i])+m.Sq(cSJpsiPrErrSystCorr[2*i+1]))) + "  ")
        fo.write( str(m.Sqrt(m.Sq(cSJpsiPrErrStat[2*i])+m.Sq(cSJpsiPrErrStat[2*i+1])+m.Sq(cSJpsiPrErrSystUncorr[2*i])+m.Sq(cSJpsiPrErrSystUncorr[2*i+1]) + +m.Sq(cSJpsiPrErrSystCorr[2*i])+m.Sq(cSJpsiPrErrSystCorr[2*i+1]))) +  "\n ")

    fo.write("\n")

    fo.write( str(binValSec+cSJpsiSec[3]) + " ")
    fo.write( str(m.Sqrt(m.Sq(binValSecErrStat)+m.Sq(cSJpsiSecErrStat[3]))) + "  ")
    fo.write( str(m.Sqrt(m.Sq(binValSecErrSystUncorr)+m.Sq(cSJpsiSecErrSystUncorr[3]))) + "  ")
    fo.write( str(m.Sqrt(m.Sq(binValSecErrSystCorr)+m.Sq(cSJpsiSecErrSystCorr[3]))) + "  ")
    fo.write( str(m.Sqrt(m.Sq(binValSecErrStat)+m.Sq(cSJpsiSecErrStat[3]) + m.Sq(binValSecErrSystUncorr)+m.Sq(cSJpsiSecErrSystUncorr[3]) + m.Sq(binValSecErrSystCorr)+m.Sq(cSJpsiSecErrSystCorr[3]))) +  "\n")
    for i in range(2, 5):
        fo.write( str(cSJpsiSec[2*i]+cSJpsiSec[2*i+1]) + "  ")
        fo.write( str(m.Sqrt(m.Sq(cSJpsiSecErrStat[2*i])+m.Sq(cSJpsiSecErrStat[2*i+1]))) + "  ")
        fo.write( str(m.Sqrt(m.Sq(cSJpsiSecErrSystUncorr[2*i])+m.Sq(cSJpsiSecErrSystUncorr[2*i+1]))) + "  ")
        fo.write( str(m.Sqrt(m.Sq(cSJpsiSecErrSystCorr[2*i])+m.Sq(cSJpsiSecErrSystCorr[2*i+1]))) + "  ")
        fo.write( str(m.Sqrt(m.Sq(cSJpsiSecErrStat[2*i])+m.Sq(cSJpsiSecErrStat[2*i+1])+m.Sq(cSJpsiSecErrSystUncorr[2*i])+m.Sq(cSJpsiSecErrSystUncorr[2*i+1]) +m.Sq(cSJpsiSecErrSystCorr[2*i])+m.Sq(cSJpsiSecErrSystCorr[2*i+1]))) + "\n ")

    fo.write("\n")
    fo.close()
'''


#calculation of J/psi CS in bin of PT (6.5-8 GeV)
def extrapol(src="prompt", pmin:float=3., pmax:float=14.0, sqrtS:int=13):

    binedge = 6.5
    # gStyle.SetOptFit(0)
    # gStyle.SetOptStat(0)

    pr = 4 if src=="prompt" else 5

    # Get histogram from J/psi prod measurement
    filename = f"{dataDir}/results/HEPData/HEPData-ins1391511-v2-Table_{pr}.root"
    f = TFile(filename,"READ")

    h = f.Get(f"Table {pr}/Hist1D_y1")
    h_st = f.Get(f"Table {pr}/Hist1D_y1_e1")  #stat
    h_sc = f.Get(f"Table {pr}/Hist1D_y1_e2")  #syst corr
    h_su = f.Get(f"Table {pr}/Hist1D_y1_e3")  #syst uncorr

    ptlow = h.FindBin(pmin)
    pthigh =  h.FindBin(pmax)
    nbins = pthigh-ptlow
    # print(ptlow, pthigh)

    # fill PT bins array for requested range
    binning = np.array([])
    s = h.GetXaxis().GetXbins().GetSize()
    b = h.GetXaxis().GetXbins().GetArray()
    for i in range(ptlow-1,pthigh):
        binning = np.append(binning, b[i])
    binninghw = (binning[1:] - binning[:-1])/2. # HW of pt bins

    h_fit = TH1F("h_fit","h_fit",nbins, pmin, pmax)
    cs_jpsi = uncPT(nbins)
    yVal = np.array([])

    for iPT in range(nbins):
        # print(iPT)
        cs_jpsi.d["val"][iPT]    = h.GetBinContent(iPT+ptlow)
        cs_jpsi.d["stat"][iPT]   = h_st.GetBinContent(iPT+ptlow)
        cs_jpsi.d["uncorr"][iPT] = h_su.GetBinContent(iPT+ptlow)
        cs_jpsi.d["corr"][iPT]   = h_sc.GetBinContent(iPT+ptlow)
        # cs_jpsi["norm"][iPT] = 0.
        cs_jpsi.calculateErrors(iPT)
        # print(f"bin: {binning[iPT+ptlow]}:{binning[iPT+ptlow+1]}")
        # print(f"bin: {h.GetBinLowEdge(iPT+ptlow)}:{h.GetBinLowEdge(iPT+ptlow)+h.GetBinWidth(iPT+ptlow)}")
        cs_jpsi(iPT)
        h_fit.SetBinContent(iPT+1, cs_jpsi.d["val"][iPT] )
        h_fit.SetBinError(iPT+1, cs_jpsi.d["tot"][iPT] )
        yVal = np.append(yVal, cs_jpsi.d["val"][iPT])


        # print(f'CS in bin {iPT+ptlow}: {cs_jpsi.d["val"][iPT]:.3f} +/- {cs_jpsi.d["tot"][iPT]:.3f}')


    cs_jpsi.calculateTotalCS(nbins, binninghw)
    cs_jpsi_tot = cs_jpsi.IntegralXsec

    # binValPr = 0.
    # binValPrErrStat = 0.
    # binValPrErrSystUncorr = 0.
    # binValPrErrSystCorr = 0.

    inter = m.Interpolator(nbins, m.Interpolation.kPOLYNOMIAL)
    inter.SetData(nbins, binning, yVal)

    nInt = 35
    x1 = array("f",nInt*[0])
    y1 = array("f",nInt*[0])

    for i in range(nInt):

      x1[i] = ( i*(pmax-pmin-1)/(nInt-1.) + pmin+0.5)
      y1[i] = (inter.Eval(x1[i]))
      #print(y1[i] + "\n")

    fitFunc = TF1("fitFunc", "expo(0)*pol2(2)", pmin, pmax)

    cs_jpsi_new = uncPT(2)
    # cs_jpsi_new[0] = cs_jpsi[0]
    # print(cs_jpsi_new(0))

    rPr = h_fit.Fit(fitFunc,"ISER")

    canv = TCanvas("canv","J/Psi cross-section",50,50,600,400)
    canv.cd()

    h_fit.Draw()
    h_fit.SetTitle("J/#psi prompt cross-section")
    h_fit.GetXaxis().SetTitle("p_{T} [GeV]")
    h_fit.GetYaxis().SetTitle("#sigma [nb/ GeV/c]")
    fitFunc.Draw("SAME")

    gi1 = TGraph(nInt, x1, y1)
    gi1.SetLineColor(kBlue)
    gi1.SetLineWidth(2)
    gi1.SetTitle("Integral")
    gi1.Draw("SAME L")

    leg = TLegend(0.55, 0.7, 0.85, 0.85)
    leg.AddEntry(h_fit,"J/#psi cross-section","lep")
    # leg.AddEntry(fitFunc,"e^{ax+b} fit","l")
    leg.AddEntry(fitFunc,"e^{ax+b}x(1+cx+dx^{2}) fit","l")
    leg.AddEntry(gi1,"pol3 interpolation","l")
    leg.SetBorderSize(0)
    leg.Draw()

    canv.SaveAs(f"{homeDir}/CS/CSJpsi{sqrtS}_{src}_fit.pdf")
    fo = open(f"{homeDir}/CS/CSJpsi{sqrtS}_{src}.txt","w")

    cs_jpsi_tot = uncPT(5)
    cs_jpsi_test = uncPT(4)

    nb = h_fit.FindBin(binedge) - 1 # count from 0 when work with arrays
    # print(f"nb: {nb}")
    cs_jpsi_new[0] = cs_jpsi[nb-1]
    cs_jpsi_new.d["val"][1] = fitFunc.Integral(6.0, binedge)
    cs_jpsi_new.d["stat"][1] = (cs_jpsi_new.d["val"][1]/fitFunc.Integral(6.0, 7.0))*cs_jpsi.d["stat"][nb]
    cs_jpsi_new.d["uncorr"][1] = (cs_jpsi_new.d["val"][1]/fitFunc.Integral(6.0, 7.0))*cs_jpsi.d["uncorr"][nb]
    cs_jpsi_new.d["corr"][1] = (cs_jpsi_new.d["val"][1]/fitFunc.Integral(6.0, 7.0))*cs_jpsi.d["corr"][nb]
    # cs_jpsi_new(0); cs_jpsi_new(1)
    cs_jpsi_new.calculateTotalCS(2, [0.5, 0.375])
    fo.write(f"{cs_jpsi_new.getIntegralXsec()} \n")
    cs_jpsi_tot[0] = cs_jpsi_new.IntegralXsec
    cs_jpsi_test[0] = cs_jpsi_new[0]
    cs_jpsi_test[1] = cs_jpsi_new[1]
    
    cs_jpsi_new.d["val"][0] = fitFunc.Integral(binedge, 7.0)
    cs_jpsi_new.d["stat"][0] = (cs_jpsi_new.d["val"][0]/fitFunc.Integral(6.0, 7.0))*cs_jpsi.d["stat"][nb]
    cs_jpsi_new.d["uncorr"][0] = (cs_jpsi_new.d["val"][0]/fitFunc.Integral(6.0, 7.0))*cs_jpsi.d["uncorr"][nb]
    cs_jpsi_new.d["corr"][0] = (cs_jpsi_new.d["val"][0]/fitFunc.Integral(6.0, 7.0))*cs_jpsi.d["corr"][nb]
    #   binValPrErr = fitFunc.IntegralError(6.5,7.0, rPr.GetParams(), rPr.GetCovarianceMatrix().GetMatrixArray())
    cs_jpsi_new[1] = cs_jpsi[nb+1]
    cs_jpsi_new.calculateTotalCS(2, [0.375, 0.5])
    # cs_jpsi_new(0); cs_jpsi_new(1)
    fo.write(f"{cs_jpsi_new.getIntegralXsec()} \n")
    # print(cs_jpsi_new.IntegralXsec)
    cs_jpsi_tot[1] = cs_jpsi_new.IntegralXsec
    cs_jpsi_test[2] = cs_jpsi_new[0]
    cs_jpsi_test[3] = cs_jpsi_new[1]
    cs_jpsi_test.calculateTotalCS(4, [0.5, 0.375, 0.375, 0.5])
    print(f"Check: {cs_jpsi_test.IntegralXsec} \n\n")
    print(f"Check2: {fitFunc.Integral(5.0, 7.0)}\n\n")
    for i in range(1, 4):
        # print(f"PT: {nb+2*i}, pt: {ptBins[i]}")
        
        cs_jpsi_new[0] = cs_jpsi[nb+2*i]
        cs_jpsi_new[1] = cs_jpsi[nb+2*i+1]
        cs_jpsi_new.calculateTotalCS(2, [0.5, 0.5])
        fo.write(f"{cs_jpsi_new.getIntegralXsec()} \n")
        cs_jpsi_tot[i+1] = cs_jpsi_new.IntegralXsec
 
    fo.write("\n")
    fo.close()
    cs_jpsi_tot.calculateTotalCS(binning=[0.375,0.375, 0.5, 0.5, 0.5])
    print(cs_jpsi_tot.getIntegralXsec())

def extrapolRel():

    gStyle.SetOptFit(0)
    gStyle.SetOptStat(0)

    PTBins1 = array("d",[0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5, 12.5, 13.5])
    PTBins2 = array("d",[0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5, 12.5, 13.5])

    cSJpsiPr = array("d", 14*[0])
    cSJpsiSec = array("d", 14*[0])
    cSJpsiPrErr = array("d", 14*[0])
    cSJpsiSecErr = array("d", 14*[0])

    fi = open(homeDir+"/CSJpsi_Run1.txt","r")

    for iPT in range(14):

        l = fi.readline()
        cSJpsiPr[iPT], cSJpsiPrErr[iPT] =  float(l.split()[0]), float(l.split()[1])

    l = fi.readline()

    for iPT in range(14):

        l = fi.readline()
        cSJpsiSec[iPT], cSJpsiSecErr[iPT] =  float(l.split()[0]), float(l.split()[1])

    fi.close()

    hJpsiPr = TH1D("hJpsiPr", "hJpsiPr", 14, 0.0, 14.0)
    hJpsiSec = TH1D("hJpsiSec", "hJpsiSec", 14, 0.0, 14.0)

    binValPr = 0.
    binValSec = 0.
    binValPrErrStat = 0.
    binValPrErrSystUncorr = 0.
    binValPrErrSystCorr = 0.
    binValSecErrStat = 0.
    binValSecErrSystUncorr = 0.
    binValSecErrSystCorr = 0.

    yD1 = []
    yD2 = []

    for iB in range(14):

      hJpsiPr.SetBinContent(iB+1,cSJpsiPr[iB])
      hJpsiPr.SetBinError(iB+1, cSJpsiPrErr[iB])
      hJpsiSec.SetBinContent(iB+1,cSJpsiSec[iB])
      hJpsiSec.SetBinError(iB+1, cSJpsiSecErr[iB])

      yD1.append(cSJpsiPr[iB])
      yD2.append(cSJpsiSec[iB])

    x1 = array("f",30*[0])
    y1 = array("f",30*[0])
    x2 = array("f",30*[0])
    y2 = array("f",30*[0])



    inter1 = m.Interpolator(14, m.Interpolation.kPOLYNOMIAL)
    inter2 = m.Interpolator(14, m.Interpolation.kPOLYNOMIAL)
    inter1.SetData(14, PTBins1, cSJpsiPr)
    inter2.SetData(14, PTBins2, cSJpsiSec)

    nInt = 10
    for i in range(nInt):

      x1[i] = ( i*(13.5-0.5)/(nInt-1.) + 0.5)
      y1[i] = (inter1.Eval(x1[i]))
      #print(y1[i] + "\n")
      x2[i] = (i*(13.5-0.5)/(nInt-1.) + 0.5)
      y2[i] = (inter2.Eval(x2[i]))


    fitFunc1 = TF1("fitFunc1", "expo(0)", 0.0, 14.0)
    fitFunc2 = TF1("fitFunc2", "expo(0)", 0.0, 14.0)

    rPr = hJpsiPr.Fit(fitFunc1,"S")
    binValPr = fitFunc1.Integral(6.5, 7.0)
    binValPrErr = (binValPr/fitFunc1.Integral(6.0, 7.0))*cSJpsiPrErr[6]
    #binValPrErrStat = (binValPr/fitFunc1.Integral(6.0, 7.0))*cSJpsiPrErrStat[2]
    #binValPrErrSystUncorr = (binValPr/fitFunc1.Integral(6.0, 7.0))*cSJpsiPrErrSystUncorr[2]
    #binValPrErrSystCorr = (binValPr/fitFunc1.Integral(6.0, 7.0))*cSJpsiPrErrSystCorr[2]
    #   binValPrErr = fitFunc1.IntegralError(6.5,7.0, rPr.GetParams(), rPr.GetCovarianceMatrix().GetMatrixArray())


    rSec = hJpsiSec.Fit(fitFunc2,"S")
    binValSec = fitFunc2.Integral(6.5, 7.0)
    binValSecErr = (binValSec/fitFunc2.Integral(6.0, 7.0))*cSJpsiSecErr[6]
    #binValSecErrStat = (binValSec/fitFunc2.Integral(6.0, 7.0))*cSJpsiSecErrStat[2]
    #binValSecErrSystUncorr = (binValSec/fitFunc2.Integral(6.0, 7.0))*cSJpsiSecErrSystUncorr[2]
    #binValSecErrSystCorr = (binValSec/fitFunc2.Integral(6.0, 7.0))*cSJpsiSecErrSystCorr[2]
    #    binValSecErr = fitFunc2.IntegralError(6.5,7.0, rSec.GetParams(), rSec.GetCovarianceMatrix().GetMatrixArray())

    canv = TCanvas("canv","J/Psi cross-section",55,55,600,800)
    canv.Divide(1,2)

    canv.cd(1)
    hJpsiPr.Draw()
    hJpsiPr.SetTitle("J/#psi prompt cross-section")
    hJpsiPr.GetXaxis().SetTitle("p_{T} [GeV]")
    hJpsiPr.GetYaxis().SetTitle("#sigma [nb/ GeV/c]")
    fitFunc1.Draw("SAME")

    gi1 = TGraph(nInt, x1, y1)
    gi1.SetLineColor(kBlue)
    gi1.SetLineWidth(2)
    gi1.SetTitle("Integral")
    gi1.Draw("SAME L")

    leg = TLegend(0.55, 0.7, 0.85, 0.85)
    leg.AddEntry(hJpsiPr,"J/#psi cross-section","lep")
    leg.AddEntry(fitFunc1,"e^{ax+b} fit","l")
    leg.AddEntry(gi1,"pol3 interpolation","l")
    leg.SetBorderSize(0)
    leg.Draw()

    canv.cd(2)
    hJpsiSec.Draw()
    hJpsiSec.SetTitle("J/#psi from-b cross-section")
    hJpsiSec.GetXaxis().SetTitle("p_{T}, GeV/c")
    hJpsiSec.GetYaxis().SetTitle("#sigma [nb/ GeV/c]")
    fitFunc2.Draw("SAME")

    gi2 = TGraph(nInt, x2, y2)
    gi2.SetLineColor(kBlue)
    gi2.SetLineWidth(2)
    gi2.SetTitle("Integral")
    gi2.Draw("SAME L")


    leg.Draw()


    canv.SaveAs(homeDir+"/CS/CSJpsiRelFit.pdf")

    fo = open(homeDir+"/CS/CSJpsi_8.txt","w")

    ptBins = {7.25, 9.0, 11.0, 13.0}
    for i in range(3, 7):

        fo.write( "%4.3f  "%((cSJpsiPr[2*i]/cSJpsiPrErr[2*i]**2 + cSJpsiPr[2*i+1]/cSJpsiPrErr[2*i+1]**2)/(1/cSJpsiPrErr[2*i]**2+1/cSJpsiPrErr[2*i+1]**2)))
        fo.write( "%4.3f  \n"%(1/(1/cSJpsiPrErr[2*i]**2+1/cSJpsiPrErr[2*i+1]**2)**0.5))

    fo.write("\n")

    for i in range(3, 7):

        fo.write( "%4.3f  "%((cSJpsiSec[2*i]/cSJpsiSecErr[2*i]**2 + cSJpsiSec[2*i+1]/cSJpsiSecErr[2*i+1]**2)/(1/cSJpsiSecErr[2*i]**2+1/cSJpsiSecErr[2*i+1]**2)))
        fo.write( "%4.3f  \n"%(1/(1/cSJpsiSecErr[2*i]**2+1/cSJpsiSecErr[2*i+1]**2)**0.5))

    fo.write("\n")

    fo.close()

#calculation of J/psi CS in bin of PT (6.5-8 GeV) for Y in [2, 4] reading from a *.csv file
def extrapolFile(src="prompt", pmin:float=3., pmax:float=14.0, sqrtS:int=13):

    binedge = 6.5
    pmin_glob = 5.0
    pmax_glob = 14.0
    # gStyle.SetOptFit(0)
    # gStyle.SetOptStat(0)

    col_list = ['val','stat','uncorr','corr', 'tot']
    csv_file = f"{homeDir}/CS_jpsi_{src}_int_inY4.csv"

    # Get values from J/psi prod measurement
    cs_jpsi = uncPT(0)
    ptlist = np.linspace(pmin, pmax_glob, int(pmax_glob-pmin+1))

    d = []
    i = 0 # to trace if the pt-binning is OKish
    try:
        with open(csv_file) as csvfile:
            reader = csv.DictReader(csvfile, delimiter=',',quoting=csv.QUOTE_NONNUMERIC)
            for row in reader:
                if row["ptmin"] == ptlist[i] and i<ptlist.size-1:
                    d.append(row)
                    logging.debug(row)  
                    i+=1
    except IOError:
        logging.error("I/O error")

    logging.debug(d, stacklevel=3)

    nbins = int(pmax_glob-pmin)
    logging.debug(f"Number of pT bins: {nbins}",stacklevel=1)
    # # print(ptlow, pthigh)

    # # fill PT bins array for requested range
    binning = np.linspace(pmin, pmax_glob, int(pmax_glob-pmin+1))
    binninghw = (binning[1:] - binning[:-1])/2. # HW of pt bins

    h_fit = TH1F("h_fit","h_fit",nbins, pmin, pmax_glob)
    cs_jpsi = uncPT(0)
    yVal = np.array([])


    for iPT in range(nbins):
    #     # print(iPT)
    
        d[iPT].pop("ptmin")
        d[iPT].pop("ptmax")
        logging.debug(d[iPT],stacklevel=4)
        cs_jpsi.append(d[iPT])
        logging.debug(cs_jpsi[iPT],stacklevel=4)
        # cs_jpsi.calculateErrors(iPT)
        h_fit.SetBinContent(iPT+1, cs_jpsi.d["val"][iPT] )
        h_fit.SetBinError(iPT+1, cs_jpsi.d["tot"][iPT] )
        yVal = np.append(yVal, cs_jpsi.d["val"][iPT])


    #     # print(f'CS in bin {iPT+ptlow}: {cs_jpsi.d["val"][iPT]:.3f} +/- {cs_jpsi.d["tot"][iPT]:.3f}')


    cs_jpsi.calculateTotalCS(nbins, binninghw)
    cs_jpsi_tot = cs_jpsi.IntegralXsec

    # # binValPr = 0.
    # # binValPrErrStat = 0.
    # # binValPrErrSystUncorr = 0.
    # # binValPrErrSystCorr = 0.

    inter = m.Interpolator(nbins, m.Interpolation.kPOLYNOMIAL)
    inter.SetData(nbins, binning, yVal)

    nInt = 26
    x1 = array("f",nInt*[0])
    y1 = array("f",nInt*[0])
    xstep = (pmax_glob-pmin)/(nInt-1.)

    for i in range(nInt):

      x1[i] = ( i*xstep + pmin )
      y1[i] = (inter.Eval(x1[i]))
      logging.debug(f"{y1[i]}")

    # fitFunc = TF1("fitFunc", "expo(0)*pol1(2)", pmin, pmax)
    # fitFunc = TF1("fitFunc", "TMath::Exp([0]*x+[1])*(1+[2]*x+[3]*x*x)", pmin, pmax)
    fitFunc = TF1("fitFunc", "[0] + [1]/(x) + [2]/(x*x*x)", pmin, pmax)

    cs_jpsi_new = uncPT(2)
    # cs_jpsi_new[0] = cs_jpsi[0]
    # print(cs_jpsi_new(0))

    # rPr = h_fit.Fit(fitFunc,"ISER")
    rPr = h_fit.Fit(fitFunc,"IFR")

    canv = TCanvas("canv","J/Psi cross-section",50,50,550,400)
    canv.cd()

    h_fit.Draw()
    h_fit.SetTitle("J/#psi prompt cross-section")
    h_fit.GetXaxis().SetTitle("p_{T} [GeV]")
    h_fit.GetYaxis().SetTitle("#sigma [nb/ GeV/c]")
    fitFunc.Draw("SAME")

    gi1 = TGraph(nInt, x1, y1)
    gi1.SetLineColor(kBlue)
    gi1.SetLineWidth(2)
    gi1.SetTitle("Integral")
    gi1.Draw("SAME L")

    leg = TLegend(0.55, 0.7, 0.85, 0.85)
    leg.AddEntry(h_fit,"J/#psi cross-section","lep")
    # leg.AddEntry(fitFunc,"e^{ax+b} fit","l")
    leg.AddEntry(fitFunc,"e^{ax+b}\times(1+cx+dx^{2}) fit","l")
    leg.AddEntry(gi1,"pol3 interpolation","l")
    leg.SetBorderSize(0)
    leg.Draw()

    canv.SaveAs(f"{homeDir}/CS/CSJpsi{sqrtS}_{src}_fit_inY4.pdf")
    fout = f"{homeDir}/CS/CSJpsi{sqrtS}_{src}_inY4.txt"
    fo = open(fout,"w")

    cs_jpsi_tot = uncPT(5)
    cs_jpsi_test = uncPT(4)
    cs_jpsi_true = uncPT(0)

    nb = h_fit.FindBin(binedge) - 1 # count from 0 when work with arrays
    logging.debug(f"nb: {nb}")
    cs_jpsi_true.append(cs_jpsi[nb-1])
    cs_jpsi_true.append(cs_jpsi[nb])
    cs_jpsi_true.append(cs_jpsi[nb+1])
    cs_jpsi_true.calculateTotalCS(3, [0.5,0.5,0.5])
    
    cs_jpsi_new[0] = cs_jpsi[nb-1]
    cs_jpsi_new.d["val"][1] = fitFunc.Integral(6.0, binedge)/(binedge-6.0)#/fitFunc.Integral(6.0, 7.0)*cs_jpsi.d["val"][nb]
    cs_jpsi_new.d["stat"][1] = (cs_jpsi_new.d["val"][1]/cs_jpsi.d["val"][nb])*cs_jpsi.d["stat"][nb]
    cs_jpsi_new.d["uncorr"][1] = (cs_jpsi_new.d["val"][1]/cs_jpsi.d["val"][nb])*cs_jpsi.d["uncorr"][nb]
    cs_jpsi_new.d["corr"][1] = (cs_jpsi_new.d["val"][1]/cs_jpsi.d["val"][nb])*cs_jpsi.d["corr"][nb]
    # cs_jpsi_new(0); cs_jpsi_new(1)
    cs_jpsi_new.calculateTotalCS(2, [0.5, 0.25])
    fo.write(f"{cs_jpsi_new.getIntegralXsec(col_list)} \n")
    cs_jpsi_tot[0] = cs_jpsi_new.IntegralXsec
    cs_jpsi_test[0] = cs_jpsi_new[0]
    cs_jpsi_test[1] = cs_jpsi_new[1]

    cs_jpsi_new.d["val"][0] = fitFunc.Integral(binedge, 7.0)/(7.0-binedge)#/fitFunc.Integral(6.0, 7.0)*cs_jpsi.d["val"][nb]
    cs_jpsi_new.d["stat"][0] = (cs_jpsi_new.d["val"][0]/cs_jpsi.d["val"][nb])*cs_jpsi.d["stat"][nb]
    cs_jpsi_new.d["uncorr"][0] = (cs_jpsi_new.d["val"][0]/cs_jpsi.d["val"][nb])*cs_jpsi.d["uncorr"][nb]
    cs_jpsi_new.d["corr"][0] = (cs_jpsi_new.d["val"][0]/cs_jpsi.d["val"][nb])*cs_jpsi.d["corr"][nb]
    #   binValPrErr = fitFunc.IntegralError(6.5,7.0, rPr.GetParams(), rPr.GetCovarianceMatrix().GetMatrixArray())
    cs_jpsi_new[1] = cs_jpsi[nb+1]
    cs_jpsi_new.calculateTotalCS(2, [0.25, 0.5])
    # cs_jpsi_new(0); cs_jpsi_new(1)
    fo.write(f"{cs_jpsi_new.getIntegralXsec(col_list)} \n")
    # print(cs_jpsi_new.IntegralXsec)
    cs_jpsi_tot[1] = cs_jpsi_new.IntegralXsec
    cs_jpsi_test[2] = cs_jpsi_new[0]
    cs_jpsi_test[3] = cs_jpsi_new[1]
    # cs_jpsi_test.calculateTotalCS(4, [0.5, 0.375, 0.375, 0.5])
    cs_jpsi_test.calculateTotalCS(4, [0.5, 0.25, 0.25, 0.5])

    logging.debug(f"Check 1: True CS in pt [6-7] {cs_jpsi.d['val'][nb]:3f} \\nb")
    logging.debug(f"Check 2: Fit  CS in pt [6-7] {fitFunc.Integral(6.0, 7.0):.3f} \\nb")

    logging.info(f"Check 1: {'Extrapol of CS in pt [5-8]':30s} {cs_jpsi_test.getIntegralXsec(col_list)} \\nb")
    logging.info(f"Check 2: {'Fit CS in pt [5-8]':30s} {fitFunc.Integral(5.0, 8.0):.3f} \\nb")
    logging.info(f"Check 3: {'True CS in pt [5-8]':30s} {cs_jpsi_true.getIntegralXsec(col_list)} \\nb")
    for i in range(1, 4):
        # print(f"PT: {nb+2*i}, pt: {ptBins[i]}")
        
        cs_jpsi_new[0] = cs_jpsi[nb+2*i]
        cs_jpsi_new[1] = cs_jpsi[nb+2*i+1]
        cs_jpsi_new.calculateTotalCS(2, [0.5, 0.5])
        fo.write(f"{cs_jpsi_new.getIntegralXsec(col_list)} \n")
        cs_jpsi_tot[i+1] = cs_jpsi_new.IntegralXsec

    fo.write("\n")
    fo.close()
    logging.info(f"File {fout} is successfully created")

    cs_jpsi_true = uncPT(0)
    for i in range(h_fit.GetNbinsX()-nb+1):
        cs_jpsi_true.append(cs_jpsi[nb-1+i])
        logging.debug(f"Bin {i}: CS {cs_jpsi_true[i]}")
    b = 0.5*np.ones(h_fit.GetNbinsX()-nb+1)
    cs_jpsi_true.calculateTotalCS(binning=b)
    logging.info(f"Integrated CS True: {cs_jpsi_true.getIntegralXsec()}")

    # cs_jpsi_tot.calculateTotalCS(binning=[0.750,0.750, 1., 1., 1.])
    cs_jpsi_tot.calculateTotalCS(binning=[0.5,0.5, 0.5, 0.5, 0.5])
    logging.info(f"Integrated CS Calc: {cs_jpsi_tot.getIntegralXsec()}")



# from ROOT import RooDataHist
# def extrapolToys(src="prompt", pmin:float=3., pmax:float=14.0, sqrtS:int=13):

#     binedge = 6.5
#     # gStyle.SetOptFit(0)
#     # gStyle.SetOptStat(0)

#     col_list = ['val','stat','uncorr','corr', 'tot']
#     csv_file = f"{homeDir}/CS_jpsi_{src}_int_inY4.csv"

#     # Get values from J/psi prod measurement
#     cs_jpsi = uncPT(0)
#     ptlist = np.linspace(pmin, pmax, int(pmax-pmin+1))

#     d = []
#     i = 0 # to trace if the pt-binning is OKish
#     try:
#         with open(csv_file) as csvfile:
#             reader = csv.DictReader(csvfile, delimiter=',',quoting=csv.QUOTE_NONNUMERIC)
#             for row in reader:
#                 if row["ptmin"] == ptlist[i]:
#                     d.append(row)
#                     logging.debug(row)  
#                     i+=1
#     except IOError:
#         logging.error("I/O error")

#     logging.debug(d, stacklevel=3)

#     nbins = int(pmax-pmin)
#     logging.debug(f"Number of pT bins: {nbins}",stacklevel=1)
#     # # print(ptlow, pthigh)

#     # # fill PT bins array for requested range
#     binning = np.linspace(pmin, pmax, int(pmax-pmin+1))
#     binninghw = (binning[1:] - binning[:-1])/2. # HW of pt bins

#     h_fit = TH1F("h_fit","h_fit",nbins, pmin, pmax)
#     cs_jpsi = uncPT(0)
#     yVal = np.array([])


#     for iPT in range(nbins):
#     #     # print(iPT)
    
#         d[iPT].pop("ptmin")
#         d[iPT].pop("ptmax")
#         logging.debug(d[iPT],stacklevel=4)
#         cs_jpsi.append(d[iPT])
#         logging.debug(cs_jpsi[iPT],stacklevel=4)
#         # cs_jpsi.calculateErrors(iPT)
#         h_fit.SetBinContent(iPT+1, cs_jpsi.d["val"][iPT] )
#         h_fit.SetBinError(iPT+1, cs_jpsi.d["tot"][iPT] )
#         yVal = np.append(yVal, cs_jpsi.d["val"][iPT])


#     #     # print(f'CS in bin {iPT+ptlow}: {cs_jpsi.d["val"][iPT]:.3f} +/- {cs_jpsi.d["tot"][iPT]:.3f}')


#     cs_jpsi.calculateTotalCS(nbins, binninghw)
#     cs_jpsi_tot = cs_jpsi.IntegralXsec


if __name__=="__main__":

    gROOT.LoadMacro("../libs/lhcbStyle.C")
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)
    extrapolFile(src="prompt", pmin=4.0, pmax=9.)
    extrapolFile(src="fromB", pmin=4.0, pmax=9.)
    # extrapolFile(src="fromB", pmin=3.0)
    # extrapol(pmin=4.0)
    # extrapol(src="fromB", pmin=4.0)