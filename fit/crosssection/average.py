from extrapolateJpsi import *
from cstools import uncPT

from ROOT import TF1, TH1F, TFile, TCanvas, TLatex, TGraphErrors, TLegend
from ROOT import gStyle, gPad, gROOT
from ROOT import kBlue, kFullCircle, kOpenCircle
from array import array
import logging
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

from cstools import uncPT

#homeDir = "../"
# homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/average/"

# nBins = 4 # for etac
nBins = 1 # for chic2chic
# nBins = 2 # for b2chic

syst_uncorr = ["rEtaToJpsi"] #"Efficiency"
syst_corr = ["Chebychev3par","eff_pppi0","xtalk","Gamma","Gauss","Polariz"]

NB = { 
    "PT" : 6,
    # "PT" : 5,
    # "PT" : 4,
    "Y"  : 4,
    "TOT": 1    
}
BHW = { 
    "PT" : array("d", [0.75, 0.75, 1.0, 1.0, 1.0, 3.0]),
    # "PT" : array("d", [0.75, 0.75, 1.0, 1.0, 1.0]),
    # "PT" : array("d", [0.75, 1.0, 1.0, 1.0]),
    "Y"  : array("d", [0.25, 0.25, 0.25, 0.25]),
    "TOT": array("d",[0.5])
}
BINNING = { 
    "PT" : array("d", [5.0, 6.5, 8.0, 10.0, 12.0, 14.0, 20.0]),
    # "PT" : array("d", [5.0, 6.5, 8.0, 10.0, 12.0, 14.0]),
    # "PT" : array("d", [6.5, 8.0, 10.0, 12.0, 14.0]),
    "Y"  : array("d", [2.0, 2.5, 3.0, 3.5, 4.0]),
    "TOT": array("d",[0.0, 1.0])
}
L   = 2.0
#eff = 0.99
#effErr  = 0.02
eff = 0.907
effErr  = 0.008
PTBins  = array("d", [5.75, 7.25, 9.0, 11.0, 13.0, 17.0])
# PTBins  = array("d", [7.25, 9.0, 11.0, 13.0, 17.0])
PTBinHW = array("d", [0.75, 0.75, 1.0, 1.0, 1.0, 3.0])
# ptBinning = array("d", [5.0, 6.5, 8.0, 10.0, 12.0, 14.0, 20.0])
#PTBins  = array("d", [2.25, 2.75, 3.25, 3.75])
YBinHW = array("d", [0.25, 0.25, 0.25, 0.25])
yBinning = array("d", [2.0, 2.5, 3.0, 3.5, 4.0])

# BREtacToPP    = 1.44e-3 # PDG 2022
BREtacToPP    = 1.45e-3
BREtacToPPErr = 0.14e-3

# BRJpsiToPP    = 2.120e-3 # PDG 2022
BRJpsiToPP    = 2.121e-3
BRJpsiToPPErr = 0.029e-3

errBRrel = ((BRJpsiToPPErr/BRJpsiToPP)**2 + (BREtacToPPErr/BREtacToPP)**2)**0.5 # 0.098

rangeL, rangeR  = 5.0, 20.
#rangeL, rangeR  = 2.0, 4.0

mcDir = "../MC/tuples/selected/"

title_dict = {
    "PT"   : ["#it{dp}_{T}", "2.0 < #it{y} < 4.0",      "[nb / GeV/c]", "#it{p}_{T}  [GeV/c]"],
    "Y"    : ["#it{dy}",     "5.0 < #it{p}_{T} < 20.0 GeV/c", "[nb]",       "#it{y}"],
    "TOT"  : ["#sqrt{s}",    "5.0 < #it{p}_{T} < 20.0 GeV/c \n 2.0 < #it{y} < 4.0", "[nb]", "#sqrt{s}"],
}


sysName = {
    "Mean":             "Mean value",
    "Stat":             "Stat. uncertainty",
    "Efficiency":       "Efficiency ratio",
    "Corr":             "Tot. corr. syst.",
    "Uncorr":           "Tot. uncorr. syst.",
    "Syst":             "Tot. syst.",
    "rEtaToJpsi":       "\\pt-dependence of $\\sigma_{\\etac}/\\sigma_{\\jpsi}$",
    "Chebychev3par":    "Comb. bkg. description",
    "eff_pppi0":        "Contribution from \\JpsiToPpbarPiz" ,
    "eff_pppi0_x2":     "Contribution from \\JpsiToPpbarPiz x2" ,
    "xtalk":            "Cross-feed",
    "CB":               "Mass resolution model",
    "Gauss":            "Mass resolution model",
    "Gamma34":          "Variation of $\\Gamma_{\\etac}$",
    "Gamma":            "Variation of $\\Gamma_{\\etac}$",
    "GammaL":           "Variation of $\\Gamma_{\\etac}$",
    "Polariz":          "\\jpsi polarisation",
    "BR":               "Branching ratios",
}

BR_b2ccbar = {
    "chic0" : [1.5e-2, 0.6e-2],
    "chic1" : [1.4e-2, 0.4e-2],
    "chic0" : [6.2e-3, 2.9e-3],
    "psi2S" : [3.06e-3, 0.30e-3],
    "jpsi"  : [1.16e-2, 0.10e-2],
}


def getJpsi(bins:str="PT",total:bool=False):

    nBins = NB[bins]
    BinHW = BHW[bins]

    CSJpsiPrD  = uncPT(nBins)
    CSJpsiFbD = uncPT(nBins)

    fiS = open(f"{homeDir}/CSJpsi_{bins}_Y4.txt","r")
    # fiS = open(f"{homeDir}/CSJpsi_{bins}_PT4.txt","r")
    # fiS = open(f"{homeDir}/CSJpsi_{bins}.txt","r")
    for iB in range(nBins):
        i=0
        line = fiS.readline()
        for key in ["val","stat","uncorr","corr","tot"]:
            CSJpsiPrD.d[key][iB] = float(line.split()[i])/(2*BinHW[iB])
            i+=1
        CSJpsiPrD.calculateErrors(iB)

    line = fiS.readline()
    for iB in range(nBins):
        i=0
        line = fiS.readline()
        for key in ["val","stat","uncorr","corr","tot"]:
            CSJpsiFbD.d[key][iB] = float(line.split()[i])/(2*BinHW[iB])
            i+=1
        CSJpsiFbD.calculateErrors(iB)
    fiS.close()

    CSJpsiPrD.calculateTotalCS(nBins,BinHW)
    CSJpsiFbD.calculateTotalCS(nBins,BinHW)

    if total:
        CSJpsiPrTot  = uncPT(1)
        CSJpsiFbTot = uncPT(1)
        CSJpsiPrTot[0] = CSJpsiPrD.IntegralXsec
        CSJpsiFbTot[0] = CSJpsiFbD.IntegralXsec
        return CSJpsiPrTot, CSJpsiFbTot
    else:
        return CSJpsiPrD, CSJpsiFbD


def readYields(filename:str="table_oldANA_prompt.txt"):
    
    yields = {}
    with open(f"{homeDir}/{filename}","r") as fi:
        for line in fi:
            if line=="\n":
                break
            key = line.split()[0]
            yields[key] = [0.0]*(nBins+1)
            line = fi.readline()
            l = line.strip('\n').split('  ')
            for iB in range(nBins+1):
                yields[key][iB] = float(l[iB])
                # print(l[iB])
        logging.debug(yields)
    return yields

def averagechic(postfix="b2chic"):

    # B(b → ηc(1S)X) = (4.88 ± 0.97) × 10−3
    # B(b → JpsiX) = (1.16 ± 0.10) × 10−2

    nstates = 3
    cnorm = 1e-3
    if postfix=="chic2chic": 
        nstates = 2
        cnorm = 1
    
    y_old = readYields(f"table_oldANA_{postfix}.txt")
    y_new = readYields(f"table_newANA_{postfix}.txt")

    y = {}
    y["Mean"] = [0.0]*nstates
    y["Stat"] = [0.0]*nstates
    y["Syst"] = [0.0]*nstates
    y["BR"] = [0.0]*nstates

    print(y_old,y_new)
    for i in range(nstates):
        # y["Mean"][i] = (y_old["Mean"][i] + y_new["Mean"][i])/2.0 / cnorm
        # y["Stat"][i] = (y_old["Stat"][i]**2 + y_new["Stat"][i]**2)**0.5/2.0 / cnorm
        # y["Syst"][i] = (y_old["Syst"][i]**2 + y_new["Syst"][i]**2)**0.5 / cnorm
        # y["BR"][i] = (y_old["BR"][i]**2 + y_new["BR"][i]**2)**0.5 / cnorm
        # # Weighted average
        # y["Mean"][i] = (y_old["Mean"][i]/y_old["Stat"][i]**2 + y_new["Mean"][i]/y_new["Stat"][i]**2)*1/(1/y_old["Stat"][i]**2 + 1/y_new["Stat"][i]**2) / cnorm
        # y["Stat"][i] = (1/y_old["Stat"][i]**2 + 1/y_new["Stat"][i]**2)**-0.5 / cnorm
        # y["Syst"][i] = y["Mean"][i]*((y_old["Syst"][i]/y_old["Mean"][i])**2 + (y_new["Syst"][i]/y_new["Mean"][i])**2)**0.5 
        # y["BR"][i]   = y["Mean"][i]*((y_old["BR"][i]/y_old["Mean"][i])**2 + (y_new["BR"][i]/y_new["Mean"][i])**2)**0.5 
        # All Weighted
        y["Mean"][i] = (y_old["Mean"][i]/y_old["Stat"][i]**2 + y_new["Mean"][i]/y_new["Stat"][i]**2)*1/(1/y_old["Stat"][i]**2 + 1/y_new["Stat"][i]**2) / cnorm
        y["Stat"][i] = (1/y_old["Stat"][i]**2 + 1/y_new["Stat"][i]**2)**-0.5 / cnorm
        y["Syst"][i] = (1/y_old["Syst"][i]**2 + 1/y_new["Syst"][i]**2)**-0.5 / cnorm
        y["BR"][i] = (1/y_old["BR"][i]**2 + 1/y_new["BR"][i]**2)**-0.5 / cnorm
        # Average rel
        # y["Syst"][i] = y["Mean"][i]*((y_old["Syst"][i]/y_old["Mean"][i])**2 + (y_new["Syst"][i]/y_new["Mean"][i])**2)**0.5/2.0 
        # y["BR"][i]   = y["Mean"][i]*((y_old["BR"][i]/y_old["Mean"][i])**2 + (y_new["BR"][i]/y_new["Mean"][i])**2)**0.5/2.0
                   
    return y

def average(src):

    y_old = readYields(f"table_oldANA_{src}.txt")
    y_new = readYields(f"table_newANA_{src}.txt")

    y = {}
    y["Mean"] = [0.0]*(nBins+1)
    y["Stat"] = [0.0]*(nBins+1)
    y["Efficiency"] = [0.0]*(nBins+1)

    for iB in range(nBins+1):
        # y["Mean"][iB] = (y_old["Mean"][iB] + y_new["Mean"][iB])/2.0
        # y["Stat"][iB] = (y_old["Stat"][iB]**2 + y_new["Stat"][iB]**2)**0.5/2.0
        y["Mean"][iB] = ((y_old["Mean"][iB]/y_old["Stat"][iB]**2) + (y_new["Mean"][iB]/y_new["Stat"][iB]**2)) * 1/(1/y_old["Stat"][iB]**2 + 1/y_new["Stat"][iB]**2)
        y["Stat"][iB] = (1/y_old["Stat"][iB]**2 + 1/y_new["Stat"][iB]**2)**-0.5 
        y["Efficiency"][iB] = (y_old["Efficiency"][iB]**2 + y_new["Efficiency"][iB]**2)**0.5/2.0

    for key in syst_uncorr:
        y[key] = [0.0]*(nBins+1)
        for iB in range(nBins+1):
            y[key][iB] = (y_old[key][iB]**2 + y_new[key][iB]**2)**0.5

    if src=="fromB":
        syst_corr.remove("Polariz")

    for key in syst_corr:
        y[key] = [0.0]*(nBins+1)
        for iB in range(nBins+1):
            y[key][iB] = max(y_old[key][iB],y_new[key][iB])

    calcSyst(y, src)
    return y

def calcSyst(y, src):

    syst_corr_ana = ["Gamma","Polariz","Gauss"]
    syst_uncorr_ana = ["rEtaToJpsi", "eff_pppi0", "xtalk", "Chebychev3par", "Efficiency"] #

    if src=="fromB":
        syst_corr_ana.remove("Polariz")

    y["Uncorr"] = [0.0]*(nBins+1)
    y["Corr"] = [0.0]*(nBins+1)
    y["Syst"] = [0.0]*(nBins+1)
    y["Tot"] = [0.0]*(nBins+1)

    for iB in range(nBins+1):
        for key in syst_corr_ana:
            y["Corr"][iB] += y[key][iB]**2
        y["Corr"][iB] = y["Corr"][iB]**0.5
        for key in syst_uncorr_ana:
            y["Uncorr"][iB] += y[key][iB]**2
            logging.debug(f"{key} {y[key][iB]:.2f} {y['Uncorr'][iB]:.2f}")
        y["Uncorr"][iB] = y["Uncorr"][iB]**0.5
        logging.debug(f"Uncorr {y['Uncorr'][iB]:.2f} \n")
        y["Syst"][iB] = (y["Uncorr"][iB]**2 + y["Corr"][iB]**2)**0.5
        y["Tot"][iB] = (y["Stat"][iB]**2 + y["Syst"][iB]**2)**0.5

    return y

def printYields(yields:dict, src:str="prompt"):

    for key in yields:
        print(f"{key} ", end="")
        for iB in range(nBins+1):
            print(f"{yields[key][iB]:.2f} ", end="")
        print()

def printLatex(yields:dict, src:str="prompt"):

    for key in yields:
        print(f"{sysName[key]:36s} ", end="")
        for iB in range(nBins+1):
            if key=="Mean":
                print(f"&  {yields[key][iB]:.2f} ", end="")
            else:
                print(f"&  {yields[key][iB]:.2f} ", end="")
        print("\\\\")

def print4CS(y_pr:dict, y_fb:dict):

    keys = ["Mean","Stat","Uncorr","Corr","Syst"]
    sc = 1.0
    for key in keys:
        if key=="Mean": 
            print(f"0 0 {y_pr[key][-1]/sc:.3f} {y_fb[key][-1]/sc:.3f}", end="\n")
        else:
            sc = 100.0
            print(f"0 0 {y_pr['Mean'][-1]*y_pr[key][-1]/sc:.3f} {y_fb['Mean'][-1]*y_fb[key][-1]/sc:.3f}", end="\n")
    print()
    for iB in range(nBins):
        sc = 1.0
        for key in keys:
            if key=="Mean": 
                print(f"0 0 {y_pr[key][iB]:.3f} {y_fb[key][iB]:.3f}", end="\n")
            else:
                sc = 100.0
                print(f"0 0 {y_pr['Mean'][iB]*y_pr[key][iB]/sc:.3f} {y_fb['Mean'][iB]*y_fb[key][iB]/sc:.3f}", end="\n")
        print()

def printCSLatex(y_pr:dict, y_fb:dict):

    keys = ["Stat","Uncorr","Corr"]
    sc = 100.0
    # for key in keys:
    #     print(f" & {y_pr['Mean'][-1]}", end="")
    #     if key!="Mean": 
    #         print(f"\\pm {y_pr['Mean'][-1]*y_pr[key][-1]/sc:.3f} ", end="")
    #     print(f" & {y_fb['Mean'][-1]}", end="")
    #     if key!="Mean": 
    #         print(f"\\pm {y_fb['Mean'][-1]*y_fb[key][-1]/sc:.3f} ", end="")

    for iB in range(nBins+1):
        print(f" & {y_pr['Mean'][iB]:.2f}", end="")
        for key in keys:
            print(f"\\pm {y_pr['Mean'][iB]*y_pr[key][iB]/sc:.2f} ", end="")
        print(f" & {y_fb['Mean'][iB]:.2f}", end="")
        for key in keys:
            print(f"\\pm {y_fb['Mean'][iB]*y_fb[key][iB]/sc:.2f} ", end="")
        print("\\\\")

def compare(src):

    y_old = readYields(f"table_oldANA_{src}.txt")
    y_new = readYields(f"table_newANA_{src}.txt")

    y = {}
    y["Mean"] = [0.0]*(nBins+1)
    y["Diff"] = [0.0]*(nBins+1)
    y["Stat"] = [0.0]*(nBins+1)
    y["Efficiency"] = [0.0]*(nBins+1)

    for iB in range(nBins+1):
        y["Mean"][iB] = (y_old["Mean"][iB] + y_new["Mean"][iB])/2 # mean
        y["Diff"][iB] = 100*(y_old["Mean"][iB] - y_new["Mean"][iB])/y["Mean"][iB] # difference in mean in %
        y["Stat"][iB] = (y_old["Stat"][iB]**2 + y_new["Stat"][iB]**2)**0.5
        y["Efficiency"][iB] = (y_old["Efficiency"][iB]**2 + y_new["Efficiency"][iB]**2)**0.5/2.0

    for key in syst_uncorr:
        y[key] = [0.0]*(nBins+1)
        for iB in range(nBins+1):
            y[key][iB] = (y_old[key][iB]**2 + y_new[key][iB]**2)**0.5

    if src=="fromB":
        syst_corr.remove("Polariz")

    for key in syst_corr:
        y[key] = [0.0]*(nBins+1)
        for iB in range(nBins+1):
            y[key][iB] = max(y_old[key][iB],y_new[key][iB])

    calcSyst(y, src)

    chi2 = [0.0]*(nBins+1)
    for iB in range(nBins+1):
        print(f'Mean diff.: {y["Diff"][iB]:.2f}; Tot. unc.: {y["Tot"][iB]:2f}')
        chi2[iB] = (y["Diff"][iB]/y["Tot"][iB])**2
    
    return chi2

# def getYields(CSfilename:str="rel_PT_sim.txt", bins:str="PT", total:bool=False):
#     '''Get the yields from the cross section file'''

#     nBins = NB[bins]
#     BinHW = BHW[bins]
    
#     if total:
#         nBins = 1
#         BinHW = array("d", [0.5])
        
#     NJpsiPrDict  = uncPT(nBins)
#     NJpsiFbDict = uncPT(nBins)

#     NEtacPrDict  = uncPT(nBins)
#     NEtacFbDict = uncPT(nBins)

#     keys = ["val","stat","uncorr","corr","tot"]
#     with open(f"{homeDir}/{CSfilename}","r") as fi:
#         for iB in range(nBins):
#             for key in keys:
#                 line = fi.readline()
#                 l = line.split()
#                 NJpsiPrDict.d[key][iB], NJpsiFbDict.d[key][iB], NEtacPrDict.d[key][iB], NEtacFbDict.d[key][iB] = float(l[0]), float(l[1]), float(l[2]), float(l[3])
#                 NJpsiPrDict.calculateErrors(iB)
#                 NJpsiFbDict.calculateErrors(iB)
#                 NEtacPrDict.calculateErrors(iB)
#                 NEtacPrDict.calculateErrors(iB)
#             line = fi.readline()

#     return NJpsiPrDict, NJpsiFbDict, NEtacPrDict, NEtacFbDict

# def getAverageRelCS(bins:str="PT",total:bool=False):

#     nBins = NB[bins]

#     NJpsiPrDict, NJpsiFbDict, NEtacPrDict, NEtacFbDict = {}, {}, {}, {}
#     NJpsiPrDict[0], NJpsiFbDict[0], NEtacPrDict[0], NEtacFbDict[1] = getYields("rel_PT_sim.txt", bins, total)
#     NJpsiPrDict[1], NJpsiFbDict[1], NEtacPrDict[1], NEtacFbDict[1] = getYields("rel_Y_sim.txt", bins, total)

#     n_etac_pr = uncPT(nBins) 
#     n_etac_fb = uncPT(nBins)
    
#     # NEtacPrDict.d["val"][iB]
#     for iB in range(nBins):
#         n_etac_pr[iB]["val"] = (NEtacPrDict[0][iB]["val"] + NEtacPrDict[1][iB]["val"])/2
#         n_etac_pr[iB]["stat"] = 1/2.*(NEtacPrDict[0][iB]["stat"]**2 + NEtacPrDict[1][iB]["stat"]**2)**0.5
#         n_etac_pr[iB]["corr"] = max(NEtacPrDict[0][iB]["corr"]/NEtacPrDict[0][iB]["val"], NEtacPrDict[1][iB]["corr"]/NEtacPrDict[1][iB]["val"])*n_etac_pr[iB]["val"]
#         n_etac_pr[iB]["uncorr"] = max(NEtacPrDict[0][iB]["uncorr"]/NEtacPrDict[0][iB]["val"], NEtacPrDict[1][iB]["uncorr"]/NEtacPrDict[1][iB]["val"])*n_etac_pr[iB]["val"]
#         n_etac_pr.calculateErrors(iB)
#         n_etac_pr.getCS(iB)

#         n_etac_fb[iB]["val"] = (NEtacFbDict[0][iB]["val"] + NEtacFbDict[1][iB]["val"])/2
#         n_etac_fb[iB]["stat"] = 1/2.*(NEtacFbDict[0][iB]["stat"]**2 + NEtacFbDict[1][iB]["stat"]**2)**0.5
#         n_etac_fb[iB]["corr"] = max(NEtacFbDict[0][iB]["corr"]/NEtacFbDict[0][iB]["val"], NEtacFbDict[1][iB]["corr"]/NEtacFbDict[1][iB]["val"])*n_etac_fb[iB]["val"]
#         n_etac_fb[iB]["uncorr"] = max(NEtacFbDict[0][iB]["uncorr"]/NEtacFbDict[0][iB]["val"], NEtacFbDict[1][iB]["uncorr"]/NEtacFbDict[1][iB]["val"])*n_etac_fb[iB]["val"]
#         n_etac_fb.calculateErrors(iB)
#         n_etac_fb.getCS(iB)

#     # n_etac_pr.calculateTotalCS(nBins,BHW[bins])

def makeAv():

    src = "prompt"
    y_pr = average(src)
    src = "fromB"
    y_fb = average(src)

    print4CS(y_pr, y_fb)
    printCSLatex(y_pr, y_fb)
    # printYields(y, src)
    # printLatex(y_pr, src)
    # printLatex(y_fb, src)
       
def makeAvChic():

    # y = averagechic()
    # printLatex(y)
    y = averagechic("chic2chic")
    printLatex(y)


def makeChi2():

    src = "fromB"
    src = "prompt"
    chi_pr = compare(src)
    src = "fromB"
    chi_fb = compare(src)
    print(f"Chi2 for prompt: {chi_pr}")
    print(f"Chi2 for feeddown: {chi_fb}")

if __name__ == "__main__":
    # getAverageRelCS("PT",False)
    # makeAv()
    makeAvChic()
    # makeChi2()
    # src = "fromB"
    # src = "prompt"
    # y = average(src)
    # calcSyst(y, src)
    # printYields(y, src)
    # # printLatex(y, src)