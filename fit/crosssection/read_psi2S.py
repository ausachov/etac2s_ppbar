import numpy as np
from extrapolateJpsi import uncPT

def readCS(src="prompt"):

    with open(f"psi2S_CS_{src}.txt") as f:
        line = f.readline()
        line = f.readline().split()
        nBins = len(line)-1
        cs = uncPT(nBins)
        keys = ["val","stat","uncorr","corr"]
        print(nBins)
        for line in f: 
            l = line.split()[1:]
            # print(l)
            for i in range(nBins):
                for k in range(len(keys)):
                    cs.d[keys[k]][i] = float(l[i*4+k])
                    # print(i, k, 4*i+k)
                cs.d["norm"][i] = 0.
                cs.calculateErrors(i)
                # print(cs.d["tot"][i])
            cs.calculateTotalCS(nBins)
            print(line.split()[0], cs.IntegralXsec)


def readSum(state:str="jpsi", src:str="fromB", ptmin:float=5.0, ptmax:float=14.0, ymin:float=2.0, ymax:float=4.5):

    print(f"Calculating integrated CS for {src} {state}")
    iY = 1
    cs_i = 0.
    keys = ["val","stat","uncorr","corr"]
    binningX = np.array([])
    binningY = np.array([])
    cs_tot = uncPT(0)
    with open(f"{state}_CS_2D.txt") as f:
        for line in f:
            if src in line:     # look for prompt or fromB
                break
        line = f.readline().split()[1:]
        nBinsY = len(line)
        a = 0.5
        for iY in range(nBinsY):
            bl = float(line[iY].split("-")[0])
            br = float(line[iY].split("-")[1])
            if bl>=ymin and br<=ymax:
                binningX = np.append(binningX, a*(br - bl))
        # binningX = np.append(binningX, float(line[-1].split("-")[1]))
        # print(binningX)
        nbX = len(binningX)
        nBinsPT = 0
        for line in f: 
            if line=="\n":
                break

            cs = uncPT(nbX)
            l = line.split()
            bl = float(l[0].split("-")[0])
            br = float(l[0].split("-")[1])
            if bl>=ptmin and br<=ptmax:
                binningY = np.append(binningY, a*(br-bl))
                l = l[1:]
                # nb = int(len(l)/4)
                # print(f"nb: {nbX}")
                for i in range(nbX):
                    for k in range(len(keys)):
                        cs.d[keys[k]][i] = float(l[i*4+k])
                    # cs.d["norm"][i] = 0.
                    cs.calculateErrors(i)
                    # cs(i)
                    if i==iY: # sum over i^th bin of rapidity
                        cs_i += cs.d["val"][i]
                    # print(cs.d["tot"][i])
                cs.calculateTotalCS(nbX, binningX)
                cs_tot.append(cs.IntegralXsec)
                print(line.split()[0], cs.getIntegralXsec(keys=["val","stat","uncorr","corr"]))
            
            nBinsPT += 1
        # print(len(binningY), binningY)
        cs_tot.calculateTotalCS(len(binningY), binningY)
        print(f"pt:{ptmin}-{ptmax} y:{ymin}-{ymax} \n \t {cs_tot.getIntegralXsec()}")
        # print(f"iY sum is: {cs_i:.3f} for iY={i}")

if __name__=="__main__":
    '''
    Correct code to compute psi2S CS integrated in bins of Y and PT
    '''
    readSum(src="fromB",ptmin=5.0, ptmax=14.0, ymax=4.0)
    readSum(src="prompt",ptmin=5.0, ptmax=14.0, ymax=4.0)
    # # for psi2S analysis
    # # stat   = sqrt(sum(stat**2))
    # # corr   = relative over all bins dsigma[i]/sigma[i]=dsigma/sigma
    # # uncorr = sqrt(sum(stat**2))
    # readSum(state="psi2S", src="prompt",ptmin=5.0, ptmax=14.0, ymax=4.5)
    # readSum(state="psi2S", src="fromB", ptmin=5.0, ptmax=14.0, ymax=4.5)
    # # readCS("fromB")
    # readTXT()
