''' Module with all charmonia constants in [MeV] ''' 

# M_PHI         = 1019.461
# M_PROTON      = 938.272
# M_PI0         = 134.977
# M_PION        = 139.571
# M_K0          = 496.611     #0.013
# M_KAON        = 493.677     #0.016

width = {
        "p"         :[  -1.,    -1.],
        "phi"       :[ 4.249, 4.262],
        "etac"      :[  32.0,  34.0],
        "jpsi"      :[   0.0,   0.0],
        "chic0"     :[  10.8,  11.1],    #0.30
        "chic1"     :[  0.84,  0.89],    #0.05
        "chic2"     :[  1.97,  2.04],    #0.07
        "hc"        :[  0.70,  1.10],    #0.4
        "etac2S"    :[  11.3,  14.5],    #+3.2/-2.9
        "psi2S"     :[   0.0,   0.0],    #0.025
        "psi3770"   :[  27.2, 27.55],    #0.35
        "psi3860"   :[  201.,  380.],    #+180-110
        "psi3872"   :[   1.0,  2.50],    #+0.18/-0.1
        "psi3930"   :[  35.3,  39.1],    #2.8
        "psi_X"     :[   10.,   0.0],    #2.8
        }

mass_alt = {
        "phi"       :1019.477,
        "p"         :938.272,
        "etac"      :2984.4,      #+1 PDG sigma 
        "jpsi"      :3096.9,
        "chic0"     :3415.01,     #+1 PDG sigma 
        "chic1"     :3510.72,     #+1 PDG sigma 
        "chic2"     :3556.24,     #+1 PDG sigma 
        "hc"        :3525.49,     #+1 PDG sigma 
        "etac2S"    :3638.6,      #+1 PDG sigma 
        "psi2S"     :3686.123,    #+1 PDG sigma 
        "psi3770"   :3773.13,     #0.35
        "psi3860"   :3862.00,     #+50/-35
        "psi3872"   :3871.69,     #0.17
        "psi3930"   :3922.2,      #1.0
        "psi_X"     :3414.71,     #0.30
        }

mass = {
        "phi"       :1019.461,
        "p"         :938.272,
        "etac"      :2983.9,
        "jpsi"      :3096.9,
        "chic0"     :3414.71,     #0.30
        "chic1"     :3510.67,     #0.05
        "chic2"     :3556.17,     #0.07
        "hc"        :3525.38,     #0.11
        "etac2S"    :3637.5,      #1.1
        "psi2S"     :3686.097,    #0.025
        "psi3770"   :3773.13,     #0.35
        "psi3860"   :3862.00,     #+50/-35
        "psi3872"   :3871.69,     #0.17
        "psi3930"   :3922.2,      #1.0
        "psi_X"     :3414.71,     #0.30
        }

spin = {
        "p"         :0.5,
        "phi"       :1.,
        "etac"      :0.,
        "jpsi"      :1.,
        "chic0"     :0.,     #0.30
        "chic1"     :1.,     #0.05
        "chic2"     :2.,     #0.07
        "hc"        :1.,     #0.11
        "etac2S"    :0.,      #1.1
        "psi2S"     :1.,    #0.025
        "psi3770"   :1.,     #0.35
        "psi3860"   :0.,     #0.35
        "psi3872"   :1.,     #0.35
        "psi3930"   :2.,     #0.35
        "psi_X"     :1,
        }


class Particle:

    def __init__(self, name):
        self.name  = name
        self.spin  = spin[name]
        self.mass  = mass[name]
        self.width = width[name][0]
        self.mass_alt  = mass_alt[name]
        self.width_alt = width[name][1]