from ROOT import TChain, TTree
from ROOT import gROOT

import logging

homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/mass_fit/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

binWidth = 0.2
binWidthDraw = 0.2

minM_Low = 2850
maxM_Low = 3250
binN_Low = int((maxM_Low-minM_Low)/binWidth)

binningDict = {
    "Jpsi_PT":                      [5000., 6500., 8000., 10000., 12000., 14000., 20000.],
    "Jpsi_Y":                       [2.0, 2.5, 3.0, 3.5, 4.0, 4.5],
    "TMath::Abs(ProtonM_CosTheta)": [0.0, 0.15, 0.3, 0.50, 1.0],
    "nSPDHits":                     [0, 100, 150, 200, 250, 300],
}

def crosstalk(nB:int=0, bins:str="PT",state:str="jpsi",cutkey:str="_ProbNNp_06"):
    '''
    Calculate prompt and b-decay fractions for a given bin
    nB: int - number of bin
    bins: str - name of pT binning
    state: str - name of charmonium state
    cutkey: str - name of cut
    '''
    variable = f"Jpsi_{bins}"

    ntJpsi_Prompt = TChain("DecayTree")
    ntJpsi_FromB = TChain("DecayTree")

    ntJpsi_Prompt.Add(f"{dataDir}/MC/Trigger/{state}DiProton_prompt_2018_AddBr.root")
    ntJpsi_FromB.Add(f"{dataDir}/MC/Trigger/{state}DiProton_fromB_2018_AddBr.root")

    treeJpsi_Prompt = TTree()
    treeJpsi_FromB = TTree()

    treeJpsi_Prompt = ntJpsi_Prompt.CopyTree("Jpsi_prompt")
    treeJpsi_FromB = ntJpsi_FromB.CopyTree("Jpsi_sec")

    cutJpsi_Tz_Sec = "Jpsi_Tz > 0.08"
    cutJpsi_Tz_Prompt = "Jpsi_Tz < 0.08"
    cutJpsi_IP_CHI2 = "((ProtonP_IPCHI2_OWNPV > 16) && (ProtonM_IPCHI2_OWNPV > 16))"
    # cutJpsi_PID = "ProtonP_ProbNNk < 0.6 && ProtonM_ProbNNk < 0.6 && ProtonP_ProbNNp > 0.6 && ProtonM_ProbNNp > 0.6 && ProtonP_ProbNNpi < 0.6 && ProtonM_ProbNNpi < 0.6"
    cutJpsi_PID = "ProtonP_ProbNNp > 0.6 && ProtonM_ProbNNp > 0.6"
    # cutJpsi_PID = "ProtonP_PIDp > 20.0 && ProtonM_PIDp > 20.0 && (ProtonP_PIDp-ProtonP_PIDK)>15 && (ProtonM_PIDp-ProtonM_PIDK)>15"

    if nB==0:
        if "_6-14" in cutkey:
            cutPT = "{0}>{1} && {0}<{2}".format(variable,binningDict[variable][1],binningDict[variable][-2])
        elif "_5-14" in cutkey:
            cutPT = "{0}>{1} && {0}<{2}".format(variable,binningDict[variable][0],binningDict[variable][-2])
        else:
            cutPT = "{0}>{1} && {0}<{2}".format(variable,binningDict[variable][0],binningDict[variable][-1])
    else:
       cutPT = "{0}>{1} && {0}<{2}".format(variable,binningDict[variable][nB-1],binningDict[variable][nB])

    NJpsi_PP = treeJpsi_Prompt.GetEntries(f"{cutPT} && {cutJpsi_Tz_Prompt} && {cutJpsi_PID}")
    NJpsi_PB = treeJpsi_Prompt.GetEntries(f"{cutPT} && {cutJpsi_Tz_Sec} && {cutJpsi_IP_CHI2} && {cutJpsi_PID}")
    NJpsi_BB = treeJpsi_FromB.GetEntries(f"{cutPT} && {cutJpsi_Tz_Sec} && {cutJpsi_IP_CHI2} && {cutJpsi_PID}")
    NJpsi_BP = treeJpsi_FromB.GetEntries(f"{cutPT} && {cutJpsi_Tz_Prompt} && {cutJpsi_PID}")

    NJpsi_Prompt = treeJpsi_Prompt.GetEntries(f"{cutJpsi_PID} && {cutPT}")
    NJpsi_FromB = treeJpsi_FromB.GetEntries(f"{cutJpsi_PID} && {cutPT}")
    # NJpsi_Prompt = treeJpsi_Prompt.GetEntries(f"{cutJpsi_PID}")
    # NJpsi_FromB = treeJpsi_FromB.GetEntries(f"{cutJpsi_PID}")

    effPP = float(NJpsi_PP/float(NJpsi_Prompt))
    effBB = float(NJpsi_BB/float(NJpsi_FromB))
    effPB = float(NJpsi_PB/float(NJpsi_Prompt))
    effBP = float(NJpsi_BP/float(NJpsi_FromB))


    effPP_err = effPP*(1/float(NJpsi_PP)+1/float(NJpsi_Prompt))**0.5
    if NJpsi_PB!=0:
       effPB_err = effPB*(1/float(NJpsi_PB)+1/float(NJpsi_Prompt))**0.5
    else:
       effPB_err = effPB*2
    effBB_err = effBB*(1/float(NJpsi_BB)+1/float(NJpsi_FromB))**0.5
    effBP_err = effBP*(1/float(NJpsi_BP)+1/float(NJpsi_FromB))**0.5

    del ntJpsi_FromB, ntJpsi_Prompt
    return effPP, effPB, effBP, effBB, effPP_err, effPB_err, effBP_err, effBB_err
