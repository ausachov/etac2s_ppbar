from ROOT import RooWorkspace, RooRealVar, RooFormulaVar, RooDataSet, RooArgSet, RooArgList, \
    RooGaussian, RooAddPdf, RooSimultaneous, RooCategory, RooAbsReal, \
    TChain, TTree, TFile, TCanvas, TLatex, \
    gROOT, RooFit

# from ROOT import *
gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+");
gROOT.LoadMacro("../libs/libBifurcatedCB/BifurcatedCB.cxx+");

from ROOT import BifurcatedCB

# homeDir = "/sps/lhcb/zhovkovska/Results_ppbar_2018/MC/MassFit/"
# homeDir = "/sps/lhcb/zhovkovska/etac_ppbar/results/MC/MassFit/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/MC/etac/mass_fit/"
# homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/MC/etac/MassFit/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/MC/Trigger/"


def getpostfix(iPT: int = 0, iY: int = 0, iTz: int = 0) -> str: 
    
    postfix = ""
    if iPT!=0:
        postfix+=f"_PT{iPT}"
    if iY!=0:
        postfix+=f"_Y{iY}"
    if iTz!=0:
        postfix+=f"_Tz{iTz}"

    return postfix


def getData_d(w: RooWorkspace, iPT: int = 0, iY: int = 0, iTz: int = 0):

    nPTBins = 6
    pt = (["5000.", "20000."], ["5000.", "6500."], ["6500.", "8000."], ["8000.", "10000."], ["10000.", "12000."], ["12000.", "14000."], ["14000.", "20000."])
    nYBins = 6
    y = (["2.", "4.5"], ["2.", "2.5"], ["2.5", "3."], ["3.", "3.5"], ["3.5", "4."], ["4.", "4.5"])
    nTzBins = 7
    tz = (["-10.0", "10.0"], ["-10.0", "-0.125"], ["-0.125", "-0.025"], ["-0.025", "0."], ["0.", "0.200"], ["0.200", "2."], ["2.", "4."], ["4.", "10."])
    # nTzBins = 11
    # tz =  (["-10.0", "10.0"], ["-10.", "-0.125"], ["-0.125", "-0.025"], ["-0.025", "0.0"], ["0.0", "0.025"], ["0.025", "0.2"], ["0.2", "1.0"], ["1.0", "1.5"], ["1.5", "2.0"], ["2.0", "3.0"], ["3.0", "4.0"], ["4.0", "10.0"])

    nt_etac_prompt = TChain("DecayTree")
    nt_jpsi_prompt = TChain("DecayTree")
    nt_etac_fromB = TChain("DecayTree")
    nt_jpsi_fromB = TChain("DecayTree")

    nt_etac_prompt.Add(dataDir + "/etacDiProton_prompt_2018_AddBr.root")
    nt_jpsi_prompt.Add(dataDir + "/jpsiDiProton_prompt_2018_AddBr.root")

    nt_etac_fromB.Add(dataDir + "/etacDiProton_fromB_2018_AddBr.root")
    nt_jpsi_fromB.Add(dataDir + "/jpsiDiProton_fromB_2018_AddBr.root")


    tree_etac_prompt = TTree()
    treeJpsi_prompt = TTree()
    tree_etac_fromB = TTree()
    treeJpsi_fromB = TTree()


    cut_PID = " && ProtonP_ProbNNp > 0.6 && ProtonM_ProbNNp > 0.6"
    if( iPT <= nPTBins ):
        #tree_etac_prompt = nt_etac_prompt.CopyTree("jpsi_prompt")
        #treeJpsi_prompt = nt_jpsi_prompt.CopyTree("jpsi_prompt")
        #tree_etac_fromB = nt_etac_fromB.CopyTree("jpsi_sec")
        #treeJpsi_fromB = nt_jpsi_fromB.CopyTree("jpsi_sec")
    #elif :
        cut_PT = " && Jpsi_PT > {} && Jpsi_PT < {}".format(pt[iPT][0],pt[iPT][1])

        if( iY <= nYBins ):
            cut_Y = " && Jpsi_Y > {} && Jpsi_Y < {}".format(y[iY][0],y[iY][1])

            if( iTz <= nTzBins ):
                cut_Tz = " && Jpsi_Tz > {} && Jpsi_Tz < {}".format(tz[iTz][0],tz[iTz][1])

                tree_etac_prompt = nt_etac_prompt.CopyTree("Jpsi_prompt " + cut_PID + cut_PT + cut_Y + cut_Tz)
                treeJpsi_prompt = nt_jpsi_prompt.CopyTree("Jpsi_prompt " + cut_PID + cut_PT + cut_Y + cut_Tz)
                tree_etac_fromB = nt_etac_fromB.CopyTree("Jpsi_sec " + cut_PID + cut_PT + cut_Y + cut_Tz)
                treeJpsi_fromB = nt_jpsi_fromB.CopyTree("Jpsi_sec  " + cut_PID + cut_PT + cut_Y + cut_Tz)
            else:
                print ("Incorrect number of Tz bin: iTz={}".format(iTz))
                exit()
        else:
            print ("Incorrect number of Y bin: iY={}".format(iY))
            exit()
    else:
        print ("Incorrect number of PT bin: iPT={}".format(iPT))
        exit()


    Jpsi_M_res = RooRealVar ("Jpsi_M_res","Jpsi_M_res",-100.0,100.0)
    ds_etac_prompt = RooDataSet("ds_etac_prompt","ds_etac_prompt",tree_etac_prompt,RooArgSet(Jpsi_M_res))
    ds_jpsi_prompt = RooDataSet("ds_jpsi_prompt","ds_jpsi_prompt",treeJpsi_prompt,RooArgSet(Jpsi_M_res))
    ds_etac_fromB = RooDataSet("ds_etac_fromB","ds_etac_fromB",tree_etac_fromB,RooArgSet(Jpsi_M_res))
    ds_jpsi_fromB = RooDataSet("ds_jpsi_fromB","ds_jpsi_fromB",treeJpsi_fromB,RooArgSet(Jpsi_M_res))
    getattr(w,"import")(ds_etac_prompt)
    getattr(w,"import")(ds_jpsi_prompt)
    getattr(w,"import")(ds_etac_fromB)
    getattr(w,"import")(ds_jpsi_fromB)

    #  ds_etac.Draw("")
    #  ds_jpsi.Draw("")



def fillRelWorkspace(w):

    Jpsi_M_res = w.var("Jpsi_M_res")

    ratioNtoW      = 0.204  #+- 0.005
    ratioEtaToJpsi = 0.94   #+- 0.01
    ratioArea      = 0.955  # +- 0.002

    r_ref_etac = RooRealVar("r_ref_etac","r_ref_etac", ratioEtaToJpsi, 0.01, 5.0)
    r_NToW = RooRealVar("r_NToW","r_NToW",ratioNtoW, 0.01, 1.0)
    r_G1ToG2 = RooRealVar("r_G1ToG2","r_G1ToG2",ratioArea, 0.01, 1.0)

    n_etac_prompt = RooRealVar("n_etac_prompt","num of etac prompt", 1e3, 0, 1.e5)
    n_jpsi_prompt = RooRealVar("n_jpsi_prompt","num of J/Psi prompt", 2e3, 0, 1.e5)
    n_etac_rel_prompt = RooRealVar("n_etac_rel_prompt","num of etac prompt", 0.0, 3.0)
    n_etac_fromB = RooRealVar("n_etac_fromB","num of etac", 1e3, 0, 5.e4)
    n_jpsi_fromB = RooRealVar("n_jpsi_fromB","num of J/Psi", 2e3, 0, 5.e4)
    n_etac_rel_fromB = RooRealVar("n_etac_rel","num of etac", 0.0, 3.0)

    n_etac_prompt_1 = RooFormulaVar("n_etac_prompt_1","num of etac","@0*@1",RooArgList(n_etac_prompt,r_G1ToG2))
    n_etac_prompt_2 = RooFormulaVar("n_etac_prompt_2","num of etac","@0-@1",RooArgList(n_etac_prompt,n_etac_prompt_1))
    n_jpsi_prompt_1 = RooFormulaVar("n_jpsi_prompt_1","num of J/Psi","@0*@1",RooArgList(n_jpsi_prompt,r_G1ToG2))
    n_jpsi_prompt_2 = RooFormulaVar("n_jpsi_prompt_2","num of J/Psi","@0-@1",RooArgList(n_jpsi_prompt,n_jpsi_prompt_1))

    n_etac_fromB_1 = RooFormulaVar("n_etac_fromB_1","num of etac","@0*@1",RooArgList(n_etac_fromB,r_G1ToG2))
    n_etac_fromB_2 = RooFormulaVar("n_etac_fromB_2","num of etac","@0-@1",RooArgList(n_etac_fromB,n_etac_fromB_1))
    n_jpsi_fromB_1 = RooFormulaVar("n_jpsi_fromB_1","num of J/Psi","@0*@1",RooArgList(n_jpsi_fromB,r_G1ToG2))
    n_jpsi_fromB_2 = RooFormulaVar("n_jpsi_fromB_2","num of J/Psi","@0-@1",RooArgList(n_jpsi_fromB,n_jpsi_fromB_1))


    mean_jpsi = RooRealVar("mean_jpsi","mean of gaussian", 0.0, -50.0, 50.0)
    mean_etac = RooRealVar("mean_etac","mean of gaussian", 0.0, -50.0, 50.0)


    sigma_etac_1 = RooRealVar("sigma_etac_1","width of gaussian", 9., 0.1, 50.)
    sigma_etac_2 = RooFormulaVar("sigma_etac_2","width of gaussian","@0/@1",RooArgList(sigma_etac_1,r_NToW))

    sigma_jpsi_1 = RooFormulaVar("sigma_jpsi_1","width of gaussian","@0/@1",RooArgList(sigma_etac_1,r_ref_etac))
    sigma_jpsi_2 = RooFormulaVar("sigma_jpsi_2","width of gaussian","@0/@1",RooArgList(sigma_jpsi_1,r_NToW))


    gauss_etac_1 = RooGaussian("gauss_etac_1","gauss_etac_1 PDF pr", Jpsi_M_res, mean_jpsi,  sigma_etac_1)
    gauss_etac_2 = RooGaussian("gauss_etac_2","gauss_etac_2 PDF pr", Jpsi_M_res, mean_jpsi,  sigma_etac_2)


    gauss_1 = RooGaussian("gauss_1","gaussian PDF",Jpsi_M_res, mean_jpsi, sigma_jpsi_1)
    gauss_2 = RooGaussian("gauss_2","gaussian PDF",Jpsi_M_res, mean_jpsi, sigma_jpsi_2)




    model_etac_prompt = RooAddPdf("model_etac_prompt","etac signal", RooArgList(gauss_etac_1, gauss_etac_2), RooArgList(n_etac_prompt_1, n_etac_prompt_2))
    model_jpsi_prompt = RooAddPdf("model_jpsi_prompt","jpsi signal", RooArgList(gauss_1, gauss_2), RooArgList(n_jpsi_prompt_1, n_jpsi_prompt_2))


    model_etac_fromB = RooAddPdf("model_etac_fromB","etac signal", RooArgList(gauss_etac_1, gauss_etac_2), RooArgList(n_etac_fromB_1, n_etac_fromB_2))
    model_jpsi_fromB = RooAddPdf("model_jpsi_fromB","jpsi signal", RooArgList(gauss_1, gauss_2), RooArgList(n_jpsi_fromB_1, n_jpsi_fromB_2))


    sample = RooCategory("sample","sample")
    sample.defineType("etac_prompt")
    sample.defineType("jpsi_prompt")
    sample.defineType("etac_fromB")
    sample.defineType("jpsi_fromB")



    data_etac_prompt = w.data("ds_etac_prompt")
    data_jpsi_prompt = w.data("ds_jpsi_prompt")
    data_etac_fromB = w.data("ds_etac_fromB")
    data_jpsi_fromB = w.data("ds_jpsi_fromB")


    combData = RooDataSet("combData", "combined data", RooArgSet(Jpsi_M_res), RooFit.Index(sample), RooFit.Import("etac_prompt",data_etac_prompt), RooFit.Import("jpsi_prompt",data_jpsi_prompt), RooFit.Import("etac_fromB",data_etac_fromB), RooFit.Import("jpsi_fromB",data_jpsi_fromB))


    simPdf = RooSimultaneous("simPdf","simultaneous signal pdf",sample)
    simPdf.addPdf(model_etac_prompt,"etac_prompt")
    simPdf.addPdf(model_jpsi_prompt,"jpsi_prompt")
    simPdf.addPdf(model_etac_fromB,"etac_fromB")
    simPdf.addPdf(model_jpsi_fromB,"jpsi_fromB")


    #   getattr(w,"import")(model,RecycleConflictNodes())
    getattr(w,"import")(combData)
    getattr(w,"import")(simPdf)



def fillRelWorkspaceCB(w):

    Jpsi_M_res = w.var("Jpsi_M_res")
    #Jpsi_M_res.setBins(1000,"cache")

    ratioNtoW      = 0.204  #+- 0.005
    ratioEtaToJpsi = 0.94   #+- 0.01
    ratioArea      = 0.955  # +- 0.002



    r_ref_etac = RooRealVar("r_ref_etac","r_ref_etac", ratioEtaToJpsi, 0.01, 5.0)
    r_NToW = RooRealVar("r_NToW","r_NToW",ratioNtoW, 0.01, 1.0)
    r_G1ToG2 = RooRealVar("r_G1ToG2","r_G1ToG2",ratioArea, 0.01, 1.0)


    n_etac_prompt = RooRealVar("n_etac_prompt","num of etac prompt", 1e3, 10, 5.e4)
    n_jpsi_prompt = RooRealVar("n_jpsi_prompt","num of J/Psi prompt", 2e3, 10, 5.e4)
    n_etac_rel_prompt = RooRealVar("n_etac_rel_prompt","num of etac prompt", 0.0, 3.0)
    n_etac_fromB = RooRealVar("n_etac_fromB","num of etac", 1e3, 10, 1.e5)
    n_jpsi_fromB = RooRealVar("n_jpsi_fromB","num of J/Psi", 2e3, 10, 1.e5)
    n_etac_rel_fromB = RooRealVar("n_etac_rel","num of etac", 0.0, 3.0)



    n_etac_prompt_1 = RooFormulaVar("n_etac_prompt_1","num of etac","@0*@1",RooArgList(n_etac_prompt,r_G1ToG2))
    n_etac_prompt_2 = RooFormulaVar("n_etac_prompt_2","num of etac","@0-@1",RooArgList(n_etac_prompt,n_etac_prompt_1))
    n_jpsi_prompt_1 = RooFormulaVar("n_jpsi_prompt_1","num of J/Psi","@0*@1",RooArgList(n_jpsi_prompt,r_G1ToG2))
    n_jpsi_prompt_2 = RooFormulaVar("n_jpsi_prompt_2","num of J/Psi","@0-@1",RooArgList(n_jpsi_prompt,n_jpsi_prompt_1))

    n_etac_fromB_1 = RooFormulaVar("n_etac_fromB_1","num of etac","@0*@1",RooArgList(n_etac_fromB,r_G1ToG2))
    n_etac_fromB_2 = RooFormulaVar("n_etac_fromB_2","num of etac","@0-@1",RooArgList(n_etac_fromB,n_etac_fromB_1))
    n_jpsi_fromB_1 = RooFormulaVar("n_jpsi_fromB_1","num of J/Psi","@0*@1",RooArgList(n_jpsi_fromB,r_G1ToG2))
    n_jpsi_fromB_2 = RooFormulaVar("n_jpsi_fromB_2","num of J/Psi","@0-@1",RooArgList(n_jpsi_fromB,n_jpsi_fromB_1))

    mean_jpsi = RooRealVar("mean_jpsi","mean of gaussian", 0.0, -50.0, 50.0)
    mean_etac = RooRealVar("mean_etac","mean of gaussian", 0.0, -50.0, 50.0)

    alpha_1 = RooRealVar("alpha_1","alpha of CB", 1., 0.0, 10.)
    alpha_2 = RooRealVar("alpha_2","alpha of CB", 1., 0.0, 10.)
    nCB_1 = RooRealVar("nCB_1","n of CB", 1., 0.0, 100.)
    nCB_2 = RooRealVar("nCB_2","n of CB", 1., 0.0, 100.)

    sigma_etac_1 = RooRealVar("sigma_etac_1","width of gaussian", 9., 0.1, 50.)
    sigma_etac_2 = RooFormulaVar("sigma_etac_2","width of gaussian","@0/@1",RooArgList(sigma_etac_1,r_NToW))

    sigma_jpsi_1 = RooFormulaVar("sigma_jpsi_1","width of gaussian","@0/@1",RooArgList(sigma_etac_1,r_ref_etac))
    sigma_jpsi_2 = RooFormulaVar("sigma_jpsi_2","width of gaussian","@0/@1",RooArgList(sigma_jpsi_1,r_NToW))

    cb_etac_1 = BifurcatedCB("cb_etac_1", "Cystal Ball Function", Jpsi_M_res, mean_jpsi, sigma_etac_1, alpha_1, nCB_1, alpha_1, nCB_1)
    cb_jpsi_1 = BifurcatedCB("cb_jpsi_1", "Cystal Ball Function", Jpsi_M_res, mean_jpsi, sigma_jpsi_1, alpha_1, nCB_1, alpha_1, nCB_1)

    model_etac_prompt = RooAddPdf("model_etac_prompt","etac signal", RooArgList(cb_etac_1), RooArgList(n_etac_prompt))
    model_jpsi_prompt = RooAddPdf("model_jpsi_prompt","jpsi signal", RooArgList(cb_jpsi_1), RooArgList(n_jpsi_prompt))


    model_etac_fromB = RooAddPdf("model_etac_fromB","etac signal", RooArgList(cb_etac_1), RooArgList(n_etac_fromB))
    model_jpsi_fromB = RooAddPdf("model_jpsi_fromB","jpsi signal", RooArgList(cb_jpsi_1), RooArgList(n_jpsi_fromB))


    sample = RooCategory("sample","sample")
    sample.defineType("etac_prompt")
    sample.defineType("jpsi_prompt")
    sample.defineType("etac_fromB")
    sample.defineType("jpsi_fromB")


    data_etac_prompt = w.data("ds_etac_prompt")
    data_jpsi_prompt = w.data("ds_jpsi_prompt")
    data_etac_fromB = w.data("ds_etac_fromB")
    data_jpsi_fromB = w.data("ds_jpsi_fromB")


    # Construct combined dataset in (Jpsi_M_res,sample)
    combData = RooDataSet("combData", "combined data", RooArgSet(Jpsi_M_res), RooFit.Index(sample), RooFit.Import("etac_prompt",data_etac_prompt), RooFit.Import("jpsi_prompt",data_jpsi_prompt), RooFit.Import("etac_fromB",data_etac_fromB), RooFit.Import("jpsi_fromB",data_jpsi_fromB))


    # Associate model with the physics state and model_ctl with the control state
    simPdf = RooSimultaneous("simPdf","simultaneous signal pdf",sample)
    simPdf.addPdf(model_etac_prompt,"etac_prompt")
    simPdf.addPdf(model_jpsi_prompt,"jpsi_prompt")
    simPdf.addPdf(model_etac_fromB,"etac_fromB")
    simPdf.addPdf(model_jpsi_fromB,"jpsi_fromB")


    #   getattr(w,"import")(model,RecycleConflictNodes())
    getattr(w,"import")(combData)
    getattr(w,"import")(simPdf)



def fitData(iPT: int = 0, iY: int = 0, iTz: int = 0, gauss: bool = True, empty: bool = False):

    gROOT.Reset()

    if gauss: add = ""
    else: add="_CB"

    w = RooWorkspace("w",True)
    getData_d(w, iPT, iY, iTz)

    if gauss:
        fillRelWorkspace(w)
    else:
        fillRelWorkspaceCB(w)

    Jpsi_M_res = w.var("Jpsi_M_res")

    model_etac_prompt = w.pdf("model_etac_prompt")
    model_jpsi_prompt = w.pdf("model_jpsi_prompt")
    model_etac_fromB = w.pdf("model_etac_fromB")
    model_jpsi_fromB = w.pdf("model_jpsi_fromB")

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf")
    combData = w.data("combData")

    data_etac_prompt = w.data("ds_etac_prompt")
    data_jpsi_prompt = w.data("ds_jpsi_prompt")
    data_etac_fromB = w.data("ds_etac_fromB")
    data_jpsi_fromB = w.data("ds_jpsi_fromB")

    if iPT!=0 or iY!=0 or iTz!=0:

        nameWksp = homeDir + "MC_MassResolution" + add + "_wksp.root"
        file_w = TFile(nameWksp)
        w_temp = file_w.Get("w")

        if gauss:
            w.var("r_NToW").setVal(w_temp.var("r_NToW").getValV())
            w.var("r_G1ToG2").setVal(w_temp.var("r_G1ToG2").getValV())
            w.var("r_NToW").setConstant(True)
            w.var("r_G1ToG2").setConstant(True)
        else:
            w.var("alpha_1").setVal(w_temp.var("alpha_1").getValV())
            w.var("nCB_1").setVal(w_temp.var("nCB_1").getValV())
            # w.var("alpha_1").setConstant(True)
            # w.var("nCB_1").setConstant(True)

    r = simPdf.fitTo(combData,RooFit.Save(True))
    r = simPdf.fitTo(combData,RooFit.Minos(True),RooFit.Save(True))

    gROOT.ProcessLine("gStyle->SetOptTitle(0)")
    if empty:
        gROOT.ProcessLine("gStyle->SetOptStat(000000000)")

    frame = []
    for i in range(4):
        frame.append(Jpsi_M_res.frame(RooFit.Title("")))

    data_etac_prompt.plotOn(frame[0])
    data_jpsi_prompt.plotOn(frame[1])
    data_etac_fromB.plotOn(frame[2])
    data_jpsi_fromB.plotOn(frame[3])


    #model_etac_prompt.paramOn(frame[0],RooFit.Layout(0.68,0.99,0.99))
    #frame[0].getAttText().SetTextSize(0.027)
    model_etac_prompt.plotOn(frame[0],RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2_etac_prompt = frame[0].chiSquare()
    #model_etac_prompt.plotOn(frame[0],RooFit.Components(RooArgSet(gauss_etac_1,gauss_etac_2)), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption("F"), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print ("chi2_etac_prompt = ", chi2_etac_prompt)

    #model_jpsi_prompt.paramOn(frame[1],RooFit.Layout(0.68,0.99,0.99))
    #frame[1].getAttText().SetTextSize(0.027)
    model_jpsi_prompt.plotOn(frame[1],RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Jpsi_prompt = frame[1].chiSquare()
    #model_jpsi_prompt.plotOn(frame[1],RooFit.Components(RooArgSet(gauss_1,gauss_2)),RooFit.FillStyle(3005),RooFit.FillColor(kMagenta),RooFit.DrawOption("F"), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print ("chi2Jpsi_prompt = ", chi2Jpsi_prompt)

    #model_etac_fromB.paramOn(frame[2],RooFit.Layout(0.68,0.99,0.99))
    #frame[2].getAttText().SetTextSize(0.027)
    model_etac_fromB.plotOn(frame[2],RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2_etac_fromB = frame[2].chiSquare()
    #model_etac_fromB.plotOn(frame[2],RooFit.Components(RooArgSet(gauss_etac_1,gauss_etac_2)), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption("F"),RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print ("chi2_etac_fromB = ", chi2_etac_fromB)

    #model_jpsi_fromB.paramOn(frame[3],RooFit.Layout(0.68,0.99,0.99))
    #frame[3].getAttText().SetTextSize(0.027)
    model_jpsi_fromB.plotOn(frame[3],RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Jpsi_fromB = frame[3].chiSquare()
    #model_jpsi_fromB.plotOn(frame[3], RooFit.Components(RooArgSet(gauss_1,gauss_2)), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption("F"), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print ("chi2Jpsi_fromB = ", chi2Jpsi_fromB)

    c = TCanvas("Masses_Fit","Masses Fit",1200,800)
    c.Divide(2,2)
    names = ["#etac_{c} prompt", "J/#psi prompt", "#etac_{c} from-b", "J/#psi from-b"]
    texMC = TLatex()
    texMC.SetNDC()

    for iC in range(4):
        pad = c.cd(iC+1)
        xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
        yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
        pad.SetPad(xl+0.005,yl+0.005,xh-0.005,yh-0.005)
        pad.SetLeftMargin(0.15);  pad.SetBottomMargin(0.15);  frame[iC].GetXaxis().SetTitle("M_{p#bar{p}} - M^{TRUE}_{p#bar{p}} / [MeV/c^2]")
        frame[iC].GetXaxis().SetTitleSize(0.06)
        frame[iC].GetYaxis().SetTitleSize(0.06)
        frame[iC].GetXaxis().SetTitleOffset(0.90)
        frame[iC].GetYaxis().SetTitleOffset(0.90)
        frame[iC].GetXaxis().SetTitleFont(12)
        frame[iC].GetYaxis().SetTitleFont(12)
        frame[iC].GetXaxis().SetLabelSize(0.05)
        frame[iC].GetYaxis().SetLabelSize(0.05)
        frame[iC].GetXaxis().SetLabelFont(62)
        frame[iC].GetYaxis().SetLabelFont(62)
        frame[iC].Draw()
        #frame[iC].SetMaximum(6.e2)
        frame[iC].SetMinimum(0.1)
        texMC.DrawLatex(0.6, 0.80, "LHCb simulation")
        texMC.DrawLatex(0.6, 0.75, "#sqrt{s}=13 TeV")
        texMC.DrawLatex(0.25, 0.75, names[iC])
        #pad.SetLogy()



    nameTxt = ""
    nameRoot = ""
    nameWksp = ""
    namePic = ""
    if iTz!=0:
        nameGen = homeDir + "7Bins/MC_MassResolution" + add
    else:
        nameGen = homeDir + "MC_MassResolution" + add
        
    postfix = getpostfix(iPT, iY, iTz)

    nameTxt  = f"{nameGen}{postfix}.txt"
    nameWksp = f"{nameGen}_wksp{postfix}.root"
    nameRoot = f"{nameGen}_Fit_plot{postfix}.root"
    namePic  = f"{nameGen}{postfix}.pdf"

    import os, sys
    save = os.dup( sys.stdout.fileno() )
    newout = open(nameTxt, "w" )
    os.dup2( newout.fileno(), sys.stdout.fileno() )
    r.Print("v")
    print("chi2 etac_c prompt %6.4f \n"%(chi2_etac_prompt))
    print("chi2 etac_c fromb %6.4f \n"%(chi2_etac_fromB))
    print("chi2 jpsi prompt %6.4f \n"%(chi2Jpsi_prompt))
    print("chi2 jpsi fromb %6.4f \n"%(chi2Jpsi_fromB))
    r.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() )
    newout.close()


    w.writeToFile(nameWksp)
    fFit = TFile (nameRoot,"RECREATE")


    c.Write("")
    fFit.Write()
    fFit.Close()

    c.SaveAs(namePic)
    #fo.write(r)
    r.correlationMatrix().Print("v")
    r.globalCorr().Print("v")






def MC_Resolution_Fit():
    # fitData(0,gauss=False)
    # fitData(0,gauss=True)
    nPTBins = 6
    nYBins = 4
    nTzBins = 7
    for iPT in range(1, nPTBins+1):
        # fitData(iPT,0, 0, gauss=True, empty=False)
        fitData(iPT,0, 0, gauss=False, empty=False)
    for iY in range(1, nYBins+1):
    #     fitData(0,iY, 0, gauss=True, empty=False)
        fitData(0,iY, 0, gauss=False, empty=False)
    # for iTz in range(1, nTzBins+1):
    # #     fitData(0, 0, iTz, gauss=True, empty=False)
    #     fitData(0, 0, iTz, gauss=False, empty=False)

    # get integral fit pars
    #fitData(0,gauss=True, empty=False)

if __name__ == "__main__":
    gROOT.LoadMacro("../libs/lhcbStyle.C")
    # fitData(0,gauss=False)
    # fitData(0,gauss=True)
    MC_Resolution_Fit()






