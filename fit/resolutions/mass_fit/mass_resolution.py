# from ROOT import *
from ROOT import gROOT, TChain, TTree, TH1D, TLatex, TCanvas, TFile, \
    RooFit, RooRealVar, RooWorkspace, RooArgSet, RooArgList, RooFormulaVar, \
    RooGaussian, RooChebychev, RooFFTConvPdf, RooAddPdf, RooDataSet
from ROOT.RooFit import *
# from ROOT.RooStats import *

gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../libs/libBifurcatedCB/BifurcatedCB.cxx+")

homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

import sys
sys.path.insert(1, "../mass_fit/")

from drawModule import binWidthDraw, binWidth, mrkSize, lineWidth1, name
import charmConstLib as cl

minM_MC_Low = 2850
maxM_MC_Low = 3120
binN_MC_Low = int((maxM_MC_Low-minM_MC_Low)/binWidthDraw)

minM_MC_High = 3505
maxM_MC_High = 3775
binN_MC_High = int((maxM_MC_High-minM_MC_High)/binWidthDraw)

binN_MC_Tot     = int((maxM_MC_High-minM_MC_Low)/binWidthDraw)
binN_MC_Tot_Fit = int((maxM_MC_High-minM_MC_Low)/binWidth)

binning_MC = RooFit.Binning(binN_MC_Tot, minM_MC_Low, maxM_MC_High)
binning_MC_Jpsi = RooFit.Binning(binN_MC_Low, minM_MC_Low, maxM_MC_Low)
binning_MC_etac2s = RooFit.Binning(binN_MC_High, minM_MC_High, maxM_MC_High)

range_MC_tot = RooFit.Range(minM_MC_Low,maxM_MC_High)
range_MC_Jpsi = RooFit.Range(minM_MC_Low,maxM_MC_Low)
range_MC_etac2s = RooFit.Range(minM_MC_High,maxM_MC_High)


def get_data_s(w, kRange, kSource):

    ccbar_mass  = names[kRange][1]

    nt = TChain("DecayTree")
    nt.Add(dataDir+'MC/Trigger/{}DiProton_{}_Mag*_AddBr.root'.format(kRange,kSource))

    hh = TH1D("hh","hh", 2000, ccbar_mass-40., ccbar_mass+40.)

    nt.Draw("Jpsi_m_scaled>>hh","","goff")
    Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M", ccbar_mass-40., ccbar_mass+40.)

    #ds = RooDataHist('ds_{}'.format(kRange),'ds_{}'.format(kRange), RooArgList(Jpsi_M) ,hh)

    tree = TTree()
    tree = nt.CopyTree("")

    ds = RooDataSet('ds_{}'.format(kRange),'ds_{}'.format(kRange),tree,RooArgSet(Jpsi_M))
    getattr(w,'import')(ds)


    print("DATA IS STORED")

names = {
    "jpsi":   ["jpsi", cl.M_JPSI, 0.],
    "etac":   ["etac", cl.M_ETAC, cl.GAMMA_ETAC],
    "etac2S": ["etac2s", cl.M_ETAC2S, cl.GAMMA_ETAC2S], #10 in generator
    "chic0":  ["chic0", cl.M_CHIC0, cl.GAMMA_CHIC0],
    "chic1":  ["chic1", cl.M_CHIC1, cl.GAMMA_CHIC1],
    "chic2":  ["chic2", cl.M_CHIC2, cl.GAMMA_CHIC2],
    "psi2S":  ["psi2S", cl.M_PSI2S, 0.]
}

def fillRelWorkspaceSplit(w, kRange, kCB=True):

    Jpsi_M = w.var('Jpsi_M')
    ccbar_name  = names[kRange][0]
    ccbar_mass  = names[kRange][1]
    ccbar_gamma = names[kRange][2]

    ratioNtoW = 0.30
    ratio_etac2etac2s = 1.1
    ratioArea = 0.90


    #ratioNtoW,ratioEtaTo_etac2s,ratioArea, gamma_etac, eff = getConstPars()

    r_etacToetac2s = RooRealVar('r_etacToetac2s','r_etacToetac2s', ratio_etac2etac2s, 0., 3.)
    r_NToW = RooRealVar('r_NToW_{}'.format(kRange),'rNarToW',ratioNtoW, 0., 1.)
    r_G1ToG2 = RooRealVar('r_G1ToG2_{}'.format(kRange),'rG1toG2',ratioArea, 0., 1.)
    n_ccbar = RooRealVar('n_{}'.format(ccbar_name),'num of etac', 1e3, 10, 5.e5)

    n_ccbar_1 = RooFormulaVar('n_{}_1'.format(ccbar_name),'num of _etac','@0*@1',RooArgList(n_ccbar,r_G1ToG2))
    n_ccbar_2 = RooFormulaVar('n_{}_2'.format(ccbar_name),'num of _etac','@0-@1',RooArgList(n_ccbar,n_ccbar_1))


    mass  = RooRealVar('mass_{}'.format(ccbar_name),'mean of gaussian', ccbar_mass, ccbar_mass-50., ccbar_mass+50.)
    gamma = RooRealVar('gamma_{}'.format(ccbar_name),'width of Br-W', ccbar_gamma, 10., 50. )
    gamma.setConstant(True)

    spin_etac = RooRealVar('spin_etac','spin_eta', 0. )
    radius_etac = RooRealVar('radius_etac','radius', 1.)
    proton_m = RooRealVar('proton_m','proton mass', cl.M_PROTON )


    sigma_1 = RooRealVar('sigma_{}_1'.format(ccbar_name),'width of gaussian', 9., 0.1, 30.)
    sigma_2 = RooFormulaVar('sigma_{}_2'.format(ccbar_name),'width of gaussian','@0/@1',RooArgList(sigma_1,r_NToW))


    #Fit etac
    brw = RooRelBreitWigner('brw_{}'.format(ccbar_name), 'brw_etac',Jpsi_M, mass, gamma, spin_etac,radius_etac,proton_m,proton_m)

    if kCB:
        alpha_1 = RooRealVar('alpha_{}_1'.format(ccbar_name),'alpha of CB', 1., 0.0, 10.)
        alpha_2 = RooRealVar('alpha_{}_2'.format(ccbar_name),'alpha of CB', 1., 0.0, 10.)
        nCB_1 = RooRealVar('nCB_{}_1'.format(ccbar_name),'n of CB', 1., 0.0, 100.)
        nCB_2 = RooRealVar('nCB_{}_2'.format(ccbar_name),'n of CB', 1., 0.0, 100.)
        resolution_1 = BifurcatedCB("cb_{}_1".format(ccbar_name), "Cystal Ball Function", Jpsi_M, RooFit.RooConst(0), sigma_1, alpha_1, nCB_1, alpha_1, nCB_1)
    else:
        resolution_1 = RooGaussian('gauss_{}_1'.format(ccbar_name),'gauss_etac_1 PDF', Jpsi_M, RooFit.RooConst(0),  sigma_1) #mass_etac -> mean_etac2s
        resolution_2 = RooGaussian('gauss_{}_2'.format(ccbar_name),'gauss_etac_2 PDF', Jpsi_M, RooFit.RooConst(0),  sigma_2) #mass_etac -> mean_etac2s


    bwxg_1 = RooFFTConvPdf('bwxg_{}_1'.format(ccbar_name),'breit-wigner (X) gauss', Jpsi_M, brw, resolution_1)
    ##bwxg_2 = RooFFTConvPdf('bwxg_{}_2'.format(ccbar_name),'breit-wigner (X) gauss', Jpsi_M, brw, resolution_2)


    ##Fit J/psi
    #resolution_1 = RooGaussian('gauss_{}_1'.format(ccbar_name),'gaussian PDF',Jpsi_M, mass, sigma_1)
    #resolution_2 = RooGaussian('gauss_{}_2'.format(ccbar_name),'gaussian PDF',Jpsi_M, mass, sigma_2)

    a0 = RooRealVar("a0","a0",0.4,-1.,1.)
    a1 = RooRealVar("a1","a1",0.05,-1.,1.)
    a2 = RooRealVar("a2","a2",-0.005,-1.,1.)
    bkg = RooChebychev ("bkg","Background",Jpsi_M,RooArgList(a0,a1))

    n_bkg = RooRealVar('n_bkg','num of etac', 1e3, 10, 5.e5)


    #if (bkgType == 'Base'):
        ##a0.setRange(0.0, 5.e2)
        #a1.setVal(0.04)
        #a2.setVal(0.04)
        #bkg = RooGenericPdf("bkg_%s"%(key),"background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.)",RooArgList(Jpsi_M,a0,a1))
        ##bkg = RooGenericPdf("bkg","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*@0.+@3*(2*@0*@0-1))",RooArgList(Jpsi_M,a0,a1,a2))
    #elif (bkgType == 'Chebychev3par'):
        #bkg = RooChebychev ("bkg_%s"%(key),"Background",Jpsi_M,RooArgList(a0,a1,a2))
    #elif (bkgType == 'Chebychev4par'):
        #bkg = RooChebychev("bkg_%s"%(key),"Background",Jpsi_M,RooArgList(a0,a1,a2,a3))



    #model = RooAddPdf('model_{}'.format(kRange),'{} signal'.format(ccbar_name), RooArgList(resolution_1), RooArgList(n_ccbar))
    #model = RooAddPdf('model_{}'.format(kRange),'{} signal'.format(ccbar_name), RooArgList(resolution_1, bkg), RooArgList(n_ccbar, n_bkg))
    model = RooAddPdf('model_{}'.format(kRange),'{} signal'.format(ccbar_name), RooArgList(bwxg_1), RooArgList(n_ccbar))
    #model = RooAddPdf('model_{}'.format(kRange),'{} signal'.format(ccbar_name), RooArgList(bwxg_1, bkg), RooArgList(n_ccbar, n_bkg))
    #model = RooAddPdf('model_{}'.format(kRange),'{} signal'.format(ccbar_name), RooArgList(bwxg_1, bwxg_2), RooArgList(n_ccbar_1, n_ccbar_2))
    #model = RooAddPdf('model_{}'.format(kRange),'{} signal'.format(ccbar_name), RooArgList(gauss_1, gauss_2), RooArgList(n_ccbar_1, n_ccbar_2))

    data = w.data('ds_{}'.format(kRange))

    getattr(w,'import')(model)

def set_frame(w, kRange):

    ccbar_mass  = names[kRange][1]

    Jpsi_M = w.var('Jpsi_M')
    data = w.data('ds_{}'.format(kRange))
    model = w.pdf('model_{}'.format(kRange))


    minM_MC = ccbar_mass-40.
    maxM_MC = ccbar_mass+40.
    binN_MC = int((maxM_MC-minM_MC)/binWidthDraw)

    binning    = RooFit.Binning(binN_MC, minM_MC, maxM_MC)
    range_draw = RooFit.Range(minM_MC, maxM_MC)


    title = kRange

    frame = Jpsi_M.frame(RooFit.Title('c #bar{c} to p #bar{p}')) #, RooFit.Range("fitRange_{}".format(kRange))
    data.plotOn(frame, binning, mrkSize, lineWidth1, name("data_{}".format(kRange))) #binning_MC_Jpsi

    model.paramOn(frame,RooFit.Layout(0.68,0.99,0.99))
    #frame.getAttText().SetTextSize(0.027)
    model.plotOn(frame, range_draw, lineWidth1) #range_MC_Jpsi ,
    chi2 = frame.chiSquare()
    print('chi2_{} = '.format(kRange), chi2)

    return frame, chi2


def draw(w,kRange):

    frame, chi2 = set_frame(w,kRange)

    texMC = TLatex();
    texMC.SetNDC();

    c = TCanvas('Masses_Fit','Masses Fit',600,400)
    #c.Divide(1)
    #c.cd(1).SetPad(.005, .505, .995, .995)
    #gPad.SetLeftMargin(0.15),
    frame.Draw()
    #frame.SetMaximum(1.e3)
    #frame.SetMinimum(0.1)
    frame.GetXaxis().SetTitle('M_{p#bar{p}} / [MeV/c^2]'),
    frame.GetXaxis().SetTitleSize(0.14)
    frame.GetYaxis().SetTitleSize(0.14)
    #gPad.SetLogy()
    texMC.DrawLatex(0.7, 0.5, "#chi^{2} = %4.2f"%chi2)
    return c

def fitData(kSource, kRange, kCB=False):

    w = RooWorkspace('w_{}'.format(kRange),True)

    get_data_s(w, kRange, kSource)
    fillRelWorkspaceSplit(w,kRange,kCB)

    #Jpsi_M_Low = w_Low.var('Jpsi_M')
    #Jpsi_M_High = w_High.var('Jpsi_M')


    model = w.pdf('model_{}'.format(kRange))
    data = w.data('ds_{}'.format(kRange))

    r = model.fitTo(data, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
    r = model.fitTo(data, RooFit.Extended(True), RooFit.Strategy(2) ,RooFit.Offset(True), RooFit.Save(True))
    r = model.fitTo(data, RooFit.Extended(True), RooFit.Minos(True), RooFit.Offset(True), RooFit.Save(True))


    gROOT.ProcessLine('gStyle->SetOptStat(000000000)')
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')

    if kCB: modName="_CB"
    else:   modName=""

    nameTxt  = homeDir+'/MC/mass_fit/fit_M_res_{}_{}_split{}.txt'.format(kSource,kRange,modName)
    nameWksp = homeDir+'/MC/mass_fit/MC_M_res_{}_{}_wksp_split{}.root'.format(kSource,kRange,modName)
    nameRoot = homeDir+'/MC/mass_fit/MC_M_res_{}_{}_split{}.root'.format(kSource,kRange,modName)
    namePic  = homeDir+'/MC/mass_fit/MC_M_res_{}_{}_split{}.pdf'.format(kSource,kRange,modName)

    w.writeToFile(nameWksp)

    c = draw(w,kRange)
    c.SaveAs(namePic)

    fFit = TFile(nameRoot,'RECREATE')
    c.Write('')
    fFit.Write()
    fFit.Close()

    #r.correlationMatrix().Print('v')
    #r.globalCorr().Print('v')


    import os, sys
    save = os.dup( sys.stdout.fileno() )
    with open(nameTxt, 'w' ) as newout:
        os.dup2( newout.fileno(), sys.stdout.fileno() )
        r.Print("v")
        r.correlationMatrix().Print()
        #print("chi2 eta_c(1S) {:6.4f} \n".format(chi2_etac))
        #print("chi2 eta_c(2S) {:6.4f} \n".format(chi2_etac2s))
        os.dup2( save, sys.stdout.fileno() )


ccbar_list = ["etac","etac2S","chic0","chic1","chic2"]
kSource = "fromB"

#for ccbar in ccbar_list:
    #fitData(kSource,ccbar,False)

#ccbar_list = ["jpsi","psi2S"]
#kSource = "prompt"

for ccbar in ccbar_list:
    fitData(kSource,ccbar,False)
