from ROOT import TFile, gROOT
gROOT.LoadMacro("../libs/libBifurcatedCB/BifurcatedCB.cxx+")

homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

parNames = {
    "mean"      : "Gaussian mean [\\mevcc~]",
    "alpha_1"   : "$\\alpha_{1}$",
    "alpha_2"   : "$\\alpha_{2}$",
    "nCB_1"     : "$n_{CB,1}$",
    "nCB_2"     : "$n_{CB,2}$",
    "r_G1ToG2"  : "$f_{n}$",
    "r_NToW"    : "$\\sigma_{n}/\\sigma_{w}$",
    "r_ref_etac": "$\\sigma_{n,\\etacones}/\\sigma_{n,\\jpsi}$",
    "r_ref_etac2S"  : "$\\sigma_{n,\\etactwos}/\\sigma_{n,\\jpsi}$",
    "r_ref_psi2S"   : "$\\sigma_{n,\\psitwos}/\\sigma_{n,\\jpsi}$",
    "r_ref_chic0"   : "$\\sigma_{n,\\chiczero}/\\sigma_{n,\\jpsi}$",
    "r_ref_chic1"   : "$\\sigma_{n,\\chicone}/\\sigma_{n,\\jpsi}$",
    "r_ref_chic2"   : "$\\sigma_{n,\\chictwo}/\\sigma_{n,\\jpsi}$",
    "r_ref_jpsi": "$\\sigma_{n,\\jpsi}/\\sigma_{n,\\etacones}$",
}
def print_table(vals):

    for src, params in vals.items():
        print(f"& {src}",end=" ")
    print("\\\\ \\hline \\hline")
    for par, names in parNames.items():
        print(f"{names}", end=" ")
        for src, params in vals.items():
            if par in params.keys():
                print(f"& {params[par]}",end=" ")
        print("\\\\")


def get_values(name_list:list, srcs:list, kCB:bool=False):

    dict_val = {}
    if kCB: modName="_CB"
    else:   modName=""

    ref_res = name_list[0]
    # print(f"{'source':20s} & r_G1ToG2 & r_NToW",end=" ")
    # for name in name_list[1:]:
    #     print(f'& r_ref_{name}',end=" ")
    # print("\\\\ \\hline\\hline")
    prec = 2
    for src in srcs:
        dict_val[src] = {}
        nameWksp = f"{homeDir}/MC/mass_fit/Wksp_M_res_{src}_{ref_res}_sim{modName}.root"
        f = TFile(nameWksp, "READ")
        w = f.Get("w").Clone()
        f.Close()
        # print(f"{src}", end=" ")
        dict_val[src]["mean"] = f"{round(w.var('mean').getVal(),prec):.2f} $\pm$ {round(w.var('mean').getError(),prec):.2f}"
        if kCB:
            dict_val[src]["alpha_1"] = f"{round(w.var('alpha_1').getVal(),prec)} $\pm$ {round(w.var('alpha_1').getError(),prec)}"
            dict_val[src]["alpha_2"] = f"{round(w.var('alpha_2').getVal(),prec)} $\pm$ {round(w.var('alpha_2').getError(),prec)}"
            dict_val[src]["nCB_1"] = f"{round(w.var('nCB_1').getVal(),prec)} $\pm$ {round(w.var('nCB_1').getError(),prec)}"
            dict_val[src]["nCB_2"] = f"{round(w.var('nCB_2').getVal(),prec)} $\pm$ {round(w.var('nCB_2').getError(),prec)}"
        else:
            dict_val[src]["r_NToW"] = f"{round(w.var('r_NToW').getVal(),prec)} $\pm$ {round(w.var('r_NToW').getError(),prec)}"
            dict_val[src]["r_G1ToG2"] = f"{round(w.var('r_G1ToG2').getVal(),prec)} $\pm$ {round(w.var('r_G1ToG2').getError(),prec)}"
        # print(f"& {w.var('r_G1ToG2').getVal():.3f} \pm {w.var('r_G1ToG2').getError():.3f}", end=" ")
        # print(f"& {w.var('r_NToW').getVal():.3f} \pm {w.var('r_NToW').getError():.3f}", end=" ")
        for name in name_list[1:]:
            dict_val[src][f"r_ref_{name}"] = f"{round(w.var(f'r_ref_{name}').getVal(),prec)} $\pm$ {round(w.var(f'r_ref_{name}').getError(),prec)}"
            # print(f"& {w.var(f'r_ref_{name}').getVal():.3f} \pm {w.var(f'r_ref_{name}').getError():.3f}", end=" ")
        # print("\\\\")

    print_table(dict_val)

if __name__ == "__main__":
    '''
    Prints a table comparing resolution parameters from MC fits of prompt, from-b and sum
    '''
    ccbar_list = ["jpsi","etac","psi2S","etac2S","chic0","chic1","chic2"]
    kSources = ["prompt", "fromB", "all"]

    # get_values(ccbar_list, kSources, True)
    get_values(ccbar_list, kSources, False)
