from ROOT import gROOT, \
    TTree, TChain, TFile, TCanvas, TLatex, \
    RooFit, RooFormulaVar, RooRealVar, RooDataSet, RooArgSet, RooArgList, \
    RooGaussian, RooAddPdf, RooGenericPdf, RooWorkspace, RooAbsReal

gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+");
homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

def getData_d(w, iTz=0):
    
    nPTBins = 6
    nTzBins = 10
    pt = (['5000', '6500'],['6500', '8000'], ['8000', '10000'], ['10000', '12000'], ['12000', '14000'], ['14000', '20000'])
    
    ntJpsi= TChain('DecayTree')

    #-----------new MC--------------------------------------------------------    
    ntJpsi.Add(dataDir+'MC/Trigger/pppi0DiProton_*_2018_AddBr.root')
    
    treeJpsi = TTree()
        
    
    if(iTz == 0):        
        treeJpsi = ntJpsi.CopyTree("")
    # elif (iTz != 0):
    #     cutJpsiTzL = 'Jpsi_Tz > %f'%(tz[iTz-1])
    #     cutJpsiTzR = 'Jpsi_Tz < %f'%(tz[iTz])

    #     treeJpsi = ntJpsi.CopyTree( cutJpsiTzL + '&&' + cutJpsiTzR)
      
    else:
        print ('Incorrect number of Tz bin %s'%(iTz))
    
    
    Jpsi_M = RooRealVar ('Jpsi_M','Jpsi_M',2850.,3100.0) 
    dsJpsi = RooDataSet('dsJpsi','dsJpsi',treeJpsi,RooArgSet(Jpsi_M))
    getattr(w,'import')(dsJpsi)
    
    #dsEtac_Prompt.Draw('')
    #dsJpsi_Prompt.Draw('')
    

def getConstPars():

    f = TFile("{}//MC/mass_fit/Wksp_M_res_all_jpsi_sim.root".format(homeDir), "READ")
    # f = TFile("{}//MC/mass_fit/MC_M_res_all_jpsi_wksp_sim.root".format(homeDir), "READ")
    wMC = f.Get("w")
    f.Close()
    
    rNtoW = wMC.var("r_NToW").getValV()
    rEtaToJpsi = wMC.var("r_ref_etac").getValV()
    rArea = wMC.var("r_G1ToG2").getValV()
    gamma = 32.0
    effic = 0.035
    #gamma = 21.3
    #effic = 0.06
    #ratioNtoW = 0.5
    #ratioEtaToJpsi = 0.88
    #ratioArea = 0.9
    
    
    return rNtoW,rEtaToJpsi,rArea, gamma, effic

def fillRelWorkspace(w):

    
    Jpsi_M = w.var('Jpsi_M')
    #Jpsi_M.setBins(1000,'cache')
    Jpsi_M.setRange("Signal", 2850, 2961.9)
    Jpsi_M.setRange("Draw", 2850, 3100)

    jpsiMass = 3096.90
    piMass = 134.977
    thresMass = jpsiMass-piMass

    ratioNtoW,ratioEtaToJpsi,ratioArea, _, _ = getConstPars()

    rEtaToJpsi = RooRealVar('rEtaToJpsi','rEtaToJpsi', ratioEtaToJpsi)
    rNarToW = RooRealVar('rNarToW','rNarToW',ratioNtoW)
    rG1toG2 = RooRealVar('rG1toG2','rG1toG2',ratioArea)

    mean_Jpsi = RooRealVar('mean_Jpsi','mean of gaussian', 0.0, -50.0, 50.0)   
    sigma_Jpsi_1 = RooRealVar('sigma_Jpsi_1','width of gaussian', 9., 0.1, 50.) 
    sigma_Jpsi_2 = RooFormulaVar('sigma_Jpsi_2','width of gaussian','@0/@1',RooArgList(sigma_Jpsi_1,rNarToW))    

    mThres = RooRealVar("M_thres", "varThres", thresMass)
        #Fit J/psi
    gauss_1 = RooGaussian('gauss_1','gaussian PDF',Jpsi_M, RooFit.RooConst(0.), sigma_Jpsi_1) 
    gauss_2 = RooGaussian('gauss_2','gaussian PDF',Jpsi_M, RooFit.RooConst(0.), sigma_Jpsi_2) 

    #nPPPi0 = RooFormulaVar("nPPPi0_PT%s_Tz%s"%(nPT,nTz),"nPPPi0","@0*@1*(1.19/2.12)",RooArgList(nJpsi,eff_pppi0))

    modelRes = RooAddPdf('modelRes','resolution', RooArgList(gauss_1, gauss_2), RooArgList(rG1toG2))
    modelRoot = RooGenericPdf("modelRoot", "modelRoot", "(sqrt(@1 - @0))*(@0<@1)", RooArgList(Jpsi_M, mThres));
    #model = RooFFTConvPdf("model", "pdfFD", Jpsi_M, modelRoot, modelRes);
    # model = RooGenericPdf("model", "model", "(sqrt(@1 - @0))*(@0<@1)", RooArgList(Jpsi_M, mThres));
    model = RooGenericPdf("model","Jpsi.pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))

    #nPPPi0 = RooRealVar('nPPPi0','num of J/Psi', 1e3, 10, 1.e4)

    #pppi0 = RooGenericPdf("pppi0","Jpsi.pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))
    ##nPPPi0 = RooFormulaVar("nPPPi0","nPPPi0","@0*@1*(1.19/2.12)",RooArgList(nJpsi,eff_pppi0))
    
    #model = RooAddPdf("model","background", RooArgList(pppi0), RooArgList(nPPPi0))

    getattr(w,'import')(model)
    




def fitData(iTz):

    
    gROOT.Reset()
    #TProof *proof = TProof.Open('')
    w = RooWorkspace('w',True)   
    
    getData_d(w, iTz)
    
    fillRelWorkspace(w)
    
    Jpsi_M = w.var('Jpsi_M')
        
    model= w.pdf('model')
    dataJpsi = w.data('dsJpsi')
            
    gROOT.ProcessLine('gStyle->SetOptStat(000000000)')
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')

    frame = Jpsi_M.frame(RooFit.Title('J/#psi to p#bar{p}#pi^{0}')) 
    dataJpsi.plotOn(frame,RooFit.Binning(100,2850,3100))
    
    #model.paramOn(frame,RooFit.Layout(0.68,0.99,0.99))
    #frame.getAttText().SetTextSize(0.027) 
    #n = w.var("nPPPi0").getValV()
    model.plotOn(frame)#,RooFit.Range("Signal"))#,RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Jpsi = frame.chiSquare()
    texJpsiPiMC1 = TLatex(0.7, 0.8, "#chi^{2} = %4.2f"%chi2Jpsi);
    texJpsiPiMC1.SetNDC();
    frame.addObject(texJpsiPiMC1);
    #model.plotOn(frame,RooFit.Components(RooArgSet(gauss_1,gauss_2)),RooFit.FillStyle(3005),RooFit.FillColor(kMagenta),RooFit.DrawOption('F'), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print('chi2Jpsi = ', chi2Jpsi)
        
    c = TCanvas('Masses_Fit','Masses Fit',600,500)
    c.cd(1)
    #gPad.SetLeftMargin(0.15)
    frame.GetYaxis().SetTitle('Candidates/ (2.5 MeV/c^{2})'),  
    frame.GetXaxis().SetTitle('M_{p#bar{p}} [MeV/c^{2}]'),  
    frame.Draw()
    frame.SetMaximum(7.e1)
    frame.SetMinimum(1.1)
    #gPad.SetLogy()
    
    
    
    nameTxt = ''
    nameRoot = ''
    nameWksp = ''
    namePic = ''
    
    if(iTz == 0):
    
        nameTxt  = homeDir+'MC/mass_fit/Result_M_pppi0.txt'
        nameWksp = homeDir+'MC/mass_fit/Wksp_M_pppi0_wksp.root'
        nameRoot = homeDir+'MC/mass_fit/Plot_M_pppi0_plot.root'
        namePic  = homeDir+'MC/mass_fit/Plot_M_pppi0.pdf'    
    
    else:
    
        nameTxt = homeDir+'MC/mass_fit/Result_M_pppi0_Tz%s.txt'%(iTz)
        nameWksp = homeDir+'MC/mass_fit/Wksp_M_pppi0_wksp_Tz%s.root'%(iTz)
        nameRoot = homeDir+'MC/mass_fit/Plot_M_pppi0_Fit_Tz%s.root'%(iTz)
        namePic = homeDir+'MC/mass_fit/Plot_M_pppi0_Tz%s.pdf'%(iTz)
    
    
    w.writeToFile(nameWksp)
    fFit = TFile (nameRoot,'RECREATE')
    
    
    c.Write('')
    fFit.Write()
    fFit.Close()
    
    c.SaveAs(namePic)
    
    import os, sys 
    save = os.dup( sys.stdout.fileno() ) 
    newout = open(nameTxt, 'w' ) 
    os.dup2( newout.fileno(), sys.stdout.fileno() ) 
    model.Print("v") 
    os.dup2( save, sys.stdout.fileno() ) 
    newout.close()


def MC_Resolution_Fit():

    nTzBins = 10 
    for iTz in range(1,nTzBins+1):
        fitData(iTz)  
    

if __name__ == "__main__":
    #MC_Resolution_Fit()
    gROOT.LoadMacro("../libs/lhcbStyle.C")
    fitData(0)



