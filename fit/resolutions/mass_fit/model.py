from ROOT import gROOT, RooFit, std, \
    RooRealVar, RooDataSet, RooFormulaVar, RooArgList, RooArgSet, \
    RooAddPdf, RooGaussian, RooChebychev, RooCategory, RooSimultaneous, RooFFTConvPdf
# from ROOT import *

gitDir  = "/users/LHCb/zhovkovska/etac2s_ppbar/fit/"

gROOT.LoadMacro(f"{gitDir}/libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro(f"{gitDir}/libs/libBifurcatedCB/BifurcatedCB.cxx+")
from ROOT import RooRelBreitWigner, BifurcatedCB

import sys
sys.path.insert(1, f"{gitDir}/mass_fit/")
sys.path.insert(1, f"{gitDir}/libs/")

from charmonium import Particle

proton = Particle("p")
m_win = 100.

def fill_workspace(w, name, ref="etac", bw=False, cb=False):

    Jpsi_M = w.var("Jpsi_M")
    ccbar = Particle(name)

    ratio_sigma = 0.30
    ratio_ref = 1.1 #ratio sigma_ccbar 2 sigma_ref
    ratio_area = 0.90


    #ratioNtoW,ratioEtaTo_etac2s,ratioArea, gamma_etac, eff = getConstPars()

    r_etacToetac2s = RooRealVar(f"r_ref_{name}","r_ref", ratio_ref, 0., 3.)
    #r_NToW = RooRealVar(f"r_NToW_{name}","rNarToW",ratio_sigma, 0., 1.)
    #r_G1ToG2 = RooRealVar(f"r_G1ToG2_{name}","rG1toG2",ratio_area, 0., 1.)
    n_ccbar = RooRealVar(f"n_{ccbar.name}","num of etac", 1e3, 10., 5.e5)

    if ref==ccbar.name:
        r_NToW = RooRealVar("r_NToW","rNarToW",ratio_sigma, 0., 1.)
        r_G1ToG2 = RooRealVar("r_G1ToG2","rG1toG2",ratio_area, 0., 1.)
        sigma_1 = RooRealVar(f"sigma_{ccbar.name}_1","width of gaussian", 9., 0.1, 30.)
    else:
        r_NToW = w.var("r_NToW")
        r_G1ToG2 = w.var("r_G1ToG2")
        sigma_ref = w.var(f"sigma_{ref}_1")
        sigma_1 = RooFormulaVar(f"sigma_{ccbar.name}_1","width of gaussian","@0*@1",RooArgList(RooArgSet(sigma_ref,r_etacToetac2s)))

    sigma_2 = RooFormulaVar(f"sigma_{ccbar.name}_2","width of gaussian","@0/@1",RooArgList(sigma_1,r_NToW))

    n_ccbar_1 = RooFormulaVar(f"n_{ccbar.name}_1","num of ","@0*@1",RooArgList(n_ccbar,r_G1ToG2))
    n_ccbar_2 = RooFormulaVar(f"n_{ccbar.name}_2","num of ","@0-@1",RooArgList(n_ccbar,n_ccbar_1))

    mass  = RooRealVar(f"mass_{ccbar.name}","mean of gaussian", ccbar.mass, ccbar.mass-m_win, ccbar.mass+m_win)
    #mass.setConstant(True)
    gamma = RooRealVar(f"gamma_{ccbar.name}","width of Br-W", ccbar.width, 10., 50. )
    gamma.setConstant(True)

    spin = RooRealVar(f"spin_{ccbar.name}","spin", ccbar.spin )
    radius = RooRealVar("radius","radius", 1.)
    proton_m = RooRealVar("proton_m","proton mass", proton.mass )


    #Fit etac

    if bw: mean = RooFit.RooConst(0)
    else: mean = mass

    brw = RooRelBreitWigner(f"brw_{ccbar.name}", "brw",Jpsi_M, mass, gamma, spin,radius,proton_m,proton_m)

    if cb:
        if ref==ccbar.name:
            alpha_1 = RooRealVar("alpha_1","alpha of CB", 1., 0.0, 10.)
            alpha_2 = RooRealVar("alpha_2","alpha of CB", 1., 0.0, 10.)
            nCB_1 = RooRealVar("nCB_1","n of CB", 1., 0.0, 100.)
            nCB_2 = RooRealVar("nCB_2","n of CB", 1., 0.0, 100.)
        else:
            alpha_1 = w.var("alpha_1")
            alpha_2 = w.var("alpha_2")
            nCB_1 = w.var("nCB_1")
            nCB_2 = w.var("nCB_2")
        # alpha_1 = RooRealVar("alpha_{}_1".format(ccbar.name),"alpha of CB", 1., 0.0, 10.)
        # alpha_2 = RooRealVar("alpha_{}_2".format(ccbar.name),"alpha of CB", 1., 0.0, 10.)
        # nCB_1 = RooRealVar("nCB_{}_1".format(ccbar.name),"n of CB", 1., 0.0, 100.)
        # nCB_2 = RooRealVar("nCB_{}_2".format(ccbar.name),"n of CB", 1., 0.0, 100.)
        resolution_1 = BifurcatedCB(f"cb_{ccbar.name}_1", "Cystal Ball Function", Jpsi_M, mean, sigma_1, alpha_1, nCB_1, alpha_1, nCB_1)
    else:
        resolution_1 = RooGaussian(f"gauss_{ccbar.name}_1","gauss_1 PDF", Jpsi_M, mean, sigma_1) #mass -> mean2s
        resolution_2 = RooGaussian(f"gauss_{ccbar.name}_2","gauss_2 PDF", Jpsi_M, mean, sigma_2) #mass -> mean2s


    a0 = RooRealVar("a0","a0",0.4,-1.,1.)
    a1 = RooRealVar("a1","a1",0.05,-1.,1.)
    a2 = RooRealVar("a2","a2",-0.005,-1.,1.)
    bkg = RooChebychev ("bkg","Background",Jpsi_M,RooArgList(a0,a1))

    n_bkg = RooRealVar("n_bkg","num of etac", 1e3, 10, 5.e5)


    model = RooAddPdf()
    if bw:
        pdf_1 = RooFFTConvPdf(f"bwxg_{ccbar.name}_1","breit-wigner (X) gauss", Jpsi_M, brw, resolution_1)
        pdf_2 = RooFFTConvPdf(f"bwxg_{ccbar.name}_2","breit-wigner (X) gauss", Jpsi_M, brw, resolution_2)
    else:
        pdf_1 = resolution_1
        pdf_2 = resolution_2
        #pdf = RooGaussian("gauss_{}_1".format(ccbar.name),"gauss_1 PDF", Jpsi_M, mass, sigma_1) #mass -> mean2s

    #model = RooAddPdf("model_{}".format(ccbar.name),"{} signal".format(ccbar.name), RooArgList(pdf_1), RooArgList(n_ccbar))
    model = RooAddPdf(f"model_{ccbar.name}",f"{ccbar.name} signal", RooArgList(pdf_1,pdf_2), RooArgList(n_ccbar_1,n_ccbar_2))
    getattr(w,"import")(model,RooFit.RecycleConflictNodes())

    return model, n_ccbar

    ##bwxg_2 = RooFFTConvPdf("bwxg_{}_2".format(ccbar.name),"breit-wigner (X) gauss", Jpsi_M, brw, resolution_2)

    ##Fit J/psi
    #resolution_1 = RooGaussian("gauss_{}_1".format(ccbar.name),"gaussian PDF",Jpsi_M, mass, sigma_1)
    #resolution_2 = RooGaussian("gauss_{}_2".format(ccbar.name),"gaussian PDF",Jpsi_M, mass, sigma_2)

    #model = RooAddPdf("model_{}".format(name),"{} signal".format(ccbar.name), RooArgList(resolution_1), RooArgList(n_ccbar))
    #model = RooAddPdf("model_{}".format(name),"{} signal".format(ccbar.name), RooArgList(resolution_1, bkg), RooArgList(n_ccbar, n_bkg))
    #model = RooAddPdf("model_{}".format(name),"{} signal".format(ccbar.name), RooArgList(bwxg_1), RooArgList(n_ccbar))
    #model = RooAddPdf("model_{}".format(name),"{} signal".format(ccbar.name), RooArgList(bwxg_1, bkg), RooArgList(n_ccbar, n_bkg))
    #model = RooAddPdf("model_{}".format(name),"{} signal".format(ccbar.name), RooArgList(bwxg_1, bwxg_2), RooArgList(n_ccbar_1, n_ccbar_2))
    #model = RooAddPdf("model_{}".format(name),"{} signal".format(ccbar.name), RooArgList(gauss_1, gauss_2), RooArgList(n_ccbar_1, n_ccbar_2))

    #data = w.data("ds_{}".format(name))

    #getattr(w,"import")(model)

def fill_res_workspace(w, name, ref="etac", cb=False):

    Jpsi_M = w.var("Jpsi_M_res")
    ccbar = Particle(name)

    ratio_sigma = 0.30
    ratio_ref = 1.1 #ratio sigma_ccbar 2 sigma_ref
    ratio_area = 0.90


    #ratioNtoW,ratioEtaTo_etac2s,ratioArea, gamma_etac, eff = getConstPars()

    r_etacToetac2s = RooRealVar(f"r_ref_{name}","r_ref", ratio_ref, 0., 3.)

    if ref==ccbar.name:
        r_NToW = RooRealVar("r_NToW","rNarToW",ratio_sigma, 0., 1.)
        r_G1ToG2 = RooRealVar("r_G1ToG2","rG1toG2",ratio_area, 0., 1.)
        sigma_1 = RooRealVar(f"sigma_{ccbar.name}_1","width of gaussian", 9., 0.1, 30.)
    else:
        r_NToW = w.var("r_NToW")
        r_G1ToG2 = w.var("r_G1ToG2")
        sigma_ref = w.var("sigma_{}_1".format(ref))
        sigma_1 = RooFormulaVar(f"sigma_{ccbar.name}_1","width of gaussian","@0*@1",RooArgList(RooArgSet(sigma_ref,r_etacToetac2s)))

    sigma_2 = None

    n_ccbar = RooRealVar(f"n_{ccbar.name}","num of etac", 1e3, 10., 5.e5)
    n_ccbar_1 = None
    n_ccbar_2 = None

    mass  = RooRealVar(f"mass_{ccbar.name}","mean of gaussian", ccbar.mass, ccbar.mass-m_win, ccbar.mass+m_win)
    #mass.setConstant(True)
    gamma = RooRealVar(f"gamma_{ccbar.name}","width of Br-W", ccbar.width, 10., 50. )
    gamma.setConstant(True)

    #Fit etac

    # mean = RooRealVar(f"mean_{ccbar.name}","mean of gaussian", 0., -2., 2.)
    mean = RooRealVar("mean","mean of gaussian", 0., -2., 2.)
    # mean = RooFit.RooConst(0)
    model = RooAddPdf()

    if cb:
        if ref==ccbar.name:
            alpha_1 = RooRealVar("alpha_1","alpha of CB", 1., 0.0, 10.)
            alpha_2 = RooRealVar("alpha_2","alpha of CB", 1., 0.0, 10.)
            nCB_1 = RooRealVar("nCB_1","n of CB", 1., 0.0, 100.)
            nCB_2 = RooRealVar("nCB_2","n of CB", 1., 0.0, 100.)
        else:
            alpha_1 = w.var("alpha_1")
            alpha_2 = w.var("alpha_2")
            nCB_1 = w.var("nCB_1")
            nCB_2 = w.var("nCB_2")
        # alpha_1 = RooRealVar(f"alpha_{ccbar.name}_1","alpha of CB", 1., 0.0, 10.)
        # alpha_2 = RooRealVar(f"alpha_{ccbar.name}_2","alpha of CB", 1., 0.0, 10.)
        # nCB_1 = RooRealVar(f"nCB_{ccbar.name}_1","n of CB", 1., 0.0, 100.)
        # nCB_2 = RooRealVar(f"nCB_{ccbar.name}_2","n of CB", 1., 0.0, 100.)
        resolution_1 = BifurcatedCB(f"cb_{ccbar.name}_1", "Cystal Ball Function", Jpsi_M, mean, sigma_1, alpha_1, nCB_1, alpha_2, nCB_2)
        model = RooAddPdf(f"model_{ccbar.name}",f"{ccbar.name} signal", RooArgList(resolution_1), RooArgList(n_ccbar))
    #elif (name=="jpsi" or name=="psi2S" or name=="etac"):
        #resolution_1 = RooGaussian(f"gauss_{ccbar.name}_1","gauss_1 PDF", Jpsi_M, mean, sigma_1) #mass -> mean2s
        #resolution_2 = RooGaussian(f"gauss_{ccbar.name}_2","gauss_2 PDF", Jpsi_M, mean, sigma_2) #mass -> mean2s
        #model = RooAddPdf(f"model_{ccbar.name}",f"{ccbar.name} signal", RooArgList(resolution_1,resolution_2), RooArgList(n_ccbar_1,n_ccbar_2))
    #else:
        #resolution_1 = RooGaussian(f"gauss_{ccbar.name}_1","gauss_1 PDF", Jpsi_M, mean, sigma_1) #mass -> mean2s
        #model = RooAddPdf(f"model_{ccbar.name}",f"{ccbar.name} signal", RooArgList(resolution_1), RooArgList(n_ccbar))
    else:
        sigma_2 = RooFormulaVar(f"sigma_{ccbar.name}_2","width of gaussian","@0/@1",RooArgList(sigma_1,r_NToW))
    
        n_ccbar_1 = RooFormulaVar(f"n_{ccbar.name}_1","num of ","@0*@1",RooArgList(n_ccbar,r_G1ToG2))
        n_ccbar_2 = RooFormulaVar(f"n_{ccbar.name}_2","num of ","@0-@1",RooArgList(n_ccbar,n_ccbar_1))
    
        resolution_1 = RooGaussian(f"gauss_{ccbar.name}_1","gauss_1 PDF", Jpsi_M, mean, sigma_1) #mass -> mean2s
        resolution_2 = RooGaussian(f"gauss_{ccbar.name}_2","gauss_2 PDF", Jpsi_M, mean, sigma_2) #mass -> mean2s
        model = RooAddPdf(f"model_{ccbar.name}",f"{ccbar.name} signal", RooArgList(resolution_1,resolution_2), RooArgList(n_ccbar_1,n_ccbar_2))


    #a0 = RooRealVar("a0","a0",0.4,-1.,1.)
    #a1 = RooRealVar("a1","a1",0.05,-1.,1.)
    #a2 = RooRealVar("a2","a2",-0.005,-1.,1.)
    #bkg = RooChebychev ("bkg","Background",Jpsi_M,RooArgList(a0,a1))

    #n_bkg = RooRealVar("n_bkg","num of etac", 1e3, 10, 5.e5)

    getattr(w,"import")(model,RooFit.RecycleConflictNodes())
    return model, n_ccbar

def combine(w, name_list):

    Jpsi_M = w.var("Jpsi_M_res")

    sample = RooCategory("sample","sample")
    for name in name_list:
        sample.defineType(name)

    datas = []
    simPdf = RooSimultaneous("simPdf","simultaneous signal pdf",sample)

    map_ds = std.map("std::string, RooDataSet*")()

    pdfs   = RooArgSet()
    yields = RooArgSet()
    for name in name_list:

        #ccbar = Particle(name)
        #Jpsi_M.setRange("fitRange_{}".format(name), ccbar.mass-m_win, ccbar.mass+m_win)
        Jpsi_M.setRange(f"fitRange_{name}", 0.-m_win, 0.+m_win)


        model = w.pdf(f"model_{name}")
        simPdf.addPdf(model,name)

        data = w.data(f"ds_{name}")
        data.Print()
        datas.append(data)
        map_ds.insert(map_ds.cbegin(), std.pair("const string,RooDataSet*")(name, data))

    combData = RooDataSet("combData", "combined data", RooArgSet(Jpsi_M), RooFit.Index(sample), RooFit.Import(map_ds))

    getattr(w,"import")(sample, RooFit.RecycleConflictNodes())
    getattr(w,"import")(combData, RooFit.RecycleConflictNodes())
    getattr(w,"import")(simPdf, RooFit.RecycleConflictNodes())



