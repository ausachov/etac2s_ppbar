from ROOT import gROOT, TGaxis, TChain, TTree, TFile, TLatex, TCanvas, TPad, \
    RooFit, RooDataSet, RooWorkspace, RooRealVar, RooArgSet

#gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
#gROOT.LoadMacro("../libs/libBifurcatedCB/BifurcatedCB.cxx+")

# homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"
gitDir  = "/users/LHCb/zhovkovska/etac2s_ppbar/fit/"

import sys
sys.path.insert(1, f"{gitDir}/mass_fit/")

#from drawModule import *
import oniaConstLib as cl

from model import fill_workspace, fill_res_workspace, combine, m_win

binWidthDraw = 5.0
binWidth = 0.25

mrkSize = RooFit.MarkerSize(0.5)
lineWidth1 = RooFit.LineWidth(1)
lineWidth2 = RooFit.LineWidth(2)
lineStyle1 = RooFit.LineStyle(2)
lineStyle2 = RooFit.LineStyle(9)
lineColor1 = RooFit.LineColor(6)
lineColor2 = RooFit.LineColor(8)
name = RooFit.Name
cut = RooFit.Cut

minM_MC_Low = 2850.
maxM_MC_Low = 3120.
binN_MC_Low = int((maxM_MC_Low-minM_MC_Low)/binWidthDraw)

minM_MC_High = 3505.
maxM_MC_High = 3775.
binN_MC_High = int((maxM_MC_High-minM_MC_High)/binWidthDraw)

binN_MC_Tot     = int((maxM_MC_High-minM_MC_Low)/binWidthDraw)
binN_MC_Tot_Fit = int((maxM_MC_High-minM_MC_Low)/binWidth)

binning_MC = RooFit.Binning(binN_MC_Tot, minM_MC_Low, maxM_MC_High)
binning_MC_Jpsi = RooFit.Binning(binN_MC_Low, minM_MC_Low, maxM_MC_Low)
binning_MC_etac2s = RooFit.Binning(binN_MC_High, minM_MC_High, maxM_MC_High)

range_MC_tot = RooFit.Range(minM_MC_Low,maxM_MC_High)
range_MC_Jpsi = RooFit.Range(minM_MC_Low,maxM_MC_Low)
range_MC_etac2s = RooFit.Range(minM_MC_High,maxM_MC_High)


def get_data_s(w, kRange, kSource):

    ccbar_mass  = names[kRange][1]

    nt = TChain("DecayTree")
    nt.Add(f'{dataDir}MC/Trigger/{kRange}DiProton_*_2018_AddBr.root')

    #hh = TH1D("hh","hh", 2000, ccbar_mass-m_win, ccbar_mass+m_win)

    #nt.Draw("Jpsi_m_scaled>>hh","","goff")
    Jpsi_M = w.var("Jpsi_M_res")

    #ds = RooDataHist('ds_{}'.format(kRange),'ds_{}'.format(kRange), RooArgList(Jpsi_M) ,hh)

    cut_PID = "ProtonP_ProbNNp > 0.6 && ProtonM_ProbNNp > 0.6"
    cut_sel = "sec"
    if kSource=="fromB" : cut_sel = f"sec && {cut_PID}"
    elif kSource=="all" : cut_sel = cut_PID
    else                : cut_sel = f"prompt && {cut_PID}"

    tree = TTree()
    tree = nt.CopyTree(cut_sel)

    ds = RooDataSet(f'ds_{kRange}',f'ds_{kRange}',tree,RooArgSet(Jpsi_M))
    w.Import(ds)
    # getattr(w,'import')(ds)


    print("DATA IS STORED")

names = {
    "jpsi":   ["jpsi", cl.M_JPSI, 0.],
    "etac":   ["etac", cl.M_ETAC, cl.GAMMA_ETAC],
    "etac2S": ["etac2s", cl.M_ETAC2S, cl.GAMMA_ETAC2S], #10 in generator
    "chic0":  ["chic0", cl.M_CHIC0, cl.GAMMA_CHIC0],
    "chic1":  ["chic1", cl.M_CHIC1, cl.GAMMA_CHIC1],
    "chic2":  ["chic2", cl.M_CHIC2, cl.GAMMA_CHIC2],
    "psi2S":  ["psi2S", cl.M_PSI2S, 0.]
}


def set_frame(w:RooWorkspace, kRange:str):

    #ccbar_mass  = names[kRange][1]
    ccbar_mass  = 0.

    Jpsi_M = w.var('Jpsi_M_res')
    data = w.data('ds_{}'.format(kRange))
    model = w.pdf('model_{}'.format(kRange))

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf")
    combData = w.data("combData")


    minM_MC = ccbar_mass-m_win
    maxM_MC = ccbar_mass+m_win
    binN_MC = int((maxM_MC-minM_MC)/binWidthDraw)

    binning    = RooFit.Binning(binN_MC, minM_MC, maxM_MC)
    range_draw = RooFit.Range(minM_MC, maxM_MC)


    title = kRange

    frame = Jpsi_M.frame(RooFit.Title('c #bar{c} to p #bar{p}')) #, RooFit.Range("fitRange_{}".format(kRange))
    combData.plotOn(frame, RooFit.Cut(f"sample==sample::{kRange}"), binning, mrkSize, lineWidth1, name(f"ds_{kRange}"))
    simPdf.plotOn(frame,lineWidth1,RooFit.Slice(sample,f"{kRange}"),RooFit.ProjWData(RooArgSet(sample),combData,True),name(f"model_{kRange}"))

    #data.plotOn(frame, binning, mrkSize, lineWidth1, name("data_{}".format(kRange))) #binning_MC_Jpsi
    # model.paramOn(frame, RooFit.Layout(0.58,0.99,0.99))
    # frame.getAttText().SetTextSize(0.025)

    #model.plotOn(frame, range_draw, lineWidth1) #range_MC_Jpsi ,
    chi2 = frame.chiSquare()
    print(f"chi2_{kRange}/ndf = {chi2}")

    h_pull = frame.pullHist()
    h_pull.SetMarkerSize(0.5)
    h_pull.SetFillColor(1)
    h_pull.SetLineColor(1)
    pull = Jpsi_M.frame(RooFit.Title(" ")) #,RooFit.Bins(100)
    for ii in range(h_pull.GetN()):
        h_pull.SetPointEYlow(ii,0)
        h_pull.SetPointEYhigh(ii,0)
    pull.SetMinimum(-4.0)
    pull.SetMaximum(4.0)
    pull.addPlotable(h_pull,"B")

    return frame, pull, chi2, minM_MC, maxM_MC


def draw(w:RooWorkspace, pad_full:TPad, kRange:str):

    frame, pull, chi2, minM_MC, maxM_MC = set_frame(w,kRange)

    texMC = TLatex()
    texMC.SetNDC()

    pad_full.Divide(1,2)

    pad = pad_full.cd(1)
    xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
    yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
    pad.SetPad(xl,yl-0.16,xh,yh)
    pad.SetLeftMargin(0.15)
    # pad.SetBottomMargin(0.20)
    pad.SetBottomMargin(0.)
    pad.SetTopMargin(0.09)
    #frame.SetMaximum(1.e3)
    #frame.SetMinimum(0.1)
    frame.GetXaxis().SetRangeUser(minM_MC, maxM_MC),
    # frame.GetXaxis().SetTitle('M_{p#bar{p}} / [MeV/c^{2}]'),
    # frame.GetXaxis().SetTitleSize(0.09)
    # frame.GetXaxis().SetLabelSize(0.08)
    frame.GetYaxis().SetTitle('Candidates / (5 MeV/c^{2})'),
    # frame.GetYaxis().SetTitleOffset(0.9)
    # frame.GetYaxis().SetTitleSize(0.09)
    # frame.GetYaxis().SetLabelSize(0.08)
    axis = frame.GetYaxis()
    axis.ChangeLabel(1, -1, -1, -1, -1, -1, " ");
    axis.SetLabelSize(0.09)
    axis.SetTitleOffset(0.7)
    axis.SetTitleSize(0.100)
    #gPad.SetLogy()
    frame.Draw()
    # axis.Draw()
    texMC.SetTextSize(0.085)
    texMC.DrawLatex(0.65, 0.7, "#chi^{2}/ndf = %4.2f"%chi2)

    pad = pad_full.cd(2)
    xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
    yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
    pad.SetPad(xl,yl,xh,yh-0.15)
    pad.SetTopMargin(0.)
    pad.SetBottomMargin(0.4)
    pad.SetLeftMargin(0.15)
    pull.GetXaxis().SetTitle('M_{p#bar{p}} / [MeV/c^{2}]'),
    pull.GetYaxis().SetTitle(''),
    pull.GetXaxis().SetTitleOffset(0.9)
    pull.GetXaxis().SetTitleSize(0.20)
    pull.GetXaxis().SetLabelSize(0.17)
    pull.GetYaxis().SetLabelSize(0.17)
    pull.GetYaxis().SetNdivisions(505)
    pull.GetYaxis().SetRangeUser(-13, 13)
    pull.Draw()

    #return pad

def drawSet(kSource:str, name_list:list, kCB:bool=False):

    ref_res = name_list[0]

    gROOT.ProcessLine('gStyle->SetOptStat(000000000)')
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')

    if kCB: modName="_CB"
    else:   modName=""

    nameWksp = f"{homeDir}/MC/mass_fit/Wksp_M_res_{kSource}_{ref_res}_sim{modName}.root"
    namePic  = f"{homeDir}/MC/mass_fit/Plot_M_res_{kSource}_{ref_res}{modName}.pdf"

    f = TFile(nameWksp,"READ")
    w = f.Get("w").Clone()
    f.Close()

    n_canv = len(name_list)
    canv = TCanvas('Masses_Fit','Masses Fit',1200,2000)
    # canv = TCanvas('Masses_Fit','Masses Fit',2000,1600)
    canv.DivideSquare(n_canv)
    # canv.DivideSquare(int(n_canv/2), 2)
    # canv.Divide(3, 3)
    i_canv = 0

    for name in name_list:
        i_canv+=1
        pad = canv.cd(i_canv)
        draw(w,pad,name)

    canv.SaveAs(namePic)

def fitData(kSource:str, name_list:list, kCB:bool=False):

    w = RooWorkspace('w',True)
    #Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M", 2850., 3850.)
    Jpsi_M = RooRealVar("Jpsi_M_res","Jpsi_M_res", 0.-m_win, 0.+m_win)
    Jpsi_M.setBins(2000,"cache")

    getattr(w,'import')(Jpsi_M)

    ref_res = name_list[0]
    for name in name_list:

        get_data_s(w, name, kSource)
        # kBW=False if (name=="jpsi" or name=="psi2S") else True
        #fill_workspace(w, name, ref=ref_res, bw=kBW, cb=kCB)
        fill_res_workspace(w, name, ref=ref_res, cb=kCB)

    combine(w,name_list)

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf")
    combData = w.data("combData")

    #model = w.pdf('model_{}'.format(kRange))
    #data = w.data('ds_{}'.format(kRange))

    r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
    r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.NumCPU(20))
    r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.NumCPU(20))
    r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.NumCPU(20))
    #r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.Minos(True))
    #r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Strategy(2) ,RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange"), RooFit.SplitRange(True))


    gROOT.ProcessLine('gStyle->SetOptStat(000000000)')
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')

    if kCB: modName="_CB"
    else:   modName=""

    nameWksp = f"{homeDir}/MC/mass_fit/Wksp_M_res_{kSource}_{ref_res}_sim{modName}.root"
    nameTxt  = f"{homeDir}/MC/mass_fit/Result_M_res_{kSource}_{ref_res}{modName}.txt"
    namePic  = f"{homeDir}/MC/mass_fit/Plot_M_res_{kSource}_{ref_res}{modName}.pdf"

    n_canv = len(name_list)
    canv = TCanvas('Masses_Fit','Masses Fit',1200,2000)
    canv.DivideSquare(n_canv)
    canv.DivideSquare(int(n_canv/2), 2)
    i_canv = 0

    for name in name_list:

        i_canv+=1
        pad = canv.cd(i_canv)
        #nameRoot = homeDir+'/MC/mass_fit/MC_M_res_{}_{}{}.root'.format(kSource,name,modName)

        draw(w,pad,name)


        #fFit = TFile (nameRoot,'RECREATE')
        #pad.Write('')
        #fFit.Write()
        #fFit.Close()

    canv.SaveAs(namePic)
    w.writeToFile(nameWksp)



    #r.correlationMatrix().Print('v')
    #r.globalCorr().Print('v')


    import os, sys
    save = os.dup( sys.stdout.fileno() )
    newout = open(nameTxt, 'w' )
    os.dup2( newout.fileno(), sys.stdout.fileno() )
    r.Print("v")
    r.correlationMatrix().Print()
    #print "chi2 eta_c %6.4f \n"%(chi2_etac)
    #print "chi2 _etac2s %6.4f \n"%(chi2_etac2s)
    os.dup2( save, sys.stdout.fileno() )
    newout.close()


if __name__ == "__main__":

    gROOT.LoadMacro(f"{gitDir}/libs/lhcbStyle.C")
    TGaxis.SetMaxDigits(3)

    #referent ccbar for resolution should be first
    # ccbar_list = ["jpsi","psi2S","etac","etac2S","chic0","chic1","chic2"]
    ccbar_list = ["jpsi","etac","psi2S","etac2S","chic0","chic1","chic2"]
    # ccbar_list = ["chic0","jpsi","etac","psi2S","etac2S","chic1","chic2"]
    kSource = "all"

    #for ccbar in ccbar_list:
        #fitData(kSource,ccbar,False)

    #ccbar_list = ["jpsi","psi2S"]
    kSources = ["all"]
    # kSources = ["prompt", "fromB"]
    # kSource = "all"

    # for kSource in kSources:
        # fitData(kSource,ccbar_list, True)
        # fitData(kSource,ccbar_list, False)
    drawSet(kSource,ccbar_list, True)
