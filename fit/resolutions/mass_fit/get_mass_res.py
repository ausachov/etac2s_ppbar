from ROOT import *
from ROOT.RooFit import *
from ROOT.RooStats import *

gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../libs/libBifurcatedCB/BifurcatedCB.cxx+")

homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

import sys
sys.path.insert(1, "../mass_fit/")

from drawModule import *
import charmConstLib as cl

#minM_MC_Low = 2870
#maxM_MC_Low = 3080
minM_MC_Low = -100
maxM_MC_Low = 100
binN_MC_Low = int((maxM_MC_Low-minM_MC_Low)/2.5)

#minM_MC_High = 3525
#maxM_MC_High = 3735
minM_MC_High = -100
maxM_MC_High = 100
binN_MC_High = int((maxM_MC_High-minM_MC_High)/2.5)

binN_MC_Tot     = int((maxM_MC_High-minM_MC_Low)/binWidthDraw)
binN_MC_Tot_Fit = int((maxM_MC_High-minM_MC_Low)/binWidth)

binning_MC = RooFit.Binning(binN_MC_Tot, minM_MC_Low, maxM_MC_High)
binning_MC_Jpsi = RooFit.Binning(binN_MC_Low, minM_MC_Low, maxM_MC_Low)
binning_MC_etac2s = RooFit.Binning(binN_MC_High, minM_MC_High, maxM_MC_High)

range_MC_tot = RooFit.Range(minM_MC_Low,maxM_MC_High)
range_MC_Jpsi = RooFit.Range(minM_MC_Low,maxM_MC_Low)
range_MC_etac2s = RooFit.Range(minM_MC_High,maxM_MC_High)

def getData_d(w, kSource):

    if kSource=="all": kSource="*"

    nt_Low = TChain("DecayTree")
    nt_High= TChain("DecayTree")

    nt_Low.Add(dataDir+"MC/Trigger/EtacDiProton_{}_Low_Mag*_AddBr.root".format(kSource))
    nt_High.Add(dataDir+"MC/Trigger/EtacDiProton_{}_High_Mag*_AddBr.root".format(kSource))

    tree_Low = TTree()
    tree_High = TTree()

    tree_Low  = nt_Low.CopyTree("")
    tree_High = nt_High.CopyTree("")


    Jpsi_M_res = RooRealVar("Jpsi_M_res","Jpsi_M_res", minM_MC_Low, maxM_MC_High)
    ds_Low = RooDataSet("ds_Low","ds_Low",tree_Low,RooArgSet(Jpsi_M_res))
    ds_High = RooDataSet("ds_High","ds_High",tree_High,RooArgSet(Jpsi_M_res))
    getattr(w,"import")(ds_Low)
    getattr(w,"import")(ds_High)

    print("DATA IS STORED")

def fillRelWorkspace(w, kCB=False):


    Jpsi_M_res = w.var("Jpsi_M_res")
    #Jpsi_M_res.setBins(1000,"cache")

    ratioNtoW = 0.30
    ratio_etac2etac2s = 0.9
    ratioArea = 0.90


    #ratioNtoW,ratioEtaTo_etac2s,ratioArea, gamma_etac, eff = getConstPars()



    r_etacToetac2s = RooRealVar("r_etacToetac2s","r_etacToetac2s", ratio_etac2etac2s, 0., 2.)
    r_NToW = RooRealVar("r_NToW","rNarToW",ratioNtoW, 0., 1.)
    r_G1ToG2 = RooRealVar("r_G1ToG2","rG1toG2",ratioArea, 0., 1.)
    r_NToW_etac2s = RooRealVar("r_NToW_etac2s","rNarToW",ratioNtoW, 0., 1.)
    r_G1ToG2_etac2s = RooRealVar("r_G1ToG2_etac2s","rG1toG2",ratioArea, 0., 1.)
    n_etac = RooRealVar("n_etac","num of _etac", 1e3, 10, 5.e5)
    n_etac2s = RooRealVar("n_etac2s","num of J/Psi", 2e3, 10, 5.e5)
    n_etacRel = RooRealVar("n_etacRel","num of _etac", 0.0, 3.0)

    n_etac_1 = RooFormulaVar("n_etac_1","num of _etac","@0*@1",RooArgList(n_etac,r_G1ToG2))
    n_etac_2 = RooFormulaVar("n_etac_2","num of _etac","@0-@1",RooArgList(n_etac,n_etac_1))
    n_etac2s_1 = RooFormulaVar("n_etac2s_1","num of J/Psi","@0*@1",RooArgList(n_etac2s,r_G1ToG2))
    n_etac2s_2 = RooFormulaVar("n_etac2s_2","num of J/Psi","@0-@1",RooArgList(n_etac2s,n_etac2s_1))


    mass_etac2s  = RooRealVar("mass_etac2s","mean of gaussian", cl.M_ETAC2S, cl.M_ETAC2S-50., cl.M_ETAC2S+50.)
    mass_etac    = RooRealVar("mass_etac","mean of gaussian", cl.M_ETAC, cl.M_ETAC-50., cl.M_ETAC+50.)
    gamma_etac   = RooRealVar("gamma_etac","width of Br-W", cl.GAMMA_ETAC, 10., 50. )
    gamma_etac2S = RooRealVar("gamma_etac2S","width of Br-W", cl.GAMMA_ETAC2S, 5., 30. ) #cl.GAMMA_ETAC2S = 11.3
    spin_etac = RooRealVar("spin_etac","spin_etac", 0. )
    radius_etac = RooRealVar("radius_etac","radius", 1.)
    proton_m = RooRealVar("proton_m","proton mass", cl.M_PROTON )

    mean_etac2s  = RooRealVar("mean_etac2s","mean of gaussian", 0., -10., 10.)
    mean_etac    = RooRealVar("mean_etac","mean of gaussian", 0., -10., 10.)

    gamma_etac.setConstant(True)
    #gamma_etac2S.setConstant(True)

    sigma_etac_1 = RooRealVar("sigma_etac_1","width of gaussian", 9., 0.1, 20.)
    sigma_etac_2 = RooFormulaVar("sigma_etac_2","width of gaussian","@0/@1",RooArgList(sigma_etac_1,r_NToW))

    sigma_etac2s_1 = RooFormulaVar("sigma_etac2s_1","width of gaussian","@0/@1",RooArgList(sigma_etac_1,r_etacToetac2s))
    #sigma_etac2s_1 = RooRealVar("sigma_etac2s_1","width of gaussian", 9., 0.1, 30.)
    #sigma_etac2s_1.setVal(6.5)
    #sigma_etac2s_1.setConstant(True)
    sigma_etac2s_2 = RooFormulaVar("sigma_etac2s_2","width of gaussian","@0/@1",RooArgList(sigma_etac2s_1,r_NToW))

    #Fit etac
    brw_etac = RooRelBreitWigner("brw_etac", "brw_etac",Jpsi_M_res, mass_etac, gamma_etac, spin_etac,radius_etac,proton_m,proton_m)
    brw_etac2S = RooRelBreitWigner("brw_etac2S", "brw_etac2S",Jpsi_M_res, mass_etac2s, gamma_etac2S, spin_etac,radius_etac,proton_m,proton_m)

    if kCB:
        alpha_1 = RooRealVar("alpha_1","alpha of CB", 1., 0.0, 10.)
        alpha_2 = RooRealVar("alpha_2","alpha of CB", 1., 0.0, 10.)
        nCB_1 = RooRealVar("nCB_1","n of CB", 1., 0.0, 100.)
        nCB_2 = RooRealVar("nCB_2","n of CB", 1., 0.0, 100.)

        #Fit etac

        cb_etac_1 = BifurcatedCB("cb_etac_1", "Cystal Ball Function", Jpsi_M_res, mean_etac, sigma_etac_1, alpha_1, nCB_1, alpha_1, nCB_1)
        bwxg_etac_1 = RooFFTConvPdf("bwxg_etac_1","breit-wigner (X) gauss", Jpsi_M_res, brw_etac, cb_etac_1)

        cb_etac2S_1 = BifurcatedCB("cb_etac2S_1", "Cystal Ball Function", Jpsi_M_res, mean_etac2s, sigma_etac2s_1, alpha_1, nCB_1, alpha_1, nCB_1)
        bwxg_etac2S_1 = RooFFTConvPdf("bwxg_etac2S_1","breit-wigner (X) gauss", Jpsi_M_res, brw_etac2S, cb_etac2S_1)

        #Fit J/psi
        #cb_Jpsi_1 = BifurcatedCB("cb_Jpsi_1", "Cystal Ball Function", Jpsi_M_res, RooFit.RooConst(0), sigma_etac_1, alpha_1, nCB_1, alpha_1, nCB_1)
        #gauss_1 = RooGaussian("gauss_1","gaussian PDF",Jpsi_M_res, mass_etac2s, sigma_etac2s_1)
        #gauss_2 = RooGaussian("gauss_2","gaussian PDF",Jpsi_M_res, mass_etac2s, sigma_etac2s_2)

        model_Low = RooAddPdf("model_Low","etac signal", RooArgList(cb_etac_1), RooArgList(n_etac))
        model_High = RooAddPdf("model_High","etac2s signal", RooArgList(cb_etac2S_1), RooArgList(n_etac2s))

    else:
        #Fit etac
        gauss_etac_1 = RooGaussian("gauss_etac_1","gauss_etac_1 PDF", Jpsi_M_res, mean_etac,  sigma_etac_1) #mass_etac -> mean_etac2s
        gauss_etac_2 = RooGaussian("gauss_etac_2","gauss_etac_2 PDF", Jpsi_M_res, mean_etac,  sigma_etac_2) #mass_etac -> mean_etac2s

        bwxg_etac_1 = RooFFTConvPdf("bwxg_etac_1","breit-wigner (X) gauss", Jpsi_M_res, brw_etac, gauss_etac_1)
        #bwxg_etac_2 = RooFFTConvPdf("bwxg_etac_2","breit-wigner (X) gauss", Jpsi_M_res, brw_etac, gauss_etac_2)


        gauss_etac2S_1 = RooGaussian("gauss_etac2S_1","gauss_etac_1 PDF", Jpsi_M_res, mean_etac2s,  sigma_etac2s_1) #mass_etac -> mean_etac2s
        gauss_etac2S_2 = RooGaussian("gauss_etac2S_2","gauss_etac_2 PDF", Jpsi_M_res, mean_etac2s,  sigma_etac2s_2) #mass_etac -> mean_etac2s

        bwxg_etac2S_1 = RooFFTConvPdf("bwxg_etac2S_1","breit-wigner (X) gauss", Jpsi_M_res, brw_etac2S, gauss_etac2S_1)
        #bwxg_etac2S_2 = RooFFTConvPdf("bwxg_etac2S_2","breit-wigner (X) gauss", Jpsi_M_res, brw_etac2S, gauss_etac2S_2)

        #Fit J/psi
        gauss_1 = RooGaussian("gauss_1","gaussian PDF",Jpsi_M_res, mass_etac2s, sigma_etac2s_1)
        #gauss_2 = RooGaussian("gauss_2","gaussian PDF",Jpsi_M_res, mass_etac2s, sigma_etac2s_2)

        model_Low = RooAddPdf("model_Low","etac signal", RooArgList(gauss_etac_1, gauss_etac_2), RooArgList(n_etac_1, n_etac_2))
        model_High = RooAddPdf("model_High","etac2s signal", RooArgList(gauss_etac2S_1,gauss_etac2S_2), RooArgList(n_etac2s_1,n_etac2s_2))


    ##Connection between parameters
    ##   RooFormulaVar f_1("f_1","f_1","@0*9.0",RooArgList(n_etac_2))

    ## Create constraints
    ##   RooGaussian constrNEta("constrNEta","constraint _etac",n_etac_1,f_1,RooConst(0.0))


    sample = RooCategory("sample","sample")
    sample.defineType("Low")
    sample.defineType("High")

    Jpsi_M_res.setRange("fitRange_Low", minM_MC_Low, maxM_MC_Low)
    Jpsi_M_res.setRange("fitRange_High", minM_MC_High, maxM_MC_High)

    data_Low = w.data("ds_Low")
    data_High = w.data("ds_High")


    # Construct combined dataset in (Jpsi_M_res,sample)
    combData = RooDataSet("combData", "combined data", RooArgSet(Jpsi_M_res), RooFit.Index(sample), RooFit.Import("Low",data_Low), RooFit.Import("High",data_High))


    # Associate model with the physics state and model_ctl with the control state
    simPdf = RooSimultaneous("simPdf","simultaneous signal pdf",sample)
    simPdf.addPdf(model_Low,"Low")
    simPdf.addPdf(model_High,"High")


    #getattr(w,"import")(model,RecycleConflictNodes())
    getattr(w,"import")(model_Low)
    getattr(w,"import")(model_High, RooFit.RecycleConflictNodes())
    getattr(w,"import")(combData, RooFit.RecycleConflictNodes())
    getattr(w,"import")(simPdf, RooFit.RecycleConflictNodes())


def set_frame(w, kRange):

    Jpsi_M_res = w.var("Jpsi_M_res")
    #data = w.data("ds_{}".format(kRange))
    model = w.pdf("model_{}".format(kRange))
    sample = w.cat("sample")
    simPdf = w.pdf("simPdf")
    combData = w.data("combData")

    title = ""
    if kRange=="Low":
        title = "#eta_c(1S)"
        binning = binning_MC_Jpsi
        range_draw = range_MC_Jpsi
    else:
        title = "#eta_c(2S)"
        binning = binning_MC_etac2s
        range_draw = range_MC_etac2s


    frame = Jpsi_M_res.frame(RooFit.Title("c #bar{c} to p #bar{p}"), RooFit.Range("fitRange_{}".format(kRange)))
    #data.plotOn(frame, binning, mrkSize, lineWidth1, name("data_{}".format(kRange))) #binning_MC_Jpsi
    combData.plotOn(frame, cut("sample==sample::{}".format(kRange)), binning, mrkSize, lineWidth1, name("data_{}".format(kRange)))

    model.paramOn(frame,RooFit.Layout(0.68,0.99,0.99))
    frame.getAttText().SetTextSize(0.027)
    simPdf.plotOn(frame, RooFit.Slice(sample,"{}".format(kRange)), range_draw, RooFit.ProjWData(RooArgSet(sample),combData,True), lineWidth1)
    #model.plotOn(frame, range_draw, lineWidth1) #range_MC_Jpsi ,
    #model_Low.plotOn(frame1, range_MC_Jpsi ,lineWidth1)
    #model_Low.plotOn(frame1,RooFit.Normalization(w.var("n_etac").getValV(),RooAbsReal.NumEvent))
    #model_Low.plotOn(frame1,RooFit.Components(RooArgSet(gauss_etac_1,gauss_etac_2)), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption("F"),RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2 = frame.chiSquare()
    print("chi2_{} = ".format(kRange), chi2)

    return frame, chi2

def draw_split(w):

    frame1, chi2_etac = set_frame(w,"Low")
    frame2, chi2_etac2s = set_frame(w,"High")

    texMC = TLatex();
    texMC.SetNDC();

    c = TCanvas("Masses_Fit","Masses Fit",600,800)
    c.Divide(1,2)
    c.cd(1).SetPad(.005, .505, .995, .995)
    gPad.SetLeftMargin(0.15),
    frame1.Draw()
    #frame1.SetMaximum(1.e3)
    #frame1.SetMinimum(0.1)
    frame1.GetXaxis().SetTitle("M_{p#bar{p}} / [MeV/c^2]"),
    frame1.GetXaxis().SetTitleSize(0.14)
    frame1.GetYaxis().SetTitleSize(0.14)
    #gPad.SetLogy()
    texMC.DrawLatex(0.7, 0.5, "#chi^{2}_{#eta_{c}} = %4.2f"%chi2_etac)
    c.cd(2).SetPad(.005, .005, .995, .495)
    gPad.SetLeftMargin(0.15),
    frame2.Draw()
    #frame2.SetMaximum(1.e3)
    #frame2.SetMinimum(0.1)
    frame2.GetXaxis().SetTitle("M_{p#bar{p}} / [MeV/c^2]"),
    frame2.GetXaxis().SetTitleSize(0.14)
    frame2.GetYaxis().SetTitleSize(0.14)
    #gPad.SetLogy()
    texMC.DrawLatex(0.7, 0.5, "#chi^{2}_{#eta_{c}(2S)} = %4.2f"%chi2_etac2s)

    return c

def fitData(kSource, kCB=False):

    w = RooWorkspace("w",True)

    getData_d(w, kSource)
    fillRelWorkspace(w, kCB)

    Jpsi_M_res = w.var("Jpsi_M_res")


    model_Low = w.pdf("model_Low")
    model_High = w.pdf("model_High")

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf")
    combData = w.data("combData")


    data_Low = w.data("ds_Low")
    data_High = w.data("ds_High")


    #w.var("gamma_etac").setConstant()
    #w.var("gamma_etac2S").setConstant()
    #sigma = w.var("sigma_eta_1")
    #sigma_etac2s = w.var("sigma_etac2s_1")
    #mean_etac2s = w.var("mean_etac2s")
    #mean_etac = w.var("mean_etac")
    #gamma_etac = w.var("gamma_etac")

    r = simPdf.fitTo(combData, RooFit.Save(True), RooFit.Range("fitRange"), RooFit.SplitRange(True))
    r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange"), RooFit.SplitRange(True))
    r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.Minos(True))

    gROOT.ProcessLine("gStyle->SetOptStat(000000000)")
    gROOT.ProcessLine("gStyle->SetOptTitle(0)")

    print(data_Low.numEntries())

    if kCB: modName="_CB"
    else:   modName=""

    nameTxt  = homeDir+"/MC/mass_fit/fit_M_res_{}{}.txt".format(kSource,modName)
    nameWksp = homeDir+"/MC/mass_fit/MC_M_res_{}{}_wksp.root".format(kSource,modName)
    nameRoot = homeDir+"/MC/mass_fit/MC_M_res_{}{}.root".format(kSource,modName)
    namePic  = homeDir+"/MC/mass_fit/MC_M_res_{}{}.pdf".format(kSource,modName)

    w.writeToFile(nameWksp)
    fFit = TFile (nameRoot,"RECREATE")

    c = draw_split(w)
    c.SaveAs(namePic)

    c.Write("")
    fFit.Write()
    fFit.Close()

    #r.correlationMatrix().Print("v")
    #r.globalCorr().Print("v")


    import os, sys
    save = os.dup( sys.stdout.fileno() )
    newout = file(nameTxt, "w" )
    os.dup2( newout.fileno(), sys.stdout.fileno() )
    r.Print("v")
    r.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() )
    newout.close()



#kSource = "fromB"
kSource = "all"
fitData(kSource, True)
#kSource = "prompt"
#fitData(kSource, True)
