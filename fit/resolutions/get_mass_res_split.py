from ROOT import *
from ROOT.RooFit import *
from ROOT.RooStats import *

gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../libs/libBifurcatedCB/BifurcatedCB.cxx+")

homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

import sys
sys.path.insert(1, "../mass_fit/")

from drawModule import *
import charmConstLib as cl

minM_MC_Low = 2850
maxM_MC_Low = 3120
binN_MC_Low = int((maxM_MC_Low-minM_MC_Low)/binWidthDraw)

minM_MC_High = 3505
maxM_MC_High = 3775
binN_MC_High = int((maxM_MC_High-minM_MC_High)/binWidthDraw)

binN_MC_Tot     = int((maxM_MC_High-minM_MC_Low)/binWidthDraw)
binN_MC_Tot_Fit = int((maxM_MC_High-minM_MC_Low)/binWidth)

binning_MC = RooFit.Binning(binN_MC_Tot, minM_MC_Low, maxM_MC_High)
binning_MC_Jpsi = RooFit.Binning(binN_MC_Low, minM_MC_Low, maxM_MC_Low)
binning_MC_etac2s = RooFit.Binning(binN_MC_High, minM_MC_High, maxM_MC_High)

range_MC_tot = RooFit.Range(minM_MC_Low,maxM_MC_High)
range_MC_Jpsi = RooFit.Range(minM_MC_Low,maxM_MC_Low)
range_MC_etac2s = RooFit.Range(minM_MC_High,maxM_MC_High)


def get_data_s(w, kRange, kSource):

    nt = TChain('DecayTree')

    nt.Add(dataDir+'MC/Trigger/Etac2sDiProton_{}_{}*.root'.format(kSource,kRange))

    tree = TTree()
    tree = nt.CopyTree("")

    if kRange=='Low':
        Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M", minM_MC_Low, maxM_MC_Low)
    else:
        Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M", minM_MC_High, maxM_MC_High)

    ds = RooDataSet('ds_{}'.format(kRange),'ds_{}'.format(kRange),tree,RooArgSet(Jpsi_M))
    getattr(w,'import')(ds)

    print("DATA IS STORED")

names = {
    "Low":  ["etac", cl.M_ETAC, cl.GAMMA_ETAC],
    "High": ["etac2s", cl.M_ETAC2S, cl.GAMMA_ETAC2S]
}

def fillRelWorkspaceSplit(w, kRange):

    Jpsi_M = w.var('Jpsi_M')
    ccbar_name  = names[kRange][0]
    ccbar_mass  = names[kRange][1]
    ccbar_gamma = names[kRange][2]

    ratioNtoW = 0.30
    ratio_etac2etac2s = 1.1
    ratioArea = 0.90


    #ratioNtoW,ratioEtaTo_etac2s,ratioArea, gamma_etac, eff = getConstPars()

    r_etacToetac2s = RooRealVar('r_etacToetac2s','r_etacToetac2s', ratio_etac2etac2s, 0., 3.)
    r_NToW = RooRealVar('r_NToW_{}'.format(kRange),'rNarToW',ratioNtoW, 0., 1.)
    r_G1ToG2 = RooRealVar('r_G1ToG2_{}'.format(kRange),'rG1toG2',ratioArea, 0., 1.)
    n_ccbar = RooRealVar('n_{}'.format(ccbar_name),'num of etac', 1e3, 10, 5.e5)

    n_ccbar_1 = RooFormulaVar('n_{}_1'.format(ccbar_name),'num of _etac','@0*@1',RooArgList(n_ccbar,r_G1ToG2))
    n_ccbar_2 = RooFormulaVar('n_{}_2'.format(ccbar_name),'num of _etac','@0-@1',RooArgList(n_ccbar,n_ccbar_1))


    mass  = RooRealVar('mass_{}'.format(ccbar_name),'mean of gaussian', ccbar_mass, ccbar_mass-50., ccbar_mass+50.)
    gamma = RooRealVar('gamma_{}'.format(ccbar_name),'width of Br-W', ccbar_gamma, 10., 50. )
    gamma.setConstant(True)

    spin_etac = RooRealVar('spin_etac','spin_eta', 0. )
    radius_etac = RooRealVar('radius_etac','radius', 1.)
    proton_m = RooRealVar('proton_m','proton mass', cl.M_PROTON )



    sigma_1 = RooRealVar('sigma_{}_1'.format(ccbar_name),'width of gaussian', 9., 0.1, 30.)
    sigma_2 = RooFormulaVar('sigma_{}_2'.format(ccbar_name),'width of gaussian','@0/@1',RooArgList(sigma_1,r_NToW))

    #Fit eta
    brw = RooRelBreitWigner('brw_{}'.format(ccbar_name), 'brw_etac',Jpsi_M, mass, gamma, spin_etac,radius_etac,proton_m,proton_m)

    gauss_1 = RooGaussian('gauss_{}_1'.format(ccbar_name),'gauss_etac_1 PDF', Jpsi_M, RooFit.RooConst(0),  sigma_1) #mass_etac -> mean_etac2s
    gauss_2 = RooGaussian('gauss_{}_2'.format(ccbar_name),'gauss_etac_2 PDF', Jpsi_M, RooFit.RooConst(0),  sigma_2) #mass_etac -> mean_etac2s

    bwxg_1 = RooFFTConvPdf('bwxg_{}_1'.format(ccbar_name),'breit-wigner (X) gauss', Jpsi_M, brw, gauss_1)
    bwxg_2 = RooFFTConvPdf('bwxg_{}_2'.format(ccbar_name),'breit-wigner (X) gauss', Jpsi_M, brw, gauss_2)


    ##Fit J/psi
    #gauss_1 = RooGaussian('gauss_1','gaussian PDF',Jpsi_M, mass_etac2s, sigma_etac2s_1)
    #gauss_2 = RooGaussian('gauss_2','gaussian PDF',Jpsi_M, mass_etac2s, sigma_etac2s_2)
    #if kRange == 'Low':
        #model = RooAddPdf('model_{}'.format(kRange),'{} signal'.format(ccbar_name), RooArgList(bwxg_1), RooArgList(n_ccbar))
    #else:
        #model = RooAddPdf('model_{}'.format(kRange),'{} signal'.format(ccbar_name), RooArgList(bwxg_1, bwxg_2), RooArgList(n_ccbar_1, n_ccbar_2))
    model = RooAddPdf('model_{}'.format(kRange),'{} signal'.format(ccbar_name), RooArgList(bwxg_1, bwxg_2), RooArgList(n_ccbar_1, n_ccbar_2))
    #model = RooAddPdf('model_{}'.format(kRange),'{} signal'.format(ccbar_name), RooArgList(gauss_1, gauss_2), RooArgList(n_ccbar_1, n_ccbar_2))

    data = w.data('ds_{}'.format(kRange))

    getattr(w,'import')(model)

def set_frame(w, kRange):

    Jpsi_M = w.var('Jpsi_M'.format(kRange))
    data = w.data('ds_{}'.format(kRange))
    model = w.pdf('model_{}'.format(kRange))

    title = ''
    if kRange=='Low':
        title = '#eta_c(1S)'
        binning = binning_MC_Jpsi
        range_draw = range_MC_Jpsi
    else:
        title = '#eta_c(2S)'
        binning = binning_MC_etac2s
        range_draw = range_MC_etac2s


    frame = Jpsi_M.frame(RooFit.Title('c #bar{c} to p #bar{p}')) #, RooFit.Range("fitRange_{}".format(kRange))
    data.plotOn(frame, binning, mrkSize, lineWidth1, name("data_{}".format(kRange))) #binning_MC_Jpsi

    model.paramOn(frame,RooFit.Layout(0.68,0.99,0.99))
    frame.getAttText().SetTextSize(0.027)
    model.plotOn(frame, range_draw, lineWidth1) #range_MC_Jpsi ,
    chi2 = frame.chiSquare()
    print('chi2_{} = '.format(kRange), chi2)

    return frame, chi2


def draw_split(w1,w2):

    frame1, chi2_etac = set_frame(w1,'Low')
    frame2, chi2_etac2s = set_frame(w2,'High')

    texMC = TLatex();
    texMC.SetNDC();

    c = TCanvas('Masses_Fit','Masses Fit',600,800)
    c.Divide(1,2)
    c.cd(1).SetPad(.005, .505, .995, .995)
    #gPad.SetLeftMargin(0.15),
    frame1.Draw()
    #frame1.SetMaximum(1.e3)
    #frame1.SetMinimum(0.1)
    frame1.GetXaxis().SetTitle('M_{p#bar{p}} / [MeV/c^2]'),
    frame1.GetXaxis().SetTitleSize(0.14)
    frame1.GetYaxis().SetTitleSize(0.14)
    #gPad.SetLogy()
    texMC.DrawLatex(0.7, 0.5, "#chi^{2}_{#eta_{c}} = %4.2f"%chi2_etac)
    c.cd(2).SetPad(.005, .005, .995, .495)
    #gPad.SetLeftMargin(0.15),
    frame2.Draw()
    #frame2.SetMaximum(1.e3)
    #frame2.SetMinimum(0.1)
    frame2.GetXaxis().SetTitle('M_{p#bar{p}} / [MeV/c^2]'),
    frame2.GetXaxis().SetTitleSize(0.14)
    frame2.GetYaxis().SetTitleSize(0.14)
    #gPad.SetLogy()
    texMC.DrawLatex(0.7, 0.5, "#chi^{2}_{#eta_{c}(2S)} = %4.2f"%chi2_etac2s)

    return c

def fitData(kSource, gauss=True):

    w_Low = RooWorkspace('w_Low',True)
    w_High = RooWorkspace('w_High',True)

    get_data_s(w_Low, 'Low', kSource)
    get_data_s(w_High, 'High', kSource)
    fillRelWorkspaceSplit(w_Low,"Low")
    fillRelWorkspaceSplit(w_High,"High")

    # In case if I'm going to try CB as resolution
    #if gauss:
        #add=""
        #fillRelWorkspace(w)
    #else:
        #add="_CB"
        #fillRelWorkspaceCB(w)

    #Jpsi_M_Low = w_Low.var('Jpsi_M')
    #Jpsi_M_High = w_High.var('Jpsi_M')


    model_Low = w_Low.pdf('model_Low')
    model_High = w_High.pdf('model_High')

    data_Low = w_Low.data('ds_Low')
    data_High = w_High.data('ds_High')


    w_Low.var("gamma_etac").setConstant()
    w_High.var("gamma_etac2s").setConstant()

    #if (iTz != 0):

        #f = TFile(homeDir+"Results/MC/MassFit/MC_M_resolution%s_2016_wksp.root"%(add),"READ")
        #wMC = f.Get("w")
        #f.Close()
        #if gauss:
            #w.var('rNarToW').setVal(wMC.var('rNarToW').getValV())
            ##     w.var('rEtaTo_etac2s').setVal(wMC.var('rEtaTo_etac2s').getValV())
            #w.var('rG1toG2').setVal(wMC.var('rG1toG2').getValV())
            #w.var('mean_etac2s').setVal(wMC.var('mean_etac2s').getValV())

            #w.var('rNarToW').setConstant(True)
            ##     w.var('rEtaTo_etac2s').setConstant(True)
            #w.var('rG1toG2').setConstant(True)
            ##w.var('mean_etac2s').setConstant(True)
        #else:
            #w.var('rEtaTo_etac2s').setVal(wMC.var('rEtaTo_etac2s').getValV())
            #w.var('alpha_eta_1').setVal(wMC.var('alpha_eta_1').getValV())
            #w.var('n_eta_1').setVal(wMC.var('n_eta_1').getValV())
            #w.var('mean_etac2s').setVal(wMC.var('mean_etac2s').getValV())

            #w.var('rEtaTo_etac2s').setConstant(True)
            #w.var('alpha_eta_1').setConstant(True)
            #w.var('n_eta_1').setConstant(True)
            ##w.var('mean_etac2s').setConstant(True)

    r1 = model_Low.fitTo(data_Low, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
    r1 = model_Low.fitTo(data_Low, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Save(True))
    #w.var("r_NToW").setConstant(True)
    #w.var("r_NToW_High").setVal(w.var("r_NToW_Low").getValV())
    #w.var("r_G1ToG2_High").setVal(w.var("r_G1ToG2_Low").getValV())
    #w.var("r_G1ToG2").setConstant(True)
    model_High.Print()
    data_High.Print()
    r2 = model_High.fitTo(data_High, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
    r2 = model_High.fitTo(data_High, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))


    gROOT.ProcessLine('gStyle->SetOptStat(000000000)')
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')


    nameTxt  = homeDir+'/MC/mass_fit/fit_M_res_{}_split_2Gauss.txt'.format(kSource)
    nameWksp = homeDir+'/MC/mass_fit/MC_M_res_{}_wksp_split_2Gauss.root'.format(kSource)
    nameRoot = homeDir+'/MC/mass_fit/MC_M_res_{}_split_2Gauss.root'.format(kSource)
    namePic  = homeDir+'/MC/mass_fit/MC_M_res_{}_split_2Gauss.pdf'.format(kSource)

    w_Low.writeToFile(nameWksp)
    w_High.writeToFile(nameWksp, False)

    c = draw_split(w_Low,w_High)
    c.SaveAs(namePic)

    fFit = TFile (nameRoot,'RECREATE')
    c.Write('')
    fFit.Write()
    fFit.Close()

    #r.correlationMatrix().Print('v')
    #r.globalCorr().Print('v')


    import os, sys
    save = os.dup( sys.stdout.fileno() )
    newout = file(nameTxt, 'w' )
    os.dup2( newout.fileno(), sys.stdout.fileno() )
    r1.Print("v")
    r1.correlationMatrix().Print()
    r2.Print("v")
    r2.correlationMatrix().Print()
    #print "chi2 eta_c %6.4f \n"%(chi2_etac)
    #print "chi2 _etac2s %6.4f \n"%(chi2_etac2s)
    os.dup2( save, sys.stdout.fileno() )
    newout.close()



#kSource = "fromB"
#fitData(kSource)
kSource = "prompt"
fitData(kSource)
