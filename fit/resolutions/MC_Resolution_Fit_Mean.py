from ROOT import *

gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+");
gROOT.LoadMacro("../libs/libBifurcatedCB/BifurcatedCB.cxx+");
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"
#homeDir = "/users/LHCb/zhovkovska/scripts/"

def getData_d(w, iTz=0):

    nPTBins = 5
    #pt = (['6500', '8000'], ['8000', '10000'], ['10000', '12000'], ['12000', '14000'], ['14000', '18000'])
    pt = [6500, 8000, 10000, 12000, 14000]
    #tz = [-10., -0.15, -0.025, 0.,        0.200,   2., 4., 10.]
    #tz = [-10., -0.125, -0.025, 0.,        0.200,   2., 4., 10.]
    #tz = [-10.0, -0.125, -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10]
    #nTzBins = len(tz)-1
    #nTzBins = 6
    #tz = [-10.0, -0.15, -0.05, 0.,        0.100,   4., 10.]
    #pt = (['6500', '7000'], ['7000', '8000'], ['8000', '9000'], ['9000', '10000'] ,['10000', '11000'], ['11000', '12000'],['12000', '13000'], ['13000', '14000'],['14000', '16000'],['16000', '18000'])

    nt_etac = TChain('DecayTree')
    nt_etac2s= TChain('DecayTree')

    nt_etac.Add(homeDir+'MC/Trigger/Etac2sDiProton_Low_2018.root')
    nt_etac2s.Add(homeDir+'MC/Trigger/Etac2sDiProton_High_2018.root')

    tree_etac2s = TTree()
    tree_etac = TTree()


    if(iTz == 0):

        #cut_etac2sTzL = '(3.3)*(_etac2sERTEX_Z-_etac2s_OWNPV_Z)*_etac2s_MM/_etac2s_PZ > %f'%(tz[iTz-1])
        #cut_etac2sTzR = '(3.3)*(_etac2sERTEX_Z-_etac2s_OWNPV_Z)*_etac2s_MM/_etac2s_PZ < %f'%(tz[iTz])

        tree_etac = nt_etac.CopyTree("prompt")
        tree_etac_FromB = nt_etac_FromB.CopyTree("sec")
        list0 = TList()
        list0.Add(tree_etac)
        list0.Add(tree_etac_FromB)

        tree_etac = TTree.MergeTrees(list0)
        tree_etac2s = nt_etac2s.CopyTree("")

    elif ( iTz <= nTzBins ):
    #if (iTz != 0):
        print iTz
        cut_etac2sTzL = '_etac2s_Tz > %f'%(tz[iTz-1])
        cut_etac2sTzR = '_etac2s_Tz < %f'%(tz[iTz])
        #cut_etac2sTzL = '(3.3)*(_etac2sERTEX_Z-_etac2s_OWNPV_Z)*_etac2s_MM/_etac2s_PZ > %f'%(tz[iTz-1])
        cut_etac2sPTL = '_etac2s_PT > %f'%(6500)
        cut_etac2sPTR = '_etac2s_PT < %f'%(14000)
        #cut_etac2sTzR = '(3.3)*(_etac2sERTEX_Z-_etac2s_OWNPV_Z)*_etac2s_MM/_etac2s_PZ < %f'%(tz[iTz])

        tree_etac = nt_etac.CopyTree("prompt" + '&&' + cut_etac2sTzL + '&&' + cut_etac2sTzR + '&&' + cut_etac2sPTL + '&&' + cut_etac2sPTR)
        tree_etac_FromB = nt_etac_FromB.CopyTree("sec" + '&&' + cut_etac2sTzL + '&&' + cut_etac2sTzR + '&&' + cut_etac2sPTL + '&&' + cut_etac2sPTR)
        list0 = TList()
        list0.Add(tree_etac)
        list0.Add(tree_etac_FromB)

        tree_etac = TTree.MergeTrees(list0)
        #tree_etac = nt_etac.CopyTree( "prompt &&" + cut_etac2sTzL + '&&' + cut_etac2sTzR)
        tree_etac2s = nt_etac2s.CopyTree(cut_etac2sTzL + '&&' + cut_etac2sTzR + '&&' + cut_etac2sPTL + '&&' + cut_etac2sPTR)

    else:
        print ('Incorrect number of Tz bin %s'%(iTz))


    _etac2s_M_res = RooRealVar ('_etac2s_M_res','_etac2s_M_res',-100.,100.0)
    ds_etac = RooDataSet('ds_etac','ds_etac',tree_etac,RooArgSet(_etac2s_M_res))
    ds_etac2s = RooDataSet('ds_etac2s','ds_etac2s',tree_etac2s,RooArgSet(_etac2s_M_res))
    getattr(w,'import')(ds_etac)
    getattr(w,'import')(ds_etac2s)

    #ds_etac.Draw('')
    #ds_etac2s.Draw('')


def getConstPars():

    f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution_2016_wksp.root","READ")
    wMC = f.Get("w")
    f.Close()

    rNtoW = wMC.var("rNarToW").getValV()
    rEtaTo_etac2s = wMC.var("rEtaTo_etac2s").getValV()
    rArea = wMC.var("rG1toG2").getValV()
    gamma = 31.8
    effic = 0.035
    #gamma = 21.3
    #effic = 0.06
    #ratioNtoW = 0.5
    #ratioEtaTo_etac2s = 0.88
    #ratioArea = 0.9


    return rNtoW,rEtaTo_etac2s,rArea, gamma, effic


def fillRelWorkspace(w):


    _etac2s_M_res = w.var('_etac2s_M_res')
    #_etac2s_M_res.setBins(1000,'cache')

    ratioNtoW = 0.50
    ratioEtaTo_etac2s = 0.88
    ratioArea = 0.70
    #gamma_etac = 31.8
    gamma_etac = 29.7

    ratioNtoW,ratioEtaTo_etac2s,ratioArea, gamma_etac, eff = getConstPars()



    rEtaTo_etac2s = RooRealVar('rEtaTo_etac2s','rEtaTo_etac2s', ratioEtaTo_etac2s)
    rNarToW = RooRealVar('rNarToW','rNarToW',ratioNtoW)
    rG1toG2 = RooRealVar('rG1toG2','rG1toG2',ratioArea)
    n_etac = RooRealVar('n_etac','num of _etac', 1e3, 10, 1.e4)
    n_etac2s = RooRealVar('n_etac2s','num of J/Psi', 2e3, 10, 1.e5)
    n_etacRel = RooRealVar('n_etacRel','num of _etac', 0.0, 3.0)

    n_etac_1 = RooFormulaVar('n_etac_1','num of _etac','@0*@1',RooArgList(n_etac,rG1toG2))
    n_etac_2 = RooFormulaVar('n_etac_2','num of _etac','@0-@1',RooArgList(n_etac,n_etac_1))
    n_etac2s_1 = RooFormulaVar('n_etac2s_1','num of J/Psi','@0*@1',RooArgList(n_etac2s,rG1toG2))
    n_etac2s_2 = RooFormulaVar('n_etac2s_2','num of J/Psi','@0-@1',RooArgList(n_etac2s,n_etac2s_1))


    mean__etac2s = RooRealVar('mean__etac2s','mean of gaussian', 0.0, -50.0, 50.0)
    mean__etac = RooRealVar('mean__etac','mean of gaussian', 0.0, -50.0, 50.0)
    gamma_eta = RooRealVar('gamma_eta','width of Br-W', gamma_etac, 10., 50. )
    spin_eta = RooRealVar('spin_eta','spin_eta', 0. )
    radius_eta = RooRealVar('radius_eta','radius', 1.)
    proton_m = RooRealVar('proton_m','proton mass', 938.3 )

    sigma_eta_1 = RooRealVar('sigma_eta_1','width of gaussian', 9., 0.1, 50.)
    sigma_eta_2 = RooFormulaVar('sigma_eta_2','width of gaussian','@0/@1',RooArgList(sigma_eta_1,rNarToW))

    sigma__etac2s_1 = RooFormulaVar('sigma__etac2s_1','width of gaussian','@0/@1',RooArgList(sigma_eta_1,rEtaTo_etac2s))
    #sigma__etac2s_1 = RooRealVar('sigma__etac2s_1','width of gaussian', 9., 0.1, 50.)
    #sigma__etac2s_1.setVal(6.5)
    #sigma__etac2s_1.setConstant(True)
    sigma__etac2s_2 = RooFormulaVar('sigma__etac2s_2','width of gaussian','@0/@1',RooArgList(sigma__etac2s_1,rNarToW))

    #Fit eta
    br_wigner = RooRelBreitWigner('br_wigner', 'br_wigner',_etac2s_M_res, mean__etac, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)

    #gaussEta_1 = RooGaussian('gaussEta_1','gaussEta_1 PDF', _etac2s_M_res, mean__etac,  sigma_eta_1)
    #gaussEta_2 = RooGaussian('gaussEta_2','gaussEta_2 PDF', _etac2s_M_res, mean__etac,  sigma_eta_2)
    gaussEta_1 = RooGaussian('gaussEta_1','gaussEta_1 PDF', _etac2s_M_res, mean__etac2s,  sigma_eta_1) #mean__etac -> mean__etac2s
    gaussEta_2 = RooGaussian('gaussEta_2','gaussEta_2 PDF', _etac2s_M_res, mean__etac2s,  sigma_eta_2) #mean__etac -> mean__etac2s

    bwxg_1 = RooFFTConvPdf('bwxg_1','breit-wigner (X) gauss', _etac2s_M_res, br_wigner, gaussEta_1)
    bwxg_2 = RooFFTConvPdf('bwxg_2','breit-wigner (X) gauss', _etac2s_M_res, br_wigner, gaussEta_2)

    #Fit J/psi
    gauss_1 = RooGaussian('gauss_1','gaussian PDF',_etac2s_M_res, mean__etac2s, sigma__etac2s_1)
    gauss_2 = RooGaussian('gauss_2','gaussian PDF',_etac2s_M_res, mean__etac2s, sigma__etac2s_2)


    ##Connection between parameters
    ##   RooFormulaVar f_1('f_1','f_1','@0*9.0',RooArgList(n_etac_2))

    ## Create constraints
    ##   RooGaussian constrNEta('constrNEta','constraint _etac',n_etac_1,f_1,RooConst(0.0))


    ##   RooAddPdf model('model','signal', RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2), RooArgList(n_etac_1,n_etac_2,n_etac2s_1,n_etac2s_2))
    ##   RooAddPdf model_etac('model_etac','_etac signal', RooArgList(bwxg_1, bwxg_2), RooArgList(n_etac_1, n_etac_2))

    model_etac = RooAddPdf('model_etac','_etac signal', RooArgList(gaussEta_1, gaussEta_2), RooArgList(n_etac_1, n_etac_2))
    model_etac2s = RooAddPdf('model_etac2s','_etac2s signal', RooArgList(gauss_1, gauss_2), RooArgList(n_etac2s_1, n_etac2s_2))

    sample = RooCategory('sample','sample')
    sample.defineType('_etac')
    sample.defineType('_etac2s')


    data_etac = w.data('ds_etac')
    data_etac2s = w.data('ds_etac2s')


    # Construct combined dataset in (_etac2s_M_res,sample)
    combData = RooDataSet('combData', 'combined data', RooArgSet(_etac2s_M_res), RooFit.Index(sample), RooFit.Import('_etac',data_etac), RooFit.Import('_etac2s',data_etac2s))


    # Associate model with the physics state and model_ctl with the control state
    simPdf = RooSimultaneous('simPdf','simultaneous signal pdf',sample)
    simPdf.addPdf(model_etac,'_etac')
    simPdf.addPdf(model_etac2s,'_etac2s')


    #   getattr(w,'import')(model,RecycleConflictNodes())
    getattr(w,'import')(model_etac)
    getattr(w,'import')(model_etac2s, RooFit.RecycleConflictNodes())
    getattr(w,'import')(combData, RooFit.RecycleConflictNodes())
    getattr(w,'import')(simPdf, RooFit.RecycleConflictNodes())


def fillRelWorkspaceCB(w):


    _etac2s_M_res = w.var('_etac2s_M_res')
    #_etac2s_M_res.setBins(1000,'cache')

    ratioNtoW = 0.50
    ratioEtaTo_etac2s = 0.88
    ratioArea = 0.70
    #gamma_etac = 31.8
    gamma_etac = 29.7



    rEtaTo_etac2s = RooRealVar('rEtaTo_etac2s','rEtaTo_etac2s', ratioEtaTo_etac2s, 0.01, 5.0)
    rNarToW = RooRealVar('rNarToW','rNarToW',ratioNtoW, 0.01, 1.0)
    rG1toG2 = RooRealVar('rG1toG2','rG1toG2',ratioArea, 0.01, 1.0)
    #rEtaTo_etac2s = RooRealVar('rEtaTo_etac2s','rEtaTo_etac2s', ratioEtaTo_etac2s, 0.01, 5.0)
    #rNarToW = RooRealVar('rNarToW','rNarToW', 1.0)
    #rG1toG2 = RooRealVar('rG1toG2','rG1toG2', 1.0)
    n_etac = RooRealVar('n_etac','num of _etac Prompt', 1e3, 10, 5.e4)
    n_etac2s = RooRealVar('n_etac2s','num of J/Psi Prompt', 2e3, 10, 5.e4)
    n_etacRel = RooRealVar('n_etacRel','num of _etac Prompt', 0.0, 3.0)

    #  n_etac_1 = RooFormulaVar('n_etac_1','num of _etac','@0*@1*@2',RooArgSet(n_etacRel,n_etac2s,rG1toG2))
    #  n_etac_2 = RooFormulaVar('n_etac_2','num of _etac','@0*@1-@2',RooArgSet(n_etacRel,n_etac2s,n_etac_1))
    n_etac_1 = RooFormulaVar('n_etac_1','num of _etac','@0*@1',RooArgList(n_etac,rG1toG2))
    n_etac_2 = RooFormulaVar('n_etac_2','num of _etac','@0-@1',RooArgList(n_etac,n_etac_1))
    n_etac2s_1 = RooFormulaVar('n_etac2s_1','num of J/Psi','@0*@1',RooArgList(n_etac2s,rG1toG2))
    n_etac2s_2 = RooFormulaVar('n_etac2s_2','num of J/Psi','@0-@1',RooArgList(n_etac2s,n_etac2s_1))


    mean__etac2s = RooRealVar('mean__etac2s','mean of gaussian', 0.0, -50.0, 50.0)
    mean__etac = RooRealVar('mean__etac','mean of gaussian', 0.0, -50.0, 50.0)
    gamma_eta = RooRealVar('gamma_eta','width of Br-W', gamma_etac, 10., 50. )
    spin_eta = RooRealVar('spin_eta','spin_eta', 0. )
    radius_eta = RooRealVar('radius_eta','radius', 1.)
    proton_m = RooRealVar('proton_m','proton mass', 938.3 )


    alpha_eta_1 = RooRealVar('alpha_eta_1','alpha of CB', 1., 0.0, 10.)
    alpha_eta_2 = RooRealVar('alpha_eta_2','alpha of CB', 1., 0.0, 10.)
    n_eta_1 = RooRealVar('n_eta_1','n of CB', 1., 0.0, 100.)
    n_eta_2 = RooRealVar('n_eta_2','n of CB', 1., 0.0, 100.)

    sigma_eta_1 = RooRealVar('sigma_eta_1','width of gaussian', 9., 0.1, 50.)
    sigma_eta_2 = RooFormulaVar('sigma_eta_2','width of gaussian','@0/@1',RooArgList(sigma_eta_1,rNarToW))

    sigma__etac2s_1 = RooFormulaVar('sigma__etac2s_1','width of gaussian','@0/@1',RooArgList(sigma_eta_1,rEtaTo_etac2s))
    sigma__etac2s_2 = RooFormulaVar('sigma__etac2s_2','width of gaussian','@0/@1',RooArgList(sigma__etac2s_1,rNarToW))

    #Prompt
    #Fit eta
    br_wigner = RooRelBreitWigner('br_wigner', 'br_wigner',_etac2s_M_res, mean__etac, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)

    cb_etac_1 = BifurcatedCB("cb_etac_1", "Cystal Ball Function", _etac2s_M_res, mean__etac2s, sigma_eta_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)
    #cb_etac_2 = BifurcatedCB("cb_etac_2", "Cystal Ball Function", _etac2s_M_res, mean__etac, sigma_eta_1, alpha_eta_2, n_eta_2);

    bwxg_1 = RooFFTConvPdf('bwxg_1','breit-wigner (X) gauss', _etac2s_M_res, br_wigner, cb_etac_1)
    #bwxg_2 = RooFFTConvPdf('bwxg_2','breit-wigner (X) gauss', _etac2s_M_res, br_wigner, cb_etac_2)

    #Fit J/psi
    cb__etac2s_1 = BifurcatedCB("cb__etac2s_1", "Cystal Ball Function", _etac2s_M_res, mean__etac2s, sigma__etac2s_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)
    #cb__etac2s_2 = BifurcatedCB("cb__etac2s_2", "Cystal Ball Function", _etac2s_M_res, mean__etac2s, sigma__etac2s_1, alpha_eta_2, n_eta_2);


    ##Connection between parameters
    ##   RooFormulaVar f_1('f_1','f_1','@0*9.0',RooArgList(n_etac_2))

    ## Create constraints
    ##   RooGaussian constrNEta('constrNEta','constraint _etac',n_etac_1,f_1,RooConst(0.0))


    ##   RooAddPdf model('model','signal', RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2), RooArgList(n_etac_1,n_etac_2,n_etac2s_1,n_etac2s_2))
    ##   RooAddPdf model_etac('model_etac','_etac signal', RooArgList(bwxg_1, bwxg_2), RooArgList(n_etac_1, n_etac_2))

    model_etac = RooAddPdf('model_etac','_etac signal', RooArgList(cb_etac_1), RooArgList(n_etac))
    model_etac2s = RooAddPdf('model_etac2s','_etac2s signal', RooArgList(cb__etac2s_1), RooArgList(n_etac2s))
    #model_etac = RooAddPdf('model_etac','_etac signal', RooArgList(cb_etac_1,cb_etac_2), RooArgList(n_etac_1,n_etac_2))
    #model_etac2s = RooAddPdf('model_etac2s','_etac2s signal', RooArgList(cb__etac2s_1,cb__etac2s_2), RooArgList(n_etac2s_1,n_etac2s_2))


    sample = RooCategory('sample','sample')
    sample.defineType('_etac')
    sample.defineType('_etac2s')


    data_etac = w.data('ds_etac')
    data_etac2s = w.data('ds_etac2s')


    # Construct combined dataset in (_etac2s_M_res,sample)
    combData = RooDataSet('combData', 'combined data', RooArgSet(_etac2s_M_res), RooFit.Index(sample), RooFit.Import('_etac',data_etac), RooFit.Import('_etac2s',data_etac2s))


    # Associate model with the physics state and model_ctl with the control state
    simPdf = RooSimultaneous('simPdf','simultaneous signal pdf',sample)
    simPdf.addPdf(model_etac,'_etac')
    simPdf.addPdf(model_etac2s,'_etac2s')


    #   getattr(w,'import')(model,RecycleConflictNodes())
    getattr(w,'import')(combData)
    getattr(w,'import')(simPdf)


def fitData(iTz, gauss=True):


    gROOT.Reset()


    w = RooWorkspace('w',True)

    getData_d(w, iTz)

    if gauss:
        add=""
        fillRelWorkspace(w)
    else:
        add="_CB"
        fillRelWorkspaceCB(w)

    _etac2s_M_res = w.var('_etac2s_M_res')


    model_etac = w.pdf('model_etac')
    model_etac2s = w.pdf('model_etac2s')

    sample = w.cat('sample')
    simPdf = w.pdf('simPdf')
    combData = w.data('combData')


    data_etac = w.data('ds_etac')
    data_etac2s = w.data('ds_etac2s')


    #sigma = w.var('sigma_eta_1')
    #sigma_etac2s = w.var('sigma__etac2s_1')
    #mean__etac2s = w.var('mean__etac2s')
    #mean__etac = w.var('mean__etac')
    #gamma_eta = w.var('gamma_eta')


    if (iTz != 0):

        f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution%s_2016_wksp.root"%(add),"READ")
        wMC = f.Get("w")
        f.Close()
        if gauss:
            w.var('rNarToW').setVal(wMC.var('rNarToW').getValV())
            #     w.var('rEtaTo_etac2s').setVal(wMC.var('rEtaTo_etac2s').getValV())
            w.var('rG1toG2').setVal(wMC.var('rG1toG2').getValV())
            w.var('mean__etac2s').setVal(wMC.var('mean__etac2s').getValV())

            w.var('rNarToW').setConstant(True)
            #     w.var('rEtaTo_etac2s').setConstant(True)
            w.var('rG1toG2').setConstant(True)
            #w.var('mean__etac2s').setConstant(True)
        else:
            w.var('rEtaTo_etac2s').setVal(wMC.var('rEtaTo_etac2s').getValV())
            w.var('alpha_eta_1').setVal(wMC.var('alpha_eta_1').getValV())
            w.var('n_eta_1').setVal(wMC.var('n_eta_1').getValV())
            w.var('mean__etac2s').setVal(wMC.var('mean__etac2s').getValV())

            w.var('rEtaTo_etac2s').setConstant(True)
            w.var('alpha_eta_1').setConstant(True)
            w.var('n_eta_1').setConstant(True)
            #w.var('mean__etac2s').setConstant(True)


    r = simPdf.fitTo(combData,RooFit.Save(True))
    r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Minos(True), RooFit.Save(True))
    r = simPdf.fitTo(combData, RooFit.Minos(True), RooFit.Save(True))

    #model_etac.fitTo(data_etac, RooFit.Minos(True), RooFit.Save(True))
    #r = model_etac2s.fitTo(data_etac2s, RooFit.Extended(True), RooFit.Minos(True), RooFit.Save(True))
    #r = model_etac2s.fitTo(data_etac2s, RooFit.Minos(True), RooFit.Save(True))

    gROOT.ProcessLine('gStyle->SetOptStat(000000000)')
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')

    frame1 = _etac2s_M_res.frame(RooFit.Title('#eta_c to p #bar{p} from_b'))
    frame2 = _etac2s_M_res.frame(RooFit.Title('J/#psi to p #bar{p} from_b'))

    data_etac.plotOn(frame1)
    data_etac2s.plotOn(frame2)



    #chi2_etac = 0.
    #chi2_etac2s = 0.
    #chi2_etac = 0.
    #chi2_etac2s = 0.
    gaussEta_1 = w.pdf('gaussEta_1')
    gaussEta_2 = w.pdf('gaussEta_2')
    gauss_1 = w.pdf('gauss_1')
    gauss_2 = w.pdf('gauss_2')


    #model_etac.paramOn(frame1,RooFit.Layout(0.68,0.99,0.99))
    #frame1.getAttText().SetTextSize(0.027)
    model_etac.plotOn(frame1,RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2_etac = frame1.chiSquare()
    #model_etac.plotOn(frame1,RooFit.Components(RooArgSet(gaussEta_1,gaussEta_2)), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption('F'),RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print 'chi2_etac = ', chi2_etac

    #model_etac2s.paramOn(frame2,RooFit.Layout(0.68,0.99,0.99))
    #frame2.getAttText().SetTextSize(0.027)
    model_etac2s.plotOn(frame2,RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2_etac2s = frame2.chiSquare()
    #model_etac2s.plotOn(frame2, RooFit.Components(RooArgSet(gauss_1,gauss_2)), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption('F'), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print 'chi2_etac2s = ', chi2_etac2s

    print   data_etac.numEntries()

    texMC = TLatex();
    texMC.SetNDC();

    c = TCanvas('Masses_Fit','Masses Fit',600,800)
    c.Divide(1,2)
    c.cd(1).SetPad(.005, .505, .995, .995)
    #gPad.SetLeftMargin(0.15),  frame1.GetXaxis().SetTitle('(M - TRUEM)_{p #bar{p}} / [MeV/c^2]'),
    frame1.Draw()
    #frame1.SetMaximum(1.e3)
    frame1.SetMinimum(0.1)
    frame1.GetXaxis().SetTitleSize(0.14)
    frame1.GetXaxis().SetTitleSize(0.14)
    #gPad.SetLogy()
    texMC.DrawLatex(0.7, 0.8, "#chi^{2}_{#eta_{c}} = %4.2f"%chi2_etac)
    #frame.addObject(tex_etac2sPiMC1);
    c.cd(2).SetPad(.005, .005, .995, .495)
    #gPad.SetLeftMargin(0.15),  frame2.GetXaxis().SetTitle('(M - TRUEM)_{p #bar{p}} / [MeV/c^2]'),
    frame2.Draw()
    #frame2.SetMaximum(1.e3)
    frame2.SetMinimum(0.1)
    frame2.GetXaxis().SetTitleSize(0.14)
    frame2.GetXaxis().SetTitleSize(0.14)
    #gPad.SetLogy()
    texMC.DrawLatex(0.7, 0.8, "#chi^{2}_{J/#psi} = %4.2f"%chi2_etac2s)

    nameTxt = ''
    nameRoot = ''
    nameWksp = ''
    namePic = ''

    if(iTz == 0):

        nameTxt = homeDir+'Results/MC/MassFit/fit_MassRes%s.txt'%(add)
        nameWksp = homeDir+'Results/MC/MassFit/MC_MassResolution%s_2016_wksp.root'%(add)
        nameRoot = homeDir+'Results/MC/MassFit/MC_MassResolution%s_Fit_plot.root'%(add)
        namePic = homeDir+'Results/MC/MassFit/MC_MassResolution%s_empty.pdf'%(add)

    else:

        nameTxt = homeDir+'Results/MC/MassFit/Tz11Bins/fit_MassRes%s_shiftFix_Tz%s.txt'%(add,iTz)
        nameWksp = homeDir+'Results/MC/MassFit/Tz11Bins/MC_MassResolution%s_shiftFix_2016_wksp_Tz%s.root'%(add,iTz)
        nameRoot = homeDir+'Results/MC/MassFit/Tz11Bins/MC_MassResolution%s_shiftFix_Fit_plot_Tz%s.root'%(add,iTz)
        namePic = homeDir+'Results/MC/MassFit/Tz11Bins/MC_MassResolution%s_shiftFix_Tz%s.pdf'%(add,iTz)



    import os, sys
    save = os.dup( sys.stdout.fileno() )
    newout = file(nameTxt, 'w' )
    os.dup2( newout.fileno(), sys.stdout.fileno() )
    r.Print("v")
    print "chi2 eta_c %6.4f \n"%(chi2_etac)
    print "chi2 _etac2s %6.4f \n"%(chi2_etac2s)
    r.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() )
    newout.close()

    w.writeToFile(nameWksp)
    fFit = TFile (nameRoot,'RECREATE')


    c.Write('')
    fFit.Write()
    fFit.Close()

    c.SaveAs(namePic)
    r.correlationMatrix().Print('v')
    r.globalCorr().Print('v')



def MC_Resolution_Fit():

    #fitData(0)
    nTzBins = 11
    for iTz in range(nTzBins):
        fitData(iTz+1)


MC_Resolution_Fit()
#fitData(1)
#fitData(2)
