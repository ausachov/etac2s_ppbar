from ROOT import *
from ROOT.RooFit import *
from ROOT.RooStats import *

gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../libs/libBifurcatedCB/BifurcatedCB.cxx+")

homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

import sys
sys.path.insert(1, "../mass_fit/")

from drawModule import *
import charmConstLib as cl

minM_MC_Low = 2860
maxM_MC_Low = 3110
binN_MC_Low = int((maxM_MC_Low-minM_MC_Low)/2.5)

minM_MC_High = 3515
maxM_MC_High = 3765
binN_MC_High = int((maxM_MC_High-minM_MC_High)/2.5)

binN_MC_Tot     = int((maxM_MC_High-minM_MC_Low)/binWidthDraw)
binN_MC_Tot_Fit = int((maxM_MC_High-minM_MC_Low)/binWidth)

binning_MC = RooFit.Binning(binN_MC_Tot, minM_MC_Low, maxM_MC_High)
binning_MC_Jpsi = RooFit.Binning(binN_MC_Low, minM_MC_Low, maxM_MC_Low)
binning_MC_etac2s = RooFit.Binning(binN_MC_High, minM_MC_High, maxM_MC_High)

range_MC_tot = RooFit.Range(minM_MC_Low,maxM_MC_High)
range_MC_Jpsi = RooFit.Range(minM_MC_Low,maxM_MC_Low)
range_MC_etac2s = RooFit.Range(minM_MC_High,maxM_MC_High)

def getData_d(w, kSource):

    nt_Low = TChain('DecayTree')
    nt_High= TChain('DecayTree')

    nt_Low.Add(dataDir+'MC/Trigger/Etac2sDiProton_{}_Low*.root'.format(kSource))
    nt_High.Add(dataDir+'MC/Trigger/Etac2sDiProton_{}_High*.root'.format(kSource))

    tree_Low = TTree()
    tree_High = TTree()

    tree_Low  = nt_Low.CopyTree("")
    tree_High = nt_High.CopyTree("")


    Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M", minM_MC_Low, maxM_MC_High)
    ds_Low = RooDataSet('ds_Low','ds_Low',tree_Low,RooArgSet(Jpsi_M))
    ds_High = RooDataSet('ds_High','ds_High',tree_High,RooArgSet(Jpsi_M))
    getattr(w,'import')(ds_Low)
    getattr(w,'import')(ds_High)

    print("DATA IS STORED")

def fillRelWorkspace(w):


    Jpsi_M = w.var('Jpsi_M')
    #Jpsi_M.setBins(1000,'cache')

    ratioNtoW = 0.30
    ratio_etac2etac2s = 1.1
    ratioArea = 0.90


    #ratioNtoW,ratioEtaTo_etac2s,ratioArea, gamma_etac, eff = getConstPars()



    r_etacToetac2s = RooRealVar('r_etacToetac2s','r_etacToetac2s', ratio_etac2etac2s, 0., 3.)
    r_NToW = RooRealVar('r_NToW','rNarToW',ratioNtoW, 0., 1.)
    r_G1ToG2 = RooRealVar('r_G1ToG2','rG1toG2',ratioArea, 0., 1.)
    r_NToW_etac2s = RooRealVar('r_NToW_etac2s','rNarToW',ratioNtoW, 0., 1.)
    r_G1ToG2_etac2s = RooRealVar('r_G1ToG2_etac2s','rG1toG2',ratioArea, 0., 1.)
    n_etac = RooRealVar('n_etac','num of _etac', 1e3, 10, 5.e5)
    n_etac2s = RooRealVar('n_etac2s','num of J/Psi', 2e3, 10, 5.e5)
    n_etacRel = RooRealVar('n_etacRel','num of _etac', 0.0, 3.0)

    n_etac_1 = RooFormulaVar('n_etac_1','num of _etac','@0*@1',RooArgList(n_etac,r_G1ToG2))
    n_etac_2 = RooFormulaVar('n_etac_2','num of _etac','@0-@1',RooArgList(n_etac,n_etac_1))
    n_etac2s_1 = RooFormulaVar('n_etac2s_1','num of J/Psi','@0*@1',RooArgList(n_etac2s,r_G1ToG2))
    n_etac2s_2 = RooFormulaVar('n_etac2s_2','num of J/Psi','@0-@1',RooArgList(n_etac2s,n_etac2s_1))


    mass_etac2s  = RooRealVar('mass_etac2s','mean of gaussian', cl.M_ETAC2S, cl.M_ETAC2S-50., cl.M_ETAC2S+50.)
    mass_etac    = RooRealVar('mass_etac','mean of gaussian', cl.M_ETAC, cl.M_ETAC-50., cl.M_ETAC+50.)
    gamma_etac   = RooRealVar('gamma_etac','width of Br-W', cl.GAMMA_ETAC, 10., 50. )
    gamma_etac2S = RooRealVar('gamma_etac2S','width of Br-W', cl.GAMMA_ETAC2S, 5., 30. )
    spin_eta = RooRealVar('spin_eta','spin_eta', 0. )
    radius_eta = RooRealVar('radius_eta','radius', 1.)
    proton_m = RooRealVar('proton_m','proton mass', cl.M_PROTON )


    gamma_etac.setConstant(True)
    #gamma_etac2S.setConstant(True)

    sigma_etac_1 = RooRealVar('sigma_etac_1','width of gaussian', 9., 0.1, 30.)
    sigma_etac_2 = RooFormulaVar('sigma_etac_2','width of gaussian','@0/@1',RooArgList(sigma_etac_1,r_NToW))

    #sigma_etac2s_1 = RooFormulaVar('sigma_etac2s_1','width of gaussian','@0/@1',RooArgList(sigma_etac_1,r_etacToetac2s))
    sigma_etac2s_1 = RooRealVar('sigma_etac2s_1','width of gaussian', 9., 0.1, 30.)
    #sigma_etac2s_1.setVal(6.5)
    #sigma_etac2s_1.setConstant(True)
    sigma_etac2s_2 = RooFormulaVar('sigma_etac2s_2','width of gaussian','@0/@1',RooArgList(sigma_etac2s_1,r_NToW))

    #Fit eta
    brw_etac = RooRelBreitWigner('brw_etac', 'brw_etac',Jpsi_M, mass_etac, gamma_etac, spin_eta,radius_eta,proton_m,proton_m)

    gauss_etac_1 = RooGaussian('gaussEta_1','gaussEta_1 PDF', Jpsi_M, RooFit.RooConst(0),  sigma_etac_1) #mass_etac -> mean_etac2s
    gauss_etac_2 = RooGaussian('gaussEta_2','gaussEta_2 PDF', Jpsi_M, RooFit.RooConst(0),  sigma_etac_2) #mass_etac -> mean_etac2s

    bwxg_etac_1 = RooFFTConvPdf('bwxg_etac_1','breit-wigner (X) gauss', Jpsi_M, brw_etac, gauss_etac_1)
    bwxg_etac_2 = RooFFTConvPdf('bwxg_etac_2','breit-wigner (X) gauss', Jpsi_M, brw_etac, gauss_etac_2)


    #Fit eta
    brw_etac2S = RooRelBreitWigner('brw_etac2S', 'brw_etac2S',Jpsi_M, mass_etac2s, gamma_etac2S, spin_eta,radius_eta,proton_m,proton_m)

    gauss_etac2S_1 = RooGaussian('gaussEta_1','gaussEta_1 PDF', Jpsi_M, RooFit.RooConst(0),  sigma_etac2s_1) #mass_etac -> mean_etac2s
    gauss_etac2S_2 = RooGaussian('gaussEta_2','gaussEta_2 PDF', Jpsi_M, RooFit.RooConst(0),  sigma_etac2s_2) #mass_etac -> mean_etac2s

    bwxg_etac2S_1 = RooFFTConvPdf('bwxg_etac2S_1','breit-wigner (X) gauss', Jpsi_M, brw_etac2S, gauss_etac2S_1)
    bwxg_etac2S_2 = RooFFTConvPdf('bwxg_etac2S_2','breit-wigner (X) gauss', Jpsi_M, brw_etac2S, gauss_etac2S_2)

    #Fit J/psi
    gauss_1 = RooGaussian('gauss_1','gaussian PDF',Jpsi_M, mass_etac2s, sigma_etac2s_1)
    gauss_2 = RooGaussian('gauss_2','gaussian PDF',Jpsi_M, mass_etac2s, sigma_etac2s_2)


    ##Connection between parameters
    ##   RooFormulaVar f_1('f_1','f_1','@0*9.0',RooArgList(n_etac_2))

    ## Create constraints
    ##   RooGaussian constrNEta('constrNEta','constraint _etac',n_etac_1,f_1,RooConst(0.0))


    #model_Low = RooAddPdf('model_Low','_etac signal', RooArgList(bwxg_etac_1), RooArgList(n_etac))
    #model_High = RooAddPdf('model_High','_etac2s signal', RooArgList(bwxg_etac2S_1), RooArgList(n_etac2s))
    model_Low = RooAddPdf('model_Low','_etac signal', RooArgList(bwxg_etac_1, bwxg_etac_2), RooArgList(n_etac_1, n_etac_2))
    model_High = RooAddPdf('model_High','_etac2s signal', RooArgList(bwxg_etac2S_1, bwxg_etac2S_2), RooArgList(n_etac2s_1, n_etac2s_2))

    #model_Low = RooAddPdf('model_Low','_etac signal', RooArgList(gauss_etac_1, gauss_etac_2), RooArgList(n_etac_1, n_etac_2))
    #model_High = RooAddPdf('model_High','_etac2s signal', RooArgList(gauss_1, gauss_2), RooArgList(n_etac2s_1, n_etac2s_2))

    sample = RooCategory('sample','sample')
    sample.defineType('Low')
    sample.defineType('High')

    Jpsi_M.setRange("fitRange_Low", minM_MC_Low, maxM_MC_Low)
    Jpsi_M.setRange("fitRange_High", minM_MC_High, maxM_MC_High)

    data_Low = w.data('ds_Low')
    data_High = w.data('ds_High')


    # Construct combined dataset in (Jpsi_M,sample)
    combData = RooDataSet('combData', 'combined data', RooArgSet(Jpsi_M), RooFit.Index(sample), RooFit.Import('Low',data_Low), RooFit.Import('High',data_High))


    # Associate model with the physics state and model_ctl with the control state
    simPdf = RooSimultaneous('simPdf','simultaneous signal pdf',sample)
    simPdf.addPdf(model_Low,'Low')
    simPdf.addPdf(model_High,'High')


    #getattr(w,'import')(model,RecycleConflictNodes())
    getattr(w,'import')(model_Low)
    getattr(w,'import')(model_High, RooFit.RecycleConflictNodes())
    getattr(w,'import')(combData, RooFit.RecycleConflictNodes())
    getattr(w,'import')(simPdf, RooFit.RecycleConflictNodes())



def fitData(kSource, gauss=True):

    w = RooWorkspace('w',True)

    getData_d(w, kSource)
    fillRelWorkspace(w)

    # In case if I'm going to try CB as resolution
    #if gauss:
        #add=""
        #fillRelWorkspace(w)
    #else:
        #add="_CB"
        #fillRelWorkspaceCB(w)

    Jpsi_M = w.var('Jpsi_M')


    model_Low = w.pdf('model_Low')
    model_High = w.pdf('model_High')

    sample = w.cat('sample')
    simPdf = w.pdf('simPdf')
    combData = w.data('combData')


    data_Low = w.data('ds_Low')
    data_High = w.data('ds_High')


    w.var("gamma_etac").setConstant()
    #w.var("gamma_etac2S").setConstant()
    #sigma = w.var('sigma_eta_1')
    #sigma_etac2s = w.var('sigma_etac2s_1')
    #mean_etac2s = w.var('mean_etac2s')
    #mean_etac = w.var('mean_etac')
    #gamma_etac = w.var('gamma_etac')


    #if (iTz != 0):

        #f = TFile(homeDir+"Results/MC/MassFit/MC_M_resolution%s_2016_wksp.root"%(add),"READ")
        #wMC = f.Get("w")
        #f.Close()
        #if gauss:
            #w.var('rNarToW').setVal(wMC.var('rNarToW').getValV())
            ##     w.var('rEtaTo_etac2s').setVal(wMC.var('rEtaTo_etac2s').getValV())
            #w.var('rG1toG2').setVal(wMC.var('rG1toG2').getValV())
            #w.var('mean_etac2s').setVal(wMC.var('mean_etac2s').getValV())

            #w.var('rNarToW').setConstant(True)
            ##     w.var('rEtaTo_etac2s').setConstant(True)
            #w.var('rG1toG2').setConstant(True)
            ##w.var('mean_etac2s').setConstant(True)
        #else:
            #w.var('rEtaTo_etac2s').setVal(wMC.var('rEtaTo_etac2s').getValV())
            #w.var('alpha_eta_1').setVal(wMC.var('alpha_eta_1').getValV())
            #w.var('n_eta_1').setVal(wMC.var('n_eta_1').getValV())
            #w.var('mean_etac2s').setVal(wMC.var('mean_etac2s').getValV())

            #w.var('rEtaTo_etac2s').setConstant(True)
            #w.var('alpha_eta_1').setConstant(True)
            #w.var('n_eta_1').setConstant(True)
            ##w.var('mean_etac2s').setConstant(True)

    #r1 = model_Low.fitTo(data_Low, RooFit.Save(True), RooFit.Range("fitRange"), RooFit.SplitRange(True))
    #r1 = model_Low.fitTo(data_Low, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange"), RooFit.SplitRange(True))
    ##w.var("r_NToW").setConstant(True)
    #w.var("r_NToW_etac2s").setVal(w.var("r_NToW").getValV())
    #w.var("r_G1ToG2_etac2s").setVal(w.var("r_G1ToG2").getValV())
    ##w.var("r_G1ToG2").setConstant(True)
    #r2 = model_High.fitTo(data_High, RooFit.Save(True), RooFit.Range("fitRange"), RooFit.SplitRange(True))
    #r2 = model_High.fitTo(data_High, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange"), RooFit.SplitRange(True))

    r = simPdf.fitTo(combData, RooFit.Save(True), RooFit.Range("fitRange"), RooFit.SplitRange(True))
    r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange"), RooFit.SplitRange(True))
    #r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange"), RooFit.SplitRange(True))

    #model_Low.fitTo(data_Low, RooFit.Minos(True), RooFit.Save(True))
    #r = model_High.fitTo(data_High, RooFit.Extended(True), RooFit.Minos(True), RooFit.Save(True))
    #r = model_High.fitTo(data_High, RooFit.Minos(True), RooFit.Save(True))

    gROOT.ProcessLine('gStyle->SetOptStat(000000000)')
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')

    frame1 = Jpsi_M.frame(RooFit.Title('#eta_c(1S) to p #bar{p}'), RooFit.Range("fitRange_Low"))
    frame2 = Jpsi_M.frame(RooFit.Title('#eta_c(2S) to p #bar{p}'), RooFit.Range("fitRange_High"))

    #data_Low.plotOn(frame1, binning_MC_Jpsi, mrkSize, lineWidth1, name("data_Low"))
    #data_High.plotOn(frame2, binning_MC_etac2s, mrkSize, lineWidth1, name("data_High"))


    #chi2_etac = 0.
    #chi2_etac2s = 0.
    #chi2_etac = 0.
    #chi2_etac2s = 0.


    model_Low.paramOn(frame1,RooFit.Layout(0.68,0.99,0.99))
    frame1.getAttText().SetTextSize(0.027)
    combData.plotOn(frame1, cut("sample==sample::{}".format("Low")), binning_MC_Jpsi, mrkSize, lineWidth1, name("data_Low"))
    simPdf.plotOn(frame1, RooFit.Slice(sample,"{}".format("Low")), range_MC_Jpsi ,RooFit.ProjWData(RooArgSet(sample),combData,True), lineWidth1)
    #model_Low.plotOn(frame1, range_MC_Jpsi ,lineWidth1)
    #model_Low.plotOn(frame1,RooFit.Normalization(w.var("n_etac").getValV(),RooAbsReal.NumEvent))
    #model_Low.plotOn(frame1,RooFit.Components(RooArgSet(gaussEta_1,gaussEta_2)), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption('F'),RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2_etac = frame1.chiSquare()
    print 'chi2_etac = ', chi2_etac

    model_High.paramOn(frame2,RooFit.Layout(0.68,0.99,0.99))
    frame2.getAttText().SetTextSize(0.027)
    combData.plotOn(frame2, cut("sample==sample::{}".format("High")), binning_MC_etac2s, mrkSize, lineWidth1, name("data_High"))
    simPdf.plotOn(frame2, RooFit.Slice(sample,"{}".format("High")), range_MC_etac2s ,RooFit.ProjWData(RooArgSet(sample),combData,True), lineWidth1)
    #model_High.plotOn(frame2, range_MC_etac2s ,lineWidth1)
    chi2_etac2s = frame2.chiSquare()
    #model_High.plotOn(frame2,RooFit.Normalization(w.var("n_etac2s").getValV(),RooAbsReal.NumEvent))
    #model_High.plotOn(frame2, RooFit.Components(RooArgSet(gauss_1,gauss_2)), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption('F'), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print 'chi2_etac2s = ', chi2_etac2s

    print(data_Low.numEntries())

    texMC = TLatex();
    texMC.SetNDC();

    c = TCanvas('Masses_Fit','Masses Fit',600,800)
    c.Divide(1,2)
    c.cd(1).SetPad(.005, .505, .995, .995)
    #gPad.SetLeftMargin(0.15),  frame1.GetXaxis().SetTitle('(M - TRUEM)_{p #bar{p}} / [MeV/c^2]'),
    frame1.Draw()
    #frame1.SetMaximum(1.e3)
    frame1.SetMinimum(0.1)
    frame1.GetXaxis().SetTitleSize(0.14)
    frame1.GetXaxis().SetTitleSize(0.14)
    #gPad.SetLogy()
    texMC.DrawLatex(0.7, 0.5, "#chi^{2}_{#eta_{c}} = %4.2f"%chi2_etac)
    #frame.addObject(tex_etac2sPiMC1);
    c.cd(2).SetPad(.005, .005, .995, .495)
    #gPad.SetLeftMargin(0.15),  frame2.GetXaxis().SetTitle('(M - TRUEM)_{p #bar{p}} / [MeV/c^2]'),
    frame2.Draw()
    #frame2.SetMaximum(1.e3)
    frame2.SetMinimum(0.1)
    frame2.GetXaxis().SetTitleSize(0.14)
    frame2.GetXaxis().SetTitleSize(0.14)
    #gPad.SetLogy()
    texMC.DrawLatex(0.7, 0.5, "#chi^{2}_{#eta_{c}(2S)} = %4.2f"%chi2_etac2s)

    nameTxt  = homeDir+'/MC/mass_fit/fit_M_res_{}_gamma_free.txt'.format(kSource)
    nameWksp = homeDir+'/MC/mass_fit/MC_M_res_{}_wksp_gamma_free.root'.format(kSource)
    nameRoot = homeDir+'/MC/mass_fit/MC_M_res_{}_gamma_free.root'.format(kSource)
    namePic  = homeDir+'/MC/mass_fit/MC_M_res_{}_gamma_free.pdf'.format(kSource)

    w.writeToFile(nameWksp)
    fFit = TFile (nameRoot,'RECREATE')


    c.Write('')
    fFit.Write()
    fFit.Close()

    c.SaveAs(namePic)
    #r.correlationMatrix().Print('v')
    #r.globalCorr().Print('v')


    import os, sys
    save = os.dup( sys.stdout.fileno() )
    newout = file(nameTxt, 'w' )
    os.dup2( newout.fileno(), sys.stdout.fileno() )
    r.Print("v")
    r.correlationMatrix().Print()
    #r1.Print("v")
    #r1.correlationMatrix().Print()
    #r2.Print("v")
    #r2.correlationMatrix().Print()
    print "chi2 eta_c %6.4f \n"%(chi2_etac)
    print "chi2 _etac2s %6.4f \n"%(chi2_etac2s)
    os.dup2( save, sys.stdout.fileno() )
    newout.close()



kSource = "fromB"
fitData(kSource)
kSource = "prompt"
fitData(kSource)
