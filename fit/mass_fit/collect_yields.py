from ROOT import *
from ROOT.RooFit import *
from ROOT.RooStats import *

from model_sim import *
import drawModule_splitFit as dm
import oniaConstLib as cl
gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")

import numpy as np

homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

all_states = {
            "jpsi" :    ["\\jpsi(1S)", "J/#psi(1S)"],
            "etac" :    ["\\etac(1S)", "#eta_{c}(1S)"],
            "chic0":    ["\\chiczero(1P)", "#chi_{c0}(1P)"],
            "chic1":    ["\\chicone(1P)", "#chi_{c1}(1P)"],
            "chic2":    ["\\chictwo(1P)", "#chi_{c2}(1P)"],
            "hc"   :    ["\\hc(1P)","#h_{c}(1P)"], 
            # "etac2S":   ["\\etactwos","#eta_{c}(2S)"], 
            "psi2S":    ["\\psitwos", "#psi(2S)"]
        }


def print_table(syst_list: list, values: dict, syst: dict):

    kNorm = "jpsi"
    kSource = "sim"
    txtfilename = "{}/mass_fit/one_hist/etac2S/Syst_{}_{}_ProbNNp_06_check.txt".format(homeDir,kNorm,kSource)
    with open(txtfilename, "w") as f:
        for key in values:
            f.write("{:30s}: {:15.2f} \n".format(key, values[key])) 
            for key_s in syst[key]:
                f.write("{:30s}: {:15.2f} \n".format(key_s, syst[key][key_s])) 
            f.write("\n")

def systematics(kNorm: str, syst_list: list):

    kNorm = "jpsi"
    kSource = "sim"
    nameWksp1 = "{}/mass_fit/one_hist/etac2S/Wksp_{}_{}_{}_ProbNNp_06_check.root".format(homeDir,kNorm,kSource,"Base")
    file_w1 = TFile(nameWksp1)
    w1 = file_w1.Get("w").Clone()
    file_w1.Close()

    syst = {}
    values = {}

    var_list_1 = w1.allVars()

    # get mean and stat for the main fit
    nuis_iter = var_list_1.createIterator()
    var = nuis_iter.Next()
    while var:
        if not var.isConstant():
            values[var.GetName()] = var.getVal()
            syst[var.GetName()] = { "Base" : var.getError()/var.getVal() }
        var = nuis_iter.Next()

    for kSyst in syst_list:
        nameWksp2 = "{}/mass_fit/one_hist/etac2S/Wksp_{}_{}_{}_ProbNNp_06_check.root".format(homeDir,kNorm,kSource,kSyst)
        file_w2 = TFile(nameWksp2)
        w2 = file_w2.Get("w").Clone()
        file_w2.Close()
        var_list_2 = w2.allVars()

        nuis_iter = var_list_2.createIterator()
        var = nuis_iter.Next()
        while var:
            if var_list_1.contains(var) and not var.isConstant():
                name_var = var.GetName()
                syst[name_var][kSyst] = (var.getValV()-values[name_var])/values[name_var]
            var = nuis_iter.Next()

    print_table(syst_list, values, syst)

def compare(kNorm,kSyst1,kSyst2):

    kSource = "sim"
    nameWksp1 = homeDir + "/mass_fit/one_hist/etac2S/Wksp_{}_{}_{}_ProbNNp_06_check.root".format(kNorm,kSource,kSyst1)
    file_w1 = TFile(nameWksp1)
    w1 = file_w1.Get("w")

    nameWksp2 = homeDir + "/mass_fit/one_hist/etac2S/Wksp_{}_{}_{}_ProbNNp_06_check.root".format(kNorm,kSource,kSyst2)
    file_w2 = TFile(nameWksp2)
    w2 = file_w2.Get("w")


    var_list_1 = w1.allVars()
    var_list_2 = w2.allVars()

    nuis_iter = var_list_1.createIterator()
    var = nuis_iter.Next()
    while var:
        if var_list_2.contains(var) and not var.isConstant():
            name_var = var.GetName()
            val1 = var.getValV()
            err1 = var.getError()
            val2 = var_list_2.find(name_var).getValV()
            if val1>1e-6:
                print("{:30s}: {:15.2f}, {:15.2f}, {:15.2}".format(name_var, val1, err1/val1, (val2-val1)/val1))
        var = nuis_iter.Next()
    print("\n")

    file_w1.Close()
    file_w2.Close()

def yields(kNorm,kSyst):

    kSource = "sim"
    sources = ["prompt", "fromB"]
    # all_states = ["jpsi", "etac", "chic0","chic1","chic2","hc", "psi2S", "etac2S"]

    nameWksp = homeDir + "/mass_fit/one_hist/Wksp_{}_{}_{}_ProbNNp_06_check.root".format(kNorm,kSource, kSyst)
    file_w = TFile(nameWksp)
    w = file_w.Get("w")
    nameTxt = "{}/mass_fit/one_hist/yield_table.txt".format(homeDir)
    with open(nameTxt, "w") as f:
        f.write(" Resonance ")
        for source in sources:
            f.write("& \t {} ".format(source))
        f.write(" \\\\ \\hline \n ")
        for ccbar in all_states:
            f.write("${}$ \t".format(all_states[ccbar][0]))
            for source in sources:
                print("n_{}_{}".format(ccbar, source))
                ccbar_val = w.var("n_{}_{}".format(ccbar, source)).getValV()
                ccbar_err = w.var("n_{}_{}".format(ccbar, source)).getError()
                f.write("& ${:.1f} \\pm {:.1f}$ \t".format(ccbar_val, ccbar_err))
            f.write("\\\\ \n")

    file_w.Close()

if __name__ == "__main__":

    # yields("jpsi","Base")
    systematics("jpsi",["CB","Exp","Sqr","Sqrt"])
    # compare("jpsi","Base","Exp")
    # compare("jpsi","Base","Sqr")
    # compare("jpsi","Base","Sqrt")