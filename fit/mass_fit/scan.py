# from ROOT import *
from ROOT import TLine
from ROOT import TFile
from ROOT import TGraph
from ROOT import TMath
from ROOT import TCanvas
from ROOT import TLatex
lTGraph = list[TGraph]
pair = [float, float]

homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"

def addBlurb(can, isdata=True, year = 0):

   
  myLatex = TLatex(0.5,0.5,"")
  myLatex.SetTextFont(132) 
  myLatex.SetTextColor(1) 
  myLatex.SetTextSize(0.06) 
  myLatex.SetNDC(True) 
  myLatex.SetTextAlign(11)  
  myLatex.SetTextSize(0.055) 
  if (isdata):
    if (year ==0):
      myLatex.DrawLatex(0.63, 0.77,"#splitline{LHCb Preliminary}{#scale[1]{#sqrt{s} = 7+8 TeV, L =3 fb^{#kern[0.7]{-1}}}}")
    elif (year == 2011):
      myLatex.DrawLatex(0.63, 0.77,"#splitline{LHCb Preliminary}{#scale[1]{#sqrt{s} = 7 TeV, L =1 fb^{#kern[0.7]{-1}}}}")
    elif (year == 2012):
      myLatex.DrawLatex(0.63, 0.77,"#splitline{LHCb Preliminary}{#scale[1]{#sqrt{s} = 8 TeV, L =2 fb^{#kern[0.7]{-1}}}}")
  else:
    myLatex.DrawLatex(0.67, 0.95,"LHCb Simulation")



def plotGraph(graph: TGraph,  yTitle="Likelihood", marker = 1, color = 2, style = 2 ):

  graph.SetMarkerStyle(marker)
  graph.SetMarkerColor(color)
  graph.SetLineColor(color)
  graph.SetLineStyle(style)
  graph.SetLineWidth(1)
  #  graph.SetMinimum(0.0)
  graph.SetTitle("")
  graph.GetXaxis().SetTitleFont(132)
  graph.GetYaxis().SetTitleFont(132)
  graph.GetXaxis().SetTitleOffset(1.15)
  graph.GetYaxis().SetTitleOffset(0.9)
  graph.GetXaxis().SetLabelFont(132)
  graph.GetYaxis().SetLabelFont(132)
  graph.GetYaxis().SetTitle(yTitle)
  graph.GetXaxis().SetTitle("m(#Upsilon(2S) #pi^{+} #pi^{-}) [MeV/#font[12]{c}^{#kern[0.5]{2}}]")
  #graph.GetYaxis().SetTitle("")

#int cols[2] = {2,4}

def addToVector(name: str, file: TFile, vec: lTGraph, i: int):
  
  graph = file.Get(name)
  if (graph):
    plotGraph(graph, "Likelihood", 1,2, 2)
    vec.append(graph)
  else:
    print("Failed To find graph")

def findMinVal(vec: lTGraph, i: int):
  minim = TMath.Max(vec[0].GetY()[i], -3400.) 
  for iv in range(len(vec)):
    if (vec[iv].GetY()[i]  < minim and vec[iv].GetY( )[i] > -3400. ): minim = vec[iv].GetY()[i] 
  return minim

def minimize(graph : TGraph, low: float, high: float) -> pair : 

  # step thrugh and get min
  # nsteps = 20000
  nsteps = 20
  step = (high - low)/float(nsteps) 
  i = 0
  minx  = low
  minim = 10000. 
  for i in range(nsteps):
    val = graph.Eval(low + i*step,0,"S")
    # std.cout << low + i*step << " " << val << std.endl
    if (val < minim):
      minim = val
      minx = low + i*step

  #std.cout << min << " " << minx << std.endl

  # now we can get error DLL goes up by 1
  i = 0
  while (graph.Eval(minx + i*step, 0,"S") < (0.5+ minim) ):
    ++i

  #  float uperr = graph.Eval(minx + i*step, 0,"S") -minx
  #/ std.cout  << 
  uperr = minx + i*step

  i = 0
  while (graph.Eval(minx - i*step, 0,"S") < (0.5+ minim)):
    ++i
  #  float downerr = minx - graph.Eval(minx - i*step, 0,"S")
  downerr = minx - i*step

  print(downerr, " ", uperr)
  return([minx, 0.5*(uperr-downerr)/1.])

#const int nf = 2

# def shiftGraph(gr: TGraph, min: float , max: float) -> TGraph:
#   res = minimize(gr,min, max) #pair[float,float]
#   themin = gr.Eval(res[0],0,"S")
#   s = TGraph(gr.GetN())
#   for i in range(gr.GetN()):
#     s.SetPoint(i,gr.GetX()[i], gr.GetY()[i] - themin)
#   return s

def shiftGraph(gr: TGraph, themin: float) -> TGraph:

  s = TGraph(gr.GetN())
  for i in range(gr.GetN()):
    s.SetPoint(i,gr.GetX()[i], gr.GetY()[i] - themin)
  return s


def addSingle( minx = 10355.04, maxx = 10355.26) -> TGraph:

  kSource = "prompt"
  kNorm = "jpsi"
  grname = "combi"
  
  c2 = TCanvas("c2", "c2",10,44,600,400)

  graphs = [] #std.vector<TGraph*> 


  # models = ["Base","Exp","Sqr","CB", "Gamma", "Mass"]
  models = ["Base","CB", "Gamma"]
  for m in models:
      file_ll = TFile("{}/mass_fit/one_hist/PLL_{}_{}_{}_ProbNNp_06_check_sim.root".format(homeDir,kSource,m,kNorm), "read")
      print("Adding ", m)
      graph = TGraph(file_ll.Get("pll").Clone())
      graph.SetLineColor(2)
      graph.SetLineStyle(2)
      graphs.append(graph)
      file_ll.Close()

  # sigmodels = ["DCB","TWOCB","GCB"]
  # bmodels = ["exp","cheb", "powexp", "pow"]
  # for b in bmodels:
  #   for s in sigmodels:
  #     name = "mgraph_" + s + "_" + b
  #     print("Adding ", name)
  #     graph = file.Get(name())
  #     graph.SetLineColor(2)
  #     graph.SetLineStyle(2)
  #     graphs.append(graph)
   
  # combine them 
  combi = TGraph(graphs[0].GetN())
  for i in range(graphs[0].GetN()):
    combi.SetPoint(i,graphs[0].GetX()[i], findMinVal(graphs,i))


  print("made combi")
  
  plotGraph(combi, "Likelihood", 1,1,1)
  combi.Draw("AL")

  # return combi
  #graphs[0].Draw("L")
  #graphs[1].Draw("L")
  #combi.Draw("L")
  combi.SetName(grname)
  
  res = minimize(combi,minx, maxx) #pair[float,float]
  print("Min combi {:.10f} {:.10f}".format(res[0], res[1]))
  themin = combi.Eval(res[0],0,"S")
  combi2 = shiftGraph(combi,themin)
  # combi2.Draw()



  res2 = minimize(graphs[0], minx, maxx) #pair[float,float]
  print("Min combi {:.10f} {:.10f}".format(res2[0], res2[1]))
  
  plotGraph(combi2, "Likelihood", 1,1,1)
  combi2.SetMaximum(4)
  combi2.SetMinimum(0)
  combi2.Draw("AC")
  for g in graphs:
    sg = shiftGraph(g,themin)
    sg.SetLineColor(2)
    sg.SetLineStyle(2)
    sg.Draw("C") 
    
  # /*TGraph* sg = shiftGraph(graphs[7],themin)
  # sg.SetLineColor(2)
  # sg.SetLineStyle(2)
  # sg.Draw("L") */
  combi2.Draw("C")

  line = TLine(res[0],0, res[0], 4)
  line.SetLineColor(4)
  line.SetLineStyle(2)
  line.Draw()

  c2.SaveAs("scan.pdf")

  return combi

if __name__ == "__main__":

  addSingle(0, 2.5)