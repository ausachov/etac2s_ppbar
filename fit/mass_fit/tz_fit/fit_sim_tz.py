from ROOT import gROOT, std, TFile, RooFit, RooWorkspace, RooRealVar, RooFormulaVar, RooArgList, RooLinkedList, RooCmdArg, \
    RooFFTConvPdf, RooGaussian, RooGenericPdf, RooChebychev, RooAddPdf, RooDataHist, RooArgSet, RooProdPdf, \
    RooSimultaneous, RooCategory, RooMinimizer
from setupPlot import Draw, set_frame, set_canvas
from setupModel import binningDict, combineTz
localDir = "/users/LHCb/zhovkovska/etac2s_ppbar/"
#from setupFitModel import *


histosDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/Histos/"
# saveDir = "/users/LHCb/zhovkovska/scripts/"
# homeDir = "/sps/lhcb/zhovkovska/etac_ppbar/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"
gROOT.LoadMacro(f"{localDir}/fit/libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro(f"{localDir}/fit/libs/libBifurcatedCB/BifurcatedCB.cxx+")
from ROOT import RooRelBreitWigner, BifurcatedCB


minM = 2850
maxM = 3250
nBins = 1000

'''
def getData_h(w:RooWorkspace, sel:str="all", nPT:int=0, nTz:int=0, state:str='Jpsi', shift:bool=False, years:list=[2018]):
       
    inCutKey = f"{sel}_l0TOS"
    # if keyPrompt: inCutKey = "all_l0TOS"
    # else:         inCutKey = "secondary_l0TOS"
    
    if nPT!=0:    
        if nTz!=0: binPT = f"Jpsi_PT/{state}/bin{nPT}_"
        else:      binPT = f"Jpsi_PT/bin{nPT}%s_"
        binTz = f"Tz{nTz}.root"
    else:        
        binPT = "Total/"
        # binTz = "Tz%s_7Bins.root"%(nTz)
        binTz = f"Tz{nTz}.root"

    hh = TH1D("hh","hh", nBins, minM, maxM)

    for year in years:
        dirName = f"{homeDir}/Histos/m_scaled/{year}/DiProton/{inCutKey}/"
        nameFile = dirName + binPT + binTz
        f = TFile(nameFile,"read")
        print(nameFile)
        # hh = f.Get("Jpsi_M")
        hh.Add(f.Get("Jpsi_M").Clone())
        f.Close()
        
    Jpsi_M = w.var("Jpsi_M")
    #Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M",2850,3250) 

    if(shift):
        hh.SetBins(nBins, minM+(maxM-minM)/(2.*float(nBins)),maxM+(maxM-minM)/(2.*float(nBins)))
        Jpsi_M.setRange(minM+(maxM-minM)/(2.*float(nBins)), maxM+(maxM-minM)/(2.*float(nBins))) 

    dh = RooDataHist(f"dh_PT{nPT}_Tz{nTz}",f"dh_PT{nPT}_Tz{nTz}", RooArgList(Jpsi_M), hh)
    getattr(w,'import')(dh)
    
    print("INFO: Created histogram for years: {years}")
'''

'''
def getConstPars(nConf, nPT=0, nTz=0):
    
    if nConf==3: add="_CB"
    else:        add=""
    
    f = TFile(homeDir+"results/MC/MassFit/MC_MassResolution_wksp.root","READ") 
    wMC = f.Get("w")
    f.Close()
    
    rNtoW = wMC.var("r_NToW").getValV()
    r_ref_etac = wMC.var("r_ref_etac").getValV()
    rArea = wMC.var("r_G1ToG2").getValV()
    sigma = wMC.var("sigma_etac_1").getValV()
    bkgOpt = 0
    gamma = 31.8
    #effic = 0.035
    #gamma = 21.3
    #effic = 0.057*1.19/2.12
    effic = 0.052*1.19/2.12
    #ratioNtoW = 0.5
    #ratioEtaToJpsi = 0.88
    #ratioArea = 0.9
    
    if nPT !=0 : 
        f = TFile(homeDir+"results/MC/MassFit/parFit%s_GeV_PT6.root"%(add),"READ")
        # fSigma = f.Get("fSigmaMC")
        fSigma = f.Get("fit_sigma_etac_1")
        f.Close()
        ptL = binningDict["Jpsi_PT"][nPT-1]
        ptR = binningDict["Jpsi_PT"][nPT]
        rScSigma = fSigma.Eval((ptR+ptL)/2000.)/sigma
    else:
        rScSigma = 1
    
    #f = TFile("/users/LHCb/zhovkovska/ppbar/param_fits/errMassTzDist_shiftFix_MC.root","READ")
    # f = TFile("/users/LHCb/zhovkovska/ppbar/param_fits/errMassTzDist_s_m_scaled_7Bins.root","READ")
    if nPT==0:
        f = TFile(f"{homeDir}/results/MC/MassFit/errMassTzDist_s_m_scaled_11Bins.root","READ")
    else:
        f = TFile(f"{homeDir}/results/MC/MassFit/errMassTzDist_s_m_scaled_7Bins.root","READ")
    hJpsi = f.Get("h_mass_jpsi")
    #hEtac = f.Get("h_mass_etac")
    hSigma = f.Get("h_sigma_mass_MC")
    #rScEtac = (1+hEtac.GetBinContent(nTz))
    mjpsi=3096.9
    rScJpsi = (mjpsi+hJpsi.GetBinContent(nTz))/mjpsi
    # rScJpsi = (1+hJpsi.GetBinContent(nTz))    #!!!!!!!!!!!!!!!
    rScSigma *= hSigma.GetBinContent(nTz)     #!!!!!!!!!!!!!!!
    print(f"m_scale: {rScJpsi}, sigma_scale: {rScSigma}")
    f.Close()
    
    
    if (nConf == 1):

        if nPT !=0 : 
            f = TFile(homeDir+"results/MC/MassFit/parFit%s_GeV.root"%(add),"READ")
            fSigma = f.Get("fSigmaMC")
            f.Close()
            ptL = binningDict["Jpsi_PT"][nPT-1]
            ptR = binningDict["Jpsi_PT"][nPT]
            rScSigma = fSigma.Eval((ptR+ptL)/2000.)/sigma
        else:
            rScSigma = 1

        if nPT==0:
            f = TFile(f"{homeDir}/results/MC/MassFit/errMassTzDist_s_m_scaled_11Bins.root","READ")
        else:
            f = TFile(f"{homeDir}/results/MC/MassFit/errMassTzDist_s_m_scaled_7Bins.root","READ")
        fSigmaTz = f.Get("fit_sigma_etac_1")
        tzL = binningDict["Jpsi_Tz"][nTz-1]
        tzR = binningDict["Jpsi_Tz"][nTz]
        rScSigma *= fSigmaTz.Eval((tzL+tzR)/2.)
        f.Close()
        print ("SETUP 1: Alternative t_z-scaling for sigma")
    elif (nConf == 2):
        f = TFile(homeDir+"results/MC/MassFit/parFit_GeV_PT6.root","READ") 
        func = f.Get("fit_r_ref_etac").Clone()
        f.Close()
        ptL = binningDict["Jpsi_PT"][nPT-1]
        ptR = binningDict["Jpsi_PT"][nPT]
        r_ref_etac = func.Eval((ptR+ptL)/2000.)
        print ("SETUP 2: Alternative pT-scaling for sigma(eta_c)/sigma(J/psi)")      
    elif (nConf == 3):
        rNtoW = 1.0
        rArea = 1.0
        print ("SETUP 3")
    elif (nConf == 4):
        bkgOpt = 1
        print ("SETUP 4")
    elif (nConf == 5):
        bkgOpt = 2
        print ("SETUP 5")
    elif (nConf == 6):
        gamma = 34.0
        print ("SETUP 6")
    elif (nConf == 7):
        f = TFile(homeDir+"results/mass_fit/prompt/Sim/m_scaled/Etac_Wksp_MassFit_PT0_C0_total.root","READ") 
        w = f.Get("w")
        f.Close()
        rScJpsi = w.var("rNormJpsi_Tz{nTz}").getValV()
        print ("SETUP 7")
    elif (nConf == 8):
        #effic = 0.032
        #effic = 0.057*1.19/2.12*(1+0.09)
        effic = 0.062*1.19/2.12*(1+0.08)
        print ("SETUP 8")
    elif (nConf == 9):
        #effic = 0.032
        rScJpsi = 1
        #rScSigma = 1
        print ("SETUP 9")
    elif (nConf == 10):
        #gamma = 19.1
        gamma = 31.8
        print ("SETUP 6")

    #if (nPT != 0):
        #nameFile = "results/MC/MassFit/MC_Mass_2016_wksp_PT%d.root"%(nPT)
        #f = TFile(nameFile,"READ") 
        #wMC = f.Get("w;1")
        #f.Close()
        #if (nConf == 1):
            #ratioEtaToJpsi = wMC.var("r_ref_etac").getValV()+wMC.var("r_ref_etac").getError()
        #else:
            #ratioEtaToJpsi = wMC.var("r_ref_etac").getValV()   
    
    return rNtoW,r_ref_etac,rArea, bkgOpt, gamma, effic, rScJpsi, rScSigma
'''    

def plotData(w:RooWorkspace, nPTs:list, nTzs:list, namePic:str):

    frames, resids, pulls = set_frame(w, nPTs, nTzs)
    c = set_canvas(nPTs, nTzs, frames, resids, pulls)

    for i in range(len(nPTs)):
        c[i].SaveAs(namePic.replace("iPT",str(i)))


def fitData( nPT=0, nTzs=[0], nConf=0, sel="all", state="Jpsi"):

    w = RooWorkspace("w",True)   
    hists, model_pts = combineTz(w, nPT, nTzs, nConf, sel)
        
    # nPTbins = len(nPTs)
    # nTzbins = len(nTzs)

    Jpsi_M = w.var("Jpsi_M")
    Jpsi_M.setBins(nBins)

    simPdf = w.pdf("simPdf")
    combData = w.data("combData")
    sample = w.cat("sample")
    
    #sigma = w.var("sigma_etac_1")
    mass_jpsi = w.var("mass_jpsi")
    # mass_res = w.var("mass_res")
    # gamma_etac = w.var("gamma_etac")

    m_jpsi = 3096.9
    if sel=="prompt":
    # if sel=="all":

        f = TFile(f"{homeDir}/results/mass_fit/etac_tz/fromB/Total/Wksp_M_PT0_Tz0_C0_fromB.root","READ") 
        wSec = f.Get("w")
        f.Close()
        w.var("mass_jpsi").setVal(wSec.var("mass_jpsi").getValV())
        m_jpsi = wSec.var("mass_jpsi").getValV()
        # w.var("mass_jpsi").setConstant(True)
        w.var("mass_res").setVal(wSec.var("mass_res").getValV())        
        #w.var("mass_res").setVal(110.2)  #etac2S2pp
        #w.var("mass_res").setVal(113.5)  #PDG
        w.var("mass_res").setConstant(True)
        w.var("sigma_etac_1").setVal(wSec.var("sigma_etac_1").getValV())
        w.var("sigma_etac_1").setConstant(True)
        


    for idx, nTz in enumerate(nTzs):

        #model_pts[idx].fitTo(hists[idx], RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
        #if nTz!=7:
            #w.var("rNormJpsi_Tz{nTz}").setConstant(False)
            #w.var("rNormSigma_Tz{nTz}").setConstant(False)
        #model_pts[idx].fitTo(hists[idx], RooFit.Extended(True), RooFit.Save(True)
        w.var(f"n_etac_PT{nPT}_Tz{nTz}").setVal(1.e5)
        w.var(f"n_jpsi_PT{nPT}_Tz{nTz}").setVal(3.e5)
        if nTz!=nTzs[-1]:
            w.var(f"rNormJpsi_Tz{nTz}").setConstant(False)
        else:
            w.var(f"rNormJpsi_Tz{nTz}").setVal(1.)
        mass_jpsi.setVal(m_jpsi)
        model_pts[idx].fitTo(hists[idx], RooFit.Extended(True), RooFit.Save(True))
        mass_jpsi.setVal(m_jpsi)
        model_pts[idx].fitTo(hists[idx], RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.NumCPU(20))
        mass_jpsi.setVal(m_jpsi)
        model_pts[idx].fitTo(hists[idx], RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.NumCPU(20))
        # w.var(f"rNormJpsi_Tz{nTz}").setConstant(True)
        # model_pts[idx].fitTo(hists[idx], RooFit.Extended(True), RooFit.Save(True))
        # model_pts[idx].fitTo(hists[idx], RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
        # model_pts[idx].fitTo(hists[idx], RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
        # model_pts[idx].fitTo(hists[idx], RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Save(True))
        # model_pts[idx].fitTo(hists[idx], RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Save(True))
        # bgVal = w.var(f"n_bkg_PT{nPT}_Tz{nTz}").getValV()
        # bgErr = w.var(f"n_bkg_PT{nPT}_Tz{nTz}").getError()
        # w.var(f"n_bkg_PT{nPT}_Tz{nTz}").setRange(bgVal-15*bgErr, bgVal+15*bgErr)
        # a0Val = w.var(f"a0_PT{nPT}_Tz{nTz}").getValV()
        # a0Err = w.var(f"a0_PT{nPT}_Tz{nTz}").getError()
        # #w.var(f"a0_PT{nPT}_Tz{nTz}").setRange(a0Val-15*a0Err, a0Val+15*a0Err)
        # a1Val = w.var(f"a1_PT{nPT}_Tz{nTz}").getValV()
        # a1Err = w.var(f"a1_PT{nPT}_Tz{nTz}").getError()
        # #w.var(f"a1_PT{nPT}_Tz{nTz}").setRange(a1Val-15*a1Err, a1Val+15*a1Err)
        # #a2Val = w.var(f"a2_PT{nPT}_Tz{nTz}").getValV()
        # #a2Err = w.var(f"a2_PT{nPT}_Tz{nTz}").getError()
        # #w.var(f"a2_PT{nPT}_Tz{nTz}").setRange(a2Val-15*a2Err, a2Val+15*a2Err)
        # etaVal = w.var(f"n_etac_PT{nPT}_Tz{nTz}").getValV()
        # etaErr = w.var(f"n_etac_PT{nPT}_Tz{nTz}").getError()
        # #if (nTz==1 and nConf==4) or nConf!=4:
        # #w.var(f"n_etac_PT{nPT}_Tz{nTz}").setRange(etaVal-4*etaErr, etaVal+20*etaErr)


    #w.var("n_bkg_PT%s_Tz1"%(nPT)).setVal(3.1e4)
    #w.var("n_bkg_PT%s_Tz1"%(nPT)).setRange(-100, 4e5)
    
    mass_jpsi.setVal(m_jpsi)
    mass_jpsi.setConstant(True)

    if False:
        #NLL with extended term, constrains, and likelihood offset
        nll = simPdf.createNLL(combData, RooFit.Extended(True), RooFit.Offset(True))

        # Manual minimization
        minimizer = RooMinimizer(nll)
        minimizer.setVerbose(False)
        minimizer.setMinimizerType("Minuit2")
        minimizer.optimizeConst(True)
        minimizer.setEps(1e-8)
        minimizer.setStrategy(2)
        minimizer.setPrintEvalErrors(1)
        minimizer.setOffsetting(True)
        minimizer.setMaxIterations(1000)
        minimizer.migrad()
        minimizer.minos()

    #ROOT.Math.MinimizerOptions.SetMaxIterations(1000)
    w.var("mass_jpsi").setConstant(False)    
    w.var("mass_res").setConstant(False)    
    w.var("sigma_etac_1").setConstant(False)
    
    llist = RooLinkedList()
    llist.Add(RooCmdArg(RooFit.Extended(True)))
    llist.Add(RooCmdArg(RooFit.Save(True)))
    llist.Add(RooCmdArg(RooFit.Offset(True)))
    llist.Add(RooCmdArg(RooFit.Strategy(1)))
    llist.Add(RooCmdArg(RooFit.NumCPU(50)))

    simPdf.Print()
    combData.Print()

    r = simPdf.fitTo(combData, llist)
    r = simPdf.fitTo(combData, llist)
    llist.Add(RooCmdArg(RooFit.Minos(True)))
    r = simPdf.fitTo(combData, llist)
    r = simPdf.fitTo(combData, llist)
        
    # Draw(w, combData, sample, simPdf, [nPT], nTzs, nConf, sel, state)

    inCutKey = sel
    
    #if nPT!=0:    binPT = "Jpsi_PT/"
    #else:         binPT = "Total/"

    # binPT = "Sim/"
    binPT = "Sim_oldPT/"
    #binTz = "Tz%s.root"%(nTz)
    
    dirName = f"{homeDir}/results/mass_fit/etac_tz/{inCutKey}/{binPT}"# + "m_scaled/"
    
    #if (nPT!=0 and nTz!=0):
        #dirName = dirName + state + "/"
        
    nameTxt  = f"{dirName}/{state}_Result_M_PT{nPT}_C{nConf}_total_oldPID.txt"
    nameRoot = f"{dirName}/{state}_Plot_M_PT{nPT}_C{nConf}_total_oldPID.root"
    nameWksp = f"{dirName}/{state}_Wksp_M_PT{nPT}_C{nConf}_total_oldPID.root"
    # namePic  = f"{dirName}/{state}_Plot_M_PT{nPT}_C{nConf}_total.pdf"
    namePic  = f"{dirName}/{state}_Plot_M_PTiPT_C{nConf}_total_oldPID.pdf"
    
    w.writeToFile(nameWksp)
    plotData(w, [nPT], nTzs, namePic)

    import os, sys 
    save = os.dup( sys.stdout.fileno() ) 
    newout = open(nameTxt, 'w' ) 
    os.dup2( newout.fileno(), sys.stdout.fileno() ) 
    r.Print("v") 
    r.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() ) 
    newout.close()
    
    
    #fFit = TFile (nameRoot,"RECREATE")
    #w.Print("v")
    #c.Write("")
    #fFit.Close()

    #c.SaveAs(namePic)
    ##r.correlationMatrix().Print()



def Fit_Mass():

    nPT = 6
    nTzTot = 10
    # nTzEtac = 5
    nTzEtac = 7
    nTzJpsi = 7
    nConf = 11
    iConf = 0
    pr = True
    #    Bool_t pr = False
    #    for (Int_t iConf=0 iConf<10 iConf++)
    #    {
    #        fitData(iConf,0,pr)
    #    }
    #   Int_t iPT=0
    #fitData(0, 0, 0, False, 'Etac')
    #for iPT in range (1, nPT+1):        
            #fitData(iPT, 0, iConf, False, 'Etac')
    #for iTz in range (1, nTzTot+1):        
            #fitData(0, iTz, iConf, pr, 'Etac')
    #for iPT in range (1, nPT+1):        
            #fitData(iPT, 0, iConf, pr, 'Etac')
            
    #for iPT in range(1, nPT+1):
        #for iTz in range (1, nTzJpsi+1):        
            #fitData(iPT, iTz, iConf, pr, 'Jpsi')
    #for iPT in range(1, nPT+1):
        #for iTz in range (1, nTzEtac+1):        
            #fitData(iPT, iTz, iConf, pr, 'Etac')


if __name__=="__main__":
    #w = RooWorkspace("w",kTRUE)   
    #fillRelWorkspace(w,0,0)
    #getData_h(w, False, 1, 0,"Etac")

    #fitData( 0, 0, 4, False)
    #fitData( 0, 0, 4, True)
    # nTzs = [1, 2, 3, 4, 5, 6, 7]
    nTzs = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    #nConfs = [1, 4, 8, 9]
    nConfs = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    # nConfs = [0]
    nConfs = ["Base"]
    for iC in nConfs:
        # for iPT in range(2, 7):
        for iPT in range(0, 1):
            fitData( iPT, nTzs, iC, "all")
    #fitData( 0, nTzs, 0, True,"Etac")
    #fitData( 1, 2, 0, True)
                
    #Fit_Mass()
