import ROOT
from ROOT import gROOT, std, TF1, TFile, TH1D
from ROOT import RooFit, RooWorkspace, RooDataHist, RooRealVar, RooFormulaVar, RooArgSet, RooArgList, \
                 RooChebychev, RooGenericPdf, RooGaussian, RooAddPdf, RooFFTConvPdf, RooProdPdf, \
                 RooCategory, RooSimultaneous
# from ROOT.RooFit import RooConst
# gInterpreter.GenerateDictionary("std::map<std::string, RooDataHist>", "map;string;RooDataHist.h")


histosDir = "/sps/lhcb/zhovkovska/etac1s_ppbar/Histos/"
# histosDir = "/users/LHCb/usachov/zhovkovska2018/scripts/Histos/"
# homeDir = "/users/LHCb/usachov/zhovkovska2018/scripts/"
#homeDir = "/sps/lhcb/zhovkovska/etacToPpbar/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"
localDir = "/users/LHCb/zhovkovska/etac2s_ppbar/"

gROOT.LoadMacro(f"{localDir}/fit/libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro(f"{localDir}/fit/libs/libBifurcatedCB/BifurcatedCB.cxx+")
from ROOT import RooRelBreitWigner, BifurcatedCB

import sys
sys.path.insert(1, f"{localDir}/fit//libs/")
import logging
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)

binningDict = {
    "Jpsi_PT":                      [5000., 6500, 8000, 10000, 12000, 14000, 20000.],
    "Jpsi_Y":                       [2.0, 2.5, 3.0, 3.5, 4.0, 4.5],
    "TMath::Abs(ProtonM_CosTheta)": [0.0, 0.15, 0.3, 0.50, 1.0],
    "nSPDHits":                     [0, 100, 150, 200, 250, 300],
    "Jpsi_Tz":                      [-10., -0.125, -0.025, 0.0, 0.20, 2.0, 4.0, 10.0],
    "Jpsi_Tz_Tot":                  [-10., -0.125, -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10], #[-10., -0.15, -0.025, 0.0, 0.20, 2.0, 10.0],
    "Jpsi_Tz_old":                  [-10., -0.025, 0.0, 0.20, 2.0, 10.0],
    "Jpsi_Tz_Tot_old":              [-10., -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10],
}



#
minM = 2850.
maxM = 3250.
nBins = 1000

def getDataHist(w:RooWorkspace, sel:str="all", nPT:int=0, nTz:int=0, state:str='Jpsi', shift:bool=False, years:list=[2018]):
    ''' Filling RooDataHist for 
        - prompt/fromB/all: sel
        - PT bin: nPT
        - Tz bin: nTz
        - merges data for a list of years  
    '''       
    inCutKey = f"{sel}_l0TOS"
    
    if nPT!=0:    
        if nTz!=0: binPT = f"Jpsi_PT/{state}/bin{nPT}_"
        else:      binPT = f"Jpsi_PT/bin{nPT}%s_"
        binTz = f"Tz{nTz}.root"
    else:        
        binPT = "Total/"
        # binTz = f"Tz{nTz}_7Bins.root"
        binTz = f"Tz{nTz}_oldPT.root"
        # binTz = f"Tz{nTz}.root"

    hh = TH1D("hh","hh", nBins, minM, maxM)

    for year in years:
        # dirName = f"{homeDir}/Histos/m_scaled/{year}/DiProton/{inCutKey}/"
        dirName = f"{homeDir}/Histos/m_scaled/{year}/DiProtonOldPID/{inCutKey}/"
        nameFile = dirName + binPT + binTz
        f = TFile(nameFile,"read")
        logging.info(f" Imporint {nameFile}")
        if years.index(year) == 0:
            hh = f.Get("Jpsi_M")
            hh.SetDirectory(0)
        else:
            hh.Add(f.Get("Jpsi_M").Clone("hh"))
            hh.SetDirectory(0)
        f.Close()
        
    Jpsi_M = w.var("Jpsi_M")

    if(shift):
        hh.SetBins(nBins, minM+(maxM-minM)/(2.*float(nBins)),maxM+(maxM-minM)/(2.*float(nBins)))
        Jpsi_M.setRange(minM+(maxM-minM)/(2.*float(nBins)), maxM+(maxM-minM)/(2.*float(nBins))) 
    dh = RooDataHist(f"dh_PT{nPT}_Tz{nTz}",f"dh_PT{nPT}_Tz{nTz}", RooArgList(Jpsi_M), hh)
    getattr(w,'import')(dh)
    
    logging.info(f" Created histogram for years: {years}")

'''
def getConstPar(nConf, nPT=0):

    f = TFile(homeDir+"/MC/MassFit/MC_MassResolution_wksp.root","READ")
    wMC = f.Get("w")
    f.Close()

    rNtoW = wMC.var("rNarToW").getValV()
    rEtaToJpsi = wMC.var("rEtaToJpsi").getValV()
    rArea = wMC.var("rG1toG2").getValV()
    sigma = wMC.var("sigma_eta_1").getValV()
    bkgOpt = 'Base'
    gamma = 31.8
    # effic = 0.057*1.19/2.12
    effic = 0.052*1.19/2.12
    #gamma = 21.3
    #effic = 0.06
    #ratioNtoW = 0.5
    #ratioEtaToJpsi = 0.88
    #ratioArea = 0.9
    rScSigma = 1.0

    add = "_CB" if nConf=="CB" else ""

    if (nPT != 0):
        #rNtoW = wMC.var("rNarToW").getValV()+wMC.var("rNarToW").getError()
        f = TFile(homeDir+"/MC/MassFit/parFit{}_GeV_PT6.root".format(add),"READ")
        # fSigma = f.Get("fSigmaMC")
        fSigma = f.Get("fit_sigma_eta_1")
        f.Close()
        ptL = binningDict["Jpsi_PT"][nPT-1]/1000.
        ptR = binningDict["Jpsi_PT"][nPT]/1000.
        rScSigma = fSigma.Eval((ptR+ptL)/2.)/sigma
    if (nConf == 'rEtaToJpsi'):
        f = TFile(homeDir+"MC/MassFit/parFit_GeV_PT6.root","READ")
        #f = TFile(homeDir+"MC/MassFit/parFit_GeV_Y4.root","READ")
        func = f.Get("fit_rEtaToJpsi")
        f.Close()
        ptL = binningDict["Jpsi_PT"][nPT-1]/1000.
        ptR = binningDict["Jpsi_PT"][nPT]/1000.
        rEtaToJpsi = func.Eval((ptR+ptL)/2.)
        #ptL = binningDict["Jpsi_Y"][nPT-1]
        #ptR = binningDict["Jpsi_Y"][nPT]
        #rEtaToJpsi = func.Eval((ptR+ptL)/2.)
        rEtaToJpsi = wMC.var("rEtaToJpsi").getValV()+wMC.var("rEtaToJpsi").getError()
        #print rEtaToJpsi
        #input()
    elif (nConf == 'CB'):
        rNtoW = 1.0
        rArea = 1.0
    elif (nConf == 'Chebychev3par' or nConf == 'Chebychev4par'):
        bkgOpt = nConf
    elif (nConf == 'Gamma34'):
        gamma = 34.0
    elif (nConf == 'eff_pppi0'):
        # effic = 0.057*1.19/2.12*(1+0.09)
        effic = 0.052*1.19/2.12*(1+0.06)
    print ("SETUP ",nConf)

    #if (nPT != 0):
        #nameFile = "results/MC/MassFit/MC_Mass_2016_wksp_PT%d.root".format(nPT)
        #f = TFile(nameFile,"READ")
        #wMC = f.Get("w;1")
        #f.Close()
        #if (nConf == 1):
            #ratioEtaToJpsi = wMC.var("rEtaToJpsi").getValV()+wMC.var("rEtaToJpsi").getError()
        #else:
            #ratioEtaToJpsi = wMC.var("rEtaToJpsi").getValV()

    return rNtoW,rEtaToJpsi,rArea, bkgOpt, gamma, effic, rScSigma
'''

'''
def setCommonParameters(w,nConf, nPTs):

    Jpsi_M = w.var("Jpsi_M")
    ratioNtoW,ratioEtaToJpsi,ratioArea, bkgType, gammaEtac, eff, rScSigma = getConstPar(nConf, 0)

    mDiffEtac = 113.501
    mass_Jpsi = RooRealVar("mass_Jpsi","mean of gaussian", 3096.9, 3030, 3150)
    mass_res = RooRealVar("mass_res","mean of gaussian",mDiffEtac, 80, 130)
    gamma_eta = RooRealVar("gamma_eta","width of Br-W", gammaEtac, 10., 50.)
    mass_eta = RooFormulaVar("mass_eta","mean of gaussian","@0-@1",RooArgList(mass_Jpsi,mass_res))

    spin_eta = RooRealVar("spin_eta","spin_eta", 0. )
    radius_eta = RooRealVar("radius_eta","radius", 1.)
    proton_m = RooRealVar("proton_m","proton mass", 938.3 )

    getattr(w,'import')(mass_Jpsi)
    getattr(w,'import')(mass_res)
    # getattr(w,'import')(mass_eta)
    # getattr(w,'import')(gamma_eta)

    # getattr(w,'import')(spin_eta)
    # getattr(w,'import')(radius_eta)
    # getattr(w,'import')(proton_m)

    rNarToW = RooRealVar("rNarToW","rNarToW",ratioNtoW)
    rG1toG2 = RooRealVar("rG1toG2","rG1toG2",ratioArea)
    eff_pppi0 = RooRealVar("eff_pppi0","eff_pppi0",eff)

    getattr(w,'import')(rNarToW)
    getattr(w,'import')(rG1toG2)
    getattr(w,'import')(eff_pppi0)

    sigma_eta = RooRealVar("sigma_eta","width of gaussian", 9., 5., 50.)
    getattr(w,'import')(sigma_eta)

    br_wigner = RooRelBreitWigner("br_wigner", "br_wigner",Jpsi_M, mass_eta, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)
    getattr(w,'import')(br_wigner)


    # Parametrisation: sum(f_i) = 1
    pt = [i for i in range(1, nPTs+1)]
    for key in ["Prompt", "FromB"]:
        for ccbar in ["Eta", "Jpsi"]:
            formula = "1. "
            fs = []
            fs_list = RooArgList()
            for nPT in range(1,nPTs):
                keyPrompt =  "PT{}_{}".format(nPT,key)

                #fraction of data for particular year
                f = RooRealVar("f{}_{}".format(ccbar,keyPrompt),"fraction of {}".format(keyPrompt), 0.2, 0., 1.)
                formula += " - @{}".format(pt.index(nPT))
                fs.append(f)
                fs_list.add(f)
                getattr(w,'import')(f)

            keyPrompt =  "PT{}_{}".format(nPTs,key)
            f_last = RooFormulaVar("f{}_{}".format(ccbar,keyPrompt),"fraction of {}".format(keyPrompt), formula, fs_list)
            getattr(w,'import')(f_last)

    nEta = RooRealVar("nEta_Prompt","num of Etac", 10, 1.e7)
    nJpsi = RooRealVar("nJpsi_Prompt","num of J/Psi", 10, 1.e7)
    getattr(w,'import')(nEta)
    getattr(w,'import')(nJpsi)
    nEta = RooRealVar("nEta_FromB","num of Etac", 10, 1.e7)
    nJpsi = RooRealVar("nJpsi_FromB","num of J/Psi", 10, 1.e7)
    getattr(w,'import')(nEta)
    getattr(w,'import')(nJpsi)
    # nEtacRel = RooRealVar("nEtacRel_{}".format(key),"num of Etac", 0.0, 3.0)
'''


def getConstPar(nConf:str="Base", nPT:int=0, nTz:int=0):
    ''' Extract constant parameters from MC fit for given configuration, PT bin and Tz bin '''
    
    if nConf!="CB": add=""
    else:           add="_CB"
    
    # f = TFile(f"{homeDir}/results/MC/MassFit/MC_MassResolution_wksp.root","READ") 
    f = TFile(f"{homeDir}/results/MC/etac/mass_fit/MC_MassResolution_wksp.root","READ") 
    wMC = f.Get("w").Clone()
    f.Close()
    
    rNtoW = wMC.var("r_NToW").getValV()
    r_ref_etac = wMC.var("r_ref_etac").getValV()
    rArea = wMC.var("r_G1ToG2").getValV()
    sigma = wMC.var("sigma_etac_1").getValV()
    if "Cheb" in nConf: bkgOpt = nConf
    else:               bkgOpt = "Exp"
    eff = 0.052
    
    if nPT !=0 : 
        # f = TFile(homeDir+"results/MC/MassFit/parFit%s_GeV_PT6.root"%(add),"READ")
        f = TFile(f"{homeDir}/results/MC/MassFit/parFit{add}_GeV_PT6.root","READ")
        # fSigma = f.Get("fSigmaMC")
        fSigma = f.Get("fit_sigma_etac_1")
        f.Close()
        ptL = binningDict["Jpsi_PT"][nPT-1]
        ptR = binningDict["Jpsi_PT"][nPT]
        rScSigma = fSigma.Eval((ptR+ptL)/2000.)/sigma
    else:
        rScSigma = 1
    
    if nPT==0:
        # f = TFile(f"{homeDir}/results/MC/MassFit/errMassTzDist_s_m_scaled_11Bins.root","READ")
        f = TFile(f"{homeDir}/results/MC/etac/mass_fit/errMassTzDist_s_m_scaled_11Bins.root","READ")
    else:
        # f = TFile(f"{homeDir}/results/MC/MassFit/errMassTzDist_s_m_scaled_7Bins.root","READ")
        f = TFile(f"{homeDir}/results/MC/etac/mass_fit/errMassTzDist_s_m_scaled_7Bins.root","READ")
    hJpsi = f.Get("h_mass_jpsi")
    #hEtac = f.Get("h_mass_etac")
    hSigma = f.Get("h_sigma_mass_MC")
    #rScEtac = (1+hEtac.GetBinContent(nTz))
    mjpsi=3096.9
    rScJpsi = (mjpsi+hJpsi.GetBinContent(nTz))/mjpsi
    # rScJpsi = (1+hJpsi.GetBinContent(nTz))    #!!!!!!!!!!!!!!!
    rScSigma *= hSigma.GetBinContent(nTz)     #!!!!!!!!!!!!!!!
    logging.debug(f" m_scale: {rScJpsi:.3f}, sigma_scale: {rScSigma:.3f}")
    f.Close()
    
    if (nConf == "tzSigma"):

        if nPT !=0 : 
            # f = TFile(homeDir+"results/MC/MassFit/parFit%s_GeV_PT6.root"%(add),"READ")
            f = TFile(homeDir+"results/MC/etac/mass_fit/parFit%s_GeV_PT6.root"%(add),"READ")
            fSigma = f.Get("fSigmaMC").Clone()
            f.Close()
            ptL = binningDict["Jpsi_PT"][nPT-1]
            ptR = binningDict["Jpsi_PT"][nPT]
            rScSigma = fSigma.Eval((ptR+ptL)/2000.)/sigma
            # f = TFile("/users/LHCb/zhovkovska/ppbar/param_fits/errMassTzDist_s_m_scaled_11Bins.root","READ")
            f = TFile(f"{homeDir}/results/MC/etac/mass_fit/errMassTzDist_s_m_scaled_11Bins.root","READ")
        else:
            rScSigma = 1
            # f = TFile("/users/LHCb/zhovkovska/ppbar/param_fits/errMassTzDist_s_m_scaled_7Bins.root","READ")
            f = TFile(f"{homeDir}/results/MC/etac/mass_fit/errMassTzDist_s_m_scaled_7Bins.root","READ")
        fSigmaTz = f.Get("fit_sigma_etac_1")
        tzL = binningDict["Jpsi_Tz"][nTz-1]
        tzR = binningDict["Jpsi_Tz"][nTz]
        rScSigma *= fSigmaTz.Eval((tzL+tzR)/2.)
        f.Close()
        print ("SETUP 1: Alternative t_z-scaling for sigma")
    elif (nConf == "rEtac2Jpsi"):
        # f = TFile(homeDir+"results/MC/MassFit/parFit_GeV_PT6.root","READ") 
        f = TFile(homeDir+"results/MC/etac/mass_fit/parFit_GeV_PT6.root","READ") 
        func = f.Get("fit_r_ref_etac").Clone()
        f.Close()
        ptL = binningDict["Jpsi_PT"][nPT-1]
        ptR = binningDict["Jpsi_PT"][nPT]
        r_ref_etac = func.Eval((ptR+ptL)/2000.)
        #r_ref_etac = wMC.var("r_ref_etac").getValV()+wMC.var("r_ref_etac").getError()
        print ("SETUP 2: Alternative pT-scaling for sigma(eta_c)/sigma(J/psi)")      
    elif (nConf == "CB"):
        rNtoW = 1.0
        rArea = 1.0
        print ("SETUP 3: Alterative resolution model: Crystal Ball")
    elif (nConf == "Chebychev3par"):
        print ("SETUP 4: Alternative background: Chebychev with 3 parameters")
    elif (nConf == "Chebychev4par"):
        print ("SETUP 5: Alternative background: Chebychev with 4 parameters")
    elif (nConf == "Gamma"):
        print ("SETUP 6: Alternative eta_c width")
    elif (nConf == "tzMass"):
        f = TFile(homeDir+"results/mass_fit/prompt/Sim/m_scaled/Etac_Wksp_MassFit_PT0_C0_total.root","READ") 
        w = f.Get("w")
        f.Close()
        rScJpsi = w.var("rNormJpsi_Tz{nTz}").getValV()
        print ("SETUP 7: Alternative t_z-scaling for mass")
    elif (nConf == "pppi0"):
        #effic = 0.032
        #effic = 0.057*1.19/2.12*(1+0.09)
        eff = 0.055
        print ("SETUP 8: Alternative efficiency for pppi0 reconstruction")
    elif (nConf == "noTzMass"):
        #effic = 0.032
        rScJpsi = 1
        #rScSigma = 1
        print ("SETUP 9: No tz-scaling for mass")
    elif (nConf == "Shift"):
        print ("SETUP 10: Alternative mass range of the histogram")

    #if (nPT != 0):
        #nameFile = "results/MC/MassFit/MC_Mass_2016_wksp_PT%d.root"%(nPT)
        #f = TFile(nameFile,"READ") 
        #wMC = f.Get("w;1")
        #f.Close()
        #if (nConf == 1):
            #ratioEtaToJpsi = wMC.var("r_ref_etac").getValV()+wMC.var("r_ref_etac").getError()
        #else:
            #ratioEtaToJpsi = wMC.var("r_ref_etac").getValV()   
    
    logging.debug(f"rNtoW: {rNtoW}, r_ref_etac: {r_ref_etac}, rArea: {rArea}, eff: {eff}, rScJpsi: {rScJpsi}, rScSigma: {rScSigma}")
    return rNtoW,r_ref_etac,rArea, eff, rScJpsi, rScSigma
    
def setCommonPar(w, nConf:str="Base"):

    # ratioNtoW,ratioEtaToJpsi, ratioArea, bkgType, width_etac, eff, rScJpsi, rScSigma = getConstPar(nConf, nPT, nTz)
    if nConf=="Gamma":
        width_etac = 32.0
        width_err = 0.7
    else:
        width_etac = 34.0
        width_err = 2.3
        print ("SETUP Gamma: Alternative eta_c width")

    Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M",2850.,3250.) 
    Jpsi_M.setBins(2000,"cache")
    Jpsi_M.setRange("SBLeft", 2850, 2900)
    Jpsi_M.setRange("SBCentral", 3035, 3060)
    Jpsi_M.setRange("SBRight", 3150, 3250)
    Jpsi_M.setRange("SignalEtac", 2900, 3035)
    Jpsi_M.setRange("SignalJpsi", 3060, 3150)
    Jpsi_M.setRange("Total", 2850, 3250)
        
    mDiffEtac = 113.501
    mean_jpsi = 3096.9
    mean_etac = 2983.4

    sigma_etac = RooRealVar("sigma_etac_1","width of gaussian", 9., 5., 50.) 

    mass_jpsi = RooRealVar("mass_jpsi","mean of gaussian",mean_jpsi, 3030., 3150.) 
    mass_res = RooRealVar("mass_res","mean of gaussian",mDiffEtac, 100., 130.)     

    mass_etac = RooFormulaVar("mass_etac","mean of gaussian","(@0-@1)",RooArgList(mass_jpsi,mass_res)) 

    gamma_etac = RooRealVar("gamma_etac","width of Br-W", width_etac, 10., 50.)
    #if nConf!=6:
    gamma_etac.setConstant()
    spin_etac = RooRealVar("spin_etac","spin_etac", 0. )
    radius_etac = RooRealVar("radius_etac","radius", 1.)
    proton_m = RooRealVar("proton_m","proton mass", 938.3 )

    w.Import(Jpsi_M)
    w.Import(sigma_etac)
    w.Import(mass_jpsi)
    w.Import(mass_etac)
    w.Import(gamma_etac)
    w.Import(spin_etac)
    w.Import(radius_etac)
    w.Import(proton_m)

    #Constraints on Mass and Gamma

    fconstrgamma = RooGaussian("fconstrgamma","fconstrgamma", gamma_etac, RooFit.RooConst(width_etac), RooFit.RooConst(width_err))  #PDG 
    fconstrmres = RooGaussian("fconstrmres","fconstrmres",mass_res, RooFit.RooConst(113.5), RooFit.RooConst(0.5))  #PDG 
    fconstrmjpsi = RooGaussian("fconstrmjpsi","fconstrmjpsi",mass_jpsi, RooFit.RooConst(3096.9),RooFit.RooConst(0.5)) 

    w.Import(fconstrgamma)
    w.Import(fconstrmres)
    w.Import(fconstrmjpsi)
        
def fillWorkspace(w:RooWorkspace, nConf:str="Base", nPT:int=0, nTz:int=0, rel:bool=False):
    
    Jpsi_M = w.var("Jpsi_M")
    
    mDiffEtac = 113.501
    mean_jpsi = 3096.9
    
    ratioNtoW,ratioEtaToJpsi,ratioArea, eff, rScJpsi, rScSigma = getConstPar(nConf, nPT, nTz)
    
    
    if (nConf=="Shift"):
        Jpsi_M.setRange(2855,3245)
    
    
    r_ref_etac = RooRealVar("r_ref_etac_PT%s"%(nPT),"r_ref_etac", ratioEtaToJpsi)
    r_NToW = RooRealVar("r_NToW","r_NToW",ratioNtoW)
    r_G1ToG2 = RooRealVar("r_G1ToG2","r_G1ToG2",ratioArea)
    eff_pppi0 = RooRealVar("eff_pppi0","eff_pppi0",eff)

    n_jpsi = RooRealVar(f"n_jpsi_PT{nPT}_Tz{nTz}","num of J/Psi", -1e2, 1.e7)    #negative yield
    n_jpsi_1 = RooFormulaVar(f"n_jpsi_1_PT{nPT}_Tz{nTz}","num of J/Psi","@0*@1",RooArgList(n_jpsi,r_G1ToG2))
    n_jpsi_2 = RooFormulaVar(f"n_jpsi_2_PT{nPT}_Tz{nTz}","num of J/Psi","@0-@1",RooArgList(n_jpsi,n_jpsi_1))
    if rel:
        n_etac_rel = RooRealVar(f"n_etac_rel_PT{nPT}_Tz{nTz}","num of etac", 0.0, 3.0)
        n_etac_1 = RooFormulaVar(f"n_etac_1_PT{nPT}_Tz{nTz}","num of etac","@0*@1*@2",RooArgList(n_etac_rel,n_jpsi,r_G1ToG2))
        n_etac_2 = RooFormulaVar(f"n_etac_2_PT{nPT}_Tz{nTz}","num of etac","@0*@1-@2",RooArgList(n_etac_rel,n_jpsi,n_etac_1))
    else:
        n_etac = RooRealVar(f"n_etac_PT{nPT}_Tz{nTz}","num of etac", -5e2, 1.e7)       #negative yield
        n_etac_1 = RooFormulaVar(f"n_etac_1_PT{nPT}_Tz{nTz}","num of etac","@0-@1",RooArgList(n_etac,r_G1ToG2))
        n_etac_2 = RooFormulaVar(f"n_etac_2_PT{nPT}_Tz{nTz}","num of etac","@0-@1",RooArgList(n_etac,n_etac_1))
        #n_etac_rel = RooRealVar(f"n_etac_rel_PT{nPT}_Tz{nTz}","n_etac_rel", "@0/@1",RooArgList(n_etac,n_jpsi))

    n_bkg = RooRealVar(f"n_bkg_PT{nPT}_Tz{nTz}","num of backgr",7e5,10,1.e+9)
    
    rNormJpsi = RooRealVar(f"rNormJpsi_Tz{nTz}", "rNormJpsi", rScJpsi, 0.99, 1.01); #rNormJpsi.setConstant(True)
    #rNormEtac = RooRealVar(f"rNormEtac_Tz{nTz}", "rNormEtac", rScEtac); rNormEtac.setConstant()
    rNormSigma = RooRealVar(f"rNormSigma_PT{nPT}_Tz{nTz}", "rNormSigma", rScSigma); rNormSigma.setConstant(True)
         
    
    mass_jpsi = w.var("mass_jpsi")
    mass_res  = w.var("mass_res")
    

    mass_etac = RooFormulaVar("mass_etac","mean of gaussian","(@0-@1)",RooArgList(mass_jpsi,mass_res)) 

    mass_jpsi_sc = RooFormulaVar(f"mass_jpsi_Tz{nTz}","mean of gaussian", "@0*@1", RooArgList(mass_jpsi,rNormJpsi)) 
    #mass_res_sc = RooFormulaVar(f"mass_res_Tz{nTz}","mean of gaussian", "@0*@1-@2*@3", RooArgList(mass_jpsi,rNormJpsi, mass_res, rNormEtac)) 
    #mass_etac_sc = RooFormulaVar(f"mass_etac_Tz{nTz}","mean of gaussian","(@0-@1)*@2",RooArgList(mass_jpsi,mass_res, rNormEtac)) 
    mass_etac_sc = RooFormulaVar(f"mass_etac_Tz{nTz}","mean of gaussian","(@0-@1)",RooArgList(mass_jpsi_sc,mass_res)) 

    gamma_etac = w.var("gamma_etac")
    gamma_etac.setConstant()
    spin_etac = w.var("spin_etac")
    radius_etac = w.var("radius_etac")
    proton_m = w.var("proton_m")
    
    sigma_etac = w.var("sigma_etac_1")
    sigma_etac.Print()
    rNormSigma.Print()
    sigma_etac_1 = RooFormulaVar(f"sigma_etac_1_PT{nPT}_Tz{nTz}","width of gaussian","@0*@1",RooArgList(sigma_etac,rNormSigma))
    sigma_etac_2 = RooFormulaVar(f"sigma_etac_2_PT{nPT}_Tz{nTz}","width of gaussian","@0/@1",RooArgList(sigma_etac_1,r_NToW))
    
    sigma_jpsi_1 = RooFormulaVar(f"sigma_jpsi_1_PT{nPT}_Tz{nTz}","width of gaussian","@0/@1",RooArgList(sigma_etac_1,r_ref_etac))
    sigma_jpsi_2 = RooFormulaVar(f"sigma_jpsi_2_PT{nPT}_Tz{nTz}","width of gaussian","@0/@1",RooArgList(sigma_jpsi_1,r_NToW))
    
    
    # Fit eta
    gauss_etac_1 = RooGaussian(f"gauss_etac_1_PT{nPT}_Tz{nTz}","gauss_etac_1 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_etac_1)
    gauss_etac_2 = RooGaussian(f"gauss_etac_2_PT{nPT}_Tz{nTz}","gauss_etac_2 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_etac_2)
    
    br_wigner = RooRelBreitWigner(f"br_wigner_Tz{nTz}", "br_wigner",Jpsi_M, mass_etac_sc, gamma_etac, spin_etac,radius_etac,proton_m,proton_m)
    
    bwxg_1 = RooFFTConvPdf(f"bwxg_1_PT{nPT}_Tz{nTz}","breit-wigner (X) gauss", Jpsi_M, br_wigner, gauss_etac_1) 
    bwxg_2 = RooFFTConvPdf(f"bwxg_2_PT{nPT}_Tz{nTz}","breit-wigner (X) gauss", Jpsi_M, br_wigner, gauss_etac_2) 
    
    # Fit J/psi
    gauss_jpsi_1 = RooGaussian(f"gauss_jpsi_1_PT{nPT}_Tz{nTz}","gaussian PDF",Jpsi_M,mass_jpsi_sc,sigma_jpsi_1) 
    gauss_jpsi_2 = RooGaussian(f"gauss_jpsi_2_PT{nPT}_Tz{nTz}","gaussian PDF",Jpsi_M,mass_jpsi_sc,sigma_jpsi_2) 
    
    
    #Create constraints
    #    RooGaussian constrNEta("constrNetac","constraint etac",n_etac_1,f_1,RooConst(0.0)) 
    fconstrgamma = w.pdf("fconstrgamma")
    fconstrmjpsi = w.pdf("fconstrmjpsi")    
    fconstrmres = w.pdf("fconstrmres")    

#    bkg = RooChebychev()
    a0 = RooRealVar(f"a0_PT{nPT}_Tz{nTz}","a0",0.4,-1,2) 
    a1 = RooRealVar(f"a1_PT{nPT}_Tz{nTz}","a1",0.05,-1.5,1.5) 
    a2 = RooRealVar(f"a2_PT{nPT}_Tz{nTz}","a2",-0.005,-1.5,1.5) 
    a3 = RooRealVar(f"a3_PT{nPT}_Tz{nTz}","a3",0.005,-0.1,0.1) 
    a4 = RooRealVar(f"a4_PT{nPT}_Tz{nTz}","a4",0.005,-0.1,0.1) 
    
    if ("Cheb" not in nConf):
        #a0.setRange(0.0, 5.e2)
        a1.setVal(0.04)
        a2.setVal(0.04)
        if nTz!=7:
            bkg = RooGenericPdf(f"bkg_PT{nPT}_Tz{nTz}","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.+@3*(@0-3050.)*(@0-3050.)/200./200.)",RooArgList(Jpsi_M,a0,a1,a2)) 
        else:
            bkg = RooGenericPdf(f"bkg_PT{nPT}_Tz{nTz}","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200)",RooArgList(Jpsi_M,a0,a1)) 
        #bkg = RooGenericPdf(f"bkg_PT{nPT}_Tz{nTz}","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.+@3*(@0-3050.)*(@0-3050.)/200./200.)",RooArgList(Jpsi_M,a0,a1,a2)) 
        #bkg = RooGenericPdf("bkg","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*@0.+@3*(2*@0*@0-1))",RooArgList(Jpsi_M,a0,a1,a2)) 
    elif (nConf == "Chebychev3par"):
        a0.setRange(-1.0, 0.0)
        a1.setRange( 0.0, 1.0)
        a2.setRange(-5.e-1, 0.0)
        bkg = RooChebychev (f"bkg_PT{nPT}_Tz{nTz}","Background",Jpsi_M,RooArgList(a0,a1,a2)) 
        print ("SETUP Chebychev3par: Alternative background: Chebychev with 3 parameters")
    elif (nConf == "Chebychev4par"):
        bkg = RooChebychev(f"bkg_PT{nPT}_Tz{nTz}","Background",Jpsi_M,RooArgList(a0,a1,a2,a3))
        print ("SETUP Chebychev4par: Alternative background: Chebychev with 4 parameters")
        
    
    pppi0 = RooGenericPdf(f"pppi0_PT{nPT}_Tz{nTz}","Jpsi.pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))
    n_pppi0 = RooFormulaVar(f"n_pppi0_PT{nPT}_Tz{nTz}","n_pppi0","@0*@1*1.19/2.12",RooArgList(n_jpsi,eff_pppi0))
    
    
    if (nConf!=3):

        modelBkg = RooAddPdf(f"modelBkg_PT{nPT}_Tz{nTz}","background", RooArgList(bkg,pppi0), RooArgList(n_bkg,n_pppi0))
        modelSignal = RooAddPdf(f"modelSignal_PT{nPT}_Tz{nTz}","signal", RooArgList(bwxg_1, bwxg_2,gauss_jpsi_1,gauss_jpsi_2), RooArgList(n_etac_1,n_etac_2,n_jpsi_1,n_jpsi_2))
        model = RooAddPdf(f"model_PT{nPT}_Tz{nTz}","signal+bkg", RooArgList(bwxg_1, bwxg_2,gauss_jpsi_1,gauss_jpsi_2, bkg,pppi0), RooArgList(n_etac_1,n_etac_2,n_jpsi_1,n_jpsi_2, n_bkg,n_pppi0))
                
        #model_constr = RooProdPdf("model_constr", "model with constraints",RooArgList(model,fconstraint,fconstrmjpsi))
        model_constr = RooProdPdf("model_constr_PT{nPT}_Tz{nTz}", "model with constraints",RooArgList(model,fconstrmres))
        
    else:
        
        # f = TFile(f"{homeDir}/results/MC/MassFit/MC_MassResolution_CB_wksp.root","READ") 
        f = TFile(f"{homeDir}/results/MC/etac/mass_fit/MC_MassResolution_CB_wksp.root","READ") 
        wMC = f.Get("w")
        f.Close()
        
        alpha_etac_1 = RooRealVar('alpha_etac_1','alpha of CB', wMC.var('alpha_etac_1').getValV(), 0.0, 10.) 
        alpha_etac_1.setConstant(True)
        n_etac_1 = RooRealVar('n_etac_1','n of CB', wMC.var('n_etac_1').getValV(), 0.0, 100.) 
        n_etac_1.setConstant(True)    

        # Fit eta
        cb_etac_1 = BifurcatedCB(f"cb_etac_1_PT{nPT}_Tz{nTz}", "Cystal Ball Function", Jpsi_M, RooFit.RooConst(0), sigma_etac_1, alpha_etac_1, n_etac_1, alpha_etac_1, n_etac_1)
        br_wigner = RooRelBreitWigner(f"br_wigner_Tz{nTz}", "br_wigner",Jpsi_M, mass_etac_sc, gamma_etac, spin_etac,radius_etac,proton_m,proton_m)
            
        bwxg_1 = RooFFTConvPdf(f"bwxg_1_PT{nPT}_Tz{nTz}","breit-wigner (X) gauss", Jpsi_M, br_wigner, cb_etac_1) 
        
        # Fit J/psi
        cb_jpsi_1 = BifurcatedCB(f"cb_jpsi_1_PT{nPT}_Tz{nTz}", "Cystal Ball Function", Jpsi_M, mass_jpsi_sc, sigma_jpsi_1, alpha_etac_1, n_etac_1, alpha_etac_1, n_etac_1)

        modelBkg = RooAddPdf(f"modelBkg_PT{nPT}_Tz{nTz}","background", RooArgList(bkg,pppi0), RooArgList(n_bkg,n_pppi0))
        modelSignal = RooAddPdf(f"modelSignal_PT{nPT}_Tz{nTz}","signal", RooArgList(bwxg_1, cb_jpsi_1), RooArgList(n_etac,n_jpsi))
        model = RooAddPdf(f"model_PT{nPT}_Tz{nTz}","signal+bkg", RooArgList(bwxg_1, cb_jpsi_1, bkg,pppi0), RooArgList(n_etac, n_jpsi, n_bkg, n_pppi0))
        
        #model_constr = RooProdPdf("model_constr", "model with constraints",RooArgList(model,fconstrmres,fconstrmjpsi))
        model_constr = RooProdPdf(f"model_constr_PT{nPT}_Tz{nTz}", "model with constraints",RooArgList(model,fconstrmres))
    

    getattr(w,'import')(model, RooFit.RecycleConflictNodes())
    getattr(w,'import')(model_constr, RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelBkg, RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelSignal, RooFit.RecycleConflictNodes())

'''
def fillRelWorkspace(w, nConf, nPT, key):


    Jpsi_M = w.var("Jpsi_M")
    # Jpsi_M.setBins(1000,"cache")

    ratioNtoW,ratioEtaToJpsi,ratioArea, bkgType, gammaEtac, eff, rScSigma = getConstPar(nConf, nPT)


    if (nConf=='halfBinShift'):
        Jpsi_M.setRange(2855,3245)


    rEtaToJpsi = RooRealVar("rEtaToJpsi_{}".format(key),"rEtaToJpsi", ratioEtaToJpsi)
    rNarToW = w.var("rNarToW")
    rG1toG2 = w.var("rG1toG2")
    eff_pppi0 = w.var("eff_pppi0")


    fEtac = w.var("fEta_{}".format(key))
    fJpsi = w.var("fJpsi_{}".format(key))
    if nPT==6:
        fEtac = w.function("fEta_{}".format(key))
        fJpsi = w.function("fJpsi_{}".format(key))


    nEta = w.var("nEta_{}".format(key.replace("PT{}_".format(nPT),"")))
    nEta.Print()
    nJpsi = w.var("nJpsi_{}".format(key.replace("PT{}_".format(nPT),"")))
    nJpsi.Print()
    # nEta = RooRealVar("nEta_{}".format(key),"num of Etac", 10, 1.e7)
    # nJpsi = RooRealVar("nJpsi_{}".format(key),"num of J/Psi", 10, 1.e7)
    # nEtacRel = RooRealVar("nEtacRel_{}".format(key),"num of Etac", 0.0, 3.0)
    #nEtacRel = RooRealVar("nEtacRel_{}".format(key),"nEtacRel", "@0/@1",RooArgList(nEta,nJpsi))
    nEta_PT = RooFormulaVar("nEta_{}".format(key),"num of Etac","@0*@1",RooArgList(nEta,fEtac))
    nJpsi_PT = RooFormulaVar("nJpsi_{}".format(key),"num of Jpsi","@0*@1",RooArgList(nJpsi,fJpsi))

    nEta_1 = RooFormulaVar("nEta_1_{}".format(key),"num of Etac","@0*@1",RooArgList(nEta_PT,rG1toG2))
    nEta_2 = RooFormulaVar("nEta_2_{}".format(key),"num of Etac","@0-@1",RooArgList(nEta_PT,nEta_1))

    nJpsi_1 = RooFormulaVar("nJpsi_1_{}".format(key),"num of Jpsi","@0*@1",RooArgList(nJpsi_PT,rG1toG2))
    nJpsi_2 = RooFormulaVar("nJpsi_2_{}".format(key),"num of Jpsi","@0-@1",RooArgList(nJpsi_PT,nJpsi_1))

    # nEta_1 = RooFormulaVar("nEta_1_{}".format(key),"num of Etac","@0*@1*@2",RooArgList(nEta,fEtac,rG1toG2))
    # nEta_2 = RooFormulaVar("nEta_2_{}".format(key),"num of Etac","@0*@1-@2",RooArgList(nEta,fEtac,nEta_1))

    # nJpsi_1 = RooFormulaVar("nJpsi_1_{}".format(key),"num of Jpsi","@0*@1*@2",RooArgList(nJpsi,fJpsi,rG1toG2))
    # nJpsi_2 = RooFormulaVar("nJpsi_2_{}".format(key),"num of Jpsi","@0*@1-@2",RooArgList(nJpsi,fJpsi,nJpsi_1))

    # nEta_1 = RooFormulaVar("nEta_1_{}".format(key),"num of Etac","@0*@1*@2",RooArgList(nEtacRel,nJpsi,rG1toG2))
    # nEta_2 = RooFormulaVar("nEta_2_{}".format(key),"num of Etac","@0*@1-@2",RooArgList(nEtacRel,nJpsi,nEta_1))
    # nEta_1 = RooFormulaVar("nEta_1_{}".format(key),"num of Etac","@0*@1",RooArgList(nEta,rG1toG2))
    # nEta_2 = RooFormulaVar("nEta_2_{}".format(key),"num of Etac","@0-@1",RooArgList(nEta,nEta_1))
    # nJpsi_1 = RooFormulaVar("nJpsi_1_{}".format(key),"num of J/Psi","@0*@1",RooArgList(nJpsi,rG1toG2))
    # nJpsi_2 = RooFormulaVar("nJpsi_2_{}".format(key),"num of J/Psi","@0-@1",RooArgList(nJpsi,nJpsi_1))
    nBckgr = RooRealVar("nBckgr_{}".format(key),"num of backgr",7e7,10,1.e+9)


    mass_Jpsi = w.var("mass_Jpsi")
    mass_res = w.var("mass_res")
    gamma_eta = w.var("gamma_eta")
    # mass_Jpsi = RooRealVar("mass_Jpsi","mean of gaussian", 3096.9, 3030, 3150)
    # mass_res = RooRealVar("mass_res","mean of gaussian",mDiffEtac, 80, 130)

    if (nConf=='constDM' or nConf==8):  mass_res.setConstant(True)

    mass_eta = w.function("mass_eta")
    # gamma_eta = RooRealVar("gamma_eta","width of Br-W", gammaEtac, 10., 50.)
    gamma_eta.setVal(gammaEtac)
    gamma_eta.setConstant()

    spin_eta = w.var("spin_eta")
    radius_eta = w.var("radius_eta")
    proton_m = w.var("proton_m")

    rNormSigma = RooRealVar("rNormSigma_PT{}".format(nPT), "rNormSigma", rScSigma, 0.7, 1.3); rNormSigma.setConstant(True)
    sigma_eta = w.var("sigma_eta")
    print(rNormSigma.getValV())
    # sigma_eta_1 = RooRealVar("sigma_eta_1_PT{}".format(nPT),"width of gaussian", 9., 5., 50.)
    sigma_eta_1 = RooFormulaVar("sigma_eta_1_PT{}".format(nPT),"width of gaussian","@0*@1",RooArgList(sigma_eta,rNormSigma))
    sigma_eta_2 = RooFormulaVar("sigma_eta_2_PT{}".format(nPT),"width of gaussian","@0/@1",RooArgList(sigma_eta_1,rNarToW))

    sigma_Jpsi_1 = RooFormulaVar("sigma_Jpsi_1_PT{}".format(nPT),"width of gaussian","@0/@1",RooArgList(sigma_eta_1,rEtaToJpsi))
    sigma_Jpsi_2 = RooFormulaVar("sigma_Jpsi_2_PT{}".format(nPT),"width of gaussian","@0/@1",RooArgList(sigma_Jpsi_1,rNarToW))


    # Fit eta
    gaussEta_1 = RooGaussian("gaussEta_1_PT{}".format(nPT),"gaussEta_1 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_eta_1)
    gaussEta_2 = RooGaussian("gaussEta_2_PT{}".format(nPT),"gaussEta_2 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_eta_2)

    br_wigner = w.pdf("br_wigner")
    # br_wigner = RooRelBreitWigner("br_wigner", "br_wigner",Jpsi_M, mass_eta, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)

    bwxg_1 = RooFFTConvPdf("bwxg_1_PT{}".format(nPT),"breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_1)
    bwxg_2 = RooFFTConvPdf("bwxg_2_PT{}".format(nPT),"breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_2)

    # Fit J/psi
    gauss_1 = RooGaussian("gauss_1_PT{}".format(nPT),"gaussian PDF",Jpsi_M, mass_Jpsi, sigma_Jpsi_1)
    gauss_2 = RooGaussian("gauss_2_PT{}".format(nPT),"gaussian PDF",Jpsi_M, mass_Jpsi, sigma_Jpsi_2)


    if (bkgType == 'Base'):
        a0 = RooRealVar("a0_{}".format(key),"a0",0.4,0.,5.)
    else:
        a0 = RooRealVar("a0_{}".format(key),"a0",0.4,-1.,1.)

    a1 = RooRealVar("a1_{}".format(key),"a1",0.05,-1.,1.)
    a2 = RooRealVar("a2_{}".format(key),"a2",-0.005,-1.,1.)
    a3 = RooRealVar("a3_{}".format(key),"a3",0.005,-0.1,0.1)
    a4 = RooRealVar("a4_{}".format(key),"a4",0.005,-0.1,0.1)


    if (bkgType == 'Base'):
        #a0.setRange(0.0, 5.e2)
        a1.setVal(0.04)
        a2.setVal(0.04)
        bkg = RooGenericPdf("bkg_{}".format(key),"background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.+@3*(@0-3050.)*(@0-3050.)/200./200.)",RooArgList(Jpsi_M,a0,a1,a2))
        #bkg = RooGenericPdf("bkg","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*@0.+@3*(2*@0*@0-1))",RooArgList(Jpsi_M,a0,a1,a2))
    elif (bkgType == 'Chebychev3par'):
        bkg = RooChebychev ("bkg_{}".format(key),"Background",Jpsi_M,RooArgList(a0,a1,a2))
    elif (bkgType == 'Chebychev4par'):
        bkg = RooChebychev("bkg_{}".format(key),"Background",Jpsi_M,RooArgList(a0,a1,a2,a3))


    pppi0 = RooGenericPdf("pppi0","Jpsi2pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))
    nPPPi0 = RooFormulaVar("nPPPi0_{}".format(key),"nPPPi0","@0*@1",RooArgList(nJpsi,eff_pppi0))


    modelBkg = RooAddPdf("modelBkg_{}".format(key),"background", RooArgList(bkg,pppi0), RooArgList(nBckgr,nPPPi0))


    if(nConf=='CB'):
        #f = TFile(homeDir+"/MC/MassFit/MC_MassResolution_CB_2016_wksp.root","READ")
        f = TFile(homeDir+"/MC/MassFit/MC_MassResolution_CB_wksp.root","READ")
        wMC = f.Get("w")
        f.Close()
        alpha_eta_1 = RooRealVar('alpha_eta_1','alpha of CB', wMC.var('alpha_eta_1').getValV(), 0.0, 10.)
        alpha_eta_1.setConstant(True)
        n_eta_1 = RooRealVar('n_eta_1','n of CB', wMC.var('n_eta_1').getValV(), 0.0, 100.)
        n_eta_1.setConstant(True)
        # Fit eta
        cb_etac_1 = BifurcatedCB("cb_etac_1_PT{}".format(nPT), "Cystal Ball Function", Jpsi_M, RooFit.RooConst(0), sigma_eta_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)
        bwxg_1 = RooFFTConvPdf("bwxg_1_PT{}".format(nPT),"breit-wigner (X) gauss", Jpsi_M, br_wigner, cb_etac_1)
        # Fit J/psi
        cb_Jpsi_1 = BifurcatedCB("cb_Jpsi_1_PT{}".format(nPT), "Cystal Ball Function", Jpsi_M, mass_Jpsi, sigma_Jpsi_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)
        modelSignal = RooAddPdf("modelSignal_{}".format(key),"signal", RooArgList(bwxg_1,cb_Jpsi_1), RooArgList(nEta,nJpsi))
        model = RooAddPdf("model_{}".format(key),"signal+bkg", RooArgList(bwxg_1,cb_Jpsi_1, bkg,pppi0), RooArgList(nEta,nJpsi, nBckgr,nPPPi0))
    else:
        modelSignal = RooAddPdf("modelSignal_{}".format(key),"signal", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2), RooArgList(nEta_1,nEta_2,nJpsi_1,nJpsi_2))
        model = RooAddPdf("model_{}".format(key),"signal+bkg", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2, bkg,pppi0), RooArgList(nEta_1,nEta_2,nJpsi_1,nJpsi_2, nBckgr,nPPPi0))


    #modelC = RooProdPdf("modelC", "model with constraints",RooArgList(model,fconstraint,fconstrJpsi))
    #modelC = RooProdPdf("modelC_{}".format(key), "model with constraints",RooArgList(model,fconstraint))

    getattr(w,'import')(model, RooFit.RecycleConflictNodes())
    #getattr(w,'import')(modelC, RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelBkg, RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelSignal, RooFit.RecycleConflictNodes())
'''

def combineTz(w:RooWorkspace, nPT:int=0, nTzs:list=[0], nConf:str="Base", sel="all", state="Jpsi"):

    setCommonPar(w, nConf)    
    hists = []    
    model_pts = []
    Jpsi_M = w.var("Jpsi_M")
    Jpsi_M.setBins(nBins)

    #if (nConf!=10):
        #getData_h(w,keyPrompt, nPT, nTz, state)
    #else:
        #getData_h(w,keyPrompt, nPT, nTz, state, True)

    for nTz in nTzs:

        print (f"t_z bin: {nTz}")
        fillWorkspace(w, nConf, nPT, nTz)
        # fillRelWorkspace(w,nConf,nPT, nTz)
        #if nTz==1:
            #w.var("rNormSigma_Tz%s"%nTz).setRange(0.7, 2.3)
        #elif nTz==7:    
            #w.var("rNormSigma_Tz%s"%nTz).setVal(1.0)
        # getData_h(w, sel, nPT, nTz, state)
        getDataHist(w, sel, nPT, nTz)
        histo = w.data(f"dh_PT{nPT}_Tz{nTz}")
        hists.append(histo)

    
    sample = RooCategory("sample","sample") 

    for nTz in nTzs:
        sample.defineType(f"PT{nPT}_Tz{nTz}") 
        model_pts.append(w.pdf(f"model_PT{nPT}_Tz{nTz}"))


    # map_ds = std.map("std::string, RooDataHist*")()
    map_dh = std.map("std::string, RooDataHist*")()
    pdfs   = RooArgSet()
    yields = RooArgSet()

    simPdf = RooSimultaneous("simPdf","",sample)

    for nTz in nTzs:
        # print(f"Tz bin: {nTz}")

        model = model_pts[nTz-1]
        name = f"PT{nPT}_Tz{nTz}"
        simPdf.addPdf(model,name)

        data = hists[nTz-1]
        data.Print()
        map_dh.insert(map_dh.cbegin(), std.pair("const string,RooDataHist*")(name, data))

    combData = RooDataHist("combData", "combined data", RooArgSet(Jpsi_M), RooFit.Index(sample), RooFit.Import(map_dh))   
    
    w.Import(sample, RooFit.RecycleConflictNodes())
    w.Import(combData, RooFit.RecycleConflictNodes())
    w.Import(simPdf, RooFit.RecycleConflictNodes())

    return hists, model_pts

def createModel(w:RooWorkspace, nPTs:list=[0], nTzs:list=[0], nConf:str="Base", sel="all", state="Jpsi"):

    setCommonParameters(w,nConf, nBins)
    Jpsi_M = w.var("Jpsi_M")

    sample = RooCategory("sample","sample") 
    hists = []    
    model_pts = []
    modelSignal_pts = []
    samples = []
    params = []
    #-----------------------------------------------------
    for nPT in nPTs:
        hists.append([])
        model_pts.append([])
        modelSignal_pts.append([])
        samples.append(RooCategory("samplePT","sample") )
        params.append([])
        #-----------------------------------------------------
        for nTz in nTzs:

            getDataHist(w, sel, nPT, nTz, state)
            fillWorkspace(w,nConf,nPT, nTz)

            idx = nPTs.index(nPT)
            histo   = w.data(f"dh_PT{nPT}_Tz{nTz}")
            hists[idx].append(histo)

            idx = nPTs.index(nPT)
            idTz = nTzs.index(nTz)
            samples[idx].defineType(f"PT{nPT}_Tz{nTz}") 
            model_pts[idx].append(w.pdf(f"model_PT{nPT}_Tz{nTz}"))
            modelSignal_pts[idx].append(w.pdf(f"modelSignal_PT{nPT}_Tz{nTz}"))
            #-----------------------------------------------------
            params[idx].append(w.pdf(f"modelSignal_PT{nPT}_Tz{nTz}").getParameters(RooArgSet(Jpsi_M)))
            w.defineSet(f"parameters_PT{nPT}_Tz{nTz}",params[idx][idTz])

        # sample.defineType(f"PT{nPT}")

    simPdf = RooSimultaneous("smodel","",sample)
    map_dh = std.map("std::string, RooDataHist*")()
    # map_dh = mp("std::string, RooDataHist*")()

    for nPT in nPTs:
        for nTz in nTzs:
            # print(f"PT bin: {nPT}, Tz bin: {nTz}")

            #ccbar = Particle(name)
            #Jpsi_M.setRange("fitRange_{}".format(name), ccbar.mass-m_win, ccbar.mass+m_win)

            model = model_pts[nPT-1][nTz-1]
            name = f"PT{nPT}_Tz{nTz}"
            simPdf.addPdf(model,name)

            data = hists[nPT-1][nTz-1]
            data.Print()
            map_dh.insert(map_dh.cbegin(), std.pair("const string,RooDataHist*")(name, data))


    combData = RooDataHist("combData", "combined data", RooArgSet(Jpsi_M), RooFit.Index(sample), RooFit.Import(map_dh))   

    w.Import(sample, RooFit.RecycleConflictNodes())
    w.Import(combData, RooFit.RecycleConflictNodes())
    w.Import(simPdf, RooFit.RecycleConflictNodes())


def create_sample(w, nBins, nConf):

    Jpsi_M = w.var("Jpsi_M")
    setCommonParameters(w,nConf, nBins)
    
    sample = RooCategory("sample","sample")

    simPdf = RooSimultaneous("simPdf","simultaneous signal pdf",sample)

    datas = []
    map_dh = ROOT.std.map("std::string, RooDataHist*")()

    for iBin in range(1, nBins+1):

        keyPrompt =  "PT{}_Prompt".format(iBin)
        keyFromB = "PT{}_FromB".format(iBin)

        sample.defineType(keyPrompt)
        sample.defineType(keyFromB)

        fillRelWorkspace(w, nConf, iBin, keyPrompt)
        fillRelWorkspace(w, nConf, iBin, keyFromB)

        print("INFO: model PT{} created".format(iBin))

        model_Prompt = w.pdf("model_{}".format(keyPrompt))
        model_FromB  = w.pdf("model_{}".format(keyFromB))

        simPdf.addPdf(model_Prompt,keyPrompt)
        simPdf.addPdf(model_FromB,keyFromB)

        print("INFO: model PT{} imported to simPdf".format(iBin))

        hist_Prompt = w.data("dh_{}".format(keyPrompt))
        hist_FromB = w.data("dh_{}".format(keyFromB))

        print("INFO: hist PT{} extracted from w".format(iBin))

        map_dh.insert(map_dh.cbegin(), ROOT.std.pair("const string,RooDataHist*")(keyPrompt, hist_Prompt))
        map_dh.insert(map_dh.cbegin(), ROOT.std.pair("const string,RooDataHist*")(keyFromB, hist_FromB))

        print("INFO: data PT{} imported to simPdf".format(iBin))

    combData = RooDataHist("combData", "combined data", RooArgList(Jpsi_M), sample, map_dh)

    getattr(w,"import")(sample, RooFit.RecycleConflictNodes())
    getattr(w,"import")(combData, RooFit.RecycleConflictNodes())
    getattr(w,"import")(simPdf, RooFit.RecycleConflictNodes())

