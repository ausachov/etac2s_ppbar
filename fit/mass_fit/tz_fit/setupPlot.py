from ROOT import gROOT, gStyle, TLatex, TCanvas, TGaxis, TFile, \
    RooFit, RooArgSet, RooAbsReal

histosDir = "Histos/"
homeDir = "/users/LHCb/zhovkovska/scripts/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"
localDir = "/users/LHCb/zhovkovska/etac2s_ppbar/"

gROOT.LoadMacro(f"{localDir}/fit/libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro(f"{localDir}/fit/libs/libBifurcatedCB/BifurcatedCB.cxx+")
from ROOT import RooRelBreitWigner, BifurcatedCB

import logging
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

binningDict = {
    "Jpsi_PT":                      [5000, 6500, 8000, 10000, 12000, 14000, 20000],
    "Jpsi_ETA":                     [2.0, 2.45, 2.8, 3.15, 4.5],
    "TMath::Abs(ProtonM_CosTheta)": [0.0, 0.15, 0.3, 0.50, 1.0],
    "nSPDHits":                     [0, 100, 150, 200, 250, 300],
    #"Jpsi_Tz":                      [-10., -0.125, -0.025, 0.0, 0.20, 2.0, 4.0, 10.0],
    "Jpsi_Tz":                      [-10., -0.125, -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10], #[-10., -0.15, -0.025, 0.0, 0.20, 2.0, 10.0],
    "Jpsi_Tz_Alt":                  [-10.0, -0.15, -0.05, 0.,        0.100,   4., 10.],
    "Jpsi_Tz_old":                  [-10., -0.025, 0.0, 0.20, 2.0, 10.0],
    "Jpsi_Tz_Tot":                  [-10., -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10],
}


minM = 2850
maxM = 3250
nBins = 1000

plot_binWidth = 10.
plot_binN = int((maxM-minM)/plot_binWidth)

binning     = RooFit.Binning(plot_binN,minM,maxM)
mrkSize     = RooFit.MarkerStyle(7)
lineWidth1  = RooFit.LineWidth(1)
lineWidth2  = RooFit.LineWidth(2)
lineStyle   = RooFit.LineStyle(2)
lineColor1  = RooFit.LineColor(6)
lineColor2  = RooFit.LineColor(8)
name        = RooFit.Name

def set_frame(w, nPTs, nTzs):

    Jpsi_M = w.var("Jpsi_M")
    
    sample = w.cat("sample")
    simPdf = w.pdf("simPdf")
    combData = w.data("combData")            
            
    frames = []
    pulls = []
    resids = []
    Jpsi_M.setBins(nBins)
    for idx, nPT in enumerate(nPTs):
        frames.append([])
        pulls.append([])
        resids.append([])
        for idxTz, nTz in enumerate(nTzs):

            frame = Jpsi_M.frame(RooFit.Title(f"PT bin {nPT}, Tz bin {nTz}"))
            combData.plotOn(frame, RooFit.Cut(f"sample==sample::PT{nPT}_Tz{nTz}"), binning, mrkSize, name(f"dataPT{nPT}Tz{nTz}"))
            logging.info(f" Creating frame for PT: {nPT}, t_z: {nTz}")
            simPdf.plotOn(frame,RooFit.Slice(sample,f"PT{nPT}_Tz{nTz}"),RooFit.ProjWData(RooArgSet(sample),combData,True),name(f"pdfPT{nPT}Tz{nTz}"))
            bkg = w.pdf(f"bkg_PT{nPT}_Tz{nTz}")
            bkg.plotOn(frame, lineColor2, lineWidth1, lineStyle, name(f"pdf_bkg_PT{nPT}Tz{nTz}"), RooFit.Normalization(w.var(f"n_bkg_PT{nPT}_Tz{nTz}").getValV(), RooAbsReal.NumEvent))
            #modelBkg = w.pdf(f"modelBkg_PT{nPT}_Tz{nTz}")
            #modelBkg.plotOn(frame, lineColor2, lineWidth1, lineStyle, name(f"pdf_bg_PT{nPT}Tz{nTz}"), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
            modelSignal = w.pdf(f"modelSignal_PT{nPT}_Tz{nTz}")
            modelSignal.plotOn(frame, lineColor1, lineWidth2, name(f"pdf_signal_PT{nPT}Tz{nTz}"), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))

            frame.GetXaxis().SetTitle("M_{p#bar{p}}, MeV/c^{2}")  
            frame.GetYaxis().SetTitle("Candidates / ( 10 MeV )")  
            frame.GetXaxis().SetTitleSize(0.06)
            frame.GetYaxis().SetTitleSize(0.06)
            frame.GetXaxis().SetTitleOffset(0.9)
            frame.GetYaxis().SetTitleOffset(1.20)

            
            hresid = frame.residHist(f"dataPT{nPT}Tz{nTz}",f"pdf_bkg_PT{nPT}Tz{nTz}")
            hresid.SetMarkerSize(0.5)
            frame_res = Jpsi_M.frame(RooFit.Bins(100)) 
            frame_res.addPlotable(hresid,"P") 
            pppi0 = w.pdf(f"pppi0_PT{nPT}_Tz{nTz}")
            nPPPi0 = w.function(f"n_pppi0_PT{nPT}_Tz{nTz}").getValV() 
            pppi0.plotOn(frame_res, lineColor2, lineWidth2, RooFit.Normalization(nPPPi0, RooAbsReal.NumEvent))
            modelSignal.plotOn(frame_res, lineColor1, lineWidth2, RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
            frame_res.GetXaxis().SetTitle("M_{p#bar{p}}, MeV/c^{2}")  
            frame_res.GetXaxis().SetTitleSize(0.00)
            frame_res.GetYaxis().SetTitle("Candidates / ( 10 MeV )")  
            frame_res.GetYaxis().SetTitleSize(0.00)
            frame_res.GetXaxis().SetLabelSize(0.06)
            frame_res.GetYaxis().SetLabelSize(0.06)
            frame_res.GetXaxis().SetNdivisions(505)
            frame_res.GetYaxis().SetNdivisions(505)
            #frame_res.GetXaxis().SetTitleOffset(0.02)
            
            hpull = frame.pullHist(f"dataPT{nPT}Tz{nTz}",f"pdfPT{nPT}Tz{nTz}",True)
            hpull.SetFillColor(1)
            hpull.SetMarkerSize(0.5)
            frame_pull = Jpsi_M.frame()
            for ii in range(hpull.GetN()):
                hpull.SetPointEYlow(ii,0)
                hpull.SetPointEYhigh(ii,0)
            frame_pull.addPlotable(hpull,"B")
            frame_pull.SetMinimum(-4.0)
            frame_pull.SetMaximum(4.0)
            frame_pull.GetXaxis().SetTitleSize(0.0)
            frame_pull.GetXaxis().SetLabelSize(0.06)
            frame_pull.GetYaxis().SetLabelSize(0.06)
            frame_pull.GetXaxis().SetNdivisions(505)
            frame_pull.GetYaxis().SetNdivisions(505)
            
            frames[idx].append(frame)
            pulls[idx].append(frame_pull)
            resids[idx].append(frame_res)

    return frames, resids, pulls

def set_canvas(nPTs, nTzs, frames, resids, pulls):
    
    gStyle.SetOptTitle(0)
    texData = TLatex()        
    texData.SetNDC()
    texData.SetTextSize(0.06)
    TGaxis.SetMaxDigits(4)

    nPTbins = len(nPTs)
    nTzbins = len(nTzs)
 
    c = []
    for nPT in range(nPTbins):
        c.append(TCanvas("c%s"%nPT,"c",1600,800))
        c[nPT].Divide(nTzbins, 3, 0.001, 0.001)
        for i in range(nTzbins):
            pad = c[nPT].cd(i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            pad.SetPad(xl,yl-0.2,xh,yh)
            pad.SetBottomMargin(0.15)
            pad.SetLeftMargin(0.15)
            frames[nPT][i].Draw()

            tzL = binningDict["Jpsi_Tz"][i]
            tzR = binningDict["Jpsi_Tz"][i+1]
            texData.DrawLatex(0.3, 0.95, "%s<t_{z}<%s"%(tzL,tzR))
            #ptL = binningDict["Jpsi_PT"][nPT]
            #ptR = binningDict["Jpsi_PT"][nPT+1]
            #texData.DrawLatex(0.3, 0.95, "%s<p_{T}<%s"%(ptL,ptR))

            pad = c[nPT].cd(nTzbins+i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            pad.SetPad(xl,yl-0.1,xh,yh-0.2)
            pad.SetBottomMargin(0.15)
            pad.SetLeftMargin(0.15)
            #pad.SetTopMargin(0)
            #pad.SetTickx(2)
            #pad.SetTicky(2)
            resids[nPT][i].Draw()


            pad = c[nPT].cd(2*nTzbins+i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            #pad.SetTickx(2)
            #pad.SetTicky(2)
            pad.SetPad(xl,yl,xh,yh-0.1)
            pad.SetLeftMargin(0.15)
            pulls[nPT][i].Draw()

    return c

def Draw(w, combData, sample, simPdf, nPTs, nTzs, nConf, sel="all", state="Etac"):
    
    Jpsi_M = w.var("Jpsi_M")
    
    nPTbins = len(nPTs)
    nTzbins = len(nTzs)

    #sample = w.cat("sample")
    #simPdf = w.pdf("simPdf")
    #combData = w.data("combData")

    plot_binWidth = 10.
    plot_binN = int((maxM-minM)/plot_binWidth)

    binning = RooFit.Binning(plot_binN,minM,maxM)
    mrkSize = RooFit.MarkerStyle(7)
    lineWidth1 = RooFit.LineWidth(1)
    lineWidth2 = RooFit.LineWidth(2)
    lineStyle = RooFit.LineStyle(2)
    lineColor1 = RooFit.LineColor(6)
    lineColor2 = RooFit.LineColor(8)
    name = RooFit.Name
            
            
    frames = []
    pulls = []
    resids = []
    Jpsi_M.setBins(nBins)
    for nPT in nPTs:
        frames.append([])
        pulls.append([])
        resids.append([])
        idx = nPTs.index(nPT)
        for nTz in nTzs:
            idxTz = nTzs.index(nTz)
            frame = Jpsi_M.frame(RooFit.Title("PT bin %s, Tz bin %s"%(nPT,nTz)))
            combData.plotOn(frame, RooFit.Cut(f"sample==sample::PT{nPT}_Tz{nTz}"), binning, mrkSize, name(f"dataPT{nPT}Tz{nTz}"))
            print(f"PT: {nPT}, t_z: {nTz}")
            simPdf.plotOn(frame,RooFit.Slice(sample,"PT{nPT}_Tz{nTz}"),RooFit.ProjWData(RooArgSet(sample),combData,True),name(f"pdfPT{nPT}Tz{nTz}"))
            bkg = w.pdf(f"bkg_PT{nPT}_Tz{nTz}")
            bkg.plotOn(frame, lineColor2, lineWidth1, lineStyle, name(f"pdf_bkg_PT{nPT}Tz{nTz}"), RooFit.Normalization(w.var(f"n_bkg_PT{nPT}_Tz{nTz}").getValV(), RooAbsReal.NumEvent))
            #modelBkg = w.pdf(f"modelBkg_PT{nPT}_Tz{nTz}")
            #modelBkg.plotOn(frame, lineColor2, lineWidth1, lineStyle, name(f"pdf_bg_PT{nPT}Tz{nTz}"), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
            modelSignal = w.pdf(f"modelSignal_PT{nPT}_Tz{nTz}")
            modelSignal.plotOn(frame, lineColor1, lineWidth2, name(f"pdf_signal_PT{nPT}Tz{nTz}"), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
            frame.GetXaxis().SetTitle("M_{p#bar{p}}, MeV/c^{2}")  
            frame.GetYaxis().SetTitle("Candidates / ( 10 MeV )")  
            frame.GetXaxis().SetTitleSize(0.06)
            frame.GetYaxis().SetTitleSize(0.06)
            frame.GetXaxis().SetTitleOffset(0.9)
            frame.GetYaxis().SetTitleOffset(1.20)

            
            hresid = frame.residHist(f"dataPT{nPT}Tz{nTz}",f"pdf_bkg_PT{nPT}Tz{nTz}")
            hresid.SetMarkerSize(0.5)
            #hresid = frame.residHist()
            frame_res = Jpsi_M.frame(RooFit.Bins(100)) 
            frame_res.addPlotable(hresid,"P") 
            pppi0 = w.pdf(f"pppi0_PT{nPT}_Tz{nTz}")
            nPPPi0 = w.function(f"n_pppi0_PT{nPT}_Tz{nTz}").getValV() 
            pppi0.plotOn(frame_res, lineColor2, lineWidth2, RooFit.Normalization(nPPPi0, RooAbsReal.NumEvent))
            modelSignal.plotOn(frame_res, lineColor1, lineWidth2, RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
            frame_res.GetXaxis().SetTitle("M_{p#bar{p}}, MeV/c^{2}")  
            frame_res.GetXaxis().SetTitleSize(0.00)
            frame_res.GetYaxis().SetTitle("Candidates / ( 10 MeV )")  
            frame_res.GetYaxis().SetTitleSize(0.00)
            frame_res.GetXaxis().SetLabelSize(0.06)
            frame_res.GetYaxis().SetLabelSize(0.06)
            frame_res.GetXaxis().SetNdivisions(505)
            frame_res.GetYaxis().SetNdivisions(505)
            #frame_res.GetXaxis().SetTitleOffset(0.02)
            
            hpull = frame.pullHist(f"dataPT{nPT}Tz{nTz}",f"pdfPT{nPT}Tz{nTz}",True)
            hpull.SetFillColor(1)
            hpull.SetMarkerSize(0.5)
            frame_pull = Jpsi_M.frame()
            for ii in range(hpull.GetN()):
                hpull.SetPointEYlow(ii,0)
                hpull.SetPointEYhigh(ii,0)
            frame_pull.addPlotable(hpull,"B")
            frame_pull.SetMinimum(-4.0)
            frame_pull.SetMaximum(4.0)
            frame_pull.GetXaxis().SetTitleSize(0.0)
            frame_pull.GetXaxis().SetLabelSize(0.06)
            frame_pull.GetYaxis().SetLabelSize(0.06)
            frame_pull.GetXaxis().SetNdivisions(505)
            frame_pull.GetYaxis().SetNdivisions(505)
            
            frames[idx].append(frame)
            pulls[idx].append(frame_pull)
            resids[idx].append(frame_res)


    inCutKey=sel
    # if keyPrompt: inCutKey = "prompt"
    # else:         inCutKey = "fromB"
    
    #if nPT!=0:    binPT = "Jpsi_PT/"
    #else:         binPT = "Total/"

    # binPT = "Sim/"
    binPT = "Sim_oldPT/"
    #binTz = "Tz%s.root"%(nTz)
    
    # dirName = homeDir+"results/mass_fit/" + inCutKey + "/" + binPT + "m_scaled/"
    dirName = f"{homeDir}/results/mass_fit/etac_tz/{inCutKey}/{binPT}/"
    
    #if (nPT!=0 and nTz!=0):
        #dirName = dirName + state + "/"
    if nPTs == [0]:
        ptNum = ""
    else:
        ptNum = "_PT6"
    
    gStyle.SetOptTitle(0)
    texData = TLatex()        
    texData.SetNDC()
    texData.SetTextSize(0.06)
    TGaxis.SetMaxDigits(4)

    nameRoot = f"{dirName}/{state}_Plot_M_PT{nPT}_C{nConf}_total.root"
    nameWksp = f"{dirName}/{state}_Wksp_M_PT{nPT}_C{nConf}_total.root"
    namePic  = f"{dirName}/{state}_Plot_M_PT{nPT}_C{nConf}_total.pdf"
    # nameTxt  = f"{dirName}/{state}_Result_M_{inCutKey}_C{nConf}{ptNum}.txt"
    # nameRoot = f"{dirName}/{state}_Plot_M_{inCutKey}_C{nConf}{ptNum}.root"
    # nameWksp = f"{dirName}/{state}_Wksp_M_{inCutKey}_C{nConf}{ptNum}.root"
    
    w.writeToFile(nameWksp)

    fFit = TFile (nameRoot,"RECREATE")
    c = []
    for nPT in range(nPTbins):
        c.append(TCanvas("c%s"%nPT,"c",1600,800))
        c[nPT].Divide(nTzbins,3, 0.001, 0.001)
        for i in range(nTzbins):
            pad = c[nPT].cd(i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            pad.SetPad(xl,yl-0.2,xh,yh)
            pad.SetBottomMargin(0.15)
            pad.SetLeftMargin(0.15)
            frames[nPT][i].Draw()

            tzL = binningDict["Jpsi_Tz"][i]
            tzR = binningDict["Jpsi_Tz"][i+1]
            texData.DrawLatex(0.3, 0.95, "%s<t_{z}<%s"%(tzL,tzR))
            #ptL = binningDict["Jpsi_PT"][nPT]
            #ptR = binningDict["Jpsi_PT"][nPT+1]
            #texData.DrawLatex(0.3, 0.95, "%s<p_{T}<%s"%(ptL,ptR))

            pad = c[nPT].cd(nTzbins+i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            pad.SetPad(xl,yl-0.1,xh,yh-0.2)
            pad.SetBottomMargin(0.15)
            pad.SetLeftMargin(0.15)
            #pad.SetTopMargin(0)
            #pad.SetTickx(2)
            #pad.SetTicky(2)
            resids[nPT][i].Draw()


            pad = c[nPT].cd(2*nTzbins+i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            #pad.SetTickx(2)
            #pad.SetTicky(2)
            pad.SetPad(xl,yl,xh,yh-0.1)
            pad.SetLeftMargin(0.15)
            pulls[nPT][i].Draw()

        namePlot = f"{dirName}/{state}_Plot_M_{inCutKey}_PT{nPTs[nPT]}_C{nConf}{ptNum}.pdf"
        c[nPT].SaveAs(namePlot)    
        c[nPT].Write("")

    fFit.Close()

    #import os, sys 
    #save = os.dup( sys.stdout.fileno() ) 
    #newout = file(nameTxt, 'w' ) 
    #os.dup2( newout.fileno(), sys.stdout.fileno() ) 
    #r.Print("v") 
    ##print w.var("rNormJpsi_Tz1").getValV(), "\n"
    ##print w.var("rNormJpsi_Tz2").getValV(), "\n"
    ##print w.var("rNormJpsi_Tz3").getValV(), "\n"
    ##print w.var("rNormJpsi_Tz4").getValV(), "\n"
    ##print w.var("rNormJpsi_Tz5").getValV(), "\n"
    #r.correlationMatrix().Print()
    #os.dup2( save, sys.stdout.fileno() ) 
    #newout.close()
        
    
    #w.Print("v")
