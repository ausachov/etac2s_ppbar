from array import array
from ROOT import gROOT, TH1D, TFile, TCanvas, RooWorkspace

oniaDict = {
    "Jpsi":     0,
    "Etac":     1,
}

homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/mass_fit/etac_tz/"
localDir = "/users/LHCb/zhovkovska/etac2s_ppbar/"

gROOT.LoadMacro(f"{localDir}/fit/libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
from ROOT import RooRelBreitWigner


def yields(nPT:int=0, nConf:str="Base", keySel:str="all"):
    #gROOT.Reset()

    #nBinsEtac = 5
    #nBinsJpsi = 7
    nBinsEtac = 11
    nBinsJpsi = 11

    yJpsi = array("d", 15*[0])
    yEtac = array("d", 15*[0])
    
    yJpsiErr = array("d", 15*[0])
    yEtacErr = array("d", 15*[0])

    tzTotalBinning =  array("d",[-10., -0.125, -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10])
    tzEtacBinsBinning = array("d", [-10., -0.025, 0.,         0.200,  2., 10.])
    #tzJpsiBinsBinning = array("d", [-10., -0.025, 0.,         0.200,  2., 10.])
    tzJpsiBinsBinning = array("d", [-10., -0.025, 0., 0.025, 0.1, 1., 4., 10.])
        
    # if keyPrompt: inCutKey = "prompt"
    # else:         inCutKey = "fromB"
    
    inCutKey = keySel

    if nPT!=0:    binPT = "Jpsi_PT/"
    else:         binPT = "Total/"
    
    binPT = "Sim_oldPT/"

    dirName = f"{homeDir}/{inCutKey}/{binPT}"        
    
    
    #nameTxt  = dirName + "/fitRes_PT%s_Tz%d_C%d.txt"%(nPT,iTz,nConf)
    #nameRoot = dirName + "/Jpsi_MassFit_PT%s_Tz%d_C%d.root"%(nPT,iTz,nConf)
    #namePic  = dirName + "/Jpsi_MassFit_PT%s_Tz%d_C%d.pdf"%(nPT,iTz,nConf)
    w = RooWorkspace("w", True)
    
    hhJpsi = TH1D()
    hhEtac = TH1D()
    hhJpsi.SetTitle("yield")
    hhEtac.SetTitle("yield")

    
    if nPT == 0:
        fo = open(f"{homeDir}/signal/signal_{nConf}_oldPID_oldPT.txt", "w")
        fo_sign = open(f"{homeDir}/signal/signif/signif_Mass_{nConf}_oldPID_oldPT.txt", "w")

        hhJpsi.SetBins(len(tzTotalBinning)-1, tzTotalBinning)
        hhEtac.SetBins(len(tzTotalBinning)-1, tzTotalBinning)

        fo.write( str(len(tzTotalBinning)-1) )
        fo.write( "\n" )
        fo_sign.write( str(len(tzTotalBinning)-1) )
        fo_sign.write( "\n" )

        nameWksp = f"{dirName}/Jpsi_Wksp_M_PT{nPT}_C{nConf}_total_oldPID.root"
        f1 = TFile(nameWksp,"READ")
        w = f1.Get("w") 
        for iTz in range(len(tzTotalBinning)-1):
            
            jpsi_varname = f"n_jpsi_PT{nPT}_Tz{iTz+1}"
            fo.write("%s %s   %6.2f    %6.2f \n"%( (tzTotalBinning[iTz]+tzTotalBinning[iTz+1])/2., (tzTotalBinning[iTz+1]-tzTotalBinning[iTz])/2., w.var(jpsi_varname).getValV(),w.var(jpsi_varname).getError()))
            fo_sign.write("%6.2f   %6.2f  %3.3f \n"%( w.var(jpsi_varname).getValV(), w.var(jpsi_varname).getError(), w.var(jpsi_varname).getValV()/w.var(jpsi_varname).getError()))
            hhJpsi.SetBinContent(iTz+1, w.var(jpsi_varname).getValV())
            hhJpsi.SetBinError(iTz+1, w.var(jpsi_varname).getError())
            #print ((tzTotalBinning[iTz]+tzTotalBinning[iTz+1])/2.0, (tzTotalBinning[iTz+1]-tzTotalBinning[iTz])/2.0)

        fo.write( str(len(tzTotalBinning)-1) )
        fo.write( "\n" )
        fo_sign.write( str(len(tzTotalBinning)-1) )
        fo_sign.write( "\n" )
        for iTz in range(len(tzTotalBinning)-1):
            
            etac_varname = f"n_etac_PT{nPT}_Tz{iTz+1}"
            fo.write("%s %s   %6.2f    %6.2f\n"%( (tzTotalBinning[iTz]+tzTotalBinning[iTz+1])/2., (tzTotalBinning[iTz+1]- tzTotalBinning[iTz])/2., w.var(etac_varname).getValV(),w.var(etac_varname).getError()))
            fo_sign.write("%6.2f   %6.2f   %3.3f \n"%(  w.var(etac_varname).getValV(),w.var(etac_varname).getError(), w.var(etac_varname).getValV()/w.var(etac_varname).getError() ))
            hhEtac.SetBinContent(iTz+1, w.var(etac_varname).getValV())
            hhEtac.SetBinError(iTz+1, w.var(etac_varname).getError())
            #print ((tzTotalBinning[iTz]+tzTotalBinning[iTz+1])/2.0, (tzTotalBinning[iTz+1]-tzTotalBinning[iTz])/2.0)

        f1.Close()

        fo.close()
        fo_sign.close()    

        # for iTz in range(len(tzTotalBinning)-1):

        #     # nameWksp = f"{dirName}/Wksp_MassFit_PT{nPT}_Tz{iTz+1}_C{nConf}.root"
        #     # nameWksp = f"{dirName}/Jpsi_Wksp_MassFit_PT{nPT}_Tz{iTz+1}_C{nConf}_total.root"
        #     nameWksp = f"{dirName}/Jpsi_Wksp_M_PT{nPT}_C{nConf}_total.root"
        #     f1 = TFile(nameWksp,"READ")
        #     w = f1.Get("w") 
            
        #     fo.write("%s %s   %6.2f    %6.2f \n"%( (tzTotalBinning[iTz]+tzTotalBinning[iTz+1])/2., (tzTotalBinning[iTz+1]-tzTotalBinning[iTz])/2., w.var("nJpsi").getValV(),w.var("nJpsi").getError()))
        #     fo_sign.write("%6.2f   %6.2f  %3.3f \n"%( w.var("nJpsi").getValV(), w.var("nJpsi").getError(), w.var("nJpsi").getValV()/w.var("nJpsi").getError()))
        #     hhJpsi.SetBinContent(iTz+1, w.var("nJpsi").getValV())
        #     hhJpsi.SetBinError(iTz+1, w.var("nJpsi").getError())
        #     #print ((tzTotalBinning[iTz]+tzTotalBinning[iTz+1])/2.0, (tzTotalBinning[iTz+1]-tzTotalBinning[iTz])/2.0)
        #     f1.Close()

        # fo.write( str(len(tzTotalBinning)-1) )
        # fo.write( "\n" )
        # fo_sign.write( str(len(tzTotalBinning)-1) )
        # fo_sign.write( "\n" )
        # for iTz in range(len(tzTotalBinning)-1):

        #     nameWksp = f"{dirName}/Wksp_MassFit_PT{nPT}_Tz{iTz+1}_C{nConf}.root"%(nPT,iTz+1,nConf)
        #     f1 = TFile(nameWksp,"READ")
        #     w = f1.Get("w") 
            
        #     fo.write("%s %s   %6.2f    %6.2f\n"%( (tzTotalBinning[iTz]+tzTotalBinning[iTz+1])/2., (tzTotalBinning[iTz+1]- tzTotalBinning[iTz])/2., w.var("nEta").getValV(),w.var("nEta").getError()))
        #     fo_sign.write("%6.2f   %6.2f   %3.3f \n"%(  w.var("nEta").getValV(),w.var("nEta").getError(), w.var("nEta").getValV()/w.var("nEta").getError() ))
        #     hhEtac.SetBinContent(iTz+1, w.var("nEta").getValV())
        #     hhEtac.SetBinError(iTz+1, w.var("nEta").getError())
        #     #print ((tzTotalBinning[iTz]+tzTotalBinning[iTz+1])/2.0, (tzTotalBinning[iTz+1]-tzTotalBinning[iTz])/2.0)

        #     f1.Close()

        # fo.close()
        # fo_sign.close()    

    else:
        fo = open(f"{homeDir}/signal/signal_PT{nPT}_{nConf}.txt", "w")
        if nPT == 1:    fo_sign = open(f"{homeDir}/signal/signif/signif_Mass_PTBins_{nConf}.txt", "w")
        else:           fo_sign = open(f"{homeDir}/signal/signif/signif_Mass_PTBins_{nConf}.txt", "a")

        hhJpsi.SetBins(len(tzEtacBinsBinning)-1, tzJpsiBinsBinning)
        hhEtac.SetBins(len(tzEtacBinsBinning)-1, tzEtacBinsBinning)

        fo.write( str(len(tzJpsiBinsBinning)-1) )
        fo.write( "\n" )
        fo_sign.write( "PTBin " + str(nPT) + "\n" )
        fo_sign.write( str(len(tzJpsiBinsBinning)-1) + "\n" )
        for iTz in range(len(tzJpsiBinsBinning)-1):

            #nameWksp = dirName + "Etac/Wksp_MassFit_PT%s_Tz%d_C%d.root"%(nPT,iTz+1,nConf)
            nameWksp = f"{dirName}/Jpsi/Wksp_MassFit_PT{nPT}_Tz{iTz+1}_C{nConf}.root"%(nPT,iTz+1,nConf)
            f1 = TFile(nameWksp,"READ")
            w = f1.Get("w") 
            
            fo.write("%s %s   %6.2f    %6.2f\n"%( (tzJpsiBinsBinning[iTz+1]+tzJpsiBinsBinning[iTz])/2., (tzJpsiBinsBinning[iTz+1]-tzJpsiBinsBinning[iTz])/2., w.var("nJpsi").getValV(),w.var("nJpsi").getError()))
            fo_sign.write("%6.2f   %6.2f  %3.3f \n"%( w.var("nJpsi").getValV(), w.var("nJpsi").getError(), w.var("nJpsi").getValV()/w.var("nJpsi").getError()))
            hhJpsi.SetBinContent(iTz+1, w.var("nJpsi").getValV())
            hhJpsi.SetBinError(iTz+1, w.var("nJpsi").getError())

            f1.Close()
            
        fo.write( str(len(tzEtacBinsBinning)-1) )
        fo.write( "\n" )
        fo_sign.write( "PTBin " + str(nPT) + "\n" )
        fo_sign.write( str(len(tzEtacBinsBinning)-1) + "\n" )
        for iTz in range(len(tzEtacBinsBinning)-1):

            nameWksp = f"{dirName}/Etac/Wksp_MassFit_PT{nPT}_Tz{iTz+1}_C{nConf}.root"%(nPT,iTz+1,nConf)
            f1 = TFile(nameWksp,"READ")
            w = f1.Get("w") 
            
            fo.write("%s %s   %6.2f    %6.2f\n"%( (tzEtacBinsBinning[iTz]+tzEtacBinsBinning[iTz+1])/2., (tzEtacBinsBinning[iTz+1]-tzEtacBinsBinning[iTz])/2., w.var("nEta").getValV(),w.var("nEta").getError()))
            fo_sign.write("%6.2f   %6.2f   %3.3f \n"%(  w.var("nEta").getValV(),w.var("nEta").getError(), w.var("nEta").getValV()/w.var("nEta").getError() ))
            hhEtac.SetBinContent(iTz+1, w.var("nEta").getValV())
            hhEtac.SetBinError(iTz+1, w.var("nEta").getError())

            f1.Close()

        fo_sign.write("\n")
        fo.close()
        fo_sign.close()    

    canv_1 = TCanvas("canv_1","J/Psi_Mass",55,55,800,800)
    canv_1.Divide(1,2)
    rangeL = -10.0
    rangeR = 10.0

    canv_1.cd(1)
    canv_1.cd(1).SetLogy()

    hhJpsi.Draw("AP")
    hhJpsi.SetTitle("J/#psi")
    hhJpsi.SetLineWidth(2)
    hhJpsi.SetLineColor(4)
    hhJpsi.SetMinimum(1.0)
    hhJpsi.SetMaximum(7.e5)
    hhJpsi.GetXaxis().SetTitle("t_{z} [ps]")
    hhJpsi.GetYaxis().SetTitle("Entries per ps")
    hhJpsi.GetXaxis().SetRangeUser(rangeL,rangeR)
    
    canv_1.cd(2)


    canv_1.cd(2).SetLogy()

    hhEtac.Draw("AP")
    hhEtac.SetTitle("#eta_{c}")
    hhEtac.SetLineWidth(2)
    hhEtac.SetLineColor(4)
    hhEtac.SetMinimum(1.0)
    hhEtac.SetMaximum(7.e5)
    hhEtac.GetXaxis().SetRangeUser(rangeL,rangeR)
    hhEtac.GetXaxis().SetTitle("t_{z} [ps]")
    hhEtac.GetYaxis().SetTitle("Entries per ps")

    canv_1.SaveAs(f"{homeDir}/signal/signal_PT{nPT}_{nConf}.pdf")

if __name__ == "__main__":
    yields(nPT=0, nConf="Base", keySel="all")
    # for iPT in range(1, 6):
        # yields(iPT, 0, True)
#def yieldPT()
#{
##    iB=0
    #for (iB=1 iB<6 iB++)
        #for(iC=0 iC<10 iC++)
        #{
            #yield(iB, iC)
        #}
#}
