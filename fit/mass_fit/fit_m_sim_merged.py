# from ROOT import *
from ROOT import TFile, TChain, TTree, TCanvas
from ROOT import RooFit, RooStats, RooMinuit
from ROOT import RooDataHist, RooArgList, RooRealVar, \
                    RooDataSet, RooArgSet, RooProfileLL, \
                    RooWorkspace, RooCmdArg, RooLinkedList
from ROOT import Math, gPad, gStyle

from model_sim import setMW, fill_workspace, combine, create_sample_sim
import drawModule_splitFit as dm
# gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
# gROOT.LoadMacro("../libs/libBifurcatedCB/BifurcatedCB.cxx+")
# from ROOT import RooRelBreitWigner, BifurcatedCB

import numpy as np
import logging 

#homeDir = "/afs/cern.ch/user/v/vazhovko/private/etac2s_ppbar/"
#dataDir = "/eos/user/v/vazhovko/etac2s_ppbar/"
# homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

CUTKEY = "ProbNNp_06"

def get_data_h(w, kSource):

    # nameWksp = dataDir + "/wksps/wksp_data_{}_ProbNNp_06_wide.root".format(kSource)
    nameWksp = f"{dataDir}/wksps/wksp_data_{kSource}_{CUTKEY}.root"
    file_w = TFile(nameWksp,"read")
    w_in = file_w.Get("w").Clone()
    file_w.Close()

    data   = w_in.data("dh")
    Jpsi_M = w_in.var("Jpsi_M")

    hist = data.createHistogram("hist",Jpsi_M)

    Jpsi_M     = w.var("Jpsi_M")

    dh  = RooDataHist("dh_{}".format(kSource),"dh",  RooArgList(Jpsi_M), hist)

    # w.Import(Jpsi_M)
    w.Import(dh)

    logging.info("RooDataHist successfully loaded")

def get_data_s(w, kSource):

    dirName_Low  = dataDir +"Data_Low/"
    dirName_High = dataDir +"Data_High/"


    nt_Low  =  TChain("DecayTree")
    nt_High =  TChain("DecayTree")

    nt_Low.Add(dirName_Low+"/Etac2sDiProton_Low_2018*.root")
    nt_High.Add(dirName_High+"/Etac2sDiProton_High_2018*.root")

    cut_FD = ""
    if kSource=="prompt":
        cut_FD = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08"
    else:
        cut_FD = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16"# & Jpsi_FDCHI2_OWNPV > 49""

    cut_PT      = "Jpsi_PT > 5000 && Jpsi_PT < 20000"

    totCut = cut_FD + " && " + cut_PT

    tree_Low = TTree()
    tree_High = TTree()

    tree_Low = nt_Low.CopyTree(totCut)
    tree_High = nt_High.CopyTree(totCut)

    Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M", minM_Low, maxM_High)

    ds_Low = RooDataSet("ds_Low","ds_Low", tree_Low, RooArgSet(Jpsi_M))
    ds_High = RooDataSet("ds_High","ds_High", tree_High, RooArgSet(Jpsi_M))
    w.Import(ds_Low)
    w.Import(ds_High)

    ds_Low.Print()
    ds_High.Print()

    logging.info("RooDataSet successfully loaded")

def plot_ll(w):

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf")
    combData = w.data("combData")

    # extFactor_pr = w.var("extFactor_prompt")
    # extFactor_fb = w.var("extFactor_fromB")
    # constr = RooArgSet(extFactor_pr,extFactor_fb)
    constr_pr = w.pdf("constr_ext_prompt")
    constr_fb = w.pdf("constr_ext_fromB")
    constr = RooArgSet(constr_pr,constr_fb)

    # if constr==None:
    nll_n = simPdf.createNLL(combData, RooFit.Extended(True), RooFit.Offset(True)) #, RooFit.Constrain(globalParams)
    # else:
    nll_c = simPdf.createNLL(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.ExternalConstraints(constr))

    poi_var = w.var("cs_etac2S_rel_prompt")
    poi_var.setRange(0., 7.5)

    # # Plot likelihood scan frac
    frameLL = poi_var.frame(RooFit.Bins(20),RooFit.Title("LL and profileLL in n_{#eta_{c}}(2S) "))

    # Minimize likelihood w.r.t all parameters before making plots
    m_n = RooMinuit(nll_n)
    m_n.migrad()
    # m_n.hesse()
    # m_n.minos()

    pll_N = RooProfileLL("pll_N", "pll_N", nll_n, RooArgSet(poi_var))
    # # # Plot the profile likelihood in frac
    pll_N.plotOn(frameLL,RooFit.LineColor(2))

    m_c = RooMinuit(nll_c)
    m_c.migrad()
    # m_c.hesse()
    # m_c.minos()

    pll_C = RooProfileLL("pll_C", "pll_C", nll_c, RooArgSet(poi_var))
    # # # Plot the profile likelihood in frac
    pll_C.plotOn(frameLL,RooFit.LineColor(3))
    # llhood  = RooFormulaVar("llhood","exp(-0.5*pll_N_etac2S)",RooArgList(pll_N_etac2S));


    # # The profile likelihood estimator on nll for frac will minimize nll w.r.t
    # # all floating parameters except frac for each evaluation



    # # Adjust frame maximum for visual clarity
    frameLL.SetMinimum(0)
    # frameLL.GetXaxis().SetRangeUser(poimin, poimax)

    cNLL = TCanvas("profilell","profilell", 800, 400)
    cNLL.cd(),  gPad.SetLeftMargin(0.15),  frameLL.GetYaxis().SetTitleOffset(1.4),  frameLL.Draw()

    cNLL.SaveAs(f"{homeDir}/mass_fit/one_hist/CB/NLL_check_{CUTKEY}.pdf")

    # nll.Write()
    # pll_N_etac2S.Write()

def plot(w:RooWorkspace, namePic:str):

    frame_pr, resid_pr, pull_pr = dm.set_frame(w,"", kSource="prompt")
    frame_fb, resid_fb, pull_fb = dm.set_frame(w,"", kSource="fromB")

    names  = ["prompt","fromB"]
    frames = [frame_pr,frame_fb]
    resids = [resid_pr,resid_fb]
    pulls  = [pull_pr,pull_fb]
    c = dm.set_canvas(names, frames, resids, pulls)

    c.SaveAs(namePic)

def perform_fit(kSyst, kNorm="jpsi", state="etac2S" ):

    logging.info(f"Performing fit for {state} for {kSyst} sy stematic")
    logging.info("-----------------------------------------------------------------------------------")
    # Math.MinimizerOptions.SetDefaultMaxFunctionCalls(1000000)

    Jpsi_M = RooRealVar("Jpsi_M", "Jpsi_M",  dm.minM_Low, dm.maxM_High)
    Jpsi_M.setBins(dm.binN_Tot_Fit)
    w = RooWorkspace("w",True)
    w.Import(Jpsi_M)

    for kSource in ["prompt","fromB"]:
        get_data_h(w,kSource)

    all_states = ["jpsi", "etac", "chic0", "chic1", "chic2", "hc", "psi2S", "etac2S"]
    all_states.insert(0, all_states.pop(all_states.index(kNorm)))

    # if kSyst=="Range":
        # all_states = ["jpsi", "etac", "psi2S", "etac2S"]

    # kCB = False if kSyst!="CB" else True
    kCB = False if kSyst=="Gauss" else True

    setMW(w, all_states)

    for name in all_states:
        fill_workspace(w, name, "", ref=kNorm, kCB=kCB, kSyst=kSyst)
    for kSource in ["prompt","fromB"]:
        combine(w, all_states, "", kSource, kSyst, split_lvl="none",kLim=state)

    create_sample_sim(w)

    Jpsi_M = w.var("Jpsi_M")
    Jpsi_M.setRange("fitRange", dm.minM_Low, dm.maxM_High)
    Jpsi_M.setRange("fitRange_Low", dm.minM_Low, dm.maxM_Low)
    Jpsi_M.setRange("fitRange_High", dm.minM_High, dm.maxM_High)
    Jpsi_M.setRange("fitRange_Low_Lim", 2850., 3200.)
    Jpsi_M.setRange("fitRange_High_Lim", 3600., dm.maxM_High)

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf_constr")
    # simPdf = w.pdf("simPdf")
    combData = w.data("combData")
    if kSyst=="Toys":
        kSource = "sim"
        f_fit = TFile(f"{homeDir}/mass_fit/one_hist/CB/Wksp_{kNorm}_{kSource}_{'Base'}_{CUTKEY}_{state}.root","READ")
        w_fit = f_fit.Get("w")
        simPdf_fit = w_fit.pdf("simPdf_constr")
        n = int(combData.sumEntries())
        # print(n) 
        # combData = simPdf.generate(RooArgList(Jpsi_M, sample), n)
        combData = simPdf_fit.generateBinned(RooArgList(Jpsi_M, sample), n)
        combData.Print()
        f_fit.Close()
        # input()

    simPdf.Print(); 

    
    model_fb = w.pdf("model_fromB")
    model_pr = w.pdf("model_prompt")
    model_fb.Print() 
    model_pr.Print()

    data_pr = w.data("dh_prompt")
    data_fb = w.data("dh_fromB")

    extFactor_pr = w.var("extFactor_prompt")
    extFactor_fb = w.var("extFactor_fromB")

    constr_pr = w.pdf("constr_ext_prompt")
    constr_fb = w.pdf("constr_ext_fromB")

    extFactorObs_pr = w.var("extFactorObs_prompt")
    extFactorObs_fb = w.var("extFactorObs_fromB")
    extFactorObs_pr.setAttribute("MyGlobalObservable")
    extFactorObs_fb.setAttribute("MyGlobalObservable")
    simPdf.setStringAttribute("GlobalObservablesTag","MyGlobalObservable")

    llist = RooLinkedList()
    llist.Add(RooCmdArg(RooFit.Extended(True)))
    llist.Add(RooCmdArg(RooFit.Save(True)))
    llist.Add(RooCmdArg(RooFit.Offset(True)))

    extFactor_pr.setConstant(True)
    extFactor_fb.setConstant(True)
    
    # w.var("cs_etac2S_rel_prompt").setVal(0.)
    # w.var("cs_etac2S_rel_prompt").setConstant(True)

    w.var("mass_jpsi").setConstant(False)
    model_fb.fitTo(data_fb, llist)
    model_fb.fitTo(data_fb, llist)

    w.var("mass_jpsi").setConstant(True)
    w.var("sigma_{}_1".format(kNorm)).setConstant(True)
    model_pr.fitTo(data_pr, llist)
    # w.var("n_psi2S_prompt").setVal(1e4)
    model_pr.fitTo(data_pr, llist)


    if kSyst=="Syst":
        w.var("extFactor_prompt").setConstant(False)
        w.var("extFactor_fromB").setConstant(False)
        llist.Add(RooCmdArg(RooFit.Constrain(RooArgSet(w.var("extFactor_prompt")))))
        llist.Add(RooCmdArg(RooFit.Constrain(RooArgSet(w.var("extFactor_fromB")))))


    # w.var("mass_jpsi").setAttribute("MyGlobalObservable") ;
    # w.var("mass_etac").setAttribute("MyGlobalObservable") ;
    # constraints = RooFit.ExternalConstraints(RooArgSet(fconstrEtac,fconstrJpsi))
    # constraints = RooFit.ExternalConstraints(RooArgSet(fconstrEtac2S,fconstrWEtac2S))
    # constraints = RooFit.ExternalConstraints(RooArgSet(fconstrEtac2S))
    # constraints = RooFit.ExternalConstraints(RooArgSet(fconstrHc))
    # llist.Add(RooCmdArg(constraints))

    mass  = w.var(f"mass_{state}")
    gamma = w.var(f"gamma_{state}")
    # if kSyst=="Mass":
    constraints = RooArgSet()    
    constraints.add(extFactor_pr)
    constraints.add(extFactor_fb)
    if kSyst!="Range":
        constraints.add(mass)
        if gamma.getVal()>1e-3 and state!="hc":
            constraints.add(gamma)
    llist.Add(RooCmdArg(RooFit.Constrain(constraints)))
    llist.Add(RooCmdArg(RooFit.GlobalObservablesTag("MyGlobalObservable")))
    # llist.Add(RooCmdArg(RooFit.ExternalConstraints(RooArgSet(constr_pr,constr_fb))))

    fitresult = simPdf.fitTo(combData, llist)

    w.var("sigma_{}_1".format(kNorm)).setConstant(False)
    fitresult = simPdf.fitTo(combData, llist)

    w.var("mass_etac").setConstant(False)
    w.var("mass_jpsi").setConstant(False)
    w.var("mass_psi2S").setConstant(False)
    if kSyst!="Stat":
        mass.setConstant(False)
        if gamma.getVal()>1e-3 and state!="hc":
            gamma.setConstant(False)

        extFactor_pr.setConstant(False)
        extFactor_fb.setConstant(False)
    
    # llist.Add(RooCmdArg(RooFit.Strategy(2)))
    fitresult = simPdf.fitTo(combData, llist)
    # print("STATUS: {}".format(fitresult.status()))
    # while fitresult.status()!=0: 
        # fitresult = simPdf.fitTo(combData, llist)
        # print("STATUS: {}".format(fitresult.status()))
        # input()
    if kSyst=="Range":
        llist.Add(RooCmdArg(RooFit.Range("fitRange_Low,fitRange_High_Lim")))
        llist.Add(RooCmdArg(RooFit.SplitRange(True)))
        for cc in ["chic0", "chic1", "chic2", "hc"]:
            for s in ["prompt","fromB"]:
                w.var("n_{}_{}".format(cc, s)).setVal(0.)
                w.var("n_{}_{}".format(cc, s)).setConstant(True)
        # llist.Add(RooCmdArg(RooFit.Range("fitRange_Low_Lim")))
        # llist.Add(RooCmdArg(RooFit.Range("fitRange_High_Lim")))

    fitresult = simPdf.fitTo(combData, llist)

    # llist.Remove(llist.find("Strategy"))
    # llist.Add(RooCmdArg(RooFit.Minos(True)))
    llist.Add(RooCmdArg(RooFit.NumCPU(20)))
    # llist.Add(RooCmdArg(RooFit.ModularL(True)))
    fitresult = simPdf.fitTo(combData, llist) 
    fitresult = simPdf.fitTo(combData, llist) 
    fitresult = simPdf.fitTo(combData, llist) 
    # fitresult = simPdf.fitTo(combData, llist) 

    fitresult.correlationMatrix().Print()

    kSource = "sim"
    nameWksp = f"{homeDir}/mass_fit/one_hist/CB/Wksp_{kNorm}_{kSource}_{kSyst}_{CUTKEY}_{state}_rel_noBR.root"
    namePic  = f"{homeDir}/mass_fit/one_hist/CB/Plot_{kNorm}_{kSource}_{kSyst}_{CUTKEY}_{state}_rel_noBR.pdf"
    nameTxt  = f"{homeDir}/mass_fit/one_hist/CB/Result_{kNorm}_{kSource}_{kSyst}_{CUTKEY}_{state}_rel_noBR.txt"

    plot(w, namePic)
    w.writeToFile(nameWksp)

    import os, sys
    save = os.dup( sys.stdout.fileno() )
    newout = open(nameTxt, "w" )
    os.dup2( newout.fileno(), sys.stdout.fileno() )
    fitresult.Print("v")
    fitresult.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() )
    newout.close()

    logging.info("Fit successfully performed")

    return w

def gen_fit(kSyst, kNorm="jpsi", state="etac2S" ):

    logging.info(f"Generating toys for {state} for {kSyst} systematic")
    logging.info("-----------------------------------------------------------------------------------")
    # Math.MinimizerOptions.SetDefaultMaxFunctionCalls(1000000)

    Jpsi_M = RooRealVar("Jpsi_M", "Jpsi_M",  dm.minM_Low, dm.maxM_High)
    Jpsi_M.setBins(dm.binN_Tot_Fit)
    w = RooWorkspace("w",True)
    w.Import(Jpsi_M)

    for kSource in ["prompt","fromB"]:
        get_data_h(w,kSource)

    all_states = ["jpsi", "etac", "chic0", "chic1", "chic2", "hc", "psi2S", "etac2S"]
    all_states.insert(0, all_states.pop(all_states.index(kNorm)))

    # if kSyst=="Range":
        # all_states = ["jpsi", "etac", "psi2S", "etac2S"]

    # kCB = False if kSyst!="CB" else True
    kCB = False if kSyst=="Gauss" else True

    setMW(w, all_states)

    for name in all_states:
        fill_workspace(w, name, "", ref=kNorm, kCB=kCB, kSyst=kSyst)
    for kSource in ["prompt","fromB"]:
        combine(w, all_states, "", kSource, kSyst, split_lvl="none",kLim=state)

    create_sample_sim(w)

    Jpsi_M = w.var("Jpsi_M")
    Jpsi_M.setRange("fitRange", dm.minM_Low, dm.maxM_High)
    Jpsi_M.setRange("fitRange_Low", dm.minM_Low, dm.maxM_Low)
    Jpsi_M.setRange("fitRange_High", dm.minM_High, dm.maxM_High)
    Jpsi_M.setRange("fitRange_Low_Lim", 2850., 3200.)
    Jpsi_M.setRange("fitRange_High_Lim", 3600., dm.maxM_High)

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf_constr")
    # simPdf = w.pdf("simPdf")
    combData = w.data("combData")
    simPdf.Print(); 
    data = simPdf.generate(RooArgList(Jpsi_M, sample), 1e9)
    data.Print()


    

''' 
    base histos: PT, tz and PID (all 0.6) cuts 
    misID histos: Kpi misID, PT, tz and PID (all 0.6) cuts 
    ProbNNp_08 histos: Kpi misID, PT, tz and PID (p>0.8) cuts 
'''

if __name__ == "__main__":

    gStyle.SetStatFormat("4.3f")
    gStyle.SetOptTitle(0)
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

    kSysts = ["Base","ChebAlt", "Exp","Gamma","eff","CB","BR","cross_talk","Resolution","pppi0"]
    # kSysts = ["Exp", "Sqrt", "Sqr"]
    kSysts = ["Base","Gauss","SRatio","Exp","pppi0","Sqrt"]
    # kSysts = ["SRatio","Sqrt","pppi0"]
    # kSysts = ["Gauss","Exp"]
    # kSysts = ["pppi0","Sqrt"]
    # kSysts = ["Toys"]
    kSysts = ["Base"]
    kSysts = ["Gauss","SRatio","Exp","pppi0","Sqrt"]
    # kSysts = ["Stat"]
    kNorm = 'jpsi'
    # kNorm = 'chic2'
    # kNorm = 'etac'
    # for kBkg in kBkgs: 
    for kSyst in kSysts:
        # w = gen_fit(kSyst, kNorm, "etac2S")
        # w = perform_fit(kSyst, kNorm, "etac2S")
        # w = perform_fit(kSyst, kNorm, "chic0")
        # w = perform_fit(kSyst, kNorm, "chic1")
        # w = perform_fit(kSyst, kNorm, "chic2")
        # w = perform_fit(kSyst, kNorm, "hc")
        # w = perform_fit(kSyst, kNorm, "psi2S")
        nameWksp = f"{homeDir}/mass_fit/one_hist/CB/Wksp_{kNorm}_sim_{kSyst}_{CUTKEY}_etac2S.root"
        file_w = TFile(nameWksp)
        w = file_w.Get("w").Clone()
        file_w.Close()
        plot(w, f"{homeDir}/mass_fit/one_hist/CB/Plot_{kNorm}_sim_{kSyst}_{CUTKEY}_etac2S_note.pdf")
        # plot_ll(w)
        # del w

