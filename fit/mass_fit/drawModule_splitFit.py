from ROOT import gROOT, gStyle
#from ROOT import TFile, TChain, TTree, TH1D, TGraph, TCanvas, TCut, TMath
#from ROOT import RooFit, RooStats
from ROOT import RooArgSet, RooAbsReal 
from ROOT import TGaxis, TCanvas, TLatex
import ROOT.RooFit as RF
# from ROOT.RooStats import *
# gROOT.LoadMacro("../libs/libBifurcatedCB/BifurcatedCB.cxx+")
# gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../libs/lhcbStyle.C")
# from ROOT import RooRelBreitWigner, BifurcatedCB

binWidth = 0.25
binWidthDraw = 5.0

minM_Low = 2850
# minM_Low = 2820
maxM_Low = 3300
# maxM_Low = 3200
nBins_Low = int((maxM_Low-minM_Low)/binWidth)
binN_Low  = int((maxM_Low-minM_Low)/binWidthDraw)

minM_High = 3300
# minM_High = 3600
maxM_High = 3780
# maxM_High = 3800
nBins_High = int((maxM_High-minM_High)/binWidth)
binN_High  = int((maxM_High-minM_High)/binWidthDraw)

binN_Tot     = int((maxM_High-minM_Low)/binWidthDraw)
binN_Tot_Fit = int((maxM_High-minM_Low)/binWidth)

binning      = RF.Binning(binN_Tot, minM_Low, maxM_High)
binning_low  = RF.Binning(binN_Low, minM_Low, maxM_Low)
binning_high = RF.Binning(binN_High, minM_High, maxM_High)

range_tot  = RF.Range(minM_Low,maxM_High)
range_low  = RF.Range(minM_Low,maxM_Low)
range_high = RF.Range(minM_High,maxM_High)

mrkSize = RF.MarkerSize(0.5)
lineWidth1 = RF.LineWidth(1)
lineWidth2 = RF.LineWidth(2)
lineStyle1 = RF.LineStyle(2)
lineStyle2 = RF.LineStyle(9)
lineColor1 = RF.LineColor(6)
lineColor2 = RF.LineColor(8)
lineColor3 = RF.LineColor(2)
name = RF.Name
cut = RF.Cut

optDic = {
    "Low":      ["J/#psi to p#bar{p}", binning_low, range_low ],
    "High":     ["#eta_c(2S) to p#bar{p}", binning_high, range_high],
    "":         ["c#bar{c} to p#bar{p}", binning, range_tot],
    "Base":     ["c#bar{c} to p#bar{p} Base", binning, range_tot ],
    "Exp":      ["c#bar{c} to p#bar{p} Exp", binning, range_tot ],
    "prompt":   ["c#bar{c} to p#bar{p} prompt", binning, range_tot ],
    "fromB":    ["c#bar{c} to p#bar{p} fromB", binning, range_tot ],
}

def draw_range(w,kRange):

    if kRange=="Low":
        Jpsi_M = w.var("Jpsi_M_Low")
    if kRange=="High":
        Jpsi_M = w.var("Jpsi_M_High")


    model       = w.pdf("model_{}".format(kRange))
    modelBkg    = w.pdf("modelBkg_{}".format(kRange))
    modelSignal = w.pdf("modelSignal_{}".format(kRange))

    dh = w.data("dh_{}".format(kRange))

    ##draw range separately

    frame = Jpsi_M.frame(RF.Title(optDic[kRange][0]),RF.Range("fitRange_{}".format(kRange)))

    dh.plotOn(frame, optDic[kRange][1], mrkSize, lineWidth1, name("data_{}".format(kRange)))

    argset = RooArgSet(Jpsi_M)
    n_events = model.createIntegral(argset, RF.NormSet(argset), RF.Range("fitRange_{}".format(kRange))).getValV()
    model.plotOn(frame, lineWidth1, RF.Normalization(n_events,RooAbsReal.RelativeExpected), name("mod_{}".format(kRange)))
    modelSignal.plotOn(frame, lineWidth1, lineColor2, lineStyle1, name("sig_{}".format(kRange)), RF.Normalization(1.,RooAbsReal.RelativeExpected))

    n_events_bkg = modelBkg.createIntegral(argset, RF.NormSet(argset), RF.Range("fitRange_{}".format(kRange))).getValV()
    modelBkg.plotOn(frame, lineWidth1, lineColor1, lineStyle1, RF.Normalization(0.,RooAbsReal.RelativeExpected))
    modelBkg.plotOn(frame, lineWidth1, lineColor1, lineStyle1, RF.Normalization(n_events_bkg,RooAbsReal.RelativeExpected), name("bkg_{}".format(kRange)))

    frame.GetYaxis().SetTitleOffset(1.5)
    frame.GetXaxis().SetTitleOffset(0.75)
    frame.GetXaxis().SetTitle("M(p#bar{p}) / [MeV/c^{2}]")
    frame.GetXaxis().SetTitleSize(0.045)
    frame.GetXaxis().SetTitleFont(12)
    frame.SetMinimum(-10.5)
    #frame.Draw()

    h_resid = frame.residHist("data_{}".format(kRange),"mod_{}".format(kRange))
    h_resid.SetMarkerSize(0.5)
    resid = Jpsi_M.frame(RF.Title(" "),RF.Bins(100), RF.Range("fitRange_{}".format(kRange)))
    resid.addPlotable(h_resid,"B")
    modelSignal.plotOn(resid, lineWidth1, name("sig_{}".format(kRange)))#, RF.Normalization(8.83177e+07,RooAbsReal.NumEvent))

    resid.GetXaxis().SetTitle("M(p#bar{p}) / [MeV/c^{2}]")
    resid.GetXaxis().SetTitleFont(12)
    #resid.Draw()

    h_pull = frame.pullHist("data_{}".format(kRange),"mod_{}".format(kRange))
    h_pull.SetMarkerSize(0.5)
    pull = Jpsi_M.frame(RF.Title(" "),RF.Bins(100), RF.Range("fitRange_{}".format(kRange)))
    for ii in range(h_pull.GetN()):
        h_pull.SetPointEYlow(ii,0)
        h_pull.SetPointEYhigh(ii,0)
    pull.SetMinimum(-4.0)
    pull.SetMaximum(4.0)
    pull.GetXaxis().SetTitleSize(0.0)
    pull.GetXaxis().SetLabelSize(0.06)
    pull.GetYaxis().SetLabelSize(0.06)
    pull.addPlotable(h_pull,"B")
    #pull.Draw()

    return frame, resid, pull


def set_single_frame(w,kRange="",kSource=""):

    postFix = ""
    if kRange!="":
        postFix += "_{}".format(kRange)
    if kSource!="":
        postFix += "_{}".format(kSource)

    Jpsi_M = w.var("Jpsi_M{}".format(postFix))
    if Jpsi_M == None:
        Jpsi_M = w.var("Jpsi_M")


    # dh  = w.data("dh{}".format(postFix))
    dh  = w.data("dh")


    ##draw low and high on the same frame
    ranges = ["Low","High"]

    frame = Jpsi_M.frame(RF.Title(optDic[kRange][0]))
    resid = Jpsi_M.frame(RF.Title(" "), RF.Bins(100))
    pull = Jpsi_M.frame(RF.Title(" "),RF.Bins(100))

    dh.plotOn(frame, mrkSize, lineWidth1, optDic["All"][1], name("data"))

    for kRange in ranges:

        postFix = "_{}".format(kRange)

        modelSignal = w.pdf("modelSignal{}".format(postFix))
        totalPdf  = w.pdf("model{}".format(postFix))
        signalPdf = w.pdf("modelSignal{}".format(postFix))

        dh.plotOn(frame, optDic[kRange][1], mrkSize, lineWidth1, name("data{}".format(postFix)))

        totalPdf.plotOn(frame, lineWidth1, name("mod{}".format(postFix)))
        modelSignal.plotOn(frame, lineWidth1, lineColor2, lineStyle1, name("sig{}".format(postFix)), RF.Normalization(1.,RooAbsReal.RelativeExpected))

        # totalPdf.plotOn(frame, lineWidth1, lineColor1, lineStyle1, RF.Normalization(0.,RooAbsReal.RelativeExpected))
        # totalPdf.plotOn(frame, RF.Components("bkg{}".format(postFix)), lineWidth1, lineColor1, lineStyle1, name("bkg{}".format(postFix)))
        totalPdf.plotOn(frame, RF.Components("bkg"), lineWidth1, lineColor1, lineStyle1, name("bkg{}".format(postFix)))

        #print("check")
        # h_resid = frame.residHist("data","bkg{}".format(postFix))
        h_resid = frame.residHist("data{}".format(postFix),"bkg{}".format(postFix))
        h_resid.SetMarkerSize(0.5)
        resid.addPlotable(h_resid,"P")
        modelSignal.plotOn(resid, lineWidth1, name("sig{}".format(postFix)), RF.Normalization(1.,RooAbsReal.RelativeExpected))#, RF.Normalization(8.83177e+07,RooAbsReal.NumEvent))

        h_pull = frame.pullHist("data{}".format(postFix),"mod{}".format(postFix))
        h_pull.SetMarkerSize(0.5)
        for ii in range(h_pull.GetN()):
            h_pull.SetPointEYlow(ii,0)
            h_pull.SetPointEYhigh(ii,0)
        pull.addPlotable(h_pull,"B")

    frame.GetYaxis().SetTitleOffset(0.55)
    frame.GetYaxis().SetTitle("Candidate / ({} MeV/c^{2})".format(binWidthDraw))
    frame.GetYaxis().SetTitleSize(0.085)
    frame.GetXaxis().SetTitleOffset(0.75)
    frame.GetXaxis().SetTitle("M(p#bar{p}) / [MeV/c^{2}]")
    frame.GetXaxis().SetTitleSize(0.085)
    frame.GetYaxis().SetTitleFont(12)
    frame.GetXaxis().SetTitleFont(12)
    frame.SetMinimum(-10.5)
    frame.Draw()

    resid.GetXaxis().SetTitle("M(p#bar{p}) / [MeV/c^{2}]")
    resid.GetXaxis().SetTitleFont(12)
    resid.Draw()

    pull.SetMinimum(-4.0)
    pull.SetMaximum(4.0)
    pull.SetFillColor(1)
    pull.GetXaxis().SetTitleSize(0.0)
    pull.GetXaxis().SetLabelSize(0.06)
    pull.GetYaxis().SetLabelSize(0.06)
    pull.Draw()

    return frame, resid, pull

def set_double_frame(w,kRange="",kSource=""):

    postFix = ""

    Jpsi_M = w.var("Jpsi_M{}".format(postFix))
    if Jpsi_M == None:
        Jpsi_M = w.var("Jpsi_M")


    dh  = w.data("dh")
    totalPdf  = w.pdf("model")
    signalPdf = w.pdf("modelSignal")


    if kRange!="":
        postFix += "_{}".format(kRange)
    if kSource!="":
        postFix += "_{}".format(kSource)

    if dh==None:    
        dh  = w.data("dh{}".format(postFix))
    if totalPdf==None:    
        totalPdf  = w.pdf("model{}".format(postFix))
    if signalPdf==None:    
        signalPdf = w.pdf("modelSignal{}".format(postFix))
    
    ##draw low and high separately
    frame = Jpsi_M.frame(RF.Title(optDic[kRange][0]), RF.Range("fitRange{}".format(postFix)))

    dh.plotOn(frame, optDic[kRange][1], mrkSize, lineWidth1, name("data{}".format(postFix)))

    totalPdf.plotOn(frame, lineWidth1, name("mod{}".format(postFix)))
    signalPdf.plotOn(frame, lineWidth1, lineColor2, lineStyle1, name("sig{}".format(postFix)), RF.Normalization(1.,RooAbsReal.RelativeExpected))

    totalPdf.plotOn(frame, lineWidth1, lineColor1, lineStyle1, RF.Normalization(0.,RooAbsReal.RelativeExpected))
    totalPdf.plotOn(frame, RF.Components("bkg"), lineWidth1, lineColor1, lineStyle1, name("bkg{}".format(postFix)))


    frame.GetYaxis().SetTitleOffset(0.55)
    frame.GetYaxis().SetTitle("Candidate / ({} MeV/c^{2})".format(binWidthDraw))
    frame.GetYaxis().SetTitleSize(0.085)
    frame.GetXaxis().SetTitleOffset(0.75)
    frame.GetXaxis().SetTitle("M(p#bar{p}) / [MeV/c^{2}]")
    frame.GetXaxis().SetTitleSize(0.085)
    frame.GetYaxis().SetTitleFont(12)
    frame.GetXaxis().SetTitleFont(12)
    # frame.SetMinimum(-10.5)
    # frame.SetMinimum(8.e5)
    frame.Draw()


    #print("check")
    h_resid = frame.residHist("data{}".format(postFix),"bkg{}".format(postFix))
    h_resid.SetMarkerSize(0.5)
    resid = Jpsi_M.frame(RF.Title(" "), RF.Bins(100), RF.Range("fitRange{}".format(postFix)))
    resid.addPlotable(h_resid,"P")
    signalPdf.plotOn(resid, lineWidth1, name("sig{}".format(postFix)))#, RF.Normalization(8.83177e+07,RooAbsReal.NumEvent))

    resid.GetXaxis().SetTitle("M(p#bar{p}) / [MeV/c^{2}]")
    resid.GetXaxis().SetTitleFont(12)
    resid.Draw()

    h_pull = frame.pullHist("data{}".format(postFix),"mod{}".format(postFix))
    h_pull.SetFillColor(1)
    h_pull.SetMarkerSize(0.5)
    pull = Jpsi_M.frame(RF.Title(" "),RF.Bins(100), RF.Range("fitRange{}".format(postFix)))
    for ii in range(h_pull.GetN()):
        h_pull.SetPointEYlow(ii,0)
        h_pull.SetPointEYhigh(ii,0)
    pull.SetMinimum(-4.0)
    pull.SetMaximum(4.0)
    pull.GetXaxis().SetTitleSize(0.0)
    pull.GetXaxis().SetLabelSize(0.06)
    pull.GetYaxis().SetLabelSize(0.06)
    # pull.SetFillColor(1)
    pull.addPlotable(h_pull,"B")


    return frame, resid, pull


def set_frame_lim(w,kRange="",kSource=""):

    postFix = ""
    if kSource!="":
        postFix += "_{}".format(kSource)

    Jpsi_M = w.var("Jpsi_M{}".format(postFix))
    if Jpsi_M == None:
        Jpsi_M = w.var("Jpsi_M")


    modelSignal = w.pdf("modelSignal{}".format(postFix))
    dh = w.data("dh")
    if dh==None:    
        dh  = w.data("dh{}".format(postFix))
    if dh==None:    
        dh  = w.data("dh_{}".format(kSource))
    
    totalPdf  = w.pdf("model{}".format(postFix))
    totalPdf.Print()

    ##draw low and high separately

    frame = Jpsi_M.frame(RF.Title(optDic[kRange][0]), optDic[kRange][2])

    dh.plotOn(frame, optDic[kRange][1], mrkSize, lineWidth1, name("data{}".format(postFix)))

    totalPdf.plotOn(frame, optDic[kRange][2], lineWidth1, name("mod{}".format(postFix)))
    modelSignal.plotOn(frame, optDic[kRange][2], lineWidth1, lineColor2, lineStyle1, name("sig{}".format(postFix)), RF.NormRange(""))#RF.Normalization(1.,RooAbsReal.RelativeExpected))

    totalPdf.plotOn(frame, optDic[kRange][2], lineWidth1, lineColor1, lineStyle1, RF.Normalization(0.,RooAbsReal.RelativeExpected))
    totalPdf.plotOn(frame, optDic[kRange][2], RF.Components("pppi0"), lineWidth1, lineColor3, lineStyle1, name("pppi0{}".format(postFix)),RF.Normalization(1,RooAbsReal.RelativeExpected))
    totalPdf.plotOn(frame, optDic[kRange][2], RF.Components("bkg{}".format(postFix)), lineWidth1, lineColor1, lineStyle1, name("bkg{}".format(postFix)))


    frame.GetYaxis().SetTitleOffset(0.85)
    # frame.GetYaxis().SetTitle("Candidate / ({} MeV/c^{2})]".format(binWidthDraw))
    frame.GetYaxis().SetTitle("Candidate / ("+str(binWidthDraw)+" MeV/c^{2})")
    frame.GetYaxis().SetTitleSize(0.085)
    frame.GetXaxis().SetTitleOffset(0.9)
    frame.GetXaxis().SetTitle("M(p#bar{p}) [MeV/c^{2}]")
    frame.GetXaxis().SetTitleSize(0.085)
    # frame.GetYaxis().SetTitleFont(12)
    # frame.GetXaxis().SetTitleFont(12)
    frame.SetMinimum(-10.5)
    axis = frame.GetYaxis()
    axis.ChangeLabel(1, -1, -1, -1, -1, -1, " ");
    axis.SetLabelSize(0.09)
    axis.SetTitleOffset(0.8)
    axis.SetTitleSize(0.09)
    frame.Draw()


    #print("check")
    h_resid = frame.residHist("data{}".format(postFix),"bkg{}".format(postFix))
    h_resid.SetMarkerSize(0.5)
    resid = Jpsi_M.frame(RF.Title(" "), RF.Bins(100), optDic[kRange][2])
    resid.addPlotable(h_resid,"P")
    modelSignal.plotOn(resid, optDic[kRange][2], lineWidth1, name("sig{}".format(postFix)))#, RF.Normalization(8.83177e+07,RooAbsReal.NumEvent))
    totalPdf.plotOn(resid, optDic[kRange][2], RF.Components("pppi0"), lineWidth1, lineColor3, lineStyle1, name("pppi0{}".format(postFix)),RF.Normalization(1,RooAbsReal.RelativeExpected))

    # resid.GetYaxis().SetTitleOffset(0.55)
    # resid.GetYaxis().SetTitle("Candidate / ({} MeV)]".format(binWidthDraw))
    resid.GetYaxis().SetTitle("")
    # resid.GetYaxis().SetTitleSize(0.085)
    resid.GetYaxis().SetLabelSize(0.19)
    resid.GetYaxis().SetNdivisions(505)
    resid.GetXaxis().SetTitle("M(p#bar{p}) / [MeV/c^{2}]")
    # resid.GetXaxis().SetTitleFont(12)
    resid.Draw()

    h_pull = frame.pullHist("data{}".format(postFix),"mod{}".format(postFix))
    h_pull.SetFillColor(1)
    h_pull.SetMarkerSize(0.5)
    pull = Jpsi_M.frame(RF.Title(" "),RF.Bins(100), optDic[kRange][2])
    for ii in range(h_pull.GetN()):
        h_pull.SetPointEYlow(ii,0)
        h_pull.SetPointEYhigh(ii,0)
    pull.SetMinimum(-5.0)
    pull.SetMaximum(5.0)
    # pull.GetXaxis().SetTitleSize(0.0)
    pull.GetXaxis().SetLabelSize(0.17)
    pull.GetYaxis().SetLabelSize(0.17)
    pull.GetYaxis().SetNdivisions(505)
    pull.GetXaxis().SetTitleSize(0.190)
    pull.GetXaxis().SetTitleOffset(0.9)
    pull.GetXaxis().SetTitle("M(p#bar{p}) / [MeV/c^{2}]")
    # pull.SetFillColor(1)
    pull.addPlotable(h_pull,"B")


    return frame, resid, pull



def set_frame(w,kRange="",kSource=""):

    postFix = ""
    if kRange!="":
        postFix += "_{}".format(kRange)
    if kSource!="":
        postFix += "_{}".format(kSource)

    Jpsi_M = w.var("Jpsi_M{}".format(postFix))
    if Jpsi_M == None:
        Jpsi_M = w.var("Jpsi_M")


    modelSignal = w.pdf("modelSignal{}".format(postFix))
    dh = w.data("dh")
    if dh==None:    
        dh  = w.data("dh{}".format(postFix))
    if dh==None:    
        dh  = w.data("dh_{}".format(kSource))
    
    # if kRange=="High":
        # totalPdf  = w.pdf("model{}_constr".format(postFix))
    # else:
    totalPdf  = w.pdf("model{}".format(postFix))
    signalPdf = w.pdf("modelSignal{}".format(postFix))
    totalPdf.Print()

    ##draw low and high separately

    frame = Jpsi_M.frame(RF.Title(optDic[kRange][0]), RF.Range("fitRange{}".format(postFix)))

    dh.plotOn(frame, optDic[kRange][1], mrkSize, lineWidth1, name("data{}".format(postFix)))

    totalPdf.plotOn(frame, lineWidth1, name("mod{}".format(postFix)))
    modelSignal.plotOn(frame, lineWidth1, lineColor2, lineStyle1, name("sig{}".format(postFix)), RF.Normalization(1.,RooAbsReal.RelativeExpected))

    totalPdf.plotOn(frame, lineWidth1, lineColor1, lineStyle1, RF.Normalization(0.,RooAbsReal.RelativeExpected))
    totalPdf.plotOn(frame, RF.Components("pppi0"), lineWidth1, lineColor3, lineStyle1, name("pppi0{}".format(postFix)),RF.Normalization(1,RooAbsReal.RelativeExpected))
    totalPdf.plotOn(frame, RF.Components("bkg{}".format(postFix)), lineWidth1, lineColor1, lineStyle1, name("bkg{}".format(postFix)))


    frame.GetYaxis().SetTitleOffset(0.85)
    # frame.GetYaxis().SetTitle("Candidate / ({} MeV/c^{2})]".format(binWidthDraw))
    frame.GetYaxis().SetTitle("Candidate / ("+str(binWidthDraw)+" MeV/c^{2})")
    frame.GetYaxis().SetTitleSize(0.085)
    frame.GetXaxis().SetTitleOffset(0.9)
    frame.GetXaxis().SetTitle("M(p#bar{p}) [MeV/c^{2}]")
    frame.GetXaxis().SetTitleSize(0.085)
    # frame.GetYaxis().SetTitleFont(12)
    # frame.GetXaxis().SetTitleFont(12)
    frame.SetMinimum(-10.5)
    axis = frame.GetYaxis()
    axis.ChangeLabel(1, -1, -1, -1, -1, -1, " ");
    axis.SetLabelSize(0.09)
    axis.SetTitleOffset(0.8)
    axis.SetTitleSize(0.09)
    frame.Draw()


    #print("check")
    h_resid = frame.residHist("data{}".format(postFix),"bkg{}".format(postFix))
    h_resid.SetMarkerSize(0.5)
    resid = Jpsi_M.frame(RF.Title(" "), RF.Bins(100), RF.Range("fitRange{}".format(postFix)))
    resid.addPlotable(h_resid,"P")
    modelSignal.plotOn(resid, lineWidth1, name("sig{}".format(postFix)))#, RF.Normalization(8.83177e+07,RooAbsReal.NumEvent))
    totalPdf.plotOn(resid, RF.Components("pppi0"), lineWidth1, lineColor3, lineStyle1, name("pppi0{}".format(postFix)),RF.Normalization(1,RooAbsReal.RelativeExpected))

    # resid.GetYaxis().SetTitleOffset(0.55)
    # resid.GetYaxis().SetTitle("Candidate / ({} MeV)]".format(binWidthDraw))
    resid.GetYaxis().SetTitle("")
    # resid.GetYaxis().SetTitleSize(0.085)
    resid.GetYaxis().SetLabelSize(0.19)
    resid.GetYaxis().SetNdivisions(505)
    resid.GetXaxis().SetTitle("M(p#bar{p}) / [MeV/c^{2}]")
    # resid.GetXaxis().SetTitleFont(12)
    resid.Draw()

    h_pull = frame.pullHist("data{}".format(postFix),"mod{}".format(postFix))
    h_pull.SetFillColor(1)
    h_pull.SetMarkerSize(0.5)
    pull = Jpsi_M.frame(RF.Title(" "),RF.Bins(100), RF.Range("fitRange{}".format(postFix)))
    for ii in range(h_pull.GetN()):
        h_pull.SetPointEYlow(ii,0)
        h_pull.SetPointEYhigh(ii,0)
    pull.SetMinimum(-5.0)
    pull.SetMaximum(5.0)
    # pull.GetXaxis().SetTitleSize(0.0)
    pull.GetXaxis().SetLabelSize(0.17)
    pull.GetYaxis().SetLabelSize(0.17)
    pull.GetYaxis().SetNdivisions(505)
    pull.GetXaxis().SetTitleSize(0.190)
    pull.GetXaxis().SetTitleOffset(0.9)
    pull.GetXaxis().SetTitle("M(p#bar{p}) / [MeV/c^{2}]")
    # pull.SetFillColor(1)
    pull.addPlotable(h_pull,"B")


    return frame, resid, pull


def set_canvas(names, frames, resids, pulls):

    nPads = len(names)
    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)

    texData = TLatex()
    texData.SetNDC()
    texData.SetTextSize(0.06)
    TGaxis.SetMaxDigits(4)

    # c = TCanvas("c1","c1", 3000, 2000)
    c = TCanvas("c1","c1", 3000, 1500)
    c.Divide(nPads, 3, 0.001, 0.001)
    for i in range(nPads):

            pad = c.cd(i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            # pad.SetPad(xl,yl-0.2,xh,yh)
            pad.SetPad(xl,yl-0.15,xh,yh)
            pad.SetTopMargin(0.1)

            pad.SetBottomMargin(0.0)
            pad.SetLeftMargin(0.15)
            # # pad.SetBottomMargin(0.15)
            # pad.SetLeftMargin(0.2)
            # pad.SetRightMargin(0.075)
            frames[i].Draw()
            # pad.SetLogy()
            
            # texData.DrawLatex(0.3, 0.95, names[i])

            #ptL = binningDict["Jpsi_PT"]
            #ptR = binningDict["Jpsi_PT"][nPT+1]
            #texData.DrawLatex(0.3, 0.95, "%s<p_{T}<%s"%(ptL,ptR))

            pad = c.cd(nPads+i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            # pad.SetPad(xl,yl-0.1,xh,yh-0.2)
            pad.SetPad(xl,yl-0.05,xh,yh-0.15)
            pad.SetTopMargin(0.0)
            pad.SetBottomMargin(0.0)
            pad.SetLeftMargin(0.15)
            # pad.SetLogy()
            # pad.SetBottomMargin(0.15)
            # pad.SetLeftMargin(0.2)
            # pad.SetRightMargin(0.075)
            #pad.SetTickx(2)
            #pad.SetTicky(2)
            resids[i].Draw()


            pad = c.cd(2*nPads+i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            #pad.SetTickx(2)
            #pad.SetTicky(2)
            # pad.SetPad(xl,yl,xh,yh-0.1)
            pad.SetPad(xl,yl,xh,yh-0.05)
            pad.SetTopMargin(0.0)
            pad.SetLeftMargin(0.15)
            pad.SetBottomMargin(0.4)
            # pad.SetLeftMargin(0.2)
            # pad.SetRightMargin(0.075)
            pulls[i].Draw()
    return c
