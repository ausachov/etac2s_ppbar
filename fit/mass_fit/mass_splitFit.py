#from ROOT import gROOT, gStyle
#from ROOT import TFile, TChain, TTree, TH1D, TGraph, TCanvas, TCut, TMath
#from ROOT import RooFit, RooStats
from ROOT import *
from ROOT.RooFit import *
from ROOT.RooStats import *
# config = RooStats.GetGlobalRooStatsConfig()
# config.useEvalErrorWall = False


# ROOT.Math.MinimizerOptions.SetDefaultMinimizer("Minuit2","Minimize");
# ROOT.Math.MinimizerOptions.SetDefaultMaxFunctionCalls(100000)
# ROOT.Math.MinimizerOptions.SetDefaultMaxFunctionCalls(300000)

from drawModule_splitFit import *
import charmConstLib as cl
gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")

import numpy as np

#homeDir = "/afs/cern.ch/user/v/vazhovko/private/etac2s_ppbar/"
#dataDir = "/eos/user/v/vazhovko/etac2s_ppbar/"
homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"


def choose_bkg(optList, regName, argList):
    return {

        "prompt cheb" : RooChebychev ("bkg_{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2),argList.at(3))),
        "fromB cheb"  : RooChebychev ("bkg_{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2))),
        "prompt exp"  : RooGenericPdf("bkg_{}".format(regName),"background","TMath::Exp(-(@0-3550.)/200.*@1)*(1.+@2*(@0-3550.)/200.+@3*(@0-3550.)*(@0-3550.)/200./200.)", RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3))) ,
        "fromB exp"   : RooGenericPdf("bkg_{}".format(regName),"background","TMath::Exp(-(@0-3550.)/200.*@1)*(1.+@2*(@0-3550.)/200)", RooArgList(argList.at(0), argList.at(1),argList.at(2)))

    }[optList[0] + " " + optList[1]]


def fillRelWorkspace(w, kSource, kBkg, kNorm="Jpsi"):

    Jpsi_M_Low  = w.var("Jpsi_M_Low")
    Jpsi_M_High = w.var("Jpsi_M_High")


    cs_Jpsi = RooRealVar("cs_Jpsi","cs Jpsi", 0.749)


    n_Jpsi = RooRealVar("n_Jpsi","num of J/Psi", 2e3, 10, 1.e6)
    n_etac = RooRealVar("n_etac","num of etac", 1e4, 10, 3.e6)



    # to make it faster one can combine Br_etac2s x / Br_Jpsi * effratio
    # this works ONLY for external constraints i.e. uncorrelated ones
    # in principle n_Jpsi can go here as well
    # systematics to be added
    ExtMeas = {
       "BR_etac"        : { "val"    : 1.50e-3,
                            "err"    : 0.16e-3,
                            "relerr" : -1.},
       "BR_Jpsi"        : { "val"    : 2.121e-3,
                            "err"    : 0.03e-3,
                            "relerr" : -1.},
       "BR_etac2s"      : { "val"    : 7.7e-5,
                            "err"    : 2.4e-5,
                            "relerr" : -1.},
       "BR_psi2S"       : { "val"    : 2.88e-4,
                            "err"    : 0.09e-4,
                            "relerr" : -1.},
       "eff_etac2s2Jpsi": { "val"    : 1.01, # should be > 1 ~ 1.5 ?
                            "err"    : -1.,
                            "relerr" : 0.02},
       "n_Jpsi"         : { "val"    : 93914., # should be picked up in more smart way every time fit of Low is updated !
                            "err"    : 3100.,  # same
                            "relerr" : -1.},
       "n_etac"         : { "val"    : 96934., # should be picked up in more smart way every time fit of Low is updated !
                            "err"    : 7670.,  # same
                            "relerr" : -1.},
       "n_psi2S"        : { "val"    : 997., # should be picked up in more smart way every time fit of Low is updated !
                            "err"    : 69.,  # same
                            "relerr" : -1.}
    }


    n_norm = "n_{}".format(kNorm)
    BR_norm = "BR_{}".format(kNorm)

    from ROOT.TMath import Sqrt, Sq
    total_val =  ExtMeas[n_norm]["val"]*ExtMeas["eff_etac2s2Jpsi"]["val"]*ExtMeas["BR_etac2s"]["val"]/ExtMeas[BR_norm]["val"]

    total_relerr = 0.
    for source in [BR_norm,"BR_etac2s","eff_etac2s2Jpsi",n_norm]:
        meas = ExtMeas[source]
        val  = meas["val"]
        if meas["relerr"] != -1. :
            relerr = meas["relerr"]
        else:
            relerr = meas["err"]/val
        total_relerr += Sq(relerr)
    total_relerr = Sqrt(total_relerr)
    total_err = total_relerr * total_val
    print(" OVERAL factor is      :", total_val)
    print(" WITH relative error   :", total_relerr)

    extFactor         = RooRealVar("extFactor",  "extFactor", total_val, 0, 100*total_val)
    constr_ext        = RooGaussian("constr_ext","constr_ext", extFactor, RooFit.RooConst(total_val), RooFit.RooConst(total_err))

    # n_etac2s_rel = RooRealVar("n_etac2s_rel","num of Etac", 1.5e-3, 0, 4.)
    # n_etac2s = RooRealVar("n_etac2s","num of etac2s", 2e3, 0, 3.e4)
    cs_etac2s_rel = RooRealVar("cs_etac2s_rel","num of Etac", 1e-2, 0, 4.0)
    n_etac2s = RooFormulaVar("n_etac2s","num of Etac","@0*@1", RooArgList(cs_etac2s_rel, extFactor))
    #if kNorm=="Jpsi":
        #n_etac2s = RooFormulaVar("n_etac2s","num of Etac","@0*@1", RooArgList(cs_etac2s_rel, extFactor))
    #else:
        #n_etac2s = RooFormulaVar("n_etac2s","num of Etac","@0*@1", RooArgList(n_etac2s_rel,n_etac))


    n_chic0 = RooRealVar("n_chic0","num of chic0", 1e3, 0, 1.e4)
    n_chic1 = RooRealVar("n_chic1","num of chic1", 1e3, 0, 1.e4)
    n_chic2 = RooRealVar("n_chic2","num of chic2", 1e3, 0, 1.e4)
    n_hc = RooRealVar("n_hc","num of hc", 1e3, 0, 1.e4)
    n_psi2S = RooRealVar("n_psi2S","num of psi2S", 1e3, 0, 1.e4)
    n_psi3770 = RooRealVar("n_psi3770","num of psi3770", 1e1, 0, 1.e5)
    #n_psi3770.setVal(0.); n_psi3770.setConstant()

    nSignal_Low = RooFormulaVar("nSignal_Low","num of Etac","@0+@1", RooArgList(n_etac,n_Jpsi))
    nSignal_High = RooFormulaVar("nSignal_High","num of Etac","@0+@1+@2+@3+@4+@5+@6", RooArgList(n_etac2s,n_psi2S,n_chic0,n_chic1,n_chic2,n_hc,n_psi3770))

    nBckgr_High = RooRealVar("nBckgr_High","num of bckgr",7e7,1e4,1.e+9)
    nBckgr_Low = RooRealVar("nBckgr_Low","num of bckgr",7e7,1e4,1.e+9)

    #Psi

    mass_Jpsi = RooRealVar("mass_Jpsi","mean of gaussian",cl.M_JPSI, 3030, 3150)
    mass_Jpsi.setConstant()

    mass_psi2S = RooRealVar("mass_psi2S","mean of gaussian",cl.M_PSI2S, 3680, 3692)
    #mass_psi2S.setConstant()
    mass_res_psi2S = RooRealVar("mass_res_psi2S","mean of gaussian", cl.M_RES_L_PSI2S)
    #mass_psi2S     = RooFormulaVar("mass_psi2S","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_psi2S))

    mass_psi3770 = RooRealVar("mass_psi3770","mean of gaussian",cl.M_PSI3770)
    mass_psi3770.setConstant()
    gamma_psi3770 = RooRealVar("gamma_psi3770","width of Br-W", cl.GAMMA_PSI3770)
    spin_psi3770 = RooRealVar("spin_psi3770","spin_psi3770", 1. )


    #eta_c

    #mass_etac = RooRealVar("mass_etac","mean of gaussian", cl.M_ETAC, 2980, 2988)
    #mass_etac.setConstant()
    mass_res_etac = RooRealVar("mass_res_etac","mean of gaussian", cl.M_RES_ETAC, 100, 125)
    mass_res_etac.setConstant()
    mass_etac     = RooFormulaVar("mass_etac","mean of gaussian","@0-@1",RooArgList(mass_Jpsi,mass_res_etac))
    gamma_etac    = RooRealVar("gamma_etac","width of Br-W", cl.GAMMA_ETAC)
    spin_etac     = RooRealVar("spin_etac","spin_eta", 0. )

    radius = RooRealVar("radius","radius", 1.)
    proton_m = RooRealVar("proton_m","proton mass", 938.3 )

    #mass_etac2s = RooRealVar("mass_etac2s","mean of gaussian", cl.M_ETAC2S)
    #mass_etac2s.setConstant()
    mass_res_etac2s = RooRealVar("mass_res_etac2s","mean of gaussian", cl.M_RES_H_ETAC2S, 30., 56.)
    mass_res_etac2s.setConstant()
    #mass_etac2s     = RooFormulaVar("mass_etac2s","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_etac2s))
    mass_etac2s     = RooFormulaVar("mass_etac2s","mean of gaussian","@0-@1",RooArgList(mass_psi2S,mass_res_etac2s))
    gamma_etac2s = RooRealVar("gamma_etac2s","width of Br-W", cl.GAMMA_ETAC2S)
    spin_etac2s = RooRealVar("spin_etac2s","spin_eta", 0. )


    #chi_c

    #mass_chic0 = RooRealVar("mass_chic0","mean of gaussian", cl.M_CHIC0)
    #mass_chic0.setConstant()
    mass_res_chic0 = RooRealVar("mass_res_chic0","mean of gaussian", cl.M_RES_L_CHIC0)
    mass_chic0     = RooFormulaVar("mass_chic0","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_chic0))
    gamma_chic0 = RooRealVar("gamma_chic0","width of Br-W", cl.GAMMA_CHIC0)
    spin_chic0 = RooRealVar("spin_chic0","spin_chic0", 0. )

    #mass_chic1 = RooRealVar("mass_chic1","mean of gaussian", cl.M_CHIC1)
    #mass_chic1.setConstant()
    mass_res_chic1 = RooRealVar("mass_res_chic1","mean of gaussian", cl.M_RES_L_CHIC1)
    mass_chic1     = RooFormulaVar("mass_chic1","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_chic1))
    gamma_chic1 = RooRealVar("gamma_chic1","width of Br-W", cl.GAMMA_CHIC1)
    spin_chic1 = RooRealVar("spin_chic1","spin_chic1", 1. )

    #mass_chic2 = RooRealVar("mass_chic2","mean of gaussian", cl.M_CHIC2)
    #mass_chic2.setConstant()
    mass_res_chic2 = RooRealVar("mass_res_chic2","mean of gaussian", cl.M_RES_L_CHIC2)
    mass_chic2     = RooFormulaVar("mass_chic2","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_chic2))
    gamma_chic2 = RooRealVar("gamma_chic2","width of Br-W", cl.GAMMA_CHIC2)
    spin_chic2 = RooRealVar("spin_chic2","spin_chic2", 2. )

    #h_c

    #mass_hc = RooRealVar("mass_hc","mean of gaussian", cl.M_HC)
    #mass_hc.setConstant()
    mass_res_hc = RooRealVar("mass_res_hc","mean of gaussian", cl.M_RES_L_HC)
    mass_hc  = RooFormulaVar("mass_hc","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_hc))
    gamma_hc = RooRealVar("gamma_hc","width of Br-W", cl.GAMMA_HC)
    spin_hc = RooRealVar("spin_hc","spin_hc", 1. )


    sigma_Jpsi = RooRealVar("sigma_Jpsi","width of gaussian", 9., 0.1, 50.)
    #sigma_etac2s = RooRealVar("sigma_etac2s","width of gaussian", 9., 0.1, 50.)
    sigma_etac = RooFormulaVar("sigma_etac","width of gaussian","@0*({}/{})**0.5".format(cl.M_ETAC,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_etac2s = RooFormulaVar("sigma_etac2s","width of gaussian","@0*({}/{})**0.5".format(cl.M_ETAC2S,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_chic0 = RooFormulaVar("sigma_chic0","width of gaussian","@0*({}/{})**0.5".format(cl.M_CHIC0,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_chic1 = RooFormulaVar("sigma_chic1","width of gaussian","@0*({}/{})**0.5".format(cl.M_CHIC1,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_chic2 = RooFormulaVar("sigma_chic2","width of gaussian","@0*({}/{})**0.5".format(cl.M_CHIC2,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_hc = RooFormulaVar("sigma_hc","width of gaussian","@0*({}/{})**0.5".format(cl.M_HC,cl.M_JPSI), RooArgList(sigma_Jpsi))
    #sigma_psi2S = RooRealVar("sigma_psi2S","width of gaussian", 9., 0.1, 50.)
    sigma_psi2S = RooFormulaVar("sigma_psi2S","width of gaussian","@0*({}/{})**0.5".format(cl.M_PSI2S,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_psi3770 = RooFormulaVar("sigma_psi3770","width of gaussian","@0*({}/{})**0.5".format(cl.M_PSI3770,cl.M_JPSI), RooArgList(sigma_Jpsi))


    #Fit signal

    gauss_Low = RooGaussian("gauss Low","gauss Low PDF", Jpsi_M_Low, RooFit.RooConst(0), sigma_Jpsi)
    gauss_High = RooGaussian("gauss Low","gauss Low PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_Jpsi)

    #eta_c
    gauss_etac = RooGaussian("gauss_etac","gauss PDF", Jpsi_M_Low, RooFit.RooConst(0), sigma_etac)
    br_wigner_etac = RooRelBreitWigner("br_wigner_etac", "br_wigner", Jpsi_M_Low, mass_etac, gamma_etac, spin_etac, radius, proton_m, proton_m)
    bwxg_etac = RooFFTConvPdf("bwxg_etac","breit-wigner (X) gauss", Jpsi_M_Low, br_wigner_etac, gauss_etac)

    gauss_etac2s = RooGaussian("gauss_etac2s","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_etac2s)
    br_wigner_etac2s = RooRelBreitWigner("br_wigner_etac2s", "br_wigner", Jpsi_M_High, mass_etac2s, gamma_etac2s, spin_etac2s, radius, proton_m, proton_m)
    bwxg_etac2s = RooFFTConvPdf("bwxg_etac2s","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_etac2s, gauss_etac2s)
    #bwxg = RooVoigtian("bwxg", "bwxg", Jpsi_M, mass_etac2s, gamma_etac2s, sigma_etac2s)

    #chi_c
    gauss_chic0 = RooGaussian("gauss_chic0","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_chic0)
    br_wigner_chic0 = RooRelBreitWigner("br_wigner_chic0", "br_wigner", Jpsi_M_High, mass_chic0, gamma_chic0, spin_chic0, radius, proton_m, proton_m)
    bwxg_chic0 = RooFFTConvPdf("bwxg_chic0","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_chic0, gauss_chic0)

    gauss_chic1 = RooGaussian("gauss_chic1","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_chic1)
    br_wigner_chic1 = RooRelBreitWigner("br_wigner_chic1", "br_wigner", Jpsi_M_High, mass_chic1, gamma_chic1, spin_chic1, radius, proton_m, proton_m)
    bwxg_chic1 = RooFFTConvPdf("bwxg_chic1","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_chic1, gauss_chic1)

    gauss_chic2 = RooGaussian("gauss_chic2","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_chic2)
    br_wigner_chic2 = RooRelBreitWigner("br_wigner_chic2", "br_wigner", Jpsi_M_High, mass_chic2, gamma_chic2, spin_chic2, radius, proton_m, proton_m)
    bwxg_chic2 = RooFFTConvPdf("bwxg_chic2","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_chic2, gauss_chic2)

    #h_c
    gauss_hc = RooGaussian("gauss_hc","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_hc)
    br_wigner_hc = RooRelBreitWigner("br_wigner_hc", "br_wigner", Jpsi_M_High, mass_hc, gamma_hc, spin_hc, radius, proton_m, proton_m)
    bwxg_hc = RooFFTConvPdf("bwxg_hc","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_hc, gauss_hc)


    #Fit J/psi
    gauss_Jpsi = RooGaussian("gauss_Jpsi","gaussian PDF", Jpsi_M_Low, mass_Jpsi, sigma_Jpsi)

    gauss_psi2S = RooGaussian("gauss_psi2S","gaussian PDF", Jpsi_M_High, mass_psi2S, sigma_psi2S)

    #br_wigner_psi3770 = RooRelBreitWigner("br_wigner_psi3770", "br_wigner", Jpsi_M, mass_psi3770, gamma_psi3770, spin_psi3770, radius, proton_m, proton_m)
    #bwxg_psi3770 = RooFFTConvPdf("bwxg_psi3770","breit-wigner (X) gauss", Jpsi_M, br_wigner_psi3770, gauss)
    bwxg_psi3770 = RooVoigtian("bwxg_psi3770", "bwxg_psi3770", Jpsi_M_High, mass_psi3770, gamma_psi3770, sigma_psi3770)



    eff_pppi0 = RooRealVar("eff_pppi0","eff_pppi0",0.06)
    pppi0 = RooGenericPdf("pppi0","Jpsi.pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M_Low))
    n_pppi0 = RooFormulaVar("n_pppi0","n_pppi0","@0*@1*(1.19/2.12)",RooArgList(n_Jpsi,eff_pppi0))

    #Bkg fit
    a0_High = RooRealVar("a0_High","a0",0.4,-2,2)
    a1_High = RooRealVar("a1_High","a1",0.05,-1.,1.)
    a2_High = RooRealVar("a2_High","a2",-0.005,-1,1.)


    a1_High.setVal(0.04)
    a2_High.setVal(0.04)

    parBkg_High = RooArgList(Jpsi_M_High, a0_High, a1_High, a2_High)
    bkg_High    = choose_bkg([kSource,kBkg], "High", parBkg_High)


    #Bkg fit
    a0_Low = RooRealVar("a0_Low","a0",0.4,-2.,2.)
    a1_Low = RooRealVar("a1_Low","a1",0.05,-1.,1.)
    a2_Low = RooRealVar("a2_Low","a2",-0.005,-1,1.)
    #a3_Low = RooRealVar("a3_Low","a2",-0.005,-1,1.)


    a1_Low.setVal(0.04)
    a2_Low.setVal(0.04)

    parBkg_Low = RooArgList(Jpsi_M_Low, a0_Low, a1_Low, a2_Low)
    bkg_Low    = choose_bkg([kSource,kBkg], "Low", parBkg_Low)

    #bkg = RooGenericPdf("bkg","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200)", RooArgList(Jpsi_M,a0,a1))
    #bkg = RooChebychev ("bkg_Low","Background",Jpsi_M,RooArgList(a0_Low,a1_Low,a2_Low,a3_Low))



    modelSignal_High = RooAddPdf("modelSignal_High","etac2s signal", RooArgList(bwxg_etac2s, bwxg_chic0, bwxg_chic1, bwxg_chic2, bwxg_hc, gauss_psi2S, bwxg_psi3770), RooArgList(n_etac2s, n_chic0, n_chic1, n_chic2, n_hc, n_psi2S, n_psi3770))
    modelSignal_etac2s = RooAddPdf("modelSignal_etac2s","etac2s signal", RooArgList(bwxg_etac2s), RooArgList(n_etac2s))
    modelBkg_High = RooAddPdf("modelBkg_High","etac2s bkg", RooArgList(bkg_High), RooArgList(nBckgr_High))
    model_High = RooAddPdf("model_High","High diapason signal", RooArgList(bkg_High, bwxg_etac2s, bwxg_chic0, bwxg_chic1, bwxg_chic2, bwxg_hc, gauss_psi2S, bwxg_psi3770), RooArgList(nBckgr_High, n_etac2s, n_chic0, n_chic1, n_chic2, n_hc, n_psi2S, n_psi3770))

    modelSignal_Low = RooAddPdf("modelSignal_Low","Jpsi signal", RooArgList(bwxg_etac, gauss_Jpsi), RooArgList(n_etac, n_Jpsi))
    modelSignal_Jpsi = RooAddPdf("modelSignal_Jpsi","Jpsi signal", RooArgList(gauss_Jpsi), RooArgList(n_Jpsi))
    modelBkg_Low = RooAddPdf("modelBkg_Low","Jpsi bkg", RooArgList(bkg_Low), RooArgList(nBckgr_Low))
    model_Low = RooAddPdf("model_Low","Low diapason signal", RooArgList(bwxg_etac,gauss_Jpsi,bkg_Low, pppi0), RooArgList(n_etac,n_Jpsi,nBckgr_Low, n_pppi0))



    model_High_constr = RooProdPdf("model_High_constr","model_High_constr", RooArgList(model_High, constr_ext))



    getattr(w,"import")(model_High,RooFit.RecycleConflictNodes())
    getattr(w,"import")(model_High_constr,RooFit.RecycleConflictNodes())
    getattr(w,"import")(model_Low,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelSignal_High,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelSignal_Low,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelBkg_High,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelBkg_Low,RooFit.RecycleConflictNodes())

    getattr(w,"import")(nSignal_Low,RooFit.RecycleConflictNodes())
    getattr(w,"import")(nSignal_High,RooFit.RecycleConflictNodes())

    #getattr(w,"import")(fconstr_etac, RooFit.RecycleConflictNodes())
    #getattr(w,"import")(n_etac_obs, RooFit.RecycleConflictNodes())



def getHists(w):

    nameWksp = dataDir + "/wksps/wksp_data_{}_ProbNN.root".format(kSource)
    file_w = TFile(nameWksp)
    w_in = file_w.Get("w")

    lowData    = w_in.data("dh_Low")
    highData   = w_in.data("dh_High")
    Jpsi_M     = w_in.var("Jpsi_M")

    histLow = lowData.createHistogram("histLow",Jpsi_M)
    nBins = histLow.GetNbinsX()
    minX =  histLow.GetXaxis().GetBinLowEdge(1)
    maxX =  histLow.GetXaxis().GetBinUpEdge(nBins)
    binWidth = histLow.GetBinWidth(1)
    firstBin = int((minM_Low - minX)/binWidth) + 1
    lastBin  = int((maxM_Low - minX)/binWidth)
    histLow.GetXaxis().SetRange(firstBin, lastBin)
    histLow.SaveAs("./hists/Low2018.root")

    histHigh = highData.createHistogram("histHigh",Jpsi_M)
    nBins = histHigh.GetNbinsX()
    minX =  histHigh.GetXaxis().GetBinLowEdge(1)
    maxX =  histHigh.GetXaxis().GetBinUpEdge(nBins)
    binWidth = histHigh.GetBinWidth(1)
    firstBin = int((minM_High - minX)/binWidth) + 1
    lastBin  = int((maxM_High - minX)/binWidth)
    histHigh.GetXaxis().SetRange(firstBin, lastBin)
    histHigh.SaveAs("./hists/High2018.root")


    Jpsi_M_Low  = RooRealVar("Jpsi_M_Low", "Jpsi_M_Low",  minM_Low, maxM_Low)
    Jpsi_M_High = RooRealVar("Jpsi_M_High","Jpsi_M_High", minM_High, maxM_High)

    dh_Low  = RooDataHist("dh_Low","dh_Low",  RooArgList(Jpsi_M_Low), histLow)
    dh_High = RooDataHist("dh_High","dh_High", RooArgList(Jpsi_M_High), histHigh)
    getattr(w,"import")(Jpsi_M_Low)
    getattr(w,"import")(Jpsi_M_High)
    getattr(w,"import")(dh_Low)
    getattr(w,"import")(dh_High)

    print("DATAHISTS READ SUCCESSFULLY")





def perform_splitted_fit(kSource,kBkg, kNorm="Jpsi" ):

    homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
    gStyle.SetStatFormat("4.3f");



    w = RooWorkspace("w")
    getHists(w)
    fillRelWorkspace(w, kSource, kBkg, kNorm)


    Jpsi_M_Low = w.var("Jpsi_M_Low")
    Jpsi_M_High = w.var("Jpsi_M_High")

    model_High    = w.pdf("model_High_constr")
    model_Low     = w.pdf("model_Low")

    modelBkg_High = w.pdf("modelBkg_High")
    modelBkg_Low  = w.pdf("modelBkg_Low")

    data_Low  = w.data("dh_Low")
    data_High = w.data("dh_High")


    if kSource=="prompt":
        f = TFile(homeDir+"/mass_fit/split_fit/FitMass_wksp_{}_fromB_{}_CS_ProbNN.root".format(kNorm, kBkg),"READ")
        w_fromB = f.Get("w")
        f.Close()
        sigma = w_fromB.var("sigma_Jpsi").getValV()
        w.var("sigma_Jpsi").setVal(sigma)
        w.var("sigma_Jpsi").setConstant(True)
        mass = w_fromB.var("mass_Jpsi").getValV()
        w.var("mass_Jpsi").setVal(mass)
        w.var("mass_Jpsi").setConstant(True)
    else:
        w.var("mass_Jpsi").setConstant(False)


    list = RooLinkedList()
    list.Add(RooCmdArg(RooFit.Extended(True)))
    list.Add(RooCmdArg(RooFit.Save(True)))
    list.Add(RooCmdArg(RooFit.Offset(True)))
    list.Add(RooCmdArg((RooFit.NumCPU(28))))


    fitresult_Low = model_Low.fitTo(data_Low, list)
    fitresult_Low = model_Low.fitTo(data_Low, list)
    w.var("sigma_Jpsi").setConstant(True)
    w.var("mass_Jpsi").setConstant(True)

    fitresult_High = model_High.fitTo(data_High, list)
    fitresult_High = model_High.fitTo(data_High, list)

    frame_Low,  resid_Low,  pull_Low = set_frame(w,"Low")
    frame_High, resid_High, pull_High = set_frame(w,"High")

    names  = ["Low","High"]
    frames = [frame_Low,frame_High]
    resids = [resid_Low,resid_High]
    pulls  = [pull_Low, pull_High]
    c = set_canvas(names, frames, resids, pulls)

    nameWksp = homeDir + "/mass_fit/split_fit/FitMass_wksp_{}_{}_{}_CS_ProbNN.root".format(kNorm,kSource,kBkg)
    namePic = homeDir + "/mass_fit/split_fit/FitMass_{}_{}_{}_CS_ProbNN.pdf".format(kNorm,kSource,kBkg)
    nameTxt = homeDir + "/mass_fit/split_fit/FitMass_result_{}_{}_{}_CS_ProbNN.txt".format(kNorm,kSource,kBkg)


    c.SaveAs(namePic)
    w.writeToFile(nameWksp)

    import os, sys
    save = os.dup( sys.stdout.fileno() )
    newout = open(nameTxt, "a" )
    os.dup2( newout.fileno(), sys.stdout.fileno() )
    # fitresult_Low.Print("v")
    fitresult_Low.Print()
    fitresult_Low.correlationMatrix().Print()

    # fitresult_High.Print("v")
    fitresult_High.Print()
    fitresult_High.correlationMatrix().Print()

    os.dup2( save, sys.stdout.fileno() )
    newout.close()
    print("Fit performed successfully !")

    return(w)





def calcUL_HighRangeOnly(w, kSource, kBkg):

    Jpsi_M_High = w.var("Jpsi_M_High")

    totalPdf_High = w.pdf("model_High_constr")
    data_High     = w.data("dh_High")

    var_list_0 = w.allVars()

    nuis_iter = var_list_0.createIterator()
    var = nuis_iter.Next()
    while var:
        var.setConstant(True)
        var = nuis_iter.Next()

    nBckgr_High = w.var("nBckgr_High")
    nBckgr_Low  = w.var("nBckgr_Low")
    n_Jpsi      = w.var("n_Jpsi")
    n_psi2S     = w.var("n_psi2S")
    n_etac      = w.var("n_etac")
    n_hc        = w.var("n_hc")
    n_chic0     = w.var("n_chic0")
    n_chic1     = w.var("n_chic1")
    n_chic2     = w.var("n_chic2")
    a0_Low      = w.var("a0_Low")
    a1_Low      = w.var("a1_Low")
    a2_Low      = w.var("a2_Low")
    a0_High     = w.var("a0_High")
    a1_High     = w.var("a1_High")
    a2_High     = w.var("a2_High")

    nBckgr_Low.setConstant()
    n_etac.setConstant()
    n_Jpsi.setConstant()
    nBckgr_High.setConstant(False)


    cs_etac2s_rel = w.var("cs_etac2s_rel")
    paramOfInterest = RooArgSet(cs_etac2s_rel)
    if kSource=="prompt":
        bgParams = RooArgSet(a0_High, a1_High, a2_High)
        cs_etac2s_rel.setRange(0., 5.)
        # cs_etac2s_rel.setRange(0., 3e4)

    else:
        bgParams = RooArgSet(a0_High, a1_High)
        #cs_etac2s_rel.setRange(0., 2.50)
        cs_etac2s_rel.setRange(0., 0.2)


    # n_etac, n_Jpsi, nBckgr_Low - can be completely independent from the fit of etac2s
    # we can just take gaussian constraint on n_Jpsi and others

    # it is not completely clear which variables should go to nuisances
    # subtle BUT very important
    # *rather* a0, a1, a2 should NOT go to nuisances ? to check in other analyses, etc.
    # if we want to use them - I would make gaussian constraint ?

    nuisanceParams = RooArgSet(n_hc, n_chic0, n_chic1, n_chic2, n_psi2S) # n_psi2S is not significant?
    nuis_iter = nuisanceParams.createIterator()
    var = nuis_iter.Next()
    band_size = 5.
    while var:
        if var.getVal()-band_size*var.getError() > 0:
            var.setRange(var.getVal()-band_size*var.getError(), var.getVal()+band_size*var.getError())
        else:
            var.setRange(0, var.getVal()+band_size*var.getError())
        print(var.GetName(), var.getVal()-band_size*var.getError(), var.getVal()+band_size*var.getError())
        var = nuis_iter.Next()

    # nBckgr_High should go to nuisances anyway
    bandsize_bkg = 10.
    nBckgr_High.setRange(nBckgr_High.getVal()-bandsize_bkg*nBckgr_High.getError(), nBckgr_High.getVal()+bandsize_bkg*nBckgr_High.getError())
    nuisanceParams.add(nBckgr_High)
    # nuisanceParams.add(bgParams) # it changes/breaks a lot -> only of reset range for a_i


    # can this UL be made for Extended() option ?
    # does it know about fitRange ?


    # such as BRs, efficiencies,
    # cross-section of Jpsi (if we want to extract UL on sigma_{\etac(2S)}),
    # cross-talks, yields from b-sample = everything with gaussian contraint
    # masses, widths also can be used here..


    modelConfig = RooStats.ModelConfig(w);
    modelConfig.SetPdf(totalPdf_High)
    modelConfig.SetProtoData(data_High)
    modelConfig.SetParametersOfInterest(paramOfInterest)
    modelConfig.SetNuisanceParameters(nuisanceParams)
    modelConfig.SetObservables(RooArgSet(RooArgList(Jpsi_M_High)))

    # I don't know what  ConstraintParameters are ...
    #   modelConfig.SetConstraintParameters(RooArgList(BR_Jpsi_mean))

    extFactor    = w.var("extFactor")
    globalParams = RooArgSet(extFactor)
    modelConfig.SetGlobalObservables(globalParams)

    modelConfig.SetName("ModelConfig")
    paramOfInterest.first().setVal(cs_etac2s_rel.getVal())
    modelConfig.SetSnapshot(paramOfInterest)
    getattr(w,"import")(modelConfig)
    modelConfig.Print()

    sbModel = w.obj("ModelConfig")
    poi = sbModel.GetParametersOfInterest().first()
    bModel = sbModel.Clone()
    bModel.SetName(sbModel.GetName()+"_with_poi_0")
    poi.setVal(0)
    bModel.SetSnapshot(RooArgSet(poi))

    fc = RooStats.FrequentistCalculator(data_High, bModel, sbModel)
    # do these values change anything ?
    fc.SetToys(1000,500)
    ac = RooStats.AsymptoticCalculator(data_High, bModel, sbModel)
    ac.SetOneSided(True)
    ac.SetPrintLevel(-1)

    calc = RooStats.HypoTestInverter(ac)
    calc.SetConfidenceLevel(0.95)

    calc.UseCLs(True)
    calc.SetVerbose(False)

    toymcs = calc.GetHypoTestCalculator().GetTestStatSampler()

    profll = RooStats.ProfileLikelihoodTestStat(sbModel.GetPdf())
    profll.SetOneSided(True)
    profll.SetLOffset(True)
    toymcs.SetTestStatistic(profll)

    if not sbModel.GetPdf().canBeExtended():
        toymcs.SetNEventsPerToy(1)
        print('can not be extended')

    npoints = 20
    poimin = poi.getMin()
    poimax = poi.getMax()


    calc.SetFixedScan(npoints,poimin,poimax)
    r = calc.GetInterval()
    upperLimit = r.UpperLimit()
    print("Exp Upper Limit is", r.GetExpectedUpperLimit())
    print("Upper Limit is", upperLimit)


    plot = RooStats.HypoTestInverterPlot("HTI_Result_Plot","HypoTest Scan Result",r)
    c = TCanvas("HypoTestInverter Scan")
    c.SetLogy(False)
    plot.Draw("CLb 2CL")

    homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"

    #plot.Draw("EXP")
    c.RangeAxis(poimin, 0., poimax, 1.1)
    c.Draw()
    c.SaveAs(homeDir + "/mass_fit/split_fit/CL_CS_Ac_{}_{}_ProbNN.root".format(kSource,kBkg))
    c.SaveAs(homeDir + "/mass_fit/split_fit/CL_CS_Ac_{}_{}_ProbNN.pdf".format(kSource,kBkg))


    # Construct binned likelihood
    nll = totalPdf_High.createNLL(data_High, RooFit.NumCPU(8), RooFit.Extended(True), RooFit.Offset(True))

    # Minimize likelihood w.r.t all parameters before making plots
    m = RooMinuit(nll)
    #m = RooMinimizer(nll)
    m.migrad()
    m.hesse()
    #m.minos()

    # Plot likelihood scan frac
    frameRS = cs_etac2s_rel.frame(RooFit.Bins(46),RooFit.Title("LL and profileLL in n_{#eta_{c}}(2S) "))
#     nll.plotOn(frameRS,RooFit.ShiftToZero())

    # The profile likelihood estimator on nll for frac will minimize nll w.r.t
    # all floating parameters except frac for each evaluation

    #pll_N_etac2s = nll.createProfile(RooArgSet(n_etac2s_rel))
    pll_N_etac2s = RooProfileLL("pll_N_etac2s", "pll_N_etac2s", nll, RooArgSet(cs_etac2s_rel))
    llhood = RooFormulaVar("llhood","exp(-0.5*pll_N_etac2s)",RooArgList(pll_N_etac2s));

    # Plot the profile likelihood in frac
    pll_N_etac2s.plotOn(frameRS,RooFit.LineColor(2))

    # Adjust frame maximum for visual clarity
    frameRS.SetMinimum(0)
    frameRS.GetXaxis().SetRangeUser(poimin, poimax)

    cNLL = TCanvas("rf605_profilell","rf605_profilell",800, 400)
    cNLL.cd(),  gPad.SetLeftMargin(0.15),  frameRS.GetYaxis().SetTitleOffset(1.4),  frameRS.Draw()

    cNLL.SaveAs(homeDir + "/mass_fit/NLL_CS_{}_{}_ProbNN.pdf".format(kSource,kBkg))

    nll.Write()
    pll_N_etac2s.Write()

    #f_nll.Write()
    #f_nll.Close()


    # sometimes it's REALLY slow
    fllhood = llhood.asTF(RooArgList(paramOfInterest))
    npoints = 20
    int_array = np.ones(npoints+1)
    int_val0 = fllhood.Integral(poimin, poimax, 1e-3)
    print(int_val0)
    poi_urs = np.linspace(poimin, poimax, npoints+1)

    for idx in range(npoints):

        int_val = fllhood.Integral(poimin, poi_urs[idx+1], 1e-6)
        int_array[idx+1] = 1.-int_val/int_val0
        if int_array[idx+1]<0.05: print(poi_urs[idx+1])

    print(int_array)
    gr_pval = TGraph(npoints+1, poi_urs, int_array)
    c.cd()
    gr_pval.SetLineColor(2)
    gr_pval.GetXaxis().SetRangeUser(poimin, poimax)
    gr_pval.Draw("same")

    c.SaveAs(homeDir + "/mass_fit/split_fit/CL_CS_Ac_check_{}_{}_ProbNN.pdf".format(kSource,kBkg))
    c.SaveAs(homeDir + "/mass_fit/split_fit/CL_CS_Ac_check_{}_{}_ProbNN.root".format(kSource,kBkg))






homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
#kBkgs = ["exp","cheb"]
kBkgs = ["cheb"]
kSources = ["fromB","prompt"]
kSources = ["fromB"]
kNorm = 'etac'

for kBkg in kBkgs:
    for kSource in kSources:
        w = perform_splitted_fit(kSource, kBkg, kNorm)
        #nameWksp = homeDir + "/mass_fit/split_fit/FitMass_wksp_{}_{}_{}_CS_ProbNN.root".format(kNorm,kSource,kBkg)
        #file_w = TFile(nameWksp)
        #w = file_w.Get("w")
        calcUL_HighRangeOnly(w,kSource,kBkg)
        del w
