from ROOT import gROOT
from ROOT import TFile
from ROOT import RooFit, RooRealVar, RooFormulaVar, RooArgList, RooArgSet, \
    RooChebychev, RooGenericPdf, RooGaussian, RooFFTConvPdf, RooAddPdf, RooProdPdf, RooVoigtian
# from ROOT import *
# from ROOT.RooFit import *
# from ROOT.RooStats import *

from drawModule_splitFit import *
import oniaConstLib as cl
gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../libs/libBifurcatedCB/BifurcatedCB.cxx+")
from ROOT import RooRelBreitWigner, BifurcatedCB
import numpy as np

#homeDir = "/afs/cern.ch/user/v/vazhovko/private/etac2s_ppbar/"
#dataDir = "/eos/user/v/vazhovko/etac2s_ppbar/"
homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

ExtMeas = {
       "BR_etac"        : { "val"    : 1.50e-3,
                            "err"    : 0.16e-3,
                            "relerr" : -1.},
       "BR_Jpsi"        : { "val"    : 2.121e-3,
                            "err"    : 0.03e-3,
                            "relerr" : -1.},
       "BR_etac2s"      : { "val"    : 7.7e-5,
                            "err"    : 2.4e-5,
                            "relerr" : -1.},
       "BR_psi2S"       : { "val"    : 2.88e-4,
                            "err"    : 0.09e-4,
                            "relerr" : -1.},
       "eff_etac2s2Jpsi": { "val"    : 1.065, # should be > 1 ~ 1.5 ?
                            "err"    : 0.008,
                            "relerr" : 0.02},
       "eff_etac2s2etac": { "val"    : 1.065, # should be > 1 ~ 1.5 ?
                            "err"    : 0.008,
                            "relerr" : 0.02},
       "n_Jpsi"         : { "val"    : 93914., # should be picked up in more smart way every time fit of Low is updated !
                            "err"    : 3100.,  # same
                            "relerr" : -1.},
       "n_etac"         : { "val"    : 96934., # should be picked up in more smart way every time fit of Low is updated !
                            "err"    : 7670.,  # same
                            "relerr" : -1.},
       "n_psi2S"        : { "val"    : 997., # should be picked up in more smart way every time fit of Low is updated !
                            "err"    : 69.,  # same
                            "relerr" : -1.}
    }


def upd_meas(w, kNorm):

    n_norm = "n_{}".format(kNorm)
    BR_norm = "BR_{}".format(kNorm)

    ExtMeas[n_norm]["val"] = w.var(n_norm).getValV()
    ExtMeas[n_norm]["err"] = w.var(n_norm).getError()
    ExtMeas[n_norm]["relerr"] = w.var(n_norm).getError()/w.var(n_norm).getValV()

    from ROOT.TMath import Sqrt, Sq
    total_val =  ExtMeas[n_norm]["val"]*ExtMeas["eff_etac2s2Jpsi"]["val"]*ExtMeas["BR_etac2s"]["val"]/ExtMeas[BR_norm]["val"]

    total_relerr = 0.
    for source in [BR_norm,"BR_etac2s","eff_etac2s2Jpsi",n_norm]:
        meas = ExtMeas[source]
        val  = meas["val"]
        if meas["relerr"] != -1. :
            relerr = meas["relerr"]
        else:
            relerr = meas["err"]/val
        total_relerr += Sq(relerr)
    total_relerr = Sqrt(total_relerr)
    total_err = total_relerr * total_val

    #total_val =  ExtMeas[n_norm]["val"]*ExtMeas["eff_etac2s2Jpsi"]["val"]

    #total_relerr = 0.
    #for source in ["eff_etac2s2Jpsi",n_norm]:
        #meas = ExtMeas[source]
        #val  = meas["val"]
        #if meas["relerr"] != -1. :
            #relerr = meas["relerr"]
        #else:
            #relerr = meas["err"]/val
        #total_relerr += Sq(relerr)
    #total_relerr = Sqrt(total_relerr)
    #total_err = total_relerr * total_val

    print(" OVERAL factor is      :", total_val)
    print(" WITH relative error   :", total_relerr)
    return total_val, total_relerr, total_err


def choose_bkg(optList, regName, argList):
    return {

        "prompt cheb" : RooChebychev ("bkg_{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2),argList.at(3))),
        "fromB cheb"  : RooChebychev ("bkg_{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2),argList.at(3))),
        "prompt exp"  : RooGenericPdf("bkg_{}".format(regName),"background","TMath::Exp(-(@0-3550.)/200.*@1)*(1.+@2*(@0-3550.)/200.+@3*(@0-3550.)*(@0-3550.)/200./200.)", RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3))) ,
        "fromB exp"   : RooGenericPdf("bkg_{}".format(regName),"background","TMath::Exp(-(@0-3550.)/200.*@1)*(1.+@2*(@0-3550.)/200)", RooArgList(argList.at(0), argList.at(1),argList.at(2)))

    }[optList[0] + " " + optList[1]]


def get_const_par(kSyst):

    kBkg = "cheb"
    eff_pi0 = 0.057

    if kSyst=="Exp":
        kBkg="exp"
    elif kSyst=="Cheb4":
        kBkg="cheb4"
    elif kSyst=="eff_pppi0":
        eff_pi0 = 0.057*(1+0.09)


    return kBkg, eff_pi0

def fillWksp_etac2S(w, kSource, kNorm="Jpsi", kSyst="Base"):

    total_val, total_relerr, total_err = upd_meas(w, kNorm)

    Jpsi_M_High = w.var("Jpsi_M_High")

    extFactor         = RooRealVar("extFactor",  "extFactor", total_val, 0, 100*total_val)
    constr_ext        = RooGaussian("constr_ext","constr_ext", extFactor, RooFit.RooConst(total_val), RooFit.RooConst(total_err))

    # n_etac2s_rel = RooRealVar("n_etac2s_rel","num of Etac", 1.5e-3, 0, 4.)
    # n_etac2s = RooRealVar("n_etac2s","num of etac2s", 2e3, 0, 3.e4)
    cs_etac2s_rel = RooRealVar("cs_etac2s_rel","num of Etac", 1e-2, 0, 5.0)
    n_etac2s = RooFormulaVar("n_etac2s","num of Etac","@0*@1", RooArgList(cs_etac2s_rel, extFactor))


    radius = w.var("radius")
    proton_m = w.var("proton_m")
    mass_psi2S = w.var("mass_psi2S")

    #mass_etac2s = RooRealVar("mass_etac2s","mean of gaussian", cl.M_ETAC2S)
    mass_res_etac2s = RooRealVar("mass_res_etac2s","mean of gaussian", cl.M_RES_H_ETAC2S, 30., 56.)
    mass_res_etac2s.setConstant()
    mass_etac2s     = RooFormulaVar("mass_etac2s","mean of gaussian","@0-@1",RooArgList(mass_psi2S,mass_res_etac2s))
    gamma_etac2s = RooRealVar("gamma_etac2s","width of Br-W", cl.GAMMA_ETAC2S)
    spin_etac2s = RooRealVar("spin_etac2s","spin_eta", 0. )

    #sigma_scale = (cl.M_ETAC2S/cl.M_PSI2S)**0.5

    #sigma = w.var("sigma_etac")
    sigma = w.var("sigma_Jpsi")
    modName = "_CB" if kSyst=="CB" else ""
    f = TFile("{}//MC/mass_fit/MC_M_res_all_jpsi_wksp_sim{}.root".format(homeDir,modName))
    w_MC = f.Get("w")
    sigma_scale = w_MC.var("r_ref_etac2S").getValV()
    sigma_etac2s = RooFormulaVar("sigma_etac2s","width of gaussian","@0*{}".format(sigma_scale), RooArgList(sigma))


    if kSyst=="CB":
        resolution_psi2S = w.pdf("cb_psi2S")
        alpha = w.var("alpha")
        nCB = w.var("nCB")
        resolution_etac2s = BifurcatedCB("cb_etac2s", "Cystal Ball Function", Jpsi_M_High, RooFit.RooConst(0), sigma_etac2s, alpha, nCB, alpha, nCB)
    else:
        resolution_psi2S = w.pdf("gauss_psi2S")
        resolution_etac2s = RooGaussian("gauss_etac2s","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_etac2s)


    br_wigner_etac2s = RooRelBreitWigner("br_wigner_etac2s", "br_wigner", Jpsi_M_High, mass_etac2s, gamma_etac2s, spin_etac2s, radius, proton_m, proton_m)
    bwxg_etac2s = RooFFTConvPdf("bwxg_etac2s","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_etac2s, resolution_etac2s)

    modelSignal_High_Lim = w.pdf("modelSignal_High_Lim")
    model_High_Lim = w.pdf("model_High_Lim")


    #n_chic0, n_chic1, n_chic2, n_hc = w.var("n_chic0"), w.var("n_chic1"), w.var("n_chic2"), w.var("n_hc")
    n_chic0, n_chic1, n_chic2, n_hc = w.function("n_chic0"), w.function("n_chic1"), w.function("n_chic2"), w.function("n_hc")
    n_psi2S, n_psi3770 = w.var("n_psi2S"), w.var("n_psi3770")
    bwxg_chic0, bwxg_chic1, bwxg_chic2, bwxg_hc,  bwxg_psi3770 = w.pdf("bwxg_chic0"), w.pdf("bwxg_chic1"), w.pdf("bwxg_chic2"), w.pdf("bwxg_hc"), w.pdf("bwxg_psi3770")


    #yields = w.argSet("yields")
    #pdfs = w.argSet("pdfs")
    yields = RooArgSet(n_chic0, n_chic1, n_chic2, n_hc, n_psi2S, n_psi3770, "yields")
    pdfs   = RooArgSet(bwxg_chic0, bwxg_chic1, bwxg_chic2, bwxg_hc, resolution_psi2S, bwxg_psi3770, "pdfs")
    yields.add(n_etac2s)
    pdfs.add(bwxg_etac2s)
    bkg_High = w.pdf("bkg_High")
    nBckgr_High = w.var("nBckgr_High")


    modelSignal_High = RooAddPdf("modelSignal_High","etac2s signal", RooArgList(pdfs), RooArgList(yields))
    yields.add(nBckgr_High)
    pdfs.add(bkg_High)
    model_High = RooAddPdf("model_High","High diapason signal", RooArgList(pdfs), RooArgList(yields))

    model_High_constr = RooProdPdf("model_High_constr","model_High_constr", RooArgList(model_High, constr_ext))

    getattr(w,"import")(model_High,RooFit.RecycleConflictNodes())
    getattr(w,"import")(model_High_constr,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelSignal_High,RooFit.RecycleConflictNodes())


def fillWksp_High(w, kSource, kNorm="Jpsi", kSyst="Base"):

    #kBkg, eff_pi0 = get_const_par(kSys)

    Jpsi_M_High = w.var("Jpsi_M")
    if Jpsi_M_High==None:
        Jpsi_M_High = w.var("Jpsi_M_High")

    cs_Jpsi = w.var("cs_Jpsi")

    n_psi2S = RooRealVar("n_psi2S","num of psi2S", 1e3, 0, 1.e4)
    n_psi3770 = RooRealVar("n_psi3770","num of psi3770", 1e1, 0, 1.e5)
    n_psi3770.setVal(0.); n_psi3770.setConstant()

    #n_chic0 = RooRealVar("n_chic0","num of chic0", 5e2, 0, 2.5e4)
    #n_chic1 = RooRealVar("n_chic1","num of chic1", 5e2, 0, 2.5e4)
    #n_chic2 = RooRealVar("n_chic2","num of chic2", 1e2, 0, 2.5e4)
    #n_hc = RooRealVar("n_hc","num of hc", 1e2, 0, 1.e4)

    n_hc_rel    = RooRealVar("n_hc_rel","num of chic0", 0.5, 0, 3.)
    n_chic0_rel = RooRealVar("n_chic0_rel","num of chic0", 0.5, 0, 3.)
    n_chic1_rel = RooRealVar("n_chic1_rel","num of chic1", 0.5, 0, 3.)
    n_chic2_rel = RooRealVar("n_chic2_rel","num of chic2", 0.6, 0, 3.)
    n_hc    = RooFormulaVar("n_hc","num of chic0","@0*@1", RooArgList(n_hc_rel,n_psi2S))
    n_chic0 = RooFormulaVar("n_chic0","num of chic0","@0*@1", RooArgList(n_chic0_rel,n_psi2S))
    n_chic1 = RooFormulaVar("n_chic1","num of chic1","@0*@1", RooArgList(n_chic1_rel,n_psi2S))
    n_chic2 = RooFormulaVar("n_chic2","num of chic2","@0*@1", RooArgList(n_chic2_rel,n_psi2S))

    nSignal_High = RooFormulaVar("nSignal_High","num of Etac","@0+@1+@2+@3+@4", RooArgList(n_psi2S,n_chic0,n_chic1,n_chic2,n_hc)) #,n_psi3770

    nBckgr_High = RooRealVar("nBckgr_High","num of bckgr",7e7,1e4,1.e+9)

    #Psi

    mass_psi2S = RooRealVar("mass_psi2S","mean of gaussian",cl.M_PSI2S, 3680, 3692)
    mass_res_psi2S = RooRealVar("mass_res_psi2S","mean of gaussian", cl.M_RES_L_PSI2S)
    #mass_psi2S     = RooFormulaVar("mass_psi2S","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_psi2S))

    mass_psi3770 = RooRealVar("mass_psi3770","mean of gaussian",cl.M_PSI3770)
    mass_psi3770.setConstant()
    gamma_psi3770 = RooRealVar("gamma_psi3770","width of Br-W", cl.GAMMA_PSI3770)
    spin_psi3770 = RooRealVar("spin_psi3770","spin_psi3770", 1. )

    radius = w.var("radius")
    proton_m = w.var("proton_m")
    if radius==None:
        radius = RooRealVar("radius","radius", 1.)
    if proton_m==None:
        proton_m = RooRealVar("proton_m","proton mass", cl.M_PROTON )

    #chi_c

    mass_chic0 = RooRealVar("mass_chic0","mean of gaussian", cl.M_CHIC0, cl.M_CHIC0-10, cl.M_CHIC0+10)
    #mass_chic0.setConstant()
    #mass_res_chic0 = RooRealVar("mass_res_chic0","mean of gaussian", cl.M_RES_H_CHIC0)
    #mass_chic0     = RooFormulaVar("mass_chic0","mean of gaussian","@0-@1",RooArgList(mass_psi2S,mass_res_chic0))
    gamma_chic0 = RooRealVar("gamma_chic0","width of Br-W", cl.GAMMA_CHIC0)
    spin_chic0 = RooRealVar("spin_chic0","spin_chic0", 0. )

    mass_chic1 = RooRealVar("mass_chic1","mean of gaussian", cl.M_CHIC1, cl.M_CHIC1-10, cl.M_CHIC1+10)
    #mass_chic1.setConstant()
    #mass_res_chic1 = RooRealVar("mass_res_chic1","mean of gaussian", cl.M_RES_H_CHIC1)
    #mass_chic1     = RooFormulaVar("mass_chic1","mean of gaussian","@0-@1",RooArgList(mass_psi2S,mass_res_chic1))
    gamma_chic1 = RooRealVar("gamma_chic1","width of Br-W", cl.GAMMA_CHIC1)
    spin_chic1 = RooRealVar("spin_chic1","spin_chic1", 1. )

    mass_chic2 = RooRealVar("mass_chic2","mean of gaussian", cl.M_CHIC2)
    #mass_chic2.setConstant()
    #mass_res_chic2 = RooRealVar("mass_res_chic2","mean of gaussian", cl.M_RES_H_CHIC2)
    #mass_chic2     = RooFormulaVar("mass_chic2","mean of gaussian","@0-@1",RooArgList(mass_psi2S,mass_res_chic2))
    gamma_chic2 = RooRealVar("gamma_chic2","width of Br-W", cl.GAMMA_CHIC2)
    spin_chic2 = RooRealVar("spin_chic2","spin_chic2", 2. )

    #h_c

    mass_hc = RooRealVar("mass_hc","mean of gaussian", cl.M_HC)
    #mass_hc.setConstant()
    #mass_res_hc = RooRealVar("mass_res_hc","mean of gaussian", cl.M_RES_H_HC)
    #mass_hc  = RooFormulaVar("mass_hc","mean of gaussian","@0-@1",RooArgList(mass_psi2S,mass_res_hc))
    gamma_hc = RooRealVar("gamma_hc","width of Br-W", cl.GAMMA_HC)
    spin_hc = RooRealVar("spin_hc","spin_hc", 1. )

    modName = "_CB" if kSyst=="CB" else ""

    # f = TFile("{}//MC/mass_fit/Wksp_M_res_all_jpsi_sim{}.root".format(homeDir,modName))
    f = TFile("{}//MC/mass_fit/Wksp_M_res_all_psi2S_sim{}.root".format(homeDir,modName))
    w_MC = f.Get("w")
    r_etac2s = w_MC.var("r_ref_etac2S")
    r_chic0 = w_MC.var("r_ref_chic0")
    r_chic1 = w_MC.var("r_ref_chic1")
    r_chic2 = w_MC.var("r_ref_chic2")
    r_etac2s.setConstant(True)
    r_chic0.setConstant(True)
    r_chic1.setConstant(True)
    r_chic2.setConstant(True)


    #sigma_psi2S = RooRealVar("sigma_psi2S","width of gaussian", 11., 5.1, 30.)
    #sigma_etac2s = RooFormulaVar("sigma_etac2s","width of gaussian","@0*({}/{})**0.5".format(cl.M_ETAC2S,cl.M_PSI2S), RooArgList(sigma_psi2S))
    #sigma_chic0 = RooFormulaVar("sigma_chic0","width of gaussian","@0*({}/{})**0.5".format(cl.M_CHIC0,cl.M_PSI2S), RooArgList(sigma_psi2S))
    #sigma_chic1 = RooFormulaVar("sigma_chic1","width of gaussian","@0*({}/{})**0.5".format(cl.M_CHIC1,cl.M_PSI2S), RooArgList(sigma_psi2S))
    #sigma_chic2 = RooFormulaVar("sigma_chic2","width of gaussian","@0*({}/{})**0.5".format(cl.M_CHIC2,cl.M_PSI2S), RooArgList(sigma_psi2S))
    if kNorm=="Jpsi":
        r_psi2S = w_MC.var("r_ref_psi2S")
        r_psi2S.setConstant(True)
        sigma = w.var("sigma_Jpsi")
        sigma_psi2S = RooFormulaVar("sigma_psi2S","width of gaussian","@0*@1", RooArgList(sigma,r_psi2S))
    else:
        sigma_psi2S = RooRealVar("sigma_psi2S","width of gaussian", 11., 5.1, 30.)
        sigma = sigma_psi2S

    sigma_etac2s = RooFormulaVar("sigma_etac2s","width of gaussian","@0*@1", RooArgList(sigma,r_etac2s))
    sigma_chic0 = RooFormulaVar("sigma_chic0","width of gaussian","@0*@1", RooArgList(sigma,r_chic0))
    sigma_chic1 = RooFormulaVar("sigma_chic1","width of gaussian","@0*@1", RooArgList(sigma,r_chic1))
    sigma_chic2 = RooFormulaVar("sigma_chic2","width of gaussian","@0*@1", RooArgList(sigma,r_chic2))

    sigma_hc = RooFormulaVar("sigma_hc","width of gaussian","@0*({}/{})**0.5".format(cl.M_HC,cl.M_PSI2S), RooArgList(sigma_psi2S))
    sigma_psi3770 = RooFormulaVar("sigma_psi3770","width of gaussian","@0*({}/{})**0.5".format(cl.M_PSI3770,cl.M_PSI2S), RooArgList(sigma_psi2S))


    #sigma_Jpsi = w.var("sigma_Jpsi")
    #sigma_etac2s = RooFormulaVar("sigma_etac2s","width of gaussian","@0*({}/{})**0.5".format(cl.M_ETAC2S,cl.M_JPSI), RooArgList(sigma_Jpsi))
    #sigma_chic0 = RooFormulaVar("sigma_chic0","width of gaussian","@0*({}/{})**0.5".format(cl.M_CHIC0,cl.M_JPSI), RooArgList(sigma_Jpsi))
    #sigma_chic1 = RooFormulaVar("sigma_chic1","width of gaussian","@0*({}/{})**0.5".format(cl.M_CHIC1,cl.M_JPSI), RooArgList(sigma_Jpsi))
    #sigma_chic2 = RooFormulaVar("sigma_chic2","width of gaussian","@0*({}/{})**0.5".format(cl.M_CHIC2,cl.M_JPSI), RooArgList(sigma_Jpsi))
    #sigma_hc = RooFormulaVar("sigma_hc","width of gaussian","@0*({}/{})**0.5".format(cl.M_HC,cl.M_JPSI), RooArgList(sigma_Jpsi))
    #sigma_psi2S = RooFormulaVar("sigma_psi2S","width of gaussian","@0*({}/{})**0.5".format(cl.M_PSI2S,cl.M_JPSI), RooArgList(sigma_Jpsi))
    #sigma_psi3770 = RooFormulaVar("sigma_psi3770","width of gaussian","@0*({}/{})**0.5".format(cl.M_PSI3770,cl.M_JPSI), RooArgList(sigma_Jpsi))


    #Fit signal

    #Resolution model
    if kSyst=="CB":
        alpha = w.var("alpha")
        nCB = w.var("nCB")
        resolution_chic0 = BifurcatedCB("cb_chic0", "Cystal Ball Function", Jpsi_M_High, RooFit.RooConst(0), sigma_chic0, alpha, nCB, alpha, nCB)
        resolution_chic1 = BifurcatedCB("cb_chic1", "Cystal Ball Function", Jpsi_M_High, RooFit.RooConst(0), sigma_chic1, alpha, nCB, alpha, nCB)
        resolution_chic2 = BifurcatedCB("cb_chic2", "Cystal Ball Function", Jpsi_M_High, RooFit.RooConst(0), sigma_chic2, alpha, nCB, alpha, nCB)
        resolution_hc    = BifurcatedCB("cb_hc",    "Cystal Ball Function", Jpsi_M_High, RooFit.RooConst(0), sigma_hc, alpha, nCB, alpha, nCB)
        resolution_psi2S = BifurcatedCB("cb_psi2S", "Cystal Ball Function", Jpsi_M_High, mass_psi2S, sigma_psi2S, alpha, nCB, alpha, nCB)

    elif kSyst=="Resolution":
        r_NToW = 0.233
        sigma_psi2S_w = RooFormulaVar("sigma_psi2S_w","width of gaussian","@0/{}".format(r_NToW), RooArgList(sigma_psi2S))
        sigma_chic0_w = RooFormulaVar("sigma_chic0_w","width of gaussian","@0/{}".format(r_NToW), RooArgList(sigma_chic0))
        #sigma_chic1_w = RooFormulaVar("sigma_chic1_w","width of gaussian","@0*{}".format(r_NToW), RooArgList(sigma_chic1))
        #sigma_chic2_w = RooFormulaVar("sigma_chic2_w","width of gaussian","@0*{}".format(r_NToW), RooArgList(sigma_chic2))

        r_G1ToG2 = 0.939
        rG1toG2 = RooRealVar("rG1toG2","rG1toG2",r_G1ToG2)

        resolution_chic0_n = RooGaussian("gauss_chic0_n","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_chic0)
        #resolution_chic1_n = RooGaussian("gauss_chic1_n","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_chic1)
        #resolution_chic2_n = RooGaussian("gauss_chic2_n","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_chic2)
        resolution_psi2S_n = RooGaussian("gauss_psi2S_n","gaussian PDF", Jpsi_M_High, mass_psi2S, sigma_psi2S)

        resolution_chic0_w = RooGaussian("gauss_chic0_w","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_chic0_w)
        #resolution_chic1_w = RooGaussian("gauss_chic1_w","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_chic1_w)
        #resolution_chic2_w = RooGaussian("gauss_chic2_w","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_chic2_w)
        resolution_psi2S_w = RooGaussian("gauss_psi2S_w","gaussian PDF", Jpsi_M_High, mass_psi2S, sigma_psi2S_w)

        resolution_chic0 = RooAddPdf("gauss_chic0","gauss PDF", RooArgList(resolution_chic0_n,resolution_chic0_w), RooArgList(rG1toG2))
        #resolution_chic1 = RooAddPdf("gauss_chic1","gauss PDF", RooArgList(resolution_chic1_n,resolution_chic1_w), RooArgList(rG1toG2))
        #resolution_chic2 = RooAddPdf("gauss_chic2","gauss PDF", RooArgList(resolution_chic2_n,resolution_chic2_w), RooArgList(rG1toG2))
        resolution_psi2S = RooAddPdf("gauss_psi2S","gauss PDF", RooArgList(resolution_psi2S_n,resolution_psi2S_w), RooArgList(rG1toG2))

        #resolution_chic0 = RooGaussian("gauss_chic0","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_chic0)
        resolution_chic1 = RooGaussian("gauss_chic1","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_chic1)
        resolution_chic2 = RooGaussian("gauss_chic2","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_chic2)
        resolution_hc = RooGaussian("gauss_hc","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_hc)


    else:
        resolution_chic0 = RooGaussian("gauss_chic0","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_chic0)
        resolution_chic1 = RooGaussian("gauss_chic1","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_chic1)
        resolution_chic2 = RooGaussian("gauss_chic2","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_chic2)
        resolution_hc = RooGaussian("gauss_hc","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_hc)

        #Fit psi(2S)
        resolution_psi2S = RooGaussian("gauss_psi2S","gaussian PDF", Jpsi_M_High, mass_psi2S, sigma_psi2S)
    #br_wigner_psi3770 = RooRelBreitWigner("br_wigner_psi3770", "br_wigner", Jpsi_M, mass_psi3770, gamma_psi3770, spin_psi3770, radius, proton_m, proton_m)
    #bwxg_psi3770 = RooFFTConvPdf("bwxg_psi3770","breit-wigner (X) gauss", Jpsi_M, br_wigner_psi3770, gauss)
    bwxg_psi3770 = RooVoigtian("bwxg_psi3770", "bwxg_psi3770", Jpsi_M_High, mass_psi3770, gamma_psi3770, sigma_psi3770)

    #chi_c
    br_wigner_chic0 = RooRelBreitWigner("br_wigner_chic0", "br_wigner", Jpsi_M_High, mass_chic0, gamma_chic0, spin_chic0, radius, proton_m, proton_m)
    bwxg_chic0 = RooFFTConvPdf("bwxg_chic0","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_chic0, resolution_chic0)
    #bwxg_chic0_n = RooFFTConvPdf("bwxg_chic0_n","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_chic0, resolution_chic0_n)
    #bwxg_chic0_w = RooFFTConvPdf("bwxg_chic0_w","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_chic0, resolution_chic0_w)

    br_wigner_chic1 = RooRelBreitWigner("br_wigner_chic1", "br_wigner", Jpsi_M_High, mass_chic1, gamma_chic1, spin_chic1, radius, proton_m, proton_m)
    bwxg_chic1 = RooFFTConvPdf("bwxg_chic1","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_chic1, resolution_chic1)
    #bwxg_chic1_n = RooFFTConvPdf("bwxg_chic1_n","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_chic1, resolution_chic1_n)
    #bwxg_chic1_w = RooFFTConvPdf("bwxg_chic1_w","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_chic1, resolution_chic1_w)

    br_wigner_chic2 = RooRelBreitWigner("br_wigner_chic2", "br_wigner", Jpsi_M_High, mass_chic2, gamma_chic2, spin_chic2, radius, proton_m, proton_m)
    bwxg_chic2 = RooFFTConvPdf("bwxg_chic2","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_chic2, resolution_chic2)
    #bwxg_chic2_n = RooFFTConvPdf("bwxg_chic2_n","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_chic2, resolution_chic2_n)
    #bwxg_chic2_w = RooFFTConvPdf("bwxg_chic2_w","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_chic2, resolution_chic2_w)

    #h_c
    br_wigner_hc = RooRelBreitWigner("br_wigner_hc", "br_wigner", Jpsi_M_High, mass_hc, gamma_hc, spin_hc, radius, proton_m, proton_m)
    bwxg_hc = RooFFTConvPdf("bwxg_hc","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_hc, resolution_hc)





    # eff_pppi0 = w.var("eff_pppi0")
    # pppi0_High = RooGenericPdf("pppi0_High","psi2S.pppi0","@0<(3551.12) ? TMath::Sqrt(3686.097-134.977-@0) : 0",RooArgList(Jpsi_M_High))
    # n_pppi0_High = RooFormulaVar("n_pppi0_High","n_pppi0_High","@0*@1*(1.53/2.94)",RooArgList(n_psi2S,eff_pppi0))

    if kSyst=="Exp": kBkg = "exp"
    else:            kBkg = "cheb"

    #Bkg fit
    a0_High = RooRealVar("a0_High","a0",-0.6, -2.,2.)
    a1_High = RooRealVar("a1_High","a1", 0.1, -1.,1.)
    a2_High = RooRealVar("a2_High","a2",-0.05,-1.,1.)

    parBkg_High = RooArgList(Jpsi_M_High, a0_High, a1_High, a2_High)
    #bkg_High    = choose_bkg([kSource,kBkg], "High", parBkg_High)
    bkg_High    = choose_bkg(["prompt",kBkg], "High", parBkg_High)


    # modelSignal_High = RooAddPdf("modelSignal_High_Lim","etac2s signal", RooArgList(bwxg_chic0, bwxg_chic1, bwxg_chic2, bwxg_hc, resolution_psi2S, bwxg_psi3770), RooArgList(n_chic0, n_chic1, n_chic2, n_hc, n_psi2S, n_psi3770))
    # modelBkg_High = RooAddPdf("modelBkg_High","etac2s bkg", RooArgList(bkg_High), RooArgList(nBckgr_High))
    # model_High = RooAddPdf("model_High_Lim","High diapason signal", RooArgList(bkg_High, bwxg_chic0, bwxg_chic1, bwxg_chic2, bwxg_hc, resolution_psi2S, bwxg_psi3770), RooArgList(nBckgr_High, n_chic0, n_chic1, n_chic2, n_hc, n_psi2S, n_psi3770))

    # modelSignal_High = RooAddPdf("modelSignal_High_Lim","etac2s signal", RooArgList(bwxg_chic0, bwxg_chic1, bwxg_chic2, bwxg_hc, resolution_psi2S), RooArgList(n_chic0, n_chic1, n_chic2, n_hc, n_psi2S))
    # modelBkg_High = RooAddPdf("modelBkg_High","etac2s bkg", RooArgList(bkg_High), RooArgList(nBckgr_High))
    # model_High = RooAddPdf("model_High_Lim","High diapason signal", RooArgList(bkg_High, bwxg_chic0, bwxg_chic1, bwxg_chic2, bwxg_hc, resolution_psi2S), RooArgList(nBckgr_High, n_chic0, n_chic1, n_chic2, n_hc, n_psi2S))

    modelSignal_High = RooAddPdf("modelSignal_High","etac2s signal", RooArgList(bwxg_chic0, bwxg_chic1, bwxg_chic2, bwxg_hc, resolution_psi2S), RooArgList(n_chic0, n_chic1, n_chic2, n_hc, n_psi2S))
    modelBkg_High = RooAddPdf("modelBkg_High","etac2s bkg", RooArgList(bkg_High), RooArgList(nBckgr_High))
    model_High = RooAddPdf("model_High","High diapason signal", RooArgList(bkg_High, bwxg_chic0, bwxg_chic1, bwxg_chic2, bwxg_hc, resolution_psi2S), RooArgList(nBckgr_High, n_chic0, n_chic1, n_chic2, n_hc, n_psi2S))

    getattr(w,"import")(model_High,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelSignal_High,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelBkg_High,RooFit.RecycleConflictNodes())


def fillWksp_Low(w, kSource, kSyst="Base"):

    kBkg, eff_pi0 = get_const_par(kSyst)

    # Jpsi_M_Low  = w.var("Jpsi_M_Low")
    Jpsi_M_Low  = w.var("Jpsi_M")

    cs_Jpsi = RooRealVar("cs_Jpsi","cs Jpsi", 0.749)


    n_Jpsi = RooRealVar("n_Jpsi","num of J/Psi", 2e3, 10, 1.e6)
    n_etac = RooRealVar("n_etac","num of etac", 1e4, 10, 3.e6)

    nSignal_Low = RooFormulaVar("nSignal_Low","num of Etac","@0+@1", RooArgList(n_etac,n_Jpsi))

    nBckgr_Low = RooRealVar("nBckgr_Low","num of bckgr",7e7,1e4,1.e+9)

    #Psi

    mass_Jpsi = RooRealVar("mass_Jpsi","mean of gaussian",cl.M_JPSI, 3030, 3150)
    mass_Jpsi.setConstant()


    #eta_c

    #mass_etac = RooRealVar("mass_etac","mean of gaussian", cl.M_ETAC, 2980, 2988)
    #mass_etac.setConstant()
    mass_res_etac = RooRealVar("mass_res_etac","mean of gaussian", cl.M_RES_ETAC, 100, 125)
    mass_res_etac.setConstant()
    mass_etac     = RooFormulaVar("mass_etac","mean of gaussian","@0-@1",RooArgList(mass_Jpsi,mass_res_etac))
    gamma_etac    = RooRealVar("gamma_etac","width of Br-W", cl.GAMMA_ETAC)
    spin_etac     = RooRealVar("spin_etac","spin_eta", 0. )

    radius = RooRealVar("radius","radius", 1.)
    proton_m = RooRealVar("proton_m","proton mass", cl.M_PROTON )


    modName = "_CB" if kSyst=="CB" else ""
    f = TFile("{}//MC/mass_fit/Wksp_M_res_all_jpsi_sim{}.root".format(homeDir,modName))
    w_MC = f.Get("w")
    #r_jpsi = w_MC.var("r_ref_jpsi")
    #r_jpsi.setConstant(True)
    #sigma_etac = RooRealVar("sigma_etac","width of gaussian", 9., 0.1, 50.)
    #sigma_Jpsi= RooFormulaVar("sigma_Jpsi","width of gaussian","@0*@1", RooArgList(sigma_etac,r_jpsi))
    r_etac = w_MC.var("r_ref_etac")
    r_etac.setConstant(True)
    sigma_Jpsi = RooRealVar("sigma_Jpsi","width of gaussian", 9., 0.1, 50.)
    sigma_etac= RooFormulaVar("sigma_etac","width of gaussian","@0*@1", RooArgList(sigma_Jpsi,r_etac))
    f.Close()
    #sigma_etac = RooFormulaVar("sigma_etac","width of gaussian","@0*({}/{})**0.5".format(cl.M_ETAC,cl.M_JPSI), RooArgList(sigma_Jpsi))

    #Fit signal
    #Resolution model
    if kSyst=="CB":
        f = TFile(homeDir+"/MC/mass_fit/Wksp_M_res_all_CB.root","READ")
        wMC = f.Get("w")
        f.Close()
        alpha = RooRealVar('alpha','alpha of CB', wMC.var('alpha_1').getValV(), 0.0, 10.)
        alpha.setConstant(True)
        nCB = RooRealVar('nCB','n of CB', wMC.var('nCB_1').getValV(), 0.0, 100.)
        nCB.setConstant(True)
        #Fit J/psi
        resolution_Jpsi = BifurcatedCB("cb_Jpsi", "Cystal Ball Function", Jpsi_M_Low, mass_Jpsi, sigma_Jpsi, alpha, nCB, alpha, nCB)
        resolution_etac = BifurcatedCB("cb_etac", "Cystal Ball Function", Jpsi_M_Low, RooFit.RooConst(0), sigma_etac, alpha, nCB, alpha, nCB)
    else:
        #Fit J/psi
        resolution_Jpsi = RooGaussian("gauss_Jpsi","gaussian PDF", Jpsi_M_Low, mass_Jpsi, sigma_Jpsi)
        resolution_etac = RooGaussian("gauss_etac","gauss PDF", Jpsi_M_Low, RooFit.RooConst(0), sigma_etac)

    #eta_c
    br_wigner_etac = RooRelBreitWigner("br_wigner_etac", "br_wigner", Jpsi_M_Low, mass_etac, gamma_etac, spin_etac, radius, proton_m, proton_m)
    bwxg_etac = RooFFTConvPdf("bwxg_etac","breit-wigner (X) gauss", Jpsi_M_Low, br_wigner_etac, resolution_etac)


    eff_pppi0 = RooRealVar("eff_pppi0","eff_pppi0",eff_pi0)
    pppi0 = RooGenericPdf("pppi0","Jpsi.pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M_Low))
    n_pppi0 = RooFormulaVar("n_pppi0","n_pppi0","@0*@1*(1.19/2.12)",RooArgList(n_Jpsi,eff_pppi0))

    #if kSyst=="Exp": kBkg = "exp"
    #else:            kBkg = "cheb"

    #Bkg fit
    a0_Low = RooRealVar("a0_Low","a0",-0.5, -2.,2.)
    a1_Low = RooRealVar("a1_Low","a1", 0.1, -1.,1.)
    a2_Low = RooRealVar("a2_Low","a2",-0.05,-1.,1.)
    #a3_Low = RooRealVar("a3_Low","a2",-0.005,-1,1.)

    parBkg_Low = RooArgList(Jpsi_M_Low, a0_Low, a1_Low, a2_Low)
    bkg_Low    = choose_bkg([kSource,kBkg], "Low", parBkg_Low)


    modelSignal_Low = RooAddPdf("modelSignal_Low","Jpsi signal", RooArgList(bwxg_etac, resolution_Jpsi), RooArgList(n_etac, n_Jpsi))
    modelSignal_Jpsi = RooAddPdf("modelSignal_Jpsi","Jpsi signal", RooArgList(resolution_Jpsi), RooArgList(n_Jpsi))
    modelBkg_Low = RooAddPdf("modelBkg_Low","Jpsi bkg", RooArgList(bkg_Low), RooArgList(nBckgr_Low))
    model_Low = RooAddPdf("model_Low","Low diapason signal", RooArgList(bwxg_etac,resolution_Jpsi,bkg_Low, pppi0), RooArgList(n_etac,n_Jpsi,nBckgr_Low, n_pppi0))



    getattr(w,"import")(model_Low,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelSignal_Low,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelBkg_Low,RooFit.RecycleConflictNodes())

    getattr(w,"import")(nSignal_Low,RooFit.RecycleConflictNodes())

    #getattr(w,"import")(fconstr_etac, RooFit.RecycleConflictNodes())
    #getattr(w,"import")(n_etac_obs, RooFit.RecycleConflictNodes())

def fillRelWorkspace(w, kSource, kBkg, kNorm="Jpsi"):

    Jpsi_M_Low  = w.var("Jpsi_M_Low")
    Jpsi_M_High = w.var("Jpsi_M_High")


    cs_Jpsi = RooRealVar("cs_Jpsi","cs Jpsi", 0.749)


    n_Jpsi = RooRealVar("n_Jpsi","num of J/Psi", 2e3, 10, 1.e6)
    n_etac = RooRealVar("n_etac","num of etac", 1e4, 10, 3.e6)




    n_norm = "n_{}".format(kNorm)
    BR_norm = "BR_{}".format(kNorm)

    from ROOT.TMath import Sqrt, Sq
    total_val =  ExtMeas[n_norm]["val"]*ExtMeas["eff_etac2s2Jpsi"]["val"]*ExtMeas["BR_etac2s"]["val"]/ExtMeas[BR_norm]["val"]

    total_relerr = 0.
    for source in [BR_norm,"BR_etac2s","eff_etac2s2Jpsi",n_norm]:
        meas = ExtMeas[source]
        val  = meas["val"]
        if meas["relerr"] != -1. :
            relerr = meas["relerr"]
        else:
            relerr = meas["err"]/val
        total_relerr += Sq(relerr)
    total_relerr = Sqrt(total_relerr)
    total_err = total_relerr * total_val
    print(" OVERAL factor is      :", total_val)
    print(" WITH relative error   :", total_relerr)

    extFactor         = RooRealVar("extFactor",  "extFactor", total_val, 0, 100*total_val)
    constr_ext        = RooGaussian("constr_ext","constr_ext", extFactor, RooFit.RooConst(total_val), RooFit.RooConst(total_err))

    # n_etac2s_rel = RooRealVar("n_etac2s_rel","num of Etac", 1.5e-3, 0, 4.)
    # n_etac2s = RooRealVar("n_etac2s","num of etac2s", 2e3, 0, 3.e4)
    cs_etac2s_rel = RooRealVar("cs_etac2s_rel","num of Etac", 1e-2, 0, 4.0)
    n_etac2s = RooFormulaVar("n_etac2s","num of Etac","@0*@1", RooArgList(cs_etac2s_rel, extFactor))
    #if kNorm=="Jpsi":
        #n_etac2s = RooFormulaVar("n_etac2s","num of Etac","@0*@1", RooArgList(cs_etac2s_rel, extFactor))
    #else:
        #n_etac2s = RooFormulaVar("n_etac2s","num of Etac","@0*@1", RooArgList(n_etac2s_rel,n_etac))


    n_chic0 = RooRealVar("n_chic0","num of chic0", 1e3, 0, 1.e4)
    n_chic1 = RooRealVar("n_chic1","num of chic1", 1e3, 0, 1.e4)
    n_chic2 = RooRealVar("n_chic2","num of chic2", 1e3, 0, 1.e4)
    n_hc = RooRealVar("n_hc","num of hc", 1e3, 0, 1.e4)
    n_psi2S = RooRealVar("n_psi2S","num of psi2S", 1e3, 0, 1.e4)
    n_psi3770 = RooRealVar("n_psi3770","num of psi3770", 1e1, 0, 1.e5)
    #n_psi3770.setVal(0.); n_psi3770.setConstant()

    nSignal_Low = RooFormulaVar("nSignal_Low","num of Etac","@0+@1", RooArgList(n_etac,n_Jpsi))
    nSignal_High = RooFormulaVar("nSignal_High","num of Etac","@0+@1+@2+@3+@4+@5+@6", RooArgList(n_etac2s,n_psi2S,n_chic0,n_chic1,n_chic2,n_hc,n_psi3770))

    nBckgr_High = RooRealVar("nBckgr_High","num of bckgr",7e7,1e4,1.e+9)
    nBckgr_Low = RooRealVar("nBckgr_Low","num of bckgr",7e7,1e4,1.e+9)

    #Psi

    mass_Jpsi = RooRealVar("mass_Jpsi","mean of gaussian",cl.M_JPSI, 3030, 3150)
    mass_Jpsi.setConstant()

    mass_psi2S = RooRealVar("mass_psi2S","mean of gaussian",cl.M_PSI2S, 3680, 3692)
    #mass_psi2S.setConstant()
    mass_res_psi2S = RooRealVar("mass_res_psi2S","mean of gaussian", cl.M_RES_L_PSI2S)
    #mass_psi2S     = RooFormulaVar("mass_psi2S","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_psi2S))

    mass_psi3770 = RooRealVar("mass_psi3770","mean of gaussian",cl.M_PSI3770)
    mass_psi3770.setConstant()
    gamma_psi3770 = RooRealVar("gamma_psi3770","width of Br-W", cl.GAMMA_PSI3770)
    spin_psi3770 = RooRealVar("spin_psi3770","spin_psi3770", 1. )


    #eta_c

    #mass_etac = RooRealVar("mass_etac","mean of gaussian", cl.M_ETAC, 2980, 2988)
    #mass_etac.setConstant()
    mass_res_etac = RooRealVar("mass_res_etac","mean of gaussian", cl.M_RES_ETAC, 100, 125)
    mass_res_etac.setConstant()
    mass_etac     = RooFormulaVar("mass_etac","mean of gaussian","@0-@1",RooArgList(mass_Jpsi,mass_res_etac))
    gamma_etac    = RooRealVar("gamma_etac","width of Br-W", cl.GAMMA_ETAC)
    spin_etac     = RooRealVar("spin_etac","spin_eta", 0. )

    radius = RooRealVar("radius","radius", 1.)
    proton_m = RooRealVar("proton_m","proton mass", 938.3 )

    #mass_etac2s = RooRealVar("mass_etac2s","mean of gaussian", cl.M_ETAC2S)
    #mass_etac2s.setConstant()
    mass_res_etac2s = RooRealVar("mass_res_etac2s","mean of gaussian", cl.M_RES_H_ETAC2S, 30., 56.)
    mass_res_etac2s.setConstant()
    #mass_etac2s     = RooFormulaVar("mass_etac2s","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_etac2s))
    mass_etac2s     = RooFormulaVar("mass_etac2s","mean of gaussian","@0-@1",RooArgList(mass_psi2S,mass_res_etac2s))
    gamma_etac2s = RooRealVar("gamma_etac2s","width of Br-W", cl.GAMMA_ETAC2S)
    spin_etac2s = RooRealVar("spin_etac2s","spin_eta", 0. )


    #chi_c

    mass_chic0 = RooRealVar("mass_chic0","mean of gaussian", cl.M_CHIC0, cl.M_CHIC0-10, cl.M_CHIC0+10)
    #mass_chic0.setConstant()
    #mass_res_chic0 = RooRealVar("mass_res_chic0","mean of gaussian", cl.M_RES_L_CHIC0)
    #mass_chic0     = RooFormulaVar("mass_chic0","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_chic0))
    gamma_chic0 = RooRealVar("gamma_chic0","width of Br-W", cl.GAMMA_CHIC0)
    spin_chic0 = RooRealVar("spin_chic0","spin_chic0", 0. )

    mass_chic1 = RooRealVar("mass_chic1","mean of gaussian", cl.M_CHIC1, cl.M_CHIC1-10, cl.M_CHIC1+10)
    #mass_chic1.setConstant()
    #mass_res_chic1 = RooRealVar("mass_res_chic1","mean of gaussian", cl.M_RES_L_CHIC1)
    #mass_chic1     = RooFormulaVar("mass_chic1","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_chic1))
    gamma_chic1 = RooRealVar("gamma_chic1","width of Br-W", cl.GAMMA_CHIC1)
    spin_chic1 = RooRealVar("spin_chic1","spin_chic1", 1. )

    mass_chic2 = RooRealVar("mass_chic2","mean of gaussian", cl.M_CHIC2)
    #mass_chic2.setConstant()
    #mass_res_chic2 = RooRealVar("mass_res_chic2","mean of gaussian", cl.M_RES_L_CHIC2)
    #mass_chic2     = RooFormulaVar("mass_chic2","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_chic2))
    gamma_chic2 = RooRealVar("gamma_chic2","width of Br-W", cl.GAMMA_CHIC2)
    spin_chic2 = RooRealVar("spin_chic2","spin_chic2", 2. )

    #h_c

    mass_hc = RooRealVar("mass_hc","mean of gaussian", cl.M_HC)
    #mass_hc.setConstant()
    #mass_res_hc = RooRealVar("mass_res_hc","mean of gaussian", cl.M_RES_L_HC)
    #mass_hc  = RooFormulaVar("mass_hc","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_hc))
    gamma_hc = RooRealVar("gamma_hc","width of Br-W", cl.GAMMA_HC)
    spin_hc = RooRealVar("spin_hc","spin_hc", 1. )


    sigma_Jpsi = RooRealVar("sigma_Jpsi","width of gaussian", 9., 0.1, 50.)
    #sigma_etac2s = RooRealVar("sigma_etac2s","width of gaussian", 9., 0.1, 50.)
    sigma_etac = RooFormulaVar("sigma_etac","width of gaussian","@0*({}/{})**0.5".format(cl.M_ETAC,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_etac2s = RooFormulaVar("sigma_etac2s","width of gaussian","@0*({}/{})**0.5".format(cl.M_ETAC2S,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_chic0 = RooFormulaVar("sigma_chic0","width of gaussian","@0*({}/{})**0.5".format(cl.M_CHIC0,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_chic1 = RooFormulaVar("sigma_chic1","width of gaussian","@0*({}/{})**0.5".format(cl.M_CHIC1,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_chic2 = RooFormulaVar("sigma_chic2","width of gaussian","@0*({}/{})**0.5".format(cl.M_CHIC2,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_hc = RooFormulaVar("sigma_hc","width of gaussian","@0*({}/{})**0.5".format(cl.M_HC,cl.M_JPSI), RooArgList(sigma_Jpsi))
    #sigma_psi2S = RooRealVar("sigma_psi2S","width of gaussian", 9., 0.1, 50.)
    sigma_psi2S = RooFormulaVar("sigma_psi2S","width of gaussian","@0*({}/{})**0.5".format(cl.M_PSI2S,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_psi3770 = RooFormulaVar("sigma_psi3770","width of gaussian","@0*({}/{})**0.5".format(cl.M_PSI3770,cl.M_JPSI), RooArgList(sigma_Jpsi))


    #Fit signal

    gauss_Low = RooGaussian("gauss Low","gauss Low PDF", Jpsi_M_Low, RooFit.RooConst(0), sigma_Jpsi)
    gauss_High = RooGaussian("gauss Low","gauss Low PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_Jpsi)

    #eta_c
    gauss_etac = RooGaussian("gauss_etac","gauss PDF", Jpsi_M_Low, RooFit.RooConst(0), sigma_etac)
    br_wigner_etac = RooRelBreitWigner("br_wigner_etac", "br_wigner", Jpsi_M_Low, mass_etac, gamma_etac, spin_etac, radius, proton_m, proton_m)
    bwxg_etac = RooFFTConvPdf("bwxg_etac","breit-wigner (X) gauss", Jpsi_M_Low, br_wigner_etac, gauss_etac)

    #chi_c
    gauss_chic0 = RooGaussian("gauss_chic0","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_chic0)
    br_wigner_chic0 = RooRelBreitWigner("br_wigner_chic0", "br_wigner", Jpsi_M_High, mass_chic0, gamma_chic0, spin_chic0, radius, proton_m, proton_m)
    bwxg_chic0 = RooFFTConvPdf("bwxg_chic0","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_chic0, gauss_chic0)

    gauss_chic1 = RooGaussian("gauss_chic1","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_chic1)
    br_wigner_chic1 = RooRelBreitWigner("br_wigner_chic1", "br_wigner", Jpsi_M_High, mass_chic1, gamma_chic1, spin_chic1, radius, proton_m, proton_m)
    bwxg_chic1 = RooFFTConvPdf("bwxg_chic1","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_chic1, gauss_chic1)

    gauss_chic2 = RooGaussian("gauss_chic2","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_chic2)
    br_wigner_chic2 = RooRelBreitWigner("br_wigner_chic2", "br_wigner", Jpsi_M_High, mass_chic2, gamma_chic2, spin_chic2, radius, proton_m, proton_m)
    bwxg_chic2 = RooFFTConvPdf("bwxg_chic2","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_chic2, gauss_chic2)

    #h_c
    gauss_hc = RooGaussian("gauss_hc","gauss PDF", Jpsi_M_High, RooFit.RooConst(0), sigma_hc)
    br_wigner_hc = RooRelBreitWigner("br_wigner_hc", "br_wigner", Jpsi_M_High, mass_hc, gamma_hc, spin_hc, radius, proton_m, proton_m)
    bwxg_hc = RooFFTConvPdf("bwxg_hc","breit-wigner (X) gauss", Jpsi_M_High, br_wigner_hc, gauss_hc)


    #Fit J/psi
    gauss_Jpsi = RooGaussian("gauss_Jpsi","gaussian PDF", Jpsi_M_Low, mass_Jpsi, sigma_Jpsi)

    gauss_psi2S = RooGaussian("gauss_psi2S","gaussian PDF", Jpsi_M_High, mass_psi2S, sigma_psi2S)

    #br_wigner_psi3770 = RooRelBreitWigner("br_wigner_psi3770", "br_wigner", Jpsi_M, mass_psi3770, gamma_psi3770, spin_psi3770, radius, proton_m, proton_m)
    #bwxg_psi3770 = RooFFTConvPdf("bwxg_psi3770","breit-wigner (X) gauss", Jpsi_M, br_wigner_psi3770, gauss)
    bwxg_psi3770 = RooVoigtian("bwxg_psi3770", "bwxg_psi3770", Jpsi_M_High, mass_psi3770, gamma_psi3770, sigma_psi3770)



    eff_pppi0 = RooRealVar("eff_pppi0","eff_pppi0",0.06)
    pppi0 = RooGenericPdf("pppi0","Jpsi.pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M_Low))
    n_pppi0 = RooFormulaVar("n_pppi0","n_pppi0","@0*@1*(1.19/2.12)",RooArgList(n_Jpsi,eff_pppi0))

    #Bkg fit
    a0_High = RooRealVar("a0_High","a0",0.4,-2,2)
    a1_High = RooRealVar("a1_High","a1",0.05,-1.,1.)
    a2_High = RooRealVar("a2_High","a2",-0.005,-1,1.)


    a1_High.setVal(0.04)
    a2_High.setVal(0.04)

    parBkg_High = RooArgList(Jpsi_M_High, a0_High, a1_High, a2_High)
    bkg_High    = choose_bkg([kSource,kBkg], "High", parBkg_High)


    #Bkg fit
    a0_Low = RooRealVar("a0_Low","a0",0.4,-2.,2.)
    a1_Low = RooRealVar("a1_Low","a1",0.05,-1.,1.)
    a2_Low = RooRealVar("a2_Low","a2",-0.005,-1,1.)
    #a3_Low = RooRealVar("a3_Low","a2",-0.005,-1,1.)


    a1_Low.setVal(0.04)
    a2_Low.setVal(0.04)

    parBkg_Low = RooArgList(Jpsi_M_Low, a0_Low, a1_Low, a2_Low)
    bkg_Low    = choose_bkg([kSource,kBkg], "Low", parBkg_Low)

    #bkg = RooGenericPdf("bkg","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200)", RooArgList(Jpsi_M,a0,a1))
    #bkg = RooChebychev ("bkg_Low","Background",Jpsi_M,RooArgList(a0_Low,a1_Low,a2_Low,a3_Low))



    modelSignal_High = RooAddPdf("modelSignal_High","etac2s signal", RooArgList(bwxg_etac2s, bwxg_chic0, bwxg_chic1, bwxg_chic2, bwxg_hc, gauss_psi2S, bwxg_psi3770), RooArgList(n_etac2s, n_chic0, n_chic1, n_chic2, n_hc, n_psi2S, n_psi3770))
    modelSignal_etac2s = RooAddPdf("modelSignal_etac2s","etac2s signal", RooArgList(bwxg_etac2s), RooArgList(n_etac2s))
    modelBkg_High = RooAddPdf("modelBkg_High","etac2s bkg", RooArgList(bkg_High), RooArgList(nBckgr_High))
    model_High = RooAddPdf("model_High","High diapason signal", RooArgList(bkg_High, bwxg_etac2s, bwxg_chic0, bwxg_chic1, bwxg_chic2, bwxg_hc, gauss_psi2S, bwxg_psi3770), RooArgList(nBckgr_High, n_etac2s, n_chic0, n_chic1, n_chic2, n_hc, n_psi2S, n_psi3770))

    modelSignal_Low = RooAddPdf("modelSignal_Low","Jpsi signal", RooArgList(bwxg_etac, gauss_Jpsi), RooArgList(n_etac, n_Jpsi))
    modelSignal_Jpsi = RooAddPdf("modelSignal_Jpsi","Jpsi signal", RooArgList(gauss_Jpsi), RooArgList(n_Jpsi))
    modelBkg_Low = RooAddPdf("modelBkg_Low","Jpsi bkg", RooArgList(bkg_Low), RooArgList(nBckgr_Low))
    model_Low = RooAddPdf("model_Low","Low diapason signal", RooArgList(bwxg_etac,gauss_Jpsi,bkg_Low, pppi0), RooArgList(n_etac,n_Jpsi,nBckgr_Low, n_pppi0))



    model_High_constr = RooProdPdf("model_High_constr","model_High_constr", RooArgList(model_High, constr_ext))



    getattr(w,"import")(model_High,RooFit.RecycleConflictNodes())
    getattr(w,"import")(model_High_constr,RooFit.RecycleConflictNodes())
    getattr(w,"import")(model_Low,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelSignal_High,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelSignal_Low,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelBkg_High,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelBkg_Low,RooFit.RecycleConflictNodes())

    getattr(w,"import")(nSignal_Low,RooFit.RecycleConflictNodes())
    getattr(w,"import")(nSignal_High,RooFit.RecycleConflictNodes())

    #getattr(w,"import")(fconstr_etac, RooFit.RecycleConflictNodes())
    #getattr(w,"import")(n_etac_obs, RooFit.RecycleConflictNodes())


