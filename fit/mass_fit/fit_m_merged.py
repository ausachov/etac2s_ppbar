from ROOT import gROOT, gStyle, gPad, TFile, TChain, TTree, TCanvas, \
        RooFit, RooStats, RooWorkspace, RooRealVar, RooArgList, RooArgSet, RooDataHist, RooDataSet, \
        RooLinkedList, RooCmdArg, RooGaussian, RooStats, RooMinuit, RooProfileLL, RooFormulaVar
from ROOT import Math

# from model import *
from model_sim import *
import drawModule_splitFit as dm
import oniaConstLib as cl
gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")

import numpy as np
import logging
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

#homeDir = "/afs/cern.ch/user/v/vazhovko/private/etac2s_ppbar/"
#dataDir = "/eos/user/v/vazhovko/etac2s_ppbar/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

CUTKEY = "ProbNNp_06"

def get_data_h(w, kSource):

    nameWksp = f"{dataDir}/wksps/wksp_data_{kSource}_{CUTKEY}.root"
    file_w = TFile(nameWksp)
    w_in = file_w.Get("w")

    data   = w_in.data("dh")
    Jpsi_M     = w_in.var("Jpsi_M")

    hist = data.createHistogram("hist",Jpsi_M)

    Jpsi_M  = w.var("Jpsi_M")
    # Jpsi_M  = RooRealVar("Jpsi_M", "Jpsi_M",  dm.minM_Low, dm.maxM_High)

    dh  = RooDataHist("dh","dh",  RooArgList(Jpsi_M), hist)
    # w.Import(Jpsi_M)
    w.Import(dh)

    logging.info("RooDataHist successfully loaded")

def get_data_s(w, kSource):

    dirName_Low  = dataDir +"Data_Low/"
    dirName_High = dataDir +"Data_High/"


    nt_Low  =  TChain("DecayTree")
    nt_High =  TChain("DecayTree")

    nt_Low.Add(dirName_Low+"/Etac2sDiProton_Low_2018*.root")
    nt_High.Add(dirName_High+"/Etac2sDiProton_High_2018*.root")

    cut_FD = ""
    if kSource=="prompt":
        cut_FD = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08"
    else:
        cut_FD = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16"# & Jpsi_FDCHI2_OWNPV > 49""

    cut_PT      = "Jpsi_PT > 5000 && Jpsi_PT < 20000"

    totCut = cut_FD + " && " + cut_PT

    tree_Low = TTree()
    tree_High = TTree()

    tree_Low = nt_Low.CopyTree(totCut)
    tree_High = nt_High.CopyTree(totCut)

    Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M", minM_Low, maxM_High)

    ds_Low = RooDataSet("ds_Low","ds_Low", tree_Low, RooArgSet(Jpsi_M))
    ds_High = RooDataSet("ds_High","ds_High", tree_High, RooArgSet(Jpsi_M))
    getattr(w,"import")(ds_Low)
    getattr(w,"import")(ds_High)

    ds_Low.Print()
    ds_High.Print()
    a = input()

    logging.info("RooDataSet successfully loaded")

def plot(w:RooWorkspace, namePic:str, kSource:str):

    frame, resid, pull = dm.set_frame(w,"", kSource)
    # frame, resid, pull = dm.set_frame_lim(w,"High", kSource)

    names  = [kSource]
    frames = [frame]
    resids = [resid]
    pulls  = [pull]
    c = dm.set_canvas(names, frames, resids, pulls)

    c.SaveAs(namePic)

def perform_fit(kSource:str,kSyst:str,kNorm:str="jpsi",state="etac2S"):

    # kSyst = kBkg.capitalize()

    gStyle.SetStatFormat("4.3f")
    #gStyle.SetOptTitle(0)
    logging.info(f"Performing fit for {state} for {kSyst} systematic")
    logging.info("-----------------------------------------------------------------------------------")

    Jpsi_M = RooRealVar("Jpsi_M", "Jpsi_M",  dm.minM_Low, dm.maxM_High)
    Jpsi_M.setBins(dm.binN_Tot_Fit)
    w = RooWorkspace("w",True)
    w.Import(Jpsi_M)

    #get_data_s(w,kSource)
    get_data_h(w,kSource)

    all_states = ["jpsi", "etac", "chic0", "chic1", "chic2", "hc", "psi2S", "etac2S"]
    all_states.insert(0, all_states.pop(all_states.index(kNorm)))

    # kCB = False if kSyst!="CB" else True
    kCB = False if kSyst=="Gauss" else True

    setMW(w, all_states)

    for ccbar in all_states:
        fill_workspace(w, ccbar, "", ref=kNorm, kCB=kCB, kSyst=kSyst)

    kBkg = "Cheb" if kSyst!="Exp" else kSyst
    # combine(w, all_states, "", kSource, kSyst, split_lvl="none",kLim=state) # for absolute yields
    combine_rel(w, all_states, "", kSource, kSyst, split_lvl="none",kLim=state) # for relative yields

    Jpsi_M = w.var("Jpsi_M")
    Jpsi_M.setRange("fitRange_Low", dm.minM_Low, dm.maxM_Low)
    Jpsi_M.setRange("fitRange_High", dm.minM_High, dm.maxM_High)

    # model = w.pdf("model_constr")
    # model = w.pdf(f"model_{kSource}_constr")
    model = w.pdf(f"model_{kSource}")
    # model = w.pdf("model")
    # modelBkg = w.pdf(f"modelBkg_{kSource}")
    # modelSignal = w.pdf(f"modelSignal_{kSource}")
    model.Print()
    # extFactor = RooArgSet(w.var(f"extFactor_{kSource}"))
    # extFactorObs = w.var(f"extFactorObs_{kSource}")
    # extFactorObs.setAttribute("MyGlobalObservable")
    # model.setStringAttribute("GlobalObservablesTag","MyGlobalObservable") ;    

    llist = RooLinkedList()
    llist.Add(RooCmdArg(RooFit.Extended(True)))
    llist.Add(RooCmdArg(RooFit.Save(True)))
    llist.Add(RooCmdArg(RooFit.Offset(True)))
    # llist.Add(RooCmdArg(RooFit.NumCPU(20, 3)))
    # llist.Add(RooCmdArg(RooFit.GlobalObservables(extFactorObs)))
    # llist.Add(RooCmdArg(RooFit.Constrain(extFactor)))

    data = w.data("dh")
    constr_sigma = RooGaussian()
    if kSource=="prompt":
        f = TFile(homeDir+"/mass_fit/one_hist/Wksp_{}_fromB_{}_misID.root".format(kNorm, "Base"),"READ")
        w_fromB = f.Get("w")
        f.Close()

        mass = w_fromB.var("mass_jpsi").getValV()
        w.var("mass_jpsi").setVal(mass)

        sigma = w.var("sigma_{}_1".format(kNorm))

        sigma_val = w_fromB.var("sigma_{}_1".format(kNorm)).getValV()
        if kSyst!="Resolution":
            sigma.setVal(sigma_val)
        else:
            sigma_err = w_fromB.var("sigma_{}_1".format(kNorm)).getError()
            sigma.setVal(sigma_val+sigma_err)
        sigma.setConstant(True)

        # constraining resolution from fit to b-decays' sample
        # sigma_err = w_fromB.var("sigma_jpsi_1").getError()
        # constr_sigma = RooGaussian("constr_sigma","constr_sigma", sigma, RooFit.RooConst(sigma_val), RooFit.RooConst(sigma_err))
        # sigma.setRange(3., 15.)
        # llist.Add(RooCmdArg(RooFit.ExternalConstraints(RooArgSet(constr_sigma))))

        #w.var("mass_jpsi").setConstant(False)
        # mass = w_fromB.var("mass_psi2S").getValV()
        # w.var("mass_psi2S").setVal(mass)
        # w.var("mass_psi2S").setConstant(True)
        # mass = w_fromB.var("mass_etac2S").getValV()
        # w.var("mass_etac2S").setVal(mass)
        # w.var("mass_etac2S").setConstant(True)
    else:
        w.var("mass_jpsi").setConstant(False)
        w.var("mass_chic0").setConstant(False)
        w.var("mass_chic1").setConstant(False)
        srange = 200
        w.var("mass_chic1").setRange(cl.M_CHIC1-srange*cl.DM_CHIC1, cl.M_CHIC1+srange*cl.DM_CHIC1)
        # w.var("mass_chic2").setConstant(False)
        w.var("mass_psi2S").setConstant(True)
        # w.var("mass_etac2S").setConstant(False)

    # if kSyst=="Mass":
    #     w.var("mass_chic0").setVal(cl.M_CHIC0+cl.DM_CHIC0)
    #     w.var("mass_chic1").setVal(cl.M_CHIC1+cl.DM_CHIC1)
    #     w.var("mass_chic2").setVal(cl.M_CHIC2+cl.DM_CHIC2)
    # for ccbar in all_states[2:-1]:
    #     w.var("n_{}".format(ccbar)).setVal(1.)
    #     w.var("n_{}".format(ccbar)).setConstant(True)

    # w.var("cs_etac2S_rel").setVal(0.)
    # w.var("extFactor").setVal(0.)
    # w.var("extFactor").setConstant(True)
    # w.var("cs_etac2S_rel").setConstant(True)

    # r = modelBkg.fitTo(data, llist)
    r = model.fitTo(data, llist)

    # for ccbar in all_states[2:-1]:
    #     w.var(f"n_{ccbar}_{kSource}").setConstant(False)

    # w.var("extFactor").setConstant(False)
    # w.var("cs_etac2S_rel").setConstant(False)

    r = model.fitTo(data, llist)
    r = model.fitTo(data, llist)
    # llist.Add(RooCmdArg(RooFit.Minos(True)))
    # llist.Add(RooCmdArg(RooFit.EvalErrorWall(False)))
    # llist.Add(RooCmdArg(RooFit.Strategy(2)))
    # w.var("gamma_etac2S").setConstant(False)
    # r = model.fitTo(data, llist)
    # r = model.fitTo(data, llist)
    # r = model.fitTo(data, llist)


    nameWksp = f"{homeDir}/mass_fit/one_hist/CB/Wksp_{kNorm}_{kSource}_{kSyst}_{CUTKEY}_{state}.root"
    namePic  = f"{homeDir}/mass_fit/one_hist/CB/Plot_{kNorm}_{kSource}_{kSyst}_{CUTKEY}_{state}.pdf"
    nameTxt  = f"{homeDir}/mass_fit/one_hist/CB/Result_{kNorm}_{kSource}_{kSyst}_{CUTKEY}_{state}.txt"

    w.writeToFile(nameWksp)
    plot(w, namePic, kSource)

    import os, sys
    save = os.dup( sys.stdout.fileno() )
    newout = open(nameTxt, "w" )
    os.dup2( newout.fileno(), sys.stdout.fileno() )
    r.Print("v")
    r.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() )
    newout.close()


    print("fit performed successfully")

    # return w


def calcULSyst(kSource, kSyst, kNorm, ll=False):

    # homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"

    nameWksp = homeDir + "/mass_fit/one_hist/Wksp_{}_{}_{}_misID.root".format(kNorm,kSource,kSyst)
    # nameWksp = homeDir + "/mass_fit/one_hist/Wksp_{}_{}_{}_misID.root".format(kNorm,kSource,"Base")
    file_w_base = TFile(nameWksp)
    w = file_w_base.Get("w").Clone("w")
    file_w_base.Close()

    poi_var = w.var("cs_etac2S_rel")
    poi_var.setConstant(False)

    # # # if kSyst!="Base":
    # nameWksp_syst = homeDir + "/mass_fit/one_hist/Wksp_{}_{}_{}_misID.root".format(kNorm,kSource,kSyst)
    # file_w_syst = TFile(nameWksp_syst)
    # w_syst = file_w_syst.Get("w").Clone("w_syst")
    # file_w_syst.Close()


    # model_base = w.pdf("model_constr")

    # cs_err = abs(w_syst.var("cs_etac2S_rel").getValV() - poi_var.getValV())
    # cs_val = w.var("cs_etac2S_rel").getValV()
    # print("{} +/- {}".format(cs_val,cs_err))
    # constr_cs = RooGaussian("constr_cs","constr_cs", poi_var, RooFit.RooConst(cs_val), RooFit.RooConst(cs_err))
    # model = RooProdPdf("model_syst","model_syst", RooArgList(model_base, constr_cs))

    # # else:
    #     model = w.pdf("model_constr")
    model = w.pdf("model_constr")

    Jpsi_M = w.var("Jpsi_M")

    data  = w.data("dh")

    nuisanceParams = RooArgSet()
    var_list_0 = w.allVars()

    nuis_iter = var_list_0.createIterator()
    var = nuis_iter.Next()
    while var:
        # var.setConstant(True)
        # if var.GetName()!="extFactorObs":
        if "n_" in var.GetName() and not "spin" in var.GetName():
            nuisanceParams.add(var)
        var = nuis_iter.Next()

    extFactor = w.var("extFactor")
    nuisanceParams.add(extFactor)



    mass_etac2S = w.var("mass_etac2S")
    w.var("gamma_etac2S").setConstant(True)
    w.var("mass_jpsi").setConstant(True)
    # w.var("sigma_jpsi_1").setConstant(True)
    # mass_etac2S_var = w.var("mass_etac2S_var")
    #mass_res_etac2S.setConstant(False)

    ## ULs Calculator using HypoTestInverter

    n_bkg     = w.var("n_bkg")
    n_jpsi    = w.var("n_jpsi")
    n_psi2S   = w.var("n_psi2S")
    n_psi3770 = w.var("n_psi3770")
    n_etac    = w.var("n_etac")
    n_hc      = w.var("n_hc")
    n_chic0   = w.var("n_chic0")
    n_chic1   = w.var("n_chic1")
    n_chic2   = w.var("n_chic2")
    a0        = w.var("a0")
    a1        = w.var("a1")
    a2        = w.var("a2")


    # BR_jpsi_mean = w.var("BR_jpsi_mean")
    # BR_etac2S_mean = w.var("BR_etac2S_mean")

    n_bkg.setConstant(False)
    #nBckgr_Low.setConstant(False)
    #n_jpsi.setConstant(False)
    #n_etac.setConstant(False)


    if kSource=="prompt":
        a3 = w.var("a3")
        a4 = w.var("a4")
        bgParams = RooArgSet(n_bkg, a0, a1, a2, a3, a4)
        poi_var.setRange(0., 10.0)

    else:
        bgParams = RooArgSet(n_bkg, a0, a1, a2)
        poi_var.setRange(0., 2.0)

    # Math.MinimizerOptions.SetDefaultMinimizer("Migrad","Minimize");
    Math.MinimizerOptions.SetDefaultMinimizer("Minuit2","Minimize");
    Math.MinimizerOptions.SetDefaultStrategy(2);
    Math.MinimizerOptions.SetDefaultTolerance(1e-4)
    Math.MinimizerOptions.SetDefaultMaxFunctionCalls(1000000)

    paramOfInterest = RooArgSet(poi_var)
    constrainedParams = RooArgSet(n_jpsi, n_etac, n_hc, n_psi2S, n_chic0, n_chic1, n_chic2)

    nuis_iter = constrainedParams.createIterator()
    var = nuis_iter.Next()
    band_size = 5
    while var:
        var.setRange(max(0., var.getVal()-band_size*var.getError()), var.getVal()+band_size*var.getError())
        print("{}: [{}, {}]".format(var.GetName(), max(0., var.getVal()-band_size*var.getError()), var.getVal()+band_size*var.getError()))
        var = nuis_iter.Next()

    # nuisanceParams = RooArgSet(n_jpsi, n_etac, n_hc, n_chic0, n_chic1, n_chic2, n_psi2S) # n_psi2S is not significant?
    # nuisanceParams.add(n_bkg)
    #constrainedParams.add(bgParams)
    #constrainedParams.add(RooArgSet(BR_jpsi_mean, BR_etac2S_mean))

    #n_etac.setRange(n_etac.getVal()-5*n_etac.getError(), n_etac.getVal()+5*n_etac.getError())
    #nBckgr_High.setRange(nBckgr_High.getVal()-5*nBckgr_High.getError(), nBckgr_High.getVal()+5*nBckgr_High.getError())
    #nBckgr_Low.setRange(nBckgr_Low.getVal()-5*nBckgr_Low.getError(), nBckgr_Low.getVal()+5*nBckgr_Low.getError())


    modelConfig = RooStats.ModelConfig(w);
    # modelConfig.SetPdf(model_syst)
    modelConfig.SetPdf(model)
    modelConfig.SetProtoData(data)
    modelConfig.SetParametersOfInterest(paramOfInterest)
    modelConfig.SetNuisanceParameters(nuisanceParams)
    modelConfig.SetObservables(RooArgSet(RooArgList(Jpsi_M)))


    # GlobalObservables - measurement from the other experiment, e.g. BR
    extFactorObs = w.var("extFactorObs")
    globalParams = RooArgSet(extFactorObs)
    modelConfig.SetGlobalObservables(globalParams)
    # I don't know what  ConstraintParameters are ...
    # modelConfig.SetConstraintParameters(globalParams)

    modelConfig.SetName("ModelConfig")
    paramOfInterest.first().setVal(poi_var.getVal())
    modelConfig.SetSnapshot(paramOfInterest)
    getattr(w,"import")(modelConfig)
    modelConfig.Print()

    sbModel = w.obj("ModelConfig")
    poi = sbModel.GetParametersOfInterest().first()
    bModel = sbModel.Clone(sbModel.GetName()+"_with_poi_0")
    # bModel.SetName(sbModel.GetName()+"_with_poi_0")
    poi.setVal(0)
    bModel.SetSnapshot(RooArgSet(poi))

    fc = RooStats.FrequentistCalculator(data, bModel, sbModel)
    # do these values change anything ?
    fc.SetToys(1000,500)
    ac = RooStats.AsymptoticCalculator(data, bModel, sbModel)
    ac.SetOneSided(True)
    ac.SetPrintLevel(-1)

    cl_value = 0.95
    calc = RooStats.HypoTestInverter(ac)
    calc.SetConfidenceLevel(cl_value)

    calc.UseCLs(True)
    calc.SetVerbose(True)

    toymcs = calc.GetHypoTestCalculator().GetTestStatSampler()

    profll = RooStats.ProfileLikelihoodTestStat(sbModel.GetPdf())
    profll.SetOneSided(True)
    # profll.SetMinimizer("Minuit2")
    profll.SetLOffset(True)
    toymcs.SetTestStatistic(profll)

    if not sbModel.GetPdf().canBeExtended():
        toymcs.SetNEventsPerToy(1)
        print('can not be extended')

    # npoints = 51
    npoints = 21
    poimin = poi.getMin()
    poimax = poi.getMax()


    calc.SetFixedScan(npoints,poimin,poimax)
    r = calc.GetInterval()
    expUpperLimit = r.GetExpectedUpperLimit()
    obsUpperLimit = r.UpperLimit()
    print("Exp Upper Limit is", expUpperLimit)
    print("Obs Upper Limit is", obsUpperLimit)


    plot = RooStats.HypoTestInverterPlot("HTI_Result_Plot","HypoTest Scan Result",r)
    c = TCanvas("HypoTestInverter Scan")
    c.SetLogy(False)
    plot.Draw("CLb 2CL")

    # homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"

    #plot.Draw("EXP")
    c.RangeAxis(poimin, 0., poimax, 1.1)
    c.Draw()
    c.SaveAs(homeDir + "/mass_fit/one_hist/CL_Ac_{}_{}_{}_misID.root".format(kSource,kSyst,kNorm))
    c.SaveAs(homeDir + "/mass_fit/one_hist/CL_Ac_{}_{}_{}_misID.pdf".format(kSource,kSyst,kNorm))


    # LL scan
    if ll:
        ### C o n s t r u c t   p l a i n   l i k e l i h o o d
        ### ---------------------------------------------------

        # Construct unbinned likelihood
        # nll = model.createNLL(data, RooFit.Extended(True), RooFit.Offset(True), RooFit.Constrain(extFactor), RooFit.GlobalObservables(extFactorObs))
        nll_base = model_base.createNLL(data, RooFit.Extended(True), RooFit.Save(True), RooFit.Offset(True), RooFit.Constrain(extFactor))
        nll = model.createNLL(data, RooFit.Extended(True), RooFit.Save(True), RooFit.Offset(True), RooFit.Constrain(extFactor))


        # Minimize likelihood w.r.t all parameters before making plots
        m = RooMinuit(nll)
        m.migrad()
        m.migrad()
        m.hesse()
        #m.minos()

        m_base = RooMinuit(nll)
        m_base.migrad()
        m_base.migrad()
        m_base.hesse()

        # Plot likelihood scan frac
        frameRS = poi_var.frame(RooFit.Bins(46),RooFit.Title("LL and profileLL in n_{#eta_{c}}(2S) "))

        # The profile likelihood estimator on nll for frac will minimize nll w.r.t
        # all floating parameters except frac for each evaluation

        pll_N_etac2S_base = RooProfileLL("pll_N_etac2S_base", "pll_N_etac2S_base", nll_base, RooArgSet(poi_var))
        llhood_base = RooFormulaVar("llhood_base","exp(-0.5*pll_N_etac2S_base)",RooArgList(pll_N_etac2S_base));

        pll_N_etac2S = RooProfileLL("pll_N_etac2S", "pll_N_etac2S", nll, RooArgSet(poi_var))
        llhood = RooFormulaVar("llhood","exp(-0.5*pll_N_etac2S)",RooArgList(pll_N_etac2S));

        # Plot the profile likelihood in frac
        pll_N_etac2S.plotOn(frameRS,RooFit.LineColor(2))
        pll_N_etac2S_base.plotOn(frameRS,RooFit.LineColor(3))

        # Adjust frame maximum for visual clarity
        frameRS.SetMinimum(0)
        frameRS.GetXaxis().SetRangeUser(poimin, poimax)

        cNLL = TCanvas("profilell","profilell",800, 400)
        cNLL.cd(),  gPad.SetLeftMargin(0.15),  frameRS.GetYaxis().SetTitleOffset(1.4),  frameRS.Draw()

        cNLL.SaveAs(homeDir + "/mass_fit/one_hist/NLL_{}_{}_{}_misID.pdf".format(kSource,kSyst,kNorm))

        nll.Write()
        pll_N_etac2S.Write()


        # fllhood = llhood.asTF(RooArgList(paramOfInterest))
        # npoints = 25
        # int_array = np.ones(npoints+1)
        # int_val0 = fllhood.Integral(poimin, poimax, 1e-6)
        # print(int_val0)
        # poi_urs = np.linspace(poimin, poimax, npoints+1)

        # fll_ul = 0.
        # for idx in range(npoints):

        #     int_val = fllhood.Integral(poimin, poi_urs[idx+1], 1e-6)
        #     int_array[idx+1] = 1.-int_val/int_val0
        #     if int_array[idx+1]<0.05: 
        #         fll_ul = poi_urs[idx+1]
        #         print(fll_ul)

        # print(int_array)
        # gr_pval = TGraph(npoints+1, poi_urs, int_array)
        # c.cd()
        # gr_pval.SetLineColor(2)
        # gr_pval.GetXaxis().SetRangeUser(poimin, poimax)
        # gr_pval.Draw("same")

        c.SaveAs(homeDir + "/mass_fit/one_hist/CL_Ac_check_{}_{}_{}_misID.pdf".format(kSource,kSyst,kNorm))
        c.SaveAs(homeDir + "/mass_fit/one_hist/CL_Ac_check_{}_{}_{}_misID.root".format(kSource,kSyst,kNorm))

    #import os, sys
    #sys.stdout = open("{}/UL_{}_{}_{}.txt".format(homeDir,kSource, kNorm, kSyst), 'w' )
    fo = open("{}/mass_fit/one_hist/UL_{}_{}_{}.txt".format(homeDir,kSource, kSyst, kNorm), 'w' )
    fo.write("Upper Limit at {} CL \n".format(cl_value))
    fo.write("Expexted: {} \n".format(expUpperLimit))
    fo.write("Observed: {} \n".format(obsUpperLimit))
    # fo.write("Bayesian: {} \n".format(fll_ul))
    #sys.stdout.close()
    fo.close()

    # from ROOT.RooStats import BayesianCalculator

    # #--Bayesean Calculator--
    # POI_Bayes = RooArgSet(poi_var)                                                              #Set BR as POI, option for other POIs
    # flat_prior = RooUniform("flat_prior","Flat Prior", RooArgSet(poi_var))                      #Make flat prior in BR
    # bcalc = BayesianCalculator(data, model, POI_Bayes, flat_prior)                              #Call BayesianCalculator, create object
    # #bcalc = BayesianCalculator(combData, sbModel)                                              #Call BayesianCalculator, create object
    # #bcalc.ForceNuisancePdf (model)
    # bcalc.SetNuisanceParameters(nuisanceParams)
    # bcalc.SetConfidenceLevel(.95)                                                               #Set 95% CL
    # bcalc.SetLeftSideTailFraction(0)                                                            #Make one-sided upper limit
    # interval = bcalc.GetInterval()                                                              #Get the interval SimpleInterval*

    # print("95\% CL interval: [ ", interval.LowerLimit(), " - ", interval.UpperLimit(), " ]\n")  #Read out bounds of interval

    # UL_Bayes = interval.UpperLimit()                                                            #Read out upper limit
    # plot = bcalc.GetPosteriorPlot()                                                             #Make plot of likelihood and integral region. RooPlot *
    # canvas4 = TCanvas("canvas4", "BayeseanCalculator", 1200, 1000)
    # canvas4.cd(1)
    # plot.Draw()
    # canvas4.SaveAs(homeDir + "/mass_fit/one_hist/A0mass_BayesPlot.png")

    from ROOT.RooStats import ProfileLikelihoodCalculator

    #---PLC Upper Limit---#
    POI_PLC_UL = RooArgSet(poi_var)
    plc_UL = ProfileLikelihoodCalculator(data, model, POI_PLC_UL)   #Call to PLC with new range on nsig
    plc_UL.SetConfidenceLevel(cl_value)
    plc_UL.SetNuisanceParameters(nuisanceParams)
    plInt_UL = plc_UL.GetInterval()                 #LikelihoodInterval*
    UL_PLC = plInt_UL.UpperLimit(poi_var)

    print("UL_PLC = ",UL_PLC)

''' _base histos: only PT, tz and PID (all 0.6) cuts '''

if __name__ == "__main__":

    kSysts = ["Base","Gauss","SRatio","Exp","pppi0","Sqrt","Mass","Gamma"]
    # kSysts = ["CB","BR","cross_talk","Resolution"]
    kSysts = ["Base"]
    kSources = ["fromB","prompt"]
    kSources = ["fromB"]
    # kNorm = 'jpsi'
    kNorm = 'chic0'
    kState = 'rel'
    for kSyst in kSysts:
        for kSource in kSources:
            perform_fit(kSource, kSyst, kNorm, kState)
            # nameWksp = f"{homeDir}/mass_fit/one_hist/CB/Wksp_{kNorm}_{kSource}_{kSyst}_{CUTKEY}_{kState}.root"
            # namePic  = f"{homeDir}/mass_fit/one_hist/CB/Plot_{kNorm}_{kSource}_{kSyst}_{CUTKEY}_{kState}_note.pdf"
            # file_w = TFile(nameWksp)
            # w = file_w.Get("w").Clone()
            # file_w.Close()
            # plot(w, namePic, kSource)

            # calcUL(w,kSource,kSyst,kNorm)
            # del w
            # calcULSyst(kSource,kSyst,kNorm, True)

