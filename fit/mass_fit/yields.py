from ROOT import gROOT, TFile
gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../libs/libBifurcatedCB/BifurcatedCB.cxx+")
from ROOT import RooRelBreitWigner, BifurcatedCB

# from uncertainties import ufloat
# from uncertainties.umath import *

homeDir = '/sps/lhcb/zhovkovska/etac2s_ppbar/results/'
dataDir = '/sps/lhcb/zhovkovska/etac2s_ppbar/'

binWidth = 0.2
binWidthDraw = 0.2

minM_Low = 2850
maxM_Low = 3250
binN_Low = int((maxM_Low-minM_Low)/binWidth)

minM_High = 3320
maxM_High = 3780
binN_High = int((maxM_High-minM_High)/binWidth)

uncorr_syst = ['Exp','eff_pppi0','CB','Gamma','Resolution']#,'rEtaToJpsi',]
#corr_syst = ['CB','Gamma34']

# BR_ppbar = {
#     "chic0" : ufloat(2.21e-4,0.08e-4),
#     "chic1" : ufloat(7.60e-5,0.34e-5),
#     "chic2" : ufloat(7.33e-5,0.33e-5),
#     "hc"    : ufloat(1.5e-4,0),
#     "psi2S" : ufloat(2.94e-4,0.08e-4 ),
#     "jpsi"  : ufloat(2.120e-3, 0.029e-3),
# }

# BR_b2ccbar = {
#     "chic0" : ufloat(1.5e-2, 0.6e-2),
#     "chic1" : ufloat(1.4e-2, 0.4e-2),
#     "chic0" : ufloat(6.2e-3, 2.9e-3),
#     "psi2S" : ufloat(3.06e-3, 0.30e-3),
#     "jpsi"  : ufloat(1.16e-2, 0.10e-2),
# }

BR_ppbar = {
    "chic0" : [2.21e-4,0.08e-4],
    "chic1" : [7.60e-5,0.34e-5],
    "chic2" : [7.33e-5,0.33e-5],
    "hc"    : [1.5e-4,0],
    "psi2S" : [2.94e-4,0.08e-4 ],
    "jpsi"  : [2.120e-3, 0.029e-3],
}

BR_b2ccbar = {
    "chic0" : [1.5e-2, 0.6e-2],
    "chic1" : [1.4e-2, 0.4e-2],
    "chic0" : [6.2e-3, 2.9e-3],
    "psi2S" : [3.06e-3, 0.30e-3],
    "jpsi"  : [1.16e-2, 0.10e-2],
}

xtalk_bb = {
    "chic0" : [0.699, 0.007],
    "chic1" : [0.712, 0.007],
    "chic2" : [0.718, 0.007],
    "jpsi"  : [0.680, 0.007],
    "psi2S" : [0.720, 0.007],
}

CUTKEY = "ProbNNp_06"

def get_corrYield(syst_list, var_list, kSource, kNorm="psi2S"):

    inCutKey = "splitFit"
    outDirName = homeDir + "/mass_fit/split_fit/"

    n_val = dict()
    n_err = dict()

    syst_c = dict()
    syst_uc = dict()

    #import os, sys
    #sys.stdout = open("{}/syst_{}.txt".format(homeDir,inCutKey), 'w' )


    kSyst = "Base"
    nameWksp    = outDirName + "Wksp_{}_{}_{}_base.root".format(kNorm,kSource,kSyst)
    file_w = TFile(nameWksp)
    w_temp = file_w.Get("w")

    for kVar in var_list:

        norm = BR_ppbar[kNorm][0]*BR_b2ccbar[kNorm][0] #/ BR_ppbar[kVar][0]
        n_val[kVar] = dict()
        n_err[kVar] = dict()

        n_val[kVar]["Mean"] = w_temp.var("n_{}".format(kVar)).getVal() * norm
        n_err[kVar]["Base"] = w_temp.var("n_{}".format(kVar)).getError()
        n_val[kVar]["Stat"] = n_err[kVar]["Base"] * norm#/n_val[kVar]["Mean"]

        n_err[kVar]["Corr"] = 0.
        n_err[kVar]["Uncorr"] = 0.

    for kSyst in syst_list:

        nameWksp    = outDirName + "Wksp_{}_{}_{}_base.root".format(kNorm,kSource,kSyst)
        file_w = TFile(nameWksp)
        w_temp = file_w.Get("w")

        for kVar in var_list:

            norm = BR_ppbar[kNorm][0]*BR_b2ccbar[kNorm][0] #/ BR_ppbar[kVar][0]
            #print(kSyst, kVar)
            n_val[kVar][kSyst] = (w_temp.var("n_{}".format(kVar)).getVal() * norm - n_val[kVar]["Mean"])#
            n_err[kVar][kSyst] = w_temp.var("n_{}".format(kVar)).getError()

            #if kSyst in corr_syst:
                #n_err[kVar]["Corr"] += n_val[kVar][kSyst]**2

            if kSyst in uncorr_syst:
                n_err[kVar]["Uncorr"] += n_val[kVar][kSyst]**2

    for kVar in var_list:
        print(kVar)
        for key in n_val[kVar]:
            print("{}:   {:.9f}".format(key,n_val[kVar][key]))

        #print("{}:   {:.2f}".format('Syst corr',n_err[kVar]["Corr"]))
        print("{}:   {:.9f}".format('Syst uncorr',n_err[kVar]["Uncorr"]**0.5))
        print("{}:   {:.9f} \n".format('Syst',(n_err[kVar]["Uncorr"] + n_err[kVar]["Corr"])**0.5))

    #sys.stdout.close()

def get_yield(syst_list, var_list, kSource, kNorm="psi2S", kBR=False):

    inCutKey = "splitFit"
    state = "rel"
    outDirName = f"{homeDir}/mass_fit/one_hist/CB/"

    n_val = dict()
    n_err = dict()

    syst_c = dict()
    syst_uc = dict()

    #import os, sys
    #sys.stdout = open("{}/syst_{}.txt".format(homeDir,inCutKey), 'w' )


    kSyst = "Base"
    nameWksp    = f"{outDirName}/Wksp_{kNorm}_{kSource}_{kSyst}_{CUTKEY}_{state}.root"
    file_w = TFile(nameWksp)
    w_temp = file_w.Get("w").Clone()
    file_w.Close()

    if kBR:
        norm = BR_ppbar[kNorm][0]*BR_b2ccbar[kNorm][0] #/ BR_ppbar[kVar][0]
    else:
        norm = 1

    for kVar in var_list:

        vname = f"{kVar}_fromB_rel"
        n_val[kVar] = dict()
        n_err[kVar] = dict()

        n_val[kVar]["Mean"] = w_temp.var("n_{}".format(vname)).getVal()*norm
        n_err[kVar]["Base"] = w_temp.var("n_{}".format(vname)).getError()
        n_val[kVar]["Stat"] = n_err[kVar]["Base"]#/n_val[kVar]["Mean"]

        # n_err[kVar]["cross_talk"] = n_val[kVar]["Mean"]*0.01
        n_val[kVar]["Syst"] = 0.
        n_val[kVar]["Corr"] = 0.
        n_val[kVar]["Uncorr"] = 0.

        n_err[kVar]["Syst"] = 0.
        n_err[kVar]["Corr"] = 0.
        n_err[kVar]["Uncorr"] = 0.

    for kSyst in syst_list:

        nameWksp    = f"{outDirName}/Wksp_{kNorm}_{kSource}_{kSyst}_{CUTKEY}_{state}.root"
        file_w = TFile(nameWksp)
        w_temp = file_w.Get("w").Clone()
        file_w.Close()

        for kVar in var_list:

            vname = f"{kVar}_fromB_rel"
            #print(kSyst, kVar)
            n_val[kVar][kSyst] = (w_temp.var("n_{}".format(vname)).getVal()*norm - n_val[kVar]["Mean"])#
            n_err[kVar][kSyst] = w_temp.var("n_{}".format(vname)).getError()

            #if kSyst in corr_syst:
                #n_val[kVar]["Corr"] += n_val[kVar][kSyst]**2
                #n_err[kVar]["Corr"] += n_val[kVar][kSyst]**2

            # if kSyst in uncorr_syst:
            n_val[kVar]["Uncorr"] += n_val[kVar][kSyst]**2
            n_err[kVar]["Uncorr"] += n_val[kVar][kSyst]**2

    cnorm = 0.01
    for kVar in var_list:

        n_val[kVar]["Syst"] = (n_val[kVar]["Uncorr"]**2 + n_val[kVar]["Corr"]**2)**0.5
        n_err[kVar]["Syst"] = (n_err[kVar]["Uncorr"]**2 + n_err[kVar]["Corr"]**2)**0.5
        n_err[kVar]["Uncorr"] = n_err[kVar]["Uncorr"]**0.5
        n_val[kVar]["Uncorr"] = n_val[kVar]["Uncorr"]**0.5

        print(kVar)
        for key in n_val[kVar]:
            print(f"{key:15s}:   {abs(n_val[kVar][key])/cnorm:.3f}")

        #print("{}:   {:.2f}".format('Syst corr',n_err[kVar]["Corr"]))
        # print("{}:   {:.9f}".format('Syst uncorr',n_err[kVar]["Uncorr"]**0.5))
        # print("{}:   {:.9f} \n".format('Syst',(n_err[kVar]["Uncorr"] + n_err[kVar]["Corr"])**0.5))
        print(f"{'Syst uncorr':15s}:   {n_val[kVar]['Uncorr']/cnorm:.3f}")
        print(f"{'Syst':15s}:   {n_val[kVar]['Syst']/cnorm:.3f} \n")

    #sys.stdout.close()
    return n_val, n_err

# def get_BR(vals:dict, errs:dict, norm:str):

#     for state in vals:

#         filename = "{0}/eff/note_v2/plots_{1}/ratio_{3}2{2}_{1}_PT_tot.root".format(homeDir, "sec", norm, state)
#         feff = TFile(filename,"READ")
#         eff = ufloat(feff.Get("eff_val").GetVal(), feff.Get("eff_err").GetVal())
#         feff.Close()

#         yield_stat = ufloat(vals[state]["Mean"],errs[state]["Base"])
#         yield_syst = ufloat(vals[state]["Mean"],errs[state]["Syst"])

        
#         br_stat = yield_stat * BR_ppbar[norm].nominal_value / BR_ppbar[state].nominal_value * eff.nominal_value
#         br_syst = yield_syst * BR_ppbar[norm].nominal_value / BR_ppbar[state].nominal_value * eff.nominal_value
#         br_br = yield_syst.nominal_value * BR_ppbar[norm] / BR_ppbar[state] * eff

#         print(f"State {state}")
#         print(f"Stat: {br_stat}")
#         print(f"Syst: {br_syst}")
#         print(f"BR: {br_br}")

def get_BRRel(vals:dict, errs:dict, norm:str):

    for state in vals:

        filename = "{0}/eff/note_v2/plots_{1}/ratio_{2}2{3}_{1}_PT_tot.root".format(homeDir, "sec", norm, state)
        feff = TFile(filename,"READ")
        eff_val = feff.Get("eff_val").GetVal()
        eff_err = feff.Get("eff_err").GetVal()
        feff.Close()
        ct_st = xtalk_bb[state]
        ct_nm = xtalk_bb[norm]

        yield_val  = vals[state]["Mean"]
        yield_stat = errs[state]["Base"]
        yield_syst = errs[state]["Syst"]

        # print(f"X-talk relative uncert.: {((ct_st[1]/ct_st[0])**2 + (ct_nm[1]/ct_nm[0])**2)**0.5}, {yield_val*((ct_st[1]/ct_st[0])**2 + (ct_nm[1]/ct_nm[0])**2)**0.5}")
        # print(f"Eff. relative uncert.: {eff_err/eff_val}, {yield_val*eff_err/eff_val}")

        br_val  = yield_val * eff_val * ct_nm[0]/ct_st[0]
        br_stat = (yield_stat/yield_val) * br_val
        br_syst = ((yield_syst/yield_val)**2 + (eff_err/eff_val)**2 + (ct_st[1]/ct_st[0])**2 + (ct_nm[1]/ct_nm[0])**2)**0.5 * br_val
        br_br   = 0.
        br_tot  = (br_stat**2 + br_syst**2 + br_br**2)**0.5

        cnorm = 1
        print(f"BR(b->{state}X) x BR({state}->ppbar) / BR(b->{norm}X) x BR({norm}->ppbar) ")
        print(f"Val : {br_val/cnorm}")
        print(f"Stat: {br_stat/cnorm}")
        print(f"Syst: {br_syst/cnorm}")
        print(f"BR  : {br_br/cnorm}")
        print(f"Tot : {br_tot/cnorm}")
        print(f"{br_val/cnorm:.2f} \\pm {br_stat/cnorm:.2f}_{{stat}} \\pm {br_syst/cnorm:.2f}_{{syst}} \\pm {br_br/cnorm:.2f}_{{\\BR}}")

        br_val  = yield_val * BR_ppbar[norm][0] / BR_ppbar[state][0] * eff_val
        br_stat = (yield_stat/yield_val) * br_val
        br_syst = ((yield_syst/yield_val)**2 + (eff_err/eff_val)**2)**0.5 * br_val
        br_br   = ((BR_ppbar[norm][1]/BR_ppbar[norm][0])**2 + (BR_ppbar[state][1]/BR_ppbar[state][0])**2)**0.5 * br_val
        br_tot  = (br_stat**2 + br_syst**2 + br_br**2)**0.5

        print(f"BR(b->{state}X) / BR(b->{norm}X) ")
        print(f"Val : {br_val/cnorm}")
        print(f"Stat: {br_stat/cnorm}")
        print(f"Syst: {br_syst/cnorm}")
        print(f"BR  : {br_br/cnorm}")
        print(f"Tot : {br_tot/cnorm}")
        print(f"{br_val/cnorm:.2f} \\pm {br_stat/cnorm:.2f}_{{stat}} \\pm {br_syst/cnorm:.2f}_{{syst}} \\pm {br_br/cnorm:.2f}_{{\\BR}}")


def get_BR(vals:dict, errs:dict, norm:str):

    for state in vals:

        filename = "{0}/eff/note_v2/plots_{1}/ratio_{2}2{3}_{1}_PT_tot.root".format(homeDir, "sec", norm, state)
        feff = TFile(filename,"READ")
        eff_val = feff.Get("eff_val").GetVal()
        eff_err = feff.Get("eff_err").GetVal()
        feff.Close()
        ct_st = xtalk_bb[state]
        ct_nm = xtalk_bb[norm]

        yield_val  = vals[state]["Mean"]
        yield_stat = errs[state]["Base"]
        yield_syst = errs[state]["Syst"]

        # print(f"X-talk relative uncert.: {((ct_st[1]/ct_st[0])**2 + (ct_nm[1]/ct_nm[0])**2)**0.5}, {yield_val*((ct_st[1]/ct_st[0])**2 + (ct_nm[1]/ct_nm[0])**2)**0.5}")
        # print(f"Eff. relative uncert.: {eff_err/eff_val}, {yield_val*eff_err/eff_val}")

        br_val  = yield_val * BR_ppbar[norm][0] * BR_b2ccbar[norm][0] * eff_val * ct_nm[0]/ct_st[0]
        br_stat = (yield_stat/yield_val) * br_val
        br_syst = ((yield_syst/yield_val)**2 + (eff_err/eff_val)**2 + (ct_st[1]/ct_st[0])**2 + (ct_nm[1]/ct_nm[0])**2)**0.5 * br_val
        br_br   = ((BR_ppbar[norm][1]/BR_ppbar[norm][0])**2 + (BR_b2ccbar[norm][1]/BR_b2ccbar[norm][0])**2)**0.5 * br_val
        br_tot  = (br_stat**2 + br_syst**2 + br_br**2)**0.5

        cnorm = 1e-7
        print(f"BR(b->{state}X) x BR({state}->ppbar)")
        print(f"Val : {br_val/cnorm}")
        print(f"Stat: {br_stat/cnorm}")
        print(f"Syst: {br_syst/cnorm}")
        print(f"BR  : {br_br/cnorm}")
        print(f"Tot : {br_tot/cnorm}")
        print(f"{br_val/cnorm:.2f} \\pm {br_stat/cnorm:.2f}_{{stat}} \\pm {br_syst/cnorm:.2f}_{{syst}} \\pm {br_br/cnorm:.2f}_{{\\BR}}")

        br_val  = yield_val * BR_ppbar[norm][0] / BR_ppbar[state][0] * BR_b2ccbar[norm][0] * eff_val
        br_stat = (yield_stat/yield_val) * br_val
        br_syst = ((yield_syst/yield_val)**2 + (eff_err/eff_val)**2)**0.5 * br_val
        br_br   = ((BR_ppbar[norm][1]/BR_ppbar[norm][0])**2 + (BR_ppbar[state][1]/BR_ppbar[state][0])**2 + (BR_b2ccbar[norm][1]/BR_b2ccbar[norm][0])**2)**0.5 * br_val
        br_tot  = (br_stat**2 + br_syst**2 + br_br**2)**0.5

        cnorm = 1e-3
        print(f"BR(b->{state}X)")
        print(f"Val : {br_val/cnorm}")
        print(f"Stat: {br_stat/cnorm}")
        print(f"Syst: {br_syst/cnorm}")
        print(f"BR  : {br_br/cnorm}")
        print(f"Tot : {br_tot/cnorm}")
        print(f"{br_val/cnorm:.2f} \\pm {br_stat/cnorm:.2f}_{{stat}} \\pm {br_syst/cnorm:.2f}_{{syst}} \\pm {br_br/cnorm:.2f}_{{\\BR}}")


if __name__ == "__main__":
    ccbar_list = ["chic0","chic1","chic2"]
    # ccbar_list = ["chic1","chic2"]
    # ccbar_list = ["chic0_fromB_rel","chic1_fromB_rel","chic2_fromB_rel"]
    nConfs = ["Gauss","SRatio","Exp","pppi0","Mass","Gamma"] #,"Sqrt"
    # kSources = ["fromB","prompt"]
    kSources = ["fromB"]
    kNorm = "jpsi"
    # kNorm = "chic0"
    for kSource in kSources:
        # get_corrYield(nConfs,ccbar_list,kSource,kNorm)
        n_val,n_err = get_yield(nConfs,ccbar_list,kSource,kNorm)
        # print(n_val,n_err)
        get_BR(n_val,n_err,kNorm)
        # get_BRRel(n_val,n_err,kNorm)
