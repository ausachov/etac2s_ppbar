#from ROOT import gROOT, gStyle
#from ROOT import TFile, TChain, TTree, TH1D, TGraph, TCanvas, TCut, TMath
#from ROOT import RooFit, RooStats
from ROOT import *
from ROOT.RooFit import *
from ROOT.RooStats import *
gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")

binWidth = 0.2
binWidthDraw = 10.0

minM_Low = 2850
maxM_Low = 3250
nBins_Low = int((maxM_Low-minM_Low)/binWidth)
binN_Low = int((maxM_Low-minM_Low)/binWidthDraw)

minM_High = 3320
maxM_High = 3780
nBins_High = int((maxM_High-minM_High)/binWidth)
binN_High = int((maxM_High-minM_High)/binWidthDraw)

binN_Tot  = int((maxM_High-minM_Low)/binWidthDraw)

binning = RooFit.Binning(binN_Tot, minM_Low, maxM_High)
binning_Jpsi = RooFit.Binning(binN_Low, minM_Low, maxM_Low)
binning_etac2s = RooFit.Binning(binN_High, minM_High, maxM_High)

range_tot = RooFit.Range(minM_Low,maxM_High)
range_Jpsi = RooFit.Range(minM_Low,maxM_Low)
range_etac2s = RooFit.Range(minM_High,maxM_High)

mrkSize = RooFit.MarkerSize(0.5)
lineWidth1 = RooFit.LineWidth(1)
lineWidth2 = RooFit.LineWidth(2)
lineStyle1 = RooFit.LineStyle(2)
lineStyle2 = RooFit.LineStyle(9)
lineColor1 = RooFit.LineColor(6)
lineColor2 = RooFit.LineColor(8)
name = RooFit.Name
cut = RooFit.Cut

optDic = {
    "Low":   ["J/#psi to p#bar{p}", binning_Jpsi, range_Jpsi ],
    "High":  ["#eta_c(2S) to p#bar{p}", binning_etac2s, range_etac2s],
    "All":   ["c#bar{c} to p#bar{p}", binning, range_tot]
}

def draw_range(w,kRange):

    if kRange=="Low":
        Jpsi_M = w.var("Jpsi_M_Low")
    if kRange=="High":
        Jpsi_M = w.var("Jpsi_M_High")


    model       = w.pdf("model_{}".format(kRange))
    modelBkg    = w.pdf("modelBkg_{}".format(kRange))
    modelSignal = w.pdf("modelSignal_{}".format(kRange))

    dh = w.data("dh_{}".format(kRange))

    ##draw range separately

    frame = Jpsi_M.frame(RooFit.Title(optDic[kRange][0]),RooFit.Range("fitRange_{}".format(kRange)))

    dh.plotOn(frame, optDic[kRange][1], mrkSize, lineWidth1, name("data_{}".format(kRange)))

    argset = RooArgSet(Jpsi_M)
    n_events = model.createIntegral(argset, RooFit.NormSet(argset), RooFit.Range("fitRange_{}".format(kRange))).getValV()
    model.plotOn(frame, lineWidth1, optDic[kRange][2], RooFit.Normalization(n_events,RooAbsReal.RelativeExpected), name("mod_{}".format(kRange)))
    modelSignal.plotOn(frame, lineWidth1, lineColor2, lineStyle1, optDic[kRange][2], name("sig_{}".format(kRange)), RooFit.Normalization(1.,RooAbsReal.RelativeExpected))

    n_events_bkg = modelBkg.createIntegral(argset, RooFit.NormSet(argset), RooFit.Range("fitRange_{}".format(kRange))).getValV()
    modelBkg.plotOn(frame, lineWidth1, lineColor1, lineStyle1, optDic[kRange][2],  RooFit.Normalization(0.,RooAbsReal.RelativeExpected))
    modelBkg.plotOn(frame, lineWidth1, lineColor1, lineStyle1, optDic[kRange][2], RooFit.Normalization(n_events_bkg,RooAbsReal.RelativeExpected), name("bkg_{}".format(kRange)))

    frame.GetYaxis().SetTitleOffset(1.5)
    frame.GetXaxis().SetTitleOffset(0.75)
    frame.GetXaxis().SetTitle("M(p#bar{p}) / [MeV/c^{2}]")
    frame.GetXaxis().SetTitleSize(0.045)
    frame.GetXaxis().SetTitleFont(12)
    frame.SetMinimum(-10.5)
    #frame.Draw()

    h_resid = frame.residHist("data_{}".format(kRange),"mod_{}".format(kRange))
    h_resid.SetMarkerSize(0.5)
    resid = Jpsi_M.frame(RooFit.Title(" "),RooFit.Bins(100), RooFit.Range("fitRange_{}".format(kRange)))
    resid.addPlotable(h_resid,"B")
    modelSignal.plotOn(resid, lineWidth1, optDic[kRange][2], name("sig_{}".format(kRange)))#, RooFit.Normalization(8.83177e+07,RooAbsReal.NumEvent))

    resid.GetXaxis().SetTitle("M(p#bar{p}) / [MeV/c^{2}]")
    resid.GetXaxis().SetTitleFont(12)
    #resid.Draw()

    h_pull = frame.pullHist("data_{}".format(kRange),"mod_{}".format(kRange))
    h_pull.SetMarkerSize(0.5)
    pull = Jpsi_M.frame(RooFit.Title(" "),RooFit.Bins(100), RooFit.Range("fitRange_{}".format(kRange)))
    for ii in range(h_pull.GetN()):
        h_pull.SetPointEYlow(ii,0)
        h_pull.SetPointEYhigh(ii,0)
    pull.SetMinimum(-4.0)
    pull.SetMaximum(4.0)
    pull.GetXaxis().SetTitleSize(0.0)
    pull.GetXaxis().SetLabelSize(0.06)
    pull.GetYaxis().SetLabelSize(0.06)
    pull.addPlotable(h_pull,"B")
    #pull.Draw()

    return frame, resid, pull



def setFrame(w,kRange="",kSource=""):

    if kRange=="Low":
        Jpsi_M = w.var("Jpsi_M_Low")
    if kRange=="High":
        Jpsi_M = w.var("Jpsi_M_High")

    postFix = ""
    if kRange!="":
        postFix += "_{}".format(kRange)
    if kSource!="":
        postFix += "_{}".format(kSource)


    modelSignal = w.pdf("modelSignal{}".format(postFix))
    dh = w.data("dh{}".format(postFix))

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf")
    combData = w.data("combData")

    ##draw low and high separately

    frame = Jpsi_M.frame(RooFit.Title(optDic[kRange][0]), RooFit.Range("fitRange{}".format(postFix)))

    combData.plotOn(frame, cut("sample==sample::{}".format(postFix[1:])), optDic[kRange][1], mrkSize, lineWidth1, name("data{}".format(postFix)))

    simPdf.plotOn(frame, RooFit.Slice(sample,"{}".format(postFix[1:])), RooFit.ProjWData(RooArgSet(sample),combData,True), lineWidth1, optDic[kRange][2], name("mod{}".format(postFix)))
    modelSignal.plotOn(frame, lineWidth1, lineColor2, lineStyle1, optDic[kRange][2], name("sig{}".format(postFix)), RooFit.Normalization(1.,RooAbsReal.RelativeExpected))

    simPdf.plotOn(frame, RooFit.Slice(sample,"{}".format(postFix[1:])), RooFit.ProjWData(RooArgSet(sample),combData,True), RooFit.Components("bkg{}".format(postFix)), lineWidth1, lineColor1, lineStyle1, optDic[kRange][2],  RooFit.Normalization(0.,RooAbsReal.RelativeExpected))
    simPdf.plotOn(frame, RooFit.Slice(sample,"{}".format(postFix[1:])), RooFit.ProjWData(RooArgSet(sample),combData,True), RooFit.Components("bkg{}".format(postFix)), lineWidth1, lineColor1, lineStyle1, optDic[kRange][2], name("bkg{}".format(postFix)))


    frame.GetYaxis().SetTitleOffset(1.5)
    frame.GetXaxis().SetTitleOffset(0.75)
    frame.GetXaxis().SetTitle("M(p#bar{p}) / [MeV/c^{2}]")
    frame.GetXaxis().SetTitleSize(0.045)
    frame.GetXaxis().SetTitleFont(12)
    frame.SetMinimum(-10.5)
    frame.Draw()


    #print("check")
    h_resid = frame.residHist("data{}".format(postFix),"bkg{}".format(postFix))
    h_resid.SetMarkerSize(0.5)
    resid = Jpsi_M.frame(RooFit.Title(" "), RooFit.Bins(100), RooFit.Range("fitRange{}".format(postFix)))
    resid.addPlotable(h_resid,"P")
    modelSignal.plotOn(resid, lineWidth1, optDic[kRange][2], name("sig{}".format(postFix)))#, RooFit.Normalization(8.83177e+07,RooAbsReal.NumEvent))

    resid.GetXaxis().SetTitle("M(p#bar{p}) / [MeV/c^{2}]")
    resid.GetXaxis().SetTitleFont(12)
    resid.Draw()

    h_pull = frame.pullHist("data{}".format(postFix),"mod{}".format(postFix))
    h_pull.SetMarkerSize(0.5)
    pull = Jpsi_M.frame(RooFit.Title(" "),RooFit.Bins(100), RooFit.Range("fitRange{}".format(postFix)))
    for ii in range(h_pull.GetN()):
        h_pull.SetPointEYlow(ii,0)
        h_pull.SetPointEYhigh(ii,0)
    pull.SetMinimum(-4.0)
    pull.SetMaximum(4.0)
    pull.GetXaxis().SetTitleSize(0.0)
    pull.GetXaxis().SetLabelSize(0.06)
    pull.GetYaxis().SetLabelSize(0.06)
    pull.addPlotable(h_pull,"B")


    return frame, resid, pull


def setCanvas(names, frames, resids, pulls):

    nPads = len(names)

    texData = TLatex()
    texData.SetNDC()
    texData.SetTextSize(0.06)
    TGaxis.SetMaxDigits(4)

    c = TCanvas("c1","c1", 1000, 700)
    c.Divide(nPads, 3, 0.001, 0.001)
    for i in range(nPads):

            pad = c.cd(i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            pad.SetPad(xl,yl-0.2,xh,yh)
            pad.SetBottomMargin(0.15)
            pad.SetLeftMargin(0.15)
            frames[i].Draw()

            texData.DrawLatex(0.3, 0.95, names[i])

            #ptL = binningDict["Jpsi_PT"]
            #ptR = binningDict["Jpsi_PT"][nPT+1]
            #texData.DrawLatex(0.3, 0.95, "%s<p_{T}<%s"%(ptL,ptR))

            pad = c.cd(nPads+i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            pad.SetPad(xl,yl-0.1,xh,yh-0.2)
            pad.SetBottomMargin(0.15)
            pad.SetLeftMargin(0.15)
            #pad.SetTopMargin(0)
            #pad.SetTickx(2)
            #pad.SetTicky(2)
            resids[i].Draw()


            pad = c.cd(2*nPads+i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            #pad.SetTickx(2)
            #pad.SetTicky(2)
            pad.SetPad(xl,yl,xh,yh-0.1)
            pad.SetLeftMargin(0.15)
            pulls[i].Draw()
    return c
