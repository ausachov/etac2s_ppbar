from ROOT import RooDataHist, RooArgList, RooRealVar, \
                    RooDataSet, RooArgSet, RooProfileLL, \
                    RooWorkspace, RooCmdArg, RooLinkedList
from ROOT import Math, gPad, gStyle
# from ROOT.RooStats import *

from model_sim import *
import drawModule_splitFit as dm
gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../libs/libBifurcatedCB/BifurcatedCB.cxx+")
from ROOT import RooRelBreitWigner, BifurcatedCB

import numpy as np
import logging 

homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

CUTKEY = "ProbNNp_06"

def print_yields(w:RooWorkspace, state:str="etac2S"):

    states = ["etac","etac2S","chic0","chic1","chic2","hc","jpsi","psi2S"]
    states.pop(states.index(state))
    print(states)

    for ccbar in states:
        print(f'\\{ccbar} & ${w.var(f"n_{ccbar}_prompt").getVal():.1f} \\pm {w.var(f"n_{ccbar}_prompt").getError():.1f}$ & ${w.var(f"n_{ccbar}_fromB").getVal():.1f}\\pm {w.var(f"n_{ccbar}_fromB").getError():.1f}$ \\\\')

if __name__ == "__main__":

    gStyle.SetStatFormat("4.3f")
    gStyle.SetOptTitle(0)
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

    kSysts = ["Base","ChebAlt", "Exp","Gamma","GammaL","eff","CB","BR","cross_talk","Resolution","pppi0"]
    kSysts = ["Base"]
    kNorm = 'jpsi'
    kState = "hc"
    # for kBkg in kBkgs:
    for kSyst in kSysts:
        nameWksp = f"{homeDir}/mass_fit/one_hist/CB/Wksp_{kNorm}_sim_{kSyst}_{CUTKEY}_{kState}.root"
        file_w = TFile(nameWksp)
        w = file_w.Get("w")
        print_yields(w,kState)
        file_w.Close()
        # del w

