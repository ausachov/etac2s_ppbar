from multiprocessing import Pool
from functools import partial
from contextlib import closing
from ROOT import gROOT, gStyle, gPad, RooFit, RooStats, \
                RooArgSet, RooArgList, RooMinimizer, RooFormulaVar, RooProfileLL, RooWorkspace, RooLinkedList, RooCmdArg, \
                Math, TCanvas, TFile, TLatex, TGraph

# from mass_simFit import *
import drawModule_splitFit as dm
import fit.mass_fit.oniaConstLib as cl

gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../libs/libBifurcatedCB/BifurcatedCB.cxx+")
homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"
gitDir  = "/users/LHCb/zhovkovska/etac2s_ppbar/fit/"

# MODEL = "Gauss"
# MODEL = "CB/noMisID"
# CUTKEY = "ProbNNp_06_df"
MODEL = "CB"
CUTKEY = "ProbNNp_06"

import sys
sys.path.insert(1, f"{gitDir}/libs/")
from charmonium import Particle

def setModelConfig(w:RooWorkspace, src:str, norm:str, state:str, constr:bool=False):

    Jpsi_M = w.var("Jpsi_M")

    cc = Particle(state)

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf_constr")
    # simPdf = w.pdf("simPdf")
    combData = w.data("combData")

    nuisanceParams = RooArgSet()
    var_list_0 = w.allVars()

    nuis_iter = var_list_0.createIterator()
    var = nuis_iter.Next()
    while var:
        if "extFactorObs" not in var.GetName() and not var.isConstant():
            nuisanceParams.add(var)
            # var.setConstant(True)
        var = nuis_iter.Next()

    # w.var("cs_etac2S_rel_prompt").setConstant(True)
    # w.var("cs_etac2S_rel_fromB").setConstant(True)

    poi_var = w.var(f"cs_{state}_rel_{src}")
    poi_var.setConstant(False)

    nuisanceParams.remove(Jpsi_M)
    nuisanceParams.remove(poi_var)

    ## ULs Calculator using HypoTestInverter

    n_bkg_pr  = w.var("n_bkg_prompt")
    n_bkg_fb  = w.var("n_bkg_fromB")

    # n_bkg_pr.setConstant(False)
    # n_bkg_fb.setConstant(False)

    all_states = ["jpsi", "etac", "chic0", "chic1", "chic2", "hc", "psi2S","etac2S"]
    all_states.remove(state)
    
    bgParams = RooArgSet()
    constrainedParams = RooArgSet()

    constrainedParams.add(w.var("mass_etac"))
    constrainedParams.add(w.var("mass_jpsi"))
    constrainedParams.add(w.var("mass_psi2S"))
    constrainedParams.add(n_bkg_pr)
    constrainedParams.add(n_bkg_fb)
    constrainedParams.add(w.var(f"sigma_{norm}_1"))

    for source in ["prompt","fromB"]:
        for ccbar in all_states:
            # w.var(f"n_{ccbar}_{source}").setConstant(True)
            constrainedParams.add(w.var(f"n_{ccbar}_{source}"))

        if source=="prompt":
            n_bgpar = 4 
        else:
            n_bgpar = 3
    
        for i in range(n_bgpar):
            bgParams.add(w.var(f"a{i}_{source}"))



    if src=="prompt":
        poi_var.setRange(0., 15.0)
        # poi_var.setRange(0., 0.3)
        # poi_var.setRange(0., 0.225)
    else:
        # poi_var.setRange(0., 1.0)
        poi_var.setRange(0., 0.1)

    poi_set = RooArgSet(poi_var)

    # w.var("n_jpsi_prompt").setConstant(False)
    # w.var("n_etac_prompt").setConstant(False)
    # w.var("sigma_{}_1".format(norm)).setConstant(False)


    if constr:

        # simPdf_def = simPdf

        # # make constraints for all non-constant parameters
        # constrList = RooArgList(simPdf_def)
        # constrPdfs = []

        nuis_iter = constrainedParams.createIterator()  # ??? probably replace with nuisanceParams ???
        var = nuis_iter.Next()
        # band_size = 5
        band_size = 20
        while var:
            # if not var.isConstant() and "n_" in var.GetName():
            #     val = var.getVal()
            #     band = band_size*var.getError()
            #     var.setRange(max(0., val-band), val+band)
            #     print("{}: [{}, {}]".format(var.GetName(), max(0., val-band), val+band))
            if not var.isConstant() and "n_bkg" not in var.GetName() and "n_jpsi" not in var.GetName() and "mass" not in var.GetName():
                var.setConstant()
                # print("{}: [{}, {}]".format(var.GetName(), max(0., val-band), val+band))

            var = nuis_iter.Next()


        nuis_iter = bgParams.createIterator()
        var = nuis_iter.Next()
        while var:
            val = var.getVal()
            band = 2*band_size*var.getError()
            var.setRange(val-band, val+band)
            var.setConstant(False)
            print("{}: [{}, {}]".format(var.GetName(), val-band, val+band))

            # gauss = RooGaussian("constr_{}".format(var.GetName()),"constr",var, RooFit.RooConst(val), RooFit.RooConst(var.getError()))
            # constrPdfs.append(gauss)
            # constrList.add(gauss)

            var = nuis_iter.Next()



        # simPdf = RooProdPdf("simPdf_constr_new","simPdf_constr_new", constrList)

    # nuisanceParams.add(n_bkg)
    #constrainedParams.add(bgParams)
    #constrainedParams.add(RooArgSet(BR_jpsi_mean, BR_etac2S_mean))

    # simPdf = w_syst.pdf("simPdf")
    # model_base = w.pdf("model_constr")

    # cs_err = abs(w_syst.var("cs_etac2S_rel").getValV() - poi_var.getValV())
    # cs_val = w.var("cs_etac2S_rel").getValV()
    # print("{} +/- {}".format(cs_val,cs_err))
    # constr_cs = RooGaussian("constr_cs","constr_cs", poi_var, RooFit.RooConst(cs_val), RooFit.RooConst(cs_err))
    # model = RooProdPdf("model_syst","model_syst", RooArgList(model_base, constr_cs))

    modelConfig = RooStats.ModelConfig(w);
    modelConfig.SetPdf(simPdf)
    # modelConfig.SetPdf(simPdf_constr)
    modelConfig.SetProtoData(combData)
    modelConfig.SetParametersOfInterest(poi_set)
    modelConfig.SetNuisanceParameters(nuisanceParams)
    modelConfig.SetObservables(RooArgSet(RooArgList(Jpsi_M, sample)))

    # # GlobalObservables - measurement from the other experiment, e.g. BR
    w.var(f"cs_{state}_rel_{'fromB'}").setConstant(True)
    w.var(f"extFactor_fromB").setConstant(True)
    # extFactor_pr = w.var("extFactor_prompt".format(src))
    # extFactorObs_pr = w.var("extFactorObs_prompt".format(src))
    # extFactor_pr.setConstant(False)
    # extFactor_fb = w.var("extFactor_fromB".format(src))
    # extFactorObs_fb = w.var("extFactorObs_fromB".format(src))
    # extFactor_fb.setConstant(False)
    extFactor = w.var("extFactor_{}".format(src))
    extFactorObs = w.var("extFactorObs_{}".format(src))
    extFactor.setConstant(False)
    # globalObs = RooArgSet(extFactorObs)
    # globalParams = RooArgSet(extFactor_fb, extFactor_pr)
    # globalObs = RooArgSet(extFactorObs_fb, extFactorObs_pr)
    # globalParams = RooArgSet(extFactor)
    # globalObs = RooArgSet(extFactorObs, RooFit.RooConst(cc.mass), RooFit.RooConst(cc.width))
    if w.var(f"gamma_{state}")!=None and w.var(f"gamma_{state}").getVal()>1e-3:
        globalParams = RooArgSet(extFactor, w.var(f"mass_{state}"),w.var(f"gamma_{state}"))
        globalObs = RooArgSet(extFactorObs, w.var(f"mass_{state}_Obs"),w.var(f"width_{state}_Obs"))
        # globalParams = RooArgSet(extFactor_fb, extFactor_pr, w.var(f"mass_{state}"),w.var(f"gamma_{state}"))
        # globalObs = RooArgSet(extFactorObs_fb, extFactorObs_pr, w.var(f"mass_{state}_Obs"),w.var(f"width_{state}_Obs"))
    else:
        globalParams = RooArgSet(extFactor, w.var(f"mass_{state}"))
        globalObs = RooArgSet(extFactorObs, w.var(f"mass_{state}_Obs"))
        # globalParams = RooArgSet(extFactor_fb, extFactor_pr, w.var(f"mass_{state}"))
        # globalObs = RooArgSet(extFactorObs_fb, extFactorObs_pr, w.var(f"mass_{state}_Obs"))

    # globalObs = RooArgSet(w.var("extFactorObs_fromB"), w.var("extFactorObs_prompt"))
    # globalParams = RooArgSet(w.var("extFactor_fromB"), w.var("extFactor_prompt"))

    # modelConfig.SetGlobalObservables(RooArgSet(extFactorObs))
    # modelConfig.SetGlobalObservables(globalObs)
    modelConfig.SetConstraintParameters(globalParams)
    # # I don't know what  ConstraintParameters are ...

    modelConfig.SetName("ModelConfig")
    poi_set.first().setVal(poi_var.getVal())
    # modelConfig.SetSnapshot(poi_set)

    nuisanceParams.add(poi_var)
    modelConfig.SetSnapshot(nuisanceParams)
    nuisanceParams.remove(poi_var)
    getattr(w,"import")(modelConfig)
    modelConfig.Print()

    w_temp = modelConfig.GetWS() 
    # w_temp.Print()
    # w_temp.writeToFile("{}/mass_fit/one_hist/Wksp_ModConf_{}_sim_{}_{CUTKEY}.root".format(homeDir, norm, syst))


def calcCLs(w:RooWorkspace, src:str, syst:str, norm:str, state:str, asympt:bool=True):

    # Math.MinimizerOptions.SetDefaultMinimizer("Minuit","Migrad");
    # Math.MinimizerOptions.SetDefaultStrategy(1);
    # Math.MinimizerOptions.SetDefaultTolerance(1)
    # Math.MinimizerOptions.SetDefaultMaxFunctionCalls(1000000)
    # Math.MinimizerOptions.SetDefaultMaxIterations(100000)

    Jpsi_M = w.var("Jpsi_M")

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf_constr")
    # simPdf = w.pdf("simPdf")
    combData = w.data("combData")

    poi_var = w.var(f"cs_{state}_rel_{src}")
    poi_var.setConstant(False)

    ## ULs Calculator using HypoTestInverter

    setModelConfig(w,src,norm, state, True)

    sbModel = w.obj("ModelConfig")
    poi = sbModel.GetParametersOfInterest().first()
    sbModel.SetSnapshot(RooArgSet(poi))
    bModel = sbModel.Clone()
    bModel.SetName(sbModel.GetName()+"_with_poi_0")
    poi.setVal(0.)
    bModel.SetSnapshot(RooArgSet(poi))

    # poi.setConstant(False)
    # nuisanceParams = bModel.GetNuisanceParameters()
    # nuisanceParams.add(poi_var)
    # bModel.SetSnapshot(nuisanceParams)
    # nuisanceParams.remove(poi_var)
    # sbModel.Print()
    # bModel.Print()

    cl_value = 0.95

    if asympt:
        calc_type = RooStats.AsymptoticCalculator(combData, bModel, sbModel)
        calc_type.SetOneSided(True)
        calc_type.SetPrintLevel(-1)
        # ac.SetQTilde(False)
        # ac.SetPrintLevel(4)
    else:
        calc_type = RooStats.FrequentistCalculator(combData, bModel, sbModel)
        # # # do these values change anything ?
        calc_type.SetToys(10,5)

    calc = RooStats.HypoTestInverter(calc_type,poi_var)
    calc = RooStats.HypoTestInverter(calc_type,poi_var)
    # calc = RooStats.HypoTestInverter(fc,poi_var)
    calc.SetConfidenceLevel(cl_value)

    calc.UseCLs(True)
    calc.SetVerbose(False)

    toymcs = calc.GetHypoTestCalculator().GetTestStatSampler()

    profll = RooStats.ProfileLikelihoodTestStat(sbModel.GetPdf())
    profll.SetOneSided(True)
    profll.SetLOffset(True)
    # profll.SetStrategy(0)
    # profll.SetMinimizer("Minuit2")
    # profll.SetReuseNLL(True)
    toymcs.SetTestStatistic(profll)
    toymcs.SetConfidenceLevel(cl_value)

    if not sbModel.GetPdf().canBeExtended():
        toymcs.SetNEventsPerToy(1)
        print( 'can not be extended')

    npoints = 10
    poimin = poi.getMin()
    poimax = poi.getMax()
    # poimax = poi.getMax()/4.

    RooStats.UseNLLOffset(True)

    calc.SetFixedScan(npoints,poimin,poimax)
    r = calc.GetInterval()
    expUpperLimit = r.GetExpectedUpperLimit()
    obsUpperLimit = r.UpperLimit()
    print("Exp Upper Limit is", expUpperLimit)
    print("Obs Upper Limit is", obsUpperLimit)


    plot = RooStats.HypoTestInverterPlot("HTI_Result_Plot","HypoTest Scan Result",r)
    c = TCanvas("HypoTestInverter Scan", "HypoTestInverter Scan", 55,55,550,400)
    c.cd(),  gPad.SetLeftMargin(0.20)
    c.SetLogy(False)
    plot.Draw("CLb 2CL")

    #plot.Draw("EXP")
    c.RangeAxis(poimin, 0., poimax, 1.1)
    c.Draw()
    c.SaveAs(f"{homeDir}/mass_fit/one_hist/{MODEL}/CL_Fq_{src}_{syst}_{norm}_{CUTKEY}_check_sim_{state}.root")
    c.SaveAs(f"{homeDir}/mass_fit/one_hist/{MODEL}/CL_Fq_{src}_{syst}_{norm}_{CUTKEY}_check_sim_{state}.pdf")


def updateConstr(w, src, syst, norm):

    if syst=="Base":
        return 0
    else:
        nameWksp = f"{homeDir}/mass_fit/one_hist/Wksp_{norm}_{src}_{syst}_{CUTKEY}.root".format(homeDir,norm,src,syst)
        # # nameWksp = homeDir + "/mass_fit/one_hist/Wksp_{}_{}_{}_misID.root".format(norm,src,"Base")
        file_w_syst = TFile(nameWksp)
        w_syst = file_w_syst.Get("w").Clone("w")
        file_w_syst.Close()

        syst_var = w_syst.var("cs_etac2S_rel_{}".format(src))
        val_syst = syst_var.getValV()
        val_base = w.var("cs_etac2S_rel_{}".format(src))
        return (val_syst-val_base)/val_base
        

def calcMCMC(w: RooWorkspace, src: str, syst: str, norm: str, state: str, postfix: str=""):


    cl_value = 0.95

    Math.MinimizerOptions.SetDefaultMinimizer("Minuit","Migrad")
    Math.MinimizerOptions.SetDefaultStrategy(2)
    Math.MinimizerOptions.SetDefaultTolerance(1)
    Math.MinimizerOptions.SetDefaultMaxFunctionCalls(1000000)
    Math.MinimizerOptions.SetDefaultMaxIterations(100000)

    RooStats.UseNLLOffset(True)

    
    Jpsi_M = w.var("Jpsi_M")

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf_constr")
    # simPdf = w.pdf("simPdf")
    combData = w.data("combData")

    poi_var = w.var(f"cs_{state}_rel_{src}")
    poi_var.setConstant(False)

    ## ULs Calculator using HypoTestInverter

    setModelConfig(w,src,norm, state, True)

    sbModel = w.obj("ModelConfig")
    poi = sbModel.GetParametersOfInterest().first()
    bModel = sbModel.Clone()
    bModel.SetName(sbModel.GetName()+"_with_poi_0")
    poi.setVal(0.)
    bModel.SetSnapshot(RooArgSet(poi))


    # constr_pr = w.pdf("constr_ext_prompt")
    # constr_fb = w.pdf("constr_ext_fromB")

    # pp_pr = RooStats.PdfProposal(constr_pr)
    # pp_pr.AddMapping(w.var("extFactorObs_prompt"), w.var("extFactor_prompt"))
    sp = RooStats.SequentialProposal(0.1)
    # sp = RooStats.SequentialProposal(0.2)

    mcmc = RooStats.MCMCCalculator(combData, sbModel)
    mcmc.SetConfidenceLevel(cl_value)
    mcmc.SetProposalFunction(sp)
    # mcmc.SetProposalFunction(pp_pr)
    mcmc.SetLeftSideTailFraction(0.)
    mcmc.SetNumIters(1000000)
    # mcmc.SetNumIters(200000)
    # mcmc.SetNumIters(10000)
    # mcmc.SetNumBurnInSteps(50)    
    mcmc.SetNumBurnInSteps(50)    
    # mcmc.SetNumBurnInSteps(75)    
    interval = mcmc.GetInterval() 
    MCMC_PLC = interval.UpperLimit(poi)

    print("MCMC_PLC = ",MCMC_PLC)

    c = TCanvas("IntervalPlot","IntervalPlot",55,55,550,400)
    c.cd(),  gPad.SetLeftMargin(0.20)
    frame = c.DrawFrame(0, -110, 2, 110)    
    frame.GetYaxis().SetTitle("Posterior")
    frame.GetXaxis().SetTitle("#frac{#sigma_{#eta_{c}(2S)} #times BR(#eta_{c}(2S)#rightarrow p#bar{p})}{#sigma_{J/#psi} #times BR(J/#psi#rightarrow p#bar{p})}")
    frame.SetTitle("")
    frame.GetXaxis().SetTitleOffset(0.9)
    frame.GetYaxis().SetTitleOffset(0.9)
    plot = RooStats.MCMCIntervalPlot(interval)
    plot.Draw()

    tl = TLatex()
    tl.DrawLatex(0.7, 0.85, "MCMC_PLC = {}".format(MCMC_PLC))
    
    c.SaveAs(f"{homeDir}/mass_fit/one_hist/{MODEL}/MCMC_{src}_{syst}_{norm}_{CUTKEY}_sim_{state}{postfix}.pdf")
    c.SaveAs(f"{homeDir}/mass_fit/one_hist/{MODEL}/MCMC_{src}_{syst}_{norm}_{CUTKEY}_sim_{state}{postfix}.root")

    with open(f"{homeDir}/mass_fit/one_hist/{MODEL}/MCMC_{src}_{syst}_{norm}_{CUTKEY}_sim_{state}{postfix}.txt","w") as f:
        f.write("MCMC_PLC = {}".format(MCMC_PLC))


def calcPLC(w, src, syst, norm):

    cl_value = 0.95

    
    Math.MinimizerOptions.SetDefaultMinimizer("Minuit2","Migrad");
    # Math.MinimizerOptions.SetDefaultStrategy(2);
    # Math.MinimizerOptions.SetDefaultTolerance(10)
    # Math.MinimizerOptions.SetDefaultMaxFunctionCalls(1000000)

    Jpsi_M = w.var("Jpsi_M")

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf_constr")
    combData = w.data("combData")
    # simPdf = w.pdf("simPdf")


    globalObs = RooArgSet(w.var("extFactorObs_fromB"), w.var("extFactorObs_prompt"))
    globalParams = RooArgSet(w.var("extFactor_fromB"), w.var("extFactor_prompt"))

    ### C o n s t r u c t   p l a i n   l i k e l i h o o d
    ### ---------------------------------------------------

    var_list_0 = w.allVars()

    nuis_iter = var_list_0.createIterator()
    var = nuis_iter.Next()
    while var:
        if "extFactorObs" not in var.GetName() and not var.isConstant():
            var.setConstant(True)
        var = nuis_iter.Next()

    n_bkg_pr  = w.var("n_bkg_prompt"); n_bkg_pr.setConstant(False)
    n_bkg_fb  = w.var("n_bkg_fromB"); n_bkg_fb.setConstant(False)


    # Construct unbinned likelihood
    nll = simPdf.createNLL(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Constrain(globalParams), RooFit.GlobalObservables(globalObs)) #RooFit.NumCPU(12, 3)

    # Minimize likelihood w.r.t all parameters before making plots



    m = RooMinimizer(nll)
    # strategy = Math.MinimizerOptions.DefaultStrategy()
    # m.setStrategy(strategy)
    # # use tolerance - but never smaller than 1 (default in RooMinimizer)
    # tol = Math.MinimizerOptions.DefaultTolerance()
    # tol = max(tol,1.0) # 1.0 is the minimum value used in RooMinimizer
    # m.setEps(tol)
    # # m.setPrintLevel(2)
    m.setOffsetting(True)

    # status = -1
    # minim.optimizeConst(2);
    minimizer = Math.MinimizerOptions.DefaultMinimizerType(); 
    algorithm = Math.MinimizerOptions.DefaultMinimizerAlgo(); 

    status = m.minimize(minimizer, algorithm)
    
    # m = RooMinuit(nll)
    # m.migrad()
    # m.hesse()
    # m.save()
    #m.minos()


    # previous = RooAbsReal.hideOffset()
    # RooAbsReal.setHideOffset(True)
    # bestVal = nll.getVal()
    # print(bestVal)
    # if (not previous):  RooAbsReal.setHideOffset(False) 
    
    # # poi_var.setVal(0.)
    # # poi_var.setConstant(True)

    # m.migrad()
    # m.save()
    # previous = RooAbsReal.hideOffset()
    # RooAbsReal.setHideOffset(True)
    # bkgVal = nll.getVal()
    # print(bkgVal)
    # if (not previous):  RooAbsReal.setHideOffset(False) 

    # qmu = 2*(bkgVal-bestVal)
    # print(qmu)
    # input()

    # # The profile likelihood estimator on nll for frac will minimize nll w.r.t
    # # all floating parameters except frac for each evaluation

    poi = w.var("cs_etac2S_rel_{}".format(src))
    # # Plot likelihood scan frac
    frameLL = poi.frame(RooFit.Bins(20), RooFit.Title("LL and profileLL"))

    # pll = nll.createProfile(poi)
    pll = RooProfileLL("pll", "pll", nll, RooArgSet(poi))
    llhood = RooFormulaVar("llhood","exp(-0.5*pll)",RooArgList(pll));


    # # Plot the profile likelihood in frac
    # llist = RooLinkedList()
    # llist.Add(RooCmdArg(LineColor(1)))
    # pll.plotOn(frameLL,llist)
    # nll.plotOn(frameLL,llist)
    # pll.plotOn(frameLL, dm.lineColor1)

    # # # Adjust frame maximum for visual clarity
    # frameLL.SetMinimum(0)
    # frameLL.GetXaxis().SetRangeUser(poimin, poimax)

    cNLL = TCanvas("profilell","profilell",55,55,550,400)
    cNLL.cd(),  gPad.SetLeftMargin(0.20)
    # frameLL.GetYaxis().SetTitleOffset(1.4),  frameLL.Draw()

    # cNLL.SaveAs(homeDir + "/mass_fit/one_hist/NLL_{}_{}_{}_{CUTKEY}_check.pdf".format(src,syst,norm))

    # nll.Write()
    # pll.Write()

    import numpy as np

    poimin = poi.getMin()
    poimax = poi.getMax()

    fllhood = llhood.asTF(RooArgList(poi))

    npoints = 25
    int_array = np.ones(npoints+1)
    int_val0 = fllhood.Integral(poimin, poimax, 1e-4)
    # print(int_val0)
    poi_urs = np.linspace(poimin, poimax, npoints+1)

    fll_ul = 0.
    for idx in range(npoints):

        int_val = fllhood.Integral(poimin, poi_urs[idx+1], 1e-4)
        int_array[idx+1] = 1.-int_val/int_val0
        if int_array[idx+1]<0.05: 
            fll_ul = poi_urs[idx+1]
            print(fll_ul)

    print(int_array)
    gr_pval = TGraph(npoints+1, poi_urs, int_array)
    cNLL.cd()
    gr_pval.SetLineColor(2)
    gr_pval.GetXaxis().SetRangeUser(poimin, poimax)
    # gr_pval.Draw("same")
    gr_pval.Draw()

    cNLL.SaveAs(f"{homeDir}/mass_fit/one_hist/{MODEL}/CL_As_check_{src}_{syst}_{norm}_{CUTKEY}_check_sim.pdf")
    cNLL.SaveAs(f"{homeDir}/mass_fit/one_hist/{MODEL}/CL_As_check_{src}_{syst}_{norm}_{CUTKEY}_check_sim.root")

    #import os, sys
    #sys.stdout = open("{}/UL_{}_{}_{}.txt".format(homeDir,src, norm, syst), 'w' )
    # fo = open("{}/mass_fit/one_hist/UL_{}_{}_{}_{CUTKEY}_check_sim.txt".format(homeDir,src, syst, norm), 'w' )
    # fo.write("Upper Limit at {} CL \n".format(cl_value))
    # fo.write("Expexted: {} \n".format(expUpperLimit))
    # fo.write("Observed: {} \n".format(obsUpperLimit))
    # # fo.write("Bayesian: {} \n".format(fll_ul))
    # #sys.stdout.close()
    # fo.close()

    # from ROOT.RooStats import BayesianCalculator, ProfileLikelihoodCalculator

    # #---PLC Upper Limit---#
    # POI_PLC_UL = RooArgSet(poi)
    # plc_UL = ProfileLikelihoodCalculator(combData, simPdf, POI_PLC_UL)   #Call to PLC with new range on nsig
    # plc_UL.SetConfidenceLevel(cl_value)
    # plInt_UL = plc_UL.GetInterval()                 #LikelihoodInterval*
    # #UL_PLC = plInt_UL.UpperLimit(nsig)
    # UL_PLC = plInt_UL.UpperLimit(poi)

    # print("UL_PLC = ",UL_PLC)


# def evalPLL(w, src, syst, norm):
def evalPLL(src:str="prompt", syst:str="Base", norm:str="jpsi", state:str="etac2S",postfix:str=""):

    cl_value = 0.95

    print(f"INFO: Computing PLL for {src} {state} for {syst} systematic")
    print("-----------------------------------------")

    nameWksp = f"{homeDir}/mass_fit/one_hist/{MODEL}/Wksp_{norm}_sim_{syst}_{CUTKEY}_{state}{postfix}.root"
    if syst=="Range":
        nameWksp = "{}/mass_fit/sim_fit/Wksp_{}_sim_{}_{CUTKEY}.root".format(homeDir,norm,"Base")
    # nameWksp = homeDir + "/mass_fit/one_hist/Wksp_{}_sim_{}_{CUTKEY}_prompt.root".format(norm,syst)
    file_w = TFile(nameWksp)
    w = file_w.Get("w").Clone()
    file_w.Close()
    # Math.MinimizerOptions.SetDefaultMinimizer("Minuit2","Migrad");
    # Math.MinimizerOptions.SetDefaultStrategy(2);
    Math.MinimizerOptions.SetDefaultTolerance(10)
    Math.MinimizerOptions.SetDefaultMaxFunctionCalls(100000)

    Jpsi_M = w.var("Jpsi_M")
    Jpsi_M.removeRange("fitRange_Low_Lim")
    Jpsi_M.removeRange("fitRange_High_Lim")
    Jpsi_M.setRange("fitRange", dm.minM_Low, dm.maxM_High)

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf_constr")
    combData = w.data("combData")
    if syst=="Range":
        simPdf = w.pdf("simPdf")


    globalObs = RooArgSet(w.var("extFactorObs_fromB"), w.var("extFactorObs_prompt"),w.var(f"mass_{state}_Obs"))
    globalParams = RooArgSet()
    globalParams.add(w.var("extFactor_fromB"))
    globalParams.add(w.var("extFactor_prompt"))
    globalParams.add(w.var(f"mass_{state}"))
    if state!="hc":
        globalObs.add(w.var(f"width_{state}_Obs"))
        globalParams.add(w.var(f"gamma_{state}"))

    print(w.var(f"cs_{state}_rel_{src}").getValV())
    print(w.var(f"cs_{state}_rel_{src}").getError())
    # w.var(f"n_hc_fromB").setRange(0., 5.e5)

    ### C o n s t r u c t   p l a i n   l i k e l i h o o d
    ### ---------------------------------------------------

    # var_list_0 = w.allVars()

    # nuis_iter = var_list_0.createIterator()
    # var = nuis_iter.Next()
    # while var:
    #     if "extFactorObs" not in var.GetName() and not var.isConstant():
    #         var.setConstant(True)
    #     var = nuis_iter.Next()

    # n_bkg_pr  = w.var("n_bkg_prompt"); n_bkg_pr.setConstant(False)
    # n_bkg_fb  = w.var("n_bkg_fromB"); n_bkg_fb.setConstant(False)

    poi = w.var(f"cs_{state}_rel_{src}")
    if src=="prompt":
        poi.setRange(0, 15.0)
        # poi.setRange(0, 4.0)
    else:
        poi.setRange(0, 0.2)
    poi.Print()

    llist = RooLinkedList()
    llist.Add(RooCmdArg(RooFit.Extended(True)))
    llist.Add(RooCmdArg(RooFit.Verbose(True)))
    llist.Add(RooCmdArg(RooFit.Offset(True)))
    llist.Add(RooCmdArg(RooFit.Optimize(True)))
    llist.Add(RooCmdArg(RooFit.Constrain(globalParams)))
    llist.Add(RooCmdArg(RooFit.GlobalObservables(globalObs)))
    # llist.Add(RooCmdArg(RooFit.NumCPU(20, 0)))
    llist.Add(RooCmdArg(RooFit.NumCPU(50, 0)))
    # llist.Add(RooCmdArg(RooFit.NumCPU(7, 0)))
    # llist.Add(RooCmdArg(RooFit.Range("fitRange")))

    # Construct unbinned likelihood
    nll = simPdf.createNLL(combData, llist)
    w.Print()
    print("-----------------------------------------")
    globalParams.Print()
    simPdf.Print()
    combData.Print()
    nll.Print()
    # Minimize likelihood w.r.t all parameters before making plots
    
    # m = RooMinuit(nll)
    # m.migrad()
    # m.migrad()
    # m.hesse()
    # # m.save()
    # m.minos()


    # previous = RooAbsReal.hideOffset()
    # RooAbsReal.setHideOffset(True)
    # bestVal = nll.getVal()
    # print(bestVal)
    # if (not previous):  RooAbsReal.setHideOffset(False) 
    
    # # poi_var.setVal(0.)
    # # poi_var.setConstant(True)

    # m.migrad()
    # m.save()
    # previous = RooAbsReal.hideOffset()
    # RooAbsReal.setHideOffset(True)
    # bkgVal = nll.getVal()
    # print(bkgVal)
    # if (not previous):  RooAbsReal.setHideOffset(False) 

    # qmu = 2*(bkgVal-bestVal)
    # print(qmu)
    # input()

    # # The profile likelihood estimator on nll for frac will minimize nll w.r.t
    # # all floating parameters except frac for each evaluation

    # # Plot likelihood scan frac
    print("--------------------------")
    frameLL = poi.frame(RooFit.Bins(20), RooFit.Title("LL and profileLL"))


    file_ll = TFile(f"{homeDir}/mass_fit/one_hist/{MODEL}/PLL_{src}_{syst}_{norm}_{CUTKEY}_sim_{state}{postfix}.root", "recreate")

    print("--------------------------")
    # nll.getIntegratorConfig().setEpsRel(1e-2)
    # input()
    # pll = nll.createProfile(poi)
    pll = RooProfileLL("pll", "pll", nll, RooArgSet(poi))
    pll.Print()
    m = pll.minimizer()
    # m.setPrintLevel(2)
    # m.setStrategy(1)
    # m.setEps(1e-5)
    # m.setMaxFunctionCalls(100000)
    f_pllhood = pll.asTF(RooArgList(poi))

    f_pllhood.Write()

    file_ll.Write()
    file_ll.Close()


# def estimateSyst():

    # TF1 fsum("f1","[&](double *x, double *p){ return p[0]*f1(x) + p[1]*f2(x); }",0,10,2);


def f(par_list:list):
    src = par_list[0]
    syst = par_list[1]
    norm = "jpsi"
    # return evalPLL(w,src,syst,norm)
    state="hc"
    return evalPLL(src,syst,norm,state)

if __name__ == "__main__":

    gROOT.LoadMacro("../libs/lhcbStyle.C")
    gStyle.SetStatFormat("4.3f")
    gStyle.SetOptTitle(0)

    systs = ["Base", "Exp","Gamma","eff","CB","BR","cross_talk","Resolution"]
    # systs = ["Range"]
    systs = ["Base","Gauss","Exp","Sqrt","SRatio","pppi0"]
    # systs = ["Sqrt","SRatio","pppi0"]
    # systs = ["Sqrt","Exp"]
    # systs = ["SRatio"]
    # systs = ["Base","Gauss","pppi0"]
    # systs = ["Base"]
    # srcs = ["fromB","prompt"]
    srcs = ["prompt"]
    norm = 'jpsi'
    # norm = 'chic2'
    # norm = 'etac'
    state = "chic2"
    # state = "chic1"
    # state = "chic0"
    # state = "hc"
    # state = "etac2S"
    # for kBkg in kBkgs:
    # postfix = ""
    postfix = "_noBR"
    # postfix = "_abs"
    for syst in systs:
        # w = perform_fit(syst, norm)
        # nameWksp = f"{homeDir}/mass_fit/one_hist/{MODEL}/Wksp_{norm}_sim_{syst}_{CUTKEY}_{state}{postfix}.root"
        # file_w = TFile(nameWksp)
        # w = file_w.Get("w").Clone()
        # file_w.Close()
        # plot_ll(w)
        for src in srcs:
            # calcCLs(w,src,syst,norm,state)
            # calcMCMC(w,src,syst,norm,state,postfix)
            # calcPLC(w,src,syst,norm)
            evalPLL(src,syst,norm,state,postfix)
        # del w

    # a = [[srcs[iSr], systs[iSt]] for iSr in range(len(srcs)) for iSt in range(len(systs))]

    # with closing(Pool(12)) as p:
    #     (p.map(f, a))
    #     p.terminate()
