from ROOT import *
from ROOT import TFile, gROOT
# from ROOT import RooFit, RooStats, RooMinuit
# from ROOT import RooDataHist, RooArgList, RooRealVar, \
#                     RooDataSet, RooArgSet, RooProfileLL, \
#                     RooWorkspace, RooCmdArg, RooLinkedList
# from ROOT import Math, gPad, gStyle

import drawModule_splitFit as dm
gROOT.LoadMacro("../libs/libBifurcatedCB/BifurcatedCB.cxx+")
gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../libs/lhcbStyle.C")
from ROOT import RooRelBreitWigner, BifurcatedCB

import numpy as np

#homeDir = "/afs/cern.ch/user/v/vazhovko/private/etac2s_ppbar/"
#dataDir = "/eos/user/v/vazhovko/etac2s_ppbar/"
homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

MODEL="CB"

def redraw(kNorm, kSyst, state):

    nameWksp = f"{homeDir}/mass_fit/one_hist/{MODEL}/Wksp_{kNorm}_sim_{kSyst}_ProbNNp_06_{state}.root"
    file_w = TFile(nameWksp)
    w = file_w.Get("w")

    frame_pr, resid_pr, pull_pr = dm.set_frame(w,"", kSource="prompt")
    frame_fb, resid_fb, pull_fb = dm.set_frame(w,"", kSource="fromB")

    names  = ["prompt","fromB"]
    frames = [frame_pr,frame_fb]
    resids = [resid_pr,resid_fb]
    pulls  = [pull_pr,pull_fb]
    c = dm.set_canvas(names, frames, resids, pulls)

    kSource = "sim"
    namePic = f"{homeDir}/mass_fit/one_hist/{MODEL}/Plot_{kNorm}_{kSource}_{kSyst}_ProbNNp_06_{state}_upd.pdf"


    c.SaveAs(namePic)


if __name__ == "__main__":

    gROOT.LoadMacro("../libs/lhcbStyle.C")
    kSysts = ["Exp","Gamma","eff","CB","BR","cross_talk","Resolution"]
    kSysts = ["Base"]
    kSources = ["fromB","prompt"]
    # kSources = ["prompt"]
    kNorm = 'jpsi'
    state = "etac2S"
    for kSyst in kSysts:
        redraw(kNorm, kSyst, state)