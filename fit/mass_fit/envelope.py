# from ROOT import *
from ROOT import gROOT, gPad, gStyle, kOcean
from ROOT import TFile, TGraph, TMath, TCanvas, TLegend, TLine, TLatex, TPad
lTGraph = list[TGraph]
pair = [float, float]

# homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/"
MODEL = "CB"
# MODEL = "Gauss"

systDict = {
  # "Base": "Mass resolution model",
  "Base":   "Baseline fit", 
  # "CB":     "Baseline fit", 
  "Gauss":  "Mass resolution model",
  "Sqrt":   "Comb. bkg. descr. 1",
  "Exp":    "Comb. bkg. descr. 2",
  "SRatio": "Resolution ratio",
  "pppi0":  "p\\bar{p}\pi^{0} contr."}

def addBlurb(valUL, isdata=True, year = 0):

   
  myLatex = TLatex(0.5,0.5,"")
  myLatex.SetTextFont(132) 
  myLatex.SetTextColor(1) 
  myLatex.SetTextSize(0.06) 
  myLatex.SetNDC(True) 
  myLatex.SetTextAlign(11)  
  myLatex.SetTextSize(0.055) 
  if (isdata):
    if (year ==0):
      myLatex.DrawLatex(0.63, 0.77,"#splitline{LHCb Preliminary}{#scale[1]{#sqrt{s} = 7+8 TeV, L =3 fb^{#kern[0.7]{-1}}}}")
    elif (year == 2011):
      myLatex.DrawLatex(0.63, 0.77,"#splitline{LHCb Preliminary}{#scale[1]{#sqrt{s} = 7 TeV, L =1 fb^{#kern[0.7]{-1}}}}")
    elif (year == 2012):
      myLatex.DrawLatex(0.63, 0.77,"#splitline{LHCb Preliminary}{#scale[1]{#sqrt{s} = 8 TeV, L =2 fb^{#kern[0.7]{-1}}}}")
  else:
    myLatex.DrawLatex(0.67, 0.95,"LHCb Simulation")

  if valUL!="":
    myLatex.DrawLatex(0.58, 0.47,f"UL @95% CL = {valUL:.3f}")


def plotGraph(graph: TGraph,  xTitle:str="etac2S", yTitle:str="Likelihood", marker:int = 1, color:int = 2, style:int = 2 ):

  gPad.SetLeftMargin(0.20)
  # graph.SetMarkerStyle(marker)
  # graph.SetMarkerColor(color)
  # graph.SetLineColor(color)
  # graph.SetLineStyle(style)
  # graph.SetLineWidth(1)
  # #  graph.SetMinimum(0.0)
  graph.SetTitle("")
  # graph.GetXaxis().SetTitleSize(0.05)
  # graph.GetXaxis().SetTitleFont(132)
  # graph.GetYaxis().SetTitleFont(132)
  graph.GetXaxis().SetTitleOffset(0.9)
  # graph.GetYaxis().SetTitleOffset(0.9)
  # graph.GetXaxis().SetLabelFont(132)
  # graph.GetYaxis().SetLabelFont(132)
  graph.GetYaxis().SetTitle(yTitle)
  # graph.GetXaxis().SetTitle("m(p #bar{p}) [MeV/#font[12]{c}^{#kern[0.5]{2}}]")
  if xTitle=="etac2S_abs":
    graph.GetXaxis().SetTitle("#sigma_{#eta_{c}(2S)} #times BR_{#eta_{c}(2S)#rightarrow p#bar{p}} [nb]")
    # graph.GetXaxis().SetTitleOffset(1.05)
  elif xTitle=="etac2S":
    # graph.GetXaxis().SetTitle("#frac{#sigma_{#eta_{c}(2S)} #times BR(#eta_{c}(2S)#rightarrow p#bar{p})}{#sigma_{J/#psi} #times BR(J/#psi#rightarrow p#bar{p})}")
    graph.GetXaxis().SetTitle("#sigma_{#eta_{c}(2S)} #times BR_{#eta_{c}(2S)#rightarrow p#bar{p}}/#sigma_{J/#psi} #times BR_{J/#psi#rightarrow p#bar{p}}")
    # graph.GetXaxis().SetTitle("#sigma_{#eta_{c}(2S)} #times BR_{#eta_{c}(2S)#rightarrow p#bar{p}}/#sigma_{#eta_{c}} #times BR_{#eta_{c}#rightarrow p#bar{p}}")
    # graph.GetXaxis().SetTitleOffset(0.90)
  elif xTitle=="hc_abs":
    graph.GetXaxis().SetTitle("#sigma_{h_{c}} #times BR_{h_{c}#rightarrow p#bar{p}} [nb]")
    # graph.GetXaxis().SetTitleOffset(1.05)
  else:
    # graph.GetXaxis().SetTitle("#frac{#sigma_{h_{c}} #times BR(h_{c}#rightarrow p#bar{p})}{#sigma_{J/#psi} #times BR(J/#psi#rightarrow p#bar{p})}")
    graph.GetXaxis().SetTitle("#sigma_{h_{c}} #times BR_{h_{c}#rightarrow p#bar{p}}/#sigma_{J/#psi} #times BR_{J/#psi#rightarrow p#bar{p}}")
    # graph.GetXaxis().SetTitle("#sigma_{h_{c}} #times BR_{h_{c}#rightarrow p#bar{p}}/#sigma_{#eta_{c}} #times BR_{#eta_{c}#rightarrow p#bar{p}}")
    # graph.GetXaxis().SetTitleOffset(0.90)
  #graph.GetYaxis().SetTitle("")

#int cols[2] = {2,4}

def addToVector(name: str, file: TFile, vec: lTGraph, i: int):
  
  graph = file.Get(name)
  if (graph):
    plotGraph(graph, "Likelihood", 1,2, 2)
    vec.append(graph)
  else:
    print("Failed To find graph")

def findMinVal(vec: lTGraph, x: float):
  minim = TMath.Max(vec[0].Eval(x), -3400.) 
  for iv in range(len(vec)):
    if (vec[iv].Eval(x)  < minim and vec[iv].Eval(x) > -3400. ): minim = vec[iv].Eval(x)
  return TMath.Exp(-0.5*minim)

def findLimit(graph: TGraph, low: float, high: float, cls=0.95) -> float:

  npoints = graph.GetN()
  step = (high-low)/float(npoints)
  ul = 0.
  # i_tot = graph.Integral(0, -1);
  i_tot = 0.
  for i in range(npoints):
    i_tot += 0.5*step*(graph.GetPointY(i)+graph.GetPointY(i+1))

  i=1
  i_lim = 0.5*step*(graph.GetPointY(0)+graph.GetPointY(i))
  while i_lim/i_tot < cls:
    i_lim += 0.5*step*(graph.GetPointY(i)+graph.GetPointY(i+1))
    # print(graph.GetPointX(i),graph.GetPointY(i),i_lim/i_tot, i_tot)
    ul = i*step
    i+=1

  print("UL: {}".format(ul))
  return ul

def minimizeG(graph : TGraph, low: float, high: float) -> pair : 

  # step thrugh and get min
  # nsteps = 20000
  nsteps = 1000
  step = (high - low)/float(nsteps) 
  i = 0
  minx  = low
  minim = 10000. 
  for i in range(nsteps):
    val = graph.Eval(low + i*step,0,"S")
    # std.cout << low + i*step << " " << val << std.endl
    if (val < minim):
      minim = val
      minx = low + i*step


  print(minim," ",minx)

  # now we can get error DLL goes up by 1
  i = 0
  while (graph.Eval(minx + i*step, 0,"S") < (0.5+ minim) ):
    i+=1

  #  float uperr = graph.Eval(minx + i*step, 0,"S") -minx
  #/ std.cout  << 
  uperr = minx + i*step

  i = 0
  while (graph.Eval(minx - i*step, 0,"S") < (0.5+ minim)):
    i+=1
  #  float downerr = minx - graph.Eval(minx - i*step, 0,"S")
  downerr = minx - i*step

  print(downerr, " ", uperr)
  return([minx, 0.5*(uperr-downerr)/1.])

def minimizeF(graph : TGraph, low: float, high: float) -> pair : 

  # step thrugh and get min
  # nsteps = 20000
  minim = graph.GetMinimum(low, high)
  minx  = graph.GetMinimumX(low, high)

  uperr = graph.GetX(minim+0.5)
  downerr = graph.GetX(minim-0.5)

  if downerr<1.e-6:
    downerr=0

  print(downerr, " ", uperr)
  return([minx, 0.5*(uperr-downerr)/1.])

#const int nf = 2

# def shiftGraph(gr: TGraph, min: float , max: float) -> TGraph:
#   res = minimize(gr,min, max) #pair[float,float]
#   themin = gr.Eval(res[0],0,"S")
#   s = TGraph(gr.GetN())
#   for i in range(gr.GetN()):
#     s.SetPoint(i,gr.GetX()[i], gr.GetY()[i] - themin)
#   return s

def shiftGraph(gr: TGraph, themin: float) -> TGraph:

  s = TGraph(gr.GetN())
  for i in range(gr.GetN()):
    s.SetPoint(i,gr.GetX()[i], gr.GetY()[i] - themin)
  return s


def addSingle( minx = 10355.04, maxx = 10355.26, state:str="etac2S", postfix:str="") -> TGraph:

  # gStyle.SetPalette(kOcean)

  kSource = "prompt"
  kNorm = "jpsi"
  # kNorm = "chic2"
  # kNorm = "etac"
  grname = "combi"
  
  c2 = TCanvas("c2", "c2",55,55,550,400)
  c2.SetBottomMargin(0.15)

  graphs = [] #std.vector<TGraph*> 
  l = TLegend(0.55, 0.55, 0.90, 0.90)

  # models = ["CB", "Base", "Sqrt", "Exp","SRatio","pppi0"]
  models = ["Base","Gauss","Sqrt","Exp","SRatio","pppi0"]
  models = ["Base"]
  for i, m in enumerate(models):
      file_ll = TFile(f"{homeDir}/mass_fit/one_hist/{MODEL}/PLL_{kSource}_{m}_{kNorm}_ProbNNp_06_sim_{state}{postfix}.root", "read")
      print(f"Adding {m}")
      # graph = TGraph(file_ll.Get("pll").Clone())
      graph = file_ll.Get("pll").Clone()
      graph.SetLineColor(2)
      graph.SetLineStyle(2+i)
      graph.SetLineWidth(2)
      graphs.append(graph)
      l.AddEntry(graph, systDict[m], "lr")
      file_ll.Close()

  # sigmodels = ["DCB","TWOCB","GCB"]
  # bmodels = ["exp","cheb", "powexp", "pow"]
  # for b in bmodels:
  #   for s in sigmodels:
  #     name = "mgraph_" + s + "_" + b
  #     print("Adding ", name)
  #     graph = file.Get(name())
  #     graph.SetLineColor(2)
  #     graph.SetLineStyle(2)
  #     graphs.append(graph)
   
  # combine them 
  npoints = 10000
  step = (maxx-minx)/float(npoints)

  combi = TGraph(npoints)
  for i in range(npoints):
    x = minx+i*step
    combi.SetPoint(i, x, findMinVal(graphs,x))

  ul = findLimit(combi, minx, maxx)
  print("made combi")
  
  plotGraph(combi, state+postfix, "Likelihood", 1,1,1)
  combi.SetLineWidth(2)
  combi.Draw("AL")
  col = 0
  for i, g in enumerate(graphs):
    if i==4: col=800
    else: col=i+1
    g.SetLineColor(col)
    g.Draw("same")

  # return combi
  #graphs[0].Draw("L")
  #graphs[1].Draw("L")
  #combi.Draw("L")
  combi.GetXaxis().SetRangeUser(minx, 0.75*maxx)
  combi.SetName(grname)
  l.Draw()
  
  # p = TPad("p", "p", 0, 0, 0.2, 0.2)
  # p.cd()
  # combi.GetXaxis().SetRangeUser(0., 0.2)
  # combi.DrawClone("AL")
  # col = 0
  # for i, g in enumerate(graphs):
  #   if i==4: col=800
  #   else: col=i+1
  #   g.SetLineColor(col)
  #   g.DrawClone("same")

  # res = minimizeG(combi,minx, maxx) #pair[float,float]
  # print("Min combi {:.10f} {:.10f}".format(res[0], res[1]))
  # themin = combi.Eval(res[0],0,"S")
  # combi2 = shiftGraph(combi,themin)
  # combi2.Draw()

  # res2 = minimizeF(graphs[0], minx, maxx) #pair[float,float]
  # print("Min combi {:.10f} {:.10f}".format(res2[0], res2[1]))
  
  # plotGraph(combi2, "Likelihood", 1,1,1)
  # combi2.SetMaximum(4)
  # combi2.SetMinimum(0)
  # combi2.Draw("AC same")
  # # col = 1
  # # for g in graphs:
  # #   # sg = shiftGraph(g,themin)
  # #   g.SetLineColor(col)
  # #   g.SetLineStyle(2)
  # #   g.Draw("C") 
    
  # # /*TGraph* sg = shiftGraph(graphs[7],themin)
  # # sg.SetLineColor(2)
  # # sg.SetLineStyle(2)
  # # sg.Draw("L") */
  # # combi2.Draw("C same")

  line = TLine(ul,0, ul, 1.1)
  line.SetLineColor(2)
  # line.SetLineStyle(2)
  line.Draw()
  addBlurb(ul, year=2018)
  if len(models)==1:
    c2.SaveAs(f"scans/scan_{kNorm}_{state}{postfix}_{models[0]}.pdf")
  else:
    c2.SaveAs(f"scans/scan_{kNorm}_{state}{postfix}.pdf")

  return combi

if __name__ == "__main__":

  gROOT.LoadMacro("../libs/lhcbStyle.C")
  # addSingle(0, 0.4, state="etac2S")
  # addSingle(0, 0.4, state="hc")
  # addSingle(0, 15.0, state="chic0",postfix="_noBR")
  # addSingle(0, 10.0, state="chic1",postfix="_noBR")
  addSingle(0, 6.0, state="chic0",postfix="_noBR")
  addSingle(0, 2.5, state="chic1",postfix="_noBR")
  addSingle(0, 15.0, state="chic2",postfix="_noBR")
  # addSingle(0, 1.5, state="chic0",postfix="_abs")
  # addSingle(0, 1.0, state="chic1",postfix="_abs")
  # addSingle(0, 1.0, state="chic2",postfix="_abs")
  # addSingle(0, 0.4, state="chic0")
  # addSingle(0, 0.4, state="chic1")
  # addSingle(0, 0.4, state="chic2")
  # addSingle(0, 1.0, state="etac2S",postfix="_abs")
  # addSingle(0, 1.0, state="hc",postfix="_abs")