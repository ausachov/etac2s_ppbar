#from ROOT import TFile, TChain, TTree, TH1D, TGraph, TCanvas, TCut, TMath
from ROOT import *
from ROOT.RooFit import *
gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")

import oniaConstLib as cl
import drawModule as dm
import numpy as np
#from math import abs
from drawModule import minM_Low, maxM_Low, minM_High, maxM_High, binWidth

# minM_Low = 2850
# maxM_Low = 3250

# minM_High = 3250
# maxM_High = 3780


# binWidth = 0.25
# binWidthDraw = 5
binN_Jpsi   = int((maxM_Low - minM_Low)/binWidth)
binN_etac2S = int((maxM_High - minM_High)/binWidth)
binN_tot    = int((maxM_High - minM_Low)/binWidth)


binning = RooFit.Binning(binN_tot, minM_Low, maxM_High)
binning_Jpsi = RooFit.Binning(binN_Jpsi, minM_Low, maxM_Low)
binning_etac2s = RooFit.Binning(binN_etac2S, minM_High, maxM_High)
mrkSize = RooFit.MarkerStyle(7)
lineWidth1 = RooFit.LineWidth(1)
lineWidth2 = RooFit.LineWidth(2)
lineStyle1 = RooFit.LineStyle(2)
lineStyle2 = RooFit.LineStyle(9)
lineColor1 = RooFit.LineColor(6)
lineColor2 = RooFit.LineColor(8)
name = RooFit.Name
cut = RooFit.Cut

#homeDir = "/afs/cern.ch/user/v/vazhovko/private/results/etac2s_ppbar/"
#dataDir = "/eos/user/v/vazhovko/etac2s_ppbar/"
homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

# def get_data_h(w, kSource):

#     dirName_Low  = dataDir +"Data_Low_NoPID/"
#     dirName_High = dataDir +"Data_High_NoPID/"


#     nt_Low  =  TChain("DecayTree")
#     nt_High =  TChain("DecayTree")

#     nt_Low.Add(dirName_Low+"/Etac2sDiProton_Low_2018*.root")
#     nt_High.Add(dirName_High+"/Etac2sDiProton_High_2018*.root")

#     #tree_Low = TTree()
#     #tree_High = TTree()

#     cut_PT      = TCut("Jpsi_PT > 6500")
#     cut_FD = ""
#     if kSource=="prompt":
#         cut_FD = TCut("(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08")# & Jpsi_FDCHI2_OWNPV > 49""
#     else:
#         cut_FD = TCut("(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16")# & Jpsi_FDCHI2_OWNPV > 49""

#     cut_PT      = TCut("Jpsi_PT > 5000 && Jpsi_PT < 20000")
#     cut_PID     = TCut("ProtonP_ProbNNk < 0.6 && ProtonM_ProbNNk < 0.6 && ProtonP_ProbNNp > 0.6 && ProtonM_ProbNNp > 0.6 && ProtonP_ProbNNpi < 0.6 && ProtonM_ProbNNpi < 0.6 ")


#     totCut = TCut(cut_FD + cut_PT)


#     hh_Low = TH1D("hh_Low","hh_Low", 3720, minM_Low, maxM_High)
#     hh_High = TH1D("hh_High","hh_High", 3720, minM_Low, maxM_High)

#     nt_Low.Draw("Jpsi_m_scaled>>hh_Low",totCut.GetTitle(),"goff")
#     nt_High.Draw("Jpsi_m_scaled>>hh_High",totCut.GetTitle(),"goff")

#     Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M", minM_Low, maxM_High)

#     dh_Low = RooDataHist("dh_Low_{}".format(kSource),"dh_Low_{}".format(kSource), RooArgList(Jpsi_M), hh_Low)
#     dh_High = RooDataHist("dh_High_{}".format(kSource),"dh_High_{}".format(kSource), RooArgList(Jpsi_M), hh_High)
#     getattr(w,"import")(dh_Low)
#     getattr(w,"import")(dh_High)

#     print "DATA\"S READ SUCCESSFULLY"

def get_data_h(w, kSource):

    nameWksp = dataDir + "/wksps/wksp_data_{}_ProbNNp_06_wide.root".format(kSource)
    # nameWksp = dataDir + "/wksps/wksp_data_{}_ProbNNp_06.root".format(kSource)
    file_w = TFile(nameWksp,"read")
    w_in = file_w.Get("w").Clone()
    file_w.Close()

    dataL   = w_in.data("dh_Low")
    dataH   = w_in.data("dh_High")
    Jpsi_M = w_in.var("Jpsi_M")

    histL = dataL.createHistogram("histL",Jpsi_M)
    histH = dataH.createHistogram("histH",Jpsi_M)

    histL.Print()
    histH.Print()

    Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M", minM_Low, maxM_High)

    dh_Low  = RooDataHist("dh_Low_{}".format(kSource),"dh",  RooArgList(Jpsi_M), histL)
    dh_High = RooDataHist("dh_High_{}".format(kSource),"dh",  RooArgList(Jpsi_M), histH)

    # getattr(w,"import")(Jpsi_M)
    getattr(w,"import")(dh_Low)
    getattr(w,"import")(dh_High)

    print("DATAHISTS READ SUCCESSFULLY")

def yields_connect(w, norm="Jpsi"):

    #eps_pp = 0.964
    #eps_pb = 0.0007
    #eps_bp = 0.064
    #eps_bb = 0.692

    eps_pp = 1.0
    eps_pb = 0.0
    eps_bp = 0.0
    eps_bb = 1.0


    N_etac2sRel_p = RooRealVar("N_etac2sRel_prompt","num of etac2s true", 0.01, 0., 0.15)
    N_etac2sRel_b = RooRealVar("N_etac2sRel_fromB","num of etac2s true", 0.01, 0., 0.15)
    N_Jpsi_p = RooRealVar("N_Jpsi_prompt","num of Jpsi true", 2e3, 10, 1.e6)
    N_Jpsi_b = RooRealVar("N_Jpsi_fromB","num of Jpsi true", 2e3, 10, 1.e6)
    N_etac_p = RooRealVar("N_etac_prompt","num of Jpsi true", 2e3, 10, 1.e6)
    N_etac_b = RooRealVar("N_etac_fromB","num of Jpsi true", 2e3, 10, 1.e6)


    n_Jpsi_p = RooFormulaVar("n_Jpsi_prompt","num of Jpsi","{}*@0 + {}*@1".format(eps_pp,eps_bp), RooArgList(N_Jpsi_p,N_Jpsi_b))
    n_Jpsi_b = RooFormulaVar("n_Jpsi_fromB","num of Jpsi","{}*@0 + {}*@1".format(eps_pb,eps_bb), RooArgList(N_Jpsi_p,N_Jpsi_b))

    n_etac_p = RooFormulaVar("n_etac_prompt","num of Jpsi","{}*@0 + {}*@1".format(eps_pp,eps_bp), RooArgList(N_etac_p,N_etac_b))
    n_etac_b = RooFormulaVar("n_etac_fromB","num of Jpsi","{}*@0 + {}*@1".format(eps_pb,eps_bb), RooArgList(N_etac_p,N_etac_b))

    if norm=="Jpsi":

        n_etac2s_p = RooFormulaVar("n_etac2s_prompt","num of etac2s","{}*@0*@1 + {}*@1*@2".format(eps_pp,eps_bp), RooArgList(N_Jpsi_p,N_etac2sRel_p,N_Jpsi_b,N_etac2sRel_b))
        n_etac2s_b = RooFormulaVar("n_etac2s_fromB","num of etac2s","{}*@0*@1 + {}*@1*@2".format(eps_pb,eps_bb), RooArgList(N_Jpsi_p,N_etac2sRel_p,N_Jpsi_b,N_etac2sRel_b))
        N_etac2s_p = RooFormulaVar("N_etac2s_prompt","num of etac2s","@0*@1", RooArgList(N_Jpsi_p,N_etac2sRel_p))
        N_etac2s_b = RooFormulaVar("N_etac2s_fromB","num of etac2s","@0*@1", RooArgList(N_Jpsi_b,N_etac2sRel_b))

    else:

        n_etac2s_p = RooFormulaVar("n_etac2s_prompt","num of etac2s","{}*@0*@1 + {}*@1*@2".format(eps_pp,eps_bp), RooArgList(N_etac_p,N_etac2sRel_p,N_etac_b,N_etac2sRel_b))
        n_etac2s_b = RooFormulaVar("n_etac2s_fromB","num of etac2s","{}*@0*@1 + {}*@1*@2".format(eps_pb,eps_bb), RooArgList(N_etac_p,N_etac2sRel_p,N_etac_b,N_etac2sRel_b))
        N_etac2s_p = RooFormulaVar("N_etac2s_prompt","num of etac2s","*@0*@1", RooArgList(N_etac_p,N_etac2sRel_p))
        N_etac2s_b = RooFormulaVar("N_etac2s_fromB","num of etac2s", "*@0*@1", RooArgList(N_etac_b,N_etac2sRel_b))


    trueVar = RooArgSet(N_etac2s_p,N_etac2s_b,N_etac2sRel_p,N_etac2sRel_b,N_Jpsi_p,N_Jpsi_b,N_etac_p,N_etac_b)
    fitVar = RooArgSet(n_etac2s_p,n_etac2s_b,n_Jpsi_p,n_Jpsi_b,n_etac_p,n_etac_b)

    #w.saveSnapshot("trueVar",trueVar,True)
    #w.saveSnapshot("fitVar",fitVar,True)
    getattr(w,"import")(trueVar)
    getattr(w,"import")(fitVar)
    #getattr(w,"import")(n_etac2s_p)
    #getattr(w,"import")(n_etac2s_b)
    #getattr(w,"import")(n_Jpsi_p)
    #getattr(w,"import")(n_Jpsi_b)



def fillRelWorkspace(w,kSource):

    Jpsi_M = w.var("Jpsi_M")
    #Jpsi_M.setBins(3720,"cache")

    ratioEtac2sToJpsi = (cl.M_ETAC2S/cl.M_JPSI)**0.5

    rEtac2sToJpsi = RooRealVar("rEtac2sToJpsi","rEtac2sToJpsi", ratioEtac2sToJpsi)#, 0.01, 5.0)

    trueVar = w.loadSnapshot("trueVar")
    fitVar = w.loadSnapshot("fitVar")


    # n_Jpsi = w.function("n_Jpsi_{}".format(kSource))
    # n_etac = w.function("n_etac_{}".format(kSource))
    # n_etac2s = w.function("n_etac2s_{}".format(kSource))
    n_Jpsi = w.function("N_Jpsi_{}".format(kSource))
    n_etac = w.function("N_etac_{}".format(kSource))
    n_etac2s = w.function("N_etac2s_{}".format(kSource))
    #n_Jpsi = RooRealVar("n_Jpsi_{}".format(kSource),"num of J/Psi", 2e3, 10, 1.e6)
    #n_etac2sRel = RooRealVar("n_etac2sRel_{}".format(kSource),"num of Etac", 0.1, 1e-3, 3.0)
    ##n_etac2s = RooRealVar("n_etac2s","num of etac2s", 1e3, 10, 1.e4)
    #n_etac2s = RooFormulaVar("n_etac2s_{}".format(kSource),"num of Etac","@0*@1", RooArgList(n_etac2sRel,n_Jpsi))



    #n_etac = RooRealVar("n_etac_{}".format(kSource),"num of etac", 1e3, 10, 1.e5)
    n_chic0 = RooRealVar("n_chic0_{}".format(kSource),"num of chic0", 1e3, 0, 1.e4)
    n_chic1 = RooRealVar("n_chic1_{}".format(kSource),"num of chic1", 1e2, 0, 1.e4)
    n_chic2 = RooRealVar("n_chic2_{}".format(kSource),"num of chic2", 1e2, 0, 1.e4)
    n_hc = RooRealVar("n_hc_{}".format(kSource),"num of hc", 1e3, 0, 1.e4)
    n_Psi2s = RooRealVar("n_Psi2s_{}".format(kSource),"num of Psi2s", 1e2, 0, 1.e4)
    n_Psi3770 = RooRealVar("n_Psi3770_{}".format(kSource),"num of Psi3770", 5e1, 0, 1.e4)
    #n_Jpsi.setVal(0.); n_Jpsi.setConstant()
    #n_etac.setVal(0.); n_etac.setConstant()
    #n_etac2s.setVal(0.); n_etac2s.setConstant()
    #n_chic0.setVal(0.); n_chic0.setConstant()
    #n_chic1.setVal(0.); n_chic1.setConstant()
    #n_chic2.setVal(0.); n_chic2.setConstant()
    #n_hc.setVal(0.); n_hc.setConstant()
    #n_Psi2s.setVal(0.); n_Psi2s.setConstant()
    n_Psi3770.setVal(0.); n_Psi3770.setConstant()


    nBckgr_High = RooRealVar("nBckgr_High_{}".format(kSource),"num of bckgr",7e7,1e4,1.e+9)
    nBckgr_Low = RooRealVar("nBckgr_Low_{}".format(kSource),"num of bckgr",7e7,1e4,1.e+9)


    #Psi

    mass_Jpsi = RooRealVar("mass_Jpsi","mean of gaussian",cl.M_JPSI, 3030, 3150)
    mass_Jpsi.setConstant()

    #mass_Psi2s = RooRealVar("mass_Psi2s","mean of gaussian",m_Psi2s, 3680, 3692)
    #mass_Psi2s.setConstant()
    mass_res_Psi2s = RooRealVar("mass_res_Psi2s","mean of gaussian", cl.M_RES_L_PSI2S)
    mass_Psi2s     = RooFormulaVar("mass_Psi2s","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_Psi2s))

    mass_Psi3770 = RooRealVar("mass_Psi3770","mean of gaussian",cl.M_PSI3770)
    mass_Psi3770.setConstant()
    gamma_Psi3770 = RooRealVar("gamma_Psi3770","width of Br-W", cl.GAMMA_PSI3770)
    spin_Psi3770 = RooRealVar("spin_Psi3770","spin_Psi3770", 1. )


    #eta_c

    #mass_etac = RooRealVar("mass_etac","mean of gaussian", m_etac, 2980, 2988)
    #mass_etac.setConstant()
    mass_res_etac = RooRealVar("mass_res_etac","mean of gaussian",cl.M_RES_ETAC, 100, 125)
    mass_res_etac.setConstant()
    mass_etac     = RooFormulaVar("mass_etac","mean of gaussian","@0-@1",RooArgList(mass_Jpsi,mass_res_etac))
    gamma_etac    = RooRealVar("gamma_etac","width of Br-W", cl.GAMMA_ETAC)
    spin_etac     = RooRealVar("spin_etac","spin_eta", 0. )

    radius = RooRealVar("radius","radius", 1.)
    proton_m = RooRealVar("proton_m","proton mass", 938.3 )

    #mass_etac2s = RooRealVar("mass_etac2s","mean of gaussian", m_etac2s)
    #mass_etac2s.setConstant()
    mass_res_etac2s = RooRealVar("mass_res_etac2s","mean of gaussian", cl.M_RES_L_ETAC2S, 530, 560)
    mass_res_etac2s.setConstant()
    mass_etac2s     = RooFormulaVar("mass_etac2s","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_etac2s))
    gamma_etac2s = RooRealVar("gamma_etac2s","width of Br-W", cl.GAMMA_ETAC2S)
    spin_etac2s = RooRealVar("spin_etac2s","spin_eta", 0. )


    #chi_c

    #mass_chic0 = RooRealVar("mass_chic0","mean of gaussian", m_chic0)
    #mass_chic0.setConstant()
    mass_res_chic0 = RooRealVar("mass_res_chic0","mean of gaussian", cl.M_RES_L_CHIC0)
    mass_chic0     = RooFormulaVar("mass_chic0","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_chic0))
    gamma_chic0 = RooRealVar("gamma_chic0","width of Br-W", cl.GAMMA_CHIC0)
    spin_chic0 = RooRealVar("spin_chic0","spin_chic0", 0. )

    #mass_chic1 = RooRealVar("mass_chic1","mean of gaussian", m_chic1)
    #mass_chic1.setConstant()
    mass_res_chic1 = RooRealVar("mass_res_chic1","mean of gaussian", cl.M_RES_L_CHIC1)
    mass_chic1     = RooFormulaVar("mass_chic1","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_chic1))
    gamma_chic1 = RooRealVar("gamma_chic1","width of Br-W", cl.GAMMA_CHIC1)
    spin_chic1 = RooRealVar("spin_chic1","spin_chic1", 1. )

    #mass_chic2 = RooRealVar("mass_chic2","mean of gaussian", m_chic2)
    #mass_chic2.setConstant()
    mass_res_chic2 = RooRealVar("mass_res_chic2","mean of gaussian", cl.M_RES_L_CHIC2)
    mass_chic2     = RooFormulaVar("mass_chic2","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_chic2))
    gamma_chic2 = RooRealVar("gamma_chic2","width of Br-W", cl.GAMMA_CHIC2)
    spin_chic2 = RooRealVar("spin_chic2","spin_chic2", 2. )

    #h_c

    #mass_hc = RooRealVar("mass_hc","mean of gaussian", m_hc)
    #mass_hc.setConstant()
    mass_res_hc = RooRealVar("mass_res_hc","mean of gaussian", cl.M_RES_L_HC)
    mass_hc  = RooFormulaVar("mass_hc","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_hc))
    gamma_hc = RooRealVar("gamma_hc","width of Br-W", cl.GAMMA_HC)
    spin_hc = RooRealVar("spin_hc","spin_hc", 1. )



    sigma_Jpsi = RooRealVar("sigma_Jpsi","width of gaussian", 9., 0.1, 50.)
    #sigma_etac2s = RooRealVar("sigma_etac2s","width of gaussian", 9., 0.1, 50.)
    #sigma_etac2s = RooFormulaVar("sigma_etac2s","width of gaussian","@0/@1", RooArgList(sigma_Jpsi,rEtac2sToJpsi))
    sigma_etac = RooFormulaVar("sigma_etac","width of gaussian","@0*({}/{})**0.5".format(cl.M_ETAC,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_etac2s = RooFormulaVar("sigma_etac2s","width of gaussian","@0*({}/{})**0.5".format(cl.M_ETAC2S,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_chic0 = RooFormulaVar("sigma_chic0","width of gaussian","@0*({}/{})**0.5".format(cl.M_CHIC0,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_chic1 = RooFormulaVar("sigma_chic1","width of gaussian","@0*({}/{})**0.5".format(cl.M_CHIC1,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_chic2 = RooFormulaVar("sigma_chic2","width of gaussian","@0*({}/{})**0.5".format(cl.M_CHIC2,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_hc = RooFormulaVar("sigma_hc","width of gaussian","@0*({}/{})**0.5".format(cl.M_HC,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_Psi2s = RooFormulaVar("sigma_Psi2s","width of gaussian","@0*({}/{})**0.5".format(cl.M_PSI2S,cl.M_JPSI), RooArgList(sigma_Jpsi))
    #sigma_Psi2s = RooRealVar("sigma_Psi2s","width of gaussian", 9., 0.1, 30.)
    #sigma_etac2s = RooFormulaVar("sigma_etac2s","width of gaussian","@0*({}/{})**0.5".format(cl.M_ETAC2S,cl.M_PSI2S), RooArgList(sigma_Psi2s))
    sigma_Psi3770 = RooFormulaVar("sigma_Psi3770","width of gaussian","@0*({}/{})**0.5".format(cl.M_PSI3770,cl.M_JPSI), RooArgList(sigma_Jpsi))


    #Fit signal

    gauss = RooGaussian("gauss","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_Jpsi)

    #eta_c
    gauss_etac = RooGaussian("gauss_etac","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_etac)
    br_wigner_etac = RooRelBreitWigner("br_wigner_etac", "br_wigner", Jpsi_M, mass_etac, gamma_etac, spin_etac, radius, proton_m, proton_m)
    bwxg_etac = RooFFTConvPdf("bwxg_etac","breit-wigner (X) gauss", Jpsi_M, br_wigner_etac, gauss_etac)

    gauss_etac2s = RooGaussian("gauss_etac2s","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_etac2s)
    br_wigner_etac2s = RooRelBreitWigner("br_wigner_etac2s", "br_wigner", Jpsi_M, mass_etac2s, gamma_etac2s, spin_etac2s, radius, proton_m, proton_m)
    bwxg_etac2s = RooFFTConvPdf("bwxg_etac2s","breit-wigner (X) gauss", Jpsi_M, br_wigner_etac2s, gauss_etac2s)
    #bwxg = RooVoigtian("bwxg", "bwxg", Jpsi_M, mass_etac2s, gamma_etac2s, sigma_etac2s)

    #chi_c
    gauss_chic0 = RooGaussian("gauss_chic0","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_chic0)
    br_wigner_chic0 = RooRelBreitWigner("br_wigner_chic0", "br_wigner", Jpsi_M, mass_chic0, gamma_chic0, spin_chic0, radius, proton_m, proton_m)
    bwxg_chic0 = RooFFTConvPdf("bwxg_chic0","breit-wigner (X) gauss", Jpsi_M, br_wigner_chic0, gauss_chic0)

    gauss_chic1 = RooGaussian("gauss_chic1","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_chic1)
    br_wigner_chic1 = RooRelBreitWigner("br_wigner_chic1", "br_wigner", Jpsi_M, mass_chic1, gamma_chic1, spin_chic1, radius, proton_m, proton_m)
    bwxg_chic1 = RooFFTConvPdf("bwxg_chic1","breit-wigner (X) gauss", Jpsi_M, br_wigner_chic1, gauss_chic1)

    gauss_chic2 = RooGaussian("gauss_chic2","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_chic2)
    br_wigner_chic2 = RooRelBreitWigner("br_wigner_chic2", "br_wigner", Jpsi_M, mass_chic2, gamma_chic2, spin_chic2, radius, proton_m, proton_m)
    bwxg_chic2 = RooFFTConvPdf("bwxg_chic2","breit-wigner (X) gauss", Jpsi_M, br_wigner_chic2, gauss_chic2)

    #h_c
    gauss_hc = RooGaussian("gauss_hc","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_hc)
    br_wigner_hc = RooRelBreitWigner("br_wigner_hc", "br_wigner", Jpsi_M, mass_hc, gamma_hc, spin_hc, radius, proton_m, proton_m)
    bwxg_hc = RooFFTConvPdf("bwxg_hc","breit-wigner (X) gauss", Jpsi_M, br_wigner_hc, gauss_hc)


    #Fit J/psi
    gauss_Jpsi = RooGaussian("gauss_Jpsi","gaussian PDF", Jpsi_M, mass_Jpsi, sigma_Jpsi)

    gauss_Psi2s = RooGaussian("gauss_Psi2s","gaussian PDF", Jpsi_M, mass_Psi2s, sigma_Psi2s)

    #br_wigner_Psi3770 = RooRelBreitWigner("br_wigner_Psi3770", "br_wigner", Jpsi_M, mass_Psi3770, gamma_Psi3770, spin_Psi3770, radius, proton_m, proton_m)
    #bwxg_Psi3770 = RooFFTConvPdf("bwxg_Psi3770","breit-wigner (X) gauss", Jpsi_M, br_wigner_Psi3770, gauss)
    bwxg_Psi3770 = RooVoigtian("bwxg_Psi3770", "bwxg_Psi3770", Jpsi_M, mass_Psi3770, gamma_Psi3770, sigma_Psi3770)



    eff_pppi0 = RooRealVar("eff_pppi0","eff_pppi0",0.06)
    pppi0 = RooGenericPdf("pppi0_{}".format(kSource),"Jpsi.pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))
    n_pppi0 = RooFormulaVar("n_pppi0_{}".format(kSource),"n_pppi0","@0*@1*(1.19/2.12)",RooArgList(n_Jpsi,eff_pppi0))


    ##Bkg fit
    #a0 = RooRealVar("a0_{}".format(kSource),"a0",0.4,-1,2)
    #a1 = RooRealVar("a1_{}".format(kSource),"a1",0.05,-1.,1.)
    #a2 = RooRealVar("a2_{}".format(kSource),"a2",-0.005,-1,1.)

    ##a0.setVal(0.4)
    #a1.setVal(0.04)
    #a2.setVal(0.04)
    #bkg = RooGenericPdf("bkg_{}".format(kSource),"background","TMath::Exp(-(@0-3550.)/200.*@1)*(1.+@2*(@0-3550.)/200.+@3*(@0-3550.)*(@0-3550.)/200./200.)", RooArgList(Jpsi_M,a0,a1,a2))
    #bkg = RooChebychev ("bkg_{}".format(kSource),"Background",Jpsi_M,RooArgList(a0,a1,a2))

    #Bkg fit
    a0_High = RooRealVar("a0_High_{}".format(kSource),"a0",0.4,-2,2)
    a1_High = RooRealVar("a1_High_{}".format(kSource),"a1",0.05,-1.,1.)
    a2_High = RooRealVar("a2_High_{}".format(kSource),"a2",-0.005,-1,1.)

    #a0_High.setVal(0.4)
    a1_High.setVal(0.04)
    a2_High.setVal(0.04)
    if kSource=="prompt":
        #bkg_High = RooGenericPdf("bkg_High_{}".format(kSource),"background","TMath::Exp(-(@0-3550.)/200.*@1)*(1.+@2*(@0-3550.)/200.+@3*(@0-3550.)*(@0-3550.)/200./200.)", RooArgList(Jpsi_M,a0_High,a1_High,a2_High))
        bkg_High = RooChebychev ("bkg_High_{}".format(kSource),"Background",Jpsi_M,RooArgList(a0_High,a1_High,a2_High))
    else:
        #bkg_High = RooGenericPdf("bkg_High_{}".format(kSource),"background","TMath::Exp(-(@0-3550.)/200.*@1)*(1.+@2*(@0-3550.)/200)", RooArgList(Jpsi_M,a0_High,a1_High))
        bkg_High = RooChebychev ("bkg_High_{}".format(kSource),"Background",Jpsi_M,RooArgList(a0_High,a1_High))
        #bkg_High = RooChebychev ("bkg_High_{}".format(kSource),"Background",Jpsi_M,RooArgList(a0_High,a1_High, a2_High))
        #bkg_High = RooGenericPdf("bkg_High_{}".format(kSource),"background","TMath::Exp(-(@0-3550.)/200.*@1)*(1.+@2*(@0-3550.)/200.+@3*(@0-3550.)*(@0-3550.)/200./200.)", RooArgList(Jpsi_M,a0_High,a1_High,a2_High))


    #Bkg fit
    a0_Low = RooRealVar("a0_Low_{}".format(kSource),"a0",0.4,-2,2)
    a1_Low = RooRealVar("a1_Low_{}".format(kSource),"a1",0.05,-1.,1.)
    a2_Low = RooRealVar("a2_Low_{}".format(kSource),"a2",-0.005,-1,1.)
    #a3_Low = RooRealVar("a3_Low","a2",-0.005,-1,1.)

    #a0_Low.setVal(0.4)
    a1_Low.setVal(0.04)
    a2_Low.setVal(0.04)
    if kSource=="prompt":
        #bkg_Low = RooGenericPdf("bkg_Low_{}".format(kSource),"background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.+@3*(@0-3050.)*(@0-3050.)/200./200.)", RooArgList(Jpsi_M,a0_Low,a1_Low,a2_Low))
        bkg_Low = RooChebychev ("bkg_Low_{}".format(kSource),"Background",Jpsi_M,RooArgList(a0_Low,a1_Low,a2_Low))
    else:
        #bkg_Low = RooGenericPdf("bkg_Low_{}".format(kSource),"background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.+@3*(@0-3050.)*(@0-3050.)/200./200.)", RooArgList(Jpsi_M,a0_Low,a1_Low,a2_Low))
        #bkg_Low = RooGenericPdf("bkg_Low_{}".format(kSource),"background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200)", RooArgList(Jpsi_M,a0_Low,a1_Low))
        bkg_Low = RooChebychev ("bkg_Low_{}".format(kSource),"Background",Jpsi_M,RooArgList(a0_Low,a1_Low))
        #bkg_Low = RooChebychev ("bkg_Low_{}".format(kSource),"Background",Jpsi_M,RooArgList(a0_Low,a1_Low, a2_Low))



    modelSignal_High = RooAddPdf("modelSignal_High_{}".format(kSource),"etac2s signal", RooArgList(bwxg_etac2s, bwxg_chic0, bwxg_chic1, bwxg_chic2, bwxg_hc, gauss_Psi2s, bwxg_Psi3770), RooArgList(n_etac2s, n_chic0, n_chic1, n_chic2, n_hc, n_Psi2s, n_Psi3770))
    modelSignal_etac2s = RooAddPdf("modelSignal_etac2s_{}".format(kSource),"etac2s signal", RooArgList(bwxg_etac2s), RooArgList(n_etac2s))
    modelBkg_High = RooAddPdf("modelBkg_High_{}".format(kSource),"etac2s bkg", RooArgList(bkg_High), RooArgList(nBckgr_High))
    model_High = RooAddPdf("model_High_{}".format(kSource),"High diapason signal", RooArgList(bkg_High, bwxg_etac2s, bwxg_chic0, bwxg_chic1, bwxg_chic2, bwxg_hc, gauss_Psi2s, bwxg_Psi3770), RooArgList(nBckgr_High, n_etac2s, n_chic0, n_chic1, n_chic2, n_hc, n_Psi2s, n_Psi3770))


    modelSignal_Low = RooAddPdf("modelSignal_Low_{}".format(kSource),"Jpsi signal", RooArgList(bwxg_etac, gauss_Jpsi), RooArgList(n_etac, n_Jpsi))
    modelSignal_Jpsi = RooAddPdf("modelSignal_Jpsi_{}".format(kSource),"Jpsi signal", RooArgList(gauss_Jpsi), RooArgList(n_Jpsi))
    modelBkg_Low = RooAddPdf("modelBkg_Low_{}".format(kSource),"Jpsi bkg", RooArgList(bkg_Low), RooArgList(nBckgr_Low))
    model_Low = RooAddPdf("model_Low_{}".format(kSource),"Low diapason signal", RooArgList(bwxg_etac,gauss_Jpsi,bkg_Low,pppi0), RooArgList(n_etac,n_Jpsi,nBckgr_Low,n_pppi0))


    getattr(w,"import")(model_High,RooFit.RecycleConflictNodes())
    getattr(w,"import")(model_Low,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelSignal_High,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelSignal_Low,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelBkg_High,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelBkg_Low,RooFit.RecycleConflictNodes())


def create_sample(w):

    Jpsi_M = w.var("Jpsi_M")

    model_High_prompt = w.pdf("model_High_prompt")
    model_High_fromB = w.pdf("model_High_fromB")
    model_Low_prompt = w.pdf("model_Low_prompt")
    model_Low_fromB = w.pdf("model_Low_fromB")

    sample = RooCategory("sample","sample")
    sample.defineType("High_prompt")
    sample.defineType("High_fromB")
    sample.defineType("Low_prompt")
    sample.defineType("Low_fromB")

    Jpsi_M.setRange("fitRange_Low_prompt", minM_Low, maxM_Low)
    Jpsi_M.setRange("fitRange_Low_fromB", minM_Low, maxM_Low)
    Jpsi_M.setRange("fitRange_High_prompt", minM_High, maxM_High)
    Jpsi_M.setRange("fitRange_High_fromB", minM_High, maxM_High)

    data_h_pr = w.data("dh_High_prompt")
    data_h_fb = w.data("dh_High_fromB")
    data_l_pr = w.data("dh_Low_prompt")
    data_l_fb = w.data("dh_Low_fromB")


    # Construct combined dataset in (Jpsi_M,sample)
    combData = RooDataHist("combData", "combined data", RooArgList(Jpsi_M), RooFit.Index(sample), RooFit.Import("High_prompt",data_h_pr), RooFit.Import("High_fromB",data_h_fb), RooFit.Import("Low_prompt",data_l_pr), RooFit.Import("Low_fromB",data_l_fb))


    # Associate model with the physics state and model_ctl with the control state
    simPdf = RooSimultaneous("simPdf","simultaneous signal pdf",sample)
    simPdf.addPdf(model_High_prompt,"High_prompt")
    simPdf.addPdf(model_High_fromB,"High_fromB")
    simPdf.addPdf(model_Low_prompt,"Low_prompt")
    simPdf.addPdf(model_Low_fromB,"Low_fromB")


    #   getattr(w,"import")(model,RecycleConflictNodes())
    getattr(w,"import")(combData)
    getattr(w,"import")(simPdf)


def perform_fit_simult():

    w = RooWorkspace("w",True)

    get_data_h(w,"prompt")
    get_data_h(w,"fromB")

    norm = "Jpsi"
    yields_connect(w,norm)

    fillRelWorkspace(w,"prompt")
    fillRelWorkspace(w,"fromB")

    create_sample(w)

    Jpsi_M = w.var("Jpsi_M")
    Jpsi_M.setBins(binN_tot)

    modelSignal_High_prompt = w.pdf("modelSignal_High_prompt")
    modelSignal_High_fromB = w.pdf("modelSignal_High_fromB")
    modelSignal_Low_prompt = w.pdf("modelSignal_Low_prompt")
    modelSignal_Low_fromB = w.pdf("modelSignal_Low_fromB")

    modelBkg_High_prompt = w.pdf("modelBkg_High_prompt")
    modelBkg_Low_prompt = w.pdf("modelBkg_Low_prompt")
    modelBkg_High_fromB = w.pdf("modelBkg_High_fromB")
    modelBkg_Low_fromB = w.pdf("modelBkg_Low_fromB")
    model_High_fromB = w.pdf("model_High_fromB")
    model_Low_fromB = w.pdf("model_Low_fromB")
    model_High_prompt = w.pdf("model_High_prompt")
    model_Low_prompt = w.pdf("model_Low_prompt")

    data_h_pr = w.data("dh_High_prompt")
    data_h_fb = w.data("dh_High_fromB")
    data_l_pr = w.data("dh_Low_prompt")
    data_l_fb = w.data("dh_Low_fromB")


    sample = w.cat("sample")
    simPdf = w.pdf("simPdf")
    combData = w.data("combData")

    #data_etac2s = w.data("dh_High")
    #data_Jpsi = w.data("dh_Low")


    #w.var("n_chic1_prompt").setVal(0), w.var("n_chic1_prompt").setConstant(True)
    #w.var("n_chic2_fromB").setVal(0), w.var("n_chic2_fromB").setConstant(True)
    w.var("n_hc_fromB").setVal(0),         w.var("n_hc_fromB").setConstant(True)
    w.var("n_Psi2s_prompt").setVal(0),     w.var("n_Psi2s_prompt").setConstant(True)
    w.var("N_etac2sRel_prompt").setVal(0), w.var("N_etac2sRel_prompt").setConstant(True)
    w.var("N_etac2sRel_fromB").setVal(0),  w.var("N_etac2sRel_fromB").setConstant(True)

    r = model_Low_fromB.fitTo(data_l_fb, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange_Low_fromB"))
    w.var("sigma_Jpsi").setConstant(True)
    # model_High_fromB.fitTo(data_h_fb, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange_High_fromB"))
    model_Low_prompt.fitTo(data_l_pr, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange_Low_prompt"))
    # model_High_prompt.fitTo(data_h_pr, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange_High_prompt"))
    # modelBkg_Low_fromB.fitTo(data_l_fb, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange_Low_fromB"), RooFit.NumCPU(8, 1))
    # modelBkg_High_fromB.fitTo(data_h_fb, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange_High_fromB"), RooFit.NumCPU(8, 1))
    # modelBkg_Low_prompt.fitTo(data_l_pr, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange_Low_prompt"), RooFit.NumCPU(8, 1))
    # modelBkg_High_prompt.fitTo(data_h_pr, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange_High_prompt"), RooFit.NumCPU(8, 1))

    w.var("N_etac2sRel_prompt").setConstant(False)
    w.var("N_etac2sRel_fromB").setConstant(False)

    #w.var("sigma_Jpsi").setVal(sigma_0)
    #simPdf.setNormRange("fitRange_High_fromB, fitRange_High_prompt, fitRange_Low_fromB, fitRange_Low_prompt")
    simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.Strategy(1) )
    # simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.Strategy(1) )
    #w.var("sigma_Jpsi").setConstant(False)
    #w.var("N_etac2sRel_prompt").setVal(w.var("N_etac2sRel_prompt").getValV())
    #w.var("N_etac2sRel_fromB").setVal(w.var("N_etac2sRel_fromB").getValV())
    #w.var("N_etac2sRel_fromB").setConstant(False)
    #w.var("n_Psi2s_prompt").setConstant(True)
    #w.var("n_Psi2s_fromB").setConstant(True)
    #simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.NumCPU(8))
    #w.var("N_etac2sRel_fromB").setConstant(True)
    #w.var("N_etac2sRel_prompt").setConstant(False)
    #w.var("N_etac2sRel_prompt").setVal(0.001)
    #w.var("N_etac2sRel_fromB").setVal(w.var("N_etac2sRel_fromB").getValV())
    #r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.NumCPU(8), RooFit.Strategy(1))#, RooFit.Minos())
    w.var("mass_Jpsi").setConstant(False)
    # w.var("sigma_Jpsi").setConstant(True)
    # r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.Strategy(1))#, RooFit.Minos())
    # w.var("mass_Jpsi").setConstant(True)
    w.var("sigma_Jpsi").setConstant(False)
    r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.Strategy(1))#, RooFit.Minos())
    # r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.Strategy(1))#, RooFit.Minos())



    names  = ["Low","High"]
    prods  = ["prompt","fromB"]
    for prod in prods:
        namePic = homeDir + "/mass_fit/sim_fit/Plot_sim_diffBg_cheb_test_{}.pdf".format(prod)
        frames, resids, pulls = [], [], []
        for reg in names:

            frame, resid, pull = dm.set_frame(w,reg,prod)
            frames.append(frame)
            resids.append(resid)
            pulls.append(pull)

        c = dm.set_canvas(names, frames, resids, pulls)
        c.SaveAs(namePic)


    nameTxt = homeDir + "/mass_fit/sim_fit/Result_sim_diffBg_cheb_test.txt"
    nameWksp = homeDir + "/mass_fit/sim_fit/Wksp_sim_diffBg_cheb_test.root"
    w.writeToFile(nameWksp)

    import os, sys
    save = os.dup( sys.stdout.fileno() )
    newout = open(nameTxt, "a" )
    os.dup2( newout.fileno(), sys.stdout.fileno() )
    r.Print("v")
    r.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() )
    newout.close()

    print("fit performed successfully")

    return w


def calcUL(w):


    Jpsi_M = w.var("Jpsi_M")

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf")
    combData = w.data("combData")

    N_etac2sRel_prompt = w.var("N_etac2sRel_prompt")
    N_etac2sRel_fromB = w.var("N_etac2sRel_fromB")

    w.var("a0_High_prompt").setConstant()
    w.var("a1_High_prompt").setConstant()
    w.var("a2_High_prompt").setConstant()
    w.var("a0_Low_prompt").setConstant()
    w.var("a1_Low_prompt").setConstant()
    w.var("a2_Low_prompt").setConstant()
    w.var("a0_High_fromB").setConstant()
    w.var("a1_High_fromB").setConstant()
    #w.var("a2_High_fromB").setConstant()
    w.var("a0_Low_fromB").setConstant()
    w.var("a1_Low_fromB").setConstant()
    #w.var("a2_Low_fromB").setConstant()
    w.var("n_chic0_fromB").setConstant(True)
    w.var("n_chic0_prompt").setConstant(True)
    w.var("n_chic1_fromB").setConstant(True)
    w.var("n_chic1_prompt").setConstant(True)
    w.var("n_chic2_fromB").setConstant(True)
    w.var("n_chic2_prompt").setConstant(True)
    w.var("n_Psi2s_prompt").setConstant(True)
    w.var("n_Psi2s_fromB").setConstant(True)
    w.var("n_hc_prompt").setConstant(True)
    w.var("n_hc_fromB").setConstant(True)
    w.var("sigma_Jpsi").setConstant(True)


    N_etac2sRel_prompt.setConstant(False)
    N_etac2sRel_fromB.setConstant(True)

    N_etac2sRel_fromB.setRange(0., 0.05)
    N_etac2sRel_prompt.setRange(0., 0.1)

    nBckgr_High_prompt = w.var("nBckgr_High_prompt")
    nBckgr_High_fromB  = w.var("nBckgr_High_fromB")
    nBckgr_Low_prompt  = w.var("nBckgr_Low_prompt")
    nBckgr_Low_fromB   = w.var("nBckgr_Low_fromB")
    N_Jpsi_prompt      = w.var("N_Jpsi_prompt")
    N_Jpsi_fromB       = w.var("N_Jpsi_fromB")
    N_etac_prompt      = w.var("N_etac_prompt")
    N_etac_fromB       = w.var("N_etac_fromB")

    #nBckgr_High_prompt.setConstant()
    #nBckgr_High_fromB.setConstant()
    nBckgr_Low_prompt.setConstant()
    nBckgr_Low_fromB.setConstant()
    N_Jpsi_prompt.setConstant()
    N_Jpsi_fromB.setConstant()
    N_etac_prompt.setConstant()
    N_etac_fromB.setConstant()


    paramOfInterest = RooArgSet(N_etac2sRel_prompt)
    #constrainedParams = RooArgSet(nBckgr_High,nBckgr_Low, n_Jpsi, n_etac)
    constrainedParams = RooArgSet(nBckgr_High_prompt,nBckgr_High_fromB, nBckgr_Low_prompt, nBckgr_Low_fromB, N_Jpsi_prompt, N_Jpsi_fromB, N_etac_prompt, N_etac_fromB)

    nuis_iter = constrainedParams.createIterator()
    var = nuis_iter.Next()
    while var:
        var.setRange(var.getVal()-10*var.getError(), var.getVal()+10*var.getError())
        var = nuis_iter.Next()



    modelConfig = RooStats.ModelConfig(w);
    modelConfig.SetPdf(simPdf)
    modelConfig.SetParametersOfInterest(paramOfInterest)
    modelConfig.SetNuisanceParameters(constrainedParams)
    modelConfig.SetObservables(RooArgSet(RooArgList(Jpsi_M, sample)))
    #modelConfig.SetGlobalObservables( RooArgSet(*gSigEff,*gSigBkg))
    modelConfig.SetName("ModelConfig")
    modelConfig.SetSnapshot(paramOfInterest)
    getattr(w,"import")(modelConfig)


    sbModel = w.obj("ModelConfig")
    poi = sbModel.GetParametersOfInterest().first()
    bModel = sbModel.Clone()
    bModel.SetName(sbModel.GetName()+"_with_poi_0")
    poi.setVal(0)
    bModel.SetSnapshot(RooArgSet(poi))

    fc = RooStats.FrequentistCalculator(combData, bModel, sbModel)
    fc.SetToys(1000,500)
    ac = RooStats.AsymptoticCalculator(combData, bModel, sbModel)
    ac.SetOneSided(True)
    ac.SetPrintLevel(-1)

    calc = RooStats.HypoTestInverter(ac)
    calc.SetConfidenceLevel(0.95)

    calc.UseCLs(True)
    calc.SetVerbose(False)

    toymcs = calc.GetHypoTestCalculator().GetTestStatSampler()

    profll = RooStats.ProfileLikelihoodTestStat(sbModel.GetPdf())
    profll.SetOneSided(True)
    toymcs.SetTestStatistic(profll)

    if not sbModel.GetPdf().canBeExtended():
        toymcs.SetNEventsPerToy(1)
        print ('can not be extended')

    npoints = 20
    poimin = poi.getMin()
    poimax = poi.getMax()


    calc.SetFixedScan(npoints,poimin,poimax)
    r = calc.GetInterval()
    upperLimit = r.UpperLimit()
    print("Exp Upper Limit is", r.GetExpectedUpperLimit())
    print("Upper Limit is", upperLimit)


    plot = RooStats.HypoTestInverterPlot("HTI_Result_Plot","HypoTest Scan Result",r)
    canvUL = TCanvas("HypoTestInverter Scan")
    canvUL.SetLogy(False)
    plot.Draw("CLb 2CL")

    homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"

    kBkg = "cheb"
    #plot.Draw("EXP")
    canvUL.RangeAxis(poimin, 0., poimax, 1.1)
    canvUL.Draw()
    canvUL.SaveAs(homeDir + "/mass_fit/sim_fit/CL_Ac_{}_{}_sim.root".format("prompt",kBkg))
    canvUL.SaveAs(homeDir + "/mass_fit/sim_fit/CL_Ac_{}_{}_sim.pdf".format("prompt",kBkg))




    ### C o n s t r u c t   p l a i n   l i k e l i h o o d
    ### ---------------------------------------------------

    # Construct unbinned likelihood
    nll = simPdf.createNLL(combData, RooFit.NumCPU(8), RooFit.Extended(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.Offset(True))
    #nll = simPdf.createNLL(combData, RooFit.NumCPU(8),  RooFit.Extended(True),  RooFit.Range("fitRange"), RooFit.SplitRange(True))

    # Minimize likelihood w.r.t all parameters before making plots
    #m = RooMinuit(nll)
    m = RooMinimizer(nll)
    #m.minos()
    m.migrad()
    m.hesse()

    # Plot likelihood scan frac
    frameRS = N_etac2sRel_prompt.frame(RooFit.Bins(46),RooFit.Title("LL and profileLL in n_{#eta_{c}}(2S) "))
#     nll.plotOn(frame1,ShiftToZero())

    # The profile likelihood estimator on nll for frac will minimize nll w.r.t
    # all floating parameters except frac for each evaluation


    #pll_N_etac2s = nll.createProfile(RooArgSet(N_etac2sRel_prompt))
    pll_N_etac2s = RooProfileLL("pll_N_etac2s", "pll_N_etac2s", nll, RooArgSet(N_etac2sRel_prompt))
    llhood = RooFormulaVar("llhood","exp(-0.5*pll_N_etac2s)", RooArgList(pll_N_etac2s));

    # Plot the profile likelihood in frac
    pll_N_etac2s.plotOn(frameRS, RooFit.LineColor(2))

    # Adjust frame maximum for visual clarity
    #frameRS.SetMinimum(0)

    cNLL = TCanvas("rf605_profilell","rf605_profilell",800, 400)
    cNLL.cd(),  gPad.SetLeftMargin(0.15),  frameRS.GetYaxis().SetTitleOffset(1.4),  frameRS.Draw()

    cNLL.SaveAs(homeDir + "/mass_fit/sim_fit/NLL_diffBg_cheb_new.pdf")


    fllhood = llhood.asTF(RooArgList(paramOfInterest))
    npoints = 50
    int_array = np.ones(npoints+1)
    int_val0 = fllhood.Integral(poimin, poimax, 1e-6)
    print(int_val0)
    poi_urs = np.linspace(poimin, poimax, npoints+1)

    for idx in range(npoints):

        int_val = fllhood.Integral(poimin, poi_urs[idx+1], 1e-6)
        int_array[idx+1] = 1.-int_val/int_val0
        if int_array[idx+1]<0.05: print(poi_urs[idx+1])

    print(int_array)
    gr_pval = TGraph(npoints+1, poi_urs, int_array)
    canvUL.cd()
    gr_pval.SetLineColor(2)
    gr_pval.GetXaxis().SetRangeUser(poimin, poimax)
    gr_pval.Draw("same")
    canvUL.SaveAs(homeDir + "/mass_fit/sim_fit/CL_As_sim_cheb_check_new.pdf")
    canvUL.SaveAs(homeDir + "/mass_fit/sim_fit/CL_As_sim_cheb_check_new.root")



    #return w

if __name__ == "__main__":

    # from drawModule import *
    perform_fit_simult()
    #w = perform_fit_simult()


    # nameWksp = homeDir + "/mass_fit/sim_fit/FitMass_wksp_sim_diffBg_cheb_new.root"
    # file_w = TFile(nameWksp)
    # w = file_w.Get("w")
    # calcUL(w)
    # del w
