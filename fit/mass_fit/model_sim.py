# from ROOT import *
from ROOT import RooRealVar, RooFormulaVar, RooGaussian, RooChebychev, RooGenericPdf, \
    RooFFTConvPdf, RooAddPdf, RooProdPdf, RooArgSet, RooArgList, \
    RooSimultaneous, RooCategory, RooDataHist 

from ROOT import RooFit, SetOwnership, TFile, gROOT
from ROOT.TMath import Sqrt, Sq

gitDir  = "/users/LHCb/zhovkovska/etac2s_ppbar/fit/"
# homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/"

gROOT.LoadMacro(f"{gitDir}/libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro(f"{gitDir}/libs/libBifurcatedCB/BifurcatedCB.cxx+")
from ROOT import RooRelBreitWigner, BifurcatedCB

import sys
sys.path.insert(1, f"{gitDir}/libs/")

from drawModule_splitFit import minM_Low, maxM_Low, minM_High, maxM_High
from charmonium import Particle

proton = Particle("p")
m_win = 100.


ExtMeas = {
    #    "cs_jpsi_prompt" : { "val"    : 1372.9, #[nb]
    #                         "err"    : 58.,
    #                         "relerr" : -1.},
       "cs_jpsi_prompt" : { "val"    : 1.3729, #[mub] for chicJ absolute CS
                            "err"    : 0.058,
                            "relerr" : -1.},
    #    "cs_jpsi_fromB"  : { "val"    : 366.6,
    #                         "err"    : 20.,
    #                         "relerr" : -1.},
       "cs_jpsi_fromB"  : { "val"    : 0.3666,  #for chicJ absolute CS
                            "err"    : 0.020,
                            "relerr" : -1.},
    #    "ct_hc"          : { "val"    : 1.0,
    #                         "err"    : 1.e-5,
    #                         "relerr" : -1.},
    #    "ct_psi2S"       : { "val"    : 1.0,
    #                         "err"    : 1.e-5,
    #                         "relerr" : -1.},
    #    "ct_etac2S"      : { "val"    : 1.0,
    #                         "err"    : 1.e-5,
    #                         "relerr" : -1.},
       "BR_hc"          : { "val"    : 3.5e-5,
                            "err"    : 2.4e-5,
                            "relerr" : -1.},
       "BR_etac"        : { "val"    : 1.45e-3,
                            "err"    : 0.14e-3,
                            "relerr" : -1.},
       "BR_jpsi"        : { "val"    : 2.121e-3,
                            "err"    : 0.03e-3,
                            "relerr" : -1.},
       "BR_etac2S"      : { "val"    : 7.7e-5,
                            "err"    : 2.4e-5,
                            "relerr" : -1.},
       "BR_psi2S"       : { "val"    : 2.94e-4,
                            "err"    : 0.08e-4,
                            "relerr" : -1.},
       "BR_chic0"       : { "val"    : 2.21e-4,
                            "err"    : 0.08e-4,
                            "relerr" : -1.},
       "BR_chic1"       : { "val"    : 7.60e-5,
                            "err"    : 0.34e-5,
                            "relerr" : -1.},
       "BR_chic2"       : { "val"    : 7.33e-5,
                            "err"    : 0.33e-5,
                            "relerr" : -1.},
    #    "eff_hc2jpsi"    : { "val"    : 1.067, 
    #                         "err"    : 0.016,
    #                         "relerr" : 0.01},
    #    "eff_hc2etac"    : { "val"    : 1.067, 
    #                         "relerr" : 0.01},
    #                         "err"    : 0.016,
    #    "eff_etac2S2jpsi": { "val"    : 1.067, 
    #                         "err"    : 0.016,
    #                         "relerr" : 0.01},
    #    "eff_psi2S2jpsi" : { "val"    : 1.067, 
    #                         "err"    : 0.016,
    #                         "relerr" : 0.01},
    #    "eff_etac2S2etac": { "val"    : 1.067,  
    #                         "err"    : 0.016,
    #                         "relerr" : 0.01},
       "n_jpsi"         : { "val"    : 93914., # should be picked up in more smart way every time fit of Low is updated !
                            "err"    : 3100.,  # same
                            "relerr" : -1.},
       "n_etac"         : { "val"    : 96934., # should be picked up in more smart way every time fit of Low is updated !
                            "err"    : 7670.,  # same
                            "relerr" : -1.},
       "n_psi2S"        : { "val"    : 997., # should be picked up in more smart way every time fit of High is updated !
                            "err"    : 69.,  # same
                            "relerr" : -1.},
       "n_jpsi_prompt"  : { "val"    : 93914., # should be picked up in more smart way every time fit of Low is updated !
                            "err"    : 3100.,  # same
                            "relerr" : -1.},
       "n_etac_prompt"  : { "val"    : 96934., # should be picked up in more smart way every time fit of Low is updated !
                            "err"    : 7670.,  # same
                            "relerr" : -1.},
       "n_psi2S_prompt" : { "val"    : 997., # should be picked up in more smart way every time fit of High is updated !
                            "err"    : 69.,  # same
                            "relerr" : -1.},
       "n_jpsi_fromB"   : { "val"    : 93914., # should be picked up in more smart way every time fit of Low is updated !
                            "err"    : 3100.,  # same
                            "relerr" : -1.},
       "n_etac_fromB"   : { "val"    : 96934., # should be picked up in more smart way every time fit of Low is updated !
                            "err"    : 7670.,  # same
                            "relerr" : -1.},
       "n_psi2S_fromB"  : { "val"    : 997., # should be picked up in more smart way every time fit of High  is updated !
                            "err"    : 69.,  # same
                            "relerr" : -1.}
    }


def upd_meas(w, kLim="etac2S", kNorm="jpsi", kSyst="Base", kSource=None):

    if kSource!=None:
        postfix = "_{}".format(kSource)
    else:
        postfix = ""

    n_norm   = f"n_{kNorm}{postfix}"
    BR_norm  = f"BR_{kNorm}"
    eff_norm = f"eff_{kLim}2{kNorm}{postfix}"
    ct_norm  = f"ct_{kNorm}" 
    ct_lim   = f"ct_{kLim}" 
    cs_norm  = f"cs_{kNorm}{postfix}"
    BR_lim   = f"BR_{kLim}"

    if kLim=="hc":
        f = open(f'{homeDir}/MC/cross_talks/cross_talk_{"chic1"}.txt', 'r')
    else:
        f = open(f'{homeDir}/MC/cross_talks/cross_talk_{kLim}.txt', 'r')
    line = f.readline().split()

    ExtMeas[ct_lim] = {}
    if kSource=="prompt":
        ExtMeas[ct_lim]["val"], ExtMeas[ct_lim]["err"] = float(line[1]), float(line[5])
    elif kSource=="fromB":
        ExtMeas[ct_lim]["val"], ExtMeas[ct_lim]["err"] = float(line[4]), float(line[8])
    else:
        ExtMeas[ct_lim]["val"], ExtMeas[ct_lim]["err"] = 1., 0.0001
    ExtMeas[ct_lim]["relerr"] = ExtMeas[ct_lim]["err"]/ExtMeas[ct_lim]["val"]
    # print(ExtMeas["ct_{}".format(kLim)]["val"], ExtMeas["ct_{}".format(kLim)]["err"])
    f.close()

    ExtMeas[ct_norm] = {}
    f = open('{}/MC/cross_talks/cross_talk_{}.txt'.format(homeDir,kNorm), 'r')
    line = f.readline().split()
    if kSource=="prompt":
        ExtMeas[ct_norm]["val"], ExtMeas[ct_norm]["err"] = float(line[1]), float(line[5])
    elif kSource=="fromB":
        ExtMeas[ct_norm]["val"], ExtMeas[ct_norm]["err"] = float(line[4]), float(line[8])
    else:
        ExtMeas[ct_norm]["val"], ExtMeas[ct_norm]["err"] = 1., 0.0001
    ExtMeas[ct_norm]["relerr"] = ExtMeas[ct_norm]["err"]/ExtMeas[ct_norm]["val"]
    # print(ExtMeas[ct_norm]["val"], ExtMeas[ct_norm]["err"])
    f.close()

    filename = ""
    if kSource=="fromB":
        if kLim != "hc":
            filename = "{0}/eff/note_v2/plots_{1}/ratio_{3}2{2}_{1}_PT_tot.root".format(homeDir, "sec", kNorm, kLim)
        else:
            filename = "{0}/eff/note_v2/plots_{1}/ratio_{3}2{2}_{1}_PT_tot.root".format(homeDir, "sec", kNorm, "chic1")
    else:
        if kLim != "hc":
            filename = "{0}/eff/note_v2/plots_{1}/ratio_{3}2{2}_{1}_PT_tot.root".format(homeDir, "prompt", kNorm, kLim)
        else:
            filename = "{0}/eff/note_v2/plots_{1}/ratio_{3}2{2}_{1}_PT_tot.root".format(homeDir, "prompt", kNorm, "chic1")


    feff = TFile(filename,"READ")
    ExtMeas[eff_norm] = {}
    ExtMeas[eff_norm]["val"] = feff.Get("eff_val").GetVal()
    ExtMeas[eff_norm]["err"] = feff.Get("eff_err").GetVal()
    ExtMeas[eff_norm]["relerr"] = ExtMeas[eff_norm]["err"]/ExtMeas[eff_norm]["val"]
    feff.Close()

    # ExtMeas[n_norm]["val"] = w.var(n_norm).getValV()
    # ExtMeas[n_norm]["err"] = w.var(n_norm).getError()
    # ExtMeas[n_norm]["relerr"] = w.var(n_norm).getError()/w.var(n_norm).getValV()

    if kSyst=="BR":
        ExtMeas[BR_norm]["val"]    += ExtMeas[BR_norm]["err"]
        ExtMeas[BR_norm]["relerr"]  = ExtMeas[BR_norm]["err"]/ExtMeas[BR_norm]["val"]
    elif kSyst=="eff":
        ExtMeas[eff_norm]["val"]   += ExtMeas[eff_norm]["err"]
        ExtMeas[eff_norm]["relerr"] = ExtMeas[eff_norm]["err"]/ExtMeas[eff_norm]["val"]
    elif kSyst=="cross_talk":
        ExtMeas[ct_lim]["val"]   += ExtMeas[ct_lim]["err"]
        ExtMeas[ct_lim]["relerr"] = ExtMeas[ct_lim]["err"]/ExtMeas[ct_lim]["val"]

    # total_val =  ExtMeas[n_norm]["val"]*ExtMeas[eff_norm]["val"]*ExtMeas["BR_etac2S"]["val"]/ExtMeas[BR_norm]["val"]/ExtMeas[ct_lim]["val"]
    # total_val =  ExtMeas[eff_norm]["val"]*ExtMeas["BR_etac2S"]["val"]/ExtMeas[BR_norm]["val"]/ExtMeas[ct_lim]["val"]

    # Relative CS x BR
    # total_val =  ExtMeas[eff_norm]["val"]*ExtMeas[ct_norm]["val"]/ExtMeas[ct_lim]["val"]
    # Absolute CS x BR
    # total_val =  ExtMeas[eff_norm]["val"]*ExtMeas[ct_norm]["val"]/ExtMeas[cs_norm]["val"]/ExtMeas[BR_norm]["val"]/ExtMeas[ct_lim]["val"] 
    # if "chic" in kNorm:
    #     total_val =  ExtMeas[eff_norm]["val"]*ExtMeas[ct_norm]["val"]/ExtMeas[ct_lim]["val"]*ExtMeas[BR_lim]["val"]/ExtMeas[BR_norm]["val"]
    # Relative CS
    total_val =  ExtMeas[eff_norm]["val"]*ExtMeas[ct_norm]["val"]/ExtMeas[ct_lim]["val"]*ExtMeas[BR_lim]["val"]
    # Absolute CS
    # total_val =  ExtMeas[eff_norm]["val"]*ExtMeas[ct_norm]["val"]/ExtMeas[ct_lim]["val"]*ExtMeas[BR_lim]["val"]/ExtMeas[cs_norm]["val"]/ExtMeas[BR_norm]["val"]

    total_relerr = 0.
    # if "chic" in kNorm:
    #     list_relerr = [ct_lim, ct_norm, eff_norm, BR_lim, BR_norm]
    # for source in [ct_lim, "BR_etac2S", BR_norm, eff_norm, n_norm]:
    # Relative CS x BR
    # list_relerr = [ct_lim, ct_norm, eff_norm]
    # Absolute CS x BR
    # list_relerr = [ct_lim, ct_norm, eff_norm, cs_norm, BR_norm,]
    # Relative CS
    list_relerr = [ct_lim, ct_norm, eff_norm, BR_lim]
    # Absolute CS
    # list_relerr = [ct_lim, ct_norm, eff_norm, cs_norm, BR_norm, BR_lim]
    for source in list_relerr:
        meas = ExtMeas[source]
        val  = meas["val"]
        if meas["relerr"] != -1. :
            relerr = meas["relerr"]
        else:
            relerr = meas["err"]/val
        total_relerr += Sq(relerr)
    total_relerr = Sqrt(total_relerr)
    total_err = total_relerr * total_val

    #total_val =  ExtMeas[n_norm]["val"]*ExtMeas["eff_etac2S2Jpsi"]["val"]

    #total_relerr = 0.
    #for source in ["eff_etac2S2Jpsi",n_norm]:
        #meas = ExtMeas[source]
        #val  = meas["val"]
        #if meas["relerr"] != -1. :
            #relerr = meas["relerr"]
        #else:
            #relerr = meas["err"]/val
        #total_relerr += Sq(relerr)
    #total_relerr = Sqrt(total_relerr)
    #total_err = total_relerr * total_val

    print(" OVERAL factor is      :", total_val)
    print(" WITH relative error   :", total_relerr)

    # constraint on the yield
    extFactorObs      = RooRealVar("extFactorObs_{}".format(kSource),  "extFactorObs", total_val, 0., total_val+5*total_err)
    extFactorObs.setConstant(True)
    
    extFactor         = RooRealVar("extFactor_{}".format(kSource),  "extFactor", total_val, 0, 25*total_val)
    constr_ext        = RooGaussian("constr_ext_{}".format(kSource),"constr_ext", extFactor, extFactorObs, RooFit.RooConst(total_err))
    getattr(w,"import")(constr_ext)

    # n_etac2S_rel = RooRealVar("n_etac2S_rel","num of Etac", 1.5e-3, 0, 4.)
    # n_etac2S = RooRealVar("n_etac2S","num of etac2S", 2e3, 0, 3.e4)
    # cs_etac2S_rel = RooRealVar("cs_{}_rel{}".format(kLim,postfix),"relative cs x BR", 5e-2, 0., 10.0) #limit was 1.5 
    cs_etac2S_rel = RooRealVar("cs_{}_rel{}".format(kLim,postfix),"relative cs x BR", 10., 0., 1000.0) #limit was 1.5 
    n_ref = w.var(n_norm)
    n_ccbar = RooFormulaVar("n_{}{}".format(kLim,postfix),"yield","@0*@1*@2", RooArgList(cs_etac2S_rel, extFactor, n_ref))

    getattr(w,"import")(n_ccbar)
    # return total_val, total_relerr, total_err
    return n_ccbar

def choose_bkg(optList: list, regName: str, argList: RooArgList):
    x = "(@0-{0})/{1}".format(3300., 500.)
    # x = "(@0-2*{0})/2000.".format(939.)
    # x = "(@0-2*{0})/1000.".format(939.)
    # x = "(@0/3800.)"
    # formula = "TMath::Exp(-{0}*@1)*(1.+@2*{0}+@3*{0}*{0}+@4*{0}*{0}*{0}+@5*{0}*{0}*{0}*{0})".format(x)
    # formula = "TMath::Exp(-@0*@1/3300.)*(1.+@2*{0}+@3*{0}*{0}+@4*{0}*{0}*{0}+@5*{0}*{0}*{0}*{0})".format(x)
    # formula = "TMath::Exp(-{0}*{0}*@1)*(1.+@2*{0}+@3*{0}*{0}+@4*{0}*{0}*{0}+@5*{0}*{0}*{0}*{0})".format(x)
    # formula = "TMath::Exp(-{0}*{0}*@1)*(1.+@2*{0}+@3*{0}*{0}+@4*{0}*{0}*{0})".format(x)
    # formula = "TMath::Exp(-{0}*@1)*(1.+@2*{0}+@3*{0}*{0})".format(x)
    # pdf_sqrt = RooGenericPdf("sqrt{}".format(regName),"background", "TMath::Sqrt(@0-2*939.)".format(x), RooArgList(argList.at(0)) )

    bkg_dict = {}

    if "Low" in regName:
    # if "Low" in regName or "High" in regName:
        bkg_dict = {
           "prompt Cheb" : RooChebychev ("bkg{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2),argList.at(3))),
           "fromB Cheb"  : RooChebychev ("bkg{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2))),
           "prompt Exp"  : RooGenericPdf("bkg{}".format(regName),"background", "TMath::Exp(-{0}*@1)*(1.+@2*{0}+@3*{0}*{0})".format(x), RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3))) ,
           # "prompt Exp"  : RooGenericPdf("bkg{}".format(regName),"background", "TMath::Exp(-{0}*@1)*(1.+@2*{0}+@3*{0}*{0}+@4*{0}*{0}*{0})".format(x), RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3),argList.at(4))) ,
           "fromB Exp"   : RooGenericPdf("bkg{}".format(regName),"background", "TMath::Exp(-{0}*@1)*(1.+@2*{0})".format(x), RooArgList(argList.at(0), argList.at(1),argList.at(2))) ,
        }
    elif "High" in regName:
        bkg_dict = {
           "prompt Cheb" : RooChebychev ("bkg{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2),argList.at(3),argList.at(4))),
           "fromB Cheb"  : RooChebychev ("bkg{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2))),
           "prompt Exp"  : RooGenericPdf("bkg{}".format(regName),"background", "TMath::Exp(-{0}*@1)*(1.+@2*{0})".format(x), RooArgList(argList.at(0), argList.at(1),argList.at(2))) ,
           "fromB Exp"   : RooGenericPdf("bkg{}".format(regName),"background", "TMath::Exp(-{0}*@1)*(1.+@2*{0})".format(x), RooArgList(argList.at(0), argList.at(1),argList.at(2))) ,
        }
    else:
        bkg_dict = {
        #    "prompt Cheb" : RooChebychev ("bkg{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2),argList.at(3),argList.at(4),argList.at(5))),
           "prompt Cheb" : RooChebychev ("bkg{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2),argList.at(3),argList.at(4))),
           "fromB Cheb"  : RooChebychev ("bkg{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2),argList.at(3))),
           "prompt ChebAlt" : RooChebychev ("bkg{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2),argList.at(3),argList.at(4),argList.at(5))),
           "fromB ChebAlt"  : RooChebychev ("bkg{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2),argList.at(3))),
           # "prompt Exp"  : RooGenericPdf("bkg{}".format(regName),"background", formula, RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3))) ,
           # "prompt Exp"  : RooGenericPdf("bkg{}".format(regName),"background", formula, RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3),argList.at(4),argList.at(5))) ,
           "prompt Sqr"  : RooGenericPdf("bkg{}".format(regName),"background", "TMath::Exp(-(@0/3800.-@2)*(@0/3800.-@2)*@1)*(1.+@3*{0}+@4*{0}*{0}+@5*{0}*{0}*{0})".format(x), RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3),argList.at(4),argList.at(5))) ,
           "fromB Sqr"   : RooGenericPdf("bkg{}".format(regName),"background", "TMath::Exp(-(@0/3800.-@2)*(@0/3800.-@2)*@1)*(1.+@3*{0}+@4*{0}*{0})".format(x), RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3),argList.at(4))) ,
           # "prompt Sqr"  : RooGenericPdf("bkg{}".format(regName),"background", "TMath::Exp(-@0*@0*@1)*(1.+@2*{0}+@3*{0}*{0}+@4*{0}*{0}*{0}+@5*{0}*{0}*{0}*{0})".format(x), RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3),argList.at(4),argList.at(5))) ,
           # "fromB Sqr"   : RooGenericPdf("bkg{}".format(regName),"background", "TMath::Exp(-@0*@0*@1)*(1.+@2*{0}+@3*{0}*{0}+@4*{0}*{0}*{0})".format(x), RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3),argList.at(4))) ,
           # "prompt Sqr"  : RooGenericPdf("bkg{}".format(regName),"background", "TMath::Exp(-{0}*{0}*@1)*(1.+@2*{0}+@3*{0}*{0}+@4*{0}*{0}*{0}+@5*{0}*{0}*{0}*{0})".format(x), RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3),argList.at(4),argList.at(5))) ,
           # "fromB Sqr"   : RooGenericPdf("bkg{}".format(regName),"background", "TMath::Exp(-{0}*{0}*@1)*(1.+@2*{0}+@3*{0}*{0}+@4*{0}*{0}*{0})".format(x), RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3),argList.at(4))) ,
           # "fromB Exp"   : RooGenericPdf("bkg{}".format(regName),"background","TMath::Exp(-(@0-3550.)/200.*@1)*(1.+@2*(@0-3550.)/200)", RooArgList(argList.at(0), argList.at(1),argList.at(2)))
        #    "prompt Exp"  : RooGenericPdf("bkg{}".format(regName),"background", "TMath::Exp(-{0}*@1)*(1.+@2*{0}+@3*{0}*{0}+@4*{0}*{0}*{0}+@5*{0}*{0}*{0}*{0})".format(x), RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3),argList.at(4),argList.at(5))) ,
           "prompt Exp"  : RooGenericPdf("bkg{}".format(regName),"background", "TMath::Exp(-{0}*@1)*(1.+@2*{0}+@3*{0}*{0}+@4*{0}*{0}*{0})".format(x), RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3),argList.at(4))) ,
           "fromB Exp"   : RooGenericPdf("bkg{}".format(regName),"background", "TMath::Exp(-{0}*@1)*(1.+@2*{0}+@3*{0}*{0})".format(x), RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3))) ,
        #    "prompt Sqrt" : RooGenericPdf("bkg{}".format(regName),"background", "TMath::Sqrt((@0-2*939.))*TMath::Exp(-{0}*@1)*(1.+@2*{0}+@3*{0}*{0}+@4*{0}*{0}*{0}+@5*{0}*{0}*{0}*{0})".format(x), RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3),argList.at(4),argList.at(5))) ,
           "prompt Sqrt" : RooGenericPdf("bkg{}".format(regName),"background", "TMath::Sqrt((@0-2*939.))*TMath::Exp(-{0}*@1)*(1.+@2*{0}+@3*{0}*{0}+@4*{0}*{0}*{0})".format(x), RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3),argList.at(4))) ,
           "fromB Sqrt"  : RooGenericPdf("bkg{}".format(regName),"background", "TMath::Sqrt((@0-2*939.))*TMath::Exp(-{0}*@1)*(1.+@2*{0}+@3*{0}*{0})".format(x), RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3))) ,
        }        
    return bkg_dict[optList[0] + " " + optList[1]]

def setMW(w, ccbar_list, ref_mass=None):

    # shifting ref particle to a first position
    # if ref_mass!=None:
    #     # ccbar_list.insert(0, ccbar_list.pop(ccbar_list.index(ref_mass)))

    radius   = RooRealVar("radius","radius", 1.)
    proton_m = RooRealVar("proton_m","proton mass", proton.mass )

    getattr(w,"import")(radius)
    getattr(w,"import")(proton_m)

    for name in ccbar_list:
        ccbar = Particle(name)

        gamma = RooRealVar("gamma_{}".format(ccbar.name),"width of Br-W", ccbar.width, 0., max(30., 3*ccbar.width) )
        gamma.setConstant(True)

    #     # mass  = RooAbsReal()
        mass  = RooRealVar("mass_{}".format(ccbar.name),"mean of gaussian", ccbar.mass, ccbar.mass-m_win, ccbar.mass+m_win)
        mass.setConstant(True)
    #     # if ref_mass==None:
    #     #     mass  = RooRealVar("mass_{}".format(ccbar.name),"mean of gaussian", ccbar.mass, ccbar.mass-m_win, ccbar.mass+m_win)
    #     #     mass.setConstant(True)

    #     # else:
        spin = RooRealVar("spin_{}".format(ccbar.name),"spin", ccbar.spin )
        spin.setConstant(True)

        getattr(w,"import")(gamma)
        getattr(w,"import")(mass)
        getattr(w,"import")(spin)


def fill_workspace(w, name, kRange, ref="etac", kCB=False, kSyst="Base"):

    if kRange!="":
        kRange = "_{}".format(kRange)

    if kCB: 
        return fill_workspace_CB(w, name, kRange, ref, kSyst)
    else:
        return fill_workspace_G(w, name, kRange, ref, kSyst)

def fill_workspace_G(w, name, kRange, ref="etac", kSyst="Base"):

    Jpsi_M = w.var("Jpsi_M{}".format(kRange))
    if Jpsi_M == None:
        Jpsi_M = w.var("Jpsi_M")

    ccbar = Particle(name)

    bw = False if (ccbar.width<1.e-3) else True

    ratio_sigma = 0.30
    ratio_ref = 1.1 # sigma_ccbar / sigma_ref
    ratio_area = 0.90

    r_ref = RooRealVar("r_ref_{}".format(name),"r_ref", ratio_ref, 0., 3.)

    f = TFile("{}//MC/mass_fit/Wksp_M_res_all_{}_sim.root".format(homeDir,ref))
    w_MC = f.Get("w")
    
    sigma_1   = w.var("sigma_{}_1".format(ccbar.name))
    r_NToW    = w.var("r_NToW")
    r_G1ToG2  = w.var("r_G1ToG2")
    
    if ref==ccbar.name and sigma_1==None:
        ratio_sigma = w_MC.var("r_NToW").getValV()
        ratio_area  = w_MC.var("r_G1ToG2").getValV()

        r_NToW    = RooRealVar("r_NToW","rNarToW",ratio_sigma)#, 0., 1.)
        r_G1ToG2  = RooRealVar("r_G1ToG2","rG1toG2",ratio_area)#, 0., 1.)
        sigma_1   = RooRealVar("sigma_{}_1".format(ccbar.name),"width of gaussian", 9., 0.1, 30.)
        
        sigma_scale = w_MC.var("r_ref_etac2S").getValV()
        f.Close()
    else:
        if name=="hc":
            r_ref_name = "r_ref_{}".format("chic1")
        else: 
            r_ref_name = "r_ref_{}".format(name)

        if kSyst != "SRatio":
            r_ref.setVal(w_MC.var(r_ref_name).getVal())
        else:
            r_ref.setVal(w_MC.var(r_ref_name).getVal()+w_MC.var(r_ref_name).getError())

        r_ref.setConstant(True)

        r_NToW    = w.var("r_NToW")
        r_G1ToG2  = w.var("r_G1ToG2")
        sigma_ref = w.var("sigma_{}_1".format(ref))
        sigma_1   = RooFormulaVar("sigma_{}_1".format(ccbar.name),"width of gaussian","@0*@1",RooArgList(RooArgSet(sigma_ref,r_ref)))
        f.Close()

    sigma_2 = RooFormulaVar("sigma_{}_2".format(ccbar.name),"width of gaussian","@0/@1",RooArgList(sigma_1,r_NToW))

    mass  = w.var("mass_{}".format(ccbar.name))
    gamma  = w.var("gamma_{}".format(ccbar.name))
    spin  = w.var("spin_{}".format(ccbar.name))
    # if kSyst=="Mass" and name=="etac2S" : mass.setVal(ccbar.mass_alt)
    # if kSyst=="Gamma" and name=="etac2S": gamma.setVal(ccbar.width_alt)

    radius = w.var("radius")
    proton_m = w.var("proton_m")

    #Fit etac

    if bw: mean = RooFit.RooConst(0)
    else: mean = mass

    brw = RooRelBreitWigner("brw_{}".format(ccbar.name), "brw", Jpsi_M, mass, gamma, spin, radius, proton_m, proton_m)

    resolution_1 = RooGaussian("gauss_{}_1".format(ccbar.name),"gauss_1 PDF", Jpsi_M, mean, sigma_1) #mass -> mean2s
    resolution_2 = RooGaussian("gauss_{}_2".format(ccbar.name),"gauss_2 PDF", Jpsi_M, mean, sigma_2) #mass -> mean2s

    f.Close()

    model = RooAddPdf()
    if bw:
        pdf_1 = RooFFTConvPdf("bwxg_{}_1".format(ccbar.name),"breit-wigner (X) gauss", Jpsi_M, brw, resolution_1)
        pdf_2 = RooFFTConvPdf("bwxg_{}_2".format(ccbar.name),"breit-wigner (X) gauss", Jpsi_M, brw, resolution_2)
    else:
        pdf_1 = resolution_1
        pdf_2 = resolution_2
        #pdf = RooGaussian("gauss_{}_1".format(ccbar.name),"gauss_1 PDF", Jpsi_M, mass, sigma_1) #mass -> mean2s

    model = RooAddPdf("model_{}".format(ccbar.name),"{} signal".format(ccbar.name), RooArgList(pdf_1,pdf_2), RooArgList(r_G1ToG2))
    getattr(w,"import")(model,RooFit.RecycleConflictNodes())

    return model


def fill_workspace_CB(w, name, kRange, ref="etac", kSyst="Base"):

    Jpsi_M = w.var("Jpsi_M{}".format(kRange))
    if Jpsi_M == None:
        Jpsi_M = w.var("Jpsi_M")

    ccbar = Particle(name)

    bw = False if (ccbar.width<1.e-3) else True

    ratio_sigma = 0.30
    ratio_ref = 1.1 #ratio sigma_ccbar 2 sigma_ref
    ratio_area = 0.90

    r_ref = RooRealVar("r_ref_{}".format(name),"r_ref", ratio_ref, 0., 3.)

    modName = "_CB"
    f = TFile("{}//MC/mass_fit/Wksp_M_res_all_{}_sim{}.root".format(homeDir,ref,modName))
    w_MC = f.Get("w")
    
    if ref==ccbar.name:
        al_1  = w_MC.var("alpha_1").getValV()
        al_2  = w_MC.var("alpha_2").getValV()
        ncb_1 = w_MC.var("nCB_1").getValV()
        ncb_2 = w_MC.var("nCB_2").getValV()

        alpha_1 = RooRealVar("alpha_1","alpha of CB", al_1)
        alpha_2 = RooRealVar("alpha_2","alpha of CB", al_2)
        nCB_1   = RooRealVar("nCB_1","n of CB", ncb_1)
        nCB_2   = RooRealVar("nCB_2","n of CB", ncb_2)

        
        sigma_1   = RooRealVar("sigma_{}_1".format(ccbar.name),"width of gaussian", 9., 0.1, 30.)
        sigma_scale = w_MC.var("r_ref_etac2S").getValV()
        f.Close()
    else:
        if name=="hc":
            r_ref_name = "r_ref_{}".format("chic1")
        else: 
            r_ref_name = "r_ref_{}".format(name)

        if kSyst != "SRatio":
            r_ref.setVal(w_MC.var(r_ref_name).getVal())
        else:
            r_ref.setVal(w_MC.var(r_ref_name).getVal()+w_MC.var(r_ref_name).getError())
        r_ref.setConstant(True)

        alpha_1 = w.var("alpha_1")
        alpha_2 = w.var("alpha_2")
        nCB_1   = w.var("nCB_1")
        nCB_2   = w.var("nCB_2")

        sigma_ref = w.var("sigma_{}_1".format(ref))
        sigma_1   = RooFormulaVar("sigma_{}_1".format(ccbar.name),"width of gaussian","@0*@1",RooArgList(RooArgSet(sigma_ref,r_ref)))
    
    f.Close()

    m = ccbar.mass_alt if kSyst=="Mass" else ccbar.mass
    mass  = w.var("mass_{}".format(ccbar.name))
    mass.setVal(m)
    width = ccbar.width_alt if kSyst=="Gamma" else ccbar.width
    gamma  = w.var("gamma_{}".format(ccbar.name))
    gamma.setVal(width)
    spin  = w.var("spin_{}".format(ccbar.name))

    radius = w.var("radius")
    proton_m = w.var("proton_m")

    #Fit etac

    if bw: mean = RooFit.RooConst(0)
    else: mean = mass

    brw = RooRelBreitWigner("brw_{}".format(ccbar.name), "brw",Jpsi_M, mass, gamma, spin,radius,proton_m,proton_m)
    resolution_1 = BifurcatedCB("cb_{}_1".format(ccbar.name), "Cystal Ball Function", Jpsi_M, mean, sigma_1, alpha_1, nCB_1, alpha_2, nCB_2)


    model = RooAddPdf()
    if bw:
        pdf_1 = RooFFTConvPdf("bwxg_{}_1".format(ccbar.name),"breit-wigner (X) gauss", Jpsi_M, brw, resolution_1)
    else:
        pdf_1 = resolution_1

    model = RooAddPdf("model_{}".format(ccbar.name),"{} signal".format(ccbar.name), RooArgList(pdf_1), RooArgList(RooFit.RooConst(1.)))
    getattr(w,"import")(model,RooFit.RecycleConflictNodes())
    return model



def combine_rel(w, name_list, kRange, kSource, kSyst="Base", split_lvl="source",kLim="etac2S"):

    kBkg = "Cheb" if (kSyst not in ["Exp","Sqrt","Sqr","ChebAlt"]) else kSyst

    if kRange=="":
        postfix = ""
    else:
        postfix = "_{}".format(kRange)

    if split_lvl=="all":
        Jpsi_M = w.var("Jpsi_M{}".format(postfix))
    else:
        Jpsi_M = w.var("Jpsi_M")

    if split_lvl=="none":
        postfix += "_{}".format(kSource)

    # define background model and yield
    a0 = RooRealVar("a0{}".format(postfix),"a0",0.0,-5.,5.) # !!! for prompt fit in big range initial value has to be 0
    a1 = RooRealVar("a1{}".format(postfix),"a1",0.1,-2.,2.)
    a2 = RooRealVar("a2{}".format(postfix),"a2",-0.01,-2.,2.)
    a3 = RooRealVar("a3{}".format(postfix),"a3",0.01,-2.,2.)
    a4 = RooRealVar("a4{}".format(postfix),"a4",-0.001,-2.,2.)
    a5 = RooRealVar("a5{}".format(postfix),"a5",0.001,-1.,1.)
    par_bkg = RooArgList(Jpsi_M, a0, a1, a2, a3, a4)

    bkg    = choose_bkg([kSource,kBkg], postfix, par_bkg)
    n_bkg  = RooRealVar("n_bkg{}".format(postfix),"num of etac", 4e4, 1, 5.e9)

    # define signal models and yields
    yields  = RooArgSet()
    ryields = RooArgSet()
    pdfs    = RooArgSet()

    yields_list = []    
    rels_list = []    
    models = []
    kNorm = name_list[0]

    # !!!!!!!UPDATE EFFICIENCY FILES FOR NORMALIZATION!!!!!!!
    if kNorm not in ["etac","jpsi","chic2","chic0"]:
        kNorm = "jpsi"

    for name in name_list:

        model = w.pdf(f"model_{name}")
        n_ccbar = RooRealVar()
        # ''' for relative yields '''
        # if name=="jpsi":
        if name in ["jpsi", kNorm]:
            n_ccbar = RooRealVar(f"n_{name}_{kSource}",f"num of {name}", 1e4, 0., 5.e6)
            # SetOwnership(n_ccbar, False) # to avoid crash in RooArgSet
            getattr(w,"import")(n_ccbar)
            yields_list.append(n_ccbar)
            
        # elif name!=kLim:
        else:
            n_ref = w.var(f"n_{kNorm}_{kSource}")
            n_rel = RooRealVar(f"n_{name}_{kSource}_rel",f"rel num of {name}", 1e0, 0., 2.e1)
            n_ccbar = RooFormulaVar(f"n_{name}_{kSource}",f"num of {name}","@0*@1", RooArgList(n_rel, n_ref))
            # getattr(w,"import")(n_rel)
            # getattr(w,"import")(n_ccbar)
            ryields.add(n_rel)
            rels_list.append(n_rel)
            yields_list.append(n_ccbar)
            # SetOwnership(n_ccbar, False) # to avoid crash in RooArgSet
        # else:
        #     # constraint on the mass and width
        #     ccbar = Particle(name)
        #     mass_const = RooRealVar("mass_{}_Obs".format(name),"mass_Obs", ccbar.mass)
        #     mass_const.setConstant(True)
        #     constr_mass   = RooGaussian("constr_mass","constr_mass", w.var("mass_{}".format(name)), mass_const, RooFit.RooConst(ccbar.mass_alt-ccbar.mass))
        #     getattr(w,"import")(constr_mass)
        #     if ccbar.width>1e-3: # around resolution value
        #         width_const = RooRealVar("width_{}_Obs".format(name),"width_Obs", ccbar.width)
        #         width_const.setConstant(True)
        #         constr_width  = RooGaussian("constr_width","constr_width", w.var("gamma_{}".format(name)), width_const, RooFit.RooConst(ccbar.width_alt-ccbar.width))
        #         getattr(w,"import")(constr_width)

        #     upd_meas(w, kLim, kNorm, kSyst, kSource)
        #     n_ccbar = w.function(f"n_{name}_{kSource}")
        #     # SetOwnership(n_ccbar, False) # to avoid crash in RooArgSet
        #     yields_list.append(n_ccbar)

        n_ccbar.Print()
        # SetOwnership(model, False) # to avoid crash in RooArgSet
        pdfs.add(model)
        yields.add(n_ccbar)
        models.append(model)

    # pdfs.Print()
    # yields.Print()
    # ryields.Print()

    n_jpsi = w.var(f"n_jpsi_{kSource}")
    eff_pppi0 = RooRealVar("eff_pppi0","eff_pppi0",0.052)
    if kSyst=="pppi0":
        eff_pppi0.setVal(0.055)
    pppi0 = RooGenericPdf("pppi0","Jpsi.pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))
    n_pppi0 = RooFormulaVar(f"n_pppi0_{kSource}","n_pppi0","@0*@1*(1.19/2.12)",RooArgList(n_jpsi,eff_pppi0))

    modelBkg    = RooAddPdf("modelBkg{}".format(postfix),"bkg", RooArgList(bkg), RooArgList(n_bkg))
    if kLim in name_list:
        constr_ext = w.pdf("constr_ext_{}".format(kSource))
        constr_mass = w.pdf("constr_mass")
        constr_width = w.pdf("constr_width")

        ''' add postfix _ext in case of recreating the model  '''
        # modelSignal  = RooAddPdf("modelSignal{}_ext".format(postfix),"{} signal".format(kRange), RooArgList(pdfs), RooArgList(yields))
        modelSignal  = RooAddPdf("modelSignal{}".format(postfix),"signal", RooArgList(pdfs), RooArgList(yields))
        yields.add(n_bkg)
        pdfs.add(bkg)
        if kRange!="High":
            yields.add(n_pppi0)
            pdfs.add(pppi0)
        # model        = RooAddPdf("model{}_ext".format(postfix),"model", RooArgList(pdfs), RooArgList(yields))
        # default!!!!

        modelSB = RooAddPdf("model{}".format(postfix),"model", RooArgList(pdfs), RooArgList(yields))

        prod = RooArgList(modelSB)
        prod.add(constr_ext)
        prod.add(constr_mass)
        if constr_width!=None:
            prod.add(constr_width)
        model_constr = RooProdPdf("model{}_constr".format(postfix),"model_constr", prod)
        # model        = RooAddPdf("model{}_base".format(postfix),"model_base", RooArgList(pdfs), RooArgList(yields))
        # model_constr = RooProdPdf("model{}".format(postfix),"model", RooArgList(model, constr_ext))
        # model_constr = RooAddPdf("model{}_constr".format(postfix),"model", RooArgList(pdfs), RooArgList(yields))
        getattr(w,"import")(model_constr,RooFit.RecycleConflictNodes())

    else:
        modelSignal = RooAddPdf("modelSignal{}".format(postfix),"signal", RooArgList(pdfs), RooArgList(yields))
        yields.add(n_bkg)
        pdfs.add(bkg)
        if kRange!="High":
            yields.add(n_pppi0)
            pdfs.add(pppi0)
        modelSB = RooAddPdf("model{}".format(postfix),"model", RooArgList(pdfs), RooArgList(yields))

    modelSB.Print()
    modelSignal.Print()
    w.Print()

    # getattr(w,"import")(model,RooFit.RecycleConflictNodes())
    # getattr(w,"import")(modelSignal,RooFit.RecycleConflictNodes())
    # getattr(w,"import")(modelBkg,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelSB,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelSignal,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelBkg,RooFit.RecycleConflictNodes())



def combine(w, name_list, kRange, kSource, kSyst="Base", split_lvl="source",kLim="etac2S"):

    kBkg = "Cheb" if (kSyst not in ["Exp","Sqrt","Sqr","ChebAlt"]) else kSyst

    if kRange=="":
        postfix = ""
    else:
        postfix = "_{}".format(kRange)

    if split_lvl=="all":
        Jpsi_M = w.var("Jpsi_M{}".format(postfix))
    else:
        Jpsi_M = w.var("Jpsi_M")

    if split_lvl=="none":
        postfix += "_{}".format(kSource)

    # define background model and yield
    # a0 = RooRealVar("a0{}".format(postfix),"a0",-0.5,-5.,5.) # !!! for prompt fit in big range initial value has to be 0
    # a1 = RooRealVar("a1{}".format(postfix),"a1",0.1,-5.,5.)
    # a2 = RooRealVar("a2{}".format(postfix),"a2",-0.01,-5.,5.)
    a0 = RooRealVar("a0{}".format(postfix),"a0",0.0,-5.,5.) # !!! for prompt fit in big range initial value has to be 0
    a1 = RooRealVar("a1{}".format(postfix),"a1",0.1,-2.,2.)
    a2 = RooRealVar("a2{}".format(postfix),"a2",-0.01,-2.,2.)
    a3 = RooRealVar("a3{}".format(postfix),"a3",0.01,-2.,2.)
    a4 = RooRealVar("a4{}".format(postfix),"a4",-0.001,-2.,2.)
    a5 = RooRealVar("a5{}".format(postfix),"a5",0.001,-1.,1.)
    par_bkg = RooArgList(Jpsi_M, a0, a1, a2, a3, a4)

    # if kBkg==kSyst:
        # a0.setRange(0., 30.)
        # a2.setRange(0., 1.)
        # a3.setRange(0., 1.)
        # a4.setRange(0., 1.)
        # if kBkg=="Exp":
        #     a1.setRange(0., 2.)

    bkg    = choose_bkg([kSource,kBkg], postfix, par_bkg)
    n_bkg  = RooRealVar("n_bkg{}".format(postfix),"num of etac", 4e4, 1, 5.e9)

    # define signal models and yields
    yields = RooArgSet()
    pdfs   = RooArgSet()

    yields_list = []    
    kNorm = name_list[0]

    # !!!!!!!UPDATE EFFICIENCY FILES FOR NORMALIZATION!!!!!!!
    if kNorm not in ["etac","jpsi","chic2"]:
        kNorm = "jpsi"

    for name in name_list:


        model = w.pdf("model_{}".format(name))
        n_ccbar = RooRealVar()
        # ''' for absoulute yields '''
        # if name!=kLim or kSource=="fromB":
        if name!=kLim:
            n_ccbar = RooRealVar(f"n_{name}_{kSource}","num of etac", 1e4, 0., 5.e6)
            # SetOwnership(n_ccbar, False) # to avoid crash in RooArgSet
            getattr(w,"import")(n_ccbar)
            yields_list.append(n_ccbar)
            
            # n_ccbar = w.var("n_{}{}".format(name,postfix))
        # ''' for relative yields '''
        # if name in [kNorm, "jpsi", "etac"]:
        #     n_ccbar = RooRealVar("n_{}{}".format(name,postfix),"num of etac", 1e4, 0., 5.e6)
        #     getattr(w,"import")(n_ccbar)
        #     SetOwnership(n_ccbar, False) # to avoid crash in RooArgSet
        # elif name!=kLim:
        #     n_ref = w.var("n_{}{}".format(kNorm,postfix))
        #     n_rel = RooRealVar("n_{}{}_rel".format(name,postfix),"num of etac", 1e0, 0., 2.e1)
        #     n_ccbar = RooFormulaVar("n_{}{}".format(name,postfix),"num of Etac","@0*@1", RooArgList(n_rel, n_ref))
        #     getattr(w,"import")(n_rel,RooFit.RecycleConflictNodes(True))
        #     getattr(w,"import")(n_ccbar,RooFit.RecycleConflictNodes(True))
        #     SetOwnership(n_ccbar, False) # to avoid crash in RooArgSet
        else:
            # constraint on the mass and width
            ccbar = Particle(name)
            mass_const = RooRealVar("mass_{}_Obs".format(name),"mass_Obs", ccbar.mass)
            mass_const.setConstant(True)
            constr_mass   = RooGaussian("constr_mass","constr_mass", w.var("mass_{}".format(name)), mass_const, RooFit.RooConst(ccbar.mass_alt-ccbar.mass))
            getattr(w,"import")(constr_mass)
            if ccbar.width>1e-3: # around resolution value
                width_const = RooRealVar("width_{}_Obs".format(name),"width_Obs", ccbar.width)
                width_const.setConstant(True)
                constr_width  = RooGaussian("constr_width","constr_width", w.var("gamma_{}".format(name)), width_const, RooFit.RooConst(ccbar.width_alt-ccbar.width))
                getattr(w,"import")(constr_width)

            upd_meas(w, kLim, kNorm, kSyst, kSource)
            n_ccbar = w.function(f"n_{name}_{kSource}")
            # SetOwnership(n_ccbar, False) # to avoid crash in RooArgSet
            yields_list.append(n_ccbar)

        n_ccbar.Print()
        SetOwnership(model, False) # to avoid crash in RooArgSet
        pdfs.add(model)
        yields.add(n_ccbar)

    pdfs.Print()
    yields.Print()

    n_jpsi = w.var("n_jpsi_{}".format(kSource))
    eff_pppi0 = RooRealVar("eff_pppi0","eff_pppi0",0.052)
    if kSyst=="pppi0":
        eff_pppi0.setVal(0.055)
    pppi0 = RooGenericPdf("pppi0","Jpsi.pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))
    n_pppi0 = RooFormulaVar("n_pppi0_{}".format(kSource),"n_pppi0","@0*@1*(1.19/2.12)",RooArgList(n_jpsi,eff_pppi0))

    modelBkg    = RooAddPdf("modelBkg{}".format(postfix),"bkg", RooArgList(bkg), RooArgList(n_bkg))
    if kLim in name_list:
        constr_ext = w.pdf("constr_ext_{}".format(kSource))
        constr_mass = w.pdf("constr_mass")
        constr_width = w.pdf("constr_width")

        ''' add postfix _ext in case of recreating the model  '''
        # modelSignal  = RooAddPdf("modelSignal{}_ext".format(postfix),"{} signal".format(kRange), RooArgList(pdfs), RooArgList(yields))
        modelSignal  = RooAddPdf("modelSignal{}".format(postfix),"signal", RooArgList(pdfs), RooArgList(yields))
        yields.add(n_bkg)
        pdfs.add(bkg)
        if kRange!="High":
            yields.add(n_pppi0)
            pdfs.add(pppi0)
        # model        = RooAddPdf("model{}_ext".format(postfix),"model", RooArgList(pdfs), RooArgList(yields))
        # default!!!!

        model        = RooAddPdf("model{}".format(postfix),"model", RooArgList(pdfs), RooArgList(yields))

        prod = RooArgList(model)
        prod.add(constr_ext)
        prod.add(constr_mass)
        if constr_width!=None:
            prod.add(constr_width)
        model_constr = RooProdPdf("model{}_constr".format(postfix),"model_constr", prod)
        # model        = RooAddPdf("model{}_base".format(postfix),"model_base", RooArgList(pdfs), RooArgList(yields))
        # model_constr = RooProdPdf("model{}".format(postfix),"model", RooArgList(model, constr_ext))
        # model_constr = RooAddPdf("model{}_constr".format(postfix),"model", RooArgList(pdfs), RooArgList(yields))
        getattr(w,"import")(model_constr,RooFit.RecycleConflictNodes())

    else:
        modelSignal = RooAddPdf("modelSignal{}".format(postfix),"signal", RooArgList(pdfs), RooArgList(yields))
        yields.add(n_bkg)
        pdfs.add(bkg)
        if kRange!="High":
            yields.add(n_pppi0)
            pdfs.add(pppi0)
        model       = RooAddPdf("model{}".format(postfix),"model", RooArgList(pdfs), RooArgList(yields))


    # getattr(w,"import")(model,RooFit.RecycleConflictNodes())
    # getattr(w,"import")(modelSignal,RooFit.RecycleConflictNodes())
    # getattr(w,"import")(modelBkg,RooFit.RecycleConflictNodes())
    getattr(w,"import")(model,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelSignal,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelBkg,RooFit.RecycleConflictNodes())


def create_sample_sim(w, ext=False):

    Jpsi_M = w.var("Jpsi_M")


    for kSource in ["prompt","fromB"]:
        if ext:
            # in case of extended pdf rename model to avoid a conflict when drawing
            model = w.pdf("model_{}".format(kSource))
            model.SetName("model_{}_base".format(kSource))
            model.SetTitle("model_{}_base".format(kSource))
            model.Print()

            model = w.pdf("model_{}_constr".format(kSource))
            model.SetName("model_{}".format(kSource))
            model.SetTitle("model_{}".format(kSource))
            model.Print()

            # modelE_High = w.pdf("model_High_ext")

            # modelS = w.pdf("modelSignal_{}_ext".format(kSource))
            # modelS.SetName("modelSignal_{}".format(kSource))
            # modelS.SetTitle("modelSignal_{}".format(kSource))

    model_fb = w.pdf("model_fromB")
    model_pr = w.pdf("model_prompt")
    model_fb.Print()
    model_pr.Print()

    sample = RooCategory("sample","sample")
    sample.defineType("prompt")
    sample.defineType("fromB")

    Jpsi_M.setRange("fitRange_prompt", minM_Low, maxM_High)
    Jpsi_M.setRange("fitRange_fromB", minM_Low, maxM_High)

    data_pr = w.data("dh_prompt")
    data_fb = w.data("dh_fromB")


    # Construct combined dataset in (Jpsi_M,sample)
    combData = RooDataHist("combData", "combined data", RooArgList(Jpsi_M), RooFit.Index(sample), RooFit.Import("prompt",data_pr), RooFit.Import("fromB",data_fb))

    constr_ext_prompt = w.pdf("constr_ext_prompt")
    constr_ext_fromB = w.pdf("constr_ext_fromB")

    constr_mass = w.pdf("constr_mass")
    constr_width = w.pdf("constr_width")

    # # Associate model with the physics state and model_ctl with the control state
    # simPdf_base = RooSimultaneous("simPdf_base","simultaneous signal pdf",sample)
    # simPdf_base.addPdf(model_pr,"prompt")
    # simPdf_base.addPdf(model_fb,"fromB")


    # Associate model with the physics state and model_ctl with the control state
    simPdf = RooSimultaneous("simPdf","simultaneous signal pdf",sample)
    simPdf.addPdf(model_pr,"prompt")
    simPdf.addPdf(model_fb,"fromB")

    if constr_width==None and constr_mass==None:
        simPdf_constr = RooProdPdf("simPdf_constr","simPdf_constr", RooArgList(simPdf, constr_ext_prompt, constr_ext_fromB))
    elif constr_width==None:
        simPdf_constr = RooProdPdf("simPdf_constr","simPdf_constr", RooArgList(simPdf, constr_ext_prompt, constr_ext_fromB, constr_mass))
    elif constr_mass==None:
        simPdf_constr = RooProdPdf("simPdf_constr","simPdf_constr", RooArgList(simPdf, constr_ext_prompt, constr_ext_fromB, constr_width))
    else:
        simPdf_constr = RooProdPdf("simPdf_constr","simPdf_constr", RooArgList(simPdf, constr_ext_prompt, constr_ext_fromB, constr_mass, constr_width))

    #   getattr(w,"import")(model,RecycleConflictNodes())
    getattr(w,"import")(combData, RooFit.RecycleConflictNodes())
    getattr(w,"import")(simPdf, RooFit.RecycleConflictNodes())
    getattr(w,"import")(simPdf_constr, RooFit.RecycleConflictNodes())


def create_sample_split(w):

    Jpsi_M = w.var("Jpsi_M")

    model_l_pr = w.pdf("model_Low_prompt")
    model_l_fb = w.pdf("model_Low_fromB")
    # model_h_pr = w.pdf("model_High_prompt")
    # model_h_fb = w.pdf("model_High_fromB")
    model_h_pr = w.pdf("model_High_prompt_constr")
    model_h_fb = w.pdf("model_High_fromB_constr")
    model_l_pr.Print()
    model_l_fb.Print()
    model_h_pr.Print()
    model_h_fb.Print()

    sample = RooCategory("sample","sample")
    sample.defineType("Low_prompt")
    sample.defineType("Low_fromB")
    sample.defineType("High_prompt")
    sample.defineType("High_fromB")

    Jpsi_M.setRange("fitRange_Low_prompt", minM_Low, maxM_Low)
    Jpsi_M.setRange("fitRange_Low_fromB", minM_Low, maxM_Low)
    Jpsi_M.setRange("fitRange_High_prompt", minM_High, maxM_High)
    Jpsi_M.setRange("fitRange_High_fromB", minM_High, maxM_High)

    data_l_pr = w.data("dh_Low_prompt")
    data_l_fb = w.data("dh_Low_fromB")
    data_h_pr = w.data("dh_High_prompt")
    data_h_fb = w.data("dh_High_fromB")


    # Construct combined dataset in (Jpsi_M,sample)
    combData = RooDataHist("combData", "combined data", RooArgList(Jpsi_M), RooFit.Index(sample), RooFit.Import("High_prompt",data_h_pr), RooFit.Import("High_fromB",data_h_fb), RooFit.Import("Low_prompt",data_l_pr), RooFit.Import("Low_fromB",data_l_fb))

    constr_ext_prompt = w.pdf("constr_ext_prompt")
    constr_ext_fromB = w.pdf("constr_ext_fromB")

    constr_mass = w.pdf("constr_mass")
    constr_width = w.pdf("constr_width")

    # # Associate model with the physics state and model_ctl with the control state
    # simPdf_base = RooSimultaneous("simPdf_base","simultaneous signal pdf",sample)
    # simPdf_base.addPdf(model_pr,"prompt")
    # simPdf_base.addPdf(model_fb,"fromB")


    # Associate model with the physics state and model_ctl with the control state
    simPdf = RooSimultaneous("simPdf","simultaneous signal pdf",sample)
    simPdf.addPdf(model_l_pr,"Low_prompt")
    simPdf.addPdf(model_l_fb,"Low_fromB")
    simPdf.addPdf(model_h_pr,"High_prompt")
    simPdf.addPdf(model_h_fb,"High_fromB")

    # if constr_width==None and constr_mass==None:
    #     simPdf_constr = RooProdPdf("simPdf_constr","simPdf_constr", RooArgList(simPdf, constr_ext_prompt, constr_ext_fromB))
    # elif constr_width==None:
    #     simPdf_constr = RooProdPdf("simPdf_constr","simPdf_constr", RooArgList(simPdf, constr_ext_prompt, constr_ext_fromB, constr_mass))
    # elif constr_mass==None:
    #     simPdf_constr = RooProdPdf("simPdf_constr","simPdf_constr", RooArgList(simPdf, constr_ext_prompt, constr_ext_fromB, constr_width))
    # else:
    #     simPdf_constr = RooProdPdf("simPdf_constr","simPdf_constr", RooArgList(simPdf, constr_ext_prompt, constr_ext_fromB, constr_mass, constr_width))

    #   getattr(w,"import")(model,RecycleConflictNodes())
    getattr(w,"import")(combData, RooFit.RecycleConflictNodes())
    getattr(w,"import")(simPdf, RooFit.RecycleConflictNodes())
    # getattr(w,"import")(simPdf_constr, RooFit.RecycleConflictNodes())

