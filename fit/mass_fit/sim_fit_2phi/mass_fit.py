from ROOT import *
from ROOT.RooFit import *
from ROOT.RooStats import *

from model import *
import drawModule_splitFit as dm
gROOT.LoadMacro("/users/LHCb/zhovkovska/etac2s_ppbar/fit/libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")

import numpy as np

#homeDir = "/afs/cern.ch/user/v/vazhovko/private/etac2s_ppbar/"
#dataDir = "/eos/user/v/vazhovko/etac2s_ppbar/"
homeDir = "/users/LHCb/zhovkovska/scripts/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"


def get_data_h(w, kSource):

    nameWksp = dataDir + "/wksps/wksp_data_{}_misID.root".format(kSource)
    file_w = TFile(nameWksp)
    w_in = file_w.Get("w")

    data   = w_in.data("dh")
    Jpsi_M = w_in.var("Jpsi_M")

    hist = data.createHistogram("hist",Jpsi_M, RooFit.Binning(20*dm.binN_phiphi, dm.minM_phiphi, dm.maxM_phiphi))

    Jpsi_M  = w.var("Jpsi_M")
    # Jpsi_M  = RooRealVar("Jpsi_M_ppbar", "Jpsi_M_ppbar",  dm.minM_Low, dm.maxM_High)

    dh  = RooDataHist("dh_ppbar","dh_ppbar",  RooArgList(Jpsi_M), hist)
    getattr(w,"import")(dh)

    print("INFO: PPbar DataHists imported")

def get_all_histo(w, years, kSource):

    hist = TH1D('hist','hist', dm.binN_phiphi, dm.minM_phiphi, dm.maxM_phiphi)
    hlist = TList()

    for year in years:
        histFName = homeDir + 'etac2s_phiphi/results/oldTrigg/all_fixed_5MeV/hist_Pure_{}_{}.root'.format(kSource, year)
        histName  = "hist_Pure_{}".format(kSource)
        file_hist = TFile(histFName,"read")
        hh        = TH1D()
        hh        = file_hist.Get(histName)
        hh.SetDirectory(0)
        hlist.Add(hh)
        file_hist.Close()

    hist.Merge(hlist)

    Jpsi_M  = w.var("Jpsi_M")
    # Jpsi_M = RooRealVar('Jpsi_M_phiphi',"Jpsi_M", minMass_phiphi, maxMass_phiphi)

    dh = RooDataHist("dh_phiphi","dh", RooArgList(Jpsi_M), hist)
    getattr(w,'import')(dh)

    print("INFO: PhiPhi DataHists imported")

def perform_fit(kSource,kSyst,kNorm="jpsi" ):

    # kSyst = kBkg.capitalize()

    homeDir = "/users/LHCb/zhovkovska/scripts/"
    gStyle.SetStatFormat("4.3f");
    #gStyle.SetOptTitle(0)


    w = RooWorkspace("w",True)

    Jpsi_M = RooRealVar('Jpsi_M',"Jpsi_M", dm.minM_phiphi, dm.maxM_phiphi)
    Jpsi_M.setBins(5*dm.binN_phiphi)
    getattr(w,'import')(Jpsi_M)

    get_all_histo(w,[2015, 2016, 2017, 2018], kSource)
    get_data_h(w,kSource)

    ppbar_states = ["etac", "jpsi", "chic0","chic1","chic2","psi2S", "etac2S"]#,"hc"]
    phiphi_states = ["etac", "chic0","chic1","chic2","etac2S","psi3930"] #"psi3860", "psi3872",

    all_states = list(ppbar_states)
    all_states.extend(x for x in phiphi_states if x not in ppbar_states)

    setMW(w, all_states)

    kCB = False if kSyst!="CB" else True
    kBkg = "Cheb" if kSyst!="Exp" else kSyst

    for ccbar in ppbar_states:
        fill_workspace(w, ccbar, "ppbar", ref=kNorm, kCB=kCB)

    combine(w, ppbar_states, "ppbar", kSource, kBkg)

    for ccbar in phiphi_states:
        fill_workspace(w, ccbar, "phiphi", ref=kNorm, kCB=kCB)

    combine(w, phiphi_states, "phiphi", kSource, kBkg)

    create_sample(w)

    model_ppbar = w.pdf("model_ppbar")
    model_phiphi = w.pdf("model_phiphi")

    dh_phiphi = w.data("dh_phiphi")
    dh_ppbar = w.data("dh_ppbar")

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf")
    combData = w.data("combData")

    llist = RooLinkedList()
    llist.Add(RooCmdArg(RooFit.Extended(True)))
    llist.Add(RooCmdArg(RooFit.Save(True)))
    llist.Add(RooCmdArg(RooFit.Offset(True)))
    llist.Add(RooCmdArg(RooFit.NumCPU(8)))
    # llist.Add(RooCmdArg(RooFit.Strategy(2)))

    llist_local = llist.Clone()
    llist_local.Add(RooCmdArg(RooFit.Range("fitRange_phiphi")))

    model_phiphi.chi2FitTo(dh_phiphi, llist_local)
    model_phiphi.chi2FitTo(dh_phiphi, llist_local)
    # model_phiphi.fitTo(dh_phiphi, RooFit.Extended(True), RooFit.Save(True), RooFit.Offset(True), RooFit.NumCPU(8), RooFit.Range("fitRange_phiphi"))
    # model_phiphi.fitTo(dh_phiphi, RooFit.Extended(True), RooFit.Save(True), RooFit.Offset(True), RooFit.NumCPU(8), RooFit.Range("fitRange_phiphi"))
    # input()
    # w.var("sigma_etac_1_phiphi").setConstant(True)
    # model_ppbar.fitTo(dh_ppbar, RooFit.Extended(True), RooFit.Save(True), RooFit.Offset(True), RooFit.NumCPU(8), RooFit.Range("fitRange_ppbar"))
    # model_ppbar.fitTo(dh_ppbar, RooFit.Extended(True), RooFit.Save(True), RooFit.Offset(True), RooFit.NumCPU(8), RooFit.Range("fitRange_ppbar"))
    model_ppbar.fitTo(dh_ppbar, RooFit.Extended(True), RooFit.Save(True), RooFit.Offset(True), RooFit.NumCPU(8), RooFit.Range("fitRange_ppbar"))
    model_ppbar.fitTo(dh_ppbar, RooFit.Extended(True), RooFit.Save(True), RooFit.Offset(True), RooFit.NumCPU(8), RooFit.Range("fitRange_ppbar"))
    # w.var("sigma_etac_1_ppbar").setVal(8.237)
    # w.var("sigma_etac_1_ppbar").setConstant(True)
    # input()
    # w.var("n_jpsi_ppbar").setVal(2.515e4)
    # w.var("n_jpsi_ppbar").setConstant(True)



    # var_list_0 = model_ppbar.getVariables()
    # # var_list_0 = w.pdf("modelBkg_prompt").getVariables()
    # nuis_iter = var_list_0.createIterator()
    # var = nuis_iter.Next()
    # while var:
    #     var.setConstant(True)
    #     var = nuis_iter.Next()

    llist.Add(RooCmdArg(RooFit.Range("fitRange")))
    llist.Add(RooCmdArg(RooFit.SplitRange(True)))
    # llist.Add(RooCmdArg(RooFit.Minimizer("Minuit2","Migrad")))
    # llist.Add(RooCmdArg(RooFit.SumCoefRange("fitRange")))
    llist.Add(RooCmdArg(RooFit.AsymptoticError(True)))

    # llist.Add(RooCmdArg(RooFit.SumW2Error(True)))

    fitresult = simPdf.fitTo(combData, llist)
    w.var("gamma_chic0").setConstant(False)
    # w.var("gamma_etac2S").setConstant(False)
    # w.var("gamma_etac").setConstant(False)
    w.var("mass_etac").setConstant(False)
    w.var("mass_jpsi").setConstant(False)
    w.var("mass_chic0").setConstant(False)
    w.var("mass_etac2S").setConstant(False)
    w.var("mass_chic1").setConstant(False)
    w.var("mass_chic2").setConstant(False)
    # w.var("sigma_{}_1".format(kNorm)).setConstant(False)
    # llist.Add(RooCmdArg(RooFit.Minos(True)))
    fitresult = simPdf.fitTo(combData, llist)
    fitresult = simPdf.fitTo(combData, llist)
    # fitresult = simPdf.fitTo(combData, llist)



    # # Minimize likelihood w.r.t all parameters before making plots
    # m = RooMinuit(nll)
    # #m = RooMinimizer(nll)
    # m.migrad()
    # m.hesse()
    #m.minos()



    frame_pr, resid_pr, pull_pr = dm.set_frame(w,kRange="ppbar", kSource="")
    frame_fb, resid_fb, pull_fb = dm.set_frame(w,kRange="phiphi", kSource="")

    names  = ["ppbar","phiphi"]
    frames = [frame_pr,frame_fb]
    resids = [resid_pr,resid_fb]
    pulls  = [pull_pr,pull_fb]
    c = dm.set_canvas(names, frames, resids, pulls)


    nameWksp = homeDir + "etac2s_ppbar/results/mass_fit/sim_2phi/Wksp_{}_{}_{}.root".format(kNorm,kSource,kSyst)
    namePic = homeDir + "etac2s_ppbar/results/mass_fit/sim_2phi/Plot_{}_{}_{}.pdf".format(kNorm,kSource,kSyst)
    nameTxt = homeDir + "etac2s_ppbar/results/mass_fit/sim_2phi/Result_{}_{}_{}.txt".format(kNorm,kSource,kSyst)


    c.SaveAs(namePic)
    w.writeToFile(nameWksp)

    import os, sys
    save = os.dup( sys.stdout.fileno() )
    newout = file(nameTxt, "w" )
    os.dup2( newout.fileno(), sys.stdout.fileno() )
    fitresult.Print("v")
    fitresult.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() )
    newout.close()


    print("fit performed successfully")

    return w

def contour_plot(w, ccbar='etac'):

    ''' Drawing gamma vs mass contour plot '''
    
    # pdg = RooEllipse("PDG 2020",2983.9,32.0,0.5, 0.7)
    # pdg = RooEllipse("PDG 2020",3414.71,10.8,0.30, 0.6)
    # pdg = RooEllipse("PDG 2020",3637.5,11.3,1.1, 3.2)
    #pdg.Draw()
    Jpsi_M = w.var("Jpsi_M")

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf")
    combData = w.data("combData")

    sigma_phiphi = w.var("sigma_{}_1_phiphi".format(ccbar))
    sigma_ppbar = w.var("sigma_{}_1_ppbar".format(ccbar))
    sigma_phiphi.setConstant(False)
    sigma_ppbar.setConstant(False)

    nll = simPdf.createNLL(combData, RooFit.NumCPU(48), RooFit.Extended(True), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True)) 
    m = RooMinuit(nll) 
    # m.migrad()
    # m.hesse()
    # m.minos(RooArgSet(gamma))
    # m.minos(RooArgSet(mass))
    fCont = m.contour(sigma_phiphi,sigma_ppbar,1,2,3)
    # fCont.addPlotable(pdg) #/, "same"/
    
    
    cCont = TCanvas("ContourPlor","Masses Fit",500,400)
    cCont.cd()
    gPad.SetLeftMargin(0.15) 
    # fCont.GetXaxis().SetRangeUser(2980., 2988.)
    # fCont.GetYaxis().SetRangeUser(25., 40.)
    # fCont.GetXaxis().SetRangeUser(3400., 3430.)
    # fCont.GetXaxis().SetRangeUser(3630., 3650.)
    # fCont.GetXaxis().SetTitle("M_{#eta_{c}(2S)}, [MeV]") 
    # fCont.GetYaxis().SetTitle("#Gamma_{#eta_{c}(2S)}, [MeV]") 
    # fCont.GetXaxis().SetTitle("M_{#chi_{c0}}, [MeV]") 
    # fCont.GetYaxis().SetTitle("#Gamma_{#chi_{c0}}, [MeV]") 
    # fCont.GetXaxis().SetTitle("#sigma, [MeV]") 
    fCont.Draw("SAME")
    # pdg.SetLineColor(8)
    # pdg.DrawClone("SAME")

    cCont.SaveAs(homeDir + "etac2s_ppbar/results/mass_fit/sim_2phi/Contour_sigma.pdf")
    return cCont

def just_plot(w, kSource, kSyst, kNorm):

    frame_pr, resid_pr, pull_pr = dm.set_frame(w,kRange="ppbar", kSource="")
    frame_fb, resid_fb, pull_fb = dm.set_frame(w,kRange="phiphi", kSource="")

    names  = ["ppbar","phiphi"]
    frames = [frame_pr,frame_fb]
    resids = [resid_pr,resid_fb]
    pulls  = [pull_pr,pull_fb]
    c = dm.set_canvas(names, frames, resids, pulls)


    nameWksp = homeDir + "etac2s_ppbar/results/mass_fit/sim_2phi/Wksp_{}_{}_{}.root".format(kNorm,kSource,kSyst)
    namePic = homeDir + "etac2s_ppbar/results/mass_fit/sim_2phi/Plot_{}_{}_{}.pdf".format(kNorm,kSource,kSyst)
    nameTxt = homeDir + "etac2s_ppbar/results/mass_fit/sim_2phi/Result_{}_{}_{}.txt".format(kNorm,kSource,kSyst)


    c.SaveAs(namePic)


def plot_sigmaLL(w):

    model_ppbar = w.pdf("model_ppbar")
    model_phiphi = w.pdf("model_phiphi")

    dh_phiphi = w.data("dh_phiphi")
    dh_ppbar = w.data("dh_ppbar")

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf")
    combData = w.data("combData")

    sigma = w.var("sigma_etac_1_phiphi")

    nll = simPdf.createNLL(combData, RooFit.NumCPU(8), RooFit.Extended(True), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True))
    nll_part = model_phiphi.createNLL(dh_phiphi, RooFit.NumCPU(8), RooFit.Extended(True), RooFit.Offset(True), RooFit.Range("fitRange_phiphi"))
    # Plot likelihood scan frac
    frameRS = sigma.frame(RooFit.Bins(50))
#     nll.plotOn(frameRS,RooFit.ShiftToZero())

    # The profile likelihood estimator on nll for frac will minimize nll w.r.t
    # all floating parameters except frac for each evaluation

    #pll_N_etac2S = nll.createProfile(RooArgSet(n_etac2S_rel))
    pll_N = RooProfileLL("pll_N", "pll_N", nll, RooArgSet(sigma))
    pll_N_part = RooProfileLL("pll_N", "pll_N", nll_part, RooArgSet(sigma))
    llhood = RooFormulaVar("llhood","exp(-0.5*pll_N)",RooArgList(pll_N));

    # Plot the profile likelihood in frac
    pll_N.plotOn(frameRS,RooFit.LineColor(2))
    pll_N_part.plotOn(frameRS,RooFit.LineColor(3))

    # Adjust frame maximum for visual clarity
    frameRS.SetMinimum(0)

    cNLL = TCanvas("rf605_profilell","rf605_profilell",800, 400)
    cNLL.cd(),  gPad.SetLeftMargin(0.15),  frameRS.GetYaxis().SetTitleOffset(1.4),  frameRS.Draw()

    cNLL.SaveAs(homeDir + "etac2s_ppbar/results/mass_fit/sim_2phi/NLL_sigma_phiphi.pdf")

''' _base histos: only PT, tz and PID (all 0.6) cuts '''

if __name__ == "__main__":

    # kSysts = ["Exp","Gamma","eff","CB","BR","cross_talk","Resolution"]
    kSysts = ["Base"]
    kSources = ["fromB"]
    kNorm = 'etac'
    for kSyst in kSysts:
        for kSource in kSources:
            w = perform_fit(kSource, kSyst, kNorm)
            # nameWksp = homeDir + "etac2s_ppbar/results/mass_fit/sim_2phi/Wksp_{}_{}_{}.root".format(kNorm,kSource,kSyst)
            # file_w = TFile(nameWksp)
            # w = file_w.Get("w")
            # plot_sigmaLL(w)
            # contour_plot(w,kNorm)
            # just_plot(w, kSource, kSyst, kNorm)
            # calcUL(w,kSource,kSyst,kNorm)
            del w

