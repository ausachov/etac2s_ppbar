#from ROOT import gROOT, gStyle
#from ROOT import TFile, TChain, TTree, TH1D, TGraph, TCanvas, TCut, TMath
#from ROOT import RooFit, RooStats
from ROOT import *
import ROOT.RooFit as RF
from ROOT.RooStats import *
gROOT.LoadMacro("/users/LHCb/zhovkovska/etac2s_ppbar/fit/libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("/users/LHCb/zhovkovska/etac2s_ppbar/fit/libs/lhcbStyle.C")

binWidth = 0.25
binWidthDraw = 5.0

minM_Low = 2850.
maxM_Low = 3250.
nBins_Low = int((maxM_Low-minM_Low)/binWidth)
binN_Low  = int((maxM_Low-minM_Low)/binWidthDraw)

minM_High = 3320.
maxM_High = 3780.
nBins_High = int((maxM_High-minM_High)/binWidth)
binN_High  = int((maxM_High-minM_High)/binWidthDraw)

minM_phiphi = 2800.
maxM_phiphi = 4000.
# minM_phiphi = 2850.
# maxM_phiphi = 3780.
minM_ppbar = 2850.
maxM_ppbar = 3780.

binN_Tot     = int((maxM_High-minM_Low)/binWidthDraw)
binN_Tot_Fit = int((maxM_High-minM_Low)/binWidth)
binN_phiphi  = int((maxM_phiphi-minM_phiphi)/binWidthDraw)
# binN_phiphi_Fit = int((maxM_phiphi-minM_phiphi)/binWidth)

binning      = RF.Binning(binN_Tot, minM_Low, maxM_High)
binning_low  = RF.Binning(binN_Low, minM_Low, maxM_Low)
binning_high = RF.Binning(binN_High, minM_High, maxM_High)
binning_ppbar = RF.Binning(binN_Tot, minM_ppbar, maxM_ppbar)
binning_2phi  = RF.Binning(binN_phiphi, minM_phiphi, maxM_phiphi)

range_tot  = RF.Range(minM_Low,maxM_High)
range_low  = RF.Range(minM_Low,maxM_Low)
range_high = RF.Range(minM_High,maxM_High)
range_ppbar   = RF.Range(minM_ppbar,maxM_ppbar)
range_phiphi  = RF.Range(minM_phiphi,maxM_phiphi)

mrkSize = RF.MarkerSize(0.5)
lineWidth1 = RF.LineWidth(1)
lineWidth2 = RF.LineWidth(2)
lineStyle1 = RF.LineStyle(2)
lineStyle2 = RF.LineStyle(9)
lineColor1 = RF.LineColor(6)
lineColor2 = RF.LineColor(8)
rfname = RF.Name
rfcut = RF.Cut

optDic = {
    "Low":      ["J/#psi to p#bar{p}", binning_low, range_low ],
    "High":     ["#eta_c(2S) to p#bar{p}", binning_high, range_high],
    "":         ["c#bar{c} to p#bar{p}", binning, range_tot],
    "prompt":   ["c#bar{c} to p#bar{p} prompt", binning, range_tot ],
    "fromB":    ["c#bar{c} to p#bar{p} fromB", binning, range_tot ],
    "ppbar":    ["c#bar{c} to p#bar{p}", binning_ppbar, range_ppbar ],
    "phiphi":   ["c#bar{c} to #phi#phi", binning_2phi, range_phiphi ],
}

def draw_range(w,kRange):

    if kRange=="Low":
        Jpsi_M = w.var("Jpsi_M_Low")
    if kRange=="High":
        Jpsi_M = w.var("Jpsi_M_High")


    model       = w.pdf("model_{}".format(kRange))
    modelBkg    = w.pdf("modelBkg_{}".format(kRange))
    modelSignal = w.pdf("modelSignal_{}".format(kRange))

    dh = w.data("dh_{}".format(kRange))

    ##draw range separately

    frame = Jpsi_M.frame(RF.Title(optDic[kRange][0]), RF.Range("fitRange_{}".format(kRange)))

    dh.plotOn(frame, optDic[kRange][1], mrkSize, lineWidth1, rfname("data_{}".format(kRange)))

    argset = RooArgSet(Jpsi_M)
    n_events = model.createIntegral(argset, RF.NormSet(argset), RF.Range("fitRange_{}".format(kRange))).getValV()
    model.plotOn(frame, lineWidth1, RF.Normalization(n_events,RooAbsReal.RelativeExpected), rfname("mod_{}".format(kRange)))
    modelSignal.plotOn(frame, lineWidth1, lineColor2, lineStyle1, rfname("sig_{}".format(kRange)), RF.Normalization(1.,RooAbsReal.RelativeExpected))

    n_events_bkg = modelBkg.createIntegral(argset, RF.NormSet(argset), RF.Range("fitRange_{}".format(kRange))).getValV()
    modelBkg.plotOn(frame, lineWidth1, lineColor1, lineStyle1, RF.Normalization(0.,RooAbsReal.RelativeExpected))
    modelBkg.plotOn(frame, lineWidth1, lineColor1, lineStyle1, RF.Normalization(n_events_bkg,RooAbsReal.RelativeExpected), rfname("bkg_{}".format(kRange)))

    frame.GetYaxis().SetTitleOffset(1.5)
    frame.GetXaxis().SetTitleOffset(0.75)
    frame.GetXaxis().SetTitle("M(p#bar{p}) / [MeV/c^{2}]")
    frame.GetXaxis().SetTitleSize(0.045)
    frame.GetXaxis().SetTitleFont(12)
    frame.SetMinimum(-10.5)
    #frame.Draw()

    h_resid = frame.residHist("data_{}".format(kRange),"mod_{}".format(kRange))
    h_resid.SetMarkerSize(0.5)
    resid = Jpsi_M.frame(RF.Title(" "),RF.Bins(100), RF.Range("fitRange_{}".format(kRange)))
    resid.addPlotable(h_resid,"B")
    modelSignal.plotOn(resid, lineWidth1, rfname("sig_{}".format(kRange)))#, RF.Normalization(8.83177e+07,RooAbsReal.NumEvent))

    resid.GetXaxis().SetTitle("M(p#bar{p}) / [MeV/c^{2}]")
    resid.GetXaxis().SetTitleFont(12)
    #resid.Draw()

    h_pull = frame.pullHist("data_{}".format(kRange),"mod_{}".format(kRange))
    h_pull.SetMarkerSize(0.5)
    pull = Jpsi_M.frame(RF.Title(" "),RF.Bins(100), RF.Range("fitRange_{}".format(kRange)))
    for ii in range(h_pull.GetN()):
        h_pull.SetPointEYlow(ii,0)
        h_pull.SetPointEYhigh(ii,0)
    pull.SetMinimum(-4.0)
    pull.SetMaximum(4.0)
    pull.GetXaxis().SetTitleSize(0.0)
    pull.GetXaxis().SetLabelSize(0.06)
    pull.GetYaxis().SetLabelSize(0.06)
    pull.addPlotable(h_pull,"B")
    #pull.Draw()

    return frame, resid, pull





def set_frame(w,kRange="",kSource=""):

    postFix = ""
    if kRange!="":
        postFix += "_{}".format(kRange)
    if kSource!="":
        postFix += "_{}".format(kSource)

    Jpsi_M = w.var("Jpsi_M{}".format(postFix))
    if Jpsi_M == None:
        Jpsi_M = w.var("Jpsi_M")


    modelSignal = w.pdf("modelSignal{}".format(postFix))
    dh  = w.data("dh")
    if dh==None:    
        dh  = w.data("dh{}".format(postFix))
    totalPdf  = w.pdf("model{}".format(postFix))
    signalPdf = w.pdf("modelSignal{}".format(postFix))


    ##draw low and high separately

    frame = Jpsi_M.frame(RF.Title(optDic[kRange][0]), RF.Range("fitRange{}".format(postFix)))

    dh.plotOn(frame, RF.RefreshNorm(), optDic[kRange][1], mrkSize, lineWidth1, rfname("data{}".format(postFix)))

    totalPdf.plotOn(frame, lineWidth1, RF.NormRange("fitRange{}".format(postFix)), RF.Range("fitRange{}".format(postFix)), rfname("mod{}".format(postFix)))
    # totalPdf.plotOn(frame, lineWidth1, rfname("mod{}".format(postFix)))
    modelSignal.plotOn(frame, lineWidth1, lineColor2, lineStyle1, rfname("sig{}".format(postFix)), RF.Normalization(1.,RooAbsReal.RelativeExpected))

    n_bkg = w.var("n_bkg{}".format(postFix)).getValV()
    totalPdf.plotOn(frame, lineWidth1, lineColor1, lineStyle1, RF.Normalization(0.,RooAbsReal.RelativeExpected))
    totalPdf.plotOn(frame, RF.Components("bkg{}".format(postFix)), RF.NormRange("fitRange{}".format(postFix)), RF.Range("fitRange{}".format(postFix)), lineWidth1, lineColor1, lineStyle1, rfname("bkg{}".format(postFix)))
    # totalPdf.plotOn(frame, RF.Components("bkg{}".format(postFix)), lineWidth1, lineColor1, lineStyle1, rfname("bkg{}".format(postFix)))


    frame.GetYaxis().SetTitleOffset(0.55)
    frame.GetYaxis().SetTitle("Candidate / ({} MeV)]".format(binWidthDraw))
    frame.GetYaxis().SetTitleSize(0.085)
    frame.GetXaxis().SetTitleOffset(0.75)
    frame.GetXaxis().SetTitle("M(p#bar{p}) / [MeV]")
    frame.GetXaxis().SetTitleSize(0.085)
    frame.GetYaxis().SetTitleFont(12)
    frame.GetXaxis().SetTitleFont(12)
    frame.SetMinimum(-10.5)
    frame.Draw()


    #print("check")
    h_resid = frame.residHist("data{}".format(postFix),"bkg{}".format(postFix))
    h_resid.SetMarkerSize(0.5)
    resid = Jpsi_M.frame(RF.Title(" "), RF.Bins(100), RF.Range("fitRange{}".format(postFix)))
    resid.addPlotable(h_resid,"P")
    modelSignal.plotOn(resid, lineWidth1, rfname("sig{}".format(postFix)))#, RF.Normalization(8.83177e+07,RooAbsReal.NumEvent))

    resid.GetXaxis().SetTitle("M(p#bar{p}) / [MeV/c^{2}]")
    resid.GetXaxis().SetTitleFont(12)
    resid.Draw()

    h_pull = frame.pullHist("data{}".format(postFix),"mod{}".format(postFix))
    h_pull.SetFillColor(1)
    h_pull.SetMarkerSize(0.5)
    pull = Jpsi_M.frame(RF.Title(" "),RF.Bins(100), RF.Range("fitRange{}".format(postFix)))
    for ii in range(h_pull.GetN()):
        h_pull.SetPointEYlow(ii,0)
        h_pull.SetPointEYhigh(ii,0)
    pull.SetMinimum(-4.0)
    pull.SetMaximum(4.0)
    pull.GetXaxis().SetTitleSize(0.0)
    pull.GetXaxis().SetLabelSize(0.06)
    pull.GetYaxis().SetLabelSize(0.06)
    # pull.SetFillColor(1)
    pull.addPlotable(h_pull,"B")


    return frame, resid, pull


def set_canvas(rfnames, frames, resids, pulls):

    nPads = len(rfnames)
    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)

    texData = TLatex()
    texData.SetNDC()
    texData.SetTextSize(0.06)
    TGaxis.SetMaxDigits(4)

    c = TCanvas("c1","c1", 2000, 1400)
    c.Divide(nPads, 3, 0.001, 0.001)
    for i in range(nPads):

            pad = c.cd(i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            pad.SetPad(xl,yl-0.2,xh,yh)
            pad.SetBottomMargin(0.15)
            pad.SetLeftMargin(0.15)
            frames[i].Draw()
            # pad.SetLogy()
            
            texData.DrawLatex(0.3, 0.95, rfnames[i])

            #ptL = binningDict["Jpsi_PT"]
            #ptR = binningDict["Jpsi_PT"][nPT+1]
            #texData.DrawLatex(0.3, 0.95, "%s<p_{T}<%s"%(ptL,ptR))

            pad = c.cd(nPads+i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            pad.SetPad(xl,yl-0.1,xh,yh-0.2)
            pad.SetBottomMargin(0.15)
            pad.SetLeftMargin(0.15)
            #pad.SetTopMargin(0)
            #pad.SetTickx(2)
            #pad.SetTicky(2)
            resids[i].Draw()


            pad = c.cd(2*nPads+i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            #pad.SetTickx(2)
            #pad.SetTicky(2)
            pad.SetPad(xl,yl,xh,yh-0.1)
            pad.SetLeftMargin(0.15)
            pulls[i].Draw()
    return c
