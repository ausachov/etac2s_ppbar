from ROOT import *
from ROOT.RooFit import *
from ROOT.RooStats import *

homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"

gROOT.LoadMacro("~/etac2s_ppbar/fit/libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("~/etac2s_ppbar/fit/libs/libBifurcatedCB/BifurcatedCB.cxx+")

import sys
sys.path.insert(1, "/users/LHCb/zhovkovska/etac2s_ppbar/fit/libs/")

from charmonium import Particle

proton = Particle("p")
phi = Particle("phi")
m_win = 50.

from drawModule_splitFit import minM_Low, maxM_Low, minM_High, maxM_High
from drawModule_splitFit import minM_ppbar, maxM_ppbar, minM_phiphi, maxM_phiphi

ExtMeas = {
       # "ct_etac2S"      : { "val"    : 0.955, # if correction is applied to etac2S, it should be applied to etac too
       #                      "err"    : 0.007,
       #                      "relerr" : -1.},
       "ct_etac2S"      : { "val"    : 1.0,
                            "err"    : 1.e-5,
                            "relerr" : -1.},
       "BR_etac"        : { "val"    : 1.50e-3,
                            "err"    : 0.16e-3,
                            "relerr" : -1.},
       "BR_jpsi"        : { "val"    : 2.121e-3,
                            "err"    : 0.03e-3,
                            "relerr" : -1.},
       "BR_etac2S"      : { "val"    : 7.7e-5,
                            "err"    : 2.4e-5,
                            "relerr" : -1.},
       "BR_psi2S"       : { "val"    : 2.88e-4,
                            "err"    : 0.09e-4,
                            "relerr" : -1.},
       "eff_etac2S2jpsi": { "val"    : 1.067, 
                            "err"    : 0.016,
                            "relerr" : 0.01},
       "eff_etac2S2etac": { "val"    : 1.067,  
                            "err"    : 0.016,
                            "relerr" : 0.01},
       "n_jpsi"         : { "val"    : 93914., # should be picked up in more smart way every time fit of Low is updated !
                            "err"    : 3100.,  # same
                            "relerr" : -1.},
       "n_etac"         : { "val"    : 96934., # should be picked up in more smart way every time fit of Low is updated !
                            "err"    : 7670.,  # same
                            "relerr" : -1.},
       "n_psi2S"        : { "val"    : 997., # should be picked up in more smart way every time fit of Low is updated !
                            "err"    : 69.,  # same
                            "relerr" : -1.}
    }


def upd_meas(w, kNorm, kSyst):

    f = open('{}/MC/cross_talks/cross_talk_{}.txt'.format(homeDir,'etac2S'), 'r')
    line = f.readline().split()
    ExtMeas["ct_etac2S"]["val"], ExtMeas["ct_etac2S"]["err"] = float(line[1]), float(line[5])
    f.close()
    # print(ExtMeas["ct_etac2S"]["val"], ExtMeas["ct_etac2S"]["err"])

    n_norm   = "n_{}".format(kNorm)
    BR_norm  = "BR_{}".format(kNorm)
    eff_norm = "eff_etac2S2{}".format(kNorm)

    feff = TFile("{0}/eff/plots_misID_{1}/ratio_etac2S2{2}_{1}_PT_tot.root".format(homeDir, "prompt", kNorm),"READ")
    ExtMeas[eff_norm]["val"] = feff.Get("eff_val").GetVal()
    ExtMeas[eff_norm]["err"] = feff.Get("eff_err").GetVal()
    ExtMeas[eff_norm]["relerr"] = ExtMeas[eff_norm]["err"]/ExtMeas[eff_norm]["val"]
    feff.Close()

    ExtMeas[n_norm]["val"] = w.var(n_norm).getValV()
    ExtMeas[n_norm]["err"] = w.var(n_norm).getError()
    ExtMeas[n_norm]["relerr"] = w.var(n_norm).getError()/w.var(n_norm).getValV()

    if kSyst=="BR":
        ExtMeas[BR_norm]["val"]    += ExtMeas[BR_norm]["err"]
        ExtMeas[BR_norm]["relerr"]  = ExtMeas[BR_norm]["err"]/ExtMeas[BR_norm]["val"]
    elif kSyst=="eff":
        ExtMeas[eff_norm]["val"]   += ExtMeas[eff_norm]["err"]
        ExtMeas[eff_norm]["relerr"] = ExtMeas[eff_norm]["err"]/ExtMeas[eff_norm]["val"]
    elif kSyst=="cross_talk":
        ExtMeas["ct_etac2S"]["val"]   += ExtMeas["ct_etac2S"]["err"]
        ExtMeas["ct_etac2S"]["relerr"] = ExtMeas["ct_etac2S"]["err"]/ExtMeas["ct_etac2S"]["val"]

    from ROOT.TMath import Sqrt, Sq
    # total_val =  ExtMeas[n_norm]["val"]*ExtMeas[eff_norm]["val"]*ExtMeas["BR_etac2S"]["val"]/ExtMeas[BR_norm]["val"]/ExtMeas["ct_etac2S"]["val"]
    total_val =  ExtMeas[eff_norm]["val"]*ExtMeas["BR_etac2S"]["val"]/ExtMeas[BR_norm]["val"]/ExtMeas["ct_etac2S"]["val"]

    total_relerr = 0.
    # for source in ["ct_etac2S", "BR_etac2S", BR_norm, eff_norm, n_norm]:
    for source in ["ct_etac2S", "BR_etac2S", BR_norm, eff_norm]:
        meas = ExtMeas[source]
        val  = meas["val"]
        if meas["relerr"] != -1. :
            relerr = meas["relerr"]
        else:
            relerr = meas["err"]/val
        total_relerr += Sq(relerr)
    total_relerr = Sqrt(total_relerr)
    total_err = total_relerr * total_val

    #total_val =  ExtMeas[n_norm]["val"]*ExtMeas["eff_etac2S2Jpsi"]["val"]

    #total_relerr = 0.
    #for source in ["eff_etac2S2Jpsi",n_norm]:
        #meas = ExtMeas[source]
        #val  = meas["val"]
        #if meas["relerr"] != -1. :
            #relerr = meas["relerr"]
        #else:
            #relerr = meas["err"]/val
        #total_relerr += Sq(relerr)
    #total_relerr = Sqrt(total_relerr)
    #total_err = total_relerr * total_val

    print(" OVERAL factor is      :", total_val)
    print(" WITH relative error   :", total_relerr)
    return total_val, total_relerr, total_err


def choose_bkg(optList, regName, argList):
    x = "(@0-{0})/{1}".format(3300., 450.)
    # formula = "TMath::Exp(-{0}*@1)*(1.+@2*{0}+@3*{0}*{0}+@4*{0}*{0}*{0}+@5*{0}*{0}*{0}*{0}+@6*{0}*{0}*{0}*{0}*{0})".format(x)
    formula = "TMath::Exp(-{0}*@1)*(1.+@2*{0}+@3*{0}*{0}+@4*{0}*{0}*{0}+@5*{0}*{0}*{0}*{0})".format(x)
    return {

        "prompt Cheb phiphi" : RooChebychev ("bkg{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2),argList.at(3),argList.at(4),argList.at(5))),
        "prompt Cheb ppbar" : RooChebychev ("bkg{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2),argList.at(3),argList.at(4),argList.at(5))),
        "fromB Cheb phiphi"  : RooChebychev ("bkg{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2))),
        "fromB Cheb ppbar"  : RooChebychev ("bkg{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2),argList.at(3))),
        # "prompt Exp"  : RooGenericPdf("bkg{}".format(regName),"background","TMath::Exp(-(@0-3550.)/200.*@1)*(1.+@2*(@0-3550.)/200.+@3*(@0-3550.)*(@0-3550.)/200./200.)", RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3))) ,
        # "fromB Exp"   : RooGenericPdf("bkg{}".format(regName),"background","TMath::Exp(-(@0-3550.)/200.*@1)*(1.+@2*(@0-3550.)/200)", RooArgList(argList.at(0), argList.at(1),argList.at(2)))
        "prompt Exp ppbar"  : RooGenericPdf("bkg{}".format(regName),"background", formula, RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3),argList.at(4),argList.at(5))) ,
        "prompt Exp phiphi"  : RooGenericPdf("bkg{}".format(regName),"background", formula, RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3),argList.at(4),argList.at(5))) ,
        "fromB Exp ppbar"  : RooGenericPdf("bkg{}".format(regName),"background","TMath::Exp(-{0}*@1)*(1.+@2*{0}+@3*{0}*{0})".format(x), RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3))) ,
        "fromB Exp phiphi"  : RooGenericPdf("bkg{}".format(regName),"background","TMath::Exp(-{0}*@1)*(1.+@2*{0}+@3*{0}*{0})".format(x), RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3))) ,
       
    }[optList[0] + " " + optList[1] + " " + optList[2]]

def setMW(w, ccbar_list, ref_mass=None):

    # shifting ref particle to a first position
    # if ref_mass!=None:
    #     # ccbar_list.insert(0, ccbar_list.pop(ccbar_list.index(ref_mass)))

    #     ccbar = Particle(ref_mass)
    #     gamma = RooRealVar("gamma_{}".format(ccbar.name),"width of Br-W", ccbar.width, 0.1, 50. )
    #     gamma.setConstant(True)

    #     mass_ref  = RooRealVar("mass_{}".format(ccbar.name),"mean of gaussian", ccbar.mass, ccbar.mass-m_win, ccbar.mass+m_win)
    #     mass.setConstant(True)

    #     ccbar_list.pop(ccbar_list.index(ref_mass))

    #     getattr(w,"import")(mass_ref)
    #     getattr(w,"import")(gamma)

    radius   = RooRealVar("radius","radius", 1.)
    proton_m = RooRealVar("proton_m","proton mass", proton.mass )
    phi_m    = RooRealVar("phi_m","phi mass", phi.mass )

    getattr(w,"import")(radius)
    getattr(w,"import")(proton_m)
    getattr(w,"import")(phi_m)

    for name in ccbar_list:
        # width = ccbar.width_alt if kSyst=="Gamma" else ccbar.width
        ccbar = Particle(name)

        gamma = RooRealVar("gamma_{}".format(ccbar.name),"width of Br-W", ccbar.width, 0., max(30., 3*ccbar.width) )
        gamma.setConstant(True)

    #     # mass  = RooAbsReal()
        mass  = RooRealVar("mass_{}".format(ccbar.name),"mean of gaussian", ccbar.mass, ccbar.mass-m_win, ccbar.mass+m_win)
        mass.setConstant(True)
    #     # if ref_mass==None:
    #     #     mass  = RooRealVar("mass_{}".format(ccbar.name),"mean of gaussian", ccbar.mass, ccbar.mass-m_win, ccbar.mass+m_win)
    #     #     mass.setConstant(True)

    #     # else:
        spin = RooRealVar("spin_{}".format(ccbar.name),"spin", ccbar.spin )
        
        getattr(w,"import")(gamma)
        getattr(w,"import")(mass)
        getattr(w,"import")(spin)


def fill_workspace(w, name, kRange, ref="etac", kCB=False, kSyst="Base"):

    # if kRange!="":
    #     kRange = "_{}".format(kRange)

    if kCB: 
        return fill_workspace_CB(w, name, kRange, ref, kSyst)
    else:
        return fill_workspace_G(w, name, kRange, ref, kCB, kSyst)

def fill_workspace_G(w, name, kRange, ref="etac", kCB=False, kSyst="Base"):

    Jpsi_M = w.var("Jpsi_M_{}".format(kRange))
    if Jpsi_M == None:
        Jpsi_M = w.var("Jpsi_M")

    ccbar = Particle(name)

    # bw = False if ("psi" in name) else True
    bw = False if (ccbar.width<1.e-3) else True

    ratio_sigma = 0.30
    ratio_ref = 1.1 #ratio sigma_ccbar 2 sigma_ref
    ratio_area = 0.90


    #ratioNtoW,ratioEtaTo_etac2S,ratioArea, gamma_etac, eff = getConstPars()

    r_NToW   = RooRealVar()
    r_G1ToG2 = RooRealVar()
    sigma_1  = RooRealVar()
    r_ref = RooRealVar("r_ref_{}_{}".format(name, kRange),"r_ref", ratio_ref, 0., 3.)

    n_ccbar = RooRealVar("n_{}_{}".format(ccbar.name, kRange),"num of etac", 1e4, 1., 5.e6)

    # if name=="etac2S":

    #     kNorm = ref
    #     total_val, total_relerr, total_err = upd_meas(w, kNorm, kSyst)

    #     extFactor         = RooRealVar("extFactor",  "extFactor", total_val, 0, 100*total_val)
    #     constr_ext        = RooGaussian("constr_ext","constr_ext", extFactor, RooFit.RooConst(total_val), RooFit.RooConst(total_err))
    #     getattr(w,"import")(constr_ext)

    #     # n_etac2S_rel = RooRealVar("n_etac2S_rel","num of Etac", 1.5e-3, 0, 4.)
    #     # n_etac2S = RooRealVar("n_etac2S","num of etac2S", 2e3, 0, 3.e4)
    #     cs_etac2S_rel = RooRealVar("cs_etac2S_rel","num of Etac", 5e-1, 0., 5.0)
    #     # n_ccbar       = RooFormulaVar("n_{}".format(ccbar.name),"num of Etac","@0*@1", RooArgList(cs_etac2S_rel, extFactor))
    #     n_ref = w.var("n_{}".format(ref))
    #     n_ccbar       = RooFormulaVar("n_{}".format(ccbar.name),"num of Etac","@0*@1*@2", RooArgList(cs_etac2S_rel, extFactor, n_ref))

    modName = "_CB" if kCB else ""
    f = TFile("{}//MC/mass_fit/Wksp_M_res_all_{}_sim{}.root".format(homeDir,ref,modName))
    w_MC = f.Get("w")
    

    if ref==ccbar.name:
        
        if kRange=="ppbar":
            ratio_sigma = w_MC.var("r_NToW").getValV()
            ratio_area  = w_MC.var("r_G1ToG2").getValV()
        else:
            ratio_sigma = 1/2.16
            ratio_area  = 0.87

        r_NToW    = RooRealVar("r_NToW_{}".format(kRange),"rNarToW",ratio_sigma)#, 0., 1.)
        r_G1ToG2  = RooRealVar("r_G1ToG2_{}".format(kRange),"rG1toG2",ratio_area)#, 0., 1.)
        sigma_1   = RooRealVar("sigma_{}_1_{}".format(ccbar.name, kRange),"width of gaussian", 8., 3.0, 15.)
        
        # sigma_scale = w_MC.var("r_ref_etac2S").getValV()
        f.Close()
    else:
        if name=="hc":
            r_ref.setVal(w_MC.var("r_ref_{}".format("chic1")).getValV())
        elif ("psi" in name and name!="jpsi"):
            r_ref.setVal(w_MC.var("r_ref_{}".format("psi2S")).getValV())
        else:
            r_ref.setVal(w_MC.var("r_ref_{}".format(name)).getValV())
        r_ref.setConstant(True)

        r_NToW    = w.var("r_NToW_{}".format(kRange))
        r_G1ToG2  = w.var("r_G1ToG2_{}".format(kRange))
        sigma_ref = w.var("sigma_{}_1_{}".format(ref, kRange))
        sigma_1   = RooFormulaVar("sigma_{}_1_{}".format(ccbar.name,kRange),"width of gaussian","@0*@1",RooArgList(RooArgSet(sigma_ref,r_ref)))
    

    sigma_2 = RooFormulaVar("sigma_{}_2_{}".format(ccbar.name,kRange),"width of gaussian","@0/@1",RooArgList(sigma_1,r_NToW))

    n_ccbar_1 = RooFormulaVar("n_{}_1_{}".format(ccbar.name,kRange),"num of ","@0*@1",RooArgList(n_ccbar,r_G1ToG2))
    n_ccbar_2 = RooFormulaVar("n_{}_2_{}".format(ccbar.name,kRange),"num of ","@0-@1",RooArgList(n_ccbar,n_ccbar_1))

    mass  = w.var("mass_{}".format(ccbar.name))
    width = ccbar.width_alt if kSyst=="Gamma" else ccbar.width
    gamma  = w.var("gamma_{}".format(ccbar.name))
    gamma.setVal(width)
    spin  = w.var("spin_{}".format(ccbar.name))

    # mass  = RooRealVar("mass_{}_{}".format(ccbar.name,kRange),"mean of gaussian", ccbar.mass, ccbar.mass-m_win, ccbar.mass+m_win)
    # mass.setConstant(True)
    # width = ccbar.width_alt if kSyst=="Gamma" else ccbar.width
    # gamma = RooRealVar("gamma_{}_{}".format(ccbar.name,kRange),"width of Br-W", width, 0., max(30., 3*width) )
    # gamma.setConstant(True)

    # spin = RooRealVar("spin_{}_{}".format(ccbar.name, kRange),"spin", ccbar.spin )
    radius   = w.var("radius")
    # radius   = RooRealVar("radius","radius", 1.)


    if kRange=="ppbar":
        daughter_name = "proton"
        # daughter_m = RooRealVar("proton_m_{}".format(kRange),"proton mass", proton.mass )
        # daughter_m = proton_m
    else:
        daughter_name = "phi"
        # daughter_m = RooRealVar("phi_m_{}".format(kRange),"phi mass", phi.mass )
        # daughter_m = phi_m

    daughter_m = w.var("{}_m".format(daughter_name))

    #Fit etac

    if bw: mean = RooFit.RooConst(0)
    else: mean = mass


    resolution_1 = RooGaussian("gauss_{}_1_{}".format(ccbar.name, kRange),"gauss_1 PDF", Jpsi_M, mean, sigma_1) #mass -> mean2s
    resolution_2 = RooGaussian("gauss_{}_2_{}".format(ccbar.name, kRange),"gauss_2 PDF", Jpsi_M, mean, sigma_2) #mass -> mean2s

    f.Close()

    model = RooAddPdf()
    if bw:
        brw = RooRelBreitWigner("brw_{}_{}".format(ccbar.name, kRange), "brw", Jpsi_M, mass, gamma, spin, radius, daughter_m, daughter_m)
        pdf_1 = RooFFTConvPdf("bwxg_{}_1_{}".format(ccbar.name, kRange),"breit-wigner (X) gauss", Jpsi_M, brw, resolution_1)
        pdf_2 = RooFFTConvPdf("bwxg_{}_2_{}".format(ccbar.name, kRange),"breit-wigner (X) gauss", Jpsi_M, brw, resolution_2)
    else:
        pdf_1 = resolution_1
        pdf_2 = resolution_2
        #pdf = RooGaussian("gauss_{}_1".format(ccbar.name),"gauss_1 PDF", Jpsi_M, mass, sigma_1) #mass -> mean2s

    #model = RooAddPdf("model_{}".format(ccbar.name),"{} signal".format(ccbar.name), RooArgList(pdf_1), RooArgList(n_ccbar))
    model = RooAddPdf("model_{}_{}".format(ccbar.name, kRange),"{} signal".format(ccbar.name), RooArgList(pdf_1,pdf_2), RooArgList(r_G1ToG2))
    # model = RooAddPdf("model_{}".format(ccbar.name),"{} signal".format(ccbar.name), RooArgList(pdf_1,pdf_2), RooArgList(n_ccbar_1,n_ccbar_2))
    getattr(w,"import")(n_ccbar)
    getattr(w,"import")(model,RooFit.RecycleConflictNodes())

    return model, n_ccbar

def fill_workspace_CB(w, name, kRange, ref="etac", kSyst="Base"):

    Jpsi_M = w.var("Jpsi_M{}".format(kRange))
    if Jpsi_M == None:
        Jpsi_M = w.var("Jpsi_M")

    ccbar = Particle(name)

    bw = False if ("psi" in name) else True

    ratio_sigma = 0.30
    ratio_ref = 1.1 #ratio sigma_ccbar 2 sigma_ref
    ratio_area = 0.90


    #ratioNtoW,ratioEtaTo_etac2S,ratioArea, gamma_etac, eff = getConstPars()

    r_ref = RooRealVar("r_ref_{}".format(name),"r_ref", ratio_ref, 0., 3.)
    n_ccbar = RooRealVar("n_{}".format(ccbar.name),"num of etac", 1e4, 10., 5.e7)

    if name=="etac2S":

        kNorm = "etac"
        total_val, total_relerr, total_err = upd_meas(w, kNorm, kSyst)

        extFactor         = RooRealVar("extFactor",  "extFactor", total_val, 0, 100*total_val)
        constr_ext        = RooGaussian("constr_ext","constr_ext", extFactor, RooFit.RooConst(total_val), RooFit.RooConst(total_err))
        getattr(w,"import")(constr_ext)

        # n_etac2S_rel = RooRealVar("n_etac2S_rel","num of Etac", 1.5e-3, 0, 4.)
        # n_etac2S = RooRealVar("n_etac2S","num of etac2S", 2e3, 0, 3.e4)
        cs_etac2S_rel = RooRealVar("cs_etac2S_rel","num of Etac", 1e-2, 0, 5.0)
        n_ccbar       = RooFormulaVar("n_{}".format(ccbar.name),"num of Etac","@0*@1", RooArgList(cs_etac2S_rel, extFactor))

    modName = "_CB"
    f = TFile("{}//MC/mass_fit/Wksp_M_res_all_{}_sim{}.root".format(homeDir,ref,modName))
    w_MC = f.Get("w")
    
    if ref==ccbar.name:
        al_1  = w_MC.var("alpha_1").getValV()
        # al_2  = w_MC.var("alpha_2").getValV()
        ncb_1 = w_MC.var("nCB_1").getValV()
        # ncb_2 = w_MC.var("nCB_2").getValV()

        alpha_1 = RooRealVar("alpha_1","alpha of CB", al_1)
        # alpha_2 = RooRealVar("alpha_2","alpha of CB", al_2)
        nCB_1   = RooRealVar("nCB_1","n of CB", ncb_1)
        # nCB_2   = RooRealVar("nCB_2","n of CB", ncb_2)

        
        sigma_1   = RooRealVar("sigma_{}_1".format(ccbar.name),"width of gaussian", 9., 0.1, 30.)
        sigma_scale = w_MC.var("r_ref_etac2S").getValV()
        f.Close()
    else:
        if name=="hc":
            r_ref.setVal(w_MC.var("r_ref_{}".format("chic1")).getValV())
        else:
            r_ref.setVal(w_MC.var("r_ref_{}".format(name)).getValV())
        r_ref.setConstant(True)

        alpha_1 = w.var("alpha_1")
        # alpha_2 = w.var("alpha_2")
        nCB_1   = w.var("nCB_1")
        # nCB_2   = w.var("nCB_2")

        sigma_ref = w.var("sigma_{}_1".format(ref))
        sigma_1   = RooFormulaVar("sigma_{}_1".format(ccbar.name),"width of gaussian","@0*@1",RooArgList(RooArgSet(sigma_ref,r_ref)))
    
    f.Close()

    mass  = RooRealVar("mass_{}".format(ccbar.name),"mean of gaussian", ccbar.mass, ccbar.mass-m_win, ccbar.mass+m_win)
    mass.setConstant(True)
    width = ccbar.width_alt if kSyst=="Gamma" else ccbar.width
    gamma = RooRealVar("gamma_{}".format(ccbar.name),"width of Br-W", width, 10., 50. )
    gamma.setConstant(True)

    spin = RooRealVar("spin_{}".format(ccbar.name),"spin", ccbar.spin )
    radius = RooRealVar("radius","radius", 1.)
    proton_m = RooRealVar("proton_m","proton mass", proton.mass )


    #Fit etac

    if bw: mean = RooFit.RooConst(0)
    else: mean = mass

    brw = RooRelBreitWigner("brw_{}".format(ccbar.name), "brw",Jpsi_M, mass, gamma, spin,radius,proton_m,proton_m)
    resolution_1 = BifurcatedCB("cb_{}_1".format(ccbar.name), "Cystal Ball Function", Jpsi_M, mean, sigma_1, alpha_1, nCB_1, alpha_1, nCB_1)


    model = RooAddPdf()
    if bw:
        pdf_1 = RooFFTConvPdf("bwxg_{}_1".format(ccbar.name),"breit-wigner (X) gauss", Jpsi_M, brw, resolution_1)
    else:
        pdf_1 = resolution_1
        #pdf = RooGaussian("gauss_{}_1".format(ccbar.name),"gauss_1 PDF", Jpsi_M, mass, sigma_1) #mass -> mean2s

    model = RooAddPdf("model_{}".format(ccbar.name),"{} signal".format(ccbar.name), RooArgList(pdf_1), RooArgList(RooFit.RooConst(1.)))
    # model = RooAddPdf("model_{}".format(ccbar.name),"{} signal".format(ccbar.name), RooArgList(pdf_1,pdf_2), RooArgList(n_ccbar_1,n_ccbar_2))
    getattr(w,"import")(n_ccbar)
    getattr(w,"import")(model,RooFit.RecycleConflictNodes())
    return model, n_ccbar


def combine(w, name_list, kRange, kSource, kBkg="Cheb", split_lvl="source"):

    # At some point should be optimised:

    if kRange=="":
        postfix = ""
    else:
        postfix = "_{}".format(kRange)

    if split_lvl=="all":
        Jpsi_M = w.var("Jpsi_M{}".format(postfix))
    else:
        Jpsi_M = w.var("Jpsi_M")

    if split_lvl=="none":
        postfix = "_{}".format(kSource)

    # define background model and yield
    a0 = RooRealVar("a0{}".format(postfix),"a0",0.0,-2.,2.) # !!! for prompt fit an big range initial value has to be 0
    a1 = RooRealVar("a1{}".format(postfix),"a1",0.1,-1.,1.)
    a2 = RooRealVar("a2{}".format(postfix),"a2",-0.01,-1.,1.)
    a3 = RooRealVar("a3{}".format(postfix),"a3",0.01,-1.,1.)
    a4 = RooRealVar("a4{}".format(postfix),"a4",-0.001,-1.,1.)
    # a5 = RooRealVar("a5{}".format(postfix),"a5",0.001,-1.,1.)
    par_bkg = RooArgList(Jpsi_M, a0, a1, a2, a3, a4)

    bkg    = choose_bkg([kSource,kBkg, kRange], postfix, par_bkg)
    n_bkg  = RooRealVar("n_bkg{}".format(postfix),"num of etac", 4e4, 1, 5.e9)

    n_jpsi = w.var("n_jpsi_ppbar")
    eff_pppi0 = RooRealVar("eff_pppi0","eff_pppi0",0.06)
    pppi0 = RooGenericPdf("pppi0","Jpsi.pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))
    n_pppi0 = RooFormulaVar("n_pppi0","n_pppi0","@0*@1*(1.19/2.12)",RooArgList(n_jpsi,eff_pppi0))

    # define signal models and yields
    yields = RooArgSet()
    pdfs   = RooArgSet()

    for name in name_list:


        model = w.pdf("model_{}{}".format(name,postfix))
        n_ccbar = w.var("n_{}{}".format(name,postfix))
        # if name!="etac2S":
        #     n_ccbar = w.var("n_{}".format(name))
        # else:
        #     n_ccbar = w.function("n_{}".format(name))

        pdfs.add(model)
        yields.add(n_ccbar)


    modelBkg    = RooAddPdf("modelBkg{}".format(postfix),"bkg", RooArgList(bkg), RooArgList(n_bkg))
    # if "etac2S" in name_list:
    #     constr_ext = w.pdf("constr_ext")


    #     ''' add postfix _ext in case of recreating the model  '''
    #     # modelSignal  = RooAddPdf("modelSignal{}_ext".format(postfix),"{} signal".format(kRange), RooArgList(pdfs), RooArgList(yields))
    #     modelSignal  = RooAddPdf("modelSignal{}".format(postfix),"signal", RooArgList(pdfs), RooArgList(yields))
    #     yields.add(n_bkg)
    #     pdfs.add(bkg)
    #     yields.add(n_pppi0)
    #     pdfs.add(pppi0)
    #     # model        = RooAddPdf("model{}_ext".format(postfix),"model", RooArgList(pdfs), RooArgList(yields))
    #     model        = RooAddPdf("model{}".format(postfix),"model", RooArgList(pdfs), RooArgList(yields))
    #     model_constr = RooProdPdf("model{}_constr".format(postfix),"model_constr", RooArgList(model, constr_ext))
    #     getattr(w,"import")(model_constr,RooFit.RecycleConflictNodes())

    # else:
    #     modelSignal = RooAddPdf("modelSignal{}".format(postfix),"signal", RooArgList(pdfs), RooArgList(yields))
    #     yields.add(n_bkg)
    #     pdfs.add(bkg)
    #     yields.add(n_pppi0)
    #     pdfs.add(pppi0)
    #     model       = RooAddPdf("model{}".format(postfix),"model", RooArgList(pdfs), RooArgList(yields))
    modelSignal = RooAddPdf("modelSignal{}".format(postfix),"signal", RooArgList(pdfs), RooArgList(yields))
    yields.add(n_bkg)
    pdfs.add(bkg)
    if kRange=="ppbar":
        yields.add(n_pppi0)
        pdfs.add(pppi0)
    model       = RooAddPdf("model{}".format(postfix),"model", RooArgList(pdfs), RooArgList(yields))

    getattr(w,"import")(model,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelSignal,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelBkg,RooFit.RecycleConflictNodes())


def create_sample(w, ext=False):

    Jpsi_M = w.var("Jpsi_M")

    # if ext:
    #     # in case of extended pdf rename model to avoid a conflict when drawing
    #     model_High = w.pdf("model_High_constr")
    #     model_High.SetName("model_High")
    #     model_High.SetTitle("model_High")

    #     # modelE_High = w.pdf("model_High_ext")

    #     modelS_High = w.pdf("modelSignal_High_ext")
    #     modelS_High.SetName("modelSignal_High")
    #     modelS_High.SetTitle("modelSignal_High")
    # else:
    #     model_High = w.pdf("model_High")

    model_ppbar = w.pdf("model_ppbar")
    model_phiphi = w.pdf("model_phiphi")

    sample = RooCategory("sample","sample")
    sample.defineType("phiphi")
    sample.defineType("ppbar")

    Jpsi_M.setRange("fitRange_ppbar", minM_ppbar, maxM_ppbar)
    Jpsi_M.setRange("fitRange_phiphi", minM_phiphi, maxM_phiphi)

    data_phiphi = w.data("dh_phiphi")
    data_ppbar = w.data("dh_ppbar")


    # Construct combined dataset in (Jpsi_M,sample)
    combData = RooDataHist("combData", "combined data", RooArgList(Jpsi_M), RooFit.Index(sample), RooFit.Import("phiphi",data_phiphi), RooFit.Import("ppbar",data_ppbar))


    # Associate model with the physics state and model_ctl with the control state
    simPdf = RooSimultaneous("simPdf","simultaneous signal pdf",sample)
    simPdf.addPdf(model_phiphi,"phiphi")
    simPdf.addPdf(model_ppbar,"ppbar")


    #   getattr(w,"import")(model,RecycleConflictNodes())
    getattr(w,"import")(combData, RooFit.RecycleConflictNodes())
    getattr(w,"import")(simPdf, RooFit.RecycleConflictNodes())

