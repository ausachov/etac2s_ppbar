from ROOT import *
import ROOT.RooFit as RF
from ROOT.RooStats import *

from model import *
import drawModule_splitFit as dm
import oniaConstLib as cl
gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")

import numpy as np

#homeDir = "/afs/cern.ch/user/v/vazhovko/private/etac2s_ppbar/"
#dataDir = "/eos/user/v/vazhovko/etac2s_ppbar/"
homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

def get_data_h_tight(kSource, cut=None):

    dirName_Low  = dataDir +"Data_Low_NoPID/"
    dirName_High = dataDir +"Data_High_NoPID/"


    nt_Low  =  TChain("DecayTree")
    nt_High =  TChain("DecayTree")

    nt_Low.Add(dirName_Low+"/Etac2sDiProton_Low_2018*.root")
    nt_High.Add(dirName_High+"/Etac2sDiProton_High_2018*.root")

    #tree_Low = TTree()
    #tree_High = TTree()

    cut_FD = ""
    if kSource=="prompt":
        cut_FD = TCut("(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08")# & Jpsi_FDCHI2_OWNPV > 49"
    else:
        cut_FD = TCut("(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16")# & Jpsi_FDCHI2_OWNPV > 49"

    cut_PT      = TCut("Jpsi_PT > 5000 && Jpsi_PT < 20000")

    totCut = TCut(cut_FD + cut_PT)
    if cut!=None:
        totCut = TCut(totCut + cut)

    nbins = int(dm.binN_Tot_Fit/40)
    hh_Low = TH1D("hh_Low","hh_Low", nbins, dm.minM_Low, dm.maxM_High)
    hh_High = TH1D("hh_High","hh_High", nbins, dm.minM_Low, dm.maxM_High)

    nt_Low.Draw("Jpsi_m_scaled>>hh_Low",totCut.GetTitle(),"goff")
    nt_High.Draw("Jpsi_m_scaled>>hh_High",totCut.GetTitle(),"goff")

    hlist = TList()
    hlist.Add(hh_Low)
    hlist.Add(hh_High)
    hist = hh_Low.Clone("hist")
    hist.Reset()
    hist.Merge(hlist)
    c = TCanvas()
    hist.Draw()
    c.SaveAs(homeDir + "/mass_fit/one_hist/hh_{}_t.pdf".format(kSource))
    return hist

if __name__ == "__main__":

    kSource = "prompt"
    h = get_data_h_tight(kSource)    
    # h = get_data_h_tight("prompt")    

    cut_misID_Kpi = TCut("(Jpsi_M01_Subst01_pp~2Kpi>1895 || Jpsi_M01_Subst01_pp~2Kpi<1835)")
    cut_misID_piK = TCut("(Jpsi_M01_Subst01_pp~2piK>1895 || Jpsi_M01_Subst01_pp~2piK<1835)")

    cut_misID_pipi = TCut("(Jpsi_M01_Subst01_pp~2pipi>1805 || Jpsi_M01_Subst01_pp~2piK<1770)")

    cut_misID_KK = TCut("(Jpsi_M01_Subst01_pp~2KK>(1245 + 0.3196*Jpsi_m_scaled) || Jpsi_M01_Subst01_pp~2KK<(1225 + 0.3196*Jpsi_m_scaled))")

    h_misID = get_data_h_tight(kSource, TCut(cut_misID_Kpi + cut_misID_piK))    
    # h_misID = get_data_h_tight("prompt", TCut(cut_misID_Kpi + cut_misID_piK))    

    c = TCanvas()
    c.cd()

    h.Draw()
    h_misID.SetLineColor(2)
    h_misID.Draw("same")
    c.SaveAs(homeDir + "/mass_fit/one_hist/hh_{}_comp.pdf".format(kSource))



