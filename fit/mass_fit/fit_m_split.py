from ROOT import *
from ROOT.RooFit import *
from ROOT.RooStats import *
# config = RooStats.GetGlobalRooStatsConfig()
# config.useEvalErrorWall = False

# ROOT.Math.MinimizerOptions.SetDefaultMinimizer("Minuit2","Minimize");
# ROOT.Math.MinimizerOptions.SetDefaultMaxFunctionCalls(100000)
# ROOT.Math.MinimizerOptions.SetDefaultMaxFunctionCalls(300000)

from drawModule_splitFit import *
import oniaConstLib as cl
# from setup_model import *
from model import *
gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")

import numpy as np

homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"


    # to make it faster one can combine Br_etac2s x / Br_Jpsi * effratio
    # this works ONLY for external constraints i.e. uncorrelated ones
    # in principle n_Jpsi can go here as well
    # systematics to be added


def get_data_h(w, kSource):

    # nameWksp = dataDir + "/wksps/wksp_data_{}_ProbNNp_06_wide.root".format(kSource)
    nameWksp = dataDir + "/wksps/wksp_data_{}_ProbNNp_06.root".format(kSource)
    file_w = TFile(nameWksp,"read")
    w_in = file_w.Get("w").Clone()
    file_w.Close()

    dataL   = w_in.data("dh_Low")
    dataH   = w_in.data("dh_High")
    Jpsi_M = w_in.var("Jpsi_M")

    histL = dataL.createHistogram("histL",Jpsi_M)
    histH = dataH.createHistogram("histH",Jpsi_M)

    histL.Print()
    histH.Print()

    Jpsi_M_Low  = RooRealVar("Jpsi_M_Low", "Jpsi_M_Low",  minM_Low, maxM_Low)
    Jpsi_M_High = RooRealVar("Jpsi_M_High","Jpsi_M_High", minM_High, maxM_High)

    dh_Low  = RooDataHist("dh_Low_{}".format(kSource),"dh",  RooArgList(Jpsi_M_Low), histL)
    dh_High = RooDataHist("dh_High_{}".format(kSource),"dh",  RooArgList(Jpsi_M_High), histH)

    getattr(w,"import")(Jpsi_M_Low)
    getattr(w,"import")(Jpsi_M_High)
    getattr(w,"import")(dh_Low)
    getattr(w,"import")(dh_High)

    print("DATAHISTS READ SUCCESSFULLY")

def print_to_file(nameFile, fitRes_list):

    import os, sys
    save = os.dup( sys.stdout.fileno() )
    newout = open(nameFile, "a" )
    os.dup2( newout.fileno(), sys.stdout.fileno() )

    for fr in fitRes_list:
        fr.Print("v")
        fr.correlationMatrix().Print()

    os.dup2( save, sys.stdout.fileno() )
    newout.close()
    print("{} is created".format(nameFile))

def perform_splitted_fit(kSource, kNorm="jpsi", kSyst="Base"):

    homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
    gStyle.SetStatFormat("4.3f")

    w = RooWorkspace("w")
    # getHists(w)
    get_data_h(w,kSource)
    # fillWksp_Low(w, kSource, kSyst)
    # fillWksp_High(w, kSource, kNorm, kSyst)

    ''' attempt to optimize this code '''

    low_states = ["jpsi", "etac"]
    # low_states = ["etac", "jpsi"]
    high_states = ["chic0","chic1","chic2","hc", "psi2S"]

    kCB = False if kSyst!="CB" else True

    for name in low_states:
        kBW = False if ("psi" in name) else True
        fill_workspace(w, name, "Low", ref=kNorm, kCB=kCB)

    for name in high_states:
        kBW = False if ("psi" in name) else True
        fill_workspace(w, name, "High", ref=kNorm, kCB=kCB)


    kBkg = "Cheb" if kSyst!="Exp" else kSyst
    combine(w, low_states, "Low", kSource, kBkg, split="all")
    combine(w, high_states, "High", kSource, kBkg, split="all")

    ''' ----------------------------- '''

    Jpsi_M_Low = w.var("Jpsi_M_Low")
    Jpsi_M_High = w.var("Jpsi_M_High")

    # model_High    = w.pdf("model_High_Lim")
    model_High    = w.pdf("model_High")
    model_Low     = w.pdf("model_Low")

    modelBkg_High = w.pdf("modelBkg_High")
    modelBkg_Low  = w.pdf("modelBkg_Low")

    data_Low  = w.data("dh_Low")
    data_High = w.data("dh_High")


    if kSource=="prompt":
        f = TFile(homeDir+"/mass_fit/split_fit/Wksp_{}_fromB_{}_base_MC.root".format(kNorm, kSyst),"READ")
        w_fromB = f.Get("w")
        f.Close()
        sigma_name = "sigma_{}_1".format(kNorm)
        sigma = w_fromB.var(sigma_name).getValV()
        w.var(sigma_name).setVal(sigma)
        w.var(sigma_name).setConstant(True)
        # mass_name  = "mass_{}".format(kNorm)
        # mass = w_fromB.var(mass_name).getValV()
        # w.var(mass_name).setVal(mass)
        # w.var(mass_name).setConstant(True)
    # else:
        # w.var("mass_jpsi").setConstant(False)
        # w.var("mass_chic0").setConstant(True)


    llist = RooLinkedList()
    llist.Add(RooCmdArg(RooFit.Extended(True)))
    llist.Add(RooCmdArg(RooFit.Save(True)))
    llist.Add(RooCmdArg(RooFit.Offset(True)))
    llist.Add(RooCmdArg(RooFit.NumCPU(8)))
    llist.Add(RooCmdArg(RooFit.Minos(True)))
    #llist.Add(RooCmdArg(RooFit.Strategy(2)))

    #llist_ext = llist.Clone()
    #llist_ext.Add(RooCmdArg(RooFit.Minos(True)))

    fitresult_Low = model_Low.fitTo(data_Low, llist)
    fitresult_Low = model_Low.fitTo(data_Low, llist)
    w.var("sigma_jpsi_1").setConstant(True)
    # w.var("sigma_etac_1").setConstant(True)
    w.var("mass_jpsi").setConstant(True)
    w.var("mass_etac").setConstant(True)


    fitresult_High = model_High.fitTo(data_High, llist)
    fitresult_High = model_High.fitTo(data_High, llist)

    ''' attempt to optimize this code '''

    high_states.append("etac2S")
    fill_workspace(w, "etac2S", "High", ref=kNorm, kCB=kCB, kSyst=kSyst)
    combine(w, high_states, "High", kSource, kBkg)

    ''' ----------------------------- '''

    # fillWksp_etac2S(w, kSource, kNorm, kSyst)
    model_High = w.pdf("model_High_constr")


    fitresult_High = model_High.fitTo(data_High, llist)
    fitresult_High = model_High.fitTo(data_High, llist)

    frame_Low,  resid_Low,  pull_Low = set_frame(w,"Low")
    frame_High, resid_High, pull_High = set_frame(w,"High")

    names  = ["Low","High"]
    frames = [frame_Low,frame_High]
    resids = [resid_Low,resid_High]
    pulls  = [pull_Low, pull_High]
    c = set_canvas(names, frames, resids, pulls)

    nameWksp = "{}/mass_fit/split_fit/Wksp_{}_{}_{}_base_MC.root".format(homeDir,kNorm,kSource,kSyst)
    namePic  = "{}/mass_fit/split_fit/Plot_{}_{}_{}_base_MC.pdf".format(homeDir, kNorm,kSource,kSyst)
    nameTxt  = "{}/mass_fit/split_fit/Result_{}_{}_{}_base_MC.txt".format(homeDir, kNorm,kSource,kSyst)

    c.SaveAs(namePic)
    w.writeToFile(nameWksp)

    print_to_file(nameTxt, [fitresult_Low, fitresult_High])

    print("Fit performed successfully!")
    return(w)


def calcUL_HighRangeOnly(w, kSource, kSyst, kNorm):

    upd_meas(w, kNorm, kSyst)

    Jpsi_M_High = w.var("Jpsi_M_High")

    totalPdf_High = w.pdf("model_High_constr")
    data_High     = w.data("dh_High")

    var_list_0 = w.allVars()

    nuis_iter = var_list_0.createIterator()
    var = nuis_iter.Next()
    while var:
        var.setConstant(True)
        var = nuis_iter.Next()

    nBckgr_High = w.var("n_bkg_High")
    nBckgr_Low  = w.var("n_bkg_Low")
    n_Jpsi      = w.var("n_jpsi")
    n_psi2S     = w.var("n_psi2S")
    n_etac      = w.var("n_etac")
    n_hc        = w.var("n_hc")
    n_chic0     = w.var("n_chic0")
    n_chic1     = w.var("n_chic1")
    n_chic2     = w.var("n_chic2")
    a0_Low      = w.var("a0_Low")
    a1_Low      = w.var("a1_Low")
    a2_Low      = w.var("a2_Low")
    a0_High     = w.var("a0_High")
    a1_High     = w.var("a1_High")
    a2_High     = w.var("a2_High")

    nBckgr_Low.setConstant()
    n_etac.setConstant()
    n_Jpsi.setConstant()
    nBckgr_High.setConstant(False)


    #cs_etac2S_rel = w.var("n_chic2_rel")
    # w.Print()
    cs_etac2S_rel = w.var("cs_etac2S_rel")
    paramOfInterest = RooArgSet(cs_etac2S_rel)
    if kSource=="prompt":
        bgParams = RooArgSet(a0_High, a1_High, a2_High)
        cs_etac2S_rel.setRange(0., 10.)
        # cs_etac2S_rel.setRange(0., 3e4)

    else:
        bgParams = RooArgSet(a0_High, a1_High, a2_High)
        #cs_etac2S_rel.setRange(0., 2.50)
        cs_etac2S_rel.setRange(0., 0.5)


    # n_etac, n_Jpsi, nBckgr_Low - can be completely independent from the fit of etac2S
    # we can just take gaussian constraint on n_Jpsi and others

    # it is not completely clear which variables should go to nuisances
    # subtle BUT very important
    # *rather* a0, a1, a2 should NOT go to nuisances ? to check in other analyses, etc.
    # if we want to use them - I would make gaussian constraint ?

    nuisanceParams = RooArgSet(n_hc, n_chic0, n_chic1, n_chic2, n_psi2S) # n_psi2S is not significant?
    nuis_iter = nuisanceParams.createIterator()
    var = nuis_iter.Next()
    band_size = 5.
    while var:
        if var.getVal()-band_size*var.getError() > 0:
            var.setRange(var.getVal()-band_size*var.getError(), var.getVal()+band_size*var.getError())
        else:
            var.setRange(0, var.getVal()+band_size*var.getError())
        print(var.GetName(), var.getVal()-band_size*var.getError(), var.getVal()+band_size*var.getError())
        var = nuis_iter.Next()

    # nBckgr_High should go to nuisances anyway
    bandsize_bkg = 10.
    nBckgr_High.setRange(nBckgr_High.getVal()-bandsize_bkg*nBckgr_High.getError(), nBckgr_High.getVal()+bandsize_bkg*nBckgr_High.getError())
    nuisanceParams.add(nBckgr_High)
    # nuisanceParams.add(bgParams) # it changes/breaks a lot -> only of reset range for a_i


    # can this UL be made for Extended() option ?
    # does it know about fitRange ?


    # such as BRs, efficiencies,
    # cross-section of Jpsi (if we want to extract UL on sigma_{\etac(2S)}),
    # cross-talks, yields from b-sample = everything with gaussian contraint
    # masses, widths also can be used here..


    modelConfig = RooStats.ModelConfig(w);
    modelConfig.SetPdf(totalPdf_High)
    modelConfig.SetProtoData(data_High)
    modelConfig.SetParametersOfInterest(paramOfInterest)
    modelConfig.SetNuisanceParameters(nuisanceParams)
    modelConfig.SetObservables(RooArgSet(RooArgList(Jpsi_M_High)))

    # I don't know what  ConstraintParameters are ...
    #   modelConfig.SetConstraintParameters(RooArgList(BR_Jpsi_mean))

    extFactor    = w.var("extFactor")
    globalParams = RooArgSet(extFactor)
    modelConfig.SetGlobalObservables(globalParams)

    modelConfig.SetName("ModelConfig")
    paramOfInterest.first().setVal(cs_etac2S_rel.getVal())
    modelConfig.SetSnapshot(paramOfInterest)
    getattr(w,"import")(modelConfig)
    modelConfig.Print()

    sbModel = w.obj("ModelConfig")
    poi = sbModel.GetParametersOfInterest().first()
    bModel = sbModel.Clone()
    bModel.SetName(sbModel.GetName()+"_with_poi_0")
    poi.setVal(0)
    bModel.SetSnapshot(RooArgSet(poi))

    fc = RooStats.FrequentistCalculator(data_High, bModel, sbModel)
    # do these values change anything ?
    fc.SetToys(1000,500)
    ac = RooStats.AsymptoticCalculator(data_High, bModel, sbModel)
    ac.SetOneSided(True)
    ac.SetPrintLevel(-1)

    cl_value = 0.95
    calc = RooStats.HypoTestInverter(ac)
    calc.SetConfidenceLevel(cl_value)

    calc.UseCLs(True)
    calc.SetVerbose(False)

    toymcs = calc.GetHypoTestCalculator().GetTestStatSampler()

    profll = RooStats.ProfileLikelihoodTestStat(sbModel.GetPdf())
    profll.SetOneSided(True)
    profll.SetLOffset(True)
    toymcs.SetTestStatistic(profll)

    if not sbModel.GetPdf().canBeExtended():
        toymcs.SetNEventsPerToy(1)
        print('can not be extended')

    npoints = 25
    poimin = poi.getMin()
    poimax = poi.getMax()


    calc.SetFixedScan(npoints,poimin,poimax)
    r = calc.GetInterval()
    upperLimit = r.UpperLimit()

    plot = RooStats.HypoTestInverterPlot("HTI_Result_Plot","HypoTest Scan Result",r)
    c = TCanvas("HypoTestInverter Scan")
    c.SetLogy(False)
    plot.Draw("CLb 2CL")

    #homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"

    #plot.Draw("EXP")
    c.RangeAxis(poimin, 0., poimax, 1.1)
    c.Draw()
    #c.SaveAs(homeDir + "/mass_fit/split_fit/CL_{}_{}_base_MC.root".format(kSource,kBkg))
    c.SaveAs(homeDir + "/mass_fit/split_fit/CL_{}_{}_base_MC.pdf".format(kSource,kSyst))


    # Construct binned likelihood
    nll = totalPdf_High.createNLL(data_High, RooFit.NumCPU(8), RooFit.Extended(True), RooFit.Offset(True))

    # Minimize likelihood w.r.t all parameters before making plots
    m = RooMinuit(nll)
    #m = RooMinimizer(nll)
    m.migrad()
    m.hesse()
    #m.minos()

    # Plot likelihood scan frac
    frameRS = cs_etac2S_rel.frame(RooFit.Bins(46),RooFit.Title("LL and profileLL in n_{#eta_{c}}(2S) "))
#     nll.plotOn(frameRS,RooFit.ShiftToZero())

    # The profile likelihood estimator on nll for frac will minimize nll w.r.t
    # all floating parameters except frac for each evaluation

    #pll_N_etac2S = nll.createProfile(RooArgSet(n_etac2S_rel))
    pll_N_etac2S = RooProfileLL("pll_N_etac2S", "pll_N_etac2S", nll, RooArgSet(cs_etac2S_rel))
    llhood = RooFormulaVar("llhood","exp(-0.5*pll_N_etac2S)",RooArgList(pll_N_etac2S));

    # Plot the profile likelihood in frac
    pll_N_etac2S.plotOn(frameRS,RooFit.LineColor(2))

    # Adjust frame maximum for visual clarity
    frameRS.SetMinimum(0)
    frameRS.GetXaxis().SetRangeUser(poimin, poimax)

    cNLL = TCanvas("rf605_profilell","rf605_profilell",800, 400)
    cNLL.cd(),  gPad.SetLeftMargin(0.15),  frameRS.GetYaxis().SetTitleOffset(1.4),  frameRS.Draw()

    cNLL.SaveAs(homeDir + "/mass_fit/NLL_{}_{}_base_MC.pdf".format(kSource,kSyst))

    nll.Write()
    pll_N_etac2S.Write()

    #f_nll.Write()
    #f_nll.Close()


    # sometimes it's REALLY slow
    fllhood = llhood.asTF(RooArgList(paramOfInterest))
    #npoints = 20
    int_array = np.ones(npoints+1)
    int_val0 = fllhood.Integral(poimin, poimax, 1e-3)
    fll_ul = 0.0
    poi_urs = np.linspace(poimin, poimax, npoints+1)

    for idx in range(npoints):

        int_val = fllhood.Integral(poimin, poi_urs[idx+1], 1e-6)
        int_array[idx+1] = 1.-int_val/int_val0
        if int_array[idx+1]>0.05: fll_ul = poi_urs[idx+1]

    gr_pval = TGraph(npoints+1, poi_urs, int_array)
    c.cd()
    gr_pval.SetLineColor(2)
    gr_pval.GetXaxis().SetRangeUser(poimin, poimax)
    gr_pval.Draw("same")

    #import os, sys
    #sys.stdout = open("{}/UL_{}_{}_{}.txt".format(homeDir,kSource, kNorm, kSyst), 'w' )
    fo = open("{}/UL_{}_{}_{}.txt".format(homeDir,kSource, kNorm, kSyst), 'w' )
    fo.write("Upper Limit at {} CL \n".format(cl_value))
    fo.write("Expexted: {} \n".format(r.GetExpectedUpperLimit()))
    fo.write("Observed: {} \n".format(upperLimit))
    fo.write("Bayesian: {} \n".format(fll_ul))
    #sys.stdout.close()
    fo.close()

    c.SaveAs(homeDir + "/mass_fit/split_fit/CL_{}_{}_{}_base_MC.pdf".format(kNorm,kSource,kSyst))
    c.SaveAs(homeDir + "/mass_fit/split_fit/CL_{}_{}_{}_base_MC.root".format(kNorm,kSource,kSyst))






homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"

if __name__ == "__main__":

    # kSources = ["fromB","prompt"]
    kSources = ["fromB"]
    kNorm = 'jpsi'

    #nConfs = ["CB","Exp","eff_pppi0","Gamma","effPP","effPB","effBB","effBP","rEtaToJpsi"]
    kSysts = ["Exp","Gamma","eff","CB","BR"]#,"Resolution"]
    kSysts = ["cross_talk"]
    kSysts = ["Base"]
    for kSyst in kSysts:
        for kSource in kSources:
            w = perform_splitted_fit(kSource, kNorm, kSyst)
            # nameWksp = homeDir + "/mass_fit/split_fit/Wksp_{}_{}_{}_base_MC.root".format(kNorm,kSource,kSyst)
            # file_w = TFile(nameWksp)
            # w = file_w.Get("w")
            # calcUL_HighRangeOnly(w,kSource,kSyst,kNorm)
            # file_w.Close()
            del w
