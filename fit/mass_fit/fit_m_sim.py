from ROOT import *
from ROOT.RooFit import *
from ROOT.RooStats import *
# config = RooStats.GetGlobalRooStatsConfig()
# config.useEvalErrorWall = False

# ROOT.Math.MinimizerOptions.SetDefaultMinimizer("Minuit2","Minimize");
# ROOT.Math.MinimizerOptions.SetDefaultMaxFunctionCalls(100000)
# ROOT.Math.MinimizerOptions.SetDefaultMaxFunctionCalls(300000)

import drawModule_splitFit as dm
import oniaConstLib as cl
# from setup_model import *
# from model import *
from model_sim import *
gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")

import numpy as np

homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"


    # to make it faster one can combine Br_etac2s x / Br_Jpsi * effratio
    # this works ONLY for external constraints i.e. uncorrelated ones
    # in principle n_Jpsi can go here as well
    # systematics to be added


def get_data_h(w, kSource):

    # nameWksp = dataDir + "/wksps/wksp_data_{}_ProbNNp_06_wide.root".format(kSource)
    nameWksp = "{}/wksps/wksp_data_{}_ProbNNp_06.root".format(dataDir,kSource)
    file_w = TFile(nameWksp,"read")
    w_in = file_w.Get("w").Clone()
    file_w.Close()

    dataL   = w_in.data("dh_Low")
    dataH   = w_in.data("dh_High")
    Jpsi_M = w_in.var("Jpsi_M")

    histL = dataL.createHistogram("histL",Jpsi_M)
    histH = dataH.createHistogram("histH",Jpsi_M)

    histL.Print()
    histH.Print()

    # Jpsi_M_Low  = RooRealVar("Jpsi_M_Low", "Jpsi_M_Low",  dm.minM_Low, dm.maxM_Low)
    # Jpsi_M_High = RooRealVar("Jpsi_M_High","Jpsi_M_High", dm.minM_High, dm.maxM_High)

    # dh_Low  = RooDataHist("dh_Low_{}".format(kSource),"dh",  RooArgList(Jpsi_M_Low), histL)
    # dh_High = RooDataHist("dh_High_{}".format(kSource),"dh",  RooArgList(Jpsi_M_High), histH)
    dh_Low  = RooDataHist("dh_Low_{}".format(kSource),"dh",  RooArgList(Jpsi_M), histL)
    dh_High = RooDataHist("dh_High_{}".format(kSource),"dh",  RooArgList(Jpsi_M), histH)

    # getattr(w,"import")(Jpsi_M_Low)
    # getattr(w,"import")(Jpsi_M_High)
    getattr(w,"import")(dh_Low)
    getattr(w,"import")(dh_High)

    print("INFO: Histogram import completed")

def print_to_file(nameFile, fitRes_list):

    import os, sys
    save = os.dup( sys.stdout.fileno() )
    newout = open(nameFile, "w" )
    os.dup2( newout.fileno(), sys.stdout.fileno() )

    for fr in fitRes_list:
        fr.Print("v")
        fr.correlationMatrix().Print()

    os.dup2( save, sys.stdout.fileno() )
    newout.close()
    print("{} is created".format(nameFile))

def perform_fit(kNorm="jpsi", kSyst="Base"):

    src_list = ["prompt","fromB"]

    homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
    gStyle.SetStatFormat("4.3f");

    Jpsi_M = RooRealVar("Jpsi_M", "Jpsi_M",  dm.minM_Low, dm.maxM_High)
    Jpsi_M.setBins(int(dm.binN_Tot_Fit/2))

    w = RooWorkspace("w",True)
    getattr(w,"import")(Jpsi_M)

    for kSource in src_list:
        get_data_h(w,kSource)

    ''' attempt to optimize this code '''

    all_states = ["jpsi", "etac", "chic0", "chic1", "chic2", "hc", "psi2S", "etac2S"]
    low_states = ["jpsi", "etac"]
    high_states = ["chic0","chic1","chic2","hc", "etac2S","psi2S"]
    # high_states = ["etac2S","psi2S"]

    kCB = False if kSyst!="CB" else True

    setMW(w, all_states)

    for name in low_states:
        fill_workspace(w, name, "Low", ref=kNorm, kCB=kCB, kSyst=kSyst)
    for name in high_states:
        fill_workspace(w, name, "High", ref=kNorm, kCB=kCB, kSyst=kSyst)

    for kSource in src_list:
        combine(w, low_states, "Low", kSource, kSyst, split_lvl="none")
        combine(w, high_states, "High", kSource, kSyst, split_lvl="none")

    create_sample_split(w)

    ''' ----------------------------- '''

    model_High_fromB = w.pdf("model_High_fromB")
    model_Low_fromB = w.pdf("model_Low_fromB")
    model_High_prompt = w.pdf("model_High_prompt")
    model_Low_prompt = w.pdf("model_Low_prompt")
    # model_High_fromB = w.pdf("model_High_fromB_constr")

    data_h_pr = w.data("dh_High_prompt")
    data_h_fb = w.data("dh_High_fromB")
    data_l_pr = w.data("dh_Low_prompt")
    data_l_fb = w.data("dh_Low_fromB")


    sample = w.cat("sample")
    simPdf = w.pdf("simPdf")
    # simPdf = w.pdf("simPdf_constr")
    combData = w.data("combData")


    extFactor_pr = w.var("extFactor_prompt")
    extFactor_fb = w.var("extFactor_fromB")

    constr_pr = w.pdf("constr_ext_prompt")
    constr_fb = w.pdf("constr_ext_fromB")

    extFactorObs_pr = w.var("extFactorObs_prompt")
    extFactorObs_fb = w.var("extFactorObs_fromB")
    extFactorObs_pr.setAttribute("MyGlobalObservable") ;
    extFactorObs_fb.setAttribute("MyGlobalObservable") ;
    simPdf.setStringAttribute("GlobalObservablesTag","MyGlobalObservable") ;    

    mass = w.var("mass_etac2S")
    gamma = w.var("gamma_etac2S")

    llist = RooLinkedList()
    llist.Add(RooCmdArg(RooFit.Extended(True)))
    llist.Add(RooCmdArg(RooFit.Save(True)))
    llist.Add(RooCmdArg(RooFit.Offset(True)))
    # llist.Add(RooCmdArg(RooFit.NumCPU(8)))
    # llist.Add(RooCmdArg(RooFit.Minos(True)))
    #llist.Add(RooCmdArg(RooFit.Strategy(2)))

    #llist_ext = llist.Clone()
    #llist_ext.Add(RooCmdArg(RooFit.Minos(True)))
    extFactor_pr.setConstant(True)
    extFactor_fb.setConstant(True)

    w.var("n_jpsi_prompt").setVal(1e5)
    w.var("n_etac_prompt").setVal(1e5)
    w.var("n_bkg_Low_prompt").setVal(1e8)

    # w.var("mass_jpsi").setConstant(False)
    w.var("sigma_jpsi_1").setVal(8.1)
    fitresult_l_fb = model_Low_fromB.fitTo(data_l_fb, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange_Low_fromB"), RooFit.Strategy(1))
    fitresult_l_fb = model_Low_fromB.fitTo(data_l_fb, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange_Low_fromB"), RooFit.Strategy(1))
    w.var("n_jpsi_fromB").setConstant(True)
    fitresult_l_pr = model_Low_prompt.fitTo(data_l_pr, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange_Low_prompt"),RooFit.Strategy(1))
    fitresult_l_pr = model_Low_prompt.fitTo(data_l_pr, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange_Low_prompt"),RooFit.Strategy(1))
    w.var("n_jpsi_prompt").setConstant(True)
    w.var("n_etac_prompt").setConstant(True)
    w.var("sigma_jpsi_1").setConstant(True)
    w.var("mass_psi2S").setConstant(False)
    # constraints = RooArgSet(mass, gamma, extFactor_fb, extFactor_pr)    
    fitresult_h_fb = model_High_fromB.fitTo(data_h_fb, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange_High_fromB"), RooFit.Strategy(1))
    fitresult_h_fb = model_High_fromB.fitTo(data_h_fb, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange_High_fromB"), RooFit.Strategy(1))

    # w.var("n_psi2S_fromB").setConstant(True)
    w.var("mass_psi2S").setConstant(True)
    fitresult_h_pr = model_High_prompt.fitTo(data_h_pr, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange_High_prompt"), RooFit.Strategy(1))
    fitresult_h_pr = model_High_prompt.fitTo(data_h_pr, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange_High_prompt"), RooFit.Strategy(1))
    fitresult_h_pr = model_High_prompt.fitTo(data_h_pr, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Range("fitRange_High_prompt"), RooFit.Strategy(2))

    input()
    # constraints = RooArgList(mass, gamma, extFactor_pr, extFactor_fb)    
    constraints = RooArgList(extFactor_pr, extFactor_fb)    
    # constraints.add(extFactor_pr)
    # constraints.add(extFactor_fb)
    # constraints.add(mass)
    # constraints.add(gamma)
    llist.Add(RooCmdArg(RooFit.Constrain(constraints)))
    llist.Add(RooCmdArg(RooFit.Range("fitRange")))
    llist.Add(RooCmdArg(RooFit.SplitRange(True)))

    for source in ["prompt","fromB"]:
    # # for source in ["fromB"]:

        if source=="prompt":
            n_bgpar = 3 
        else:
            n_bgpar = 2
    
        for i in range(n_bgpar):            
            a = w.var("a{}_Low_{}".format(i, source))
            a.setRange(a.getVal()-abs(a.getVal()), a.getVal()+abs(a.getVal()))
    #         w.var("a{}_Low_{}".format(i, source)).setConstant(True)
    #         w.var("a{}_Low_{}".format(i, source)).Print()
            # if source=="prompt":
                # w.var("a{}_High_{}".format(i, source)).setConstant(True)
        # n_bgpar = 2
        # for i in range(n_bgpar):
            # w.var("a{}_High_{}".format(i, source)).setConstant(True)

    # input()

    # w.var("n_jpsi_prompt").setConstant(False)
    w.var("n_jpsi_fromB").setConstant(False)
    w.var("n_etac_fromB").setConstant(False)
    w.var("n_jpsi_prompt").setConstant(False)
    w.var("n_etac_prompt").setConstant(False)
    fitresult = simPdf.fitTo(combData, llist)
    w.var("sigma_jpsi_1").setConstant(False)
    # llist.Add(RooCmdArg(RooFit.Strategy(2)))
    # fitresult = simPdf.fitTo(combData, llist)
    # w.var("mass_jpsi").setConstant(False)
    # w.var("mass_etac").setConstant(False)
    # w.var("mass_psi2S").setConstant(False)
    fitresult = simPdf.fitTo(combData, llist)
    extFactor_pr.setConstant(False)
    extFactor_fb.setConstant(False)
    # mass.setConstant(False)
    # gamma.setConstant(False)
    fitresult = simPdf.fitTo(combData, llist)
    # llist.Remove(llist.find("Strategy"))
    llist.Add(RooCmdArg(RooFit.Minos(True)))
    fitresult = simPdf.fitTo(combData, llist)
    fitresult = simPdf.fitTo(combData, llist)
    fitresult = simPdf.fitTo(combData, llist)


    names  = ["Low","High"]
    prods  = ["prompt","fromB"]
    for kSource in prods:
        namePic  = "{}/mass_fit/sim_fit/Plot_{}_{}_{}_ProbNNp_06.pdf".format(homeDir, kNorm, kSource, kSyst)
        frames, resids, pulls = [], [], []
        for reg in names:

            frame, resid, pull = dm.set_frame(w,reg,kSource)
            frames.append(frame)
            resids.append(resid)
            pulls.append(pull)

        c = dm.set_canvas(names, frames, resids, pulls)
        c.SaveAs(namePic)


    nameWksp = "{}/mass_fit/sim_fit/Wksp_{}_sim_{}_ProbNNp_06.root".format(homeDir,kNorm,kSyst)
    nameTxt  = "{}/mass_fit/sim_fit/Result_{}_sim_{}_ProbNNp_06.txt".format(homeDir,kNorm,kSyst)
    w.writeToFile(nameWksp)

    print_to_file(nameTxt, [fitresult])

    print("Fit performed successfully!")
    return(w)




homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"

if __name__ == "__main__":

    # kSources = ["fromB","prompt"]
    kSources = ["fromB"]
    kNorm = 'jpsi'

    #nConfs = ["CB","Exp","eff_pppi0","Gamma","effPP","effPB","effBB","effBP","rEtaToJpsi"]
    kSysts = ["Exp","Gamma","eff","CB","BR"]#,"Resolution"]
    kSysts = ["cross_talk"]
    kSysts = ["Base"]
    for kSyst in kSysts:
        w = perform_fit(kNorm, kSyst)
        # nameWksp = homeDir + "/mass_fit/split_fit/Wksp_{}_{}_{}_base_MC.root".format(kNorm,kSource,kSyst)
        # file_w = TFile(nameWksp)
        # w = file_w.Get("w")
        # calcUL_HighRangeOnly(w,kSource,kSyst,kNorm)
        # file_w.Close()
        del w
