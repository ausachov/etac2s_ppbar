#from ROOT import gROOT, gStyle
#from ROOT import TFile, TChain, TTree, TH1D, TGraph, TCanvas, TCut, TMath
#from ROOT import RooFit, RooStats
from ROOT import *
from ROOT.RooFit import *

from drawModule import *
import oniaConstLib as cl

from array import array
import numpy as np

#homeDir = "/afs/cern.ch/user/v/vazhovko/private/etac2s_ppbar/"
#dataDir = "/eos/user/v/vazhovko/etac2s_ppbar/"
homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

part_dict = {
    'pi': cl.M_PION,
    'K' : cl.M_KAON,
    'p' : cl.M_PROTON

}


def plot_histo(kRange, particle1, particle2):

    dirName  = dataDir +"Data_{}_NoPID/".format(kRange)


    nt  =  TChain("DecayTree")
    nt.Add(dirName+"/Etac2sDiProton_{}_2018*.root".format(kRange))

    #tree_Low = TTree()
    #tree_High = TTree()

    cut_FD = ""
    #if kSource=="prompt":
        #cut_FD = TCut("(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08")# & Jpsi_FDCHI2_OWNPV > 49"
    #else:
        #cut_FD = TCut("(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16")# & Jpsi_FDCHI2_OWNPV > 49"

    cut_PT      = TCut("Jpsi_PT > 6500 && Jpsi_PT < 20000")
    #cut_PID     = TCut("ProtonP_ProbNNp > 0.7 && ProtonM_ProbNNp > 0.7 ")
    #cut_PID     = TCut("ProtonP_ProbNNk < 0.6 && ProtonM_ProbNNpi < 0.6 && ProtonP_ProbNNp > 0.7 && ProtonM_ProbNNp > 0.7 && ProtonP_ProbNNpi < 0.4 && ProtonM_ProbNNpi < 0.4 ")

    totCut = TCut(cut_FD + cut_PT)
    #tree = nt_Low.CopyTree(totCut)

    #if kRange=="Low":
        #hh = TH1D("hh_{}".format(kRange),"hh_{}".format(kRange), binN_Tot_Fit, minM_Low, maxM_Low)
    #else:
        #hh = TH1D("hh_{}".format(kRange),"hh_{}".format(kRange), binN_Tot_Fit, minM_High, maxM_High)

    hh_prompt      = TH1D("hh_{}_prompt".format(kRange),"hh_{}".format(kRange), binN_Tot, minM_Low, maxM_High)
    hh_Reco_prompt = TH1D("hh_Reco_{}_prompt".format(kRange),"hh_Reco_{}".format(kRange), binN_Tot, minM_Low, maxM_High)

    hh_fromB      = TH1D("hh_{}_fromB".format(kRange),"hh_{}".format(kRange), binN_Tot, minM_Low, maxM_High)
    hh_Reco_fromB = TH1D("hh_Reco_{}_fromB".format(kRange),"hh_Reco_{}".format(kRange), binN_Tot, minM_Low, maxM_High)

    Jpsi_ENDVERTEX_Z = array( 'd', [0])
    Jpsi_OWNPV_Z = array( 'd', [0])
    Jpsi_MM = array( 'd', [0])
    Jpsi_PZ = array( 'd', [0])
    ProtonP_IPCHI2_OWNPV = array( 'd', [0])
    ProtonM_IPCHI2_OWNPV = array( 'd', [0])

    Jpsi_M  = array( 'd', [0])
    ProtonM_P  = array( 'd', [0])
    ProtonM_PT = array( 'd', [0])
    ProtonP_P  = array( 'd', [0])
    ProtonP_PT = array( 'd', [0])

    nt.SetBranchAddress("Jpsi_ENDVERTEX_Z",Jpsi_ENDVERTEX_Z)
    nt.SetBranchAddress("Jpsi_OWNPV_Z",Jpsi_OWNPV_Z)
    nt.SetBranchAddress("Jpsi_MM",Jpsi_MM)
    nt.SetBranchAddress("Jpsi_PZ",Jpsi_PZ)
    nt.SetBranchAddress("ProtonP_IPCHI2_OWNPV",ProtonP_IPCHI2_OWNPV)
    nt.SetBranchAddress("ProtonM_IPCHI2_OWNPV",ProtonM_IPCHI2_OWNPV)

    nt.SetBranchAddress("Jpsi_m_scaled",Jpsi_M)
    nt.SetBranchAddress("ProtonM_P",ProtonM_P)
    nt.SetBranchAddress("ProtonM_PT",ProtonM_PT)
    nt.SetBranchAddress("ProtonP_P",ProtonP_P)
    nt.SetBranchAddress("ProtonP_PT",ProtonP_PT)

    n_entries = nt.GetEntries()

    m_1 = part_dict[particle1]
    m_2 = part_dict[particle2]

    for i_entry in range(n_entries):

        nt.GetEntry(i_entry)
        #if (i_entry/1000):
            #print(i_entry)
        if (Jpsi_ENDVERTEX_Z[0]-Jpsi_OWNPV_Z[0])*Jpsi_MM[0]/Jpsi_PZ[0] < 0.08:

            ProtonM_L = (ProtonM_P[0]**2 - ProtonM_PT[0]**2)
            ProtonP_L = (ProtonP_P[0]**2 - ProtonP_PT[0]**2)
            ProtonM_E = (ProtonM_P[0]**2 + m_1**2)
            ProtonP_E = (ProtonP_P[0]**2 + m_2**2)
            Jpsi_M_misID = (m_1**2 + m_2**2 + 2*((ProtonM_E*ProtonP_E)**0.5 - (ProtonM_L*ProtonP_L)**0.5 - ProtonM_PT[0]*ProtonP_PT[0]))**0.5

            hh_prompt.Fill(Jpsi_M_misID)
            hh_Reco_prompt.Fill(Jpsi_M[0])

        elif ProtonP_IPCHI2_OWNPV[0]>16 and ProtonM_IPCHI2_OWNPV[0]>16:

            ProtonM_L = (ProtonM_P[0]**2 - ProtonM_PT[0]**2)**0.5
            ProtonP_L = (ProtonP_P[0]**2 - ProtonP_PT[0]**2)**0.5
            ProtonM_E = (ProtonM_P[0]**2 + m_1**2)**0.5
            ProtonP_E = (ProtonP_P[0]**2 + m_2**2)**0.5
            Jpsi_M_misID = (m_1**2 + m_2**2 + 2*(ProtonM_E*ProtonP_E - ProtonM_PT[0]*ProtonP_PT[0] - ProtonM_L*ProtonP_L))**0.5

            hh_fromB.Fill(Jpsi_M_misID)
            hh_Reco_fromB.Fill(Jpsi_M[0])

    name_plot = "Plot_misID_{}_{}{}".format(kSource,particle1,particle2)

    c1 = TCanvas("c","c", 600, 400)
    c1.cd()
    hh_prompt.Draw()
    hh_Reco_prompt.SetLineColor(2)
    hh_Reco_prompt.Draw("same")

    c1.SaveAs(name_plot+"_prompt.pdf")
    c1.SaveAs(name_plot+"_prompt.root")

    c2 = TCanvas("c","c", 600, 400)
    c2.cd()
    hh_fromB.Draw()
    hh_Reco_fromB.SetLineColor(2)
    hh_Reco_fromB.Draw("same")

    c2.SaveAs(name_plot+"_fromB.pdf")
    c2.SaveAs(name_plot+"_fromB.root")

    print( "DATAHISTS READ SUCCESSFULLY")

#plot_histo("prompt","Low","p","p")
plot_histo("High","p","pi")
plot_histo("High","pi","p")
#get_data_h(w, "prompt")
#w = RooWorkspace("w",True)