from ROOT import *
from ROOT.RooFit import *
from ROOT.RooStats import *
gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+")

import numpy as np
#from math import abs

binWidth = 0.2
binWidthDraw = 0.2

minM_Low = 2850
maxM_Low = 3250
binN_Low = int((maxM_Low-minM_Low)/binWidth)

minM_High = 3320
maxM_High = 3780
binN_High = int((maxM_High-minM_High)/binWidth)



binning = RooFit.Binning(93, minM_Low, maxM_High)
binning_Jpsi = RooFit.Binning(40, minM_Low, maxM_Low)
binning_etac2s = RooFit.Binning(46, minM_High, maxM_High)
mrkSize = RooFit.MarkerStyle(7)
lineWidth1 = RooFit.LineWidth(1)
lineWidth2 = RooFit.LineWidth(2)
lineStyle1 = RooFit.LineStyle(2)
lineStyle2 = RooFit.LineStyle(9)
lineColor1 = RooFit.LineColor(6)
lineColor2 = RooFit.LineColor(8)
name = RooFit.Name
cut = RooFit.Cut

#homeDir = '/afs/cern.ch/user/v/vazhovko/private/etac2s_ppbar/'
#dataDir = '/eos/user/v/vazhovko/etac2s_ppbar/'
homeDir = '/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/'
dataDir = '/sps/lhcb/zhovkovska/etac2s_ppbar/'


def get_data_h(w, kSource):

    dirName_Low  = dataDir +"Data_Low/"
    dirName_High = dataDir +"Data_High/"


    nt_Low  =  TChain("DecayTree")
    nt_High =  TChain("DecayTree")

    nt_Low.Add(dirName_Low+'/Etac2sDiProton_Low_2018*.root')
    nt_High.Add(dirName_High+'/Etac2sDiProton_High_2018*.root')

    #tree_Low = TTree()
    #tree_High = TTree()

    #cut_Jpsi_Y = TCut('0.5*log((Jpsi_PE + Jpsi_PZ)/(Jpsi_PE-Jpsi_PZ)) > 2 & 0.5*log((Jpsi_PE + Jpsi_PZ)/(Jpsi_PE-Jpsi_PZ)) < 4.5')
    #cut_Ghost = TCut('ProtonP_TRACK_GhostProb<0.2 & ProtonM_TRACK_GhostProb<0.2')
    cut_FD = ''
    if kSource=='prompt':
        cut_FD = TCut('(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08')# & Jpsi_FDCHI2_OWNPV > 49'
    else:
        cut_FD = TCut('(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16')# & Jpsi_FDCHI2_OWNPV > 49'

    cut_PT      = TCut('Jpsi_PT > 6500')
    #cut_ProbNNp = TCut('ProtonP_ProbNNp > 0.95 && ProtonM_ProbNNp > 0.95 ')

    totCut = TCut(cut_FD + cut_PT)

    hh_Low = TH1D('hh_Low','hh_Low', 3720, minM_Low, maxM_High)
    hh_High = TH1D('hh_High','hh_High', 3720, minM_Low, maxM_High)

    #hh_High.SetBinContent(3161, hh_High.GetBinContent(3168))
    #hh_High.SetBinContent(3160, hh_High.GetBinContent(3168))
    #hh_High.SetBinContent(3159, hh_High.GetBinContent(3168))
    #hh_High.SetBinContent(3158, hh_High.GetBinContent(3168))
    #hh_High.SetBinContent(3157, hh_High.GetBinContent(3168))
    #hh_Low.Smooth(5)
    #hh_High.Smooth(5)

    nt_Low.Draw('Jpsi_m_scaled>>hh_Low',totCut.GetTitle(),'goff')
    nt_High.Draw('Jpsi_m_scaled>>hh_High',totCut.GetTitle(),'goff')

    Jpsi_M = RooRealVar('Jpsi_M',"Jpsi_M", minM_Low, maxM_High)

    dh_Low = RooDataHist("dh_Low","dh_Low", RooArgList(Jpsi_M), hh_Low)
    dh_High = RooDataHist("dh_High","dh_High", RooArgList(Jpsi_M), hh_High)
    getattr(w,'import')(dh_Low)
    getattr(w,'import')(dh_High)

    print "DATA\'S READ SUCCESSFULLY"


#def get_data_h(w, kSource):

    #dirName_Low  = dataDir +"Data_Low_Proton_ProbNNp/{}".format(kSource)
    #dirName_High = dataDir +"Data_High_Proton_ProbNNp/{}".format(kSource)

    #f_Low  = TFile(dirName_Low+'/Etac2sDiProton_Low_2018_Bin_2.root',"READ")
    #f_High = TFile(dirName_High+'/Etac2sDiProton_High_2018_Bin_2.root',"READ")

    ##hh_Low = TH1D('hh_Low','hh_Low', binN_Low, minM_Low, maxM_Low)
    ##hh_High = TH1D('hh_High','hh_High', binN_High, minM_High, maxM_High)

    #hh_Low  = TH1D()
    #hh_High = TH1D()

    #hh_Low  = f_Low.Get("Jpsi_M")
    #hh_High = f_High.Get("Jpsi_M")

    #hh_Low.SetBins(4650, minM_Low, maxM_High)
    ##hh_High.SetBins(4650, minM_Low, maxM_High)

    #Jpsi_M = RooRealVar('Jpsi_M',"Jpsi_M", minM_Low, maxM_High)

    #dh_Low = RooDataHist("dh_Low","dh_Low", RooArgList(Jpsi_M), hh_Low)
    #dh_High = RooDataHist("dh_High","dh_High", RooArgList(Jpsi_M), hh_High)
    #getattr(w,'import')(dh_Low)
    #getattr(w,'import')(dh_High)


    #print "DATA\'S READ SUCCESSFULLY"


def choose_bkg(optList, regName, argList):
    return {

        'prompt cheb' : RooChebychev ("bkg_{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2),argList.at(3))),
        'fromB cheb'  : RooChebychev ("bkg_{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2))),
        'prompt exp'  : RooGenericPdf("bkg_{}".format(regName),"background","TMath::Exp(-(@0-3550.)/200.*@1)*(1.+@2*(@0-3550.)/200.+@3*(@0-3550.)*(@0-3550.)/200./200.)", RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3))) ,
        'fromB exp'   : RooGenericPdf("bkg_{}".format(regName),"background","TMath::Exp(-(@0-3550.)/200.*@1)*(1.+@2*(@0-3550.)/200)", RooArgList(argList.at(0), argList.at(1),argList.at(2)))

    }[optList[0] + ' ' + optList[1]]


def fillRelWorkspace(w, kSource, kBkg, kNorm='Jpsi'):

    Jpsi_M = w.var("Jpsi_M")
    #Jpsi_M.setBins(4650,"cache")


    gamma_etac2s = 11.3
    gamma_etac = 32.0
    gamma_chic0 = 10.8
    gamma_chic1 = 0.84
    gamma_chic2 = 1.97
    gamma_hc = 0.70
    gamma_Psi3770 = 27.2

    m_Jpsi = 3096.9
    m_etac2s = 3637.6
    m_etac = 2983.9
    m_chic0 = 3414.71
    m_chic1 = 3510.67
    m_chic2 = 3556.17
    m_hc = 3525.38
    m_Psi2s = 3686.097
    m_Psi3770 = 3773.13

    m_res_etac   = 113.0
    m_res_chic0  = 317.81
    m_res_chic1  = 413.77
    m_res_chic2  = 459.27
    m_res_hc     = 428.9
    m_res_etac2s = 540.7
    m_res_Psi2s  = 589.197


    ratioEtac2sToJpsi = 1.0
    ratioNtoW = 0.21
    ratioArea = 0.95

    rEtac2sToJpsi = RooRealVar('rEtac2sToJpsi','rEtac2sToJpsi', ratioEtac2sToJpsi)#, 0.01, 5.0)
    rNarToW = RooRealVar("rNarToW","rNarToW",ratioNtoW)
    rG1toG2 = RooRealVar("rG1toG2","rG1toG2",ratioArea)

    n_Jpsi = RooRealVar('n_Jpsi','num of J/Psi', 2e3, 10, 1.e6)
    n_etac = RooRealVar('n_etac','num of etac', 1e4, 10, 1.e6)
    n_etac2sRel = RooRealVar("n_etac2sRel","num of Etac", 1e-2, 0, 0.15)
    #n_etac2s = RooRealVar('n_etac2s','num of etac2s', 1e3, 10, 1.e4)
    if kNorm=='Jpsi':
        n_etac2s = RooFormulaVar("n_etac2s","num of Etac","@0*@1", RooArgList(n_etac2sRel,n_Jpsi))
    else:
        n_etac2s = RooFormulaVar("n_etac2s","num of Etac","@0*@1", RooArgList(n_etac2sRel,n_etac))


    n_chic0 = RooRealVar('n_chic0','num of chic0', 1e3, 0, 1.e4)
    n_chic1 = RooRealVar('n_chic1','num of chic1', 1e3, 0, 1.e4)
    n_chic2 = RooRealVar('n_chic2','num of chic2', 1e3, 0, 1.e4)
    n_hc = RooRealVar('n_hc','num of hc', 1e3, 0, 1.e4)
    n_Psi2s = RooRealVar('n_Psi2s','num of Psi2s', 1e3, 0, 1.e4)
    n_Psi3770 = RooRealVar('n_Psi3770','num of Psi3770', 1e1, 0, 1.e5)
    #n_Jpsi.setVal(0.); n_Jpsi.setConstant()
    #n_etac.setVal(0.); n_etac.setConstant()
    #n_chic0.setVal(0.); n_chic0.setConstant()
    #n_chic1.setVal(0.); n_chic1.setConstant()
    #n_chic2.setVal(0.); n_chic2.setConstant()
    #n_hc.setVal(0.); n_hc.setConstant()
    #n_Psi2s.setVal(0.); n_Psi2s.setConstant()
    n_Psi3770.setVal(0.); n_Psi3770.setConstant()


    n_etac_1 = RooFormulaVar("n_etac_1","num of Etac","@0*@1",RooArgList(n_etac,rG1toG2))
    n_etac_2 = RooFormulaVar("n_etac_2","num of Etac","@0-@1",RooArgList(n_etac,n_etac_1))
    n_Jpsi_1 = RooFormulaVar("n_Jpsi_1","num of J/Psi","@0*@1",RooArgList(n_Jpsi,rG1toG2))
    n_Jpsi_2 = RooFormulaVar("n_Jpsi_2","num of J/Psi","@0-@1",RooArgList(n_Jpsi,n_Jpsi_1))

    n_etac2s_1 = RooFormulaVar("n_etac2s_1","num of Etac","@0*@1",RooArgList(n_etac2s,rG1toG2))
    n_etac2s_2 = RooFormulaVar("n_etac2s_2","num of Etac","@0-@1",RooArgList(n_etac2s,n_etac2s_1))

    n_chic0_1 = RooFormulaVar("n_chic0_1","num of Etac","@0*@1",RooArgList(n_chic0,rG1toG2))
    n_chic0_2 = RooFormulaVar("n_chic0_2","num of Etac","@0-@1",RooArgList(n_chic0,n_chic0_1))
    n_chic1_1 = RooFormulaVar("n_chic1_1","num of Etac","@0*@1",RooArgList(n_chic1,rG1toG2))
    n_chic1_2 = RooFormulaVar("n_chic1_2","num of Etac","@0-@1",RooArgList(n_chic1,n_chic1_1))
    n_chic2_1 = RooFormulaVar("n_chic2_1","num of Etac","@0*@1",RooArgList(n_chic2,rG1toG2))
    n_chic2_2 = RooFormulaVar("n_chic2_2","num of Etac","@0-@1",RooArgList(n_chic2,n_chic2_1))
    n_hc_1 = RooFormulaVar("n_hc_1","num of Etac","@0*@1",RooArgList(n_hc,rG1toG2))
    n_hc_2 = RooFormulaVar("n_hc_2","num of Etac","@0-@1",RooArgList(n_hc,n_hc_1))
    n_Psi2s_1 = RooFormulaVar("n_Psi2s_1","num of J/Psi","@0*@1",RooArgList(n_Psi2s,rG1toG2))
    n_Psi2s_2 = RooFormulaVar("n_Psi2s_2","num of J/Psi","@0-@1",RooArgList(n_Psi2s,n_Psi2s_1))


    nSignal_Low = RooFormulaVar("nSignal_Low","num of Etac","@0+@1", RooArgList(n_etac,n_Jpsi))
    nSignal_High = RooFormulaVar("nSignal_High","num of Etac","@0+@1+@2+@3+@4+@5+@6", RooArgList(n_etac2s,n_Psi2s,n_chic0,n_chic1,n_chic2,n_hc,n_Psi3770))

    nBckgr_High = RooRealVar("nBckgr_High","num of bckgr",7e7,1e4,1.e+9)
    nBckgr_Low = RooRealVar("nBckgr_Low","num of bckgr",7e7,1e4,1.e+9)

    #Psi

    mass_Jpsi = RooRealVar("mass_Jpsi","mean of gaussian",m_Jpsi, 3030, 3150)
    mass_Jpsi.setConstant()

    #mass_Psi2s = RooRealVar("mass_Psi2s","mean of gaussian",m_Psi2s, 3680, 3692)
    #mass_Psi2s.setConstant()
    mass_res_Psi2s = RooRealVar("mass_res_Psi2s","mean of gaussian", m_res_Psi2s)
    mass_Psi2s     = RooFormulaVar("mass_Psi2s","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_Psi2s))

    mass_Psi3770 = RooRealVar("mass_Psi3770","mean of gaussian",m_Psi3770)
    mass_Psi3770.setConstant()
    gamma_Psi3770 = RooRealVar('gamma_Psi3770','width of Br-W', gamma_Psi3770)
    spin_Psi3770 = RooRealVar('spin_Psi3770','spin_Psi3770', 1. )


    #eta_c

    #mass_etac = RooRealVar("mass_etac","mean of gaussian", m_etac, 2980, 2988)
    #mass_etac.setConstant()
    mass_res_etac = RooRealVar("mass_res_etac","mean of gaussian", m_res_etac, 100, 125)
    mass_res_etac.setConstant()
    mass_etac     = RooFormulaVar("mass_etac","mean of gaussian","@0-@1",RooArgList(mass_Jpsi,mass_res_etac))
    gamma_etac = RooRealVar('gamma_etac','width of Br-W', gamma_etac)
    spin_etac = RooRealVar('spin_etac','spin_eta', 0. )

    radius = RooRealVar('radius','radius', 1.)
    proton_m = RooRealVar('proton_m','proton mass', 938.3 )

    #mass_etac2s = RooRealVar("mass_etac2s","mean of gaussian", m_etac2s)
    #mass_etac2s.setConstant()
    mass_res_etac2s = RooRealVar("mass_res_etac2s","mean of gaussian", m_res_etac2s, 530, 560)
    mass_res_etac2s.setConstant()
    mass_etac2s     = RooFormulaVar("mass_etac2s","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_etac2s))
    gamma_etac2s = RooRealVar('gamma_etac2s','width of Br-W', gamma_etac2s)
    spin_etac2s = RooRealVar('spin_etac2s','spin_eta', 0. )


    #chi_c

    #mass_chic0 = RooRealVar("mass_chic0","mean of gaussian", m_chic0)
    #mass_chic0.setConstant()
    mass_res_chic0 = RooRealVar("mass_res_chic0","mean of gaussian", m_res_chic0)
    mass_chic0     = RooFormulaVar("mass_chic0","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_chic0))
    gamma_chic0 = RooRealVar('gamma_chic0','width of Br-W', gamma_chic0)
    spin_chic0 = RooRealVar('spin_chic0','spin_chic0', 0. )

    #mass_chic1 = RooRealVar("mass_chic1","mean of gaussian", m_chic1)
    #mass_chic1.setConstant()
    mass_res_chic1 = RooRealVar("mass_res_chic1","mean of gaussian", m_res_chic1)
    mass_chic1     = RooFormulaVar("mass_chic1","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_chic1))
    gamma_chic1 = RooRealVar('gamma_chic1','width of Br-W', gamma_chic1)
    spin_chic1 = RooRealVar('spin_chic1','spin_chic1', 1. )

    #mass_chic2 = RooRealVar("mass_chic2","mean of gaussian", m_chic2)
    #mass_chic2.setConstant()
    mass_res_chic2 = RooRealVar("mass_res_chic2","mean of gaussian", m_res_chic2)
    mass_chic2     = RooFormulaVar("mass_chic2","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_chic2))
    gamma_chic2 = RooRealVar('gamma_chic2','width of Br-W', gamma_chic2)
    spin_chic2 = RooRealVar('spin_chic2','spin_chic2', 2. )

    #h_c

    #mass_hc = RooRealVar("mass_hc","mean of gaussian", m_hc)
    #mass_hc.setConstant()
    mass_res_hc = RooRealVar("mass_res_hc","mean of gaussian", m_res_hc)
    mass_hc  = RooFormulaVar("mass_hc","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_hc))
    gamma_hc = RooRealVar('gamma_hc','width of Br-W', gamma_hc)
    spin_hc = RooRealVar('spin_hc','spin_hc', 1. )


    sigma_Jpsi    = RooRealVar('sigma_Jpsi','width of gaussian', 9., 0.1, 50.)
    sigma_etac    = RooFormulaVar('sigma_etac','width of gaussian','@0*({}/{})**0.5'.format(m_etac,m_Jpsi), RooArgList(sigma_Jpsi))
    sigma_etac2s  = RooFormulaVar('sigma_etac2s','width of gaussian','@0*({}/{})**0.5'.format(m_etac2s,m_Jpsi), RooArgList(sigma_Jpsi))
    sigma_chic0   = RooFormulaVar('sigma_chic0','width of gaussian','@0*({}/{})**0.5'.format(m_chic0,m_Jpsi), RooArgList(sigma_Jpsi))
    sigma_chic1   = RooFormulaVar('sigma_chic1','width of gaussian','@0*({}/{})**0.5'.format(m_chic1,m_Jpsi), RooArgList(sigma_Jpsi))
    sigma_chic2   = RooFormulaVar('sigma_chic2','width of gaussian','@0*({}/{})**0.5'.format(m_chic2,m_Jpsi), RooArgList(sigma_Jpsi))
    sigma_hc      = RooFormulaVar('sigma_hc','width of gaussian','@0*({}/{})**0.5'.format(m_hc,m_Jpsi), RooArgList(sigma_Jpsi))
    sigma_Psi2s   = RooFormulaVar('sigma_Psi2s','width of gaussian','@0*({}/{})**0.5'.format(m_Psi2s,m_Jpsi), RooArgList(sigma_Jpsi))
    sigma_Psi3770 = RooFormulaVar('sigma_Psi3770','width of gaussian','@0*({}/{})**0.5'.format(m_Psi3770,m_Jpsi), RooArgList(sigma_Jpsi))


    sigma_Jpsi_w    = RooFormulaVar('sigma_Jpsi_w','width of gaussian','@0/@1', RooArgList(sigma_Jpsi,rNarToW))
    sigma_etac_w    = RooFormulaVar('sigma_etac_w','width of gaussian','@0/@1', RooArgList(sigma_etac,rNarToW))
    sigma_etac2s_w  = RooFormulaVar('sigma_etac2s_w','width of gaussian','@0/@1', RooArgList(sigma_etac2s,rNarToW))
    sigma_chic0_w   = RooFormulaVar('sigma_chic0_w','width of gaussian','@0/@1', RooArgList(sigma_chic0,rNarToW))
    sigma_chic1_w   = RooFormulaVar('sigma_chic1_w','width of gaussian','@0/@1', RooArgList(sigma_chic1,rNarToW))
    sigma_chic2_w   = RooFormulaVar('sigma_chic2_w','width of gaussian','@0/@1', RooArgList(sigma_chic2,rNarToW))
    sigma_hc_w      = RooFormulaVar('sigma_hc_w','width of gaussian','@0/@1', RooArgList(sigma_hc,rNarToW))
    sigma_Psi2s_w   = RooFormulaVar('sigma_Psi2s_w','width of gaussian','@0/@1', RooArgList(sigma_Psi2s,rNarToW))
    sigma_Psi3770_w = RooFormulaVar('sigma_Psi3770_w','width of gaussian','@0/@1', RooArgList(sigma_Psi3770,rNarToW))

    #Fit signal

    gauss = RooGaussian("gauss","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_Jpsi)

    #eta_c
    gauss_etac_1 = RooGaussian("gauss_etac_1","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_etac)
    gauss_etac_2 = RooGaussian("gauss_etac_2","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_etac_w)
    br_wigner_etac = RooRelBreitWigner("br_wigner_etac", "br_wigner", Jpsi_M, mass_etac, gamma_etac, spin_etac, radius, proton_m, proton_m)
    bwxg_etac_1 = RooFFTConvPdf("bwxg_etac_1","breit-wigner (X) gauss", Jpsi_M, br_wigner_etac, gauss_etac_1)
    bwxg_etac_2 = RooFFTConvPdf("bwxg_etac_2","breit-wigner (X) gauss", Jpsi_M, br_wigner_etac, gauss_etac_2)

    gauss_etac2s_1 = RooGaussian("gauss_etac2s_1","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_etac2s)
    gauss_etac2s_2 = RooGaussian("gauss_etac2s_2","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_etac2s_w)
    br_wigner_etac2s = RooRelBreitWigner("br_wigner_etac2s", "br_wigner", Jpsi_M, mass_etac2s, gamma_etac2s, spin_etac2s, radius, proton_m, proton_m)
    bwxg_etac2s_1 = RooFFTConvPdf("bwxg_etac2s_1","breit-wigner (X) gauss", Jpsi_M, br_wigner_etac2s, gauss_etac2s_1)
    bwxg_etac2s_2 = RooFFTConvPdf("bwxg_etac2s_2","breit-wigner (X) gauss", Jpsi_M, br_wigner_etac2s, gauss_etac2s_2)
    #bwxg = RooVoigtian("bwxg", "bwxg", Jpsi_M, mass_etac2s, gamma_etac2s, sigma_etac2s)

    #chi_c
    gauss_chic0 = RooGaussian("gauss_chic0","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_chic0)
    br_wigner_chic0 = RooRelBreitWigner("br_wigner_chic0", "br_wigner", Jpsi_M, mass_chic0, gamma_chic0, spin_chic0, radius, proton_m, proton_m)
    bwxg_chic0 = RooFFTConvPdf("bwxg_chic0","breit-wigner (X) gauss", Jpsi_M, br_wigner_chic0, gauss_chic0)

    gauss_chic1 = RooGaussian("gauss_chic1","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_chic1)
    br_wigner_chic1 = RooRelBreitWigner("br_wigner_chic1", "br_wigner", Jpsi_M, mass_chic1, gamma_chic1, spin_chic1, radius, proton_m, proton_m)
    bwxg_chic1 = RooFFTConvPdf("bwxg_chic1","breit-wigner (X) gauss", Jpsi_M, br_wigner_chic1, gauss_chic1)

    gauss_chic2 = RooGaussian("gauss_chic2","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_chic2)
    br_wigner_chic2 = RooRelBreitWigner("br_wigner_chic2", "br_wigner", Jpsi_M, mass_chic2, gamma_chic2, spin_chic2, radius, proton_m, proton_m)
    bwxg_chic2 = RooFFTConvPdf("bwxg_chic2","breit-wigner (X) gauss", Jpsi_M, br_wigner_chic2, gauss_chic2)

    #h_c
    gauss_hc = RooGaussian("gauss_hc","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_hc)
    br_wigner_hc = RooRelBreitWigner("br_wigner_hc", "br_wigner", Jpsi_M, mass_hc, gamma_hc, spin_hc, radius, proton_m, proton_m)
    bwxg_hc = RooFFTConvPdf("bwxg_hc","breit-wigner (X) gauss", Jpsi_M, br_wigner_hc, gauss_hc)


    #Fit J/psi
    gauss_Jpsi_1 = RooGaussian("gauss_Jpsi_1","gaussian PDF", Jpsi_M, mass_Jpsi, sigma_Jpsi)
    gauss_Jpsi_2 = RooGaussian("gauss_Jpsi_2","gaussian PDF", Jpsi_M, mass_Jpsi, sigma_Jpsi_w)

    gauss_Psi2s = RooGaussian("gauss_Psi2s","gaussian PDF", Jpsi_M, mass_Psi2s, sigma_Psi2s)
    #gauss_Psi2s = RooGaussian("gauss_Psi2s","gaussian PDF", Jpsi_M, mass_Psi2s, sigma_Psi2s)

    #br_wigner_Psi3770 = RooRelBreitWigner("br_wigner_Psi3770", "br_wigner", Jpsi_M, mass_Psi3770, gamma_Psi3770, spin_Psi3770, radius, proton_m, proton_m)
    #bwxg_Psi3770 = RooFFTConvPdf("bwxg_Psi3770","breit-wigner (X) gauss", Jpsi_M, br_wigner_Psi3770, gauss)
    bwxg_Psi3770 = RooVoigtian("bwxg_Psi3770", "bwxg_Psi3770", Jpsi_M, mass_Psi3770, gamma_Psi3770, sigma_Psi3770)



    eff_pppi0 = RooRealVar("eff_pppi0","eff_pppi0",0.06)
    pppi0 = RooGenericPdf("pppi0","Jpsi.pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))
    n_pppi0 = RooFormulaVar("n_pppi0","n_pppi0","@0*@1*(1.19/2.12)",RooArgList(n_Jpsi,eff_pppi0))

    #Bkg fit
    a0_High = RooRealVar("a0_High","a0",0.4,-2,2)
    a1_High = RooRealVar("a1_High","a1",0.05,-1.,1.)
    a2_High = RooRealVar("a2_High","a2",-0.005,-1,1.)

    a1_High.setVal(0.04)
    a2_High.setVal(0.04)

    parBkg_High = RooArgList(Jpsi_M, a0_High, a1_High, a2_High)
    bkg_High    = choose_bkg([kSource,kBkg], 'High', parBkg_High)


    #Bkg fit
    a0_Low = RooRealVar("a0_Low","a0",0.4,-2.,2.)
    a1_Low = RooRealVar("a1_Low","a1",0.05,-1.,1.)
    a2_Low = RooRealVar("a2_Low","a2",-0.005,-1,1.)
    #a3_Low = RooRealVar("a3_Low","a2",-0.005,-1,1.)

    a1_Low.setVal(0.04)
    a2_Low.setVal(0.04)

    parBkg_Low = RooArgList(Jpsi_M, a0_Low, a1_Low, a2_Low)
    bkg_Low    = choose_bkg([kSource,kBkg], 'Low', parBkg_Low)

    #bkg = RooGenericPdf("bkg","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200)", RooArgList(Jpsi_M,a0,a1))
    #bkg = RooChebychev ("bkg_Low","Background",Jpsi_M,RooArgList(a0_Low,a1_Low,a2_Low,a3_Low))



    modelSignal_High = RooAddPdf('modelSignal_High','etac2s signal', RooArgList(bwxg_etac2s_1, bwxg_etac2s_2, bwxg_chic0, bwxg_chic1, bwxg_chic2, bwxg_hc, gauss_Psi2s, bwxg_Psi3770), RooArgList(n_etac2s_1, n_etac2s_2, n_chic0, n_chic1, n_chic2, n_hc, n_Psi2s, n_Psi3770))
    modelSignal_etac2s = RooAddPdf('modelSignal_etac2s','etac2s signal', RooArgList(bwxg_etac2s_1, bwxg_etac2s_2), RooArgList(n_etac2s_1, n_etac2s_2))
    modelBkg_High = RooAddPdf('modelBkg_High','etac2s bkg', RooArgList(bkg_High), RooArgList(nBckgr_High))
    model_High = RooAddPdf('model_High','High diapason signal', RooArgList(bkg_High, bwxg_etac2s_1, bwxg_etac2s_2, bwxg_chic0, bwxg_chic1, bwxg_chic2, bwxg_hc, gauss_Psi2s, bwxg_Psi3770), RooArgList(nBckgr_High, n_etac2s_1, n_etac2s_2, n_chic0, n_chic1, n_chic2, n_hc, n_Psi2s, n_Psi3770))

    modelSignal_Low = RooAddPdf('modelSignal_Low','Jpsi signal', RooArgList(bwxg_etac_1, bwxg_etac_2, gauss_Jpsi_1, gauss_Jpsi_2), RooArgList(n_etac_1, n_etac_2, n_Jpsi_1, n_Jpsi_2))
    modelSignal_Jpsi = RooAddPdf('modelSignal_Jpsi','Jpsi signal', RooArgList(gauss_Jpsi_1, gauss_Jpsi_2), RooArgList(n_Jpsi_1, n_Jpsi_2))
    modelBkg_Low = RooAddPdf('modelBkg_Low','Jpsi bkg', RooArgList(bkg_Low), RooArgList(nBckgr_Low))
    model_Low = RooAddPdf('model_Low','Low diapason signal', RooArgList(bwxg_etac_1, bwxg_etac_2, gauss_Jpsi_1, gauss_Jpsi_2,bkg_Low, pppi0), RooArgList(n_etac_1, n_etac_2, n_Jpsi_1, n_Jpsi_2,nBckgr_Low, n_pppi0))


    #n_etac_obs = RooRealVar('n_etac_obs','num of etac', 9.1639e+04, 8.4869e+4, 9.8409e+4)
    #fconstr_etac = RooGaussian("fconstr_etac","fconstr_etac", n_etac, n_etac_obs, RooFit.RooConst(6.77e+03))
    #model_Low_c = RooProdPdf("model_Low_c", "model with constraint", RooArgList(model_Low, fconstr_etac))


    sample = RooCategory('sample','sample')
    sample.defineType('High')
    sample.defineType('Low')

    Jpsi_M.setRange("fitRange_Low", minM_Low, maxM_Low)
    Jpsi_M.setRange("fitRange_High", minM_High, maxM_High)

    data_etac2s = w.data('dh_High')
    data_Jpsi = w.data('dh_Low')


    # Construct combined dataset in (Jpsi_M,sample)
    combData = RooDataHist('combData', 'combined data', RooArgList(Jpsi_M), RooFit.Index(sample), RooFit.Import('High',data_etac2s), RooFit.Import('Low',data_Jpsi))


    # Associate model with the physics state and model_ctl with the control state
    simPdf = RooSimultaneous('simPdf','simultaneous signal pdf',sample)
    simPdf.addPdf(model_High,'High')
    simPdf.addPdf(model_Low,'Low')


    #simPdf_c = RooSimultaneous('simPdf_c','simultaneous signal pdf',sample)
    #simPdf_c.addPdf(model_High,'High')
    #simPdf_c.addPdf(model_Low_c,'Low')


    #   getattr(w,'import')(model,RecycleConflictNodes())
    getattr(w,'import')(combData)
    getattr(w,'import')(simPdf)

    getattr(w,'import')(model_High,RooFit.RecycleConflictNodes())
    getattr(w,'import')(model_Low,RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelSignal_High,RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelSignal_Low,RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelBkg_High,RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelBkg_Low,RooFit.RecycleConflictNodes())

    getattr(w,'import')(nSignal_Low,RooFit.RecycleConflictNodes())
    getattr(w,'import')(nSignal_High,RooFit.RecycleConflictNodes())

    #getattr(w,'import')(simPdf_c, RooFit.RecycleConflictNodes())
    #getattr(w,'import')(fconstr_etac, RooFit.RecycleConflictNodes())
    #getattr(w,'import')(n_etac_obs, RooFit.RecycleConflictNodes())




def perform_fit_simult(kSource,kBkg):

    #gStyle.SetOptTitle(0)
    w = RooWorkspace("w",True)

    kNorm = 'Jpsi'
    #kNorm = 'etac'

    get_data_h(w,kSource)
    fillRelWorkspace(w, kSource, kBkg, kNorm)

    Jpsi_M = w.var("Jpsi_M")

    model_High = w.pdf('model_High')
    model_Low = w.pdf('model_Low')
    modelSignal_High = w.pdf('modelSignal_High')
    modelSignal_Low = w.pdf('modelSignal_Low')
    modelBkg_High = w.pdf('modelBkg_High')
    modelBkg_Low = w.pdf('modelBkg_Low')


    sample = w.cat('sample')
    simPdf = w.pdf('simPdf')
    combData = w.data('combData')

    data_etac2s = w.data('dh_High')
    data_Jpsi = w.data('dh_Low')

    if kSource=='prompt':
        f = TFile(homeDir+"/mass_fit/FitMass_wksp_{}_fromB_{}_DG.root".format(kNorm, kBkg),"READ")
        w_fromB = f.Get("w")
        f.Close()
        sigma = w_fromB.var('sigma_Jpsi').getValV()
        w.var('sigma_Jpsi').setVal(sigma)
        w.var('sigma_Jpsi').setConstant(True)
        mass = w_fromB.var('mass_Jpsi').getValV()
        w.var('mass_Jpsi').setVal(mass)
        w.var('mass_Jpsi').setConstant(True)
    else:
        w.var('mass_Jpsi').setConstant(False)

    modelBkg_Low.fitTo(combData, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.NumCPU(48))
    modelBkg_High.fitTo(combData, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.NumCPU(48))

    #sigma_0 = 10.9646
    #w.var('sigma_Jpsi').setVal(sigma_0)
    #w.var('sigma_Jpsi').setConstant(True)
    simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.NumCPU(48))
    #w.var('mass_Jpsi').setConstant(False)
    simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.NumCPU(48))
    simPdf.fitTo(combData, RooFit.Strategy(2), RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.NumCPU(48))
    w.var('n_Jpsi').setVal(w.var('n_Jpsi').getValV())
    r = simPdf.fitTo(combData, RooFit.Strategy(2), RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.NumCPU(48))


    ##draw low and high separately

    frame1 = Jpsi_M.frame(RooFit.Title('J/#psi to p #bar{p}'), RooFit.Range('fitRange_Low'))
    frame2 = Jpsi_M.frame(RooFit.Title('#eta_c(2S) to p #bar{p}'), RooFit.Range('fitRange_High'))

    combData.plotOn(frame1, cut("sample==sample::Low"), binning_Jpsi, mrkSize, lineWidth1, name('data_Low'))
    combData.plotOn(frame2, cut("sample==sample::High"), binning_etac2s, mrkSize, lineWidth1, name('data_High'))

    simPdf.plotOn(frame1, RooFit.Slice(sample,'Low'), RooFit.ProjWData(RooArgSet(sample),combData,True), lineWidth1, RooFit.Range(minM_Low,maxM_Low))
    simPdf.plotOn(frame1, RooFit.Slice(sample,'Low'), RooFit.Components('bkg_Low'), RooFit.ProjWData(RooArgSet(sample),combData,True), lineWidth1, lineColor1, lineStyle1, RooFit.Range(minM_Low,maxM_Low), name('bkg_Low'))

    simPdf.plotOn(frame2, RooFit.Slice(sample,"High"), RooFit.ProjWData(RooArgSet(sample),combData,True), lineWidth1, RooFit.Range(minM_High,maxM_High))
    simPdf.plotOn(frame2, RooFit.Slice(sample,"High"), RooFit.ProjWData(RooArgSet(sample),combData,True), RooFit.Components('bkg_High'), lineWidth1, lineColor1, lineStyle1, RooFit.Range(minM_High,maxM_High), name('bkg_High'))


    #print('check')


    h_resid_Low = frame1.residHist("data_Low","bkg_Low")
    frame_res1 = Jpsi_M.frame(RooFit.Bins(100), RooFit.Range('fitRange_Low'))
    frame_res1.addPlotable(h_resid_Low,"P")
    modelSignal_Low.plotOn(frame_res1, lineWidth1, RooFit.Range(minM_Low,maxM_Low), name('sig_Low'))#, RooFit.Normalization(8.83177e+07,RooAbsReal.NumEvent))

    h_resid_High = frame2.residHist("data_High","bkg_High")
    frame_res2 = Jpsi_M.frame(RooFit.Bins(100), RooFit.Range('fitRange_High'))
    frame_res2.addPlotable(h_resid_High,"P")
    modelSignal_High.plotOn(frame_res2, lineWidth1, RooFit.Range(minM_High,maxM_High), name('sig_High'))#, RooFit.Normalization(8.83177e+07,RooAbsReal.NumEvent))

    c = TCanvas('c','c', 3600, 2000)
    c.Divide(2,2)

    pad = c.cd(1)
    xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
    yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
    pad.SetPad(xl,yl-0.2,xh,yh)
    pad.SetBottomMargin(0.15)
    pad.SetLeftMargin(0.15)
    frame1.GetYaxis().SetTitleOffset(1.5)
    frame1.GetXaxis().SetTitleOffset(0.75)
    frame1.GetXaxis().SetTitle("M(pp) / [MeV/c^{2}]")
    frame1.GetXaxis().SetTitleSize(0.045)
    frame1.GetXaxis().SetTitleFont(12)
    frame1.Draw()

    pad = c.cd(2)
    xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
    yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
    pad.SetPad(xl,yl-0.2,xh,yh)
    pad.SetBottomMargin(0.15)
    pad.SetLeftMargin(0.15)
    frame2.GetYaxis().SetTitleOffset(1.5)
    frame2.GetXaxis().SetTitleOffset(0.75)
    frame2.GetXaxis().SetTitle("M(pp) / [MeV/c^{2}]")
    frame2.GetXaxis().SetTitleSize(0.045)
    frame2.GetXaxis().SetTitleFont(12)
    frame2.Draw()


    pad = c.cd(3)
    xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
    yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
    pad.SetPad(xl,yl,xh,yh-0.2)
    pad.SetBottomMargin(0.15)
    pad.SetLeftMargin(0.15)
    frame_res1.GetXaxis().SetTitle("M(pp) / [MeV/c^{2}]")
    frame_res1.GetXaxis().SetTitleFont(12)
    frame_res1.Draw()


    pad = c.cd(4)
    xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
    yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
    pad.SetPad(xl,yl,xh,yh-0.2)
    pad.SetBottomMargin(0.15)
    pad.SetLeftMargin(0.15)
    frame_res2.GetXaxis().SetTitle("M(pp) / [MeV/c^{2}]")
    frame_res2.GetXaxis().SetTitleFont(12)
    frame_res2.Draw()

    #draw low and high at the same plot

    #frame = Jpsi_M.frame(RooFit.Title('c #bar{c} to p #bar{p}'))

    #kNorm = w.pdf('model_Low').getValV()
    #combData.plotOn(frame, cut("sample==sample::Low"), binning, mrkSize, lineWidth1, name('data_Low'))
    #simPdf.plotOn(frame, RooFit.Slice(sample,'Low'), RooFit.ProjWData(RooArgSet(sample),combData,True), lineWidth1, RooFit.Range(minM_Low,maxM_Low))#, RooFit.Normalization(kNorm, RooAbsReal.RelativeExpected))
    #simPdf.plotOn(frame, RooFit.Slice(sample,'Low'), RooFit.ProjWData(RooArgSet(sample),combData,True), RooFit.Components('bkg_Low'), lineWidth1, lineColor1, lineStyle1, RooFit.Range(minM_Low,maxM_Low), name('bkg_Low'))

    #h_resid_Low = frame.residHist("data_Low","bkg_Low")

    #combData.plotOn(frame, cut("sample==sample::High"), binning, mrkSize, lineWidth1, name('data_High'))
    #simPdf.plotOn(frame, RooFit.Slice(sample,"High"), RooFit.ProjWData(RooArgSet(sample),combData,True), lineWidth1, RooFit.Range(minM_High,maxM_High))#, RooFit.Normalization(1, RooAbsReal.RelativeExpected))
    #simPdf.plotOn(frame, RooFit.Slice(sample,"High"), RooFit.ProjWData(RooArgSet(sample),combData,True), RooFit.Components('bkg_High'), lineWidth1, lineColor1, lineStyle1, RooFit.Range(minM_High,maxM_High), name('bkg_High'))

    #h_resid_High = frame.residHist("data_High","bkg_High")

    #frame_res = Jpsi_M.frame(RooFit.Bins(100))
    #frame_res.addPlotable(h_resid_Low,"P")
    #frame_res.addPlotable(h_resid_High,"P")

    #kNorm1 = h_resid_Low.getFitRangeNEvt(minM_Low,maxM_Low)
    #kNorm2 = h_resid_High.getFitRangeNEvt(minM_High,maxM_High)
    ##kNorm1 = w.function("nSignal_Low").getValV()
    ##kNorm2 = w.function("nSignal_High").getValV()
    #print (kNorm1, kNorm2)
    #modelSignal_Low.plotOn(frame_res, lineWidth1, lineColor2, RooFit.Range(minM_Low,maxM_Low), name('sig_Low'), RooFit.Normalization(kNorm1, RooAbsReal.NumEvent))
    #modelSignal_High.plotOn(frame_res, lineWidth1, lineColor2, RooFit.Range(minM_High,maxM_High), name('sig_High'), RooFit.Normalization(kNorm2, RooAbsReal.NumEvent))

    #c = TCanvas('c','c', 3600, 2000)
    #c.Divide(1,2)

    #pad = c.cd(1)
    #xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
    #yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
    #pad.SetPad(xl,yl-0.2,xh,yh)
    #pad.SetBottomMargin(0.15)
    #pad.SetLeftMargin(0.15)
    #frame.Draw()


    #pad = c.cd(2)
    #xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
    #yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
    #pad.SetPad(xl,yl,xh,yh-0.2)
    #pad.SetBottomMargin(0.15)
    #pad.SetLeftMargin(0.15)
    #frame_res.Draw()


    nameWksp = homeDir + '/mass_fit/FitMass_wksp_{}_{}_{}_DG.root'.format(kNorm,kSource,kBkg)
    namePic = homeDir + '/mass_fit/FitMass_{}_{}_{}_DG.pdf'.format(kNorm,kSource,kBkg)
    nameTxt = homeDir + '/mass_fit/FitMass_result_{}_{}_{}_DG.txt'.format(kNorm,kSource,kBkg)


    c.SaveAs(namePic)
    w.writeToFile(nameWksp)

    import os, sys
    save = os.dup( sys.stdout.fileno() )
    newout = file(nameTxt, 'a' )
    os.dup2( newout.fileno(), sys.stdout.fileno() )
    r.Print("v")
    r.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() )
    newout.close()


    print('fit performed successfully')

    return w


def calcUL(w, kSource, kBkg):

    Jpsi_M = w.var("Jpsi_M")

    sample = w.cat('sample')
    simPdf = w.pdf('simPdf')
    combData = w.data('combData')

    #simPdf_c = w.pdf('simPdf_c')

    n_etac2sRel = w.var('n_etac2sRel')

    w.var("sigma_Jpsi").setConstant()
    w.var('mass_Jpsi').setConstant(True)
    w.var("a0_Low").setConstant()
    w.var("a1_Low").setConstant()
    w.var("a0_High").setConstant()
    w.var("a1_High").setConstant()
    if kSource=='prompt':
        w.var("a2_Low").setConstant()
        w.var("a2_High").setConstant()
    w.var('n_chic0').setConstant(True)
    w.var('n_chic1').setConstant(True)
    w.var('n_chic2').setConstant(True)
    w.var('n_hc').setConstant(True)
    w.var('n_Psi2s').setConstant(True)

    ## ULs Calculator using HypoTestInverter

    nBckgr_High = w.var('nBckgr_High')
    nBckgr_Low  = w.var('nBckgr_Low')
    n_Jpsi      = w.var('n_Jpsi')
    n_Psi2s     = w.var('n_Psi2s')
    n_etac      = w.var('n_etac')
    n_hc        = w.var('n_hc')
    n_chic0     = w.var('n_chic0')
    n_chic1     = w.var('n_chic1')
    n_chic2     = w.var('n_chic2')


    #n_etac_obs  = w.var('n_etac_obs')
    #n_etac_obs.setConstant(True)



    nBckgr_High.setConstant(True)
    nBckgr_Low.setConstant(True)
    n_Jpsi.setConstant(True)
    n_etac.setConstant(True)




    Math.MinimizerOptions.SetDefaultMinimizer('Minuit','Minimize');
    Math.MinimizerOptions.SetDefaultStrategy(2);
    Math.MinimizerOptions.SetDefaultTolerance(10)
    Math.MinimizerOptions.SetDefaultMaxIterations(25000)


    paramOfInterest = RooArgSet(n_etac2sRel)
    #constrainedParams = RooArgSet(nBckgr_High, nBckgr_Low, n_Jpsi, n_etac, n_hc, n_Psi2s, n_chic0, n_chic1, n_chic2)
    constrainedParams = RooArgSet(nBckgr_High,nBckgr_Low, n_etac, n_Jpsi, n_Psi2s)

    nuis_iter = constrainedParams.createIterator()
    var = nuis_iter.Next()
    band_size = 10
    while var:
        if var.getVal()-band_size*var.getError() > 0:
            var.setRange(var.getVal()-band_size*var.getError(), var.getVal()+band_size*var.getError())
        else:
            var.setRange(0, var.getVal()+band_size*var.getError())
        print(var.GetName(), var.getVal()-band_size*var.getError(), var.getVal()+band_size*var.getError())
        var = nuis_iter.Next()

    n_etac2sRel.setRange(0., 0.2)

    #n_etac.setRange(n_etac.getVal()-5*n_etac.getError(), n_etac.getVal()+5*n_etac.getError())
    nBckgr_High.setRange(nBckgr_High.getVal()-5*nBckgr_High.getError(), nBckgr_High.getVal()+5*nBckgr_High.getError())
    nBckgr_Low.setRange(nBckgr_Low.getVal()-5*nBckgr_Low.getError(), nBckgr_Low.getVal()+5*nBckgr_Low.getError())


    modelConfig = RooStats.ModelConfig(w);
    modelConfig.SetPdf(simPdf)
    modelConfig.SetProtoData(combData)
    modelConfig.SetParametersOfInterest(paramOfInterest)
    modelConfig.SetNuisanceParameters(constrainedParams)
    modelConfig.SetObservables(RooArgSet(RooArgList(Jpsi_M, sample)))
    #modelConfig.SetConstraintParameters(RooArgSet(n_etac))
    #modelConfig.SetGlobalObservables(RooArgSet(n_etac_obs))
    modelConfig.SetName("ModelConfig")
    paramOfInterest.first().setVal(0.03)
    modelConfig.SetSnapshot(paramOfInterest)
    getattr(w,'import')(modelConfig)


    modelConfigHypo = modelConfig.Clone()
    modelConfigHypo.SetName(modelConfig.GetName()+"_with_poi_0")
    paramOfInterest.first().setVal(0)
    modelConfigHypo.SetSnapshot(paramOfInterest)

    RooStats.UseNLLOffset(True)


    fc = RooStats.FrequentistCalculator(combData, modelConfigHypo, modelConfig)
    fc.SetToys(1000,1000)
    ac = RooStats.AsymptoticCalculator(combData, modelConfigHypo, modelConfig)
    ac.SetOneSidedDiscovery(True)
    ac.SetPrintLevel(-1)
    hc = RooStats.HybridCalculator(combData, modelConfigHypo, modelConfig)
    hc.SetToys(1000,1000)
    hc.ForcePriorNuisanceAlt(modelConfig.GetPriorPdf());
    hc.ForcePriorNuisanceNull(modelConfigHypo.GetPriorPdf())

    calc = RooStats.HypoTestInverter(ac)
    calc.SetConfidenceLevel(0.95)
    calc.UseCLs(True)
    #calc.SetVerbose(False)
    toymcs = calc.GetHypoTestCalculator().GetTestStatSampler()
    #toymcs = ac.GetTestStatSampler()



    profll = RooStats.ProfileLikelihoodTestStat(modelConfig.GetPdf())
    #profll = RooStats.SimpleLikelihoodRatioTestStat(modelConfig.GetPdf(), modelConfigHypo.GetPdf());
    profll.SetStrategy(2)
    profll.SetMinimizer('Minuit')
    profll.SetLOffset(True)
    profll.SetOneSided(True)
    toymcs.SetTestStatistic(profll)
    toymcs.SetNToys(10000)
    toymcs.SetNEventsPerToy(1)

    npoints = 25
    poimin = n_etac2sRel.getMin()
    poimax = n_etac2sRel.getMax()

    calc.SetFixedScan(npoints,poimin,poimax)
    res = calc.GetInterval()
    upperLimit = res.UpperLimit()
    print("Upper Limit is", res.GetExpectedUpperLimit())
    print("Upper Limit is", upperLimit)

    plot = RooStats.HypoTestInverterPlot("HTI_Result_Plot","HypoTest Scan Result",res)
    c = TCanvas("HypoTestInverter Scan","HypoTestInverter Scan", 3600, 2000)
    c.cd()
    c.SetLogy(False)
    plot.Draw("CLb 2CL")

    #plot.Draw("EXP")
    c.RangeAxis(poimin, 0., poimax, 1.1)
    c.Draw()
    c.SaveAs(homeDir + "/mass_fit/CL_As_{}_{}_DG.root".format(kSource,kBkg))
    c.SaveAs(homeDir + "/mass_fit/CL_As_{}_{}_DG.pdf".format(kSource,kBkg))


    ### C o n s t r u c t   p l a i n   l i k e l i h o o d
    ### ---------------------------------------------------

    #f_nll = TFile(homeDir + "/mass_fit/NLL_var_{}_{}_DG.root".format(kSource,kBkg),'RECREATE')

    # Construct unbinned likelihood
    #nll = simPdf_c.createNLL(combData, RooFit.Constrain(RooArgSet(n_etac)), RooFit.NumCPU(8), RooFit.Extended(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.Offset(True))
    nll = simPdf.createNLL(combData, RooFit.NumCPU(8), RooFit.Extended(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.Offset(True))
    #nll  = RooNLLVar("nll","nll",simPdf, combData, RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.Offset(True), RooFit.Extended(True), RooFit.NumCPU(12))


    # Minimize likelihood w.r.t all parameters before making plots
    m = RooMinuit(nll)
    #m = RooMinimizer(nll)
    m.migrad()
    m.hesse()
    #m.minos()

    # Plot likelihood scan frac
    frameRS = n_etac2sRel.frame(RooFit.Bins(46),RooFit.Title("LL and profileLL in n_{#eta_{c}}(2S) "))
#     nll.plotOn(frameRS,RooFit.ShiftToZero())

    # The profile likelihood estimator on nll for frac will minimize nll w.r.t
    # all floating parameters except frac for each evaluation

    #pll_N_etac2s = nll.createProfile(RooArgSet(n_etac2sRel))
    pll_N_etac2s = RooProfileLL("pll_N_etac2s", "pll_N_etac2s", nll, RooArgSet(n_etac2sRel))
    llhood = RooFormulaVar("llhood","exp(-0.5*pll_N_etac2s)",RooArgList(pll_N_etac2s));

    # Plot the profile likelihood in frac
    pll_N_etac2s.plotOn(frameRS,RooFit.LineColor(2))

    # Adjust frame maximum for visual clarity
    frameRS.SetMinimum(0)
    frameRS.GetXaxis().SetRangeUser(poimin, poimax)
    #frameRS.GetYaxis().SetRangeUser(-1e-2, 1e-2)

    cNLL = TCanvas("rf605_profilell","rf605_profilell",800, 400)
    cNLL.cd(),  gPad.SetLeftMargin(0.15),  frameRS.GetYaxis().SetTitleOffset(1.4),  frameRS.Draw()

    cNLL.SaveAs(homeDir + "/mass_fit/NLL_{}_{}_DG.pdf".format(kSource,kBkg))

    nll.Write()
    pll_N_etac2s.Write()

    #f_nll.Write()
    #f_nll.Close()




    fllhood = llhood.asTF(RooArgList(paramOfInterest))
    npoints = 50
    int_array = np.ones(npoints+1)
    int_val0 = fllhood.Integral(poimin, poimax, 1e-6)
    print(int_val0)
    poi_urs = np.linspace(poimin, poimax, npoints+1)

    for idx in range(npoints):

        int_val = fllhood.Integral(poimin, poi_urs[idx+1], 1e-6)
        int_array[idx+1] = 1.-int_val/int_val0
        if int_array[idx+1]<0.05: print(poi_urs[idx+1])

    print(int_array)
    gr_pval = TGraph(npoints+1, poi_urs, int_array)
    c.cd()
    gr_pval.SetLineColor(2)
    gr_pval.GetXaxis().SetRangeUser(poimin, poimax)
    gr_pval.Draw('same')
    c.SaveAs(homeDir + "/mass_fit/CL_As_check_{}_{}_DG.pdf".format(kSource,kBkg))
    c.SaveAs(homeDir + "/mass_fit/CL_As_check_{}_{}_DG.root".format(kSource,kBkg))


    #n_etac2sRel.setRange(0., 0.2)

    ## First, let's use a Calculator based on the Profile Likelihood Ratio
    ##ProfileLikelihoodCalculator plc(*data, *modelWithConstraints, paramOfInterest)
    #plc = RooStats.ProfileLikelihoodCalculator(combData, modelConfig)
    ###plc = RooStats.ProfileLikelihoodCalculator(combData, simPdf, paramOfInterest)
    #plc.SetTestSize(.05)
    #lrint = plc.GetInterval()  #ConfInterval that was easy
    #print(lrint.UpperLimit(n_etac2sRel))

    #r = simPdf.fitTo(combData, RooFit.Strategy(2), RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.NumCPU(48))
    ##r = simPdf.fitTo(combData, RooFit.Strategy(2), RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.NumCPU(48))


    #ph = RooStats.ProposalHelper()
    #ph.SetVariables(RooArgSet(r.floatParsFinal()))
    ##ph.SetVariables(RooArgSet(n_etac2sRel))
    #ph.SetCovMatrix(r.covarianceMatrix())
    #ph.SetUpdateProposalParameters(True)
    #ph.SetCacheSize(5000)
    #pdfProp = ph.GetProposalFunction()
    ##pdfProp = RooStats.SequentialProposal(0.1)


    #mc = RooStats.MCMCCalculator(combData, modelConfig)
    ##mc = RooStats.MCMCCalculator(combData, simPdf, paramOfInterest)
    #mc.SetNumIters(50000) # steps to propose in the chain
    #mc.SetTestSize(.05) # 95% CL
    ##mc.SetConfidenceLevel(.90)
    #mc.SetNumBins(50)
    #mc.SetNumBurnInSteps(50) # ignore first N steps in chain as "burn in"
    #mc.SetProposalFunction(pdfProp)
    #mc.SetLeftSideTailFraction(0.0)  # find a "central" interval
    #mcInt = mc.GetInterval()  # that was eas (MCMCInterval*)

    #mcul = mcInt.UpperLimit(n_etac2sRel)
    #mcll = mcInt.LowerLimit(n_etac2sRel)
    #print("MCMC lower limit on n_etac2sRel = ", mcll )
    #print("MCMC upper limit on n_etac2sRel = ", mcul )
    #print("MCMC Actual confidence level = ", mcInt.GetActualConfidenceLevel() )


    ## Plot MCMC interval and print some statistics
    #mcPlot = RooStats.MCMCIntervalPlot(mcInt);
    #mcPlot.SetLineColor(kMagenta);
    #mcPlot.SetLineWidth(2);
    ##mcPlot.Draw("same");

    #cNLL = TCanvas("rf605_profilell","rf605_profilell",800, 400)
    #cNLL.cd(),  gPad.SetLeftMargin(0.15),      mcPlot.Draw();
    #cNLL.SaveAs(homeDir + "/mass_fit/N_etac2s_MCMC_{}.pdf".format(kSource))


    #bayesianCalc = RooStats.BayesianCalculator(combData, modelConfig)
    #bayesianCalc.SetConfidenceLevel(0.95)
    #bayesianCalc.SetLeftSideTailFraction(0.0)
    #bayesianCalc.SetNumIters(1000)
    ##bayesianCalc.SetIntegrationType("PLAIN")
    #bayesianCalc.SetScanOfPosterior(50)
    #bInt = bayesianCalc.GetInterval()

    #bul = bInt.UpperLimit()
    #bll = bInt.LowerLimit()
    #print("Bayesian lower limit on n_etac2sRel = ", bll )
    #print("Bayesian upper limit on n_etac2sRel = ", bul )
    ##print("Bayesian Actual confidence level = ", bInt.GetActualConfidenceLevel() )

    ## Plot MCMC interval and print some statistics
    ##bPlot = RooStats.MCMCIntervalPlot(bInt);
    #bPlot = bayesianCalc.GetPosteriorPlot();
    #bPlot.SetLineColor(kMagenta);
    #bPlot.SetLineWidth(2);
    ##mcPlot.Draw("same");

    #cNLL = TCanvas("rf605_profilell","rf605_profilell",800, 400)
    #cNLL.cd(),  gPad.SetLeftMargin(0.15),      bPlot.Draw();
    #cNLL.SaveAs(homeDir + "/mass_fit/N_etac2s_Bayesian_released_{}.pdf".format(kSource))






#kBkgs = ['exp','cheb']
kBkgs = ['cheb']
#kSources = ['fromB','prompt']
kSources = ['prompt']

for kBkg in kBkgs:
    for kSource in kSources:
        w = perform_fit_simult(kSource, kBkg)
        #nameWksp = homeDir + '/mass_fit/FitMass_wksp_{}_{}_{}_DG.root'.format('Jpsi',kSource, kBkg)
        #file_w = TFile(nameWksp)
        #w = file_w.Get('w')
        #print(w.pdf('simPdf').indexCat())
        #calcUL(w,kSource,kBkg)
        del w

#kBkg = 'cheb'
#kSource = 'fromB'
#w = perform_fit_simult(kSource, kBkg)
#kSource = 'prompt'
#w = perform_fit_simult(kSource, kBkg)
#nameWksp = homeDir + '/mass_fit/FitMass_wksp_{}_{}_{}.root'.format('Jpsi',kSource, kBkg)
#file_w = TFile(nameWksp)
#w = file_w.Get('w')
#calcUL(w,kSource,kBkg)
#del w

