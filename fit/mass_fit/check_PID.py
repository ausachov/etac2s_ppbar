from ROOT import *
import numpy as np
gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+")

import numpy as np
#from math import abs

binWidth = 0.2
binWidthDraw = 0.2

minM_Low = 2850
maxM_Low = 3250
binN_Low = int((maxM_Low-minM_Low)/binWidth)

minM_High = 3320
maxM_High = 3780
binN_High = int((maxM_High-minM_High)/binWidth)



binning = RooFit.Binning(93, minM_Low, maxM_High)
binning_Jpsi = RooFit.Binning(40, minM_Low, maxM_Low)
binning_etac2s = RooFit.Binning(46, minM_High, maxM_High)
mrkSize = RooFit.MarkerStyle(7)
lineWidth1 = RooFit.LineWidth(1)
lineWidth2 = RooFit.LineWidth(2)
lineStyle1 = RooFit.LineStyle(2)
lineStyle2 = RooFit.LineStyle(9)
lineColor1 = RooFit.LineColor(6)
lineColor2 = RooFit.LineColor(8)
name = RooFit.Name
cut = RooFit.Cut

#homeDir = '/afs/cern.ch/user/v/vazhovko/private/etac2s_ppbar/'
#dataDir = '/eos/user/v/vazhovko/etac2s_ppbar/'
homeDir = '/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/'
dataDir = '/sps/lhcb/zhovkovska/etac2s_ppbar/'


kBkg = 'cheb'
kSources = ['fromB','prompt']

def check_PID():

    probNNp_val = np.append(np.arange(0.4, 1.0, 0.2), np.arange(0.85, 0.99, 0.05))
    probNNp_err = np.append(0.2*np.ones(3), 0.05*np.ones(3))
    n_points = probNNp_val.size

    n_var_prompt = 0
    n_var_fromB = 0

    sign_list_pr = {}
    sign_list_fb = {}
    sign_list_pr_0 = {}
    sign_list_fb_0 = {}

    nameWksp = homeDir + '/mass_fit/FitMass_wksp_Jpsi_fromB_{}.root'.format(kBkg)
    file_w = TFile(nameWksp)
    w = file_w.Get('w')
    var_list_0 = w.allVars()

    nuis_iter = var_list_0.createIterator()
    var = nuis_iter.Next()
    while var:
        if not var.isConstant() and var.GetName()!='Jpsi_M':
            sign_list_fb[var.GetName()] = np.zeros(n_points)
            sign_list_fb_0[var.GetName()] = np.array([abs(var.getValV()/var.getError())])
            n_var_fromB+=1

        var = nuis_iter.Next()


    nameWksp = homeDir + '/mass_fit/FitMass_wksp_Jpsi_prompt_{}.root'.format(kBkg)
    file_w = TFile(nameWksp)
    w = file_w.Get('w')
    var_list_0 = w.allVars()

    nuis_iter = var_list_0.createIterator()
    var = nuis_iter.Next()
    while var:
        if not var.isConstant() and var.GetName()!='Jpsi_M':
            sign_list_pr[var.GetName()] = np.zeros(n_points)
            sign_list_pr_0[var.GetName()] = np.array([abs(var.getValV()/var.getError())])
            n_var_prompt+=1
        var = nuis_iter.Next()


    #print(sign_list_fb, sign_list_pr)



    for i_cut in probNNp_val:

        idx_file = int(i_cut*10)
        idx_cut  = list(probNNp_val).index(i_cut)

        nameWksp = homeDir + '/mass_fit/PID_check/FitMass_wksp_Jpsi_fromB_{}_PID_0{}.root'.format(kBkg,idx_file)
        file_w = TFile(nameWksp)
        w = file_w.Get('w')

        for var_name in sign_list_fb:
            sign_list_fb[var_name][idx_cut] = abs(w.var(var_name).getValV()/w.var(var_name).getError())


        nameWksp = homeDir + '/mass_fit/PID_check/FitMass_wksp_Jpsi_prompt_{}_PID_0{}.root'.format(kBkg,idx_file)
        file_w = TFile(nameWksp)
        w = file_w.Get('w')

        for var_name in sign_list_pr:
            sign_list_pr[var_name][idx_cut] = abs(w.var(var_name).getValV()/w.var(var_name).getError())

    #print(sign_list_fb_0, sign_list_pr_0)
    #print(sign_list_fb, sign_list_pr)

    c_fb = TCanvas('c_fb','c_fb', 5600, 2000)
    c_fb.Divide(n_var_fromB/3 + 1, 3)

    gr_fb   = {}
    line_fb = {}
    n = 1

    for var_name in sign_list_fb:

        c_fb.cd(n)
        gr_fb[var_name] = TGraph(n_points,probNNp_val,sign_list_fb[var_name])
        gr_fb[var_name].Draw("AP")
        gr_fb[var_name].SetTitle(var_name)
        gr_fb[var_name].SetLineColor(2);
        gr_fb[var_name].SetLineWidth(1);
        gr_fb[var_name].SetMarkerColor(4);
        gr_fb[var_name].SetMarkerSize(1.5);
        gr_fb[var_name].SetMarkerStyle(21);
        gr_fb[var_name].GetXaxis().SetTitle("Prob NNp");
        gr_fb[var_name].GetYaxis().SetTitle("significance");
        gr_fb[var_name].GetYaxis().SetTitleOffset(1.5);
        gr_fb[var_name].GetYaxis().SetRangeUser(sign_list_fb_0[var_name]*0.1, sign_list_fb_0[var_name]*5);

        line_fb[var_name]  = TLine(0.4,sign_list_fb_0[var_name],1.0,sign_list_fb_0[var_name]);
        line_fb[var_name].SetLineColor(kYellow)
        line_fb[var_name].SetLineWidth(2)
        line_fb[var_name].Draw("same")
        n+=1

    c_fb.SaveAs("fromB_PID_check.pdf")

    c_pr = TCanvas('c_pr','c_pr', 3600, 2000)
    c_pr.Divide(n_var_prompt/3 + 1, 3)

    gr_pr   = {}
    line_pr = {}
    n = 1

    for var_name in sign_list_pr:

        c_pr.cd(n)
        gr_pr[var_name] = TGraph(n_points,probNNp_val,sign_list_pr[var_name])
        gr_pr[var_name].Draw("AP")
        gr_pr[var_name].SetTitle(var_name)
        gr_pr[var_name].SetLineColor(2);
        gr_pr[var_name].SetLineWidth(1);
        gr_pr[var_name].SetMarkerColor(4);
        gr_pr[var_name].SetMarkerSize(1.5);
        gr_pr[var_name].SetMarkerStyle(21);
        gr_pr[var_name].GetXaxis().SetTitle("Prob NNp");
        gr_pr[var_name].GetYaxis().SetTitle("significance");
        gr_pr[var_name].GetYaxis().SetTitleOffset(1.5);
        gr_pr[var_name].GetYaxis().SetRangeUser(sign_list_pr_0[var_name]*0.1, sign_list_pr_0[var_name]*1.5);

        line_pr[var_name]  = TLine(0.4,sign_list_pr_0[var_name],1.0,sign_list_pr_0[var_name]);
        line_pr[var_name].SetLineColor(kYellow)
        line_pr[var_name].SetLineWidth(2)
        line_pr[var_name].Draw("same")
        n+=1

    c_pr.SaveAs("prompt_PID_check.pdf")

check_PID()
