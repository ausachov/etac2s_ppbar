from multiprocessing import Pool
from functools import partial
from contextlib import closing
from ROOT import *
from fit_m_sim_merged import perform_fit


# gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
# gROOT.LoadMacro("mass_fit_2D.py");

def f(list):
    kSyst = list[0]
    kNorm = "jpsi"
    return perform_fit(kSyst, kNorm)

# kSysts = ["Base", "Exp","Gamma","eff","CB","BR","cross_talk","Resolution"]
kSysts = ["Exp","Sqr","Sqrt","CB"]
kSources = ["fromB","prompt"]
# a = kSysts
a = [[kSysts[iS]] for iS in range(len(kSysts))]
# a = [[kSysts[iS], years[iYear], bins[iBin]] for iKey in range(len(keys)) for iYear in range(len(years)) for iBin in range(len(bins))]
print(a);

if __name__ == '__main__':

    with closing(Pool(12)) as p:
        (p.map(f, a))
        p.terminate()

