# Module with all charmonia constants in [MeV]

GAMMA_ETAC2S  = 11.3
GAMMA_ETAC    = 32.0
GAMMA_CHIC0   = 10.8
GAMMA_CHIC1   = 0.84
GAMMA_CHIC2   = 1.97
GAMMA_HC      = 0.70
GAMMA_PSI3770 = 27.2

M_PROTON      = 938.272
M_PI0         = 134.977
M_PION        = 139.571
M_K0          = 496.611     #0.013
M_KAON        = 493.677     #0.016

M_ETAC        = 2983.9
M_JPSI        = 3096.9
M_CHIC0       = 3414.71     #0.30
M_CHIC1       = 3510.67     #0.05
M_CHIC2       = 3556.17     #0.07
M_HC          = 3525.38     #0.11
M_ETAC2S      = 3637.5      #1.1
M_PSI2S       = 3686.097    #0.025
M_PSI3770     = 3773.13     #0.35

# |M(ccbar) - M(J/psi)|
M_RES_ETAC    = 113.0
M_RES_L_CHIC0   = 317.81
M_RES_L_CHIC1   = 413.77
M_RES_L_CHIC2   = 459.27
M_RES_L_HC      = 428.9
M_RES_L_ETAC2S  = 540.6
M_RES_L_PSI2S   = 589.197
M_RES_L_PSI3770 = 676.23

# |M(ccbar) - M(psi(2S))|
M_RES_H_CHIC0   = 271.387
M_RES_H_CHIC1   = 175.427
M_RES_H_CHIC2   = 129.927
M_RES_H_HC      = 160.717
M_RES_H_ETAC2S  = 48.597
M_RES_H_PSI3770 = 87.033
