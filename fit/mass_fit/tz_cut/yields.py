from ROOT import gROOT, TFile, TCanvas, TF1, TH1D

import numpy as np
import logging 
# homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"
localDir = "/users/LHCb/zhovkovska/etac2s_ppbar/"
gROOT.LoadMacro(f"{localDir}/fit/libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro(f"{localDir}/fit/libs/libBifurcatedCB/BifurcatedCB.cxx+")
gROOT.LoadMacro(f"{localDir}/fit/libs/lhcbStyle.C")



sysName = {
    "rEtaToJpsi":       "\\pt-dependence of $\\sigma_{\\etac}/\\sigma_{\\jpsi}$",
    "Chebychev3par":    "Comb. bkg. description",
    "eff_pppi0":        "Contribution from \\JpsiToPpbarPiz" ,
    "eff_pppi0_x2":     "Contribution from \\JpsiToPpbarPiz x2" ,
    "xtalk":            "Cross-feed",
    "CB":               "Mass resolution model",
    "Gauss":            "Mass resolution model",
    "Gamma34":          "Variation of $\\Gamma_{\\etac}$",
    "Gamma":            "Variation of $\\Gamma_{\\etac}$",
    "GammaL":           "Variation of $\\Gamma_{\\etac}$",
    "Polariz":          "\\jpsi polarisation"
}
uncorr_syst = ["rEtaToJpsi","Chebychev3par","eff_pppi0","eff_pppi0_x2","xtalk","effRatio"]
# corr_syst = ["CB","Gamma34"]
corr_syst = ["Gauss","Gamma","GammaL"]
cross_talk_syst = ['effPP','effPB','effBP','effBB']

binning_dict = {
    "PT"    : np.array([5.0, 6.5, 8.0, 10.0, 12.0, 14.0, 20.0]),
    "Y"     : np.array([2.0, 2.5, 3.0, 3.5, 4.0]),
    "TOT"   : np.array([0.0, 1.0])
}

MODEL = "CB"

# CUTKEY = "_6-14_ProbNNp_06"
# CUTKEY = "_5-14_ProbNNp_06"
# CUTKEY = "_ProbNNp_06"
CUTKEY = "_oldPID"

def get_corrYield(nBin, syst_list, bins="PT", verbose=False):

    inCutKey = "RunIMethod"
    outDirName = f"{homeDir}/mass_fit/etac/CB"

    val_pr = dict()
    err_pr = dict()
    val_fb = dict()
    err_fb = dict()

    syst_c_pr = 0.
    syst_c_fb = 0.
    syst_uc_pr = 0.
    syst_uc_fb = 0.

    #import os, sys
    #sys.stdout = open("{}/syst_{}.txt".format(homeDir,inCutKey), "w" )
    if nBin==0: bins="PT"

    kSyst = "Base"
    nameWksp    = f"{outDirName}/Wksp_{bins}{nBin}_C{kSyst}{CUTKEY}.root"
    file_w = TFile(nameWksp)
    w_temp = file_w.Get("w")

    val_pr["Mean"] = w_temp.var("N_etac_Prompt").getVal()
    err_pr["Base"] = w_temp.var("N_etac_Prompt").getError()
    val_pr["Stat"] = 100*err_pr["Base"]/val_pr["Mean"]
    val_fb["Mean"] = w_temp.var("N_etac_FromB").getVal()
    err_fb["Base"] = w_temp.var("N_etac_FromB").getError()
    val_fb["Stat"] = 100*err_fb["Base"]/val_fb["Mean"]
    val_pr["effRatio"] = 0.8
    val_fb["effRatio"] = 0.8

    logging.debug(f" Bin {bins}: {nBin} \t Stat unc: {val_pr['Stat']}")


    for kSyst in syst_list:

        if kSyst=="xtalk":
            val_pr[kSyst], err_pr[kSyst], val_fb[kSyst], err_fb[kSyst] = 0., 0., 0., 0.
            for key in cross_talk_syst:
                nameWksp    = f"{outDirName}/Wksp_{bins}{nBin}_C{key}{CUTKEY}.root"
                file_w = TFile(nameWksp)
                w_temp = file_w.Get("w")

                val_pr[kSyst] += (w_temp.var("N_etac_Prompt").getVal() - val_pr["Mean"])**2
                # err_pr[kSyst] += w_temp.var("N_etac_Prompt").getError()
                err_pr[kSyst] += (w_temp.var("N_etac_Prompt").getError()/w_temp.var("N_etac_Prompt").getVal())**2
                val_fb[kSyst] += (w_temp.var("N_etac_FromB").getVal()  - val_fb["Mean"])**2
                # err_fb[kSyst] += w_temp.var("N_etac_FromB").getError()
                err_fb[kSyst] += (w_temp.var("N_etac_FromB").getError()/w_temp.var("N_etac_FromB").getVal())**2

                # print(val_pr[kSyst], val_fb[kSyst], val_pr["Mean"], val_fb["Mean"])


            val_pr[kSyst] = 100.*(val_pr[kSyst])**0.5/val_pr["Mean"]
            val_fb[kSyst] = 100.*(val_fb[kSyst])**0.5/val_fb["Mean"]

            err_pr[kSyst] = 100*(err_pr[kSyst])**0.5
            err_fb[kSyst] = 100*(err_fb[kSyst])**0.5

            # to avoid having 0 in TH1, since it's treated as Poisson 0
            if val_pr[kSyst] < 9e-6: val_pr[kSyst]=1e-8  
            if val_fb[kSyst] < 9e-6: val_fb[kSyst]=1e-8  

        elif kSyst=="Gamma":
            val_pr[kSyst] = 0.
            val_fb[kSyst] = 0.

            for key in ["GammaL", "Gamma"]:
                nameWksp    = f"{outDirName}/Wksp_{bins}{nBin}_C{key}{CUTKEY}.root"
                file_w = TFile(nameWksp)
                w_temp = file_w.Get("w")
                v_pr = 100.*abs(w_temp.var("N_etac_Prompt").getVal()/val_pr["Mean"] - 1.)
                v_fb = 100.*abs(w_temp.var("N_etac_FromB").getVal()/val_fb["Mean"] - 1.)
                logging.debug(f"Value pr: {v_pr}; Value fb: {v_fb}")

            # err_pr[kSyst] = w_temp.var("N_etac_Prompt").getError()

                if v_pr > val_pr[kSyst]:
                    val_pr[kSyst] = v_pr
                    err_pr[kSyst] = 100*w_temp.var("N_etac_Prompt").getError()/w_temp.var("N_etac_Prompt").getVal()

                if v_fb > val_fb[kSyst]:
                    val_fb[kSyst] = v_fb
                    err_fb[kSyst] = 100*w_temp.var("N_etac_FromB").getError()/w_temp.var("N_etac_FromB").getVal()

        elif kSyst=="effRatio":
            pass

        else:
            nameWksp    = f"{outDirName}/Wksp_{bins}{nBin}_C{kSyst}{CUTKEY}.root"
            file_w = TFile(nameWksp)
            w_temp = file_w.Get("w")

            val_pr[kSyst] = 100.*(w_temp.var("N_etac_Prompt").getVal()/val_pr["Mean"] - 1.)
            # err_pr[kSyst] = w_temp.var("N_etac_Prompt").getError()
            err_pr[kSyst] = 100*w_temp.var("N_etac_Prompt").getError()/w_temp.var("N_etac_Prompt").getVal()
            val_fb[kSyst] = 100.*(w_temp.var("N_etac_FromB").getVal()/val_fb["Mean"] - 1.)
            # err_fb[kSyst] = w_temp.var("N_etac_FromB").getError()
            err_fb[kSyst] = 100*w_temp.var("N_etac_FromB").getError()/w_temp.var("N_etac_FromB").getVal()

        if kSyst in corr_syst:
            syst_c_pr += val_pr[kSyst]**2
            syst_c_fb += val_fb[kSyst]**2

        if kSyst in uncorr_syst:
            syst_uc_pr += val_pr[kSyst]**2
            syst_uc_fb += val_fb[kSyst]**2



    if verbose:

        val_pr["Uncorr"] = syst_uc_pr**0.5
        val_fb["Uncorr"] = syst_uc_fb**0.5
        val_pr["Corr"]   = syst_c_pr**0.5
        val_fb["Corr"]   = syst_c_fb**0.5
        val_pr["Syst"]   = (syst_uc_pr + syst_c_pr)**0.5
        val_fb["Syst"]   = (syst_uc_fb + syst_c_fb)**0.5

        fo = open(f"{homeDir}/{MODEL}/syst_{inCutKey}_{bins}{nBin}{CUTKEY}.txt", "w" )
        key = "Mean"
        fo.write("{}:   {:.3f}  {:.2f} \n".format(key,val_pr[key],val_fb[key]))
        key = "Stat"
        fo.write("{}:   {:.3f}  {:.2f} \n".format(key,val_pr[key]*val_pr["Mean"]/100.,val_fb[key]))
        for key in syst_list:
            fo.write("{}:   {:.3f}  {:.3f} \n".format(key,val_pr[key]*val_pr["Mean"]/100.,val_fb[key]))

        fo.write("{}:   {:.3f}   {:.2f} \n".format("Syst corr", val_pr["Corr"]*val_pr["Mean"]/100., val_fb["Corr"]))
        fo.write("{}:   {:.3f}   {:.2f} \n".format("Syst uncorr",val_pr["Uncorr"]*val_pr["Mean"]/100., val_fb["Uncorr"]))
        fo.write("{}:   {:.3f}   {:.2f} \n".format("Syst", val_pr["Syst"]*val_pr["Mean"]/100., val_fb["Syst"]))

        #sys.stdout.close()
        fo.close()

    val_pr["Uncorr"] = 0.
    val_fb["Uncorr"] = 0.
    val_pr["Corr"]   = 0.
    val_fb["Corr"]   = 0.
    val_pr["Syst"]   = 0.
    val_fb["Syst"]   = 0.

    return val_pr, val_fb, err_pr, err_fb

def fit_syst(syst_list:list, bins:str="PT"):

    fitOpt = "IFM"
    hist_pr = dict()
    hist_fb = dict()
    func_pr = dict()
    func_fb = dict()
    x_arr = binning_dict[bins]
    xMax = x_arr[-1]
    xMin = x_arr[0]
    nBins = x_arr.size - 1

    val_pr = np.array([])
    val_fb = np.array([])
    err_pr = np.array([])
    err_fb = np.array([])

    for iBin in range(0, nBins+1):
        v_pr, v_fb, e_pr, e_fb = get_corrYield(iBin, syst_list, bins)
        val_pr = np.append(val_pr,v_pr)
        val_fb = np.append(val_fb,v_fb)
        err_pr = np.append(err_pr,e_pr)
        err_fb = np.append(err_fb,e_fb)

    from ROOT.TMath import Abs
    n=1
    n_canv = len(set(syst_list).intersection(uncorr_syst))
    # canvas = TCanvas("Systematics","Systematics",1200, 700)
    # canvas.Divide(n_canv, 2, 0.001, 0.001)

    for kSyst in set(syst_list).intersection(uncorr_syst):

        canvas = TCanvas("Systematics","Systematics",1200, 500)
        canvas.Divide(2, 1, 0.001, 0.001)
        # pad = canvas.cd(n)
        pad = canvas.cd(1)

        func_pr[kSyst] = TF1("f_pr_{}".format(kSyst),"pol1", xMin, xMax)
        func_fb[kSyst] = TF1("f_fb_{}".format(kSyst),"pol1", xMin, xMax)
        func_pr[kSyst].SetLineColor(2)
        func_fb[kSyst].SetLineColor(2)

        hist_pr[kSyst] = TH1D("h_pr_{}".format(kSyst),kSyst,nBins,x_arr)
        hist_fb[kSyst] = TH1D("h_fb_{}".format(kSyst),kSyst,nBins,x_arr)
        for iBin in range(1, nBins+1):
            hist_pr[kSyst].SetBinContent(iBin,Abs(val_pr[iBin][kSyst]))
            hist_fb[kSyst].SetBinContent(iBin,Abs(val_fb[iBin][kSyst]))
            hist_pr[kSyst].SetBinError(iBin,Abs(err_pr[iBin][kSyst]))
            hist_fb[kSyst].SetBinError(iBin,Abs(err_fb[iBin][kSyst]))

        hist_pr[kSyst].Fit(func_pr[kSyst],fitOpt)
        hist_pr[kSyst].Fit(func_pr[kSyst],fitOpt)
        hist_fb[kSyst].Fit(func_fb[kSyst],fitOpt)
        hist_fb[kSyst].Fit(func_fb[kSyst],fitOpt)

        for iBin in range(1, nBins+1):
            if kSyst!="xtalk" and kSyst!="effRatio":
                xVal = (x_arr[iBin-1] + x_arr[iBin])/2.
                val_pr[iBin][kSyst] = func_pr[kSyst].Eval(xVal)#/((x_arr[iBin-1] - x_arr[iBin])/2.)
                val_fb[iBin][kSyst] = func_fb[kSyst].Eval(xVal)#/((x_arr[iBin-1] - x_arr[iBin])/2.)
                # val_pr[iBin][kSyst] = func_pr[kSyst].Integral(x_arr[iBin-1],x_arr[iBin])/((x_arr[iBin] - x_arr[iBin-1]))
                # val_fb[iBin][kSyst] = func_fb[kSyst].Integral(x_arr[iBin-1],x_arr[iBin])/((x_arr[iBin] - x_arr[iBin-1]))

        pad.SetLeftMargin(0.15)
        hist_pr[kSyst].SetFillColor(17)
        # hist_pr[kSyst].SetFillStyle(3004)
        hist_pr[kSyst].Draw("")
        hist_pr[kSyst].Draw("E1 SAME")
        # pad.SetLogy()

        hist_fb[kSyst].SetFillColor(17)
        # hist_fb[kSyst].SetFillStyle(3000)
        # pad = canvas.cd(n_canv+n)
        pad = canvas.cd(2)
        pad.SetLeftMargin(0.15)
        hist_fb[kSyst].Draw("")
        hist_fb[kSyst].Draw("E1 SAME")
        # pad.SetLogy()
        print(kSyst,n)
        n+=1

        canvas.Draw()
        canvas.SaveAs("syst_{}_{}.pdf".format(kSyst,bins))


    # canvas.Draw()
    # canvas.SaveAs("syst_{}.pdf".format(bins))
    return val_pr, val_fb, func_pr, func_fb


def print_table(syst_list, bins, av=False):
    ''' Prints a table for a all bins with smoothing of systematics 
    syst_list: list of systematics to be included
    bins: type of binning
    av: if True, print tables with all systematics for averaging'''
    
    x_arr = binning_dict[bins]
    xMax = x_arr[-1]
    xMin = x_arr[0]
    nBins = x_arr.size - 1
    logging.info(f"Number of Bins: {nBins}")

    v_pr_0, v_fb_0, _, _ = get_corrYield(0,syst_list,bins,True)
    v_pr, v_fb, fit_pr, fit_fb = fit_syst(syst_list, bins)
    logging.debug(f"Values: {v_pr}, {v_fb}, {fit_pr}, {fit_fb}")

    inCutKey = "RunIMethod"
    # f = TFile(f"/sps/lhcb/zhovkovska/Results_ppbar_2018/MC/polariz_eff_total_{bins}.root","READ")
    f = TFile(f"{homeDir}/MC/polariz/polariz_eff_total_{bins}.root","READ")
    h_pol = f.Get("histEff").Clone()
    f_pol = f.Get(f"polariz_{bins}0").Clone()

    # file for CS plot drawing
    fo_gen = open(f"{homeDir}/{MODEL}/rel_{bins}_{inCutKey}{CUTKEY}.txt", "w" )

    prec = 1
    #for kSyst in syst_list:
    for iBin in range(0, nBins+1):

        # file for CS LaTeX tables
        if av:
            namefile = f"{homeDir}/{MODEL}/table_{inCutKey}_{bins}{iBin}{CUTKEY}_av.txt"
        else:
            namefile = f"{homeDir}/{MODEL}/table_{inCutKey}_{bins}{iBin}{CUTKEY}.txt"
        fo = open(namefile, "w" )
        if av:
            fo.write(f"{'Mean':36s}  {round(v_pr[iBin]['Mean'],3)}  {round(v_fb[iBin]['Mean'], 3)} \n")
            fo.write(f"{'Stat':36s}  {round(v_pr[iBin]['Stat'],prec)}  {round(v_fb[iBin]['Stat'],prec)} \n")
        else:
            fo.write(f"{'Mean value':36s}  & {round(v_pr[iBin]['Mean'],3)} \t  & {round(v_fb[iBin]['Mean'], 3)} \\\\ \\hline \n")
            fo.write(f"{'Stat. uncertainty':36s}  & {round(v_pr[iBin]['Stat'],prec)} \t  & {round(v_fb[iBin]['Stat'],prec)} \\\\ \\hline \n")

        fo_gen.write("0 0 {:.4f} {:.4f} \n".format(v_pr[iBin]["Mean"], v_fb[iBin]["Mean"]))
        fo_gen.write("0 0 {:.4f} {:.4f} \n".format(v_pr[iBin]["Stat"]/100.*v_pr[iBin]["Mean"], v_fb[iBin]["Stat"]/100.*v_fb[iBin]["Mean"]))

        for kSyst in set(syst_list).intersection(uncorr_syst):

            if av:
                fo.write(f"{kSyst:36s}  {round(abs(v_pr[iBin][kSyst]),prec)}  {round(abs(v_fb[iBin][kSyst]),prec)} \n")
            else:
                fo.write(f"{sysName[kSyst]:36s}  & {round(abs(v_pr[iBin][kSyst]),prec)} \t  & {round(abs(v_fb[iBin][kSyst]),prec)} \\\\ \n")

            v_pr[iBin]["Uncorr"] += v_pr[iBin][kSyst]**2
            v_fb[iBin]["Uncorr"] += v_fb[iBin][kSyst]**2


        v_pr[iBin]["Syst"] = v_pr[iBin]["Uncorr"]
        v_fb[iBin]["Syst"] = v_fb[iBin]["Uncorr"]
        v_pr[iBin]["Uncorr"] = v_pr[iBin]["Uncorr"]**0.5
        v_fb[iBin]["Uncorr"] = v_fb[iBin]["Uncorr"]**0.5

        if av:
            fo.write(f"{'Uncorr.':36s}  {round(v_pr[iBin]['Uncorr'], prec)}  {round(v_fb[iBin]['Uncorr'],prec)} \n")
        else:    
            fo.write(" \\hline \\hline \n")
            fo.write(f"{'Tot. syst. uncorr.':36s}  & {round(v_pr[iBin]['Uncorr'], prec)} \t  & {round(v_fb[iBin]['Uncorr'],prec)} \\\\ \n \\hline \\hline \n")
            # fo.write("{} & {:.2f} & {:.2f} \\\\ \\hline \\hline \n".format("Tot. Syst. Uncorr.",v_pr[iBin]["Uncorr"], v_fb[iBin]["Uncorr"]))

        fo_gen.write("0 0 {:.4f} {:.4f} \n".format(v_pr[iBin]["Uncorr"]/100.*v_pr[iBin]["Mean"], v_fb[iBin]["Uncorr"]/100.*v_fb[iBin]["Mean"]))

        for kSyst in set(syst_list).intersection(corr_syst):

            if av:
                fo.write(f"{kSyst:36s}  {round(abs(v_pr_0[kSyst]),prec)}  {round(abs(v_fb_0[kSyst]),prec)} \n")
            else:
                fo.write(f"{sysName[kSyst]:36s}  & {round(abs(v_pr_0[kSyst]),prec)} \t  & {round(abs(v_fb_0[kSyst]),prec)} \\\\ \n")

            v_pr[iBin]["Corr"] += v_pr_0[kSyst]**2
            v_fb[iBin]["Corr"] += v_fb_0[kSyst]**2

        if iBin!=0:
            v_pr[iBin]["Polariz"] = 100.*h_pol.GetBinContent(iBin)
        else:
            v_pr[iBin]["Polariz"] = 100.*f_pol.GetParameter(0)

        v_pr[iBin]["Corr"] += v_pr[iBin]["Polariz"]**2

        if av:
            fo.write(f"{'Polariz':36s}  {round(v_pr[iBin]['Polariz'], prec)}  {0:.1f} \n")
        else:
            fo.write(f"{sysName['Polariz']:36s}  & {round(v_pr[iBin]['Polariz'], prec)} \t  & {0:.1f} \\\\ \n")
        # fo.write("{} & {:.2f} & {:.2f} \\\\ \n".format(sysName["Polariz"],v_pr[iBin]["Polariz"], 0))

        v_pr[iBin]["Syst"] = (v_pr[iBin]["Syst"] + v_pr[iBin]["Corr"])**0.5
        v_fb[iBin]["Syst"] = (v_fb[iBin]["Syst"] + v_fb[iBin]["Corr"])**0.5
        v_pr[iBin]["Corr"] = v_pr[iBin]["Corr"]**0.5
        v_fb[iBin]["Corr"] = v_fb[iBin]["Corr"]**0.5

        if av:
            fo.write(f"{'Corr.':36s}  {round(v_pr[iBin]['Corr'],prec)}  {round(v_fb[iBin]['Corr'],prec)} \n")
            fo.write(f"{'Syst.':36s}  {round(v_pr[iBin]['Syst'],prec)}  {round(v_fb[iBin]['Syst'],prec)} \n")
        else:
            fo.write(f"{'Tot. syst. corr.':36s}  & {round(v_pr[iBin]['Corr'],prec)} \t  & {round(v_fb[iBin]['Corr'],prec)} \\\\ \n \\hline \\hline \n")
            fo.write(f"{'Tot. syst.':36s} & {round(v_pr[iBin]['Syst'],prec)} \t & {round(v_fb[iBin]['Syst'],prec)} \\\\ \n \\hline \\hline \n")
        # fo.write("{} & {:.2f} & {:.2f} \\\\ \\hline \\hline \n".format("Tot. Syst. Corr.",v_pr[iBin]["Corr"], v_fb[iBin]["Corr"]))
        # fo.write("{} & {:.2f} & {:.2f} \\\\ \\hline \\hline \n".format("Tot. Syst.",v_pr[iBin]["Syst"], v_fb[iBin]["Syst"]))

        fo_gen.write("0 0 {:.4f} {:.4f} \n".format(v_pr[iBin]["Corr"]/100.*v_pr[iBin]["Mean"], v_fb[iBin]["Corr"]/100.*v_fb[iBin]["Mean"]))
        fo_gen.write("0 0 {:.4f} {:.4f} \n \n".format(v_pr[iBin]["Syst"]/100.*v_pr[iBin]["Mean"], v_fb[iBin]["Syst"]/100.*v_fb[iBin]["Mean"]))
        fo.close()

    fo_gen.close()
    f.Close()

def print_single(iBin, syst_list, bins, inCutKey = "RunIMethod"):
    ''' Prints a table for a single bin without smoothing of systematics '''

    x_arr = binning_dict[bins]
    nBins = x_arr.size - 1
    logging.debug(f" Number of {bins} bins: {nBins}")

    v_pr_0, v_fb_0, _, _ = get_corrYield(0,syst_list,bins,True)
    v_pr, v_fb, _, _     = get_corrYield(iBin,syst_list,bins,True)
    logging.debug(f"{v_pr} \n {v_fb}",stacklevel=5)

    f = TFile(f"{homeDir}/MC/polariz/polariz_eff_total_{bins}.root","READ")
    h_pol = f.Get("histEff")
    f_pol = f.Get(f"polariz_{bins}0")

    # file for CS plot drawing
    fo_gen = open(f"{homeDir}/{MODEL}/rel_{bins}{iBin}_{inCutKey}{CUTKEY}.txt", "w" )

    prec = 1
    #for kSyst in syst_list:

    # file for CS LaTeX tables
    filelatex = f"{homeDir}/{MODEL}/table_{inCutKey}_{bins}{iBin}{CUTKEY}.txt"
    fo = open(filelatex, "w" )
    fo.write(f"{'Mean value':36s}  & {round(v_pr['Mean'],3)} \t  & {round(v_fb['Mean'], 3)} \\\\ \\hline \n")
    fo.write(f"{'Stat. uncertainty':36s}  & {round(v_pr['Stat'],prec)} \t  & {round(v_fb['Stat'],prec)} \\\\ \\hline \n")

    fo_gen.write("0 0 {:.4f} {:.4f} \n".format(v_pr["Mean"], v_fb["Mean"]))
    fo_gen.write("0 0 {:.4f} {:.4f} \n".format(v_pr["Stat"]/100.*v_pr["Mean"], v_fb["Stat"]/100.*v_fb["Mean"]))

    for kSyst in set(syst_list).intersection(uncorr_syst):

        fo.write(f"{sysName[kSyst]:36s}  & {round(v_pr[kSyst],prec)} \t  & {round(v_fb[kSyst],prec)} \\\\ \n")

        v_pr["Uncorr"] += v_pr[kSyst]**2
        v_fb["Uncorr"] += v_fb[kSyst]**2


    v_pr["Syst"] = v_pr["Uncorr"]
    v_fb["Syst"] = v_fb["Uncorr"]
    v_pr["Uncorr"] = v_pr["Uncorr"]**0.5
    v_fb["Uncorr"] = v_fb["Uncorr"]**0.5

    fo.write(" \\hline \\hline \n")
    fo.write(f"{'Tot. syst. uncorr.':36s}  & {round(v_pr['Uncorr'], prec)} \t  & {round(v_fb['Uncorr'],prec)} \\\\ \n \\hline \\hline \n")

    fo_gen.write("0 0 {:.4f} {:.4f} \n".format(v_pr["Uncorr"]/100.*v_pr["Mean"], v_fb["Uncorr"]/100.*v_fb["Mean"]))

    for kSyst in set(syst_list).intersection(corr_syst):

        fo.write(f"{sysName[kSyst]:36s}  & {round(v_pr_0[kSyst],prec)} \t  & {round(v_fb_0[kSyst],prec)} \\\\ \n")

        v_pr["Corr"] += v_pr_0[kSyst]**2
        v_fb["Corr"] += v_fb_0[kSyst]**2

    if iBin!=0:
        v_pr["Polariz"] = 100.*h_pol.GetBinContent(iBin)
    else:
        v_pr["Polariz"] = 100*f_pol.GetParameter(0)
        # v_pr["Polariz"] = 1.6

    v_pr["Corr"] += v_pr["Polariz"]**2

    fo.write(f"{sysName['Polariz']:36s}  & {round(v_pr['Polariz'], prec)} \t  & {0:.1f} \\\\ \n")

    v_pr["Syst"] = (v_pr["Syst"] + v_pr["Corr"])**0.5
    v_fb["Syst"] = (v_fb["Syst"] + v_fb["Corr"])**0.5
    v_pr["Corr"] = v_pr["Corr"]**0.5
    v_fb["Corr"] = v_fb["Corr"]**0.5

    fo.write(f"{'Tot. syst. corr.':36s}  & {round(v_pr['Corr'],prec)} \t  & {round(v_fb['Corr'],prec)} \\\\ \n \\hline \\hline \n")
    fo.write(f"{'Tot. syst.':36s}  & {round(v_pr['Syst'],prec)} \t  & {round(v_fb['Syst'],prec)} \\\\ \n \\hline \\hline \n")
 
    fo_gen.write("0 0 {:.4f} {:.4f} \n".format(v_pr["Corr"]/100.*v_pr["Mean"], v_fb["Corr"]/100.*v_fb["Mean"]))
    fo_gen.write("0 0 {:.4f} {:.4f} \n \n".format(v_pr["Syst"]/100.*v_pr["Mean"], v_fb["Syst"]/100.*v_fb["Mean"]))

    fo_gen.close()
    f.Close()
    logging.info(f" File {filelatex} was created")

if __name__ == "__main__":
    # nConfs = ["rEtaToJpsi","Chebychev3par","eff_pppi0","xtalk","CB","Gamma34"] #,"effRatio"
    nConfs = ["rEtaToJpsi","Chebychev3par","eff_pppi0","xtalk","Gauss","Gamma"] #,"effRatio"
    nBins = 6

    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

    # for iPT in range (0,nBins+1):
        # get_corrYield(iPT,nConfs,"PT",True)

    # print_single(0, nConfs, "PT", "RunIMethod")
    fit_syst(nConfs, "PT")
    # fit_syst(nConfs, "Y")

    print_table(nConfs, "PT", False)
    # print_table(nConfs, "Y", False)
    # get_corrYield(0,nConfs,"PT",True)