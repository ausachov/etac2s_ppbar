from ROOT import gROOT, TFile, \
    RooFit, RooRealVar, RooFormulaVar, RooArgList, \
    RooGaussian, RooFFTConvPdf, RooGenericPdf, RooChebychev, RooAddPdf

# histosDir = "/users/LHCb/usachov/zhovkovska2018/scripts/Histos/"
# homeDir = "/users/LHCb/usachov/zhovkovska2018/scripts/"
#homeDir = "/sps/lhcb/zhovkovska/etacToPpbar/"
# homeDir = "/sps/lhcb/zhovkovska/Results_ppbar_2018/"
homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/"
localDir = "/users/LHCb/zhovkovska/etac2s_ppbar/"

gROOT.LoadMacro(f"{localDir}/fit/libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro(f"{localDir}/fit/libs/libBifurcatedCB/BifurcatedCB.cxx+")
from ROOT import RooRelBreitWigner, BifurcatedCB

binningDict = {
    "Jpsi_PT":                      [5000., 6500., 8000., 10000., 12000., 14000., 20000.],
    "Jpsi_Y":                       [2.0, 2.5, 3.0, 3.5, 4.0, 4.5],
    "TMath::Abs(ProtonM_CosTheta)": [0.0, 0.15, 0.3, 0.50, 1.0],
    "nSPDHits":                     [0, 100, 150, 200, 250, 300],
}



#
minM = 2850
maxM = 3250
nBins = 1000

def getConstPars(nConf, nPT=0):

    if nConf!="Gauss":
        modName = "_CB"
    else:
        modName = ""

    f = TFile(f"{homeDir}/MC/etac/mass_fit/MC_MassResolution{modName}_wksp.root","READ")
    # f = TFile(f"{homeDir}/MC/mass_fit/Wksp_M_res_all_jpsi_sim{modName}.root")
    wMC = f.Get("w").Clone()
    f.Close()

    r_ref_etac = wMC.var("r_ref_etac").getValV()
    rNtoW = 1.0
    rArea = 1.0
    bkgOpt = 'Base'
    gamma = 32.0
    effic = 0.057*1.19/2.12
    # effic = 0.052*1.19/2.12
    #gamma = 21.3
    #effic = 0.06
    #ratioNtoW = 0.5
    #ratioEtaToJpsi = 0.88
    #ratioArea = 0.9
    rScSigma = 1.0

    if nPT!=0:
        f = TFile(f"{homeDir}/MC/etac/mass_fit/MC_MassResolution{modName}_wksp_PT{nPT}.root","READ")
        # f = TFile(f"{homeDir}/MC/etac/mass_fit/MC_MassResolution{modName}_wksp_Y{nPT}.root","READ")
        wMC = f.Get("w").Clone()
        f.Close()
        r_ref_etac = wMC.var("r_ref_etac").getValV()

    if (nConf == 1):
        #rNtoW = wMC.var("r_NToW").getValV()+wMC.var("r_NToW").getError()
        f = TFile(f"{homeDir}/MC/etac/mass_fit/parFit_CB_GeV_PT6.root","READ")
        # f = TFile(f"{homeDir}/MC/etac/mass_fit/parFit_GeV_PT6.root","READ")
        fSigma = f.Get("fSigmaMC")
        f.Close()
        sigma = 8.0
        ptL = binningDict["Jpsi_PT"][nPT-1]/1000.
        ptR = binningDict["Jpsi_PT"][nPT]/1000.
        rScSigma = fSigma.Eval((ptR+ptL)/2.)/sigma
    elif (nConf == 'rEtaToJpsi'):
        f = TFile(f"{homeDir}/MC/etac/mass_fit/parFit_CB_GeV_PT6.root","READ")
        # f = TFile(f"{homeDir}/MC/etac/mass_fit/parFit_CB_GeV_Y4.root","READ")
        func = f.Get("fit_r_ref_etac")
        # func = f.Get("fit_rEta2Jpsi")
        f.Close()
        if nPT==0:
            r_ref_etac = wMC.var("r_ref_etac").getValV()+wMC.var("r_ref_etac").getError()
        else:
            ptL = binningDict["Jpsi_PT"][nPT-1]/1000.
            ptR = binningDict["Jpsi_PT"][nPT]/1000.
            r_ref_etac = func.Eval((ptR+ptL)/2.)
        #ptL = binningDict["Jpsi_Y"][nPT-1]
        #ptR = binningDict["Jpsi_Y"][nPT]
        #r_ref_etac = func.Eval((ptR+ptL)/2.)
        #print r_ref_etac
        #input()
    elif (nConf == 'Gauss'):
        # f = TFile(f"{homeDir}/MC/mass_fit/Wksp_M_res_all_jpsi_sim{modName}.root")
        f = TFile(f"{homeDir}/MC/etac/mass_fit/MC_MassResolution{modName}_wksp.root","READ")
        wMC = f.Get("w").Clone()
        f.Close()
        rNtoW = wMC.var("r_NToW").getValV()
        rArea = wMC.var("r_G1ToG2").getValV()
    elif (nConf == 'Chebychev3par' or nConf == 'Chebychev4par'):
        bkgOpt = nConf
    elif (nConf == 'Gamma'):
        gamma = 32.7
    elif (nConf == 'GammaL'):
        gamma = 31.3
    elif (nConf == 'Gamma34'):
        gamma = 34.0
    elif (nConf == 'eff_pppi0'):
        effic = 0.057*1.19/2.12*(1+0.11)
    elif (nConf == 'eff_pppi0_x2'):
        effic = 0.057*1.19/2.12*(2.)
        #(0.11) = ((0.0006/0.057)**2 + (0.08/1.19)**2) ** 0.5
        # effic = 0.052*1.19/2.12*(1+0.06)
    print ("SETUP ",nConf)

    #if (nPT != 0):
        #nameFile = "results/MC/MassFit/MC_Mass_2016_wksp_PT%d.root"%(nPT)
        #f = TFile(nameFile,"READ")
        #wMC = f.Get("w;1")
        #f.Close()
        #if (nConf == 1):
            #ratioEtaToJpsi = wMC.var("r_ref_etac").getValV()+wMC.var("r_ref_etac").getError()
        #else:
            #ratioEtaToJpsi = wMC.var("r_ref_etac").getValV()

    return rNtoW,r_ref_etac,rArea, bkgOpt, gamma, effic




def fillRelWorkspace(w, nConf, nPT, key):


    Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M",2850,3250)
    #Jpsi_M = w.var("Jpsi_M")
    Jpsi_M.setBins(1000,"cache")

    mDiffEtac = 113.501

    ratioNtoW,ratioEtaToJpsi,ratioArea, bkgType, gammaEtac, eff = getConstPars(nConf, nPT)


    if (nConf=='halfBinShift'):
        Jpsi_M.setRange(2855,3245)


    r_ref_etac = RooRealVar("r_ref_etac","r_ref_etac", ratioEtaToJpsi)
    r_NToW = RooRealVar("r_NToW","r_NToW",ratioNtoW)
    r_G1ToG2 = RooRealVar("r_G1ToG2","r_G1ToG2",ratioArea)
    eff_pppi0 = RooRealVar("eff_pppi0","eff_pppi0",eff)

    n_etac = RooRealVar(f"n_etac_{key}","num of Etac", 10, 1.e7)
    n_jpsi = RooRealVar(f"n_jpsi_{key}","num of J/Psi", 10, 1.e7)
    n_etac_rel = RooRealVar(f"n_etac_rel_{key}","num of Etac", 0.0, 3.0)
    #n_etac_rel = RooRealVar(f"n_etac_rel_{key}","n_etac_rel", "@0/@1",RooArgList(n_etac,n_jpsi))

    #n_etac_1 = RooFormulaVar(f"n_etac_1_{key}","num of Etac","@0*@1*@2",RooArgList(n_etac_rel,n_jpsi,r_G1ToG2))
    #n_etac_2 = RooFormulaVar(f"n_etac_2_{key}","num of Etac","@0*@1-@2",RooArgList(n_etac_rel,n_jpsi,n_etac_1))
    n_etac_1 = RooFormulaVar(f"n_etac_1_{key}","num of Etac","@0*@1",RooArgList(n_etac,r_G1ToG2))
    n_etac_2 = RooFormulaVar(f"n_etac_2_{key}","num of Etac","@0-@1",RooArgList(n_etac,n_etac_1))
    n_jpsi_1 = RooFormulaVar(f"n_jpsi_1_{key}","num of J/Psi","@0*@1",RooArgList(n_jpsi,r_G1ToG2))
    n_jpsi_2 = RooFormulaVar(f"n_jpsi_2_{key}","num of J/Psi","@0-@1",RooArgList(n_jpsi,n_jpsi_1))
    n_bkgr = RooRealVar(f"n_bkgr_{key}","num of backgr",7e7,10,1.e+9)


    mass_jpsi = RooRealVar("mass_jpsi","mean of gaussian", 3096.9, 3030, 3150)
    mass_res = RooRealVar("mass_res","mean of gaussian",mDiffEtac, 80, 130)

    if (nConf=='constDM' or nConf==8):  mass_res.setConstant(True)

    mass_eta = RooFormulaVar("mass_etac","mean of gaussian","@0-@1",RooArgList(mass_jpsi,mass_res))
    gamma_eta = RooRealVar("gamma_etac","width of Br-W", gammaEtac, 10., 50.)
    if nConf!="GammaFree":
        gamma_eta.setConstant()

    spin_eta = RooRealVar("spin_etac","spin_etac", 0. )
    radius_eta = RooRealVar("radius_etac","radius", 1.)
    proton_m = RooRealVar("proton_m","proton mass", 938.3 )
    sigma_etac_1 = RooRealVar(f"sigma_etac_1_PT{nPT}","width of gaussian", 9., 5., 50.)
    sigma_etac_2 = RooFormulaVar(f"sigma_etac_2_PT{nPT}","width of gaussian","@0/@1",RooArgList(sigma_etac_1,r_NToW))

    sigma_jpsi_1 = RooFormulaVar(f"sigma_jpsi_1_PT{nPT}","width of gaussian","@0/@1",RooArgList(sigma_etac_1,r_ref_etac))
    sigma_jpsi_2 = RooFormulaVar(f"sigma_jpsi_2_PT{nPT}","width of gaussian","@0/@1",RooArgList(sigma_jpsi_1,r_NToW))


    # Fit eta
    gauss_etac_1 = RooGaussian(f"gauss_etac_1_PT{nPT}","gauss_etac_1 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_etac_1)
    gauss_etac_2 = RooGaussian(f"gauss_etac_2_PT{nPT}","gauss_etac_2 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_etac_2)

    br_wigner = RooRelBreitWigner("br_wigner", "br_wigner",Jpsi_M, mass_eta, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)

    bwxg_1 = RooFFTConvPdf(f"bwxg_1_PT{nPT}","breit-wigner (X) gauss", Jpsi_M, br_wigner, gauss_etac_1)
    bwxg_2 = RooFFTConvPdf(f"bwxg_2_PT{nPT}","breit-wigner (X) gauss", Jpsi_M, br_wigner, gauss_etac_2)

    # Fit J/psi
    gauss_1 = RooGaussian(f"gauss_1_PT{nPT}","gaussian PDF",Jpsi_M, mass_jpsi, sigma_jpsi_1)
    gauss_2 = RooGaussian(f"gauss_2_PT{nPT}","gaussian PDF",Jpsi_M, mass_jpsi, sigma_jpsi_2)


    if (bkgType == 'Base'):
        a0 = RooRealVar(f"a0_{key}","a0",0.4,0.,5.)
    else:
        a0 = RooRealVar(f"a0_{key}","a0",0.4,-1.,1.)

    a1 = RooRealVar(f"a1_{key}","a1",0.05,-1.,1.)
    a2 = RooRealVar(f"a2_{key}","a2",-0.005,-1.,1.)
    a3 = RooRealVar(f"a3_{key}","a3",0.005,-0.1,0.1)
    a4 = RooRealVar(f"a4_{key}","a4",0.005,-0.1,0.1)


    if (bkgType == 'Base'):
        #a0.setRange(0.0, 5.e2)
        a1.setVal(0.04)
        a2.setVal(0.04)
        bkg = RooGenericPdf(f"bkg_{key}","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.+@3*(@0-3050.)*(@0-3050.)/200./200.)",RooArgList(Jpsi_M,a0,a1,a2))
        #bkg = RooGenericPdf("bkg","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*@0.+@3*(2*@0*@0-1))",RooArgList(Jpsi_M,a0,a1,a2))
    elif (bkgType == 'Chebychev3par'):
        bkg = RooChebychev (f"bkg_{key}","Background",Jpsi_M,RooArgList(a0,a1,a2))
    elif (bkgType == 'Chebychev4par'):
        bkg = RooChebychev(f"bkg_{key}","Background",Jpsi_M,RooArgList(a0,a1,a2,a3))


    pppi0 = RooGenericPdf("pppi0","jpsi2pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))
    n_pppi0 = RooFormulaVar(f"n_pppi0_{key}","n_pppi0","@0*@1",RooArgList(n_jpsi,eff_pppi0))


    modelBkg = RooAddPdf(f"modelBkg_{key}","background", RooArgList(bkg,pppi0), RooArgList(n_bkgr,n_pppi0))

    if(nConf=='Gauss'):
        modelSignal = RooAddPdf(f"modelSignal_{key}","signal", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2), RooArgList(n_etac_1,n_etac_2,n_jpsi_1,n_jpsi_2))
        model = RooAddPdf(f"model_{key}","signal+bkg", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2, bkg,pppi0), RooArgList(n_etac_1,n_etac_2,n_jpsi_1,n_jpsi_2, n_bkgr,n_pppi0))
    else:
        # f = TFile(homeDir+"/MC/MassFit/MC_MassResolution_CB_Integr_wksp.root","READ")
        # f = TFile(f"{homeDir}/MC/mass_fit/Wksp_M_res_all_jpsi_sim_CB.root")
        f = TFile(f"{homeDir}/MC/etac/mass_fit/MC_MassResolution_CB_wksp.root","READ")
        wMC = f.Get("w").Clone()
        f.Close()
        alpha_1 = RooRealVar('alpha_1','alpha of CB', wMC.var('alpha_1').getValV(), 0.0, 10.)
        alpha_1.setConstant(True)
        nCB_1 = RooRealVar('nCB_1','n of CB', wMC.var('nCB_1').getValV(), 0.0, 100.)
        nCB_1.setConstant(True)
        # Fit eta
        cb_etac_1 = BifurcatedCB(f"cb_etac_1_PT{nPT}", "Cystal Ball Function", Jpsi_M, RooFit.RooConst(0), sigma_etac_1, alpha_1, nCB_1, alpha_1, nCB_1)
        bwxg_1 = RooFFTConvPdf(f"bwxg_1_PT{nPT}","breit-wigner (X) gauss", Jpsi_M, br_wigner, cb_etac_1)
        # Fit J/psi
        cb_jpsi_1 = BifurcatedCB(f"cb_jpsi_1_PT{nPT}", "Cystal Ball Function", Jpsi_M, mass_jpsi, sigma_jpsi_1, alpha_1, nCB_1, alpha_1, nCB_1)
        modelSignal = RooAddPdf(f"modelSignal_{key}","signal", RooArgList(bwxg_1,cb_jpsi_1), RooArgList(n_etac,n_jpsi))
        model = RooAddPdf(f"model_{key}","signal+bkg", RooArgList(bwxg_1,cb_jpsi_1, bkg,pppi0), RooArgList(n_etac,n_jpsi, n_bkgr,n_pppi0))

    # if(nConf=='CB'):
    #     # f = TFile(homeDir+"/MC/MassFit/MC_MassResolution_CB_Integr_wksp.root","READ")
    #     f = TFile(homeDir+"/MC/etac/MassFit/MC_MassResolution_CB_wksp.root","READ")
    #     wMC = f.Get("w")
    #     f.Close()
    #     alpha_etac_1 = RooRealVar('alpha_etac_1','alpha of CB', wMC.var('alpha_etac_1').getValV(), 0.0, 10.)
    #     alpha_etac_1.setConstant(True)
    #     n_etac_1 = RooRealVar('n_etac_1','n of CB', wMC.var('n_etac_1').getValV(), 0.0, 100.)
    #     n_etac_1.setConstant(True)
    #     # Fit eta
    #     cb_etac_1 = BifurcatedCB("cb_etac_1_PT%s"%(nPT), "Cystal Ball Function", Jpsi_M, RooFit.RooConst(0), sigma_etac_1, alpha_etac_1, n_etac_1, alpha_etac_1, n_etac_1)
    #     bwxg_1 = RooFFTConvPdf("bwxg_1_PT%s"%(nPT),"breit-wigner (X) gauss", Jpsi_M, br_wigner, cb_etac_1)
    #     # Fit J/psi
    #     cb_jpsi_1 = BifurcatedCB("cb_jpsi_1_PT%s"%(nPT), "Cystal Ball Function", Jpsi_M, mass_jpsi, sigma_jpsi_1, alpha_etac_1, n_etac_1, alpha_etac_1, n_etac_1)
    #     modelSignal = RooAddPdf(f"modelSignal_{key}","signal", RooArgList(bwxg_1,cb_jpsi_1), RooArgList(n_etac,n_jpsi))
    #     model = RooAddPdf(f"model_{key}","signal+bkg", RooArgList(bwxg_1,cb_jpsi_1, bkg,pppi0), RooArgList(n_etac,n_jpsi, n_bkgr,n_pppi0))
    # else:
    #     modelSignal = RooAddPdf(f"modelSignal_{key}","signal", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2), RooArgList(n_etac_1,n_etac_2,n_jpsi_1,n_jpsi_2))
    #     model = RooAddPdf(f"model_{key}","signal+bkg", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2, bkg,pppi0), RooArgList(n_etac_1,n_etac_2,n_jpsi_1,n_jpsi_2, n_bkgr,n_pppi0))


    #modelC = RooProdPdf("modelC", "model with constraints",RooArgList(model,fconstraint,fconstrJpsi))
    #modelC = RooProdPdf(f"modelC_{key}", "model with constraints",RooArgList(model,fconstraint))

    getattr(w,'import')(model, RooFit.RecycleConflictNodes())
    #getattr(w,'import')(modelC, RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelBkg, RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelSignal, RooFit.RecycleConflictNodes())