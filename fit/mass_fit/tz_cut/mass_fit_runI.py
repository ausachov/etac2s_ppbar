from ROOT import TChain, TTree, TH1D, TCut, TFile, \
                RooFit, RooWorkspace, RooRealVar, RooDataHist, RooGaussian, \
                RooArgList, RooArgSet, RooLinkedList, RooCmdArg, RooCategory, RooSimultaneous
from ROOT import gROOT

from setupPlot import setupCanvas, setupFrame
# from setupFitModel_RunI import fillRelWorkspace, binningDict
from setupFitModel_RunII import fillRelWorkspace, binningDict

import logging
import sys

# homeDir = "/sps/lhcb/zhovkovska/Results_ppbar_2018/mass_fit/RunIMethod/"
# homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/mass_fit/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/mass_fit/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"
localDir = "/users/LHCb/zhovkovska/etac2s_ppbar/"

sys.path.insert(1,f"{localDir}/fit/libs/")
gROOT.LoadMacro(f"{localDir}/fit/libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro(f"{localDir}/fit/libs/libBifurcatedCB/BifurcatedCB.cxx+")
gROOT.LoadMacro(f"{localDir}/fit/libs/lhcbStyle.C")

from crosstalk import crosstalk

binWidth = 0.2
binWidthDraw = 0.2

minM_Low = 2850
maxM_Low = 3250
binN_Low = int((maxM_Low-minM_Low)/binWidth)

# CUTKEY = "_6-14_ProbNNp_06"
# CUTKEY = "_5-14_ProbNNp_06"
CUTKEY = "_ProbNNp_06"
# CUTKEY = "_oldPID"


def correctedYield(w, nPT, nConf, bins="PT"):

    # effPP, effPB, effBP, effBB, effPP_err, effPB_err, effBP_err, effBB_err  = promptImp(nPT, bins)
    effPP, effPB, effBP, effBB, effPP_err, effPB_err, effBP_err, effBB_err  = crosstalk(nB=nPT, bins=bins, cutkey=CUTKEY)    

    if nConf=="effPP":
            effPP += effPP_err
    elif nConf=="effPB":
            effPB += effPB_err
    elif nConf=="effBP":
            effBP += effBP_err
    elif nConf=="effBB":
            effBB += effBB_err


    n_jpsi_Pr = w.var(f"n_jpsi_PT{nPT}_Prompt").getValV()
    n_jpsi_Fb = w.var(f"n_jpsi_PT{nPT}_FromB").getValV()
    n_etac_Pr = w.var(f"n_etac_PT{nPT}_Prompt").getValV()
    n_etac_Fb = w.var(f"n_etac_PT{nPT}_FromB").getValV()

    # n_jpsi_Pr_Err  = w.var("n_jpsi_PT{}_Prompt".format(nPT)).getError()
    # n_jpsi_Fb_Err = w.var("n_jpsi_PT{}_FromB".format(nPT)).getError()
    # n_etac_Pr_Err  = w.var("n_etac_PT{}_Prompt".format(nPT)).getError()
    # n_etac_Fb_Err = w.var("n_etac_PT{}_FromB".format(nPT)).getError()

    n_jpsi_Pr_Err = (w.var(f"n_jpsi_PT{nPT}_Prompt").getErrorHi() - w.var(f"n_jpsi_PT{nPT}_Prompt").getErrorLo())/2.
    n_jpsi_Fb_Err = (w.var(f"n_jpsi_PT{nPT}_FromB").getErrorHi()  - w.var(f"n_jpsi_PT{nPT}_FromB").getErrorLo())/2.
    n_etac_Pr_Err = (w.var(f"n_etac_PT{nPT}_Prompt").getErrorHi() - w.var(f"n_etac_PT{nPT}_Prompt").getErrorLo())/2.
    n_etac_Fb_Err = (w.var(f"n_etac_PT{nPT}_FromB").getErrorHi()  - w.var(f"n_etac_PT{nPT}_FromB").getErrorLo())/2.


    NRel_Pr = (n_etac_Pr*effBB - n_etac_Fb*effBP) / (n_jpsi_Pr*effBB - n_jpsi_Fb*effBP)
    NRel_Fb = (n_etac_Fb*effPP - n_etac_Pr*effPB) / (n_jpsi_Fb*effPP - n_jpsi_Pr*effPB)

    # N_jpsi_true_Pr = (n_jpsi_Pr*effBB - n_jpsi_Fb*effBP) / (effPP*effBB - effPB*effBP)
    # N_jpsi_true_Fb = (n_jpsi_Fb*effPP - n_jpsi_Pr*effPB) / (effPP*effBB - effPB*effBP)

    # N_etac_true_Pr = (n_etac_Pr*effBB - n_etac_Fb*effBP) / (effPP*effBB - effPB*effBP)
    # N_etac_true_Fb = (n_etac_Fb*effPP - n_etac_Pr*effPB) / (effPP*effBB - effPB*effBP)

    dNRel_Pr = NRel_Pr * ( ((n_etac_Pr_Err*effBB)**2 + (n_etac_Fb_Err*effBP)**2)/(n_etac_Pr*effBB - n_etac_Fb*effBP)**2 + ((n_jpsi_Pr_Err*effBB)**2 + (n_jpsi_Fb_Err*effBP)**2)/(n_jpsi_Pr*effBB - n_jpsi_Fb*effBP)**2 )**0.5
    dNRel_Fb = NRel_Fb * ( ((n_etac_Fb_Err*effPP)**2 + (n_etac_Pr_Err*effPB)**2)/(n_etac_Fb*effPP - n_etac_Pr*effPB)**2 + ((n_jpsi_Fb_Err*effPP)**2 + (n_jpsi_Pr_Err*effPB)**2)/(n_jpsi_Fb*effPP - n_jpsi_Pr*effPB)**2 )**0.5

    nameTxt = f"{homeDir}/etac/CB/correctedYield_{bins}{nPT}_C{nConf}{CUTKEY}.txt"

    fo = open(nameTxt,"w")

    fo.write("n_etac_Prompt Relative = {:2.4f} +/- {:2.4f}, {:3.2f} \n".format( NRel_Pr, dNRel_Pr, NRel_Pr/dNRel_Pr ))
    fo.write("n_etac_FromB Relative = {:2.4f} +/- {:2.4f}, {:3.2f} \n".format( NRel_Fb, dNRel_Fb, NRel_Fb/dNRel_Fb ))

    N_etac_Prompt = RooRealVar("N_etac_Prompt","num of Etac", NRel_Pr); N_etac_Prompt.setError(dNRel_Pr)
    N_etac_FromB = RooRealVar("N_etac_FromB","num of Etac", NRel_Fb); N_etac_FromB.setError(dNRel_Fb)

    getattr(w,"import")(N_etac_Prompt)
    getattr(w,"import")(N_etac_FromB)

    fo.close()
    logging.info(f" Corrected yields are written to {nameTxt}")


def getData_h(w, keyPrompt, nPT=0, bins="PT", shift=False):

    variable = "Jpsi_"+bins
    dirName  = dataDir +"Data_Low/"

    nt  =  TChain("DecayTree")
    nt.Add(f"{dirName}/Etac2sDiProton_Low_2018*.root")

    cut_FD = ""
    if keyPrompt:
        cut_FD = TCut("(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08")# & Jpsi_FDCHI2_OWNPV > 49""
        histName = f"dh_PT{nPT}_Prompt"
    else:
        cut_FD = TCut("(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16")# & Jpsi_FDCHI2_OWNPV > 49""
        histName = f"dh_PT{nPT}_FromB"

    if nPT==0:
       cutPT = "{0}>{1} && {0}<{2}".format(variable,binningDict[variable][0],binningDict[variable][-1])
    else:
       cutPT = "{0}>{1} && {0}<{2}".format(variable,binningDict[variable][nPT-1],binningDict[variable][nPT])

    cut_PT = TCut(cutPT)

    totCut = TCut(cut_FD + cut_PT)

    hh = TH1D("hh","hh", 2000, minM_Low, maxM_Low)

    nt.Draw("Jpsi_m_scaled>>hh",totCut.GetTitle(),"goff")

    #Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M", minM_Low, maxM_Low)
    Jpsi_M = w.var("Jpsi_M")

    if(shift):
        hh.SetBins(276,2705,3395)
        Jpsi_M.setRange(2855,3245)

    dh = RooDataHist(histName,histName, RooArgList(Jpsi_M) ,hh)
    getattr(w,"import")(dh)

    dh.Print()
    logging.info(" RooDataHist is successfully created from a TTree")

def get_data_w(w, nPT, bins="PT", shift=False):

    tag = "PT0"
    if bins=="PT": 
        tag = f"PT{nPT}"

    nameWksp = f"{dataDir}/wksps/wksp_data_fromB_Low_{bins}{nPT}{CUTKEY}.root"
    file_w = TFile(nameWksp)
    w_temp = file_w.Get("w")

    histName = f"dh_{tag}_FromB"
    htemp = w_temp.data(histName)
    htemp.SetName(f"dh_PT{nPT}_FromB")
    logging.debug(f" RDH {histName}")
    htemp.Print()
    getattr(w,"import")(htemp)
    file_w.Close()

    nameWksp = f"{dataDir}/wksps/wksp_data_prompt_Low_{bins}{nPT}{CUTKEY}.root"
    file_w = TFile(nameWksp)
    w_temp = file_w.Get("w")

    histName = f"dh_{tag}_Prompt"
    htemp = w_temp.data(histName)
    htemp.SetName(f"dh_PT{nPT}_Prompt")
    logging.debug(f" RDH {histName}")
    htemp.Print()
    getattr(w,"import")(htemp)
    file_w.Close()

    logging.info(" RooDataHist is successfully loaded from RooWorkspace")

def plot(w:RooWorkspace, nPT:int, nConf:str, bins:str):

    Jpsi_M = w.var("Jpsi_M")

    keyPrompt = f"PT{nPT}_Prompt"
    keyFromB  = f"PT{nPT}_FromB"
    outDirName = f"{homeDir}/etac/CB"

    frames = []; pulls = []; resids = []


    if nConf=="Gauss":
        signal = RooArgSet(w.pdf(f"gauss_1_PT{nPT}"),
                           w.pdf(f"gauss_2_PT{nPT}"),
                           w.pdf(f"bwxg_1_PT{nPT}"),
                           w.pdf(f"bwxg_2_PT{nPT}"))
    else:
        signal = RooArgSet(w.pdf(f"cb_jpsi_1_PT{nPT}"),w.pdf(f"bwxg_1_PT{nPT}"))


    fullBkg_Prompt =  RooArgSet(w.pdf(f"bkg_{keyPrompt}"),w.pdf("pppi0"))
    fullBkg_FromB  =  RooArgSet(w.pdf(f"bkg_{keyFromB}"),w.pdf("pppi0"))

    frames0, resid0, pull0, chi2_Prompt = setupFrame(Jpsi_M, keyPrompt, w.data(f"dh_{keyPrompt}"), w.pdf(f"model_{keyPrompt}"), signal, fullBkg_Prompt)
    frames1, resid1, pull1, chi2_FromB = setupFrame(Jpsi_M, keyFromB, w.data(f"dh_{keyFromB}"), w.pdf(f"model_{keyFromB}"), signal, fullBkg_FromB)

    frames.append(frames0); frames.append(frames1)
    resids.append(resid0); resids.append(resid1)
    pulls.append(pull0);  pulls.append(pull1)

    nameTxt  = f"{outDirName}/Result_{bins}{nPT}_C{nConf}{CUTKEY}.txt"
    nameRoot = f"{outDirName}/Plot_{bins}{nPT}_C{nConf}{CUTKEY}.root"
    namePic  = f"{outDirName}/Plot_{bins}{nPT}_C{nConf}{CUTKEY}.pdf"

    names = ["prompt sample", "b-decays sample"]
    c = setupCanvas(names, frames, resids, pulls)
    c.SaveAs(namePic)
    # c.SaveAs(nameRoot)

    fo = open(nameTxt,"a")
    fo.write("$chi^2 prompt$ {:6.4f} \n".format(chi2_Prompt))
    fo.write("$chi^2 from-b$ {:6.4f} \n".format(chi2_FromB))
    fo.close()

    print("$chi^2 prompt$ {:6.4f} \n".format(chi2_Prompt))
    print("$chi^2 from-b$ {:6.4f} \n".format(chi2_FromB))
    input("Press Enter to continue...")


def fitData(nPT:int, nConf:str, bins:str="PT"):

    inCutKey = "RunIMethod"
    outDirName = f"{homeDir}/etac/CB"

    w = RooWorkspace("w",True)

    keyPrompt = f"PT{nPT}_Prompt"
    keyFromB  = f"PT{nPT}_FromB"

    fillRelWorkspace(w, nConf, nPT, keyPrompt)
    fillRelWorkspace(w, nConf, nPT, keyFromB)

    #getData_h(w, True,  nPT, 0)
    #getData_h(w, False, nPT, 0)
    get_data_w(w, nPT, bins, 0)

    model_Prompt = w.pdf(f"model_{keyPrompt}")
    model_FromB  = w.pdf(f"model_{keyFromB}")



    Jpsi_M = w.var("Jpsi_M")

    sample = RooCategory("sample","sample")
    sample.defineType(keyPrompt)
    sample.defineType(keyFromB)

    hist_Prompt = w.data(f"dh_{keyPrompt}")
    hist_FromB  = w.data(f"dh_{keyFromB}")

    # combData = RooDataHist("combData", "combined data", RooArgList(Jpsi_M), RooFit.Index(sample), RooFit.Import(keyFromB,hist_FromB))
    combData = RooDataHist("combData", "combined data", RooArgList(Jpsi_M), RooFit.Index(sample), RooFit.Import(keyPrompt,hist_Prompt), RooFit.Import(keyFromB,hist_FromB))


    simPdf = RooSimultaneous("simPdf","simultaneous signal pdf",sample)
    simPdf.addPdf(model_Prompt,keyPrompt)
    simPdf.addPdf(model_FromB,keyFromB)

    sigma = w.var("sigma_etac_1_PT{}".format(nPT))
    mass_jpsi = w.var("mass_jpsi")
    mass_res = w.var("mass_res")
    gamma_etac = w.var("gamma_etac")


    mass_jpsi.setConstant(True)
    mass_res.setConstant(True)
    sigma.setConstant(True)

    w.var(f"a2_{keyFromB}").setVal(0.)
#    w.var("a2_{}".format(keyFromB)).setConstant(True)

    llist = RooLinkedList()
    llist.Add(RooCmdArg(RooFit.Extended(True)))
    llist.Add(RooCmdArg(RooFit.Save(True)))
    llist.Add(RooCmdArg(RooFit.Offset(True)))
    llist.Add(RooCmdArg(RooFit.NumCPU(8)))


    simPdf.fitTo(combData, llist)
    simPdf.fitTo(combData, llist)

    llist.Remove(llist.find("NumCPU"))

    mass_jpsi.setConstant(False)
    mass_res.setConstant(False)
    sigma.setConstant(False)

    if nPT!=0:
        f_temp = TFile(f"{outDirName}/Wksp_PT{0}_C{nConf}{CUTKEY}.root","READ")
        w_temp = f_temp.Get("w")

        massJpsi_tot = w_temp.var("mass_jpsi").getValV(); massJpsi_totErr = w_temp.var("mass_jpsi").getError();
        massres_tot  = w_temp.var("mass_res").getValV();  massres_totErr =  w_temp.var("mass_res").getError();

        f_temp.Close()
    else:
        massJpsi_tot = 3096.57; massJpsi_totErr = 0.106
        massres_tot  = 112.62; massres_totErr =  0.83

    fconstrJpsi = RooGaussian("fconstrEtac","fconstrEtac",mass_jpsi, RooFit.RooConst(massJpsi_tot), RooFit.RooConst(massJpsi_totErr))  #PDG
    fconstrEtac = RooGaussian("fconstrJpsi","fconstrJpsi",mass_res, RooFit.RooConst(massres_tot),RooFit.RooConst(massres_totErr))
    constraints = RooFit.ExternalConstraints(RooArgSet(fconstrEtac,fconstrJpsi))



    if(nPT!=0):
        llist.Add(RooCmdArg(constraints))
        simPdf.fitTo(combData, llist)
        simPdf.fitTo(combData, llist)
        # llist.Add(RooCmdArg(RooFit.Minos(True)))
        # llist.Add(RooCmdArg(RooFit.Strategy(2)))
        simPdf.fitTo(combData, llist)
        simPdf.fitTo(combData, llist)
        simPdf.fitTo(combData, llist)
        r = simPdf.fitTo(combData, llist)

        # simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
        # simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
        # simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Strategy(2))
        # simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Save(True), RooFit.Strategy(1))
        # # simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Save(True), RooFit.Strategy(1), RooFit.NumCPU(8),constraints)
        # r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Save(True), RooFit.Strategy(1))

# # BEGIN 1: to check masses from individual pt-fits::
#         simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
#         simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.NumCPU(8))
#         simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Strategy(1), RooFit.NumCPU(8))
#         simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Strategy(1), RooFit.NumCPU(8))
#         simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(False), RooFit.Save(True), RooFit.Strategy(1), RooFit.NumCPU(8))
#         r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(False), RooFit.Save(True), RooFit.Strategy(1), RooFit.NumCPU(8))
# # END 1
    else:
        simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
        simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
        # simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Strategy(1), RooFit.NumCPU(8, 3))
        # simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Strategy(2), RooFit.NumCPU(8, 3))
        #simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Strategy(1), RooFit.NumCPU(8))
        # simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Strategy(1), RooFit.NumCPU(8))
        # simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Strategy(2), RooFit.NumCPU(8))
        # simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Strategy(2), RooFit.NumCPU(8))
        # r = simPdf.fitTo(combData, RooFit.Minos(True), RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Strategy(2))
        # r = simPdf.fitTo(combData, RooFit.Minos(True), RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
        r = simPdf.fitTo(combData, RooFit.Minos(True), RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
        r = simPdf.fitTo(combData, RooFit.Minos(True), RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))


    correctedYield(w, nPT, nConf, bins)

    nameTxt     = f"{outDirName}/Result_{bins}{nPT}_C{nConf}{CUTKEY}.txt"
    nameWksp    = f"{outDirName}/Wksp_{bins}{nPT}_C{nConf}{CUTKEY}.root"

    import os, sys
    save = os.dup( sys.stdout.fileno() )
    newout = open(nameTxt, "w" )
    os.dup2( newout.fileno(), sys.stdout.fileno() )
    r.Print("v")
    r.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() )
    newout.close()

    w.writeToFile(nameWksp)

    plot(w, nPT, nConf, bins)

    r.correlationMatrix().Print("v")
    r.globalCorr().Print("v")

    return w


if __name__ == "__main__":

    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
    # nConfs = ["Base","CB","Chebychev3par","eff_pppi0","Gamma34","effPP","effPB","effBB","effBP","rEtaToJpsi"]
    nConfs = ["Base","Gauss","Chebychev3par","eff_pppi0","Gamma","GammaL","effPP","effPB","effBB","effBP","rEtaToJpsi"]
    # nConfs = ["effPP","effPB","effBB","effBP"]
    # nConfs = ["eff_pppi0_x2"]
    nConfs = ["Base"]
    nConfs = ["Chebychev3par"]
    bins = "PT"
    bins = "Y"
    # for iPT in range (0,1):
    for iPT in range (1,5):
        for iConf in nConfs:
            # w = fitData(iPT, iConf, bins)
            nameWksp    = f"{homeDir}/etac/CB/Wksp_{bins}{iPT}_C{iConf}{CUTKEY}.root"
            file_w = TFile(nameWksp)
            w = file_w.Get("w").Clone()
            file_w.Close()
            plot(w, iPT, iConf, bins)
            # correctedYield(w, iPT, iConf, bins)


        # effPP, effPB, effBP, effBB, effPP_err, effPB_err, effBP_err, effBB_err = promptImp(iPT)
        # print ("effPP = ",effPP," +- ",effPP_err)
        # print ("effBB = ",effBB," +- ",effBB_err)
        # print ("effPB = ",effPB," +- ",effPB_err)
        # print ("effBP = ",effBP," +- ",effBP_err)
