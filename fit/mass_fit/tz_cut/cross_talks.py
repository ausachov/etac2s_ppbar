from ROOT import gROOT, TCanvas, TH1F
# gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
# gROOT.LoadMacro("../libs/libBifurcatedCB/BifurcatedCB.cxx+")


# from setupPlot import *
# from setupFitModel_RunI import *
from mass_fit_runI import promptImp

# homeDir = '/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/mass_fit/'
homeDir = '/sps/lhcb/zhovkovska/etac2s_ppbar/results/mass_fit/'
dataDir = '/sps/lhcb/zhovkovska/etac2s_ppbar/'
localDir = "/users/LHCb/zhovkovska/etac2s_ppbar/"

binWidth = 0.2
binWidthDraw = 0.2

minM_Low = 2850
maxM_Low = 3250
binN_Low = int((maxM_Low-minM_Low)/binWidth)

minM_High = 3320
maxM_High = 3780
binN_High = int((maxM_High-minM_High)/binWidth)

from array import array
nPTBins = 6
ptBinning = array("d", [5.0, 6.5, 8.0, 10.0, 12.0, 14.0, 20.0])

#import sys

#orig_stdout = sys.stdout
#f = open('eff_file.txt', 'w')
#sys.stdout = f

#for iPT in range(0, 7):
    #effPP, effPB, effBP, effBB, effPP_err, effPB_err, effBP_err, effBB_err = promptImp(iPT)
    #print(iPT, effPP, effPB, effBP, effBB, effPP_err, effPB_err, effBP_err, effBB_err)

#sys.stdout = orig_stdout
#f.close()

def createHisto(hName = "", vals=[], errors=[]):
    nBins = len(vals)
    hist = TH1F(hName,hName,nBins,ptBinning[0:nBins+1])
    if nBins==1:
        binning = array("d", [ptBinning[0],ptBinning[-1]])
        hist = TH1F(hName,hName,nBins,binning)

    for ii in range(nBins):
        hist.SetBinContent(ii+1, vals[ii])
        hist.SetBinError(ii+1, errors[ii])
    return hist


#f = open('eff_file.txt', 'r')
def get_eff():

    effPP_list_0 = [0.]
    effPB_list_0 = [0.]
    effBP_list_0 = [0.]
    effBB_list_0 = [0.]

    effPP_list_err_0 = [0.]
    effPB_list_err_0 = [0.]
    effBP_list_err_0 = [0.]
    effBB_list_err_0 = [0.]

    effPP_list = []
    effPB_list = []
    effBP_list = []
    effBB_list = []

    effPP_list_err = []
    effPB_list_err = []
    effBP_list_err = []
    effBB_list_err = []


    effPP_list_0[0], effPB_list_0[0], effBP_list_0[0], effBB_list_0[0], \
    effPP_list_err_0[0], effPB_list_err_0[0], effBP_list_err_0[0], effBB_list_err_0[0] = promptImp(0)

    print(effPP_list_0[0], effPB_list_0[0], effBP_list_0[0], effBB_list_0[0])
    print(effPP_list_err_0[0], effPB_list_err_0[0], effBP_list_err_0[0], effBB_list_err_0[0])
    h_PP_0 = createHisto("PP_0",effPP_list_0,effPP_list_err_0)
    h_PB_0 = createHisto("PB_0",effPB_list_0,effPB_list_err_0)
    h_BP_0 = createHisto("BP_0",effBP_list_0,effBP_list_err_0)
    h_BB_0 = createHisto("BB_0",effBB_list_0,effBB_list_err_0)


    for iPT in range(1, nPTBins+1):
        effPP, effPB, effBP, effBB, effPP_err, effPB_err, effBP_err, effBB_err = promptImp(iPT)
        effPP_list.append(effPP)
        effPB_list.append(effPB)
        effBP_list.append(effBP)
        effBB_list.append(effBB)

        effPP_list_err.append(effPP_err)
        effPB_list_err.append(effPB_err)
        effBP_list_err.append(effBP_err)
        effBB_list_err.append(effBB_err)

    h_PP = createHisto("PP",effPP_list,effPP_list_err)
    h_PB = createHisto("PB",effPB_list,effPB_list_err)
    h_BP = createHisto("BP",effBP_list,effBP_list_err)
    h_BB = createHisto("BB",effBB_list,effBB_list_err)


    c = TCanvas("c1","c1", 1000, 700)
    c.Divide(2, 2, 0.001, 0.001)
    c.cd(1)
    h_PP_0.SetFillColor(2)
    h_PP_0.SetFillStyle(3002)
    h_PP_0.SetMaximum(1)
    # h_PP_0.SetMinimum(0.8)
    h_PP_0.SetMinimum(0.0)
    h_PP_0.Draw("E2")
    h_PP.Draw("E1 SAME")
    # c.SaveAs("h_PP.pdf")

    c.cd(2)
    h_PB_0.SetFillColor(2)
    h_PB_0.SetFillStyle(3002)
    # h_PB_0.SetMaximum(0.015)
    h_PB_0.SetMaximum(0.005)
    h_PB_0.SetMinimum(0)
    h_PB_0.Draw("E2")
    h_PB.Draw("E1 SAME")
    # c.SaveAs("h_PB.pdf")

    c.cd(3)
    h_BP_0.SetFillColor(2)
    h_BP_0.SetFillStyle(3002)
    h_BP_0.SetMaximum(0.1)
    h_BP_0.SetMinimum(0)
    h_BP_0.Draw("E2")
    h_BP.Draw("E1 SAME")
    # c.SaveAs("h_BP.pdf")

    c.cd(4)
    h_BB_0.SetFillColor(2)
    h_BB_0.SetFillStyle(3002)
    h_BB_0.SetMaximum(0.8)
    # h_BB_0.SetMinimum(0.6)
    h_BB_0.SetMinimum(0.0)
    h_BB_0.Draw("E2")
    h_BB.Draw("E1 SAME")
    # c.SaveAs("h_BB.pdf")
    c.SaveAs("h_ct_oldPID.pdf")

if __name__ == "__main__":

    gROOT.LoadMacro(f"{localDir}/fit/libs/lhcbStyle.C")
    get_eff()