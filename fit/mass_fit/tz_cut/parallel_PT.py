from multiprocessing import Pool
from functools import partial
from contextlib import closing
from mass_fit_runI import fitData as fitData
# from mass_fit_sim import fitData as fitDataSim
# from mass_fit_runI_y import fitData

# gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
# gROOT.LoadMacro("mass_fit_2D.py");

def f(list):
    iPT = list[0]
    iConf = list[1]
    bins = list[2]
    return fitData(iPT, iConf, bins)

# def fSim(list):
#     iPT = list[0]
#     iConf = list[1]
#     bins = list[2]
#     return fitDataSim(6, iConf, bins)

PT = [i for i in range(1, 7)]
# PT = [i for i in range(1, 5)]
# PT = [0]
# Confs = ["Base","CB","Chebychev3par","eff_pppi0","Gamma34","effPP","effPB","effBB","effBP","rEtaToJpsi"]
# Confs = ["Base","Gauss","Chebychev3par","eff_pppi0","Gamma","effPP","effPB","effBB","effBP","rEtaToJpsi"]
Confs = ["Base","Gauss","Chebychev3par","eff_pppi0","Gamma","GammaL","effPP","effPB","effBB","effBP","rEtaToJpsi"]
# Confs = ["Gauss","Chebychev3par","eff_pppi0","Gamma","GammaL","effPP","effPB","effBB","effBP","rEtaToJpsi"]
# Confs = ["eff_pppi0","GammaL"]
# Confs = ["eff_pppi0_x2"]
Bins = ["PT"]
# Bins = ["Y"]
a = [[PT[iPT], Confs[iConf], Bins[iB]] for iPT in range(len(PT)) for iConf in range(len(Confs)) for iB in range(len(Bins))]
print(a)

if __name__ == '__main__':

    nProc = 24
    nProc = 10
    with closing(Pool(nProc)) as p:
        (p.map(f, a))
        # (p.map(fSim, a))
        p.terminate()
