from ROOT import TFile, gROOT
from array import array
import logging
import math

homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"
localDir = "/users/LHCb/zhovkovska/etac2s_ppbar/"

gROOT.LoadMacro(f"{localDir}/fit/libs/libBifurcatedCB/BifurcatedCB.cxx+")

MODEL = "CB"
UNITS = {
    "PT" : ["\\pt","\\gevc"],
    "Y"  : ["\\y", ""]
}
NB = { 
    "PT" : 6,
    # "PT" : 5,
    # "PT" : 4,
    "Y"  : 4,
    "TOT": 1    
}
BINNING = { 
    "PT" : array("d", [5.0, 6.5, 8.0, 10.0, 12.0, 14.0, 20.0]),
    # "PT" : array("d", [5.0, 6.5, 8.0, 10.0, 12.0, 14.0]),
    # "PT" : array("d", [6.5, 8.0, 10.0, 12.0, 14.0]),
    "Y"  : array("d", [2.0, 2.5, 3.0, 3.5, 4.0]),
    "TOT": array("d",[0.0, 1.0])
}

parNames = {
    # "mean"      : "Gaussian mean [\\mevcc~]",
    # "alpha_1"   : "$\\alpha_{1}$",
    # "alpha_2"   : "$\\alpha_{2}$",
    # "nCB_1"     : "$n_{CB,1}$",
    # "nCB_2"     : "$n_{CB,2}$",
    # "r_G1ToG2"  : "$f_{n}$",
    # "r_NToW"    : "$\\sigma_{n}/\\sigma_{w}$",
    # "r_ref_etac"    : "$\\sigma_{n,\\etacones}/\\sigma_{n,\\jpsi}$",
    # "r_ref_etac2S"  : "$\\sigma_{n,\\etactwos}/\\sigma_{n,\\jpsi}$",
    # "r_ref_psi2S"   : "$\\sigma_{n,\\psitwos}/\\sigma_{n,\\jpsi}$",
    # "r_ref_chic0"   : "$\\sigma_{n,\\chiczero}/\\sigma_{n,\\jpsi}$",
    # "r_ref_chic1"   : "$\\sigma_{n,\\chicone}/\\sigma_{n,\\jpsi}$",
    # "r_ref_chic2"   : "$\\sigma_{n,\\chictwo}/\\sigma_{n,\\jpsi}$",
    # "r_ref_jpsi": "$\\sigma_{n,\\jpsi}/\\sigma_{n,\\etacones}$",
          "a0_PT_FromB": "$a_{0}^{from-b}$",
         "a0_PT_Prompt": "$a_{0}^{prompt}$",
          "a1_PT_FromB": "$a_{1}^{from-b}$",
         "a1_PT_Prompt": "$a_{1}^{prompt}$",
          "a2_PT_FromB": "$a_{2}^{from-b}$",
         "a2_PT_Prompt": "$a_{2}^{prompt}$",
            "mass_jpsi": "$M_{\\jpsi}$",
             "mass_res": "$M_{\\jpsi}-M_{\\etac}$",
      "n_bkgr_PT_FromB": "$n_{bkg}^{from-b}$",
     "n_bkgr_PT_Prompt": "$n_{bkg}^{prompt}$",
      "n_etac_PT_FromB": "$n_{\\etac}^{from-b}$",
     "n_etac_PT_Prompt": "$n_{\\etac}^{prompt}$",
      "n_jpsi_PT_FromB": "$n_{\\jpsi}^{from-b}$",
     "n_jpsi_PT_Prompt": "$n_{\\jpsi}^{prompt}$",
      "sigma_etac_1_PT": "$\\sigma_{\\etac}$",

}

parScales = {
          "a0_PT_FromB": 1e-1,
         "a0_PT_Prompt": 1e-1,
          "a1_PT_FromB": 1e-1,
         "a1_PT_Prompt": 1e-2,
          "a2_PT_FromB": 1e-2,
         "a2_PT_Prompt": 1e-2,
            "mass_jpsi": 1.,
             "mass_res": 1.,
      "n_bkgr_PT_FromB": 1e4,
     "n_bkgr_PT_Prompt": 1e7,
      "n_etac_PT_FromB": 1e3,
     "n_etac_PT_Prompt": 1e4,
      "n_jpsi_PT_FromB": 1e3,
     "n_jpsi_PT_Prompt": 1e4,
      "sigma_etac_1_PT": 1.,

}


def print_table(vals):

    print("\\begin{tabular}{",end="")
    for i in range(len(vals)):
        print("c|",end="")
    print("c}")
    print("\\hline \\hline")
    for src, params in vals.items():
        print(f"& {src}",end=" ")
    print("\\\\ \\hline \\hline")
    for par, names in parNames.items():
        print(f"{names:30s}", end=" ")
        for src, params in vals.items():
            if par in params.keys():
                print(f"& {params[par]}",end=" ")
        print("\\\\")
    print("\\hline")
    print("\\end{tabular}")

def bkg_params(bins="PT", srcs=["Prompt", "FromB"], npar=3):
    return lambda b: [f"a{x}_{bins}{b}_{src}" for x in range(npar) for src in srcs]

def gen_params(bins="PT", srcs=["Prompt", "FromB"], npar=3):
    '''Generate list of parameter names for the given binning and sources'''
    p_list  = [f"a{x}_{bins}_{src}" for x in range(npar) for src in srcs]
    p_list += [f"n_{par}_{bins}_{src}" for par in ["bkgr", "etac", "jpsi"] for src in srcs]
    p_list += [f"sigma_etac_1_{bins}"]
    return p_list

def bin_param(p_name:str, iB:int, bins:str):
    # logging.debug(f"Old par name: {p_name}")
    # if iB==0 and bins!="PT":
    #     p_name = p_name.replace(bins, "PT")
    #     bins="PT"
    #     logging.debug(f"New par name: {p_name}")
    idx = p_name.rfind(bins)+len(bins)
    if idx>len(bins):
        return f'{p_name[:idx]}{iB}{p_name[idx:]}'
    else:
        return p_name

def get_values(par_list:list, bins:str="PT", cName:str="Base", inCutKey:str="ProbNNp_06"):
    '''Get values of parameters from the workspace and prints them in a table'''
    dict_val = {}
    n_jpsi_val = 0.
    n_jpsi_err = 0.
    # if kCB: modName="_CB"
    # else:   modName=""

    # print(f"{'source':20s} & r_G1ToG2 & r_NToW",end=" ")
    # for name in name_list[1:]:
    #     print(f'& r_ref_{name}',end=" ")
    # print("\\\\ \\hline\\hline")
    for iB in range(NB[bins]+1):
        dict_val[iB] = {}
        if iB==0:
            bbins="PT"
        else:
            bbins=bins
        nameWksp = f"{homeDir}/mass_fit/etac/{MODEL}/Wksp_{bbins}{iB}_C{cName}_{inCutKey}.root"
        f = TFile(nameWksp, "READ")
        w = f.Get("w").Clone()
        f.Close()
        # bkg_list = bkg_params(bins=bins)
        # prec = 2
        # for bag_par in bkg_list(iB):
        #     logging.debug(f"Bkg Par name: {bag_par}")
        #     dict_val[iB][bag_par] = f"{round(w.var(bag_par).getVal(),prec):.2f} $\pm$ {round(w.var(bag_par).getError(),prec):.2f}"
        prec = 3
        if iB>0:
            n_jpsi_val+=w.var(f"n_etac_PT{iB}_Prompt").getVal()
            n_jpsi_err+=w.var(f"n_etac_PT{iB}_Prompt").getError()**2
        else:
            n_jpsi_val=w.var(f"n_etac_PT{iB}_Prompt").getVal()
            n_jpsi_err=w.var(f"n_etac_PT{iB}_Prompt").getError()
            print(f"Fit: J/psi yeild in all bins is {n_jpsi_val:.1f}+/-{n_jpsi_err:.1f}")
            n_jpsi_val=0.
            n_jpsi_err=0.

        for p_name in par_list:
        # print(f"{src}", end=" ")
            par_name = bin_param(p_name, iB, "PT")
            logging.debug(f"All Par name: {par_name}")
            if "n_" in p_name:
            # if "_PT_" in p_name:
                # points = math.log(w.var(par_name).getVal()/w.var(par_name).getError(), 10)
                # sc = 10**points
                # dict_val[iB][p_name] = f"{round(w.var(par_name).getVal()/parScales[p_name],prec):.3e} $\pm$ {round(w.var(par_name).getError()/parScales[p_name],prec):.3e}"
                dict_val[iB][p_name] = f"{round(w.var(par_name).getVal()/parScales[p_name],prec):.2f} $\pm$ {round(w.var(par_name).getError()/parScales[p_name],prec):.2f}"
            # elif "n_" in p_name:
                # dict_val[iB][p_name] = f"{round(w.var(par_name).getVal(),prec):.0f} $\pm$ {round(w.var(par_name).getError(),prec):.0f}"
            else:
                dict_val[iB][p_name] = f"{round(w.var(par_name).getVal()/parScales[p_name],prec):.2f} $\pm$ {round(w.var(par_name).getError()/parScales[p_name],prec):.2f}"
        # if kCB:
        #     dict_val[src]["alpha_1"] = f"{round(w.var('alpha_1').getVal(),prec)} $\pm$ {round(w.var('alpha_1').getError(),prec)}"
        #     dict_val[src]["alpha_2"] = f"{round(w.var('alpha_2').getVal(),prec)} $\pm$ {round(w.var('alpha_2').getError(),prec)}"
        #     dict_val[src]["nCB_1"] = f"{round(w.var('nCB_1').getVal(),prec)} $\pm$ {round(w.var('nCB_1').getError(),prec)}"
        #     dict_val[src]["nCB_2"] = f"{round(w.var('nCB_2').getVal(),prec)} $\pm$ {round(w.var('nCB_2').getError(),prec)}"
        # else:
        #     dict_val[src]["r_NToW"] = f"{round(w.var('r_NToW').getVal(),prec)} $\pm$ {round(w.var('r_NToW').getError(),prec)}"
        #     dict_val[src]["r_G1ToG2"] = f"{round(w.var('r_G1ToG2').getVal(),prec)} $\pm$ {round(w.var('r_G1ToG2').getError(),prec)}"
        # # print(f"& {w.var('r_G1ToG2').getVal():.3f} \pm {w.var('r_G1ToG2').getError():.3f}", end=" ")
        # # print(f"& {w.var('r_NToW').getVal():.3f} \pm {w.var('r_NToW').getError():.3f}", end=" ")
        # for name in name_list[1:]:
        #     dict_val[src][f"r_ref_{name}"] = f"{round(w.var(f'r_ref_{name}').getVal(),prec)} $\pm$ {round(w.var(f'r_ref_{name}').getError(),prec)}"
            # print(f"& {w.var(f'r_ref_{name}').getVal():.3f} \pm {w.var(f'r_ref_{name}').getError():.3f}", end=" ")
        # print("\\\\")

    print_table(dict_val)
    n_jpsi_err = n_jpsi_err**0.5
    print(f"Sum: J/psi yeild in all bins is {n_jpsi_val:.1f}+/-{n_jpsi_err:.1f}")

def print_pics(bins:str="PT", nB:int=0, inCutKey:str="ProbNNp_06"):
    '''A function to prind Latex form for all pictures'''

    binRange = f"${BINNING[bins][nB-1]} < {UNITS[bins][0]} < {BINNING[bins][nB]}{UNITS[bins][1]}$"
    if nB==0:
        binRange = f"${BINNING[bins][0]} < {UNITS[bins][0]} < {BINNING[bins][-1]}{UNITS[bins][1]}$"
    
    caption = "{The $M_{\\ppbar}$ distribution for prompt (left) and b-decays (right) for "+binRange
    caption += " with old PID cuts. The solid blue lines represent the total fit result. Magenta and green lines show the signal and background components, respectively. The corresponding residual and pull distributions are shown below.}"
    
    print("\\begin{figure}[ht]")
    print("\\centering")
    print("\\protect\\includegraphics[width=0.90\\linewidth]{figs/etac/"+f"Plot_{bins}{nB}_CBase_{inCutKey}.pdf"+"}")
    print(f"\\caption{caption}")
    print("\\label{fig:M_"+f"{bins}{nB}"+"}")
    print("\\end{figure}")
    print("")

if __name__ == "__main__":
    '''
    Prints a table comparing mass fit parameters in bins
    '''
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)
    
    bb = "Y"
    param_list = gen_params("PT")
    param_list += [ "mass_jpsi", "mass_res" ]
    logging.info(f"Parameters: {param_list}")
    # param_list = [ "a0_PT_"
    #     "mass_jpsi", "mass_res", "sigma_etac_1_PT", \
    #     "n_bkgr_PT_FromB", "n_bkgr_PT_Prompt", "n_etac_PT_FromB", "n_etac_PT_Prompt", "n_jpsi_PT_FromB", "n_jpsi_PT_Prompt"]
    inCut="ProbNNp_06"
    # inCut="oldPID"
    syst_list = ["Base","Gauss","Chebychev3par","eff_pppi0","Gamma","GammaL","effPP","effPB","effBB","effBP","rEtaToJpsi"]
    syst_list = ["Gauss","Chebychev3par","eff_pppi0","Gamma","GammaL","rEtaToJpsi"]
    # syst_list = ["Base"]

    for syst in syst_list:
        logging.info(f"Systematic: {syst}...........................")
        get_values(param_list, bins=bb, cName=syst, inCutKey=inCut)
    # for i in range(7):
        # print_pics(bb, i, inCut)
