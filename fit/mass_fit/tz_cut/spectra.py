from ROOT import gROOT, TChain, TFile, TH1F, TH2F, TCanvas, TLegend
from array import array
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/MC/Trigger/"
localDir = "/users/LHCb/zhovkovska/etac2s_ppbar/"

dictBins = {
    "PT":   array('f',[5000., 6500., 8000., 10000., 12000., 14000., 20000.]),
    "Y":    array('f',[2., 2.5, 3., 3.5, 4.])
}

def compare(bins="PT"):

    TypePrefix = f"{bins}_RunIMethod_CB"
    nt = TChain("DecayTree")
    nt.Add(f"{dataDir}/etacDiProton_prompt_2018_AddBr.root")

    cut_ProbNN  = "ProtonP_ProbNNp > 0.6 && ProtonM_ProbNNp > 0.6"
    cut_Trigger = "Jpsi_L0HadronDecision_TOS && (Jpsi_Hlt1DiProtonDecision_TOS || Jpsi_Hlt1DiProtonHighDecision_TOS)"

    nt_etac_MC = nt.CopyTree(f"{cut_ProbNN} && {cut_Trigger}")

    nt = TChain("DecayTree")

    nt.Add(f"{dataDir}/jpsiDiProton_prompt_2018_AddBr.root")
    nt_jpsi_MC = nt.CopyTree(f"{cut_ProbNN} && {cut_Trigger}")

    f = TFile(f"{homeDir}/CS/NRel_{TypePrefix}.root","READ")
    hh_pr = f.Get("hn_pr").Clone()
    hh_pr.SetDirectory(0)
    f.Close()

    hh_pr_MC = TH1F()
    hh_pr_data = TH1F()
    hh_pr.Copy(hh_pr_MC)   
    hh_pr.Copy(hh_pr_data)   

    factor = 1000.  if bins=="PT" else 1

    for i in range(1, hh_pr_data.GetNbinsX()+1):
        n_jpsi = nt_jpsi_MC.GetEntries(f"Jpsi_{bins}>{factor*hh_pr_data.GetBinLowEdge(i)} && Jpsi_{bins}<{factor*(hh_pr_data.GetBinLowEdge(i)+hh_pr_data.GetBinWidth(i))}")
        n_etac = nt_etac_MC.GetEntries(f"Jpsi_{bins}>{factor*hh_pr_data.GetBinLowEdge(i)} && Jpsi_{bins}<{factor*(hh_pr_data.GetBinLowEdge(i)+hh_pr_data.GetBinWidth(i))}")
        hh_pr_data.SetBinContent(i, n_jpsi*hh_pr.GetBinContent(i))
        hh_pr_data.SetBinError(i, hh_pr_data.GetBinContent(i)*(1/n_jpsi**2 + (hh_pr.GetBinError(i)/hh_pr.GetBinContent(i))**2)**0.5)
        hh_pr_MC.SetBinContent(i, n_etac)

    # # nt_etac_MC.Draw("Jpsi_PT>>hh_pr_MC")
    hh_pr_MC.Scale(1/hh_pr_MC.Integral()) 
    hh_pr_data.Scale(1/hh_pr_data.Integral(),"nosw2") 


    l = TLegend(0.8, 0.7, 0.9, 0.9)
    l.AddEntry(hh_pr_data,"Data","lep")
    l.AddEntry(hh_pr_MC,"MC","lep")

    canv = TCanvas("canv", "canv", 55,55,550,400)
    pad = canv.cd(1)
    pad.SetLeftMargin(0.2)
    hh_pr_MC.SetLineColor(2)
    hh_pr_MC.SetMarkerSize(1)
    hh_pr_data.SetMarkerSize(1)
    hh_pr_data.GetYaxis().SetTitle("N^{p_{T}}_{\eta_{c}}/N_{\eta_{c}}")
    # hh_pr_data.Divide(hh_pr_MC)
    hh_pr_data.Draw("E1")
    hh_pr_MC.Draw("E1 same")
    l.Draw()
    canv.SaveAs(f"compare{bins}.pdf")

    # h_new = hh_pr_data.Clone()
    # h_new.Sumw2(True)
    # h_new.Divide(hh_pr_data, hh_pr_MC)
    hh_pr_data.Divide(hh_pr_MC)
    return hh_pr_data

def reweight():

    # gROOT.Reset()
    # gROOT.SetStyle("Plain")

    nt = TChain("DecayTree")
    nt.Add(f"{dataDir}/etacDiProton_prompt_2018_AddBr.root")

    # cut_ProbNN  = "ProtonP_ProbNNp > 0.6 && ProtonM_ProbNNp > 0.6"
    # cut_Trigger = "Jpsi_L0HadronDecision_TOS && (Jpsi_Hlt1DiProtonDecision_TOS || Jpsi_Hlt1DiProtonHighDecision_TOS)"

    hh_pr_Y  = compare("Y")
    hh_pr_PT = compare("PT")

    # hh_pr_Y = TH1F()
    # f = TFile(f"{homeDir}/CS/NRel_Y_RunIMethod_CB.root","READ")
    # hh_pr_Y  = f.Get("hn_pr").Clone("hn_pr_Y")
    # hh_pr_Y.SetDirectory(0)
    # hh_pr_Y.Scale(1/hh_pr_Y.Integral(),"nosw2") 
    # f.Close()
    # f = TFile(f"{homeDir}/CS/NRel_PT_RunIMethod_CB.root","READ")
    # hh_pr_PT = f.Get("hn_pr").Clone("hn_pr_PT")
    # hh_pr_PT.SetDirectory(0)
    # hh_pr_PT.Scale(1/hh_pr_PT.Integral(),"nosw2") 
    # f.Close()
    # nt_etac_MC = nt.CopyTree(f"{cut_ProbNN} && {cut_Trigger}")
    
    # nt_etac_MC.Branch()


    # weight_PT = array( "d", [0.])
    # weight_Y  = array( "d", [0.])

    # f_new = TFile(f"{dataDir}/etacDiProton_prompt_2018_AddBr_w.root", "RECREATE")

    # tt = nt.CloneTree(0)
    # tt.Branch("weight_PT",weight_PT,"weight_PT/D")
    # tt.Branch("weight_Y",weight_Y,"weight_Y/D")
    
    # a=0
    # for i in nt:
    #     if nt.ProtonP_ProbNNp>0.6 and nt.ProtonM_ProbNNp>0.6 \
    #         and nt.Jpsi_L0HadronDecision_TOS and nt.Jpsi_Hlt1DiProtonDecision_TOS \
    #         and (nt.Jpsi_PT>5000. and nt.Jpsi_PT<20000.) \
    #         and (nt.Jpsi_Y>2. and nt.Jpsi_Y<4.):
    #         a+=1
    #         nPT =  hh_pr_PT.FindBin(nt.Jpsi_PT/1000.)
    #         weight_PT[0] = hh_pr_PT.GetBinContent(nPT)
    #         # if (a%100==0): print(nPT, weight_PT[0],nt.Jpsi_PT)
    #         if nPT!=3 and weight_PT[0]< 1.4 and  weight_PT[0]>1.1:
    #             print(nPT, weight_PT[0],nt.Jpsi_PT)
    #         nY =  hh_pr_Y.FindBin(nt.Jpsi_Y)
    #         weight_Y[0] = hh_pr_Y.GetBinContent(nY)
    #         # print(weight_Y[0])
    #         tt.Fill()

    # # tt.Print()
    # tt.Write()
    # f_new.Close()

    f_new = TFile(f"{dataDir}/etacDiProton_prompt_2018_AddBr_w.root", "READ")
    tt = f_new.Get("DecayTree")

    # bins = "PT"
    # binsOp = "Y"
    bins = "Y"
    binsOp = "PT"
    varBins = dictBins[bins]


    print(hh_pr_PT.GetBinLowEdge(3),hh_pr_PT.GetBinWidth(3))
    print(hh_pr_Y.GetBinLowEdge(3),hh_pr_PT.GetBinWidth(3))
    histJpsiBins = TH1F("histJpsiBins", "histJpsiBins", 50, varBins[0], varBins[-1])
    # tt.Project("histJpsiBins", f"Jpsi_{bins}", f"Jpsi_{binsOp}")
    # tt.Project("histJpsiBins", f"Jpsi_{bins}", "1")
    tt.Draw(f"Jpsi_{bins}>>histJpsiBins", "")
    print(histJpsiBins.Integral())
    histJpsiBinsW = TH1F("histJpsiBinsW", "histJpsiBinsW", 50, varBins[0], varBins[-1])
    # tt.Project("histJpsiBinsW", f"Jpsi_{bins}", f"weight_{binsOp}")
    tt.Draw(f"Jpsi_{bins}>>histJpsiBinsW", f"weight_{binsOp}","same")
    print(histJpsiBinsW.Integral())

    # canv = TCanvas("canv", "canv", 1000, 800)
    # canv.cd(1)
    # hist2D = TH2F("hist2D", "hist2D", 100, dictBins["PT"][0], dictBins["PT"][-1], 100, dictBins["Y"][0], dictBins["Y"][-1])
    # tt.Draw("Jpsi_Y:Jpsi_PT>>hist2D","","colz")
    # canv.SaveAs("PTvsY.pdf")
# #====================


#     histJpsiMC = TH1F("histJpsiMC", "Invariant mass of p#bar{p}", binN, minMass, maxMass)
#     ntJpsiMC.Project("histJpsiMC", "Jpsi_MM", "")

#     histJpsiBins = TH1F("histJpsiBins", "histJpsiBins", 100, 0, varBins[-1])
#     ntJpsiMC.Project("histJpsiBins", "Jpsi_{}".format(bins), "")
#     histJpsiBinsW = TH1F("histJpsiBinsW", "histJpsiBinsW", 100, 0, varBins[-1])
#     ntJpsiMC.Project("histJpsiBinsW", "Jpsi_{}".format(bins), cutReweight)

    canvA = TCanvas("canvA", "canvA", 1000, 800)

    # canvA.Divide(1,2)
    canvA.cd(1)
    # histJpsiBinsW.GetXaxis().SetTitle("p_{T} [MeV/c]")
    histJpsiBinsW.GetXaxis().SetTitle("y")
    histJpsiBinsW.GetYaxis().SetTitle("MC Candidates")
    histJpsiBinsW.SetLineColor(2)
    histJpsiBinsW.Draw()
    histJpsiBins.Draw("SAME")
    canvA.SaveAs(f"reweight_MC_{bins}.pdf")
    f_new.Close()

#     histEff = TH1F("histEff","histEff", nBins, varBins)

#     for iBin in range(nBins):
#         #NW = ntJpsiMC.GetEntries(cutReweight+" && Jpsi_PT>{} && Jpsi_PT<{}".format(varBins[iBin], varBins[iBin+1]))
#         #N = ntJpsiMC.GetEntries("Jpsi_PT>{} && Jpsi_PT<{}".format(varBins[iBin], varBins[iBin+1]))
#         NW = histJpsiBinsW.Integral(histJpsiBinsW.FindBin(varBins[iBin]), histJpsiBinsW.FindBin(varBins[iBin+1]))
#         N = histJpsiBins.Integral(histJpsiBinsW.FindBin(varBins[iBin]), histJpsiBins.FindBin(varBins[iBin+1]))
#         print(varBins[iBin], N, NW)
#         histEff.SetBinContent( iBin+1,1-N/float(NW))
#         histEff.SetBinError( iBin+1, N/float(NW)*(1./N + 1./NW)**0.5)

#     func = TF1(f"polariz_{bins}0","pol0",varBins[0],varBins[-1])
#     NW = histJpsiBinsW.Integral(histJpsiBinsW.FindBin(varBins[0]), histJpsiBinsW.FindBin(varBins[-1]))
#     N  = histJpsiBins.Integral(histJpsiBinsW.FindBin(varBins[0]), histJpsiBins.FindBin(varBins[-1]))
#     func.SetParameter(0, 1-N/float(NW))

#     canv = TCanvas("canv", "canv", 1000, 800)
#     canv.cd(1)
#     histEff.Draw("E1")
#     func.Draw("SAME")
#     canv.SaveAs(f"{homeDir}/eff_polariz_{bins}.pdf")

#     f = TFile(f"{homeDir}/polariz_eff_total_{bins}.root","RECREATE")
#     histEff.Write()
#     func.Write()
#     f.Close()

if __name__=='__main__':

    gROOT.LoadMacro(f"{localDir}/fit/libs/lhcbStyle.C")
    # compare("PT")
    # compare("Y")
    reweight()