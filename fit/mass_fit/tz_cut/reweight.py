from ROOT import gROOT, TChain, TFile, TH1D, TCanvas, TF1
from array import array
#import numpy as np
#homeDir = "/users/LHCb/zhovkovska/scripts/"
# homeDir = "/sps/lhcb/zhovkovska/Results_ppbar_2018/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/MC/polariz/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/MC/Trigger/"
localDir = "/users/LHCb/zhovkovska/etac2s_ppbar/"

dictBins = {
    "PT":   array('f',[5000., 6500., 8000., 10000., 12000., 14000., 20000.]),
    "Y":    array('f',[2., 2.5, 3., 3.5, 4.])
}

def reweight(bins="PT"):

    gROOT.Reset()
    gROOT.SetStyle("Plain")

    jpsiMass = 3096.90
    #varBins = array('f',[5000., 6500., 8000., 10000., 12000., 14000., 20000.])
    varBins = dictBins[bins]
    nBins = len(varBins)-1

    nt = TChain("DecayTree")
    # ntJpsiMC = TChain("DecayTree")
    #ntJpsiMC.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root')
    #ntJpsiMC.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2015_AddBr.root')
    # ntJpsiMC.Add(dataDir + "/jpsiDiProton_prompt_2018_AddBr.root")
    nt.Add(f"{dataDir}/jpsiDiProton_prompt_2018_AddBr.root")


    cut_ProbNN  = "ProtonP_ProbNNp > 0.6 && ProtonM_ProbNNp > 0.6"
    cut_Trigger = "Jpsi_L0HadronDecision_TOS && (Jpsi_Hlt1DiProtonDecision_TOS || Jpsi_Hlt1DiProtonHighDecision_TOS)"

    ntJpsiMC = nt.CopyTree(f"{cut_ProbNN} && {cut_Trigger}")
    #Float_t jpsiGenN = 644997 + 629999
    #cout << jpsiGenN << " jpsi were generated" << endl


#====================


    minMass = jpsiMass-70.
    maxMass = jpsiMass+70.
    binWidth = 10.
    binN = int((maxMass-minMass)/binWidth)

    entries = ntJpsiMC.GetEntries()
    print(f"entries = {entries:2.2f}\n")

    cutReweight = "(1./(1-0.1/3.))*(1-0.1*ProtonM_CosTheta**2)"

    histJpsiMC = TH1D("histJpsiMC", "Invariant mass of p#bar{p}", binN, minMass, maxMass)
    ntJpsiMC.Project("histJpsiMC", "Jpsi_MM", "")

    histProtonPCos = TH1D("histProtonPCos", "histProtonPCos", 100, -1, 1)
    ntJpsiMC.Project("histProtonPCos", "ProtonM_CosTheta", "")
    histProtonPCosw = TH1D("histProtonPCosw", "histProtonPCosw", 100, -1, 1)
    ntJpsiMC.Project("histProtonPCosw", "ProtonM_CosTheta", cutReweight)

    histJpsiBins = TH1D("histJpsiBins", "histJpsiBins", 100, 0, varBins[-1])
    ntJpsiMC.Project("histJpsiBins", "Jpsi_{}".format(bins), "")
    histJpsiBinsW = TH1D("histJpsiBinsW", "histJpsiBinsW", 100, 0, varBins[-1])
    ntJpsiMC.Project("histJpsiBinsW", "Jpsi_{}".format(bins), cutReweight)

    canvA = TCanvas("canvA", "canvA", 1000, 800)

    canvA.Divide(1,2)
    canvA.cd(1)
    histProtonPCosw.SetLineColor(2)
    histProtonPCosw.Draw()
    histProtonPCos.Draw("SAME")

    canvA.cd(2)
#    gPad.SetLogy(1)
    histJpsiBinsW.SetLineColor(2)
    histJpsiBinsW.Draw()
    histJpsiBins.Draw("SAME")

    #ntJpsiMC.Draw("Jpsi_PT:ProtonM_CosTheta", "")
    canvA.SaveAs(f"{homeDir}/reweight_MC_{bins}.pdf")

    print(f"unweighted: {histProtonPCos.GetSumOfWeights():.3f}\n")
    print(f"weighted:   {histProtonPCosw.GetSumOfWeights():.3f}\n")

    histEff = TH1D("histEff","histEff", nBins, varBins)

    for iBin in range(nBins):
        #NW = ntJpsiMC.GetEntries(cutReweight+" && Jpsi_PT>{} && Jpsi_PT<{}".format(varBins[iBin], varBins[iBin+1]))
        #N = ntJpsiMC.GetEntries("Jpsi_PT>{} && Jpsi_PT<{}".format(varBins[iBin], varBins[iBin+1]))
        NW = histJpsiBinsW.Integral(histJpsiBinsW.FindBin(varBins[iBin]), histJpsiBinsW.FindBin(varBins[iBin+1]))
        N = histJpsiBins.Integral(histJpsiBinsW.FindBin(varBins[iBin]), histJpsiBins.FindBin(varBins[iBin+1]))
        print(varBins[iBin], N, NW)
        histEff.SetBinContent( iBin+1,1-N/float(NW))
        histEff.SetBinError( iBin+1, N/float(NW)*(1./N + 1./NW)**0.5)

    func = TF1(f"polariz_{bins}0","pol0",varBins[0],varBins[-1])
    NW = histJpsiBinsW.Integral(histJpsiBinsW.FindBin(varBins[0]), histJpsiBinsW.FindBin(varBins[-1]))
    N  = histJpsiBins.Integral(histJpsiBinsW.FindBin(varBins[0]), histJpsiBins.FindBin(varBins[-1]))
    func.SetParameter(0, 1-N/float(NW))

    canv = TCanvas("canv", "canv", 1000, 800)
    canv.cd(1)
    histEff.Draw("E1")
    func.Draw("SAME")
    canv.SaveAs(f"{homeDir}/eff_polariz_{bins}.pdf")

    f = TFile(f"{homeDir}/polariz_eff_total_{bins}.root","RECREATE")
    histEff.Write()
    func.Write()
    f.Close()

if __name__=='__main__':
    gROOT.LoadMacro(f"{localDir}/fit/libs/lhcbStyle.C")
    reweight("PT")
    reweight("Y")