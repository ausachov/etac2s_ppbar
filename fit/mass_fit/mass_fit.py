from ROOT import RooFit, RooStats, RooMinuit, gROOT, gStyle, gPad, Math, TChain, TTree, TFile, TGraph, TCanvas, \
    RooRealVar, RooArgSet, RooArgList, RooDataSet, RooDataHist, \
    RooChebychev, RooGenericPdf, RooAddPdf, RooFormulaVar, RooGaussian, RooFFTConvPdf, RooVoigtian, \
    RooSimultaneous, RooCategory, RooProdPdf, RooProfileLL
from ROOT.RooFit import *
from ROOT.RooStats import *

import drawModule as dm
import oniaConstLib as cl
gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
from ROOT import RooRelBreitWigner

import numpy as np

#homeDir = "/afs/cern.ch/user/v/vazhovko/private/etac2s_ppbar/"
#dataDir = "/eos/user/v/vazhovko/etac2s_ppbar/"
homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"


# def get_data_h(w, kSource):

#     dirName_Low  = dataDir +"Data_Low/"
#     dirName_High = dataDir +"Data_High/"


#     nt_Low  =  TChain("DecayTree")
#     nt_High =  TChain("DecayTree")

#     nt_Low.Add(dirName_Low+"/Etac2sDiProton_Low_2018*.root")
#     nt_High.Add(dirName_High+"/Etac2sDiProton_High_2018*.root")

#     #tree_Low = TTree()
#     #tree_High = TTree()

#     cut_FD = ""
#     if kSource=="prompt":
#         cut_FD = TCut("(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08")# & Jpsi_FDCHI2_OWNPV > 49"
#     else:
#         cut_FD = TCut("(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16")# & Jpsi_FDCHI2_OWNPV > 49"

#     cut_PT      = TCut("Jpsi_PT > 6500 && Jpsi_PT < 14000")

#     totCut = TCut(cut_FD + cut_PT)

#     hh_Low = TH1D("hh_Low","hh_Low", bin_Tot_Fit, dm.minM_Low, dm.maxM_High)
#     hh_High = TH1D("hh_High","hh_High", bin_Tot_Fit, dm.minM_Low, dm.maxM_High)

#     #hh_High.SetBinContent(3161, hh_High.GetBinContent(3168))
#     #hh_High.SetBinContent(3160, hh_High.GetBinContent(3168))
#     #hh_High.SetBinContent(3159, hh_High.GetBinContent(3168))
#     #hh_High.SetBinContent(3158, hh_High.GetBinContent(3168))
#     #hh_High.SetBinContent(3157, hh_High.GetBinContent(3168))
#     #hh_Low.Smooth(5)
#     #hh_High.Smooth(5)

#     nt_Low.Draw("Jpsi_m_scaled>>hh_Low",totCut.GetTitle(),"goff")
#     nt_High.Draw("Jpsi_m_scaled>>hh_High",totCut.GetTitle(),"goff")

#     Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M", dm.minM_Low, dm.maxM_High)

#     dh_Low = RooDataHist("dh_Low","dh_Low", RooArgList(Jpsi_M), hh_Low)
#     dh_High = RooDataHist("dh_High","dh_High", RooArgList(Jpsi_M), hh_High)
#     getattr(w,"import")(dh_Low)
#     getattr(w,"import")(dh_High)

#     print("DATAHISTS READ SUCCESSFULLY")

def get_data_h(w, kSource):

    nameWksp = dataDir + "/wksps/wksp_data_{}_base.root".format(kSource)
    file_w = TFile(nameWksp)
    w_in = file_w.Get("w")

    lowData    = w_in.data("dh_Low")
    highData   = w_in.data("dh_High")
    Jpsi_M     = w_in.var("Jpsi_M")

    histLow = lowData.createHistogram("histLow",Jpsi_M)
    nBins = histLow.GetNbinsX()
    minX =  histLow.GetXaxis().GetBinLowEdge(1)
    maxX =  histLow.GetXaxis().GetBinUpEdge(nBins)
    binWidth = histLow.GetBinWidth(1)
    firstBin = int((dm.minM_Low - minX)/binWidth) + 1
    lastBin  = int((dm.maxM_Low - minX)/binWidth)
    histLow.GetXaxis().SetRange(firstBin, lastBin)
    histLow.SaveAs("./hists/Low2018.root")

    histHigh = highData.createHistogram("histHigh",Jpsi_M)
    nBins = histHigh.GetNbinsX()
    minX =  histHigh.GetXaxis().GetBinLowEdge(1)
    maxX =  histHigh.GetXaxis().GetBinUpEdge(nBins)
    binWidth = histHigh.GetBinWidth(1)
    firstBin = int((dm.minM_High - minX)/binWidth) + 1
    lastBin  = int((dm.maxM_High - minX)/binWidth)
    histHigh.GetXaxis().SetRange(firstBin, lastBin)
    histHigh.SaveAs("./hists/High2018.root")


    Jpsi_M  = RooRealVar("Jpsi_M", "Jpsi_M",  dm.minM_Low, dm.maxM_High)

    dh_Low  = RooDataHist("dh_Low","dh_Low",  RooArgList(Jpsi_M), histLow)
    dh_High = RooDataHist("dh_High","dh_High", RooArgList(Jpsi_M), histHigh)
    getattr(w,"import")(Jpsi_M)
    getattr(w,"import")(dh_Low)
    getattr(w,"import")(dh_High)

    print("DATAHISTS READ SUCCESSFULLY")

def get_data_s(w, kSource):

    dirName_Low  = dataDir +"Data_Low/"
    dirName_High = dataDir +"Data_High/"


    nt_Low  =  TChain("DecayTree")
    nt_High =  TChain("DecayTree")

    nt_Low.Add(dirName_Low+"/Etac2sDiProton_Low_2018*.root")
    nt_High.Add(dirName_High+"/Etac2sDiProton_High_2018*.root")

    cut_FD = ""
    if kSource=="prompt":
        cut_FD = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08"# & Jpsi_FDCHI2_OWNPV > 49""
    else:
        cut_FD = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16"# & Jpsi_FDCHI2_OWNPV > 49""

    cut_PT      = "Jpsi_PT > 6500 && Jpsi_PT < 14000"

    totCut = cut_FD + " && " + cut_PT

    tree_Low = TTree()
    tree_High = TTree()

    tree_Low = nt_Low.CopyTree(totCut)
    tree_High = nt_High.CopyTree(totCut)

    Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M", dm.minM_Low, dm.maxM_High)

    ds_Low = RooDataSet("ds_Low","ds_Low", tree_Low, RooArgSet(Jpsi_M))
    ds_High = RooDataSet("ds_High","ds_High", tree_High, RooArgSet(Jpsi_M))
    getattr(w,"import")(ds_Low)
    getattr(w,"import")(ds_High)

    ds_Low.Print()
    ds_High.Print()
    a = input()
    print( "DATASETS READ SUCCESSFULLY")


#def get_data_h(w, kSource):

    #dirName_Low  = dataDir +"Data_Low_Proton_base_mConstrp/{}".format(kSource)
    #dirName_High = dataDir +"Data_High_Proton_base_mConstrp/{}".format(kSource)

    #f_Low  = TFile(dirName_Low+"/Etac2sDiProton_Low_2018_Bin_2.root","READ")
    #f_High = TFile(dirName_High+"/Etac2sDiProton_High_2018_Bin_2.root","READ")

    ##hh_Low = TH1D("hh_Low","hh_Low", binN_Low, dm.minM_Low, dm.maxM_Low)
    ##hh_High = TH1D("hh_High","hh_High", binN_High, dm.minM_High, dm.maxM_High)

    #hh_Low  = TH1D()
    #hh_High = TH1D()

    #hh_Low  = f_Low.Get("Jpsi_M")
    #hh_High = f_High.Get("Jpsi_M")

    #hh_Low.SetBins(4650, dm.minM_Low, dm.maxM_High)
    ##hh_High.SetBins(4650, dm.minM_Low, dm.maxM_High)

    #Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M", dm.minM_Low, dm.maxM_High)

    #dh_Low = RooDataHist("dh_Low","dh_Low", RooArgList(Jpsi_M), hh_Low)
    #dh_High = RooDataHist("dh_High","dh_High", RooArgList(Jpsi_M), hh_High)
    #getattr(w,"import")(dh_Low)
    #getattr(w,"import")(dh_High)


    #print "DATA\"S READ SUCCESSFULLY"


def choose_bkg(optList, regName, argList):
    return {

        "prompt cheb" : RooChebychev ("bkg_{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2),argList.at(3))),
        "fromB cheb"  : RooChebychev ("bkg_{}".format(regName),"Background",argList.at(0), RooArgList(argList.at(1),argList.at(2))),
        "prompt exp"  : RooGenericPdf("bkg_{}".format(regName),"background","TMath::Exp(-(@0-3550.)/200.*@1)*(1.+@2*(@0-3550.)/200.+@3*(@0-3550.)*(@0-3550.)/200./200.)", RooArgList(argList.at(0), argList.at(1),argList.at(2),argList.at(3))) ,
        "fromB exp"   : RooGenericPdf("bkg_{}".format(regName),"background","TMath::Exp(-(@0-3550.)/200.*@1)*(1.+@2*(@0-3550.)/200)", RooArgList(argList.at(0), argList.at(1),argList.at(2)))

    }[optList[0] + " " + optList[1]]


def fillRelWorkspace(w, kSource, kBkg, kNorm="Jpsi"):

    Jpsi_M = w.var("Jpsi_M")
    #Jpsi_M.setBins(4650,"cache")

    #BR_etac2s = RooRealVar("BR_etac2s","BR_etac2s2ppbar",7.1e-5, 4.7e-5, 9.5e-5)
    #BR_etac   = RooRealVar("BR_etac","BR_etac2ppbar",1.51e-3, 1.35e-3, 1.67e-3)
    #BR_Jpsi   = RooRealVar("BR_Jpsi","BR_Jpsi2ppbar",2.121e-3, 2.092e-3, 2.150e-3)

    #BR_etac2s_mean = RooRealVar("BR_etac2s_mean","BR_etac2s2ppbar",4.715e-2) # 4.715e-2 for 1e-4, 3.63e-2 for 7.7e-5
    BR_etac2s_mean = RooRealVar("BR_etac2s_mean","BR_etac2s2ppbar",0.769e-4)
    BR_etac_mean   = RooRealVar("BR_etac_mean","BR_etac2ppbar",1.51e-3)
    BR_Jpsi_mean   = RooRealVar("BR_Jpsi_mean","BR_Jpsi2ppbar",2.121e-3)
    BR_psi2S_mean   = RooRealVar("BR_Jpsi_mean","BR_Jpsi2ppbar",2.88e-4)

    cs_Jpsi = RooRealVar("cs_Jpsi","cs Jpsi", 0.749)
    cs_etac2s_rel = RooRealVar("cs_etac2s_rel","num of Etac", 1e-2, 0, 5.0)


    ratioEtac2sToJpsi = 1.0


    rEtac2sToJpsi = RooRealVar("rEtac2sToJpsi","rEtac2sToJpsi", ratioEtac2sToJpsi)#, 0.01, 5.0)

    n_Jpsi = RooRealVar("n_Jpsi","num of J/Psi", 2e3, 10, 1.e6)

    n_Jpsi_1 = RooFormulaVar("n_Jpsi_1","num of J/Psi","@0*0.95",RooArgList(n_Jpsi))
    n_Jpsi_2 = RooFormulaVar("n_Jpsi_2","num of J/Psi","@0-@1",RooArgList(n_Jpsi,n_Jpsi_1))

    n_psi2S = RooRealVar("n_psi2S","num of psi2S", 1e3, 0, 1.e4)
    #n_psi2S_rel = RooRealVar("n_psi2S_rel","num of psi2S", 0.5, 0, 5.)
    #n_psi2S = RooFormulaVar("n_psi2S","num of Etac","@0*@1", RooArgList(n_psi2S_rel,n_Jpsi))

    n_etac = RooRealVar("n_etac","num of etac", 1e4, 10, 1.e6)
    n_etac2s_rel = RooRealVar("n_etac2s_rel","num of Etac", 1e-2, 0, 2.15)
    #n_etac2s = RooRealVar("n_etac2s","num of etac2s", 1e3, 10, 1.e4)
    if kNorm=="Jpsi":
        #n_etac2s = RooFormulaVar("n_etac2s","num of Etac","@0*@1*@2/@3", RooArgList(cs_etac2s_rel,n_Jpsi,BR_etac2s_mean,BR_Jpsi_mean))
        #n_etac2s = RooFormulaVar("n_etac2s","num of Etac","3.63e-2*@0*@1", RooArgList(cs_etac2s_rel,n_Jpsi))
        n_etac2s = RooFormulaVar("n_etac2s","num of Etac","@0*@1", RooArgList(n_etac2s_rel,n_Jpsi))
    elif kNorm=="psi2S":
        n_etac2s = RooFormulaVar("n_etac2s","num of Etac","@0*@1", RooArgList(n_etac2s_rel,n_psi2S))
    else:
        #n_etac2s = RooFormulaVar("n_etac2s","num of Etac","@0*@1*@2/@3", RooArgList(cs_etac2s_rel,n_etac,BR_etac2s_mean,BR_etac_mean))
        n_etac2s = RooFormulaVar("n_etac2s","num of Etac","@0*@1", RooArgList(n_etac2s_rel,n_etac))


    n_chic0 = RooRealVar("n_chic0","num of chic0", 1e3, 0, 1.e4)
    n_chic1 = RooRealVar("n_chic1","num of chic1", 1e3, 0, 1.e4)
    n_chic2 = RooRealVar("n_chic2","num of chic2", 1e3, 0, 1.e4)
    #n_chic1_rel = RooRealVar("n_chic1_rel","num of chic1", 0.5, 0, 3.)
    #n_chic2_rel = RooRealVar("n_chic2_rel","num of chic2", 0.6, 0, 3.)
    #n_chic1 = RooFormulaVar("n_chic1","num of Etac","@0*@1", RooArgList(n_chic1_rel,n_chic0))
    #n_chic2 = RooFormulaVar("n_chic2","num of Etac","@0*@1", RooArgList(n_chic2_rel,n_chic0))

    n_hc = RooRealVar("n_hc","num of hc", 1e3, 0, 1.e4)


    n_psi3770 = RooRealVar("n_psi3770","num of psi3770", 1e1, 0, 1.e5)
    #n_psi3770.setVal(0.); n_psi3770.setConstant()

    # nSignal_Low = RooFormulaVar("nSignal_Low","num of Etac","@0+@1", RooArgList(n_etac,n_Jpsi))
    # nSignal_High = RooFormulaVar("nSignal_High","num of Etac","@0+@1+@2+@3+@4+@5+@6", RooArgList(n_etac2s,n_psi2S,n_chic0,n_chic1,n_chic2,n_hc))

    nBckgr_High = RooRealVar("nBckgr_High","num of bckgr",7e7,1e4,1.e+9)
    nBckgr_Low = RooRealVar("nBckgr_Low","num of bckgr",7e7,1e4,1.e+9)

    #Psi

    mass_Jpsi = RooRealVar("mass_Jpsi","mean of gaussian",cl.M_JPSI, 3030, 3150)
    mass_Jpsi.setConstant()

    mass_psi2S = RooRealVar("mass_psi2S","mean of gaussian",cl.M_PSI2S, 3674, 3698)
    #mass_psi2S.setConstant()
    #mass_res_psi2S = RooRealVar("mass_res_psi2S","mean of gaussian", cl.M_RES_L_PSI2S)
    #mass_psi2S     = RooFormulaVar("mass_psi2S","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_psi2S))

    #mass_psi3770 = RooRealVar("mass_psi3770","mean of gaussian",cl.M_PSI3770)
    #mass_psi3770.setConstant()
    mass_res_psi3770 = RooRealVar("mass_res_psi3770","mean of gaussian", cl.M_RES_H_PSI3770)
    mass_psi3770     = RooFormulaVar("mass_psi3770","mean of gaussian","@0+@1",RooArgList(mass_psi2S,mass_res_psi3770))
    gamma_psi3770 = RooRealVar("gamma_psi3770","width of Br-W", cl.GAMMA_PSI3770)
    spin_psi3770 = RooRealVar("spin_psi3770","spin_psi3770", 1. )


    #eta_c

    #mass_etac = RooRealVar("mass_etac","mean of gaussian", cl.M_ETAC, 2980, 2988)
    #mass_etac.setConstant()
    mass_res_etac = RooRealVar("mass_res_etac","mean of gaussian", cl.M_RES_ETAC, 100, 125)
    mass_res_etac.setConstant()
    mass_etac     = RooFormulaVar("mass_etac","mean of gaussian","@0-@1",RooArgList(mass_Jpsi,mass_res_etac))
    gamma_etac    = RooRealVar("gamma_etac","width of Br-W", cl.GAMMA_ETAC)
    spin_etac     = RooRealVar("spin_etac","spin_eta", 0. )

    radius = RooRealVar("radius","radius", 1.)
    proton_m = RooRealVar("proton_m","proton mass", cl.M_PROTON )

    #mass_etac2s = RooRealVar("mass_etac2s","mean of gaussian", cl.M_ETAC2S)
    #mass_etac2s.setConstant()
    #mass_res_etac2s = RooRealVar("mass_res_etac2s","mean of gaussian", cl.M_RES_L_ETAC2S, 530, 560)
    #mass_res_etac2s.setConstant()
    #mass_etac2s     = RooFormulaVar("mass_etac2s","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_etac2s))
    mass_res_etac2s = RooRealVar("mass_res_etac2s","mean of gaussian", cl.M_RES_H_ETAC2S, cl.M_RES_H_ETAC2S-22.3, cl.M_RES_H_ETAC2S+22.3)
    mass_res_etac2s_var = RooRealVar("mass_res_etac2s_var","mean of gaussian", cl.M_RES_H_ETAC2S)
    mass_res_etac2s.setConstant()
    mass_etac2s     = RooFormulaVar("mass_etac2s","mean of gaussian","@0-@1",RooArgList(mass_psi2S,mass_res_etac2s))
    gamma_etac2s = RooRealVar("gamma_etac2s","width of Br-W", cl.GAMMA_ETAC2S)
    spin_etac2s = RooRealVar("spin_etac2s","spin_eta", 0. )


    #chi_c

    #mass_chic0 = RooRealVar("mass_chic0","mean of gaussian", cl.M_CHIC0)
    #mass_chic0.setConstant()
    #mass_res_chic0 = RooRealVar("mass_res_chic0","mean of gaussian", cl.M_RES_L_CHIC0)
    #mass_chic0     = RooFormulaVar("mass_chic0","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_chic0))
    mass_res_chic0 = RooRealVar("mass_res_chic0","mean of gaussian", cl.M_RES_H_CHIC0, cl.M_RES_H_CHIC0-10.,cl.M_RES_H_CHIC0+10.)
    mass_res_chic0.setConstant()
    mass_chic0     = RooFormulaVar("mass_chic0","mean of gaussian","@0-@1",RooArgList(mass_psi2S,mass_res_chic0))
    gamma_chic0 = RooRealVar("gamma_chic0","width of Br-W", cl.GAMMA_CHIC0)
    spin_chic0 = RooRealVar("spin_chic0","spin_chic0", 0. )

    #mass_chic1 = RooRealVar("mass_chic1","mean of gaussian", cl.M_CHIC1)
    #mass_chic1.setConstant()
    #mass_res_chic1 = RooRealVar("mass_res_chic1","mean of gaussian", cl.M_RES_L_CHIC1)
    #mass_chic1     = RooFormulaVar("mass_chic1","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_chic1))
    mass_res_chic1 = RooRealVar("mass_res_chic1","mean of gaussian", cl.M_RES_H_CHIC1, cl.M_RES_H_CHIC1-15, cl.M_RES_H_CHIC1+15)
    mass_res_chic1.setConstant()
    mass_chic1     = RooFormulaVar("mass_chic1","mean of gaussian","@0-@1",RooArgList(mass_psi2S,mass_res_chic1))
    gamma_chic1 = RooRealVar("gamma_chic1","width of Br-W", cl.GAMMA_CHIC1)
    spin_chic1 = RooRealVar("spin_chic1","spin_chic1", 1. )

    #mass_chic2 = RooRealVar("mass_chic2","mean of gaussian", cl.M_CHIC2)
    #mass_chic2.setConstant()
    #mass_res_chic2 = RooRealVar("mass_res_chic2","mean of gaussian", cl.M_RES_L_CHIC2)
    #mass_chic2     = RooFormulaVar("mass_chic2","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_chic2))
    mass_res_chic2 = RooRealVar("mass_res_chic2","mean of gaussian", cl.M_RES_H_CHIC2)
    mass_chic2     = RooFormulaVar("mass_chic2","mean of gaussian","@0-@1",RooArgList(mass_psi2S,mass_res_chic2))
    gamma_chic2 = RooRealVar("gamma_chic2","width of Br-W", cl.GAMMA_CHIC2)
    spin_chic2 = RooRealVar("spin_chic2","spin_chic2", 2. )

    #h_c

    #mass_hc = RooRealVar("mass_hc","mean of gaussian", cl.M_HC)
    #mass_hc.setConstant()
    #mass_res_hc = RooRealVar("mass_res_hc","mean of gaussian", cl.M_RES_L_HC)
    #mass_hc  = RooFormulaVar("mass_hc","mean of gaussian","@0+@1",RooArgList(mass_Jpsi,mass_res_hc))
    mass_res_hc = RooRealVar("mass_res_hc","mean of gaussian", cl.M_RES_H_HC)
    mass_hc  = RooFormulaVar("mass_hc","mean of gaussian","@0-@1",RooArgList(mass_psi2S,mass_res_hc))
    gamma_hc = RooRealVar("gamma_hc","width of Br-W", cl.GAMMA_HC)
    spin_hc = RooRealVar("spin_hc","spin_hc", 1. )


    sigma_Jpsi = RooRealVar("sigma_Jpsi","width of gaussian", 9., 0.1, 50.)
    sigma_Jpsi_w = RooFormulaVar('sigma_Jpsi_w','width of gaussian','@0/0.21', RooArgList(sigma_Jpsi))

    #sigma_etac2s = RooRealVar("sigma_etac2s","width of gaussian", 9., 0.1, 50.)
    sigma_etac = RooFormulaVar("sigma_etac","width of gaussian","@0*({}/{})**0.5".format(cl.M_ETAC,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_etac2s = RooFormulaVar("sigma_etac2s","width of gaussian","@0*({}/{})**0.5".format(cl.M_ETAC2S,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_chic0 = RooFormulaVar("sigma_chic0","width of gaussian","@0*({}/{})**0.5".format(cl.M_CHIC0,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_chic1 = RooFormulaVar("sigma_chic1","width of gaussian","@0*({}/{})**0.5".format(cl.M_CHIC1,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_chic2 = RooFormulaVar("sigma_chic2","width of gaussian","@0*({}/{})**0.5".format(cl.M_CHIC2,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_hc = RooFormulaVar("sigma_hc","width of gaussian","@0*({}/{})**0.5".format(cl.M_HC,cl.M_JPSI), RooArgList(sigma_Jpsi))
    #sigma_psi2S = RooRealVar("sigma_psi2S","width of gaussian", 9., 0.1, 30.)
    sigma_psi2S = RooFormulaVar("sigma_psi2S","width of gaussian","@0*({}/{})**0.5".format(cl.M_PSI2S,cl.M_JPSI), RooArgList(sigma_Jpsi))
    sigma_psi3770 = RooFormulaVar("sigma_psi3770","width of gaussian","@0*({}/{})**0.5".format(cl.M_PSI3770,cl.M_JPSI), RooArgList(sigma_Jpsi))


    #Fit signal

    gauss = RooGaussian("gauss","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_Jpsi)

    #eta_c
    gauss_etac = RooGaussian("gauss_etac","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_etac)
    br_wigner_etac = RooRelBreitWigner("br_wigner_etac", "br_wigner", Jpsi_M, mass_etac, gamma_etac, spin_etac, radius, proton_m, proton_m)
    bwxg_etac = RooFFTConvPdf("bwxg_etac","breit-wigner (X) gauss", Jpsi_M, br_wigner_etac, gauss_etac)

    gauss_etac2s = RooGaussian("gauss_etac2s","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_etac2s)
    br_wigner_etac2s = RooRelBreitWigner("br_wigner_etac2s", "br_wigner", Jpsi_M, mass_etac2s, gamma_etac2s, spin_etac2s, radius, proton_m, proton_m)
    bwxg_etac2s = RooFFTConvPdf("bwxg_etac2s","breit-wigner (X) gauss", Jpsi_M, br_wigner_etac2s, gauss_etac2s)
    #bwxg = RooVoigtian("bwxg", "bwxg", Jpsi_M, mass_etac2s, gamma_etac2s, sigma_etac2s)

    #chi_c
    gauss_chic0 = RooGaussian("gauss_chic0","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_chic0)
    br_wigner_chic0 = RooRelBreitWigner("br_wigner_chic0", "br_wigner", Jpsi_M, mass_chic0, gamma_chic0, spin_chic0, radius, proton_m, proton_m)
    bwxg_chic0 = RooFFTConvPdf("bwxg_chic0","breit-wigner (X) gauss", Jpsi_M, br_wigner_chic0, gauss_chic0)

    gauss_chic1 = RooGaussian("gauss_chic1","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_chic1)
    br_wigner_chic1 = RooRelBreitWigner("br_wigner_chic1", "br_wigner", Jpsi_M, mass_chic1, gamma_chic1, spin_chic1, radius, proton_m, proton_m)
    bwxg_chic1 = RooFFTConvPdf("bwxg_chic1","breit-wigner (X) gauss", Jpsi_M, br_wigner_chic1, gauss_chic1)

    gauss_chic2 = RooGaussian("gauss_chic2","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_chic2)
    br_wigner_chic2 = RooRelBreitWigner("br_wigner_chic2", "br_wigner", Jpsi_M, mass_chic2, gamma_chic2, spin_chic2, radius, proton_m, proton_m)
    bwxg_chic2 = RooFFTConvPdf("bwxg_chic2","breit-wigner (X) gauss", Jpsi_M, br_wigner_chic2, gauss_chic2)

    #h_c
    gauss_hc = RooGaussian("gauss_hc","gauss PDF", Jpsi_M, RooFit.RooConst(0), sigma_hc)
    br_wigner_hc = RooRelBreitWigner("br_wigner_hc", "br_wigner", Jpsi_M, mass_hc, gamma_hc, spin_hc, radius, proton_m, proton_m)
    bwxg_hc = RooFFTConvPdf("bwxg_hc","breit-wigner (X) gauss", Jpsi_M, br_wigner_hc, gauss_hc)


    #Fit J/psi
    #gauss_Jpsi = RooGaussian("gauss_Jpsi","gaussian PDF", Jpsi_M, mass_Jpsi, sigma_Jpsi)
    gauss_Jpsi_1 = RooGaussian("gauss_Jpsi_1","gaussian PDF", Jpsi_M, mass_Jpsi, sigma_Jpsi)
    gauss_Jpsi_2 = RooGaussian("gauss_Jpsi_2","gaussian PDF", Jpsi_M, mass_Jpsi, sigma_Jpsi_w)

    gauss_psi2S = RooGaussian("gauss_psi2S","gaussian PDF", Jpsi_M, mass_psi2S, sigma_psi2S)

    #br_wigner_psi3770 = RooRelBreitWigner("br_wigner_psi3770", "br_wigner", Jpsi_M, mass_psi3770, gamma_psi3770, spin_psi3770, radius, proton_m, proton_m)
    #bwxg_psi3770 = RooFFTConvPdf("bwxg_psi3770","breit-wigner (X) gauss", Jpsi_M, br_wigner_psi3770, gauss)
    bwxg_psi3770 = RooVoigtian("bwxg_psi3770", "bwxg_psi3770", Jpsi_M, mass_psi3770, gamma_psi3770, sigma_psi3770)



    eff_pppi0 = RooRealVar("eff_pppi0","eff_pppi0",0.06)
    pppi0 = RooGenericPdf("pppi0","Jpsi.pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))
    n_pppi0 = RooFormulaVar("n_pppi0","n_pppi0","@0*@1*(1.19/2.12)",RooArgList(n_Jpsi,eff_pppi0))

    pppi0_High = RooGenericPdf("pppi0_High","psi2S.pppi0","@0<(3551.12) ? TMath::Sqrt(3686.097-134.977-@0) : 0",RooArgList(Jpsi_M))
    n_pppi0_High = RooFormulaVar("n_pppi0_High","n_pppi0_High","@0*@1*(1.53/2.94)",RooArgList(n_psi2S,eff_pppi0))

    #Bkg fit
    a0_High = RooRealVar("a0_High","a0",-0.4,-1.5,1.5)
    a1_High = RooRealVar("a1_High","a1",0.05,-1.,1.)
    a2_High = RooRealVar("a2_High","a2",-0.005,-1.,1.)

    a1_High.setVal(0.04)
    a2_High.setVal(0.04)

    parBkg_High = RooArgList(Jpsi_M, a0_High, a1_High, a2_High)
    #bkg_High    = choose_bkg([kSource,kBkg], "High", parBkg_High)
    bkg_High    = choose_bkg(["prompt",kBkg], "High", parBkg_High)


    #Bkg fit
    a0_Low = RooRealVar("a0_Low","a0",-0.4,-1.5,1.5)
    a1_Low = RooRealVar("a1_Low","a1",0.05,-1.,1.)
    a2_Low = RooRealVar("a2_Low","a2",-0.005,-1.,1.)
    #a3_Low = RooRealVar("a3_Low","a2",-0.005,-1,1.)

    a1_Low.setVal(0.04)
    a2_Low.setVal(0.04)

    parBkg_Low = RooArgList(Jpsi_M, a0_Low, a1_Low, a2_Low)
    bkg_Low    = choose_bkg([kSource,kBkg], "Low", parBkg_Low)

    #bkg = RooGenericPdf("bkg","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200)", RooArgList(Jpsi_M,a0,a1))
    #bkg = RooChebychev ("bkg_Low","Background",Jpsi_M,RooArgList(a0_Low,a1_Low,a2_Low,a3_Low))



    modelSignal_High = RooAddPdf("modelSignal_High","etac2s signal", RooArgList(bwxg_etac2s, bwxg_chic0, bwxg_chic1, bwxg_chic2, bwxg_hc, gauss_psi2S), RooArgList(n_etac2s, n_chic0, n_chic1, n_chic2, n_hc, n_psi2S))
    modelSignal_etac2s = RooAddPdf("modelSignal_etac2s","etac2s signal", RooArgList(bwxg_etac2s), RooArgList(n_etac2s))
    modelBkg_High = RooAddPdf("modelBkg_High","etac2s bkg", RooArgList(bkg_High), RooArgList(nBckgr_High))
    model_High = RooAddPdf("model_High","High diapason signal", RooArgList(bkg_High, pppi0_High, bwxg_etac2s, bwxg_chic0, bwxg_chic1, bwxg_chic2, bwxg_hc, gauss_psi2S), RooArgList(nBckgr_High, n_pppi0_High, n_etac2s, n_chic0, n_chic1, n_chic2, n_hc, n_psi2S))

    #modelSignal_Low = RooAddPdf("modelSignal_Low","Jpsi signal", RooArgList(bwxg_etac, gauss_Jpsi), RooArgList(n_etac, n_Jpsi))
    #modelSignal_Jpsi = RooAddPdf("modelSignal_Jpsi","Jpsi signal", RooArgList(gauss_Jpsi), RooArgList(n_Jpsi))
    #modelBkg_Low = RooAddPdf("modelBkg_Low","Jpsi bkg", RooArgList(bkg_Low), RooArgList(nBckgr_Low))
    #model_Low = RooAddPdf("model_Low","Low diapason signal", RooArgList(bwxg_etac,gauss_Jpsi,bkg_Low, pppi0), RooArgList(n_etac,n_Jpsi,nBckgr_Low, n_pppi0))

    modelSignal_Low = RooAddPdf("modelSignal_Low","Jpsi signal", RooArgList(bwxg_etac, gauss_Jpsi_1, gauss_Jpsi_2), RooArgList(n_etac, n_Jpsi_1, n_Jpsi_2))
    modelSignal_Jpsi = RooAddPdf("modelSignal_Jpsi","Jpsi signal", RooArgList(gauss_Jpsi_1, gauss_Jpsi_2), RooArgList(n_Jpsi_1, n_Jpsi_2))
    modelBkg_Low = RooAddPdf("modelBkg_Low","Jpsi bkg", RooArgList(bkg_Low), RooArgList(nBckgr_Low))
    model_Low = RooAddPdf("model_Low","Low diapason signal", RooArgList(bwxg_etac,gauss_Jpsi_1, gauss_Jpsi_2,bkg_Low, pppi0), RooArgList(n_etac,n_Jpsi_1, n_Jpsi_2,nBckgr_Low, n_pppi0))

    #n_etac_obs = RooRealVar("n_etac_obs","num of etac", 9.1639e+04, 8.4869e+4, 9.8409e+4)
    #fconstr_etac = RooGaussian("fconstr_etac","fconstr_etac", n_etac, n_etac_obs, RooFit.RooConst(6.77e+03))
    #model_Low_c = RooProdPdf("model_Low_c", "model with constraint", RooArgList(model_Low, fconstr_etac))

    fconstr_mass = RooGaussian("fconstr_mass","fconstr_mass", mass_res_etac2s, mass_res_etac2s_var, RooFit.RooConst(5.5))
    model_High_c = RooProdPdf("model_High_c", "model with constraint", RooArgList(model_High, fconstr_mass))

    sample = RooCategory("sample","sample")
    sample.defineType("High")
    sample.defineType("Low")

    Jpsi_M.setRange("fitRange_Low", dm.dm.minM_Low, dm.dm.maxM_Low)
    Jpsi_M.setRange("fitRange_High", dm.dm.minM_High, dm.dm.maxM_High)

    data_etac2s = w.data("dh_High")
    data_Jpsi = w.data("dh_Low")


    # Construct combined dataset in (Jpsi_M,sample)
    combData = RooDataHist("combData", "combined data", RooArgList(Jpsi_M), RooFit.Index(sample), RooFit.Import("High",data_etac2s), RooFit.Import("Low",data_Jpsi))

    #data_etac2s = w.data("ds_High")
    #data_Jpsi = w.data("ds_Low")


    ## Construct combined dataset in (Jpsi_M,sample)
    #combData = RooDataSet("combData", "combined data", RooArgSet(Jpsi_M), RooFit.Index(sample), RooFit.Import("High",data_etac2s), RooFit.Import("Low",data_Jpsi))


    # Associate model with the physics state and model_ctl with the control state
    simPdf = RooSimultaneous("simPdf","simultaneous signal pdf",sample)
    simPdf.addPdf(model_High,"High")
    simPdf.addPdf(model_Low,"Low")


    simPdf_c = RooSimultaneous("simPdf_c","simultaneous signal pdf",sample)
    simPdf_c.addPdf(model_High_c,"High")
    simPdf_c.addPdf(model_Low,"Low")


    #   getattr(w,"import")(model,RecycleConflictNodes())
    getattr(w,"import")(combData)
    getattr(w,"import")(simPdf)

    getattr(w,"import")(model_High,RooFit.RecycleConflictNodes())
    getattr(w,"import")(model_Low,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelSignal_High,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelSignal_Low,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelBkg_High,RooFit.RecycleConflictNodes())
    getattr(w,"import")(modelBkg_Low,RooFit.RecycleConflictNodes())

    # getattr(w,"import")(nSignal_Low,RooFit.RecycleConflictNodes())
    # getattr(w,"import")(nSignal_High,RooFit.RecycleConflictNodes())

    getattr(w,"import")(simPdf_c, RooFit.RecycleConflictNodes())
    getattr(w,"import")(fconstr_mass, RooFit.RecycleConflictNodes())
    getattr(w,"import")(mass_res_etac2s_var, RooFit.RecycleConflictNodes())
    #getattr(w,"import")(fconstr_etac, RooFit.RecycleConflictNodes())
    #getattr(w,"import")(n_etac_obs, RooFit.RecycleConflictNodes())


def perform_fit_simult(kSource,kBkg,kNorm="Jpsi" ):


    homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
    gStyle.SetStatFormat("4.3f");
    #gStyle.SetOptTitle(0)
    #w = RooWorkspace("w",True)

    #get_data_s(w,kSource)
    #get_data_h(w,kSource)

    nameWksp = dataDir + "/wksps/wksp_data_{}_base.root".format(kSource)
    file_w = TFile(nameWksp)
    w = file_w.Get("w")

    fillRelWorkspace(w, kSource, kBkg, kNorm)

    Jpsi_M = w.var("Jpsi_M")

    modelBkg_High = w.pdf("modelBkg_High")
    modelBkg_Low = w.pdf("modelBkg_Low")


    sample = w.cat("sample")
    simPdf = w.pdf("simPdf_c")
    combData = w.data("combData")

    data_etac2s = w.data("dh_High")
    data_Jpsi = w.data("dh_Low")

    if kSource=="prompt":
        f = TFile(homeDir+"/mass_fit/FitMass_wksp_{}_fromB_{}_base_mConstr.root".format(kNorm, kBkg),"READ")
        w_fromB = f.Get("w")
        f.Close()
        sigma = w_fromB.var("sigma_Jpsi").getValV()
        w.var("sigma_Jpsi").setVal(sigma)
        w.var("sigma_Jpsi").setConstant(True)
        mass = w_fromB.var("mass_Jpsi").getValV()
        w.var("mass_Jpsi").setVal(mass)
        #w.var("mass_Jpsi").setConstant(False)
        mass = w_fromB.var("mass_psi2S").getValV()
        w.var("mass_psi2S").setVal(mass)
        w.var("mass_psi2S").setConstant(True)
        mass = w_fromB.var("mass_res_etac2s").getValV()
        w.var("mass_res_etac2s").setVal(mass)
        w.var("mass_res_etac2s").setConstant(True)
        #w.var("mass_res_etac").setConstant(False)
    else:
        w.var("mass_Jpsi").setConstant(False)
        w.var("mass_psi2S").setConstant(True)
        w.var("mass_res_etac2s").setConstant(False)

    modelBkg_Low.fitTo(combData, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.NumCPU(28))
    modelBkg_High.fitTo(combData, RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.NumCPU(28))

    r = simPdf.fitTo(combData, RooFit.Strategy(2), RooFit.Extended(True), RooFit.Save(), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.NumCPU(28))
    #w.var("n_Jpsi").setVal(w.var("n_Jpsi").getValV())
    #w.var("mass_psi2S").setConstant(True)
    #w.var("mass_res_etac").setConstant(False)
    r = simPdf.fitTo(combData, RooFit.Strategy(2), RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.NumCPU(28))
    #w.var("mass_res_etac2s").setConstant(False)
    #w.var("mass_res_etac").setConstant(True)
    #w.var("mass_psi2S").setConstant(False)
    r = simPdf.fitTo(combData, RooFit.Strategy(2), RooFit.Extended(True), RooFit.Save(), RooFit.Offset(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.NumCPU(28))

    frame_Low, resid_Low, pull_Low = dm.set_frame(w,"Low")
    frame_High, resid_High, pull_High = dm.set_frame(w,"High")

    names  = ["Low","High"]
    frames = [frame_Low,frame_High]
    resids = [resid_Low,resid_High]
    pulls  = [pull_Low,pull_High]
    c = dm.set_canvas(names, frames, resids, pulls)

    nameWksp = homeDir + "/mass_fit/Wksp_{}_{}_{}_base_mConstr.root".format(kNorm,kSource,kBkg)
    namePic = homeDir + "/mass_fit/Plot_{}_{}_{}_base_mConstr.pdf".format(kNorm,kSource,kBkg)
    nameTxt = homeDir + "/mass_fit/Result_{}_{}_{}_base_mConstr.txt".format(kNorm,kSource,kBkg)


    c.SaveAs(namePic)
    w.writeToFile(nameWksp)

    import os, sys
    save = os.dup( sys.stdout.fileno() )
    newout = open(nameTxt, "a" )
    os.dup2( newout.fileno(), sys.stdout.fileno() )
    r.Print("v")
    r.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() )
    newout.close()


    print("fit performed successfully")

    return w


def calcUL(w, kSource, kBkg, kNorm):

    Jpsi_M = w.var("Jpsi_M")

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf_c")
    combData = w.data("combData")

    var_list_0 = w.allVars()

    nuis_iter = var_list_0.createIterator()
    var = nuis_iter.Next()
    while var:
        var.setConstant(True)
        var = nuis_iter.Next()

    #poi_var = w.var("cs_etac2s_rel")
    poi_var = w.var("n_etac2s_rel")
    poi_var.setConstant(False)

    mass_res_etac2s = w.var("mass_res_etac2s")
    mass_res_etac2s_var = w.var("mass_res_etac2s_var")
    #mass_res_etac2s.setConstant(False)

    ## ULs Calculator using HypoTestInverter

    nBckgr_High = w.var("nBckgr_High")
    nBckgr_Low  = w.var("nBckgr_Low")
    n_Jpsi      = w.var("n_Jpsi")
    n_psi2S     = w.var("n_psi2S")
    n_psi3770   = w.var("n_psi3770")
    n_etac      = w.var("n_etac")
    n_hc        = w.var("n_hc")
    n_chic0     = w.var("n_chic0")
    n_chic1     = w.var("n_chic1")
    n_chic2     = w.var("n_chic2")
    a0_Low      = w.var("a0_Low")
    a1_Low      = w.var("a1_Low")
    a2_Low      = w.var("a2_Low")
    a0_High     = w.var("a0_High")
    a1_High     = w.var("a1_High")
    a2_High     = w.var("a2_High")


    BR_Jpsi_mean = w.var("BR_Jpsi_mean")
    BR_etac2s_mean = w.var("BR_etac2s_mean")

    nBckgr_High.setConstant(False)
    #nBckgr_Low.setConstant(False)
    #n_Jpsi.setConstant(False)
    #n_etac.setConstant(False)


    if kSource=="prompt":
        bgParams = RooArgSet(nBckgr_High, nBckgr_Low, a0_Low, a1_Low, a2_Low, a0_High, a1_High, a2_High)
        poi_var.setRange(0., 0.20)

    else:
        bgParams = RooArgSet(nBckgr_High, nBckgr_Low, a0_Low, a1_Low, a0_High, a1_High)
        poi_var.setRange(0., 0.4)

    Math.MinimizerOptions.SetDefaultMinimizer("Minuit2","Minimize");
    Math.MinimizerOptions.SetDefaultStrategy(2);
    Math.MinimizerOptions.SetDefaultTolerance(1e-3)
    Math.MinimizerOptions.SetDefaultMaxFunctionCalls(1000000)

    paramOfInterest = RooArgSet(poi_var)
    constrainedParams = RooArgSet(n_Jpsi, n_etac, n_hc, n_psi2S, n_chic0, n_chic1, n_chic2)

    nuis_iter = constrainedParams.createIterator()
    var = nuis_iter.Next()
    band_size = 10
    while var:
        if var.getVal()-band_size*var.getError() > 0:
            var.setRange(var.getVal()-band_size*var.getError(), var.getVal()+band_size*var.getError())
        else:
            var.setRange(0, var.getVal()+band_size*var.getError())
        print(var.GetName(), var.getVal()-band_size*var.getError(), var.getVal()+band_size*var.getError())
        var = nuis_iter.Next()

    #constrainedParams.add(bgParams)
    #constrainedParams.add(RooArgSet(BR_Jpsi_mean, BR_etac2s_mean))

    #n_etac.setRange(n_etac.getVal()-5*n_etac.getError(), n_etac.getVal()+5*n_etac.getError())
    #nBckgr_High.setRange(nBckgr_High.getVal()-5*nBckgr_High.getError(), nBckgr_High.getVal()+5*nBckgr_High.getError())
    #nBckgr_Low.setRange(nBckgr_Low.getVal()-5*nBckgr_Low.getError(), nBckgr_Low.getVal()+5*nBckgr_Low.getError())


    modelConfig = RooStats.ModelConfig(w);
    modelConfig.SetPdf(simPdf)
    modelConfig.SetProtoData(combData)
    modelConfig.SetParametersOfInterest(paramOfInterest)
    modelConfig.SetNuisanceParameters(constrainedParams)
    modelConfig.SetObservables(RooArgSet(Jpsi_M, sample))
    modelConfig.SetConstraintParameters(RooArgSet(mass_res_etac2s))
    modelConfig.SetGlobalObservables(RooArgSet(mass_res_etac2s_var))
    modelConfig.SetName("ModelConfig")
    paramOfInterest.first().setVal(0.03)
    #paramOfInterest.add(constrainedParams)
    modelConfig.SetSnapshot(paramOfInterest)
    modelConfig.SetWS(w)
    getattr(w,"import")(modelConfig)

    sbModel = w.obj("ModelConfig")
    poi = sbModel.GetParametersOfInterest().first()
    bModel = sbModel.Clone()
    bModel.SetName(sbModel.GetName()+"_with_poi_0")
    poi.setVal(0)
    bModel.SetSnapshot(RooArgSet(poi))

    fc = RooStats.FrequentistCalculator(combData, bModel, sbModel)
    fc.SetToys(1000,500)
    ac = RooStats.AsymptoticCalculator(combData, bModel, sbModel)
    ac.SetOneSided(True)
    ac.SetPrintLevel(-1)

    calc = RooStats.HypoTestInverter(ac)
    calc.SetConfidenceLevel(0.95)

    calc.UseCLs(True)
    calc.SetVerbose(False)

    toymcs = calc.GetHypoTestCalculator().GetTestStatSampler()

    profll = RooStats.ProfileLikelihoodTestStat(sbModel.GetPdf())
    profll.SetOneSided(True)
    profll.SetLOffset(True)
    toymcs.SetTestStatistic(profll)

    if not sbModel.GetPdf().canBeExtended():
        toymcs.SetNEventsPerToy(1)
        print ('can not be extended')

    npoints = 25
    poimin = poi.getMin()
    poimax = poi.getMax()


    calc.SetFixedScan(npoints,poimin,poimax)
    r = calc.GetInterval()
    upperLimit = r.UpperLimit()
    print("Exp Upper Limit is", r.GetExpectedUpperLimit())
    print("Upper Limit is", upperLimit)


    plot = RooStats.HypoTestInverterPlot("HTI_Result_Plot","HypoTest Scan Result",r)
    c = TCanvas("HypoTestInverter Scan")
    c.SetLogy(False)
    plot.Draw("CLb 2CL")

    homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"

    #plot.Draw("EXP")
    c.RangeAxis(poimin, 0., poimax, 1.1)
    c.Draw()
    c.SaveAs(homeDir + "/mass_fit/CL_Ac_{}_{}_{}_base_mConstr.root".format(kSource,kBkg,kNorm))
    c.SaveAs(homeDir + "/mass_fit/CL_Ac_{}_{}_{}_base_mConstr.pdf".format(kSource,kBkg,kNorm))


    ### C o n s t r u c t   p l a i n   l i k e l i h o o d
    ### ---------------------------------------------------

    # Construct unbinned likelihood
    nll = simPdf.createNLL(combData, RooFit.NumCPU(8), RooFit.Extended(True), RooFit.Range("fitRange"), RooFit.SplitRange(True), RooFit.Offset(True))


    # Minimize likelihood w.r.t all parameters before making plots
    m = RooMinuit(nll)
    m.migrad()
    m.hesse()
    #m.minos()

    # Plot likelihood scan frac
    frameRS = poi_var.frame(RooFit.Bins(46),RooFit.Title("LL and profileLL in n_{#eta_{c}}(2S) "))

    # The profile likelihood estimator on nll for frac will minimize nll w.r.t
    # all floating parameters except frac for each evaluation

    pll_N_etac2s = RooProfileLL("pll_N_etac2s", "pll_N_etac2s", nll, RooArgSet(poi_var))
    llhood = RooFormulaVar("llhood","exp(-0.5*pll_N_etac2s)",RooArgList(pll_N_etac2s));

    # Plot the profile likelihood in frac
    pll_N_etac2s.plotOn(frameRS,RooFit.LineColor(2))

    # Adjust frame maximum for visual clarity
    frameRS.SetMinimum(0)
    frameRS.GetXaxis().SetRangeUser(poimin, poimax)

    cNLL = TCanvas("rf605_profilell","rf605_profilell",800, 400)
    cNLL.cd(),  gPad.SetLeftMargin(0.15),  frameRS.GetYaxis().SetTitleOffset(1.4),  frameRS.Draw()

    cNLL.SaveAs(homeDir + "/mass_fit/NLL_{}_{}_{}_base_mConstr.pdf".format(kSource,kBkg,kNorm))

    nll.Write()
    pll_N_etac2s.Write()


    fllhood = llhood.asTF(RooArgList(paramOfInterest))
    npoints = 50
    int_array = np.ones(npoints+1)
    int_val0 = fllhood.Integral(poimin, poimax, 1e-6)
    print(int_val0)
    poi_urs = np.linspace(poimin, poimax, npoints+1)

    for idx in range(npoints):

        int_val = fllhood.Integral(poimin, poi_urs[idx+1], 1e-6)
        int_array[idx+1] = 1.-int_val/int_val0
        if int_array[idx+1]<0.05: print(poi_urs[idx+1])

    print(int_array)
    gr_pval = TGraph(npoints+1, poi_urs, int_array)
    c.cd()
    gr_pval.SetLineColor(2)
    gr_pval.GetXaxis().SetRangeUser(poimin, poimax)
    gr_pval.Draw("same")

    c.SaveAs(homeDir + "/mass_fit/CL_Ac_check_{}_{}_{}_base_mConstr.pdf".format(kSource,kBkg,kNorm))
    c.SaveAs(homeDir + "/mass_fit/CL_Ac_check_{}_{}_{}_base_mConstr.root".format(kSource,kBkg,kNorm))


    from ROOT.RooStats import BayesianCalculator, ProfileLikelihoodCalculator

    #simPdf = w.pdf("simPdf")

    ##--Bayesean Calculator--
    #POI_Bayes = RooArgSet(poi_var)                                                              #Set BR as POI, option for other POIs
    #flat_prior = RooUniform("flat_prior","Flat Prior", RooArgSet(poi_var))                      #Make flat prior in BR
    #bcalc = BayesianCalculator(combData, simPdf, POI_Bayes, flat_prior,constrainedParams)       #Call BayesianCalculator, create object
    ##bcalc = BayesianCalculator(combData, sbModel)                                              #Call BayesianCalculator, create object
    ##bcalc.ForceNuisancePdf (simPdf)
    #bcalc.SetConfidenceLevel(.95)                                                               #Set 95% CL
    #bcalc.SetLeftSideTailFraction(0)                                                            #Make one-sided upper limit
    #interval = bcalc.GetInterval()                                                              #Get the interval SimpleInterval*

    #print("95\% CL interval: [ ", interval.LowerLimit(), " - ", interval.UpperLimit(), " ]\n")  #Read out bounds of interval

    #UL_Bayes = interval.UpperLimit()                                                            #Read out upper limit
    #plot = bcalc.GetPosteriorPlot()                                                             #Make plot of likelihood and integral region. RooPlot *
    #canvas4 = TCanvas("canvas4", "Fourth canvas", 1200, 1000)
    #canvas4.cd(1)
    #plot.Draw()
    #canvas4.SaveAs("A0mass_BayesPlot.png")

    #---PLC Upper Limit---#
    POI_PLC_UL = RooArgSet(poi_var)
    plc_UL = ProfileLikelihoodCalculator(combData, simPdf, POI_PLC_UL)   #Call to PLC with new range on nsig
    plc_UL.SetConfidenceLevel(0.95)
    plInt_UL = plc_UL.GetInterval()                 #LikelihoodInterval*
    #UL_PLC = plInt_UL.UpperLimit(nsig)
    UL_PLC = plInt_UL.UpperLimit(poi_var)

    print("UL_PLC = ",UL_PLC)



if __name__ == "__main__":

    #kBkgs = ["exp","cheb"]
    kBkgs = ["cheb"]
    #kSources = ["fromB","prompt"]
    kSources = ["fromB"]
    #kSources = ["prompt"]
    kNorm = 'psi2S'
    for kBkg in kBkgs:
        for kSource in kSources:
            w = perform_fit_simult(kSource, kBkg, kNorm)
            # nameWksp = homeDir + "/mass_fit/FitMass_wksp_{}_{}_{}_base_mConstr.root".format(kNorm,kSource, kBkg)
            # file_w = TFile(nameWksp)
            # w = file_w.Get("w")
            #print(w.pdf("simPdf").indexCat())
            # calcUL(w,kSource,kBkg,kNorm)
            #calcUL_HighRangeOnly(w,kSource,kBkg)
            del w

