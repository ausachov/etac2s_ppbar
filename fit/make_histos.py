from ROOT import *
from array import array
import numpy as np
from math import log
import os 
from ROOT import RDataFrame
from ROOT.RDF import TH1DModel


homeDir = '/afs/cern.ch/work/v/vazhovko/private/results/etac2s_ppbar/'
#dataDir = '/eos/user/v/vazhovko/etac2s_ppbar/'
dataDir = '/sps/lhcb/zhovkovska/etac2s_ppbar/'


minM_Low = 2850
maxM_Low = 3250

minM_High = 3320
maxM_High = 3780

minMass_etac2s  = 2850
#minMass_etac2s  = 3300
maxMass_etac2s  = 3250
binWidth = 0.2
binN = int((maxMass_etac2s-minMass_etac2s)/binWidth)

binN_Low  = int((maxM_Low-minM_Low)/binWidth)
binN_High = int((maxM_High-minM_High)/binWidth)


minMassPhi = 1000.
maxMassPhi = 1040.
binWidthPhi = 0.2
binWidthPhiDraw = 1.
binNPhi      = int((maxMassPhi-minMassPhi)/binWidthPhi)
plot_binPhiN = int((maxMassPhi-minMassPhi)/binWidthPhiDraw)


# var_list = [
#             'Jpsi_M', 'Jpsi_MM','Jpsi_ENDVERTEX_CHI2','Jpsi_ENDVERTEX_Z','Jpsi_OWNPV_Z','Jpsi_PT','Jpsi_FDCHI2_OWNPV',
#             'Jpsi_PE','Jpsi_PZ','Jpsi_P',
#             'ProtonP_PT','ProtonM_PT',
#             #'ProtonP_TRACK_CHI2NDOF','ProtonM_TRACK_CHI2NDOF',
#             'ProtonP_ProbNNp','ProtonM_ProbNNp',
#             'ProtonP_PIDK', 'ProtonP_PIDp', 'ProtonM_PIDK', 'ProtonM_PIDp',
#             'ProtonP_IPCHI2_OWNPV', 'ProtonM_IPCHI2_OWNPV',
#             #'ProtonP_TRACK_GhostProb', 'ProtonM_TRACK_GhostProb', #'ProtonP_TRACK_CloneDist', 'ProtonM_TRACK_CloneDist',

#             ]

var_list = [
            'Jpsi_m_scaled', 'Jpsi_MM','Jpsi_ENDVERTEX_Z','Jpsi_OWNPV_Z','Jpsi_PT','Jpsi_FDCHI2_OWNPV',
            'Jpsi_PZ',#'Jpsi_P','Jpsi_PE','Jpsi_ENDVERTEX_CHI2','Jpsi_M',
            #'ProtonP_TRACK_CHI2NDOF','ProtonM_TRACK_CHI2NDOF',
            'ProtonP_ProbNNp','ProtonM_ProbNNp',
            'ProtonP_ProbNNpi','ProtonM_ProbNNpi',
            'ProtonP_ProbNNk','ProtonM_ProbNNk',
            # 'ProtonP_PIDK', 'ProtonP_PIDp', 'ProtonM_PIDK', 'ProtonM_PIDp',
            'ProtonP_IPCHI2_OWNPV', 'ProtonM_IPCHI2_OWNPV',
            #'ProtonP_TRACK_GhostProb', 'ProtonM_TRACK_GhostProb', #'ProtonP_TRACK_CloneDist', 'ProtonM_TRACK_CloneDist',

            ]

link_list = {
                'Jpsi_PT'             : ['Jpsi_PT'],
                'Proton_PT'           : ['ProtonP_PT','ProtonM_PT'],
                'Proton_ProbNNp'      : ['ProtonP_ProbNNp','ProtonM_ProbNNp'],
                'Proton_ProbNNk'      : ['ProtonP_ProbNNk','ProtonM_ProbNNk'],
                'Proton_ProbNNpi'     : ['ProtonP_ProbNNpi','ProtonM_ProbNNpi'],
                'Proton_PIDp'         : ['ProtonP_PIDp','ProtonM_PIDp'],
                'Proton_PIDpK'        : ['ProtonP_PIDp-ProtonP_PIDK', 'ProtonM_PIDp-ProtonM_PIDK']

               }

opt_var_list = [
                #'Jpsi_PT',
                #'Proton_PT',
                'Proton_ProbNNp',
                # 'Proton_ProbNNpi',
                # 'Proton_ProbNNk',
                #'Proton_PIDp',
                #'Proton_PIDpK'
               ]

opt_var_list_2d = [
                # ['Proton_PIDp','Proton_PIDdiff'],
                ['Proton_ProbNNp','Proton_ProbNNk'],
                ['Proton_ProbNNp','Proton_ProbNNpi'],
                ['Proton_ProbNNpi','Proton_ProbNNk'],
               ]



cuts = {'Jpsi_PT':              ['>', np.arange(5000., 10000., 500)],
        'Proton_PT':            ['>', np.arange(2000., 11000., 1000)],

        # 'Proton_ProbNNp':       ['>', np.append(np.arange(0.4, 0.9, 0.10), np.arange(0.85, 0.99, 0.05))],
        # 'Proton_ProbNNk':       ['<', np.arange(0.1, 1.0, 0.1)],
        # 'Proton_ProbNNpi':      ['<', np.arange(0.1, 0.7, 0.1)],

        'Proton_ProbNNp':       ['>', np.arange(0.0, 1.0, 0.1)],
        'Proton_ProbNNpi':      ['<', np.arange(0.1, 1.01, 0.1)],
        'Proton_ProbNNk':       ['<', np.arange(0.1, 1.01, 0.1)],
        # 'Proton_ProbNNp':       ['>', np.append(np.arange(0.4, 1.0, 0.2), np.arange(0.85, 1.0, 0.05))],
        'Proton_PIDp':          ['>', np.arange(0, 30, 2)],
        'Proton_PIDpK':         ['>', np.arange(5, 25, 2)],

       }


def get_data_s(kRange, kSource, dim=1):

    ROOT.EnableImplicitMT()

    dirName = dataDir +"Data_{}_NoPID/".format(kRange)

    nt = TChain("DecayTree")

    nt.Add(dirName +'/Etac2sDiProton_{}_2018*.root'.format(kRange))

    nt.SetBranchStatus("*",0)
    for var in var_list:
        nt.SetBranchStatus("{}".format(var),1);


    df = RDataFrame(nt)
    # cut_Protons = TCut('ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
    #                     ProtonP_PT>2000 && ProtonM_PT>2000 && \
    #                     ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366 && \
    #                     Jpsi_PT > 5000 && Jpsi_PT < 20000')
    cut_Protons = TCut('Jpsi_PT > 5000 && Jpsi_PT < 20000')

    if kSource=='prompt':
        cut_FD  = TCut('(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08')# & Jpsi_FDCHI2_OWNPV > 49'
    else:
        cut_FD  = TCut('(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16')# & Jpsi_FDCHI2_OWNPV > 49')


    #tree_base = TTree()
    #tree_base = nt.CopyTree(cut_FD.GetTitle())

    #Jpsi_ENDVERTEX_Z = array( 'd', [0])
    #Jpsi_OWNPV_Z     = array( 'd', [0])
    #Jpsi_MM          = array( 'd', [0])
    #Jpsi_PZ          = array( 'd', [0])
    #Jpsi_PE          = array( 'd', [0])
    #Jpsi_Tz          = array( 'd', [0])
    #Jpsi_Y           = array( 'd', [0])

    #tree_base.SetBranchAddress("Jpsi_ENDVERTEX_Z",Jpsi_ENDVERTEX_Z)
    #tree_base.SetBranchAddress("Jpsi_OWNPV_Z",    Jpsi_OWNPV_Z)
    #tree_base.SetBranchAddress("Jpsi_MM",         Jpsi_MM)
    #tree_base.SetBranchAddress("Jpsi_PZ",         Jpsi_PZ)
    #tree_base.SetBranchAddress("Jpsi_PE",         Jpsi_PE)

    #Jpsi_Tz_Branch    = tree_base.Branch("Jpsi_Tz",  Jpsi_Tz,   "Jpsi_Tz/D")
    #Jpsi_Y_Branch     = tree_base.Branch("Jpsi_Y",   Jpsi_Y,    "Jpsi_Y/D")

    #print("Base tree is creating")

    #nEntries = tree_base.GetEntries()
    #for iEn in range(nEntries):
        #if iEn%1000==0: print(iEn)
        #tree_base.GetEntry(iEn)
        #Jpsi_Tz[0] = 3.3*(Jpsi_ENDVERTEX_Z[0] - Jpsi_OWNPV_Z[0])*Jpsi_MM[0]/Jpsi_PZ[0]
        #Jpsi_Y[0]  = 0.5*log((Jpsi_PE[0] + Jpsi_PZ[0])/(Jpsi_PE[0] - Jpsi_PZ[0]))
        #Jpsi_Tz_Branch.Fill()
        #Jpsi_Y_Branch.Fill()

    print("Base tree was created")

    if dim==1:

        for var_opt in opt_var_list:

            print('cutting '+var_opt)
            finalDataDir = '{}/Data_{}'.format(dataDir,var_opt)

            len_x = cuts[var_opt][1].size

            try:
                os.system('mkdir ' + finalDataDir)
                os.system('mkdir {}/{}'.format(finalDataDir,kSource))
            except:
                pass

            finalDataDir += "/" + kSource

            for idx in range(0, len_x):
                cut_val = cuts[var_opt][1][idx]
            #for cut_val in cuts[var_opt][1]:

                #idx = list(cuts[var_opt][1]).index(cut_val)

                newfile = TFile(finalDataDir+"/Etac2sDiProton_{}_2018_Bin_{}_PT1.root".format(kRange, idx+1),"recreate")
                #Jpsi_M = TH1D('Jpsi_M','Jpsi_M', 1600, minMass_etac2s, maxMass_etac2s)
                histModel = TH1DModel()
                if kRange == 'Low':
                    histModel = TH1DModel('Jpsi_M','Jpsi_M', binN_Low, minM_Low, maxM_Low)
                else:
                    histModel = TH1DModel('Jpsi_M','Jpsi_M', binN_High, minM_High, maxM_High)

                # cut_string = ''
                cut_string = 'Jpsi_PT>5000. && Jpsi_PT<6500'

                for var_name in link_list[var_opt]:
                    if cut_string == '':
                        cut_string += '{} {} {:.2f}'.format(var_name,cuts[var_opt][0],cut_val)
                    else:
                        cut_string += ' && {} {} {:.2f}'.format(var_name,cuts[var_opt][0],cut_val)

                cut_opt = TCut(cut_string)
                totCut  = TCut(cut_opt + cut_FD)

                Jpsi_M = df.Filter(totCut.GetTitle()).Histo1D(histModel,"Jpsi_m_scaled") # this fills the histogram of x in parallel, leveraging the aforementioned pool
                Jpsi_M.Draw()
                # input()
                # nt.Draw('Jpsi_m_scaled>>Jpsi_M',totCut.GetTitle(),'goff')
                #tree_base.Draw('Jpsi_M>>Jpsi_M',totCut.GetTitle(),'goff')
                #tree    = tree_base.CopyTree(totCut.GetTitle())

                Jpsi_M.Write()
                newfile.Write()
                newfile.Close()

                print('Bin {} is ready'.format(idx+1))
                del newfile,#tree
    elif dim==2:
        for var_opt in opt_var_list_2d:

            print('cutting {} and {}'.format(var_opt[0], var_opt[1]))
            finalDataDir = '{}/Data_{}_{}'.format(dataDir,var_opt[0],var_opt[1])

            try:
                os.system('mkdir {}'.format(finalDataDir))
                os.system('mkdir {}/{}'.format(finalDataDir,kSource))
            except:
                pass

            finalDataDir += "/"+kSource

            len_x = cuts[var_opt[0]][1].size
            len_y = cuts[var_opt[1]][1].size


            for idx in range(0, len_x):
                cut_val_x = cuts[var_opt[0]][1][idx]
                for idy in range(0, len_y):
                    cut_val_y = cuts[var_opt[1]][1][idy]


                    newfile = TFile(finalDataDir+"/Etac2sDiProton_{}_2018_Bin_{}_{}.root".format(kRange, idx+1, idy+1),"recreate")
                    histModel = TH1DModel()
                    if kRange == 'Low':
                        histModel = TH1DModel('Jpsi_M','Jpsi_M', binN_Low, minM_Low, maxM_Low)
                    else:
                        histModel = TH1DModel('Jpsi_M','Jpsi_M', binN_High, minM_High, maxM_High)

                    cut_string = '1'

                    for var_name in link_list[var_opt[0]]:
                        cut_string += ' && {} {} {:.2f}'.format(var_name,cuts[var_opt[0]][0],cut_val_x)

                    for var_name in link_list[var_opt[1]]:
                        cut_string += ' && {} {} {:.2f}'.format(var_name,cuts[var_opt[1]][0],cut_val_y)

                    cut_opt = TCut(cut_string)
                    totCut  = TCut(cut_opt + cut_FD)

                    Jpsi_M = df.Filter(totCut.GetTitle()).Histo1D(histModel,"Jpsi_m_scaled") # this fills the histogram of x in parallel, leveraging the aforementioned pool
                    Jpsi_M.Draw("goff")

                    Jpsi_M.Write()
                    newfile.Write()
                    newfile.Close()

                    print('Bin [{} {}] is ready: \n {}'.format(idx+1, idy+1, cut_string))

                    del newfile

    del nt#, tree_base

    print("Histogram was created successfully")


def get_cut_s(kRange, kSource, dim=1):

    cut_Protons = TCut('Jpsi_PT > 5000 && Jpsi_PT < 20000')

    if kSource=='prompt':
        cut_FD  = TCut('(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08')# & Jpsi_FDCHI2_OWNPV > 49'
    else:
        cut_FD  = TCut('(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16')# & Jpsi_FDCHI2_OWNPV > 49')


    if dim==1:

        for var_opt in opt_var_list:

            print('cutting '+var_opt)
            len_x = cuts[var_opt][1].size

            for idx in range(0, len_x):
                cut_val = cuts[var_opt][1][idx]
                cut_string = ''

                for var_name in link_list[var_opt]:
                    if cut_string == '':
                        cut_string += '{} {} {:.2f}'.format(var_name,cuts[var_opt][0],cut_val)
                    else:
                        cut_string += ' && {} {} {:.2f}'.format(var_name,cuts[var_opt][0],cut_val)

                print('Bin {} is ready: \n {}'.format(idx+1, cut_string))
                del newfile,#tree
    elif dim==2:
        for var_opt in opt_var_list_2d:

            print('cutting {} and {}'.format(var_opt[0], var_opt[1]))

            len_x = cuts[var_opt[0]][1].size
            len_y = cuts[var_opt[1]][1].size


            for idx in range(0, len_x):
                cut_val_x = cuts[var_opt[0]][1][idx]
                for idy in range(0, len_y):
                    cut_val_y = cuts[var_opt[1]][1][idy]

                    cut_string = '1'

                    for var_name in link_list[var_opt[0]]:
                        cut_string += ' && {} {} {:.2f}'.format(var_name,cuts[var_opt[0]][0],cut_val_x)

                    for var_name in link_list[var_opt[1]]:
                        cut_string += ' && {} {} {:.2f}'.format(var_name,cuts[var_opt[1]][0],cut_val_y)

                    print('Bin [{} {}] is ready: \n {}'.format(idx+1, idy+1, cut_string))


    del nt#, tree_base

    print("Histogram was created successfully")



if __name__ == '__main__':
    '''
    Creates TH1D with Jpsi_m_scaled in bins of optimization variables defined above
    Uses RDataFrame to parallel filling of histograms (~10x speed up)
    '''

    kSources = ['fromB','prompt']
    # kSources = ['fromB']
    kRanges  = ['Low', "High"]
    # kRanges  = ['High']

    for kSource in kSources:
        for kRange in kRanges:
            get_data_s( kRange, kSource, dim=1)
            # get_data_s( kRange, kSource, dim=2)
            # get_cut_s( kRange, kSource, dim=2)

    # kRange = "High"
    # kSource = "prompt"
    # get_data_s(kRange, kSource, dim=2)
