from ROOT import TTree, TChain, TH1F, TCanvas

homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"


def promptImp(ccbar, nPT):

    nt = TChain('DecayTree')
    nt.Add('{}/MC/Trigger/{}DiProton_*_2018_AddBr.root'.format(dataDir,ccbar))

    tree_prompt = TTree()
    tree_fromB = TTree()

    tree_prompt = nt.CopyTree("Jpsi_prompt")
    tree_fromB = nt.CopyTree("Jpsi_sec")

    N_prompt = tree_prompt.GetEntries()
    N_fromB = tree_fromB.GetEntries()

    cut_Tz_Sec = 'Jpsi_Tz > 0.08'
    cut_Tz_Prompt = 'Jpsi_Tz < 0.08'
    # cut_IP_CHI2 = '((ProtonP_IPCHI2_OWNPV > 16) && (ProtonM_IPCHI2_OWNPV > 16))'
    cut_IP_CHI2 = '1'

    pt = [5000., 6500., 8000., 10000., 12000., 14000., 20000.]
    if nPT==0:
       cutPT = "Jpsi_PT>{} && Jpsi_PT<{}".format(pt[0], pt[-1])
    else:
       cutPT = "Jpsi_PT>{} && Jpsi_PT<{}".format(pt[nPT-1], pt[nPT])


    N_PP = tree_prompt.GetEntries(cutPT + " && " + cut_Tz_Prompt)
    N_PB = tree_prompt.GetEntries(cutPT + " && " + cut_Tz_Sec + "&&" + cut_IP_CHI2)
    N_BB = tree_fromB.GetEntries(cutPT + " && " + cut_Tz_Sec + "&&" + cut_IP_CHI2)
    N_BP = tree_fromB.GetEntries(cutPT + " && " + cut_Tz_Prompt)


    effPP = float(N_PP/float(N_prompt))
    effBB = float(N_BB/float(N_fromB))
    effPB = float(N_PB/float(N_prompt))
    effBP = float(N_BP/float(N_fromB))


    effPP_err = effPP*(1/float(N_PP)+1/float(N_prompt))**0.5
    if N_PB!=0:
       effPB_err = effPB*(1/float(N_PB)+1/float(N_prompt))**0.5
    else:
       effPB_err = effPB*2
    effBB_err = effBB*(1/float(N_BB)+1/float(N_fromB))**0.5
    effBP_err = effBP*(1/float(N_BP)+1/float(N_fromB))**0.5

    return effPP, effPB, effBP, effBB, effPP_err, effPB_err, effBP_err, effBB_err

from array import array
nPTBins = 6
ptBinning = array("d", [5.0, 6.5, 8.0, 10.0, 12.0, 14.0, 20.0])

def createHisto(hName = "", vals=[], errors=[]):
    hist = TH1F(hName,hName,nPTBins,ptBinning)
    # hist.SetContent(vals)
    # hist.SetError(errors)
    for ii in range(nPTBins):
        hist.SetBinContent(ii+1, vals[ii])
        hist.SetBinError(ii+1, errors[ii])
    return hist


#f = open('eff_file.txt', 'r')
def get_eff(ccbar):

    effPP_list = array("d", [])
    effPB_list = array("d", [])
    effBP_list = array("d", [])
    effBB_list = array("d", [])

    effPP_list_err = array("d", [])
    effPB_list_err = array("d", [])
    effBP_list_err = array("d", [])
    effBB_list_err = array("d", [])

    for iPT in range(1, nPTBins+1):
        effPP, effPB, effBP, effBB, effPP_err, effPB_err, effBP_err, effBB_err = promptImp(ccbar, iPT)
        effPP_list.append(effPP)
        effPB_list.append(effPB)
        effBP_list.append(effBP)
        effBB_list.append(effBB)

        effPP_list_err.append(effPP_err)
        effPB_list_err.append(effPB_err)
        effBP_list_err.append(effBP_err)
        effBB_list_err.append(effBB_err)

    h_PP = createHisto("PP",effPP_list,effPP_list_err)
    h_PB = createHisto("PB",effPB_list,effPB_list_err)
    h_BP = createHisto("BP",effBP_list,effBP_list_err)
    h_BB = createHisto("BB",effBB_list,effBB_list_err)


    c = TCanvas("c1","c1", 1000, 700)
    c.Divide(2, 2, 0.001, 0.001)
    h_PP.Draw()
    c.SaveAs("h_{}_PP.pdf".format(ccbar))

    h_PB.Draw("")
    c.SaveAs("h_{}_PB.pdf".format(ccbar))

    h_BP.Draw("")
    c.SaveAs("h_{}_BP.pdf".format(ccbar))

    h_BB.Draw("")
    c.SaveAs("h_{}_BB.pdf".format(ccbar))


if __name__ == "__main__":

    import sys
    orig_stdout = sys.stdout

    # ccbar_list = ["etac","etac2S","jpsi","chic0","chic1","chic2","psi2S"]
    ccbar_list = ["etac","jpsi"]

    for ccbar in ccbar_list:
    #   with open('{}/MC/cross_talks/cross_talk_{}.txt'.format(homeDir,ccbar), 'w') as f:
      with open('{}/MC/cross_talks/cross_talk_{}_noIP.txt'.format(homeDir,ccbar), 'w') as f:
        # f = open('cross_talk_{}.txt'.format(ccbar), 'w')
        # sys.stdout = f

        # for iPT in range(0, 7):
        for iPT in range(0, 1):
            effPP, effPB, effBP, effBB, effPP_err, effPB_err, effBP_err, effBB_err = promptImp(ccbar, iPT)
            f.write(f"{iPT}  {effPP:.4f} {effPB:.4f} {effBP:.4f} {effBB:.4f}   {effPP_err:.4f} {effPB_err:.4f} {effBP_err:.4f} {effBB_err:.4f} \n")
            #   print("{}  {:.4f} {:.4f}   {:.4f} {:.4f}   {:.4f} {:.4f}   {:.4f} {:.4f} ".format(iPT, effPP, effPB, effBP, effBB, effPP_err, effPB_err, effBP_err, effBB_err))
        # effPP, effPB, effBP, effBB, effPP_err, effPB_err, effBP_err, effBB_err = promptImp(ccbar, 0)
        # print(f"{ccbar} & {effPP:.4f} $\pm$ {effPP_err:.4f}  & {effPB:.4f} $\pm$ {effPB_err:.4f} &  {effBP:.4f} $\pm$ {effBP_err:.4f}  & {effBB:.4f} $\pm$ {effBB_err:.4f} \\\\")
        

        # sys.stdout = orig_stdout
        # f.close()
        # get_eff(ccbar)
        print("{} DONE".format(ccbar))  
