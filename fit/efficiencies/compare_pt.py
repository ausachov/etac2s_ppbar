from ROOT import TH1D, TChain, TFile, TCanvas, TLegend, gROOT
from array import array

homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/mass_fit"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"
gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")

from fit.efficiencies.efficiency import cut_accept, cut_true_v, cut_select, cut_PID

def compare_pt():

    n_bins = 6
    bins = array("f",[5000., 6500, 8000., 10000., 12000., 14000., 20000.])

    h_data_p = TH1D("h_data_p","h_data_p",n_bins, bins)
    h_data_b = TH1D("h_data_b","h_data_b",n_bins, bins)
    h_MC_p   = TH1D("h_MC_p","h_MC_p",n_bins, bins)
    h_MC_b   = TH1D("h_MC_b","h_MC_b",n_bins, bins)

    cut = "{} && {} && {} && {}".format(cut_accept, cut_true_v, cut_select, cut_PID)

    for iPT in range(1, 7):    
        nameWksp    = homeDir + "/etac/Wksp_MassFit_PT%s_C%s.root"%(iPT,"Base")
        file_w = TFile(nameWksp,"READ")
        w = file_w.Get("w")
        # nJpsi_Pr = w.var("nJpsi_PT%s_Prompt"%(nPT)).getValV()
        # nJpsi_FrB = w.var("nJpsi_PT%s_FromB"%(nPT)).getValV()
        nEtac_Pr = w.var("nEta_PT%s_Prompt"%(iPT)).getValV()
        nEtac_Fb = w.var("nEta_PT%s_FromB"%(iPT)).getValV()

        # nJpsi_Pr_Err  = w.var("nJpsi_PT%s_Prompt"%(nPT)).getError()
        # nJpsi_FrB_Err = w.var("nJpsi_PT%s_FromB"%(nPT)).getError()
        nEtac_Pr_Err  = w.var("nEta_PT%s_Prompt"%(iPT)).getError()
        nEtac_Fb_Err = w.var("nEta_PT%s_FromB"%(iPT)).getError()

        h_data_p.SetBinContent(iPT, nEtac_Pr)
        h_data_p.SetBinError(iPT, nEtac_Pr_Err)

        h_data_b.SetBinContent(iPT, nEtac_Fb)
        h_data_b.SetBinError(iPT, nEtac_Fb_Err)

        file_w.Close()

        nameMC = dataDir + "/MC/Trigger/etacDiProton_*_2018_AddBr.root"
        # file_t = TFile(nameMC,"READ")
        nt = TChain("DecayTree")
        nt.Add(nameMC)

        nt.Draw("Jpsi_PT>>h_MC_p","{} && prompt".format(cut))
        nt.Draw("Jpsi_PT>>h_MC_b","{} && sec".format(cut))

    for h in [h_data_p, h_data_b, h_MC_p, h_MC_b]:
        h.SetDirectory(0)
        h.Scale(1./h.Integral())

    c = TCanvas()
    h_data_p.Draw("E1")
    h_MC_p.SetLineColor(2)
    h_MC_p.Draw("E1 same")

    leg1 = TLegend(0.70,0.57,0.90,0.75)
    leg1.SetBorderSize(0)
    leg1.AddEntry(h_data_p,"data","lep")
    leg1.AddEntry(h_MC_p,"MC","lep")
    leg1.Draw()

    c.SaveAs("etac_pt_prompt.pdf")

    c = TCanvas()
    h_data_b.Draw("E1")
    h_MC_b.SetLineColor(2)
    h_MC_b.Draw("E1 same")

    leg2 = TLegend(0.70,0.57,0.90,0.75)
    leg2.SetBorderSize(0)
    leg2.AddEntry(h_data_b,"data","lep")
    leg2.AddEntry(h_MC_b,"MC","lep")
    leg2.Draw()
    c.SaveAs("etac_pt_fromb.pdf")

if __name__ == "__main__":

    gROOT.LoadMacro("../lhcbStyle.C")
    compare_pt()

