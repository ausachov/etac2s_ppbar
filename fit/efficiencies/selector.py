from ROOT import TFile, TChain, TCut, TH1D
from ROOT.TMath import Abs
from array import array

dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"
#dataDir = "/eos/user/v/vazhovko/etac2s_ppbar/"

pid = {
    #"etac":   443,
    "etac":   441,
    "jpsi":   443,
    "etac2S": 100441,
    #"etac2S": 100443,
    "chic0":  10441,
    "chic1":  20443,
    "chic2":  445,
    "psi2S":  100443,
    "proton": 2212

}
cut_L0 = TCut('Jpsi_L0HadronDecision_TOS')
cut_Hlt = TCut('Jpsi_Hlt1DiProtonDecision_TOS')# && Jpsi_Hlt2CcDiHadronDiPhiDecision_TOS')

cut_Jpsi = TCut('Jpsi_ENDVERTEX_CHI2<4.0')
cut_Jpsi_Y = TCut('Jpsi_Y > 2 && Jpsi_Y < 4.5')
#cut_Jpsi_Y = TCut('0.5*log((Jpsi_PE + Jpsi_PZ)/(Jpsi_PE-Jpsi_PZ)) > 2 && 0.5*log((Jpsi_PE + Jpsi_PZ)/(Jpsi_PE-Jpsi_PZ)) < 4.5')

cut_FD = TCut('(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08')
#cut_FD = TCut('(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16')


cut_Protons = TCut('ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
                    ProtonP_PT>2000 && ProtonM_PT>2000 && \
                    ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366 && \
                    ProtonP_TRACK_CHI2NDOF<2.5 && ProtonM_TRACK_CHI2NDOF<2.5 && \
                    ProtonP_TRACK_CloneDist<0 && ProtonM_TRACK_CloneDist<0 && \
                    ProtonP_PIDp>5 && ProtonM_PIDp>5 && \
                    (ProtonP_PIDp-ProtonP_PIDK)>0 && (ProtonM_PIDp-ProtonM_PIDK)>0 && \
                    ProtonP_TRACK_GhostProb<0.2 && ProtonM_TRACK_GhostProb<0.2 \
         ')


optVar_list  = ['Jpsi_M', 'Jpsi_m_scaled', 'Jpsi_MM','Jpsi_ENDVERTEX_CHI2','Jpsi_ENDVERTEX_Z','Jpsi_OWNPV_Z','Jpsi_PT','Jpsi_FDCHI2_OWNPV',
                'Jpsi_PE','Jpsi_PZ','Jpsi_P',
                # 'Jpsi_ETA',
                'Jpsi_Y',
                'Jpsi_L0HadronDecision_TOS', 'Jpsi_Hlt1DiProtonDecision_TOS',
                'ProtonP_P', 'ProtonP_PT','ProtonM_P','ProtonM_PT',
                'ProtonP_TRACK_CHI2NDOF','ProtonM_TRACK_CHI2NDOF',
                'ProtonP_ProbNNp','ProtonM_ProbNNp',
                'ProtonP_ProbNNpi','ProtonM_ProbNNpi',
                'ProtonP_ProbNNk','ProtonM_ProbNNk',
                'ProtonP_PIDK', 'ProtonP_PIDp', 'ProtonM_PIDK', 'ProtonM_PIDp',
                'ProtonP_IPCHI2_OWNPV', 'ProtonM_IPCHI2_OWNPV',
                'ProtonP_TRACK_GhostProb', 'ProtonM_TRACK_GhostProb', 'ProtonP_TRACK_CloneDist', 'ProtonM_TRACK_CloneDist',
                'Jpsi_M01_Subst0_p2pi','Jpsi_M01_Subst1_p2pi',
                'Jpsi_M01_Subst0_p2K','Jpsi_M01_Subst1_p2K',
                'Jpsi_M01_Subst01_pp~2KK','Jpsi_M01_Subst01_pp~2Kpi','Jpsi_M01_Subst01_pp~2piK','Jpsi_M01_Subst01_pp~2pipi',
]


def calc_eff(kSource, kRange):

    #gROOT.Reset()
    f = TFile("{}/MC/Trigger/{}DiProton_{}_2018_AddBr.root".format(dataDir, kRange, kSource))
    t = f.Get("DecayTree")
    #nt = TChain("DecayTree")
    #nt.Add("{}/MC/Trigger/{}DiProton_{}_Mag{}_2018_AddBr.root".format(dataDir, kRange, kSource, kMag))

    #new branches
    eff_total     = array( "d", [0])
    eff_pid       = array( "d", [0])
    eff_trigger   = array( "d", [0])
    eff_selection = array( "d", [0])
    eff_accept    = array( "d", [0])


    #nScaledTracks = array( "i", [0])



    f_new = TFile("{}/MC/Trigger/{}DiProton_{}_2018_eff.root".format(dataDir, kRange, kSource), "RECREATE")

    tree_new = t.CloneTree(0)
    #tree_new.Branch("eff_total",eff_total,"eff_total/D")
    tree_new.Branch("eff_trigger",eff_trigger,"eff_trigger/D")
    tree_new.Branch("eff_selection",eff_selection,"eff_selection/D")
    tree_new.Branch("eff_pid",eff_pid,"eff_pid/D")
    tree_new.Branch("eff_accept",eff_accept,"eff_accept/D")

    #t.Branch("nScaledTracks",nScaledTracks,"nScaledTracks/I")

    nEntries = t.GetEntries()

    print("Tree is created.......................................... \n")

    for event in t:

        eff_trigger[0]   = 0.
        eff_pid[0]       = 0.
        eff_selection[0] = 0.
        eff_accept[0]   = 0.

        if (t.Jpsi_Y > 2 and t.Jpsi_Y < 4.5):
            eff_accept[0]   = 1.

        #if t.Jpsi_L0HadronDecision_TOS and (t.Jpsi_Hlt1DiProtonDecision_TOS or t.Jpsi_Hlt1DiProtonHighDecision_TOS):
        #if t.Jpsi_L0HadronDecision_TOS and t.Jpsi_Hlt1DiProtonDecision_TOS:   #for etac and jpsi
        if t.Jpsi_L0HadronDecision_TOS and t.Jpsi_Hlt1DiProtonHighDecision_TOS:   #for etac2S and psi2S
                eff_trigger[0] = 1.

        if (t.ProtonM_ProbNNp  > 0.6 and t.ProtonP_ProbNNp  > 0.6) \
           and (t.ProtonM_ProbNNpi < 0.6 and t.ProtonP_ProbNNpi < 0.6) \
           and (t.ProtonM_ProbNNk  < 0.6 and t.ProtonP_ProbNNk  < 0.6):
           #and (t.ProtonM_PIDp - t.ProtonM_PIDK  > 0 and t.ProtonP_PIDp - t.ProtonP_PIDK  > 0) \
           #and (t.ProtonM_PIDp  > 5 and t.ProtonP_PIDp  > 5) \
                eff_pid[0] = 1.

        if (eff_accept[0] == 1) \
           and (t.ProtonM_P > 12500. and t.ProtonP_P > 12500.) \
           and (t.ProtonM_PT > 2000. and t.ProtonP_PT > 2000.) \
           and (t.ProtonM_PT/t.ProtonM_P > 0.0366 and t.ProtonP_PT/t.ProtonP_P > 0.0366) \
           and (t.ProtonM_TRACK_CHI2NDOF < 2.5 and t.ProtonP_TRACK_CHI2NDOF < 2.5) \
           and (t.ProtonM_TRACK_GhostProb < 0.2 and t.ProtonP_TRACK_GhostProb < 0.2) \
           and (t.ProtonM_TRACK_CloneDist < 0.0 and t.ProtonP_TRACK_CloneDist < 0.0) \
           and (t.Jpsi_PT > 5000. and t.Jpsi_ENDVERTEX_CHI2 < 4.0) \
           and (t.nSPDHits < 300. ) :
                eff_selection[0] = 1.

        tree_new.Fill()


    tree_new.Write()

    f_new.Close()
    print(kRange, kSource)
    del tree_new, t




#    branch name                        [n_bins, xmin, xmax]
binning = {
        "Jpsi_Y":                            [6, 1.5, 0.5],
        "Jpsi_L0HadronDecision_TOS":         [2,  0.0, 1.],
        "Jpsi_Hlt1DiProtonDecision_TOS":     [2,  0.0, 1.],
        # "Jpsi_PT":                           [36, 4000., 1000.],
        "Jpsi_TRUEPT":                       [5000., 5750., 6500, 7250., 8000., 8500., 9000., 9500., 10000., 10500., 11000., 12000., 13000., 14000., 16000., 18000., 20000., 60000.],
        "Jpsi_ENDVERTEX_CHI2":               [10, 0.0, 1.0],
        #"nSPDHits":                         [300, 0., 300.0],
        #"ProtonM_PIDp":                     [200, -50.0, 150.],
        #"ProtonP_PIDp":                     [200, -50.0, 150.],
        #"ProtonM_PIDK":                     [200, -50.0, 150.],
        #"ProtonP_PIDK":                     [200, -50.0, 150.],
        "ProtonM_ProbNNp":                   [5, 0.0, 0.2],
        "ProtonP_ProbNNp":                   [5, 0.0, 0.2],
        "ProtonM_ProbNNpi":                  [5, 0.0, 0.2],
        "ProtonP_ProbNNpi":                  [5, 0.0, 0.2],
        "ProtonM_ProbNNk":                   [5, 0.0, 0.2],
        "ProtonP_ProbNNk":                   [5, 0.0, 0.2],
        "ProtonM_P":                         [24, 10000.0, 10000.],
        "ProtonP_P":                         [24, 10000.0, 10000.],
        "ProtonM_PT":                        [24, 1000.0, 1000.],
        "ProtonP_PT":                        [24, 1000.0, 1000.],
        #"Proton_PT":                         array("d",[-50000.0, -4000., -3000., -2000., -1000., 0., 1000., 2000., 3000., 4000., 50000.]),
        #"Proton_PZ":                         array("d",[0, 15000.0, 20000., 25000, 30000., 37500., 45000., 55000., 65000., 90000., 1000000.]),
        "Proton_PT":                         array("d",[-50000.0, -3000., -1000., 1000., 3000., 50000.]),
        "Proton_PZ":                         array("d",[0, 20000., 30000., 45000., 65000., 1000000.]),
        "ProtonM_PX":                        [-50000.0, -4000., -3000., -2000., -1000., 0., 1000., 2000., 3000., 4000., 50000.],
        "ProtonP_PX":                        [-50000.0, -4000., -3000., -2000., -1000., 0., 1000., 2000., 3000., 4000., 50000.],
        "ProtonM_PY":                        [-50000.0, -4000., -3000., -2000., -1000., 0., 1000., 2000., 3000., 4000., 50000.],
        "ProtonP_PY":                        [-50000.0, -4000., -3000., -2000., -1000., 0., 1000., 2000., 3000., 4000., 50000.],
        "ProtonM_PZ":                        [0, 15000.0, 20000., 25000, 30000., 37500., 45000., 55000., 65000., 90000., 1000000.],
        "ProtonP_PZ":                        [0, 15000.0, 20000., 25000, 30000., 37500., 45000., 55000., 65000., 90000., 1000000.],
        "ProtonM_TRACK_CHI2NDOF":            [6, 0.0, 0.5],
        "ProtonP_TRACK_CHI2NDOF":            [6, 0.0, 0.5],
        }


def find_bin(var_name, val):

    if val > binning[var_name][1]:
        bin_n = int((val - binning[var_name][1])/binning[var_name][2])
    else:
        bin_n = 1

    return bin_n

def build_map(kSource, kRange):


    #dirName  = dataDir +"Data_Low_NoPID/"
    dirName  = dataDir

    nt_MC = TChain("DecayTree")
    nt_MC.Add("{}/MC/Trigger/{}DiProton_{}_2018_eff.root".format(dataDir, kRange, kSource))

    nt  =  TChain("Jpsi2ppTuple/DecayTree")
    nt.Add(dirName+'/Tuple.root')
    #nt.SetBranchStatus("*",0)
    #for var in binning:
        #nt.SetBranchStatus("{}".format(var),1);

    eff_trigger   = array( "d", [0])
    eff_selection = array( "d", [0])


    f_new = TFile(dirName+'/Tuple_w_jpsipt.root', "RECREATE")

    tree = nt.CloneTree(0)
    tree.Branch("eff_trigger",eff_trigger,"eff_trigger/D")
    tree.Branch("eff_selection",eff_selection,"eff_selection/D")


    #f = TFile("{}/MC/Trigger/{}DiProton_{}_2018_eff.root".format(dataDir, kRange, kSource))
    #t_MC = f.Get("DecayTree")


    for event in nt:
        #x = {
            #"Jpsi_Y":                           nt.Jpsi_Y,
            #"Jpsi_L0HadronDecision_TOS":        nt.Jpsi_L0HadronDecision_TOS,
            #"Jpsi_Hlt1DiProtonDecision_TOS":    nt.Jpsi_Hlt1DiProtonDecision_TOS,
            #"Jpsi_PT":                          nt.Jpsi_PT,
            #"Jpsi_ENDVERTEX_CHI2":              nt.Jpsi_ENDVERTEX_CHI2,
            #"ProtonM_ProbNNp":                  nt.ProtonM_ProbNNp,
            #"ProtonP_ProbNNp":                  nt.ProtonP_ProbNNp,
            #"ProtonM_ProbNNpi":                 nt.ProtonM_ProbNNpi,
            #"ProtonP_ProbNNpi":                 nt.ProtonP_ProbNNpi,
            #"ProtonM_ProbNNk":                  nt.ProtonM_ProbNNk,
            #"ProtonP_ProbNNk":                  nt.ProtonP_ProbNNk,
            #"ProtonM_P":                        nt.ProtonM_P,
            #"ProtonP_P":                        nt.ProtonP_P,
            #"ProtonM_PT":                       nt.ProtonM_P,
            #"ProtonP_PT":                       nt.ProtonP_P,
            #"ProtonM_TRACK_CHI2NDOF":           nt.ProtonM_TRACK_CHI2NDOF,
            #"ProtonP_TRACK_CHI2NDOF":           nt.ProtonP_TRACK_CHI2NDOF,
            #}
        x = {
            #"Jpsi_Y":                           nt.Jpsi_Y,
            "Jpsi_TRUEPT":                        nt.Jpsi_PT,
            #"Jpsi_ENDVERTEX_CHI2":              nt.Jpsi_ENDVERTEX_CHI2,
            # "ProtonM_PX":                       nt.ProtonM_PX,
            # "ProtonP_PX":                       nt.ProtonP_PX,
            # "ProtonM_PY":                       nt.ProtonM_PY,
            # "ProtonP_PY":                       nt.ProtonP_PY,
            # "ProtonM_PZ":                       nt.ProtonM_PZ,
            # "ProtonP_PZ":                       nt.ProtonP_PZ,
            #"ProtonM_TRACK_CHI2NDOF":           nt.ProtonM_TRACK_CHI2NDOF,
            #"ProtonP_TRACK_CHI2NDOF":           nt.ProtonP_TRACK_CHI2NDOF,
            }
        cut = "1"
        for key in x:
            #bin_n = find_bin(key, x[key])
            bin_n = np.digitize([x[key]], binning[key])[0]
            left  = binning[key][bin_n-1]
            right = binning[key][bin_n]
            cut = "{0} && ({1} > {2} && {1} < {3})".format(cut, key, left, right)
            #if key!="Jpsi_Hlt1DiProtonDecision_TOS" and key!="Jpsi_L0HadronDecision_TOS":
                ##left  = binning[key][1] +  (bin_n) * binning[key][2]
                ##right = binning[key][1] + (bin_n+1) * binning[key][2]
                #cut = "{0} && ({1} > {2} && {1} < {3})".format(cut, key, left, right)
            #else:
                #cut = "{0} && ({1}=={2})".format(cut, key, x[key])

        n_sel = float(nt_MC.GetEntries("{} && eff_selection>0.0".format(cut)))
        n_tr  = float(nt_MC.GetEntries("{} && eff_trigger>0.0".format(cut)))
        n     = float(nt_MC.GetEntries(cut))
        #print(cut,n, n_cut)
        if n>1e-6:
            eff_selection[0] = n_sel/n
            eff_trigger[0]   = n_tr/n
            #print(eff_etac[0])
            #input()

        else:
            eff_selection[0] = 0.
            eff_trigger[0]   = 0.
        tree.Fill()

    tree.Write()

    f_new.Close()
    del tree, nt, nt_MC

def eff_map(kSource, kRange):


    #dirName  = dataDir +"Data_Low_NoPID/"
    dirName  = dataDir

    nt = TChain("DecayTree")
    nt.Add("{}/MC/Trigger/{}DiProton_{}_2018_eff.root".format(dataDir, kRange, kSource))

    # n_vars = 6
    # n_bins = array("i",n_vars*[5])
    # xmin = array("d",4*[-50000.0]+2*[0.])
    # xmax = array("d",4*[50000.0]+2*[1000000.0])
    # h_eff = THnD("h_eff","h_eff",n_vars, n_bins, xmin, xmax)
    # h_def = THnD("h_def","h_def",n_vars, n_bins, xmin, xmax)

    n_vars = 1
    n_bins = array("i",n_vars*[5])
    xmin = array("d",[5000.0])
    xmax = array("d",[20000.0])

    h_eff = TH1D("h_eff","h_eff",1, 18, xmin, xmax)
    h_def = TH1D("h_def","h_def",1, 18, xmin, xmax)

    for i_d in range(4):
        h_eff.SetBinEdges(i_d, binning["Proton_PT"])
        h_def.SetBinEdges(i_d, binning["Proton_PT"])
    for i_d in range(4, 6):
        h_eff.SetBinEdges(i_d, binning["Proton_PZ"])
        h_def.SetBinEdges(i_d, binning["Proton_PZ"])

    f_new = TFile('{}/MC/Trigger/hh_{}_eff_map.root'.format(dirName,kRange), "RECREATE")

    for event in nt:
        x = array("d", [
                        nt.Jpsi_TRUEPT,
                        ])
        # x = array("d", [
        #                 nt.ProtonM_PX,
        #                 nt.ProtonP_PX,
        #                 nt.ProtonM_PY,
        #                 nt.ProtonP_PY,
        #                 nt.ProtonM_PZ,
        #                 nt.ProtonP_PZ,
        #                 ])
        if nt.eff_selection > 0.1:
            h_eff.Fill(x)

        h_def.Fill(x)

    h_def.Print()
    h_eff.Divide(h_def)
    h_eff.Write()
    #h_def.Write()

    f_new.Close()
    del nt

def get_entries(kSource, kRange):

    nt = TChain("DecayTree")
    nt.Add("{}/MC/Trigger/{}DiProton_{}_2018_AddBr.root".format(dataDir, kRange, kSource))

    for ibin_x in range(len(binning["ProtonM_PX"])-1):
        key = "ProtonM_PX"
        left  = binning[key][ibin_x]
        right = binning[key][ibin_x+1]
        cut_x = "({0} > {1} && {0} < {2})".format(key, left, right)

        for ibin_y in range(len(binning["ProtonM_PY"])-1):
            key = "ProtonM_PY"
            left  = binning[key][ibin_y]
            right = binning[key][ibin_y+1]
            cut_y = "{0} && ({1} > {2} && {1} < {3})".format(cut_x, key, left, right)

            for ibin_z in range(len(binning["ProtonM_PZ"])-1):
                key = "ProtonM_PZ"
                left  = binning[key][ibin_z]
                right = binning[key][ibin_z+1]
                cut_z = "{0} && ({1} > {2} && {1} < {3})".format(cut_y, key, left, right)
                n = nt.GetEntries(cut_z)
                print(n)

import numpy as np

if __name__ == "__main__":

    kSources = ["fromB"]
    kRanges = ["etac","jpsi"]
    kRanges = ["chic0","chic1","chic2","etac","etac2S","psi2S","jpsi"]
    #kRanges = ["chic0","chic1","chic2","etac2S","psi2S"]
    #selectGen("prompt","High","Up")
    #selectGen("fromB","Low","Down")
    #weightSPD("Up")
    #weightSPD("Down")
    #for kMag in kPol:
        #select("prompt","High", kMag)

    for kRange in kRanges:
        for kSource in kSources:
            calc_eff(kSource,kRange)
            # eff_map(kSource,kRange)
            build_map(kSource,kRange)

