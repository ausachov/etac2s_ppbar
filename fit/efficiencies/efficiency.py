from ROOT import TChain, TTree, TFile, TH1D, TH1, TParameter, TCanvas, gPad, gStyle, gROOT, \
    RooFit, RooRealVar, RooFormulaVar, RooDataSet, RooArgSet, RooArgList, RooLinkedList, \
    RooGaussian, RooVoigtian, RooAddPdf, RooGenericPdf, RooCmdArg, RooAbsReal
from array import array
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/eff/note_v2"
# homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/eff/binom"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"
gROOT.LoadMacro("/users/LHCb/zhovkovska/etac2s_ppbar/fit/libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")

import logging

PRINTTABLE = True
# PLOT = True
PLOT = False

###############################################
#  
#     Event types for existing files:
#     10132000: "psi(2S)",
#     10132010: "J/psi(1S)",
#     10132030: "chi_c0(1P)",
#     10132040: "chi_c1(1P)",
#     10132050: "chi_c2(1P)",
#     10132060: "eta_c(1S)",
#     10132080: "eta_c(2S)",
#     24102002: "J/psi(1S)",
#     24102011: "J/psi(1S)", #prompt etac
#     24102013: "J/psi(1S)",
#     28102000: "psi(2S)",
#     28102001: "psi(2S)", # prompt "eta_c(2S)",
#     28102002: "psi(2S)",
#     28102033: "chi_c0(1P)",
#     28102043: "chi_c1(1P)",
#     28102053: "chi_c2(1P)",
# 
# ###############################################


genNoCutTuples = {
    "prompt":
    {    
        "etac":   dataDir + "/MC/Generator/Tuple-24102011-Total_AddBr.root",    #P_PT>0.6 & P_P>8.0
        "Jpsi":   dataDir + "/MC/Generator/Tuple-24102002-Total_AddBr.root",    #old decay file
        "jpsi":   dataDir + "/MC/Generator/Tuple-24102013-Total_AddBr.root",
        "etac2S": dataDir + "/MC/Generator/Tuple-28102001-Total_AddBr.root",
        "chic0":  dataDir + "/MC/Generator/Tuple-28102033-Total_AddBr.root",
        "chic1":  dataDir + "/MC/Generator/Tuple-28102043-Total_AddBr.root",
        "chic2":  dataDir + "/MC/Generator/Tuple-28102053-Total_AddBr.root",
        "psi2S":  dataDir + "/MC/Generator/Tuple-28102000-Total_AddBr.root",
        "Psi2S":  dataDir + "/MC/Generator/Tuple-28102002-Total_AddBr.root",    #old decay file
        "pppi0":  dataDir + "/MC/Generator/Tuple-24102402-Total_AddBr.root",
    },
    "sec":
    {   
        "Etac":   dataDir + "/MC/Generator/Tuple-10132060-Total_AddBr.root",    #P_PT>0.6 & P_P>8.0
        # "Etac":   dataDir + "/MC/Generator/Tuple-10030060-Total_AddBr.root",    #P_PT>0.6 & P_P>8.0
        "etac":   dataDir + "/MC/Generator/Tuple-10132060-Total_AddBr.root",
        "Jpsi":   dataDir + "/MC/Generator/Tuple-24102002-Total_AddBr.root",    #old decay file
        "jpsi":   dataDir + "/MC/Generator/Tuple-10132010-Total_AddBr.root",
        "etac2S": dataDir + "/MC/Generator/Tuple-10132080-Total_AddBr.root",
        "chic0":  dataDir + "/MC/Generator/Tuple-10132030-Total_AddBr.root",
        "chic1":  dataDir + "/MC/Generator/Tuple-10132040-Total_AddBr.root",
        "chic2":  dataDir + "/MC/Generator/Tuple-10132050-Total_AddBr.root",
        "psi2S":  dataDir + "/MC/Generator/Tuple-10132000-Total_AddBr.root",
        "Psi2S":  dataDir + "/MC/Generator/Tuple-28102002-Total_AddBr.root",    #old decay file
        "pppi0":  dataDir + "/MC/Generator/Tuple-24102402-Total_AddBr.root",
    }
}

###############################################
#
#   Directories and existing files:
#
#   genTuples:    /NoTrigger  : "Etac","etac","Jpsi","jpsi","etac2S","chic0","chic1","chic2","psi2S","Psi2S","pppi0"
#   recoTuples:   /NoTurbo    : "etac","jpsi","etac2S","chic0","chic1","chic2","psi2S"
#   selTuples:    /Trigger    : "Etac","etac","Jpsi","jpsi","etac2S","chic0","chic1","chic2","psi2S","Psi2S","pppi0"
#
###############################################


# fucntion that returns a name of a MC file
tuples = lambda ccbar, filedir, postfix: f"{dataDir}/MC/{filedir}/{ccbar}DiProton_*_2018_{postfix}.root"

probNN = "0.6"
cut_accept  = "ProtonP_TRUETHETA>0.01 && ProtonP_TRUETHETA<0.4 && ProtonM_TRUETHETA>0.01 && ProtonM_TRUETHETA<0.4"#" && 0.5*TMath::Log((Jpsi_TRUEP_E + Jpsi_TRUEP_Z)/(Jpsi_TRUEP_E-Jpsi_TRUEP_Z))>2. && 0.5*TMath::Log((Jpsi_TRUEP_E + Jpsi_TRUEP_Z)/(Jpsi_TRUEP_E-Jpsi_TRUEP_Z))<4.5 "#" && nSPDHits < 300" #
# cut_accept  = "1"#" && 0.5*TMath::Log((Jpsi_TRUEP_E + Jpsi_TRUEP_Z)/(Jpsi_TRUEP_E-Jpsi_TRUEP_Z))>2. && 0.5*TMath::Log((Jpsi_TRUEP_E + Jpsi_TRUEP_Z)/(Jpsi_TRUEP_E-Jpsi_TRUEP_Z))<4.5 "#" && nSPDHits < 300" #
cut_true_v  = "Jpsi_TRUEPT > 5000 && Jpsi_TRUEPT < 20000 && Jpsi_TRUEY > 2.0 && Jpsi_TRUEY < 4.0"
cut_reco_v  = "Jpsi_PT > 5000 && Jpsi_PT < 20000 && Jpsi_Y > 2.0 && Jpsi_Y < 4.0"
# cut_PID     = "ProtonP_ProbNNk < 0.6 && ProtonM_ProbNNk < 0.6 && ProtonP_ProbNNp > 0.6 && ProtonM_ProbNNp > 0.6 && ProtonP_ProbNNpi < 0.6 && ProtonM_ProbNNpi < 0.6 "
# cut_PID     = "(ProtonP_PIDp-ProtonP_PIDK)>0 && (ProtonM_PIDp-ProtonM_PIDK)>0 && ProtonP_PIDp>5 && ProtonM_PIDp>5"
cut_PID     = f"ProtonP_ProbNNp > {probNN} && ProtonM_ProbNNp > {probNN} && (ProtonP_PIDp-ProtonP_PIDK)>0 && (ProtonM_PIDp-ProtonM_PIDK)>0 && ProtonP_PIDp>5 && ProtonM_PIDp>5"
cut_Trigger = "Jpsi_L0HadronDecision_TOS && (Jpsi_Hlt1DiProtonDecision_TOS || Jpsi_Hlt1DiProtonHighDecision_TOS)"


cut_select  = "     nSPDHits < 300 && \
                    ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
                    ProtonP_PT>2000 && ProtonM_PT>2000 && \
                    ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366 && \
                    ProtonP_TRACK_CHI2NDOF<2.5 && ProtonM_TRACK_CHI2NDOF<2.5 && \
                    ProtonP_TRACK_CloneDist<0 && ProtonM_TRACK_CloneDist<0 && \
                    ProtonP_TRACK_GhostProb<0.2 && ProtonM_TRACK_GhostProb<0.2"

                    #ProtonP_PIDp>5 && ProtonM_PIDp>5 && \
                    #(ProtonP_PIDp-ProtonP_PIDK)>0 && (ProtonM_PIDp-ProtonM_PIDK)>0 && \

cut_misID_Kpi = "!(Jpsi_M01_Subst01_pp~2Kpi>1835 && Jpsi_M01_Subst01_pp~2Kpi<1895)"
cut_misID_piK = "!(Jpsi_M01_Subst01_pp~2piK>1835 && Jpsi_M01_Subst01_pp~2piK<1895)"
cut_misID     = cut_misID_Kpi + " && " + cut_misID_piK

cut_TRUEPT = {
                "all"   : 1800.,
                "prompt": 1800.,
                # "sec"   : 0.
                "sec"   : 900.
            }

ProtonP_TRUEP = "TMath::Sqrt(ProtonP_TRUEP_X*ProtonP_TRUEP_X + ProtonP_TRUEP_Y*ProtonP_TRUEP_Y + ProtonP_TRUEP_Z*ProtonP_TRUEP_Z)"
ProtonM_TRUEP = "TMath::Sqrt(ProtonM_TRUEP_X*ProtonM_TRUEP_X + ProtonM_TRUEP_Y*ProtonP_TRUEP_Y + ProtonM_TRUEP_Z*ProtonP_TRUEP_Z)"

#Y = "0.5*TMath::Log((Jpsi_TRUEP_E + Jpsi_TRUEP_Z)/(Jpsi_TRUEP_E-Jpsi_TRUEP_Z))"
Y = "Jpsi_TRUEPT"

binDict = {
    # "PT"    : array("f",[5000., 5750., 6500, 7250., 8000., 8500., 9000., 9500., 10000., 10500., 11000., 12000., 13000., 14000., 17000., 20000.]), #
    # "PT"    : array("f",[5000., 5500., 6000., 6500, 7000., 7500., 8000., 8500., 9000., 9500., 10000., 10500., 11000., 11500., 12000., 13000., 14000., 17000., 20000.]), #
    # "PT"    : array("f",[5000., 20000.]), #
    "PT"    : array("f",[5000., 6500, 8000., 10000., 12000., 14000., 20000.]), #
    # "PT"    : array("f",[5000., 6500, 8000., 10000., 12000., 14000.]), #
    "Y"     : array("f",[2., 2.5, 3., 3.5, 4.]),
    "nSPD"  : array("f",[0., 75., 150., 225., 300.]),
}

varDict = {
    "DecayTree":
            {
                "PT"    : "Jpsi_TRUEPT",
                "Y"     : "Jpsi_TRUEY",
                # "Y"     : "0.5*TMath::Log((Jpsi_TRUEP_E + Jpsi_TRUEP_Z)/(Jpsi_TRUEP_E-Jpsi_TRUEP_Z))",
                # "PT"    : "Jpsi_PT",
                # "Y"     : "Jpsi_Y",
                "nSPD"  : "nSPDHits"
            },
    "MCDecayTree":
            {
                "PT"    : "Jpsi_TRUEPT",
                "Y"     : "Jpsi_TRUEY",
                "nSPD"  : "nSPDHits"
            },
    "MCDecayTreeTuple/MCDecayTree":
            {
                "PT"    : "Jpsi_TRUEPT",
                "Y"     : "Jpsi_TRUEY",
                "nSPD"  : "nSPDHits"
            }
        }

massDict = {
    "etac"  : [2910, 3060],
    "jpsi"  : [3025, 3175],
    "etac2S": [3565, 3715],
    "chic0" : [3340, 3490],
    "chic1" : [3435, 3585],
    "chic2" : [3490, 3630],
    "psi2S" : [3610, 3760],
}


gammaDict = {
    "etac"  : 32.0,
    "jpsi"  : 0.,
    "etac2S": 11.3,
    "chic0" : 10.8,
    "chic1" : 0.84,
    "chic2" : 1.97,
    "psi2S" : 0.,
}


def plot_hist(hist, binsName="PT", eff=None, err=None):

    bins = binDict[binsName]

    canv = TCanvas("canv", "canv", 550, 400)
    canv.cd(1)
    gPad.SetLeftMargin(0.15)
    hist.ClearUnderflowAndOverflow()
    # hist.GetXaxis().SetTitle(binsName)
    # hist.GetYaxis().SetRangeUser(0.8, 1.2)
    hist.GetYaxis().SetRangeUser(0.8*hist.GetMinimum(), 1.2*hist.GetMaximum())
    hist.GetXaxis().SetRangeUser(bins[0]-1.,bins[-1]+1.)
    hist.DrawCopy("E1","")
    if eff!=None and err!=None:
        # print(eff, err)
        h = TH1D("h_tot","h_tot", 1, bins[0], bins[-1])
        h.SetBinContent(1, eff)
        h.SetBinError(1, err)
        h.SetFillColorAlpha(2, 0.7)
        h.SetFillStyle(3001)
        h.SetMarkerSize(0)
        h.DrawCopy("same E2","")
    return canv

def write2file(filename, hist, eff=None, err=None):

    file = TFile(filename, "RECREATE")
    hist.Write()
    if eff!=None:
        eff = TParameter(float)("eff_val", eff)
        file.WriteObject(eff, "eff_val")
    if err!=None:
        err = TParameter(float)("eff_err", err)
        file.WriteObject(err, "eff_err")

    file.Close()


def get_entries(tree, cut, ccbar, draw=False):


    if tree.GetName()=="DecayTree":

        tree_fit = TTree()
        tree_fit = tree.CopyTree(cut)

        #Jpsi_M = RooRealVar ('Jpsi_M_res','Jpsi_M_res', -30., 30.)
        min_m, max_m = massDict[ccbar]
        gamma = gammaDict[ccbar]
        Jpsi_M = RooRealVar ('Jpsi_M','Jpsi_M', min_m, max_m)
        ds = RooDataSet('ds','ds',tree_fit,RooArgSet(Jpsi_M))

        n_ccbar = RooRealVar('n_ccbar','num of _etac', 1e3, 10, 1.e7)
        mean_ccbar = RooRealVar('mean_ccbar','mean of gaussian', (min_m+max_m)/2., min_m, max_m)
        gamma_ccbar = RooRealVar('gamma_ccbar','mean of gaussian', gamma)
        sigma_ccbar = RooRealVar('sigma_ccbar','width of gaussian', 9., 0.1, 30.)

        f = TFile(f"{homeDir}/MC/mass_fit/Wksp_M_prompt_sim.root","READ")
        w = f.Get("w")
        f.Close()
        r_yield = w.var("r_G1ToG2").getValV()
        r_sigma = w.var("r_NToW").getValV()
        # sigma_ccbar_2 = RooRealVar('sigma_ccbar_2','width of gaussian', 9., 0.1, 30.)
        sigma_ccbar_2 = RooFormulaVar('sigma_ccbar_2',f'sigma_ccbar/{r_sigma}',RooArgList(sigma_ccbar))
        f = RooRealVar('f','num of _etac', r_yield)

        #Fit J/psi
        if ccbar=="jpsi" or ccbar=="psi2S":
            gauss_1 = RooGaussian('gauss_1','gaussian PDF',Jpsi_M, mean_ccbar, sigma_ccbar)
            gauss_2 = RooGaussian('gauss_2','gaussian PDF',Jpsi_M, mean_ccbar, sigma_ccbar_2)
            resol = RooAddPdf('resol','gaussian PDF',RooArgList(gauss_1, gauss_2), RooArgList(f))
        else:
            bwxg_1 = RooVoigtian("bwxg_1", "bwxg", Jpsi_M, mean_ccbar, gamma_ccbar, sigma_ccbar)
            bwxg_2 = RooVoigtian("bwxg_2", "bwxg", Jpsi_M, mean_ccbar, gamma_ccbar, sigma_ccbar_2)
            resol = RooAddPdf('resol','gaussian PDF',RooArgList(bwxg_1, bwxg_2), RooArgList(f))


        #a0 = RooRealVar("a0","a0",0.4,-2.,2.)
        a0 = RooRealVar("a0","a0",0.4,-10.,10.)
        a1 = RooRealVar("a1","a1",0.05,-2.,2.)
        a2 = RooRealVar("a2","a2",-0.005,-2.,2.)
        #bkg = RooChebychev ("bkg","Background",Jpsi_M,RooArgList(a0,a1))
        bkg = RooGenericPdf("bkg","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.)",RooArgList(Jpsi_M,a0,a1))

        n_bkg = RooRealVar('n_bkg','num of etac', 1e3, 0, 5.e4)

        llist = RooLinkedList()
        llist.Add(RooCmdArg(RooFit.Offset(True)))
        llist.Add(RooCmdArg(RooFit.Extended(True)))
        llist.Add(RooCmdArg(RooFit.Save(True)))
        #llist.Add(RooCmdArg(RooFit.PrintLevel(-1)))

        model_ccbar = RooAddPdf('model_ccbar','ccbar signal', RooArgList(resol), RooArgList(n_ccbar))
        #model_ccbar = RooAddPdf('model_ccbar','ccbar signal', RooArgList(resol, bkg), RooArgList(n_ccbar, n_bkg))
        model_ccbar.fitTo(ds, llist)
        model_ccbar.fitTo(ds, llist)
        model_ccbar.fitTo(ds, llist)

        # draw = True
        if draw:
            frame = Jpsi_M.frame(RooFit.Title('#eta_c to p #bar{p} from_b'))

            ds.plotOn(frame)
            model_ccbar.plotOn(frame,RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
            model_ccbar.paramOn(frame,RooFit.Layout(0.68,0.99,0.99))
            c = TCanvas('Masses_Fit','Masses Fit',1000,800)
            frame.Draw()
            # c.SaveAs("fit_{}.pdf".format(ccbar))
            # input()

        n, n_err = n_ccbar.getValV(), n_ccbar.getError()
    else:
        n = tree.GetEntries(cut)
        n_err = n**0.5

    return n, n_err

def calc_eff(tree_1, tree_2, binsName, ccbar):

    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)

    draw = False 

    varName1 = varDict[tree_1.GetName()][binsName]
    varName2 = varDict[tree_2.GetName()][binsName]
    bins = binDict[binsName]
    nBins = len(bins)-1

    n_1 = tree_1.GetEntries("{0}>{1} && {0}<{2}".format(varName1,bins[0],bins[-1])) 
    n_2 = tree_2.GetEntries("{0}>{1} && {0}<{2}".format(varName2,bins[0],bins[-1])) 
    er_1 = n_1**0.5
    er_2 = n_2**0.5
    # n_1, er_1 = get_entries(tree_1, "{0}>{1} && {0}<{2}".format(varName1,bins[0],bins[-1]), ccbar)
    # n_2, er_2 = get_entries(tree_2, "{0}>{1} && {0}<{2}".format(varName2,bins[0],bins[-1]), ccbar)

    # print("{0}>{1} && {0}<{2}".format(varName1,bins[0],bins[-1]))
    # print("{0}>{1} && {0}<{2}".format(varName2,bins[0],bins[-1]))
    # print(n_1, n_2)
    logging.debug(f"tree 1 : {n_1}")
    logging.debug(f"tree 2 : {n_2}" )
    eff_val = float(n_1)/float(n_2)
    # Poisson error
    eff_err = eff_val * ((er_1/n_1)**2 + (er_2/n_2)**2)**0.5
    # Binomial error
    # eff_err = ((n_1+1)*(n_2-n_1+1)/(n_2+2)**2/(n_2+3))**0.5

    logging.info(f"Total: {eff_val:.3f} +/- {eff_err:3f} \n" )
    n_1, n_2 = 0., 0.
    #eff_err = eff_val * (1/float(n_1) + 1/float(n_2))**0.5

    hist_eff = TH1D("hist_eff","hist_eff", nBins, bins)

    for iB in range(nBins):

        logging.debug("Binning var 1: {0}>{1} && {0}<{2}".format(varName1,bins[iB],bins[iB+1]))
        logging.debug("Binning var 2: {0}>{1} && {0}<{2}".format(varName2,bins[iB],bins[iB+1]))
        entr_1  = tree_1.GetEntries("{0}>{1} && {0}<{2}".format(varName1,bins[iB],bins[iB+1])) 
        entr_2  = tree_2.GetEntries("{0}>{1} && {0}<{2}".format(varName2,bins[iB],bins[iB+1])) 
        err_1   = entr_1**0.5
        err_2   = entr_2**0.5
        n_1+=entr_1
        n_2+=entr_2

        # entr_1, err_1  = get_entries(tree_1, "{0}>{1} && {0}<{2}".format(varName1,bins[iB],bins[iB+1]), ccbar, draw)
        # entr_2, err_2  = get_entries(tree_2, "{0}>{1} && {0}<{2}".format(varName2,bins[iB],bins[iB+1]), ccbar, draw)

        eff_bin     = float(entr_1)/float(entr_2)
        #eff_err_bin = eff_bin*(1/float(entr_1)+1/float(entr_2))**0.5
        # Poisson error
        eff_err_bin = eff_bin*((err_1/entr_1)**2 + (err_2/entr_2)**2)**0.5
        # Binomial error
        # eff_err_bin = ((entr_1+1)*(entr_2-entr_1+1)/(entr_2+2)**2/(entr_2+3))**0.5
        hist_eff.SetBinContent(iB+1,eff_bin)
        hist_eff.SetBinError(iB+1,eff_err_bin)
        logging.info(f"Bin {iB+1} \n")
        logging.debug(f"tree 1 : {entr_1}" )
        logging.debug(f"tree 2 : {entr_2}" )
        logging.info(f"{eff_bin:.3f} +/- {eff_err_bin:.3f} \n")

    logging.debug(f"Sum of entries: {n_1} and {n_2}")
    # eff_val = float(n_1)/float(n_2)
    # eff_err = eff_val * ((er_1/n_1)**2 + (er_2/n_2)**2)**0.5
    # print(eff_val,eff_err)
    # input()

    return hist_eff, eff_val, eff_err


def calc_effTot(tree_1:TTree, tree_2:TTree, ccbar:str):

    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)

    n_1, er_1 = get_entries(tree_1, "", ccbar)
    n_2, er_2 = get_entries(tree_2, "", ccbar)

    eff_val = n_1/float(n_2)
    eff_err = eff_val * ((er_1/n_1)**2 + (er_2/n_2)**2)**0.5
    return eff_val, eff_err

def gen_eff(ccbar:str="etac", source:str="prompt", binsName:str="PT"):

    nt = TChain("MCDecayTree")
    if source=="all":
        nt.Add(genNoCutTuples["prompt"][ccbar])
        nt.Add(genNoCutTuples["sec"][ccbar])
    else:
        nt.Add(genNoCutTuples[source][ccbar])

    tree_acc = TTree()
    tree_gen = TTree()

    truePTCut = cut_TRUEPT[source]

    # truePTCut = 900.
    # truePCut = 8000.
    cut_gen = "ProtonM_TRUEPT>{0} && ProtonP_TRUEPT>{0}".format(truePTCut)

    # cut_gen = "ProtonM_TRUEPT>{0} && ProtonP_TRUEPT>{0} && {2}>{1} && {3}>{1}".format(truePTCut, truePCut, ProtonM_TRUEP, ProtonP_TRUEP)

    if source=="all": source="1"

    tree_gen = nt.CopyTree("{0} && {1}".format(cut_true_v, source))
    tree_acc = nt.CopyTree("{} && {} && {} && {}".format(cut_true_v, source, cut_gen, cut_accept)) 
    # tree_acc = nt.CopyTree("{} && {}".format(cut_accept, source)) #cut_gen, 

    hist_eff, eff_val, eff_err = calc_eff(tree_acc, tree_gen, binsName, ccbar)

    logging.info(f"{ccbar} gen: {round(eff_val, 3)} +/- {round(eff_err,3)}")
    return hist_eff, eff_val, eff_err

def pid_def_eff(ccbar:str="etac", source:str="prompt", binsName:str="PT", typeCalib:str=None):

    # if  typeCalib=="gen":
    #     filedir = "Trigger/PIDGen"
    #     cut_PID     = f"ProtonP_ProbNNp_corr > {probNN} && ProtonM_ProbNNp_corr > {probNN}"
    #     postfix = "Lb"
    if  typeCalib=="gen":
        filedir = "Trigger/PIDGen"
        cut_PID     = f"ProtonP_ProbNNp_corr > {probNN} && ProtonM_ProbNNp_corr > {probNN}"
        postfix = "corr"
    elif typeCalib=="gen_Lc":
        filedir = "Trigger/PIDGen"
        cut_PID     = f"ProtonP_ProbNNp_corr_Lc > {probNN} && ProtonM_ProbNNp_corr_Lc > {probNN}"
        postfix = "corr"
    elif typeCalib=="gen_Lb":
        filedir = "Trigger/PIDGen"
        cut_PID     = f"ProtonP_ProbNNp_corr_Lb > {probNN} && ProtonM_ProbNNp_corr_Lb > {probNN}"
        postfix = "corr"
    elif typeCalib=="corr":
        filedir = "Trigger/PIDCorr"
        cut_PID     = f"ProtonP_ProbNNp_corr > {probNN} && ProtonM_ProbNNp_corr > {probNN}"
        postfix = "Lb"
    else: 
        filedir = "Trigger"
        cut_PID = f"ProtonP_ProbNNp > {probNN} && ProtonM_ProbNNp > {probNN}"
        postfix = "AddBr"

    if source=="prompt":
        src = "prompt"
    elif source=="sec":            
        src = "fromB"
    else:
        src = "1"

    if source=='all': source = "1"

    nt = TChain("DecayTree")
    nt.Add(tuples(ccbar,filedir,postfix))

    tree_rec = TTree()
    tree_pid = TTree()

    truePTCut = cut_TRUEPT[source]


    cut_gen = "{1} && ProtonM_TRUEPT>{0} && ProtonP_TRUEPT>{0}".format(truePTCut, source)

    tree_rec = nt.CopyTree("{} && {} && {}".format(cut_reco_v,cut_gen, cut_select))
    tree_pid = nt.CopyTree("{} && {} && {} && {}".format(cut_reco_v, cut_gen, cut_select, cut_PID))

    hist_eff, eff_val, eff_err = calc_eff(tree_pid, tree_rec, binsName, ccbar)

    logging.info(f"{ccbar} pid: {round(eff_val,3)} +/- {round(eff_err,3)}")

    return hist_eff, eff_val, eff_err

def turbo_eff(ccbar:str="etac", source:str="prompt", binsName:str="PT"):

    if source=="prompt":
        src = "prompt"
    elif source=="sec":            
        src = "fromB"
    else:
        src = "1"

    bins = binDict[binsName]
    nBins = len(bins)-1

    nt = TChain("DecayTree")
    logging.debug(f'Tuple name: {tuples(ccbar, "NoTurbo", "AddBr")}')
    nt.Add(tuples(ccbar, "NoTurbo", "AddBr"))

    tree_reco = TTree()
    tree_hlt  = TTree()

    truePTCut = cut_TRUEPT[source]

    if ccbar.lower()=="etac" or ccbar.lower()=="jpsi" or ccbar=="pppi0":
        cut_Trigger = "Jpsi_L0HadronDecision_TOS && Jpsi_Hlt1DiProtonDecision_TOS"
        cut_Turbo = "Jpsi_Hlt2CcDiHadronDiProtonTurboDecision_TOS==1"
    else:
        cut_Trigger = "Jpsi_L0HadronDecision_TOS && Jpsi_Hlt1DiProtonHighDecision_TOS"
        cut_Turbo = "Jpsi_Hlt2CcDiHadronDiProtonHighTurboDecision_TOS"

    cut_gen = "{1} && ProtonM_TRUEPT>{0} && ProtonP_TRUEPT>{0}".format(truePTCut, source)
    cut_sel = f"{cut_reco_v} && {cut_gen} && {cut_select} && {cut_PID} && {cut_Trigger}"
    # cut_sel = "{} && {}".format(cut_reco_v, cut_gen)
    logging.debug(cut_sel)
    logging.debug(cut_Turbo)

    tree_reco = nt.CopyTree("{}".format(cut_sel))
    tree_hlt  = nt.CopyTree("{} && {}".format(cut_sel, cut_Turbo))
    logging.debug(tree_reco.GetEntries(""),tree_hlt.GetEntries(""))

    hist_eff, eff_val, eff_err = calc_eff(tree_hlt, tree_reco, binsName, ccbar)

    return hist_eff, eff_val, eff_err

def trigger_eff(ccbar:str="etac", source:str="prompt", binsName:str="PT"):

    if source=="prompt":
        src = "prompt"
    elif source=="sec":            
        src = "fromB"
    else:
        src = "1"

    bins = binDict[binsName]
    nBins = len(bins)-1

    h_l0 = TH1D()
    ff = TFile(f"{homeDir}/plots_{source}/eff_{ccbar}_{source}_{binsName}_l0.root", "READ")
    h_l0 = ff.Get("hist_eff")
    h_l0.SetDirectory(0)
    l0_tot = ff.Get("eff_val").GetVal()
    l0_err = ff.Get("eff_err").GetVal()
    ff.Close()

    nt = TChain("DecayTree")
    nt.Add(tuples(ccbar, "Trigger", "AddBr"))

    tree_pid = TTree()
    tree_hlt = TTree()

    truePTCut = cut_TRUEPT[source]

    if ccbar.lower()=="etac" or ccbar.lower()=="jpsi":
        cut_Trigger = "Jpsi_Hlt1DiProtonDecision_TOS"
    else:
        cut_Trigger = "Jpsi_Hlt1DiProtonHighDecision_TOS"


    cut_gen = "{1} && ProtonM_TRUEPT>{0} && ProtonP_TRUEPT>{0}".format(truePTCut, source)
    cut_sel = f"{cut_reco_v} && {cut_gen} && {cut_select} && {cut_PID}"

    tree_pid = nt.CopyTree(f"{cut_sel}")
    tree_hlt = nt.CopyTree(f"{cut_sel} && {cut_Trigger}")

    hist_eff, eff_val, eff_err = calc_eff(tree_hlt, tree_pid, binsName, ccbar)

    hist_eff.Multiply(h_l0)
    eff_err = (eff_err/eff_val)**2+(l0_err/l0_tot)**2
    eff_val *= l0_tot
    eff_err = eff_val*(eff_err**0.5)
    
    return hist_eff, eff_val, eff_err

def l0_def_eff(ccbar:str="etac", source:str="prompt", binsName:str="PT"):

    if source=="prompt":
        src = "prompt"
    elif source=="sec":            
        src = "fromB"
    else:
        src = "1"

    bins = binDict[binsName]
    nBins = len(bins)-1

    nt = TChain("DecayTree")
    nt.Add(tuples(ccbar, "Trigger", "AddBr"))

    logging.debug(f'Tuple name: {tuples(ccbar, "Trigger", "AddBr")}')
    nt.Print()
    tree_pid = TTree()
    tree_hlt = TTree()

    truePTCut = cut_TRUEPT[source]

    cut_Trigger = "Jpsi_L0HadronDecision_TOS"

    cut_gen = "{1} && ProtonM_TRUEPT>{0} && ProtonP_TRUEPT>{0}".format(truePTCut, source)
    cut_sel = "{0} && {1} && {2} && {3}".format(cut_reco_v, cut_gen, cut_select, cut_PID)

    tree_pid = nt.CopyTree("{}".format(cut_sel))
    tree_hlt = nt.CopyTree("{} && {}".format(cut_sel, cut_Trigger))

    hist_eff, eff_val, eff_err = calc_eff(tree_hlt, tree_pid, binsName, ccbar)

    return hist_eff, eff_val, eff_err

def l0_eff(ccbar:str="etac", source:str="prompt", binsName:str="PT"):

    if source=="prompt":
        src = "prompt"
    elif source=="sec":            
        src = "fromB"
    else:
        src = "1"

    bins = binDict[binsName]
    nBins = len(bins)-1

    # h_D = TH1D("hD_l0","hD_l0", nBins, bins)
    h_Down = TH1D()
    f1 = TFile("{}/MC/Trigger/L0Calib/{}DiProton_{}_MagDown_L0.root".format(dataDir,ccbar,src), "READ")
    h_Down = f1.Get("effJpsiL0Had")
    h_Down.SetDirectory(0)
    h_Down.SetBit(TH1.kIsAverage)
    f1.Close()

    h_Up = TH1D()
    f2 = TFile("{}/MC/Trigger/L0Calib/{}DiProton_{}_MagUp_L0.root".format(dataDir,ccbar,src), "READ")
    h_Up = f2.Get("effJpsiL0Had")
    h_Up.SetDirectory(0)
    h_Up.SetBit(TH1.kIsAverage)
    f2.Close()

    f11 = TFile("{}/MC/Trigger/L0Calib/{}DiProton_{}_MagDown_L0_int.root".format(dataDir,ccbar,src), "READ")
    eff_Down = f11.Get("effJpsiL0Had").GetBinContent(1)
    err_Down = f11.Get("effJpsiL0Had").GetBinError(1)
    f11.Close()

    f12 = TFile("{}/MC/Trigger/L0Calib/{}DiProton_{}_MagUp_L0_int.root".format(dataDir,ccbar,src), "READ")
    eff_Up = f12.Get("effJpsiL0Had").GetBinContent(1)
    err_Up = f12.Get("effJpsiL0Had").GetBinError(1)
    f11.Close()


    # h_tot = TH1D("hist_eff","hist_eff", nBins, bins)
    # h_tot = TH1D()
    h_tot = h_Down.Clone("hist_eff")
    h_tot.Add(h_Down, h_Up)
    h_tot.SetName("hist_eff")
    h_tot.SetTitle("hist_eff")
    h_tot.SetDirectory(0)
    # h_tot.Multiply(h_Up)
    # h_tot.Multiply(h_Down, h_Up)
    eff_val = (eff_Down+eff_Up)/2.
    eff_err = eff_val*((err_Up/eff_Up) ** 2 + (err_Down/eff_Down) ** 2 )**0.5
    logging.debug(f"eff_val: {eff_val}, eff_err: {eff_err}")
    return h_tot, eff_val, eff_err

def rec_eff(ccbar:str="etac", source:str="prompt", binsName:str="PT"):

    truePTCut = cut_TRUEPT[source]
    if source=="prompt":
        # cut_tz = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08"
        src = "prompt"
    elif source=="sec":            
        # cut_tz = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16"
        src = "fromB"

    if source=='all': source = "1"

    nt_gen = TChain("MCDecayTree")
    nt_rec = TChain("DecayTree")
    nt_gen.Add(tuples(ccbar, "NoTrigger", "AddBr"))
    nt_rec.Add(tuples(ccbar, "Trigger", "AddBr"))

    tree_acc = TTree()
    tree_sel = TTree()
        
    if ccbar.lower()=="etac" or ccbar.lower()=="jpsi" or ccbar=="pppi0":
        # cut_Trigger = "Jpsi_L0HadronDecision_TOS && Jpsi_Hlt1DiProtonDecision_TOS"
        cut_Trigger = "Jpsi_Hlt1DiProtonDecision_TOS"
    else:
        cut_Trigger = "Jpsi_Hlt1DiProtonHighDecision_TOS"

    cut_gen     = "ProtonM_TRUEPT>{0} && ProtonP_TRUEPT>{0} && {1}".format(truePTCut, source)
    cut_gen_rec = "ProtonM_PT>{0}     && ProtonP_PT>{0}     && {1}".format(truePTCut, source)

    # truePTCut = 900.
    # truePCut = 8000.
    # cut_gen = "ProtonM_TRUEPT>{0} && ProtonP_TRUEPT>{0} && {2}>{1} && {3}>{1} && {4}".format(truePTCut, truePCut, ProtonM_TRUEP, ProtonP_TRUEP, source)

    tree_acc = nt_gen.CopyTree("{} && {} && {}".format(cut_true_v, cut_gen, cut_accept))
    # tree_sel = nt_rec.CopyTree("{} && {} && {} && {} && {} && {}".format(cut_true_v, cut_gen, cut_accept, cut_select, cut_Trigger, cut_misID)) #L0 & PID is included separately
    tree_sel = nt_rec.CopyTree("{} && {} && {} && {} && {}".format(cut_true_v, cut_gen, cut_accept, cut_select, cut_Trigger)) #L0 & PID is included separately
    # tree_sel = nt_rec.CopyTree("{} && {} && {} && {} && {}".format(cut_true_v, cut_gen, cut_accept, cut_select, cut_misID)) #L0 & PID is included separately
    # remove trigger cut to calculate proper reco+sel efficiency alone


    hist_eff, eff_val, eff_err = calc_eff(tree_sel, tree_acc, binsName, ccbar)

    logging.info(f"{ccbar} rec: {round(eff_val,3)} +/- {round(eff_err,3)}")

    return hist_eff, eff_val, eff_err

def recsel_eff(ccbar:str="etac", source:str="prompt", binsName:str="PT"):

    truePTCut = cut_TRUEPT[source]
    if source=="prompt":
        # cut_tz = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08"
        src = "prompt"
    elif source=="sec":            
        # cut_tz = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16"
        src = "fromB"

    if source=='all': source = "1"

    nt_gen = TChain("MCDecayTree")
    nt_rec = TChain("DecayTree")
    nt_gen.Add(tuples(ccbar, "NoTrigger", "AddBr"))
    nt_rec.Add(tuples(ccbar, "Trigger", "AddBr"))

    tree_acc = TTree()
    tree_sel = TTree()
        
    if ccbar.lower()=="etac" or ccbar.lower()=="jpsi" or ccbar=="pppi0":
        # cut_Trigger = "Jpsi_L0HadronDecision_TOS && Jpsi_Hlt1DiProtonDecision_TOS"
        cut_Trigger = "Jpsi_Hlt1DiProtonDecision_TOS"
    else:
        cut_Trigger = "Jpsi_Hlt1DiProtonHighDecision_TOS"

    cut_gen     = "ProtonM_TRUEPT>{0} && ProtonP_TRUEPT>{0} && {1}".format(truePTCut, source)
    cut_gen_rec = "ProtonM_PT>{0}     && ProtonP_PT>{0}     && {1}".format(truePTCut, source)

    # truePTCut = 900.
    # truePCut = 8000.
    # cut_gen = "ProtonM_TRUEPT>{0} && ProtonP_TRUEPT>{0} && {2}>{1} && {3}>{1} && {4}".format(truePTCut, truePCut, ProtonM_TRUEP, ProtonP_TRUEP, source)

    tree_acc = nt_gen.CopyTree("{} && {} && {}".format(cut_true_v, cut_gen, cut_accept))
    # tree_sel = nt_rec.CopyTree("{} && {} && {} && {} && {} && {}".format(cut_true_v, cut_gen, cut_accept, cut_select, cut_Trigger, cut_misID)) #L0 & PID is included separately
    tree_sel = nt_rec.CopyTree("{} && {} && {} && {}".format(cut_true_v, cut_gen, cut_accept, cut_select)) #L0 & PID is included separately
    # tree_sel = nt_rec.CopyTree("{} && {} && {} && {} && {}".format(cut_true_v, cut_gen, cut_accept, cut_select, cut_misID)) #L0 & PID is included separately
    # remove trigger cut to calculate proper reco+sel efficiency alone


    hist_eff, eff_val, eff_err = calc_eff(tree_sel, tree_acc, binsName, ccbar)

    logging.info(f"{ccbar} rec: {round(eff_val,3)} +/- {round(eff_err,3)}")

    return hist_eff, eff_val, eff_err

def MCID_eff(ccbar:str="etac", source:str="prompt", binsName:str="PT"):

    truePTCut = cut_TRUEPT[source]
    if source=="prompt":
        src = "prompt"
    elif source=="sec":            
        src = "fromB"
    else:
        src = "1"

    nt_rec = TChain("DecayTree")
    nt_sel = TChain("DecayTree")
    # nt_rec.Add(f"{dataDir}/MC/Trigger/{ccbar}DiProton_{src}_Mag*_2018.root")
    # nt_sel.Add(tuples(ccbar, "Trigger", "AddBr"))
    nt_rec.Add(f"{dataDir}/MC/Trigger/{ccbar}DiProton_{src}_2018_AddBr.root")
    nt_sel.Add(f"{dataDir}/MC/Trigger/{ccbar}DiProton_{src}_2018_AddBr.root")

    tree_rec = TTree()
    tree_sel = TTree()        

    cut_gen = "ProtonM_TRUEPT>{0} && ProtonP_TRUEPT>{0}".format(truePTCut)
    cut_tot = "{} && {}".format(cut_reco_v, cut_gen)

    tree_rec = nt_rec.CopyTree(cut_tot)
    tree_sel = nt_sel.CopyTree(f"{cut_tot} && {source}")

    logging.debug(f"{ccbar} MCmatch: {tree_sel.GetEntries()} / {tree_rec.GetEntries()}")
    hist_eff, eff_val, eff_err = calc_eff(tree_sel, tree_rec, binsName, ccbar)

    logging.info(f"{ccbar} MCmatch: {round(eff_val,3)} +/- {round(eff_err, 3)}")

    return hist_eff, eff_val, eff_err

def total_eff(ccbar:str="etac", source:str="prompt", binsName:str="PT", eff_list=["rec","gen","pid","l0"]):
    
    bins  = binDict[binsName]
    nBins = len(bins)-1
    h_tot  = TH1D("hist_eff","hist_eff", nBins, bins)
    for ib in range(1, nBins+1): 
        h_tot.SetBinContent(ib, 1.)
        h_tot.SetBinError(ib, 0.)
    h_tot.Sumw2()
    eff_val, eff_err = 1., 0.

    for eff_type in eff_list:
        h = TH1D()
        f = TFile(f"{homeDir}/plots_{source}/eff_{ccbar}_{source}_{binsName}_{eff_type}.root", "READ")
        h = f.Get("hist_eff")
        h.SetDirectory(0)
        tot = f.Get("eff_val").GetVal()
        err = f.Get("eff_err").GetVal()
        f.Close()
        h_tot.Multiply(h)
        eff_val *= tot
        eff_err += (err/tot)**2
        print(eff_val)

    eff_err = eff_val*(eff_err**0.5)
    return h_tot, eff_val, eff_err


effDict = {
    "mcid":     MCID_eff,
    "gen":      gen_eff,
    "rec":      rec_eff,
    "recsel":   recsel_eff,
    "pid_def":  pid_def_eff,
    "l0":       l0_eff,
    "l0_def":   l0_def_eff,
    "hlt":      trigger_eff,
    "turbo":    turbo_eff,
    "tot":      total_eff,
 }

def efficiency(eff_type, *args):
    return effDict[eff_type](*args)

def all_eff(ccbar, source, binsName, eff_list, draw=True):

    nameTxt = f"{homeDir}/plots_{source}/{ccbar}_{source}.txt"
    with open(nameTxt, "w") as f:

        for eff_type in eff_list:

            filename = f"{homeDir}/plots_{source}/eff_{ccbar}_{source}_{binsName}_{eff_type}.root"
            hist, eff_val, eff_err = efficiency(eff_type,ccbar,source,binsName)
            hist.Print()
            write2file(filename, hist, eff_val, eff_err)
            # eff_val = TParameter(float)("eff_val", eff_val)
            # eff_err = TParameter(float)("eff_err", eff_err)
            # file = TFile("plots_{}/eff_{}_{}_{}_{}.root".format(source,ccbar,source,binsName,eff_type), "RECREATE")
            # hist.Write()
            # file.WriteObject(eff_val, "eff_val")
            # file.WriteObject(eff_err, "eff_err")
            # file.Close()
            # hist.SaveAs("eff_{}_{}_{}_{}.root".format(ccbar,source,binsName,eff_type))
            # f.write("{}: {} +/- {} \n".format(eff_type, eff_val.GetVal(), eff_err.GetVal()))
            f.write("{}: {} \\pm {} \n".format(eff_type, eff_val, eff_err))

            if draw:
                canv = plot_hist(hist, binsName, eff_val, eff_err)
                canv.SaveAs("{}/plots_{}/eff_{}_{}_{}_{}.pdf".format(homeDir,source,ccbar,source,binsName,eff_type))

def ratio_eff(ccbar1, ccbar2, source, binsName, eff_list=["gen","tot"]):

    nameTxt = f"{homeDir}/plots_{source}/{ccbar1}2{ccbar2}_{source}_{binsName}.txt"
    bins = binDict[binsName]
    nBins = len(bins)-1
    print(f"{ccbar1}/{ccbar2}")
    with open(nameTxt, "w") as f:

        for eff_type in eff_list:
            print(f"Computing {eff_type} efficiency ratio for {source} between {ccbar1} and {ccbar2}")
            f1 = TFile(f"{homeDir}/plots_{source}/eff_{ccbar1}_{source}_{binsName}_{eff_type}.root", "READ")
            hist1 = f1.Get("hist_eff")
            eff1_val = f1.Get("eff_val").GetVal()
            eff1_err = f1.Get("eff_err").GetVal()
            f2 = TFile(f"{homeDir}/plots_{source}/eff_{ccbar2}_{source}_{binsName}_{eff_type}.root", "READ")
            hist2 = f2.Get("hist_eff")
            eff2_val = f2.Get("eff_val").GetVal()
            eff2_err = f2.Get("eff_err").GetVal()

            hist = TH1D(f"hist_{eff_type}",f"hist_{eff_type}", nBins, bins)
            hist.Divide(hist1, hist2)
        
            eff_val = eff1_val / eff2_val 
            eff_err = eff_val * ( (eff1_err/eff1_val)**2 + (eff2_err/eff2_val)**2 )**0.5

            rval = 3 if eff_type in ["rec","tot"] else 2
            # rval = 3
            if ccbar1=="jpsi" and ccbar2=="etac" and PRINTTABLE:
                for i, valBin in enumerate(bins[:-1]):
                    print(f"{valBin/1000.:.1f}-{bins[i+1]/1000.:.1f}",end=" ")
                    # print(f"{valBin:.1f}-{bins[i+1]:.1f}",end=" ")
                    print(f"& {round(hist1.GetBinContent(i+1),rval)} $\\pm$ {round(hist1.GetBinError(i+1),rval)}",end=" ")
                    print(f"& {round(hist2.GetBinContent(i+1),rval)} $\\pm$ {round(hist2.GetBinError(i+1),rval)}",end=" ")
                    print(f"& {round(hist.GetBinContent(i+1),2)} $\\pm$ {round(hist.GetBinError(i+1),2)} \\\\")
                    # print(f"& {hist.GetBinError(i+1)} {hist.GetBinContent(i+1)}  \\\\")
                    # print(f"& {round(100*hist.GetBinError(i+1)/hist.GetBinContent(i+1),1)}  \\\\")
            print(f"{bins[0]/1000.:.1f}-{bins[-1]/1000.:.1f}",end=" ")
            # print(f"{bins[0]:.1f}-{bins[-1]:.1f}",end=" ")
            print(f"& {round(eff2_val,rval)} $\\pm$ {round(eff2_err,rval)}",end=" ")
            print(f"& {round(eff1_val,rval)} $\\pm$ {round(eff1_err,rval)}",end=" ")
            print(f"& {round(eff_val,3)} $\\pm$ {round(eff_err,3)} \\\\")
            # print(f"& {round(100*eff_err/eff_val,1)} \\\\")

            filename = f"{homeDir}/plots_{source}/ratio_{ccbar1}2{ccbar2}_{source}_{binsName}_{eff_type}"
            if PLOT:
                canv = plot_hist(hist, binsName, eff_val, eff_err)
                canv.SaveAs(filename+".pdf")
            write2file(filename+".root", hist, eff_val, eff_err)
            
            f.write(f"{eff_type}: {eff_val:.4f} +/- {eff_err:.4f} \n")
            f1.Close()
            f2.Close()


            del hist 
            if PLOT: del canv
            # for i in range(1,7):
            #     print(hist.GetBinContent(i))
            # print("{}  {}: {} +/- {}".format(eff_type, ccbar1, eff1_val, eff1_err))
            # print("{}  {}: {} +/- {}".format(eff_type, ccbar2, eff2_val, eff2_err))
    # print("{} to {}: {} +/- {}".format(ccbar1, ccbar2, eff_val, eff_err))
    # #os.dup2( save, sys.stdout.fileno() )
    # #newout.close()
        

if __name__ == "__main__":
    '''
    !!!!!!!!!!!!!!
    rec efficiency has to be computed with Trigger ON for the total eff
    and with Trigger OFF for rec+sel efficiency
    DO NOT RUN l0_eff for Y bins
    !!!!!!!!!!!!!!
    '''
    gROOT.LoadMacro("../libs/lhcbStyle.C")
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
    
    # binsList = ["PT","Y"]
    binsList = ["PT"]
    # eff_list = ["gen","rec","pid_def","tot"]
    # eff_list = ["gen","rec","l0","pid","tot"]
    eff_list = ["tot"]
    # eff_list = ["l0_def"]
    # eff_list = ["mcid"]
    # eff_list = ["turbo"]
    # eff_list = ["gen","rec","recsel","l0","hlt"]
    # eff_list = ["gen"]
    # states = ["jpsi", "etac","chic0","chic1","chic2", "etac2S", "psi2S", "Etac", "Jpsi"] # 
    # states = ["jpsi", "etac","chic0","chic1", "chic2", "etac2S", "psi2S"] # 
    states = ["chic0","chic1", "chic2", "etac2S", "psi2S"] # 
    # states = ["pppi0","Jpsi"]
    # states = ["jpsi"]
    # states = ["Etac","jpsi"]
    # states = ["etac","jpsi"]

    # sources = ["all"]
    sources = ["sec","prompt"]
    # sources = ["prompt"]
    # eff_list = ["gen", "recsel", "hlt", "pid", "tot"]
    # print_eff(eff_list, binsName)
    for source in sources:
    #     # check_entries("prod", source)
        for binsName in binsList:
            # for ccbar in states:
            #     all_eff(ccbar, source, binsName, eff_list)

            # ratio_eff('pppi0','Jpsi',source, binsName, eff_list)
            # ratio_eff('pppi0','jpsi',source, binsName, eff_list)
            # ratio_eff('chic0','Etac', source, binsName, eff_list)
            # ratio_eff('chic1','Etac', source, binsName, eff_list)
            # ratio_eff('chic2','Etac', source, binsName, eff_list)
            # ratio_eff('jpsi','Etac', source, binsName, eff_list)
            # ratio_eff('psi2S','Etac', source, binsName, eff_list)

            # ratio_eff('jpsi', 'etac', source, binsName, eff_list)
            # ratio_eff('jpsi', 'chic0', source, binsName, eff_list)
            # ratio_eff('jpsi', 'chic1', source, binsName, eff_list)
            # ratio_eff('jpsi', 'chic2', source, binsName, eff_list)
            # ratio_eff('jpsi', 'etac2S', source, binsName, eff_list)
            # ratio_eff('jpsi', 'psi2S', source, binsName, eff_list)

            ratio_eff('chic0', 'chic2', source, binsName, eff_list)
            ratio_eff('chic0', 'chic1', source, binsName, eff_list)
            # ratio_eff('chic0', 'chic2', source, binsName, eff_list)
            # ratio_eff('chic1', 'chic2', source, binsName, eff_list)
            # ratio_eff('etac', 'jpsi', source, binsName, eff_list)
            # ratio_eff('etac', 'chic0', source, binsName, eff_list)
            # ratio_eff('etac', 'chic1', source, binsName, eff_list)
            # ratio_eff('etac', 'chic2', source, binsName, eff_list)
            # ratio_eff('etac', 'etac2S', source, binsName, eff_list)
            # ratio_eff('etac', 'psi2S', source, binsName, eff_list)
            # ratio_eff('etac','jpsi', source, binsName, eff_list)
            # ratio_eff('psi2S','jpsi', source, binsName, eff_list)
            # ratio_eff('etac2S','jpsi', source, binsName, eff_list)
            # ratio_eff('chic0','jpsi', source, binsName, eff_list)
            # ratio_eff('chic1','jpsi', source, binsName, eff_list)
            # ratio_eff('chic2','jpsi', source, binsName, eff_list)
            # ratio_eff('psi2S','etac2S', source, binsName, eff_list)
            # ratio_eff('etac2S','etac', source, binsName, eff_list)
            # ratio_eff('chic0','etac', source, binsName, eff_list)
            # ratio_eff('chic1','etac', source, binsName, eff_list)
            # ratio_eff('chic2','etac', source, binsName, eff_list)
            # ratio_eff('chic1','chic0', source, binsName, eff_list)
            # ratio_eff('chic2','chic0', source, binsName, eff_list)
            # ratio_eff('chic2','chic1', source, binsName, eff_list)

