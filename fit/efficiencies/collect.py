from ROOT import TFile, TMultiGraph, TLegend, TCanvas, TGraphErrors, gROOT, gStyle, nullptr
from array import array

from efficiency import *
import logging

homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/eff/note_v2/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"
gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")

all_states = {
            "jpsi" :    ["\\jpsi(1S)", "J/#psi(1S)"],
            "etac" :    ["\\etac(1S)", "#eta_{c}(1S)"],
            "chic0":    ["\\chiczero(1P)", "#chi_{c0}(1P)"],
            "chic1":    ["\\chicone(1P)", "#chi_{c1}(1P)"],
            "chic2":    ["\\chictwo(1P)", "#chi_{c2}(1P)"],
            "etac2S":   ["\\etactwos","#eta_{c}(2S)"], 
            "psi2S":    ["\\psitwos", "#psi(2S)"]
        }

def collect_eff(ccbar_ref, eff_type, source):

    states = ["jpsi", "etac","chic0","chic1","chic2", "etac2S", "psi2S"] # 
    states.remove(ccbar_ref)
    # nameTxt = "{}/plots_misID_{}/eff2{}_{}_{}.txt".format(homeDir,source, ccbar_ref, source, eff_type)
    nameTxt = "{}/plots_{}/eff2{}_{}_{}.txt".format(homeDir,source, ccbar_ref, source, eff_type)
    with open(nameTxt, "w") as f:
        # f1 = TFile("{}/plots_misID_{}/eff_{}_{}_{}_{}.root".format(homeDir,source,ccbar_ref,source,binsName,eff_type), "READ")
        f1 = TFile("{}/plots_{}/eff_{}_{}_{}_{}.root".format(homeDir,source,ccbar_ref,source,binsName,eff_type), "READ")
        eff1_val = f1.Get("eff_val").GetVal()
        eff1_err = f1.Get("eff_err").GetVal()
        for ccbar in states:
            # f2 = TFile("{}/plots_misID_{}/eff_{}_{}_{}_{}.root".format(homeDir,source,ccbar,source,binsName,eff_type), "READ")
            f2 = TFile("{}/plots_{}/eff_{}_{}_{}_{}.root".format(homeDir,source,ccbar,source,binsName,eff_type), "READ")
            eff2_val = f2.Get("eff_val").GetVal()
            eff2_err = f2.Get("eff_err").GetVal()

            eff_val = eff1_val / eff2_val 
            eff_err = eff_val * ( (eff1_err/eff1_val)**2 + (eff2_err/eff2_val)**2 )**0.5
            
            f.write("{}: {} {} \n".format(ccbar, eff_val, eff_err))
            f2.Close()
        f1.Close()

def print_eff(eff_list,binsName):

    sources = ["prompt", "sec"]

    nameTxt = f"{homeDir}/eff_table_{binsName}.txt"
    with open(nameTxt, "w") as f:
        for eff_type in eff_list:
            f.write(f"{eff_type} \t prompt \t from-b \n ")
            for ccbar in all_states:
                f.write("${}$ \t".format(all_states[ccbar][0]))

                for source in sources:
                    # f_eff = TFile(f"{homeDir}/plots_misID_{source}/eff_{ccbar}_{source}_{binsName}_{eff_type}.root", "READ")
                    f_eff = TFile(f"{homeDir}/plots_{source}/eff_{ccbar}_{source}_{binsName}_{eff_type}.root", "READ")
                    eff_val = f_eff.Get("eff_val").GetVal()
                    eff_err = f_eff.Get("eff_err").GetVal()
        
                    f.write(f"& ${eff_val:.3f} \\pm {eff_err:.3f}$ \t")
                    f_eff.Close()
                f.write("\\\\ \n")
    logging.info(f"File {homeDir}/eff_table_{binsName}.txt is written")


    bins  = binDict[binsName]
    nBins = len(bins)-1

    for source in sources:
        nameTxt = f"{homeDir}/eff_table_{source}_{binsName}.txt"
        with open(nameTxt, "w") as f:
            for eff_type in eff_list:
                f.write("{} {}".format(eff_type,binsName))
                for iB in range(1, nBins+1):
                    f.write("\t & {}-{} ".format(bins[iB-1]/1000., bins[iB]/1000.))
                f.write(" \\\\ \\hline \n ".format(eff_type))
                for ccbar in all_states:
                    # print(ccbar)
                    f.write("${}$ \t".format(all_states[ccbar][0]))
                    # f_eff = TFile("{}/plots_misID_{}/eff_{}_{}_{}_{}.root".format(homeDir,source,ccbar,source,binsName,eff_type), "READ")
                    f_eff = TFile("{}/plots_{}/eff_{}_{}_{}_{}.root".format(homeDir,source,ccbar,source,binsName,eff_type), "READ")
                    hist    = f_eff.Get("hist_eff")
                    eff_val = f_eff.Get("eff_val").GetVal()
                    eff_err = f_eff.Get("eff_err").GetVal()
                    # eff_val_calc = 0.
                    # eff_err_calc = 0.
                    for iB in range(1, nBins+1):
                        f.write("& ${:.3f} \\pm {:.3f}$ \t".format(hist.GetBinContent(iB), hist.GetBinError(iB)))
                        # eff_val_calc += hist.GetBinContent(iB)/hist.GetBinError(iB)**2
                        # eff_err_calc += 1./hist.GetBinError(iB)**2
                    # eff_val_calc = eff_val_calc/eff_err_calc
                    # eff_err_calc = eff_err_calc**(-0.5)
                    # print("mean: {:.3f} +/- {:.3f}".format(eff_val_calc, eff_err_calc))
                    # print("calc: {:.3f} +/- {:.3f}".format(eff_val, eff_err))
                    f_eff.Close()
                    f.write("\\\\ \n")
                f.write("\\\\ \n")
        logging.info(f"File {nameTxt} is written")


def plot_eff(states,eff_list,binsName):
    '''function to plot efficiencies for defferent states on the same plot'''

    gStyle.SetPalette(52, nullptr)

    sources = ["prompt", "sec"]
    bins  = binDict[binsName]
    nBins = len(bins)-1
    from ROOT import kOcean

    for eff_type in eff_list:
        for source in sources:
            c = TCanvas()
            c.cd()
            mg = TMultiGraph()
            # gStyle.SetPalette(kOcean)
            if binsName == "PT":
                legend = TLegend(0.75,0.2,0.88,0.45)
            else:
                legend = TLegend(0.20,0.2,0.33,0.45)
            i=0
            for ccbar in states:
                # f_eff = TFile("{}/plots_misID_{}/eff_{}_{}_{}_{}.root".format(homeDir,source,ccbar,source,binsName,eff_type), "READ")
                f_eff = TFile(f"{homeDir}/plots_{source}/eff_{ccbar}_{source}_{binsName}_{eff_type}.root", "READ")
                hist    = f_eff.Get("hist_eff")
                # eff_val = f_eff.Get("eff_val").GetVal()
                # eff_err = f_eff.Get("eff_err").GetVal()
                hist.SetDirectory(0)
                f_eff.Close()

                graph = TGraphErrors(hist)

                gPad.SetLeftMargin(0.15)
                hist.ClearUnderflowAndOverflow()

                graph.SetLineColor(i+1)
                # graph.SetMarkerSize(0)
                graph.SetMarkerColor(i+1)
                graph.SetFillColor(i+1)
                graph.SetFillStyle(3003)
                mg.Add(graph)
                legend.AddEntry(graph,"{}".format(all_states[ccbar][1]),"lep")
                # legend.AddEntry(hist,"{}".format(all_states[ccbar][1]),"f");

                i+=1

            # mg.Draw("a5")
            mg.Draw("ap")
            # mg.GetXaxis().SetTitle("|y|")
            mg.SetMinimum(0.)
            mg.SetMaximum(mg.GetHistogram().GetMaximum()*1.1)
            if binsName=="PT":
                mg.GetXaxis().SetTitle("p_{T} [GeV/c]")
            else:
                mg.GetXaxis().SetTitle("y")
            legend.Draw()
            # gPad.BuildLegend();
            c.SaveAs(f"{homeDir}/eff_{source}_{binsName}_{eff_type}.pdf")

def overlap_eff(states,eff_list,binsName):
    '''function to plot defferent efficiencies on the same plot'''

    gStyle.SetPalette(52, nullptr)

    sources = ["prompt", "sec"]
    bins  = binDict[binsName]
    nBins = len(bins)-1
    from ROOT import kOcean

    for ccbar in states:
        for source in sources:
            c = TCanvas()
            c.cd()
            mg = TMultiGraph()
            # gStyle.SetPalette(kOcean)
            legend = TLegend(0.65,0.35,0.88,0.55)
            i=0
            for eff_type in eff_list:
                f_eff = TFile("{}/plots_misID_{}/eff_{}_{}_{}_{}.root".format(homeDir,source,ccbar,source,binsName,eff_type), "READ")
                hist    = f_eff.Get("hist_eff")
                # eff_val = f_eff.Get("eff_val").GetVal()
                # eff_err = f_eff.Get("eff_err").GetVal()
                hist.SetDirectory(0)
                f_eff.Close()

                graph = TGraphErrors(hist)

                # only for l0
                # if graph.GetPointX(1) < 1e3:
                #     for iP in range(graph.GetN()):
                #         graph.SetPointX(iP, 1000.*graph.GetPointX(iP))
                #         graph.SetPointError(iP, 1000.*graph.GetErrorX(iP), graph.GetErrorY(iP))

                gPad.SetLeftMargin(0.15)
                hist.ClearUnderflowAndOverflow()

                graph.SetLineColor(i+1)
                graph.SetMarkerSize(0.2)
                graph.SetMarkerColor(i+1)
                graph.SetFillColor(i+1)
                graph.SetFillStyle(3003)
                mg.Add(graph)
                if "def" in eff_type:
                    title = "MC sample"
                else:
                    title = "Efficiency tables"

                legend.AddEntry(graph,"{}".format(title),"lep");
                # legend.AddEntry(hist,"{}".format(all_states[ccbar][1]),"f");

                i+=1

            # mg.Draw("a5")
            mg.Draw("ap")
            # mg.GetXaxis().SetTitle("|y|")
            mg.SetMinimum(0.)
            # mg.SetMaximum(1.1 )
            mg.GetXaxis().SetTitle("p_{T} [GeV/c]")
            legend.Draw()
            # gPad.BuildLegend();
            c.SaveAs("{}/eff_{}_{}_{}_pid.pdf".format(homeDir,source,binsName,ccbar))

def check_entries(tree_type, source):


    if source=="prompt":
        src="prompt"
    else:    
        src="fromB"

    tree_list = {}
    if tree_type=="rec":
        tree_list = recoTuples
        tree_name = "DecayTree"
    elif tree_type=="prod":
        tree_list = genTuples
        tree_name = "MCDecayTree"
    else:
        tree_list = genNoCutTuples[source]
        tree_name = "MCDecayTree"

    for ccbar in tree_list:
        nt = TChain(tree_name)
        file_name = "{}/MC/NoTrigger/{}DiProton_{}_2018_AddBr.root".format(dataDir, ccbar, src)
        # nt.Add(tree_list[ccbar])
        nt.Add(file_name)
        n_evts = nt.GetEntries()
        # n_evts = nt.GetEntries(src)
        print("{} {}: {} events".format(ccbar, source, n_evts))

if __name__ == "__main__":

    gROOT.LoadMacro("../libs/lhcbStyle.C")

    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)

    binsName = "PT"
    eff_list = ["gen","rec","recsel","l0","pid","hlt","tot"]
    eff_list = ["tot"]
    # eff_list = ["pid_def","pid"]
    # states = ["jpsi", "etac","chic0","chic1","chic2", "etac2S", "psi2S", "Etac", "Jpsi"] # 
    states = ["jpsi", "etac","chic0","chic1","chic2", "etac2S", "psi2S"] # 
    states = ["jpsi", "etac"] # 

    # sources = ["sec"]
    sources = ["prompt", "sec"]
    # eff_list = ["gen", "recsel", "rec", "l0", "hlt", "pid", "tot"]
    print_eff(eff_list, binsName)
    # plot_eff(states, eff_list, binsName)
    # overlap_eff(states, eff_list, binsName)
    # for source in sources:
    #     check_entries("prod", source)
