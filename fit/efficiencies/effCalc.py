from ROOT import *
from array import array
homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"
gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")

            #10132000: "psi(2S)",
            #10132010: "J/psi(1S)",
            #10132030: "chi_c0(1P)",
            #10132040: "chi_c1(1P)",
            #10132050: "chi_c2(1P)",
            #10132060: "eta_c(1S)",
            #10132080: "eta_c(2S)",
            #24102002: "J/psi(1S)",
            #24102011: "J/psi(1S)", #prompt etac
            #24102013: "J/psi(1S)",
            #28102000: "psi(2S)",
            #28102001: "psi(2S)", # prompt "eta_c(2S)",
            #28102002: "psi(2S)",
            #28102033: "chi_c0(1P)",
            #28102043: "chi_c1(1P)",
            #28102053: "chi_c2(1P)",

genNoCutTuples = {
    "prompt":
    {    "etac":   dataDir + "/MC/Generator/Tuple-24102011-Total_AddBr.root",
        "jpsi":   dataDir + "/MC/Generator/Tuple-24102013-Total_AddBr.root",
        # "jpsi":   dataDir + "/MC/Generator/Tuple-24102002-Total_AddBr.root",
        "etac2S": dataDir + "/MC/Generator/Tuple-28102001-Total_AddBr.root",
        "chic0":  dataDir + "/MC/Generator/Tuple-28102033-Total_AddBr.root",
        "chic1":  dataDir + "/MC/Generator/Tuple-28102043-Total_AddBr.root",
        "chic2":  dataDir + "/MC/Generator/Tuple-28102053-Total_AddBr.root",
        # "psi2S":  dataDir + "/MC/Generator/Tuple-28102002-Total_AddBr.root",
        "psi2S":  dataDir + "/MC/Generator/Tuple-28102000-Total_AddBr.root",
    },
    "sec":
    {    "etac":   dataDir + "/MC/Generator/Tuple-10132060-Total_AddBr.root",
        "jpsi":   dataDir + "/MC/Generator/Tuple-10132010-Total_AddBr.root",
        "etac2S": dataDir + "/MC/Generator/Tuple-10132080-Total_AddBr.root",
        "chic0":  dataDir + "/MC/Generator/Tuple-10132030-Total_AddBr.root",
        "chic1":  dataDir + "/MC/Generator/Tuple-10132040-Total_AddBr.root",
        "chic2":  dataDir + "/MC/Generator/Tuple-10132050-Total_AddBr.root",
        "psi2S":  dataDir + "/MC/Generator/Tuple-10132000-Total_AddBr.root",
    }
}

genTuples = {
    "etac":   dataDir + "/MC/NoTrigger/etacDiProton_*_2018_AddBr.root",
    "jpsi":   dataDir + "/MC/NoTrigger/jpsiDiProton_*_2018_AddBr.root",
    "etac2S": dataDir + "/MC/NoTrigger/etac2SDiProton_*_2018_AddBr.root",
    "chic0":  dataDir + "/MC/NoTrigger/chic0DiProton_*_2018_AddBr.root",
    "chic1":  dataDir + "/MC/NoTrigger/chic1DiProton_*_2018_AddBr.root",
    "chic2":  dataDir + "/MC/NoTrigger/chic2DiProton_*_2018_AddBr.root",
    "psi2S":  dataDir + "/MC/NoTrigger/psi2SDiProton_*_2018_AddBr.root",
}


recoTuples = {
    "etac":   dataDir + "/MC/Trigger/etacDiProton_*_2018_AddBr.root",
    "jpsi":   dataDir + "/MC/Trigger/jpsiDiProton_*_2018_AddBr.root",
    "etac2S": dataDir + "/MC/Trigger/etac2SDiProton_*_2018_AddBr.root",
    "chic0":  dataDir + "/MC/Trigger/chic0DiProton_*_2018_AddBr.root",
    "chic1":  dataDir + "/MC/Trigger/chic1DiProton_*_2018_AddBr.root",
    "chic2":  dataDir + "/MC/Trigger/chic2DiProton_*_2018_AddBr.root",
    "psi2S":  dataDir + "/MC/Trigger/psi2SDiProton_*_2018_AddBr.root",
}


cut_accept  = "ProtonP_TRUETHETA>0.01 && ProtonP_TRUETHETA<0.4 && ProtonM_TRUETHETA>0.01 && ProtonM_TRUETHETA<0.4"#" && 0.5*TMath::Log((Jpsi_TRUEP_E + Jpsi_TRUEP_Z)/(Jpsi_TRUEP_E-Jpsi_TRUEP_Z))>2. && 0.5*TMath::Log((Jpsi_TRUEP_E + Jpsi_TRUEP_Z)/(Jpsi_TRUEP_E-Jpsi_TRUEP_Z))<4.5 "#" && nSPDHits < 300" #
cut_true_v  = "Jpsi_TRUEPT > 5000 && Jpsi_TRUEPT < 20000 && Jpsi_TRUEY > 2.0 && Jpsi_TRUEY < 4.5"
cut_reco_v  = "Jpsi_PT > 5000 && Jpsi_PT < 20000 && Jpsi_Y > 2.0 && Jpsi_Y < 4.5"
cut_PID     = "ProtonP_ProbNNk < 0.6 && ProtonM_ProbNNk < 0.6 && ProtonP_ProbNNp > 0.6 && ProtonM_ProbNNp > 0.6 && ProtonP_ProbNNpi < 0.6 && ProtonM_ProbNNpi < 0.6 "
cut_Trigger = "Jpsi_L0HadronDecision_TOS && (Jpsi_Hlt1DiProtonDecision_TOS || Jpsi_Hlt1DiProtonHighDecision_TOS)"


cut_select  = "     nSPDHits < 300 && \
                    ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
                    ProtonP_PT>2000 && ProtonM_PT>2000 && \
                    ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366"
#cut_select  = "     nSPDHits < 300 && \
                    #ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
                    #ProtonP_PT>2000 && ProtonM_PT>2000 && \
                    #ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366 && \
                    #Jpsi_PT>5000 && Jpsi_PT<20000 && \
                    #ProtonP_TRACK_CHI2NDOF<2.5 && ProtonM_TRACK_CHI2NDOF<2.5 && \
                    #ProtonP_TRACK_CloneDist<0 && ProtonM_TRACK_CloneDist<0 && \
                    #ProtonP_PIDp>5 && ProtonM_PIDp>5 && \
                    #(ProtonP_PIDp-ProtonP_PIDK)>0 && (ProtonM_PIDp-ProtonM_PIDK)>0 && \
                    #ProtonP_TRACK_GhostProb<0.2 && ProtonM_TRACK_GhostProb<0.2"

#cutAccept_th = "Jpsi_TRUEPT>6500 && Jpsi_TRUEPT<14000 && Jpsi_TRUEETA>2. && Jpsi_TRUEETA<4.5"

#Y = "0.5*TMath::Log((Jpsi_TRUEP_E + Jpsi_TRUEP_Z)/(Jpsi_TRUEP_E-Jpsi_TRUEP_Z))"
Y = "Jpsi_TRUEPT"

binDict = {
    "PT"    : array("f",[5000., 6500, 8000., 10000., 12000., 14000., 20000.]),
    "Y"     : array("f",[2., 2.5, 3., 3.5, 4.]),
    "nSPD"  : array("f",[0., 75., 150., 225., 300.]),
}

varDict = {
    "DecayTree":
            {
                "PT"    : "Jpsi_PT",
                "Y"     : "Jpsi_Y",
                "nSPD"  : "nSPDHits"
            },
    "MCDecayTree":
            {
                "PT"    : "Jpsi_TRUEPT",
                "Y"     : "Jpsi_TRUEY",
                # "Y"     : "0.5*TMath::Log((Jpsi_TRUEP_E + Jpsi_TRUEP_Z)/(Jpsi_TRUEP_E-Jpsi_TRUEP_Z))",
                "nSPD"  : "nSPDHits"
            },
    "MCDecayTreeTuple/MCDecayTree":
            {
                "PT"    : "Jpsi_TRUEPT",
                "Y"     : "Jpsi_TRUEY",
                # "Y"     : "0.5*TMath::Log((Jpsi_TRUEP_E + Jpsi_TRUEP_Z)/(Jpsi_TRUEP_E-Jpsi_TRUEP_Z))",
                "nSPD"  : "nSPDHits"
            }
        }

massDict = {
    "etac"  : [2910, 3060],
    "jpsi"  : [3025, 3175],
    "etac2S": [3565, 3715],
    "chic0" : [3340, 3490],
    "chic1" : [3435, 3585],
    "chic2" : [3490, 3630],
    "psi2S" : [3610, 3760],
}


gammaDict = {
    "etac"  : 32.0,
    "jpsi"  : 0.,
    "etac2S": 11.3,
    "chic0" : 10.8,
    "chic1" : 0.84,
    "chic2" : 1.97,
    "psi2S" : 0.,
}


GAMMA_ETAC2S  = 11.3 #-2.9 +3.2
GAMMA_ETAC    = 32.0 #0.7
GAMMA_CHIC0   = 10.8 #0.6
GAMMA_CHIC1   = 0.84 #0.04
GAMMA_CHIC2   = 1.97 #0.09
GAMMA_HC      = 0.70 #0.40
GAMMA_PSI3770 = 27.2 #1.0

def plot_hist(hist, binsName="PT"):

    bins = binDict[binsName]

    canv = TCanvas("canv", "canv", 1000, 800)
    canv.cd(1)
    gPad.SetLeftMargin(0.15)
    hist.GetXaxis().SetTitle(binsName)
    hist.Draw("E1")
    hist.GetXaxis().SetRangeUser(bins[0]-1.,bins[-1]+1.)
    return canv

def get_entries(tree, cut, ccbar, draw=False):


    if tree.GetName()=="DecayTree":

        tree_fit = TTree()
        tree_fit = tree.CopyTree(cut)

        #Jpsi_M = RooRealVar ('Jpsi_M_res','Jpsi_M_res', -30., 30.)
        min_m, max_m = massDict[ccbar]
        gamma = gammaDict[ccbar]
        Jpsi_M = RooRealVar ('Jpsi_M','Jpsi_M', min_m, max_m)
        ds = RooDataSet('ds','ds',tree_fit,RooArgSet(Jpsi_M))

        n_ccbar = RooRealVar('n_ccbar','num of _etac', 1e3, 10, 1.e7)
        mean_ccbar = RooRealVar('mean_ccbar','mean of gaussian', (min_m+max_m)/2., min_m, max_m)
        gamma_ccbar = RooRealVar('gamma_ccbar','mean of gaussian', gamma)
        sigma_ccbar = RooRealVar('sigma_ccbar','width of gaussian', 9., 0.1, 30.)

        f = TFile("{}/MC/mass_fit/MC_M_prompt_wksp_sim.root".format(homeDir),"READ")
        w = f.Get("w")
        f.Close()
        r_yield = w.var("r_G1ToG2").getValV()
        r_sigma = w.var("r_NToW").getValV()
        # sigma_ccbar_2 = RooRealVar('sigma_ccbar_2','width of gaussian', 9., 0.1, 30.)
        sigma_ccbar_2 = RooFormulaVar('sigma_ccbar_2','sigma_ccbar/{}'.format(r_sigma),RooArgList(sigma_ccbar))
        f = RooRealVar('f','num of _etac', r_yield)

        #Fit J/psi
        if ccbar=="jpsi" or ccbar=="psi2S":
            gauss_1 = RooGaussian('gauss_1','gaussian PDF',Jpsi_M, mean_ccbar, sigma_ccbar)
            gauss_2 = RooGaussian('gauss_2','gaussian PDF',Jpsi_M, mean_ccbar, sigma_ccbar_2)
            resol = RooAddPdf('resol','gaussian PDF',RooArgList(gauss_1, gauss_2), RooArgList(f))
        else:
            bwxg_1 = RooVoigtian("bwxg_1", "bwxg", Jpsi_M, mean_ccbar, gamma_ccbar, sigma_ccbar)
            bwxg_2 = RooVoigtian("bwxg_2", "bwxg", Jpsi_M, mean_ccbar, gamma_ccbar, sigma_ccbar_2)
            resol = RooAddPdf('resol','gaussian PDF',RooArgList(bwxg_1, bwxg_2), RooArgList(f))


        #a0 = RooRealVar("a0","a0",0.4,-2.,2.)
        a0 = RooRealVar("a0","a0",0.4,-10.,10.)
        a1 = RooRealVar("a1","a1",0.05,-2.,2.)
        a2 = RooRealVar("a2","a2",-0.005,-2.,2.)
        #bkg = RooChebychev ("bkg","Background",Jpsi_M,RooArgList(a0,a1))
        bkg = RooGenericPdf("bkg","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.)",RooArgList(Jpsi_M,a0,a1))

        n_bkg = RooRealVar('n_bkg','num of etac', 1e3, 0, 5.e4)

        llist = RooLinkedList()
        llist.Add(RooCmdArg(RooFit.Offset(True)))
        llist.Add(RooCmdArg(RooFit.Extended(True)))
        llist.Add(RooCmdArg(RooFit.Save(True)))
        #llist.Add(RooCmdArg(RooFit.PrintLevel(-1)))

        model_ccbar = RooAddPdf('model_ccbar','ccbar signal', RooArgList(resol), RooArgList(n_ccbar))
        #model_ccbar = RooAddPdf('model_ccbar','ccbar signal', RooArgList(resol, bkg), RooArgList(n_ccbar, n_bkg))
        model_ccbar.fitTo(ds, llist)
        model_ccbar.fitTo(ds, llist)
        model_ccbar.fitTo(ds, llist)

        # draw = True
        if draw:
            frame = Jpsi_M.frame(RooFit.Title('#eta_c to p #bar{p} from_b'))

            ds.plotOn(frame)
            model_ccbar.plotOn(frame,RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
            model_ccbar.paramOn(frame,RooFit.Layout(0.68,0.99,0.99))
            c = TCanvas('Masses_Fit','Masses Fit',600,400)
            frame.Draw()
            # c.SaveAs("fit_{}.pdf".format(ccbar))
            input()

        n, n_err = n_ccbar.getValV(), n_ccbar.getError()
    else:
        n = tree.GetEntries(cut)
        n_err = n**0.5

    return n, n_err

def calc_eff(tree_1, tree_2, binsName, ccbar):

    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)

    draw = False 

    varName1 = varDict[tree_1.GetName()][binsName]
    varName2 = varDict[tree_2.GetName()][binsName]
    bins = binDict[binsName]
    nBins = len(bins)-1

    n_1 = tree_1.GetEntries("{0}>{1} && {0}<{2}".format(varName1,bins[0],bins[-1])) 
    print(varName1,bins[0],bins[-1])
    n_2 = tree_2.GetEntries("{0}>{1} && {0}<{2}".format(varName2,bins[0],bins[-1])) 
    er_1 = n_1**0.5
    er_2 = n_2**0.5
    # n_1, er_1 = get_entries(tree_1, "{0}>{1} && {0}<{2}".format(varName1,bins[0],bins[-1]), ccbar)
    # n_2, er_2 = get_entries(tree_2, "{0}>{1} && {0}<{2}".format(varName2,bins[0],bins[-1]), ccbar)

    eff_val = float(n_1)/float(n_2)
    eff_err = eff_val * ((er_1/n_1)**2 + (er_2/n_2)**2)**0.5
    #eff_err = eff_val * (1/float(n_1) + 1/float(n_2))**0.5
    # print(n_1, n_2)

    hist_eff = TH1D("hist_eff","hist_eff", nBins, bins)

    for iB in range(nBins):

        entr_1  = tree_1.GetEntries("{0}>{1} && {0}<{2}".format(varName1,bins[iB],bins[iB+1])) 
        entr_2  = tree_2.GetEntries("{0}>{1} && {0}<{2}".format(varName2,bins[iB],bins[iB+1])) 
        err_1   = entr_1**0.5
        err_2   = entr_2**0.5
        # entr_1, err_1  = get_entries(tree_1, "{0}>{1} && {0}<{2}".format(varName1,bins[iB],bins[iB+1]), ccbar, draw)
        # entr_2, err_2  = get_entries(tree_2, "{0}>{1} && {0}<{2}".format(varName2,bins[iB],bins[iB+1]), ccbar, draw)

        eff_bin     = float(entr_1)/float(entr_2)
        #eff_err_bin = eff_bin*(1/float(entr_1)+1/float(entr_2))**0.5
        eff_err_bin = eff_bin*((err_1/entr_1)**2 + (err_2/entr_2)**2)**0.5
        hist_eff.SetBinContent(iB+1,eff_bin)
        hist_eff.SetBinError(iB+1,eff_err_bin)
        # print("tree 1 : {}".format(entr_1) )
        # print("tree 2 : {}".format(entr_2) )
        # print("{} +/- {}".format(eff_bin, eff_err_bin) )

    return hist_eff, eff_val, eff_err


def calc_effTot(tree_1, tree_2, ccbar):

    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)

    #n_1 = tree_1.GetEntries()
    #n_2 = tree_2.GetEntries()
    n_1, er_1 = get_entries(tree_1, "", ccbar)
    n_2, er_2 = get_entries(tree_2, "", ccbar)

    eff_val = n_1/float(n_2)
    eff_err = eff_val * ((er_1/n_1)**2 + (er_1/n_1)**2)**0.5
    #eff_err = eff_val * (1/float(n_1) + 1/float(n_2))**0.5
    #print(n_1, n_2)
    return eff_val, eff_err

def gen_eff(ccbar="etac", source="prompt", binsName="PT"):

    nt = TChain("MCDecayTree")
    nt.Add(genNoCutTuples[source][ccbar])

    tree_acc = TTree()
    tree_gen = TTree()

    if source=="prompt":
        truePTCut = 1800
        # if "etac" in ccbar:
        #     truePTCut = 1800
        # else:
        #     truePTCut = 900
    else:    
        truePTCut = 0
        # if "psi" in ccbar:
        #     truePTCut = 900
        # else:
        #     truePTCut = 0

    cut_gen = "ProtonM_TRUEPT>{0} && ProtonP_TRUEPT>{0}".format(truePTCut)

    tree_gen = nt.CopyTree("{0} && {1}".format(cut_true_v, source))
    tree_acc = nt.CopyTree("{} && {} && {} && {}".format(cut_true_v, source, cut_gen, cut_accept)) 
    # tree_acc = nt.CopyTree("{} && {}".format(cut_accept, source)) #cut_gen, 

    hist_eff, eff_val, eff_err = calc_eff(tree_acc, tree_gen, binsName, ccbar)

    print("{} gen: {} +/- {}".format(ccbar, eff_val, eff_err))
    return hist_eff, eff_val, eff_err


def pid_eff(ccbar="etac", source="prompt", binsName="PT"):

    nt = TChain("DecayTree")
    nt.Add(recoTuples[ccbar])

    tree_rec = TTree()
    tree_pid = TTree()

    # if "etac" in ccbar:
        # truePTCut = 900
    if source=="prompt":
        truePTCut = 1800
    else:            
        truePTCut = 0

    cut_gen = "{1} && ProtonM_TRUEPT>{0} && ProtonP_TRUEPT>{0}".format(truePTCut, source)

    tree_rec = nt.CopyTree("{} && {} && {}".format(cut_reco_v,cut_gen, cut_select))
    tree_pid = nt.CopyTree("{} && {} && {} && {}".format(cut_reco_v, cut_gen, cut_select, cut_PID))

    hist_eff, eff_val, eff_err = calc_eff(tree_pid, tree_rec, binsName, ccbar)

    print("{} pid: {} +/- {}".format(ccbar, eff_val, eff_err))

    return hist_eff, eff_val, eff_err


def trigger_eff(ccbar="etac", source="prompt", binsName="PT"):

    nt = TChain("DecayTree")
    nt.Add(recoTuples[ccbar])

    tree_pid = TTree()
    tree_hlt = TTree()

    if source=="prompt":
        truePTCut = 1800
    else:            
        truePTCut = 0

    # if ccbar=="etac" or ccbar=="jpsi":
    #     cut_Trigger = "Jpsi_L0HadronDecision_TOS && Jpsi_Hlt1DiProtonDecision_TOS"
    # else:
    #     cut_Trigger = "Jpsi_L0HadronDecision_TOS && Jpsi_Hlt1DiProtonHighDecision_TOS"

    cut_gen = "{1} && ProtonM_TRUEPT>{0} && ProtonP_TRUEPT>{0}".format(truePTCut, source)
    cut_sel = "{} && {} && {} && {}".format(cut_reco_v, cut_gen, cut_select, cut_PID)

    tree_pid = nt.CopyTree("{}".format(cut_sel))
    tree_hlt = nt.CopyTree("{} && {}".format(cut_sel, cut_Trigger))

    hist_eff, eff_val, eff_err = calc_eff(tree_hlt, tree_pid, binsName, ccbar)

    print("{} hlt: {} +/- {}".format(ccbar, eff_val, eff_err))

    return hist_eff, eff_val, eff_err


def recsel_eff(ccbar="etac", source="prompt", binsName="PT"):

    nt_gen = TChain("MCDecayTree")
    nt_rec = TChain("DecayTree")
    nt_gen.Add(genTuples[ccbar])
    nt_rec.Add(recoTuples[ccbar])

    tree_acc = TTree()
    tree_sel = TTree()

    if source=="prompt":
        # cut_tz = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08"
        truePTCut = 1800
        # if "etac" in ccbar:
        #     truePTCut = 1800
        # else:
        #     truePTCut = 900
    else:            
        truePTCut = 0
        # cut_tz = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16"
        # if "psi" in ccbar:
        #     truePTCut = 900
        # else:
        #     truePTCut = 0
        

    cut_gen     = "ProtonM_TRUEPT>{0} && ProtonP_TRUEPT>{0} && {1}".format(truePTCut, source)
    cut_gen_rec = "ProtonM_PT>{0}     && ProtonP_PT>{0}     && {1}".format(truePTCut, source)

    tree_acc = nt_gen.CopyTree("{} && {} && {}".format(cut_true_v, cut_gen, cut_accept))
    tree_sel = nt_rec.CopyTree("{} && {} && {}  && {} && {}".format(cut_reco_v, cut_gen_rec, cut_select, cut_PID, cut_Trigger)) #cut_accept,
    # tree_sel = nt_rec.CopyTree("{} && {} && {}".format(cut_reco_v, cut_gen_rec, cut_select)) #cut_accept,

    hist_eff, eff_val, eff_err = calc_eff(tree_sel, tree_acc, binsName, ccbar)

    print("{} rec: {} +/- {}".format(ccbar, eff_val, eff_err))

    return hist_eff, eff_val, eff_err


def total_eff(ccbar="etac", source="prompt", binsName="PT"):

    h_gen, gen_tot, gen_err = gen_eff(ccbar,source,binsName)
    h_rec, rec_tot, rec_err = recsel_eff(ccbar,source,binsName)

    h_pid = TH1D()
    f1 = TFile("plots_{}/eff_{}_{}_{}_{}.root".format(source,ccbar,source,binsName,"pid"), "READ")
    h_pid = f1.Get("hist_eff")
    h_pid.SetDirectory(0)
    pid_tot = f1.Get("eff_val").GetVal()
    pid_err = f1.Get("eff_err").GetVal()
    f1.Close()
 
    # h_pid.Draw()
    bins  = binDict[binsName]
    nBins = len(bins)-1
    h_tot  = TH1D("hist_eff","hist_eff", nBins, bins)

    for i_b in range(1,nBins+1):
        eff = h_rec.GetBinContent(i_b)*h_pid.GetBinContent(i_b)*h_gen.GetBinContent(i_b)
        err = eff*((h_rec.GetBinError(i_b)/h_rec.GetBinContent(i_b))**2 + (h_gen.GetBinError(i_b)/h_gen.GetBinContent(i_b))**2 + (h_pid.GetBinError(i_b)/h_pid.GetBinContent(i_b))**2)**0.5
        h_tot.SetBinContent(i_b, eff)
        h_tot.SetBinError(i_b, err)



    eff_val = (rec_tot * gen_tot)
    eff_err = eff_val * ( (rec_err/rec_tot)**2 + (gen_err/gen_tot)**2 )**0.5
    return h_tot, eff_val, eff_err



effDict = {
    "pid": pid_eff,
    "hlt": trigger_eff,
    "gen": gen_eff,
    "rec": recsel_eff,
    "tot": total_eff
 }

def efficiency(eff_type, *args):
    return effDict[eff_type](*args)

def all_eff(ccbar, source, binsName, eff_list, draw=True):

    nameTxt = "plots_{}/{}_{}.txt".format(source,ccbar,source)
    with open(nameTxt, "w") as f:

        for eff_type in eff_list:
            hist, eff_val, eff_err = efficiency(eff_type,ccbar,source,binsName)
            eff_val = TParameter(float)("eff_val", eff_val)
            eff_err = TParameter(float)("eff_err", eff_err)
            file = TFile("plots_{}/eff_{}_{}_{}_{}.root".format(source,ccbar,source,binsName,eff_type), "RECREATE")
            hist.Write()
            file.WriteObject(eff_val, "eff_val")
            file.WriteObject(eff_err, "eff_err")
            file.Close()
            # hist.SaveAs("eff_{}_{}_{}_{}.root".format(ccbar,source,binsName,eff_type))
            f.write("{}: {} +/- {} \n".format(eff_type, eff_val.GetVal(), eff_err.GetVal()))

            if draw:
                canv = plot_hist(hist, binsName)
                canv.SaveAs("plots_{}/eff_{}_{}_{}_{}.pdf".format(source,ccbar,source,binsName,eff_type))

def ratio_eff(ccbar1, ccbar2, source, binsName, eff_list=["gen","tot"]):

    nameTxt = "plots_{}/{}2{}_{}_{}.txt".format(source, ccbar1,ccbar2, source, binsName)
    with open(nameTxt, "w") as f:

        for eff_type in eff_list:
            f1 = TFile("plots_{}/eff_{}_{}_{}_{}.root".format(source,ccbar1,source,binsName,eff_type), "READ")
            hist1 = f1.Get("hist_eff")
            eff1_val = f1.Get("eff_val").GetVal()
            eff1_err = f1.Get("eff_err").GetVal()
            f2 = TFile("plots_{}/eff_{}_{}_{}_{}.root".format(source,ccbar2,source,binsName,eff_type), "READ")
            hist2 = f2.Get("hist_eff")
            eff2_val = f2.Get("eff_val").GetVal()
            eff2_err = f2.Get("eff_err").GetVal()

            bins = binDict[binsName]
            nBins = len(bins)-1

            hist = TH1D("hist_{}".format(eff_type),"hist_{}".format(eff_type), nBins, bins)
            hist.Divide(hist1, hist2)
            # hist.SaveAs("ratio_{}2{}_{}_{}_{}.root".format(ccbar1,ccbar2,source,binsName,eff_type))
        
            canv = plot_hist(hist, binsName)
            canv.SaveAs("plots_{}/ratio_{}2{}_{}_{}_{}.pdf".format(source,ccbar1,ccbar2,source,binsName,eff_type))
            
            eff_val = eff1_val / eff2_val 
            eff_err = eff_val * ( (eff1_err/eff1_val)**2 + (eff2_err/eff2_val)**2 )**0.5

            f.write("{}: {} +/- {} \n".format(eff_type, eff_val, eff_err))
            f1.Close()
            f2.Close()

            # for i in range(1,7):
            #     print(hist.GetBinContent(i))
            # print("{}  {}: {} +/- {}".format(eff_type, ccbar1, eff1_val, eff1_err))
            # print("{}  {}: {} +/- {}".format(eff_type, ccbar2, eff2_val, eff2_err))
    # print("{} to {}: {} +/- {}".format(ccbar1, ccbar2, eff_val, eff_err))
    # #os.dup2( save, sys.stdout.fileno() )
    # #newout.close()


if __name__ == "__main__":

    gROOT.LoadMacro("../lhcbStyle.C")


    binsName = "PT"
    eff_list = ["gen","rec","hlt","tot"]
    eff_list = ["tot"]
    # states = ["jpsi","etac","chic0","chic1","chic2", "etac2S", "psi2S"] # 
    states = ["chic0", "chic1","chic2"] # "chic2", "etac2S", 
    # states = ["etac","etac2S","jpsi","psi2S"]

    sources = ["sec"]
    # sources = ["prompt", "sec"]
    for source in sources:
        for ccbar in states:
            all_eff(ccbar, source, binsName, eff_list)

        # ratio_eff('psi2S','jpsi', source, binsName, eff_list)
        # ratio_eff('psi2S','etac2S', source, binsName, eff_list)
        # ratio_eff('jpsi', 'etac',source, binsName, eff_list)
        # ratio_eff('etac2S','etac', source, binsName, eff_list)
        # ratio_eff('chic0','etac', source, binsName, eff_list)
        # ratio_eff('chic1','etac', source, binsName, eff_list)
        # ratio_eff('chic2','etac', source, binsName, eff_list)
        ratio_eff('chic1','chic0', source, binsName, eff_list)
        ratio_eff('chic2','chic0', source, binsName, eff_list)
        ratio_eff('chic2','chic1', source, binsName, eff_list)


    #h2_rec, recJpsi_tot, recJpsi_err = recoSel_eff("jpsi",source,binsName)
    #h1_rec, recEtac_tot, recEtac_err = recoSel_eff("etac",source,binsName)
    #h_rec, canv_rec = ratioHist(h1_rec, h2_rec, binsName)
    #print(canv_rec)
    #canv_rec.SaveAs('canv.pdf')
