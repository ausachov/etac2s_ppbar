from ROOT import *
from ROOT.RooFit import *
#from ROOT.RooStats import *
from ROOT import RooStats

gROOT.LoadMacro("../libs/libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../libs/libBifurcatedCB/BifurcatedCB.cxx+")

homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

import sys
sys.path.insert(1, "../mass_fit/")
sys.path.insert(1, "./")

from drawModule import *
import charmConstLib as cl

minM_MC = 2850
maxM_MC = 3250
#minM_MC = -100
#maxM_MC = 100
binN_MC = int((maxM_MC-minM_MC)/2.5)

binN_MC_Tot     = int((maxM_MC-minM_MC)/binWidthDraw)
binN_MC_Tot_Fit = int((maxM_MC-minM_MC)/binWidth)

binning_MC = RooFit.Binning(binN_MC_Tot, minM_MC, maxM_MC)
binning_MC_jpsi = RooFit.Binning(binN_MC, minM_MC, maxM_MC)

range_MC_tot = RooFit.Range(minM_MC,maxM_MC)
range_MC_jpsi = RooFit.Range(minM_MC,maxM_MC)

var_list = [
            'Jpsi_M','Jpsi_MM',#'Jpsi_m_scaled',
            #'Jpsi_ENDVERTEX_CHI2',
            'Jpsi_ENDVERTEX_Z','Jpsi_OWNPV_Z',
            #'Jpsi_PT',
            #'Jpsi_Y',
            ##'Jpsi_FDCHI2_OWNPV',
            'Jpsi_PZ','Jpsi_PT',#'Jpsi_PE','Jpsi_P',
            #'ProtonP_ProbNNp','ProtonM_ProbNNp',
            #'ProtonP_ProbNNpi','ProtonM_ProbNNpi',
            #'ProtonP_ProbNNk','ProtonM_ProbNNk',
            ##'ProtonP_P','ProtonM_P',
            ##'ProtonP_PT','ProtonM_PT',
            #'ProtonP_TRACK_CHI2NDOF','ProtonM_TRACK_CHI2NDOF',
            ##'ProtonP_PIDK', 'ProtonP_PIDp', 'ProtonM_PIDK', 'ProtonM_PIDp',
            'ProtonP_IPCHI2_OWNPV', 'ProtonM_IPCHI2_OWNPV',
            ##'ProtonP_TRACK_GhostProb', 'ProtonM_TRACK_GhostProb', 'ProtonP_TRACK_CloneDist', 'ProtonM_TRACK_CloneDist',
            ##'Jpsi_M01_Subst0_p2pi','Jpsi_M01_Subst1_p2pi',
            ##'Jpsi_M01_Subst0_p2K','Jpsi_M01_Subst1_p2K',
            ##'Jpsi_M01_Subst01_pp~2KK','Jpsi_M01_Subst01_pp~2pipi',
            ##'Jpsi_M01_Subst01_pp~2Kpi','Jpsi_M01_Subst01_pp~2piK',

            ]

def getHists(w):

    nameWksp = dataDir + "/wksps/wksp_data_{}_base.root".format(kSource)
    file_w = TFile(nameWksp)
    w_in = file_w.Get("w")

    lowData    = w_in.data("dh_Low")
    #highData   = w_in.data("dh_High")
    Jpsi_M     = w_in.var("Jpsi_M")
    Jpsi_M.setRange(minM_MC, maxM_MC)

    histLow = lowData.createHistogram("histLow",Jpsi_M)
    nBins = histLow.GetNbinsX()
    minX =  histLow.GetXaxis().GetBinLowEdge(1)
    maxX =  histLow.GetXaxis().GetBinUpEdge(nBins)
    binWidth = histLow.GetBinWidth(1)
    firstBin = int((minM_Low - minX)/binWidth) + 1
    lastBin  = int((maxM_Low - minX)/binWidth)

    #histHigh = highData.createHistogram("histHigh",Jpsi_M)
    #nBins = histHigh.GetNbinsX()
    #minX =  histHigh.GetXaxis().GetBinLowEdge(1)
    #maxX =  histHigh.GetXaxis().GetBinUpEdge(nBins)
    #binWidth = histHigh.GetBinWidth(1)
    #firstBin = int((minM_High - minX)/binWidth) + 1
    #lastBin  = int((maxM_High - minX)/binWidth)


    #Jpsi_M_Low  = RooRealVar("Jpsi_M_Low", "Jpsi_M_Low",  minM_Low, maxM_Low)
    #Jpsi_M_High = RooRealVar("Jpsi_M_High","Jpsi_M_High", minM_High, maxM_High)

    dh_Low  = RooDataHist("dh","dh",  RooArgList(Jpsi_M), histLow)
    #dh_High = RooDataHist("dh_High","dh_High", RooArgList(Jpsi_M), histHigh)
    getattr(w,"import")(Jpsi_M)
    #getattr(w,"import")(Jpsi_M_High)
    getattr(w,"import")(dh_Low)
    #getattr(w,"import")(dh_High)

    print("DATAHISTS READ SUCCESSFULLY")


def getData_d(w, kSource):

    if kSource=="all": kSource="*"

    dirName  = dataDir +"Data_Low_NoPID/"

    nt = TChain("DecayTree")
    nt.Add(dirName+"/Etac2sDiProton_Low_2018_2*.root")
    #nt.Add(dirName+"/Etac2sDiProton_Low_2018_1.root")
    #nt.Add(dirName+"/Etac2sDiProton_Low_2018_2.root")
    #nt.Add(dirName+"/Etac2sDiProton_Low_2018_3.root")
    #nt.Add(dirName+"/Etac2sDiProton_Low_2018_4.root")
    #nt.Add(dirName+"/Etac2sDiProton_Low_2018_5.root")
    #nt.Add(dirName+"/Etac2sDiProton_Low_2018_6.root")
    #nt.Add(dirName+"/Etac2sDiProton_Low_2018_7.root")
    #nt.Add(dirName+"/Etac2sDiProton_Low_2018_8.root")
    #nt.Add(dirName+"/Etac2sDiProton_Low_2018_9.root")
    #nt.Add(dirName+"/Etac2sDiProton_Low_2018_10.root")

    nt.SetBranchStatus("*",0)
    for var in var_list:
        nt.SetBranchStatus("{}".format(var),1);

    cut_FD = ""
    if kSource=="prompt":
        cut_FD = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08"# & Jpsi_FDCHI2_OWNPV > 49"
    else:
        cut_FD = "(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && (ProtonP_IPCHI2_OWNPV > 16 && ProtonM_IPCHI2_OWNPV > 16)"

    #nt.Add(dataDir+"MC/Trigger/etacDiProton_{}_Mag*_AddBr.root".format(kSource))

    tree = TTree()
    tree  = nt.CopyTree(cut_FD)
    Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M", minM_MC, maxM_MC)
    Jpsi_PT = RooRealVar("Jpsi_PT","Jpsi_PT", 0, 50000.)
    ds = RooDataSet("ds","ds",tree,RooArgSet(Jpsi_M,Jpsi_PT))
    getattr(w,"import")(ds)

    print(tree.GetEntries())
    print("DATA IS STORED")

def define_model(w):


    Jpsi_M = w.var('Jpsi_M')
    Jpsi_M.setBins(2000,'cache')

    ratioNtoW = 0.2438
    ratioEtaToJpsi = 0.94
    ratioArea = 0.9356
    width_etac = 31.8
    #width_etac = 29.7


    f = TFile("{}//MC/mass_fit/MC_M_res_all_jpsi_wksp_sim.root".format(homeDir))
    w_MC = f.Get("w")
    rEtaToJpsi = w_MC.var("r_ref_etac")
    rEtaToJpsi.setConstant(True)
    f.Close()

    #rEtaToJpsi = RooRealVar('rEtaToJpsi','rEtaToJpsi', ratioEtaToJpsi, 0.01, 5.0)
    rNarToW = RooRealVar('rNarToW','rNarToW',ratioNtoW, 0.01, 1.0)
    rG1toG2 = RooRealVar('rG1toG2','rG1toG2',ratioArea, 0.01, 1.0)
    #rEtaToJpsi = RooRealVar('rEtaToJpsi','rEtaToJpsi', ratioEtaToJpsi, 0.01, 5.0)
    #rNarToW = RooRealVar('rNarToW','rNarToW', 1.0)
    #rG1toG2 = RooRealVar('rG1toG2','rG1toG2', 1.0)
    rNarToW.setConstant(True)
    rG1toG2.setConstant(True)

    n_etac = RooRealVar('n_etac','num of etac prompt', 1e4, 0., 5.e6)
    n_jpsi = RooRealVar('n_jpsi','num of J/Psi prompt', 2e4, 0., 5.e6)
    n_etacRel = RooRealVar('n_etacRel','num of etac prompt', 0.0, 3.0)


    n_etac_1 = RooFormulaVar('n_etac_1','num of etac','@0*@1',RooArgList(n_etac,rG1toG2))
    n_etac_2 = RooFormulaVar('n_etac_2','num of etac','@0-@1',RooArgList(n_etac,n_etac_1))
    n_jpsi_1 = RooFormulaVar('n_jpsi_1','num of J/Psi','@0*@1',RooArgList(n_jpsi,rG1toG2))
    n_jpsi_2 = RooFormulaVar('n_jpsi_2','num of J/Psi','@0-@1',RooArgList(n_jpsi,n_jpsi_1))


    mass_jpsi = RooRealVar("mass_jpsi","mean of gaussian",3096.9, 3030, 3150)
    mass_etac = RooRealVar('mass_etac','mean of gaussian',2983.9, 2920, 3040)
    gamma_etac = RooRealVar('gamma_etac','width of Br-W', width_etac, 10., 50. )
    spin_etac = RooRealVar('spin_etac','spin_etac', 0. )
    radius_etac = RooRealVar('radius_etac','radius', 1.)
    proton_m = RooRealVar('proton_m','proton mass', 938.3 )

    mass_jpsi.setConstant(True)
    mass_etac.setConstant(True)
    gamma_etac.setConstant(True)


    #sigma_etac_1 = RooRealVar('sigma_etac_1','width of gaussian', 9., 0.1, 50.)
    #sigma_jpsi_1 = RooFormulaVar('sigma_jpsi_1','width of gaussian','@0/@1',RooArgList(sigma_etac_1,rEtaToJpsi))

    sigma_jpsi_1 = RooRealVar('sigma_jpsi_1','width of gaussian', 9., 0.1, 50.)
    sigma_jpsi_2 = RooFormulaVar('sigma_jpsi_2','width of gaussian','@0/@1',RooArgList(sigma_jpsi_1,rNarToW))

    sigma_etac_1 = RooFormulaVar('sigma_etac_1','width of gaussian','@0*@1',RooArgList(sigma_jpsi_1,rEtaToJpsi))
    sigma_etac_2 = RooFormulaVar('sigma_etac_2','width of gaussian','@0/@1',RooArgList(sigma_etac_1,rNarToW))


 #prompt
    #Fit eta
    br_wigner = RooRelBreitWigner('br_wigner', 'br_wigner',Jpsi_M, mass_etac, gamma_etac, spin_etac,radius_etac,proton_m,proton_m)

    gauss_etac_1 = RooGaussian('gauss_etac_1','gauss_etac_1 PDF', Jpsi_M, RooFit.RooConst(0),  sigma_etac_1) #mass_etac -> mass_jpsi
    gauss_etac_2 = RooGaussian('gauss_etac_2','gauss_etac_2 PDF', Jpsi_M, RooFit.RooConst(0),  sigma_etac_2) #mass_etac -> mass_jpsi

    bwxg_1 = RooFFTConvPdf('bwxg_1','breit-wigner (X) gauss', Jpsi_M, br_wigner, gauss_etac_1)
    bwxg_2 = RooFFTConvPdf('bwxg_2','breit-wigner (X) gauss', Jpsi_M, br_wigner, gauss_etac_2)

    #Fit J/psi
    gauss_1 = RooGaussian('gauss_1','gaussian PDF',Jpsi_M, mass_jpsi, sigma_jpsi_1)
    gauss_2 = RooGaussian('gauss_2','gaussian PDF',Jpsi_M, mass_jpsi, sigma_jpsi_2)


    model_etac = RooAddPdf('model_etac','etac signal', RooArgList(bwxg_1, bwxg_2), RooArgList(n_etac_1, n_etac_2))
    model_jpsi = RooAddPdf('model_jpsi','jpsi signal', RooArgList(gauss_1, gauss_2), RooArgList(n_jpsi_1, n_jpsi_2))

    a0 = RooRealVar("a0","a0",0.4,-2.,2.)
    a1 = RooRealVar("a1","a1",0.05,-2.,2.)
    a2 = RooRealVar("a2","a2",-0.005,-2.,2.)
    bkg = RooChebychev ("bkg","Background",Jpsi_M,RooArgList(a0,a1,a2))

    n_bkg = RooRealVar("n_bkg","num of etac", 2e5, 5e2, 1.e9)

    model = RooAddPdf('model','etac signal', RooArgList(bwxg_1, bwxg_2, gauss_1, gauss_2, bkg), RooArgList(n_etac_1, n_etac_2, n_jpsi_1, n_jpsi_2, n_bkg))
    #   getattr(w,'import')(model,RecycleConflictNodes())
    getattr(w,'import')(model)



def set_frame(w):

    Jpsi_M = w.var("Jpsi_M")
    data = w.data("ds")
    model = w.pdf("model")

    title = ""
    binning = binning_MC_jpsi
    range_draw = range_MC_jpsi

    frame = Jpsi_M.frame(RooFit.Title("c #bar{c} to p #bar{p}"))
    data.plotOn(frame, binning, mrkSize, lineWidth1, name("data")) #binning_MC_jpsi

    model.paramOn(frame,RooFit.Layout(0.68,0.99,0.99))
    frame.getAttText().SetTextSize(0.027)
    model.plotOn(frame, range_draw, lineWidth1)
    chi2 = frame.chiSquare()
    print("chi2", chi2)

    return frame, chi2

def draw(w):

    frame, chi2 = set_frame(w)

    texMC = TLatex();
    texMC.SetNDC();

    c = TCanvas("Masses_Fit","Masses Fit",600,800)
    c.Divide(1,2)
    c.cd(1).SetPad(.005, .505, .995, .995)
    gPad.SetLeftMargin(0.15),
    frame.Draw()
    #frame.SetMaximum(1.e3)
    #frame.SetMinimum(0.1)
    frame.GetXaxis().SetTitle("M_{p#bar{p}} / [MeV/c^2]"),
    frame.GetXaxis().SetTitleSize(0.14)
    frame.GetYaxis().SetTitleSize(0.14)
    #gPad.SetLogy()
    texMC.DrawLatex(0.7, 0.5, "#chi^{2} = %4.2f"%chi2)

    return c

def make_splot(w,kSource):

    #w_new = RooWorkspace("w",True)

    Jpsi_M = w.var("Jpsi_M")

    model = w.pdf("model")
    data = w.data("ds")
    n_etac = w.var("n_etac")
    n_jpsi = w.var("n_jpsi")
    n_bkg  = w.var("n_bkg")

    splot = RooStats.SPlot('sData', 'sData',
                  data, model,
                  RooArgList(n_etac, n_jpsi, n_bkg))
    weighted_sig_etac =  RooDataSet("{}_etac".format(data.GetName()),
                               data.GetTitle(),
                               data,
                               data.get(),
                               "1", "n_etac_sw")

    weighted_sig_jpsi =  RooDataSet("{}_jpsi".format(data.GetName()),
                               data.GetTitle(),
                               data,
                               data.get(),
                               "1", "n_jpsi_sw")

    weighted_bkg =  RooDataSet("{}_bkg".format(data.GetName()),
                               data.GetTitle(),
                               data,
                               data.get(),
                               "1", "n_bkg_sw")

    getattr(w,'import')(weighted_sig_etac)
    getattr(w,'import')(weighted_sig_jpsi)
    getattr(w,'import')(weighted_bkg)
    nameTree1 = "{}/splot/splot_{}_tree_etac.root".format(dataDir,kSource)
    f1 = TFile(nameTree1,"RECREATE")
    tree_sig_etac = TTree()
    tree_sig_etac.SetMaxTreeSize(500000000)
    tree_sig_etac = weighted_sig_etac.tree()
    tree_sig_etac.Write()
    f1.Close()
    #tree_sig_etac.GetCurrentFile().Close()

    nameTree2 = "{}/splot/splot_{}_tree_jpsi.root".format(dataDir,kSource)
    f2 = TFile(nameTree2,"RECREATE")
    tree_sig_jpsi = TTree()
    tree_sig_jpsi.SetMaxTreeSize(500000000)
    tree_sig_jpsi = weighted_sig_jpsi.tree()
    tree_sig_jpsi.Write()
    #tree_sig_jpsi.GetCurrentFile().Close()
    f2.Close()

    nameTree3 = "{}/splot/splot_{}_tree_bkg.root".format(dataDir,kSource)
    f3 = TFile(nameTree3,"RECREATE")
    tree_bkg = TTree()
    tree_bkg.SetMaxTreeSize(500000000)
    tree_bkg = weighted_bkg.tree()
    tree_bkg.Write()
    #tree_bkg.GetCurrentFile().Close()
    f3.Close()
    #return tree_sig, tree_bkg

def fitData(kSource, kCB=False):

    w = RooWorkspace("w",True)

    #getHists(w)
    getData_d(w, kSource)
    define_model(w)

    Jpsi_M = w.var("Jpsi_M")

    model = w.pdf("model")
    data = w.data("ds")

    f = TFile(homeDir+"/mass_fit/split_fit/Wksp_jpsi_fromB_Base_base_MC.root","READ")
    w_fromB = f.Get("w")
    f.Close()
    sigma = w_fromB.var("sigma_jpsi_1").getValV()
    w.var("sigma_jpsi_1").setVal(sigma)
    w.var("sigma_jpsi_1").setConstant(True)

    #w.var("gamma_etac").setConstant()
    #w.var("gamma_etac2S").setConstant()
    #sigma = w.var("sigma_etac_1")
    #sigma_etac2s = w.var("sigma_etac2s_1")
    #mean_etac2s = w.var("mean_etac2s")
    #mean_etac = w.var("mean_etac")
    #gamma_etac = w.var("gamma_etac")

    r = model.fitTo(data, RooFit.Save(True), RooFit.NumCPU(16))
    r = model.fitTo(data, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.NumCPU(16))
    r = model.fitTo(data, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.NumCPU(16))
    #r = model.fitTo(data, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.NumCPU(10))


    gROOT.ProcessLine("gStyle->SetOptStat(000000000)")
    gROOT.ProcessLine("gStyle->SetOptTitle(0)")

    print(data.numEntries())

    if kCB: modName="_CB"
    else:   modName=""

    make_splot(w,kSource)

    #nameTxt  = homeDir+"/MC/mass_fit/fit_M_res_{}{}.txt".format(kSource,modName)
    #nameRoot = homeDir+"/MC/mass_fit/MC_M_res_{}{}.root".format(kSource,modName)
    namePic  = "{}/splot_{}.pdf".format(homeDir,kSource)


    #fFit = TFile (nameRoot,"RECREATE")

    c = draw(w)
    c.SaveAs(namePic)

    #c.Write("")
    #fFit.Write()
    #fFit.Close()

    #r.correlationMatrix().Print("v")
    #r.globalCorr().Print("v")


    #import os, sys
    #save = os.dup( sys.stdout.fileno() )
    #newout = file(nameTxt, "w" )
    #os.dup2( newout.fileno(), sys.stdout.fileno() )
    #r.Print("v")
    #r.correlationMatrix().Print()
    #os.dup2( save, sys.stdout.fileno() )
    #newout.close()



kSource = "fromB"
# kSource = "prompt"
fitData(kSource, False)
kSource = "prompt"
fitData(kSource, False)

