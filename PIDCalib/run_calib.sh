#!/bin/bash

# python PIDCalib/PIDPerfScripts/scripts/python/MultiTrack/MakePerfHistsRunRange.py --binSchemeFile="PIDCalib/PIDPerfScripts/python/PIDPerfScripts/Binning.py" --schemeName="mybinning"  "Turbo18" "MagDown" "P" "[DLLp>5.0 && DLLp-DLLK>0.0, MC15TuneV1_ProbNNK<0.6 && MC15TuneV1_ProbNNpi<0.6 && MC15TuneV1_ProbNNp>0.6, DLLp>5.0 && DLLp-DLLK>0.0 && MC15TuneV1_ProbNNK<0.6 && MC15TuneV1_ProbNNpi<0.6 && MC15TuneV1_ProbNNp>0.6]" "P" "ETA" "nSPDHits" | tee log_PIDCalib_down.txt
mc_path="/eos/user/v/vazhovko/etac2s_ppbar/MC/Trigger/"
# mc_path="/eos/user/v/vazhovko/etac2s_ppbar/MC/Reco/"
ccbar_list=("etac")
 # "etac2S" "chic0" "chic1" "chic2")
# "jpsi" "psi2S")
pol_list=("Up" "Down")

src_list=("prompt")
 # "fromB")

pid_cut="DLLp>5.0 && DLLp-DLLK>0.0"
 # && MC15TuneV1_ProbNNK<0.6 && MC15TuneV1_ProbNNpi<0.6 && MC15TuneV1_ProbNNp>0.6"

for ccbar in ${ccbar_list[@]}
do
  for pol in ${pol_list[@]}
  do
    for src in ${src_list[@]}
    do
        python $PIDPERFSCRIPTSROOT/scripts/python/MultiTrack/PerformMultiTrackCalib.py -s "P" "mybinning" -X "P" -Y "ETA" -Z "nSPDHits" -y "ETA" -z "nSPDHits" \
            "Turbo18" "Mag"$pol $mc_path"/"$ccbar"DiProton_"$src"_Mag"$pol"_2018_AddBr.root" "DecayTree" $mc_path"/PIDCalib/"$ccbar"_"$src"_Mag"$pol"_2018_DLL.root" \
            "[ProtonM, P, $pid_cut]" "[ProtonP, P, $pid_cut]"
    done;
  done;
done;

# for i in *.root; do     NEWNAME="${i/_PIDCalib/}"; mv -- "$i" "$NEWNAME"; done