#/bin/bash

TOTALCUT=""
cuts_u=("0.1" "0.2" "0.3" "0.4" "0.5" "0.6" "0.7" "0.8" "0.9" )
cuts_l=("0.2" "0.4" "0.6" "0.8" "1.0" )
for cut_1 in ${cuts_u[@]}
do
    for cut_2 in ${cuts_l[@]}
    do
        for cut_3 in ${cuts_l[@]}
        do
            TOTALCUT=${TOTALCUT}' --pid-cut "MC15TuneV1_ProbNNp>'${cut_1}' & MC15TuneV1_ProbNNk<'${cut_2}' & MC15TuneV1_ProbNNpi<'${cut_3}'" \ \n'
        done
    done
done
echo $TOTALCUT
