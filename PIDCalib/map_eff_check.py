from ROOT import TChain, TFile, TH2D

import numpy as np
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

import json

def plot_map(ccbar, src, mag, postfix):
    ''' 
        creates 2D map of efficiency: P vs ETA
        checks bins with non-physical efficiencies
        for each of this bins checks how many MC events we have
    '''

    # Opening JSON file
    f = open('binning_ppbar.json',)
      
    # returns JSON object as 
    # a dictionary
    data = json.load(f)

    # eta_bins = array("f",[1.5,  2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0])
    eta_bins = np.array(data["P"]["ETA"])
    p_bins = np.array(data["P"]["P"])
    SPD_bins = np.array(data["P"]["nSPDhits"])

    n_eta_bins = len(eta_bins)-1
    n_p_bins   = len(p_bins)-1
    n_SPD_bins = len(SPD_bins)-2

    f_eff = TFile("effhists-Turbo18-up-P-DLLp>5.0&DLLp-DLLK>0.0&MC15TuneV1_ProbNNp>0.8-P.ETA.nSPDhits.root","read")
    # f_eff = TFile("effhists-Turbo18-up-P-DLLp>5.0&DLLp-DLLK>0.0-P.ETA.nSPDhits.root","read")
    # f_eff = TFile("effhists-Turbo18-up-P-MC15TuneV1_ProbNNp>0.8-P.ETA.nSPDhits.root","read")
    hh_eff_3D = f_eff.Get("eff")
    hh_tot_3D = f_eff.Get("total")
    hh_pass_3D = f_eff.Get("passing")

    nt = TChain("DecayTree")
    nt.Add("{}/MC/Trigger/{}DiProton_{}_Mag{}_2018_AddBr_{}.root".format(dataDir, ccbar, src, mag, postfix))

    hh_eff_2D = TH2D("hh_eff_2D","hh ETA vs P",n_p_bins, p_bins, n_eta_bins, eta_bins)
    
    cut_accept  = "ProtonP_TRUETHETA>0.01 && ProtonP_TRUETHETA<0.4 && ProtonM_TRUETHETA>0.01 && ProtonM_TRUETHETA<0.4"#" && 0.5*TMath::Log((Jpsi_TRUEP_E + Jpsi_TRUEP_Z)/(Jpsi_TRUEP_E-Jpsi_TRUEP_Z))>2. && 0.5*TMath::Log((Jpsi_TRUEP_E + Jpsi_TRUEP_Z)/(Jpsi_TRUEP_E-Jpsi_TRUEP_Z))<4.5 "#" && nSPDHits < 300" #

    for i_P in range(1, n_p_bins+1):
        for i_ETA in range(1, n_eta_bins+1):
            eff_val = 0.
            eff_err = 0.
            for i_SPD in range(1, n_SPD_bins+1):
                eff_val_bin = hh_eff_3D.GetBinContent(i_P, i_ETA, i_SPD)
                # eff_val += hh_eff_3D.GetBinContent(i_P, i_ETA, i_SPD)
                eff_val += eff_val_bin
                eff_err += (hh_eff_3D.GetBinError(i_P, i_ETA, i_SPD))**2
                if eff_val_bin<0. or eff_val_bin>1.:
                    print("{}:{}:{}  {}".format(i_P, i_ETA, i_SPD, eff_val_bin))
                    # print("{}:{}".format(hh_tot_3D.GetBinContent(i_P, i_ETA, i_SPD), hh_pass_3D.GetBinContent(i_P, i_ETA, i_SPD)))
                    
                    cut_string = "ProtonP_P > {0} && ProtonP_P < {1} && ProtonP_ETA > {2} && ProtonP_ETA < {3} && nSPDHits > {4} && nSPDHits < {5}".format(p_bins[i_P-1],p_bins[i_P],eta_bins[i_ETA-1],eta_bins[i_ETA],SPD_bins[i_SPD-1],SPD_bins[i_SPD])
                    nn = nt.GetEntries(cut_string + " && " + cut_accept)
                    
                    if nn>0:
                        print("{} events in MC".format(nn))
                        nt.Draw("PIDCalibEff",cut_string + " && " + cut_accept)
                        input()


            eff_err = eff_err**0.5/n_eta_bins
            eff_val/=n_eta_bins
            hh_eff_2D.SetBinContent(i_P, i_ETA, eff_val)
            hh_eff_2D.SetBinError(i_P, i_ETA, eff_err)

    hh_eff_2D.Draw("colz")
    input()
    hh_eff_2D.SaveAs("eff_eta_vs_p.pdf")

if __name__ == "__main__":

    ccbar_list = ['jpsi','etac','etac2S','chic0','chic1','chic2','psi2S']
    srcs = ["prompt", "fromB"]
    mags = ["Up","Down"] 
    postfix = "all"

    for ccbar in ccbar_list:
        for src in srcs:
            for mag in mags:
                plot_map(ccbar, src, mag, postfix)