#/bin/bash
# lb-conda pidcalib bash 
magPol=("up" "down")
ccbar_list=("etac" "jpsi" "chic0" "chic1" "chic2" "etac2S" "psi2S")
# ccbar_list=("jpsi")
PATHTOMC=/sps/lhcb/zhovkovska/etac2s_ppbar/MC/NoTurbo/
# PATHTOMC=/sps/lhcb/zhovkovska/etac2s_ppbar/MC/Trigger/
src_list=("prompt" "fromB")
# src_list=("fromB")

# postfix="all"
# postfix="DLL"
postfix="ProbNN"
postfix="ProbNNTight"

# declare -A pid_cuts=( ["DLL"]="DLLp>5.0&DLLp-DLLK>0.0" \
#                       ["DLLTight"]="DLLp>20.0&DLLp-DLLK>15.0" \
#                       ["ProbNN"]="MC15TuneV1_ProbNNp>0.6" \
#                       ["all"]="DLLp>5.0&DLLp-DLLK>0.0&MC15TuneV1_ProbNNp>0.6" )
#                     #   ["ProbNNTight"]="MC15TuneV1_ProbNNp>0.8" \
#                     #   ["allTight"]="DLLp>20.0&DLLp-DLLK>15.0&MC15TuneV1_ProbNNp>0.8")
declare -A pid_cuts=(   ["ProbNNTight"]="MC15TuneV1_ProbNNp>0.8" \
                        ["allTight"]="DLLp>20.0&DLLp-DLLK>15.0&MC15TuneV1_ProbNNp>0.8")


cuts_u=("0.1" "0.2" "0.3" "0.4" "0.5" "0.6" "0.7" "0.8" "0.9" )
cuts_l=("0.2" "0.4" "0.6" "0.8" "1.0" )
# TOTALCUT=""
# for cut_1 in ${cuts_u[@]}
# do
#     for cut_2 in ${cuts_l[@]}
#     do
#         NEWCUT=' --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>'${cut_1}' & MC15TuneV1_ProbNNpi<'${cut_2}'"'
#         TOTALCUT="${TOTALCUT} ${NEWCUT}"
#         # for cut_3 in ${cuts_l[@]}
#         # do
#         #     NEWCUT=' --pid-cut "MC15TuneV1_ProbNNp>'${cut_1}' & MC15TuneV1_ProbNNk<'${cut_2}' & MC15TuneV1_ProbNNpi<'${cut_3}'"'
#         #     TOTALCUT="${TOTALCUT} ${NEWCUT}"
#         # done
#     done
# done
# echo $TOTALCUT

MAKETABLES="n"
COMPUTEEFF="n"
echo -n "Performing PID Calibration...."
read -e -p "Prepare PIDCalib tables? [y/n/q]:" PROCEED
PROCEED="${PROCEED:-${MAKETABLES}}"
if [ "${PROCEED}" == "q" ] ; then
    echo "Quitting"
    exit
# condition for non specific letter (ie anything other than q/y)
# if you want to have the active 'y' code in the last section
elif [ "${PROCEED}" != "y" ] ; then
    echo "Skip preparing tables"
else
    echo -n "Preparing tables...."
    for pol in ${magPol[@]}
    do
        pidcalib2.make_eff_hists --binning-file binning_ppbar.json --sample Turbo18 --magnet ${pol} --particle P \
            --pid-cut "DLLp>20.0 & DLLp-DLLK>15.0 & MC15TuneV1_ProbNNp>0.8" \
            --pid-cut "MC15TuneV1_ProbNNp>0.8" \
            --bin-var P --bin-var ETA --bin-var nSPDhits --output-dir  ~/etac2s_ppbar/PIDCalib/ProbNN \
            # --pid-cut "DLLp>20.0 & DLLp-DLLK>15.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0" \
            # --pid-cut "MC15TuneV1_ProbNNp>0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNpi<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNpi<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNpi<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNpi<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNpi<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNk<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNk<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNk<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNk<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNk<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.1 & MC15TuneV1_ProbNNk<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.1 & MC15TuneV1_ProbNNk<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.1 & MC15TuneV1_ProbNNk<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.1 & MC15TuneV1_ProbNNk<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.1 & MC15TuneV1_ProbNNk<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.2 & MC15TuneV1_ProbNNk<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.2 & MC15TuneV1_ProbNNk<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.2 & MC15TuneV1_ProbNNk<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.2 & MC15TuneV1_ProbNNk<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.2 & MC15TuneV1_ProbNNk<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.3 & MC15TuneV1_ProbNNk<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.3 & MC15TuneV1_ProbNNk<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.3 & MC15TuneV1_ProbNNk<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.3 & MC15TuneV1_ProbNNk<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.3 & MC15TuneV1_ProbNNk<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.4 & MC15TuneV1_ProbNNk<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.4 & MC15TuneV1_ProbNNk<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.4 & MC15TuneV1_ProbNNk<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.4 & MC15TuneV1_ProbNNk<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.4 & MC15TuneV1_ProbNNk<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.5 & MC15TuneV1_ProbNNk<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.5 & MC15TuneV1_ProbNNk<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.5 & MC15TuneV1_ProbNNk<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.5 & MC15TuneV1_ProbNNk<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.5 & MC15TuneV1_ProbNNk<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.6 & MC15TuneV1_ProbNNk<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.6 & MC15TuneV1_ProbNNk<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.6 & MC15TuneV1_ProbNNk<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.6 & MC15TuneV1_ProbNNk<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.6 & MC15TuneV1_ProbNNk<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.7 & MC15TuneV1_ProbNNk<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.7 & MC15TuneV1_ProbNNk<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.7 & MC15TuneV1_ProbNNk<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.7 & MC15TuneV1_ProbNNk<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.7 & MC15TuneV1_ProbNNk<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.8 & MC15TuneV1_ProbNNk<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.8 & MC15TuneV1_ProbNNk<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.8 & MC15TuneV1_ProbNNk<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.8 & MC15TuneV1_ProbNNk<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.8 & MC15TuneV1_ProbNNk<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.9 & MC15TuneV1_ProbNNk<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.9 & MC15TuneV1_ProbNNk<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.9 & MC15TuneV1_ProbNNk<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.9 & MC15TuneV1_ProbNNk<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.9 & MC15TuneV1_ProbNNk<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.1 & MC15TuneV1_ProbNNpi<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.1 & MC15TuneV1_ProbNNpi<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.1 & MC15TuneV1_ProbNNpi<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.1 & MC15TuneV1_ProbNNpi<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.1 & MC15TuneV1_ProbNNpi<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.2 & MC15TuneV1_ProbNNpi<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.2 & MC15TuneV1_ProbNNpi<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.2 & MC15TuneV1_ProbNNpi<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.2 & MC15TuneV1_ProbNNpi<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.2 & MC15TuneV1_ProbNNpi<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.3 & MC15TuneV1_ProbNNpi<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.3 & MC15TuneV1_ProbNNpi<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.3 & MC15TuneV1_ProbNNpi<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.3 & MC15TuneV1_ProbNNpi<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.3 & MC15TuneV1_ProbNNpi<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.4 & MC15TuneV1_ProbNNpi<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.4 & MC15TuneV1_ProbNNpi<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.4 & MC15TuneV1_ProbNNpi<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.4 & MC15TuneV1_ProbNNpi<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.4 & MC15TuneV1_ProbNNpi<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.5 & MC15TuneV1_ProbNNpi<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.5 & MC15TuneV1_ProbNNpi<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.5 & MC15TuneV1_ProbNNpi<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.5 & MC15TuneV1_ProbNNpi<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.5 & MC15TuneV1_ProbNNpi<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.6 & MC15TuneV1_ProbNNpi<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.6 & MC15TuneV1_ProbNNpi<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.6 & MC15TuneV1_ProbNNpi<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.6 & MC15TuneV1_ProbNNpi<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.6 & MC15TuneV1_ProbNNpi<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.7 & MC15TuneV1_ProbNNpi<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.7 & MC15TuneV1_ProbNNpi<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.7 & MC15TuneV1_ProbNNpi<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.7 & MC15TuneV1_ProbNNpi<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.7 & MC15TuneV1_ProbNNpi<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.8 & MC15TuneV1_ProbNNpi<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.8 & MC15TuneV1_ProbNNpi<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.8 & MC15TuneV1_ProbNNpi<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.8 & MC15TuneV1_ProbNNpi<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.8 & MC15TuneV1_ProbNNpi<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.9 & MC15TuneV1_ProbNNpi<0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.9 & MC15TuneV1_ProbNNpi<0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.9 & MC15TuneV1_ProbNNpi<0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.9 & MC15TuneV1_ProbNNpi<0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.9 & MC15TuneV1_ProbNNpi<1.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.1" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.2" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.3" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.4" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.5" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.7" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.8" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.9" \
            # --max-files 30 
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNk<0.6 & MC15TuneV1_ProbNNpi<0.6 & MC15TuneV1_ProbNNp>0.6" \
            # --pid-cut "MC15TuneV1_ProbNNk<0.6 & MC15TuneV1_ProbNNpi<0.6 & MC15TuneV1_ProbNNp>0.6" \
            # --pid-cut "DLLp>5.0 & DLLp-DLLK>0.0 & MC15TuneV1_ProbNNp>0.7" \
            # --pid-cut "MC15TuneV1_ProbNNp>0.7" \
    done
fi

read -e -p "Compute efficiency? [y/n/q]:" PROCEED
PROCEED="${PROCEED:-${COMPUTEEFF}}"
if [ "${PROCEED}" == "q" ] ; then
    echo "Quitting"
    exit
# condition for non specific letter (ie anything other than q/y)
# if you want to have the active 'y' code in the last section
elif [ "${PROCEED}" != "y" ] ; then
    echo "Skip computing efficiency"
else
    echo -n "Proceeding with MC...."
    for ccbar in ${ccbar_list[@]}
    do
        for src in ${src_list[@]}
        do
            for pol in ${magPol[@]}
            do
                for postfix in ${!pid_cuts[@]}
                do
                    cp $PATHTOMC/${ccbar}DiProton_${src}_Mag${pol^}_2018_sel.root $PATHTOMC/${ccbar}DiProton_${src}_Mag${pol^}_2018_sel_${postfix}.root
                    pidcalib2.ref_calib --sample Turbo18 --magnet ${pol} \
                        --ref-file $PATHTOMC/${ccbar}DiProton_${src}_Mag${pol^}_2018_sel_${postfix}.root \
                        --histo-dir ~/etac2s_ppbar/PIDCalib/ProbNN/ \
                        --bin-vars '{"P": "P", "ETA": "ETA", "nSPDhits": "nSPDHits"}' \
                        --ref-pars '{"ProtonP": ["P", "'${pid_cuts[$postfix]}'"], "ProtonM": ["P", "'${pid_cuts[$postfix]}'"]}' \
                        --output-file $PATHTOMC/${ccbar}DiProton_${src}_Mag${pol^}_2018_sel_PID_eff_${postfix}.root \
                        --merge
                done
            done
        done
    done
fi


# cut="0.0"
# echo ${cut}
# for ccbar in ${ccbar_list[@]}
# do
#     for src in ${src_list[@]}
#     do
#         for pol in ${magPol[@]}
#         do
#             for cut_2 in ${cuts_u[@]}
#             do
#                 # for cut_2 in ${cuts_l[@]}
#                 # do
#                     # cut="DLLp>5.0&DLLp-DLLK>0.0&MC15TuneV1_ProbNNk<${cut_2}"
#                     cut="DLLp>5.0&DLLp-DLLK>0.0&MC15TuneV1_ProbNNp>${cut_2}"
#                     # cut="DLLp>5.0&DLLp-DLLK>0.0&MC15TuneV1_ProbNNp>${cut_1}&MC15TuneV1_ProbNNpi<${cut_2}"
#                     cp $PATHTOMC/${ccbar}DiProton_${src}_Mag${pol^}_2018_sel.root $PATHTOMC/PIDOpt/Proton_ProbNNp/${ccbar}DiProton_${src}_Mag${pol^}_2018_sel_${cut_2}.root
#                     pidcalib2.ref_calib --sample Turbo18 --magnet ${pol} \
#                     --ref-file $PATHTOMC/PIDOpt/Proton_ProbNNp/${ccbar}DiProton_${src}_Mag${pol^}_2018_sel_${cut_2}.root \
#                     --histo-dir ~/etac2s_ppbar/PIDCalib/ProbNN/ \
#                     --bin-vars '{"P": "P", "ETA": "ETA", "nSPDhits": "nSPDHits"}' \
#                     --ref-pars '{"ProtonP": ["P", "'${cut}'"], "ProtonM": ["P", "'${cut}'"]}' \
#                     --output-file $PATHTOMC/PIDOpt/Proton_ProbNNp/${ccbar}DiProton_${src}_Mag${pol^}_2018_sel_PID_eff_${cut_2}.root \
#                     --merge
#                     # --output-file $PATHTOMC/PIDOpt/ProbNNp_ProbNNpi/${ccbar}DiProton_${src}_Mag${pol^}_2018_sel_PID_eff_${cut_1}_${cut_2}.root 
#                     # --ref-pars '{"ProtonP": ["P", "DLLp>5.0&DLLp-DLLK>0.0&MC15TuneV1_ProbNNp>'${cut}'"], "ProtonM": ["P", "DLLp>5.0&DLLp-DLLK>0.0&MC15TuneV1_ProbNNp>'${cut}'"]}' \
#                     # --ref-pars '{"ProtonP": ["P", "DLLp>5.0&DLLp-DLLK>0.0"], "ProtonM": ["P", "DLLp>5.0&DLLp-DLLK>0.0"]}' \
#                 done
#             # done
#         done
#     done
# done