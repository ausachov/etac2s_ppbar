from ROOT import gROOT, gStyle, TFile, TH1, TCanvas, TH2D
# from ROOT import kIsAverage
from array import array

import sys
sys.path.insert(1,"../fit/libs/")
gROOT.LoadMacro("../fit/libs/lhcbStyle.C")

import json
from pid_eff import binNameDict

homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/"

def plot_map(x="P",y="ETA",cut="DLLp>5.0&DLLp-DLLK>0.0&MC15TuneV1_ProbNNp>0.6", binSlice=0):
    ''' 
        plots 2D map of efficiency: x vs y (P vs ETA by default)
        in a bin = binSlice of variable z 
    '''

    gStyle.SetPalette(57)

    axisDict = {
        "P":    "x",
        "ETA":  "y",
        "nSPDHits": "z"
    }

    proj = axisDict[y]+axisDict[x]

    f_u = TFile("ProbNN/effhists-Turbo18-up-P-{}-P.ETA.nSPDhits.root".format(cut),"READ")
    f_d = TFile("ProbNN/effhists-Turbo18-down-P-{}-P.ETA.nSPDhits.root".format(cut),"READ")
    eff = f_u.Get("eff")
    eff.SetBit(TH1.kIsAverage)            #to average histo instead of adding
    eff_temp = f_d.Get("eff")
    eff_temp.SetBit(TH1.kIsAverage)            #to average histo instead of adding
    eff.Add(eff_temp)
    # eff.SetDirectory(0)
    # xBins = binDict[x]
    # yBins = binDict[y]

    # h_map = TH2D("h_map","h_map",len(xBins)-1,xBins, len(yBins)-1, yBins)
    # fb = open("binning_ppbar.json")
    # data = json.load(fb)
    # data = data["P"]

    h_map = TH2D()

    if binSlice==0:
        # h_map = eff_u.Project3D(proj)
        # h_map.SetBit(kIsAverage)            #to average histo instead of adding
        # h_map.Add(eff_d.Project3D(proj))
        h_map = eff.Project3D(proj)
        h_map.Scale(1/7.)
    else:
        if "z" not in proj:
            eff.GetZaxis().SetRange(binSlice,binSlice)
        elif "y" not in proj:
            eff.GetYaxis().SetRange(binSlice,binSlice)
        else:
            eff.GetXaxis().SetRange(binSlice,binSlice)

        # h_map = eff_u.Project3D(proj)
        # h_map.SetBit(kIsAverage)            #to average histo instead of adding
        # h_map.Add(eff_d.Project3D(proj))
        h_map = eff.Project3D(proj)

    # h_map.SetDirectory(0)

    c = TCanvas("c","c", 1000, 800)
    pad = c.cd()
    pad.SetRightMargin(0.15)
    pad.SetTopMargin(0.075)

    h_map.GetXaxis().SetTitle(binNameDict[x])
    h_map.GetXaxis().SetTitleOffset(0.9)
    h_map.GetYaxis().SetTitle(binNameDict[y])
    # pad.SetLogy()
    h_map.GetZaxis().SetRangeUser(0., 1.)
    h_map.DrawClone("colz")    
    # h_map.Draw("text same")    

    c.SaveAs("{}/eff/pid/PIDCalib_map_{}_{}_slice{}.pdf".format(homeDir,x, y, binSlice))
    f_u.Close()
    f_d.Close()
    del c

if __name__ == "__main__":

    for sl in range(0, 7):
        plot_map(y="P",x="ETA",cut="DLLp>5.0&DLLp-DLLK>0.0&MC15TuneV1_ProbNNp>0.6",binSlice=sl)
    # plot_map(y="P",x="ETA",cut="DLLp>5.0&DLLp-DLLK>0.0&MC15TuneV1_ProbNNp>0.6",binSlice=6)
    # plot_map(y="P",x="nSPDHits",cut="DLLp>5.0&DLLp-DLLK>0.0&MC15TuneV1_ProbNNp>0.8")