from ROOT import gROOT, gStyle, gPad, TChain, TFile, TH1D, THStack, TParameter, TCanvas, TLegend, kOcean
from array import array
import math
import sys
sys.path.insert(1,"../fit/efficiencies/")
sys.path.insert(1,"../fit/libs/")
# from effCalc import calc_eff, get_entries
from efficiency import pid_def_eff, write2file, plot_hist

# dataDir = "/eos/user/v/vazhovko/etac2s_ppbar/MC/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/MC/"
homeDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/results/"

cut_accept  = "ProtonP_TRUETHETA>0.01 && ProtonP_TRUETHETA<0.4 && ProtonM_TRUETHETA>0.01 && ProtonM_TRUETHETA<0.4"#" && 0.5*TMath::Log((Jpsi_TRUEP_E + Jpsi_TRUEP_Z)/(Jpsi_TRUEP_E-Jpsi_TRUEP_Z))>2. && 0.5*TMath::Log((Jpsi_TRUEP_E + Jpsi_TRUEP_Z)/(Jpsi_TRUEP_E-Jpsi_TRUEP_Z))<4.5 "#" && nSPDHits < 300" #
cut_true_v  = "Jpsi_TRUEPT > 5000 && Jpsi_TRUEPT < 20000 && Jpsi_TRUEY > 2.0 && Jpsi_TRUEY < 4.5"
cut_reco_v  = "Jpsi_PT > 5000 && Jpsi_PT < 20000 && Jpsi_Y > 2.0 && Jpsi_Y < 4.5"
# cut_ProbNN  = "ProtonP_ProbNNk < 0.6 && ProtonM_ProbNNk < 0.6 && ProtonP_ProbNNp > 0.6 && ProtonM_ProbNNp > 0.6 && ProtonP_ProbNNpi < 0.6 && ProtonM_ProbNNpi < 0.6 "
# cut_ProbNN  = "ProtonP_ProbNNp > 0.8 && ProtonM_ProbNNp > 0.8"
cut_ProbNN  = "ProtonP_ProbNNp > 0.6 && ProtonM_ProbNNp > 0.6"
cut_PID     = "(ProtonP_PIDp-ProtonP_PIDK)>0 && (ProtonM_PIDp-ProtonM_PIDK)>0 && ProtonP_PIDp>5 && ProtonM_PIDp>5"
cut_Trigger = "Jpsi_L0HadronDecision_TOS && (Jpsi_Hlt1DiProtonDecision_TOS || Jpsi_Hlt1DiProtonHighDecision_TOS)"


cut_select  = "     nSPDHits < 300 && \
                    ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
                    ProtonP_PT>2000 && ProtonM_PT>2000 && \
                    ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366"

cut_total = "{} && {} && {} && {}".format(cut_reco_v, cut_select, cut_Trigger, cut_ProbNN)

binDict = {
    "P"         : array("f",[ 12000.0 , 13950.0 , 14940.0 , 16410.0 , 18180.0 , 
                              19200.0 , 20160.0 , 21120.0 , 22950.0 , 23880.0 , 
                              24840.0 , 27750.0 , 36690.0 , 39300.0 , 42120.0 , 
                              43650.0 , 48750.0 , 50610.0 , 52560.0 , 53550.0 , 
                              55620.0 , 57750.0 , 60030.0 , 62400.0 , 64950.0 , 
                              67650.0 , 70590.0 , 73770.0 , 75450.0 , 79110.0 , 
                              81090.0 , 83220.0 , 85470.0 , 87870.0 , 90420.0 , 
                              93180.0 , 96180.0 , 99390.0 , 102930.0 , 106800.0 , 
                              111090.0 , 115980.0 , 121710.0 , 128760.0 , 138060.0 , 152490.0, 300000.]),
    "ETA"       : array("f",[ 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0]),
    "Y"         : array("f",[ 2.0, 2.5, 3.0, 3.5, 4.0]), # 4.5
    "nSPDHits"  : array("f",[ 0., 50., 100., 150., 200., 250., 300., 350.]),
    "eff"       : array("f",[ x*0.01 for x in range(101) ]),
    "PT"        : array("f",[5000., 6500., 8000., 10000., 12000., 14000., 20000.])
    # "PT"        : array("f",[5000., 6500., 8000., 10000., 12000., 14000.])

    # "eff"     : array("f",[ x for x in range(0, 300) ])
}

binNameDict = {
    "PT" : "p_{T} [MeV/c]",
    "P"  : "p [MeV/c]",
    "Y"  : "y",
    "ETA": "#eta",
    "nSPDHits": "nSPDHits"
}
# PT

titleDict = {
    # "all":      "PIDCalib: ProbNN + DLL",
    "5bin":     "L0 calibration tables", # 5 bins
    "def":      "MC Sample",
    "calib":    "L0 calibration tables",
    "all":      "PIDCalib tables",
    "DLL":      "PIDCalib: DLL",
    "ProbNN":   "PIDCalib: ProbNN",
    "none":     "MC Sample",
    "gen":      "PIDGen",
    "gen_Lb":   "PIDGen #Lambda_{b}",
    "gen_Lc":   "PIDGen #Lambda_{c}",
    "corr":     "PIDCorr"
}
def calc_PID(ccbar, source, key, binsName="PT", X="P", Y="ETA", Z="nSPDHits"):
    '''
    computes PID efficiency using MC files with PIDCalib efficiencies

    '''

    nt = TChain("DecayTree")
    # nt.Add(f"{dataDir}/Trigger/{ccbar}DiProton_{source}_Mag*_2018_sel_{key}.root")
    nt.Add(f"{dataDir}/NoTurbo/{ccbar}DiProton_{source}_Mag*_2018_sel_{key}.root")

    # nt_eff = TChain("CalibTool_PIDCalibTree")
    # nt_eff.Add("{}/Trigger/PIDCalib/{}_{}_Mag*_2018_{}.root".format(dataDir, ccbar, source, key))

    # nt.AddFriend(nt_eff)

    
    # nBinsX = len(binDict[X])-1 
    # nBinsY = len(binDict[Y])-1 
    # nBinsZ = len(binDict[Z])-1
    nBins = len(binDict[binsName])-1
    n_counts = array("i", (nBins+1)*[0])
    n_counts[0] = nt.GetEntries()

    hist_eff = TH1D("hist_eff","hist", nBins, binDict[binsName])
    hist_err = TH1D("hist_err","hist", nBins, binDict[binsName])

    eff = 0.
    err = 0.

    # hist_w = TH3D("hist_w","hist", nBinsX, binDict[X], nBinsY, binDict[Y], nBinsZ, binDict[Z])
    # hist_n = TH3D("hist_n","hist", nBinsX, binDict[X], nBinsY, binDict[Y], nBinsZ, binDict[Z])
    # nx = 47
    # ny = 9
    print(nt.GetEntries())
    for event in nt:

    #   # print(t_tot.nSPDHits, t_eff.PIDCalibEff)
        # if nt.Jpsi_prompt:
        # if nt.Jpsi_sec:
        if 0. < nt.ProtonP_PIDCalibEff < 1. and 0. < nt.ProtonM_PIDCalibEff < 1. :
            eff += nt.PIDCalibEff
            err += nt.PIDCalibErr**2
            nBin = 0.
            if binsName=="PT":
                nBin = hist_eff.FindBin(nt.Jpsi_PT)
            else:
                nBin = hist_eff.FindBin(nt.Jpsi_Y)
            if nBin <= nBins: 
                n_counts[nBin] += 1
            hist_eff.AddBinContent(nBin, nt.PIDCalibEff)
            hist_err.AddBinContent(nBin, nt.PIDCalibErr**2)
        # n = hist_w.FindBin(nt.ProtonP_P, nt.ProtonP_ETA, nt.nSPDHits)
    #   # print(hist_w.GetBinContent(n))
    #   hist_w.AddBinContent(t_tot.ProtonP_PIDCalibBinNumber, t_tot.PIDCalibEff)

        # print(n, nt.ProtonP_PIDCalibBinNumber)

    for iBin in range(1, nBins+1):

        eff_i = hist_eff.GetBinContent(iBin) / float(n_counts[iBin])
        err_i = (hist_err.GetBinContent(iBin)/float(n_counts[iBin]))**0.5 
        hist_eff.SetBinContent(iBin, eff_i)
        hist_eff.SetBinError(iBin, err_i)

    hist_eff.Draw("E1")
    eff = eff/n_counts[0]
    err = (err/n_counts[0])**0.5


    effDir = homeDir + "/eff/note_v2/pid"
    # effDir = f"{homeDir}/eff/note_v2/pid/no_turbo/"
    eff_val = TParameter(float)("eff_val", eff)
    eff_err = TParameter(float)("eff_err", err)
    file = TFile("{}/eff_{}_{}_{}_pid_{}.root".format(effDir,ccbar,source,binsName,key), "RECREATE")
    hist_eff.Write()
    file.WriteObject(eff_val, "eff_val")
    file.WriteObject(eff_err, "eff_err")
    file.Close()

    print(f"{ccbar} pid efficiency: {round(eff,3)} +/- {round(err, 3)}")

def compare(ccbar, source, key_list, binsName="PT", eff_type="pid"):
    '''
    Compare PID efficiencies obtained with differend methods
    '''
    gStyle.SetPalette(kOcean)

    # effDir = homeDir + "/eff/note_v2/pid/no_turbo/"
    effDir = f"{homeDir}/eff/note_v2/{eff_type}/"

    c = TCanvas("canv", "canv", 550, 400)
    gPad.SetLeftMargin(0.15)
    hh = THStack("hh","PID eff distribution");
    legend = TLegend(0.5,0.2,0.88,0.55)
    i=0
    h_list = []
    for key_val in key_list:
        # file = TFile("{}/eff_{}_{}_{}_pid_{}.root".format(effDir,ccbar,source,binsName,key_val), "READ")
        file = TFile(f"{effDir}/eff_{ccbar}_{source}_{binsName}_{eff_type}_{key_val}.root", "READ")
        h = file.Get("hist_eff").Clone(f"h_{key_val}")
        h.SetDirectory(0)
        file.Close()
        h.Print()
        # h.SetName("h_{}".format(key_val))
        h.SetLineColor(i+1)
        h.SetLineWidth(2)
        h.SetMarkerColor(i+1)
        h.SetMarkerStyle(i+20)
        h_list.append(h)
        hh.Add(h)
        # h.SetFillColor(i+1)
        # h.SetFillStyle(3003)
        legend.AddEntry(h,titleDict[key_val],"lep");
        # legend.AddEntry(h,"{}".format(key_val),"lep");
        i+=1
    
    hh.Draw("nostack,e1p")
    hh.GetXaxis().SetTitle(binNameDict[binsName])
    legend.Draw()
    # gPad.BuildLegend();
    c.SaveAs(f"{effDir}/Comparison_{ccbar}_{source}_{binsName}_tools.pdf")

def make_ratio(ccbar, source, ref_key, key_list, binsName="PT"):
    ''' Plot ratio of PID efficiencies obtained with differend methods for the same state '''

    # gStyle.SetPalette(kOcean)

    effDir = f"{homeDir}/eff/note_v2/pid/no_turbo/"
    effDir = f"{homeDir}/eff/note_v2/pid/"

    c = TCanvas("canv", "canv", 550, 400)
    gPad.SetLeftMargin(0.15)
    hh = THStack("hh","PID eff distribution")
    legend = TLegend(0.5,0.2,0.88,0.55)

    file_ref = TFile(f"{effDir}/eff_{ccbar}_{source}_{binsName}_pid_{ref_key}.root", "READ")
    h_ref = file_ref.Get("hist_eff").Clone("h_{}".format(ref_key))
    h_ref.SetDirectory(0)
    file_ref.Close()
    i=0
    h_list = []
    for key_val in key_list:
        file = TFile("{}/eff_{}_{}_{}_pid_{}.root".format(effDir,ccbar,source,binsName,key_val), "READ")
        h = file.Get("hist_eff").Clone("h_{}".format(key_val))
        h.SetDirectory(0)
        file.Close()
        h.Print()
        # h.SetName("h_{}".format(key_val))
        h.SetLineColor(i+1)
        h.SetLineWidth(2)
        h.SetMarkerColor(i+1)
        h.SetMarkerStyle(i+20)
        # h.SetMarkerSize(2)
        h.SetFillColor(i+1)
        h.SetFillStyle(3001)
        h.Divide(h_ref)
        h_list.append(h)
        hh.Add(h)
        # h.SetFillColor(i+1)
        # h.SetFillStyle(3003)
        # legend.AddEntry("h_{}".format(key_val),"{}".format(key_val),"lep");
        legend.AddEntry(h,"{}/{}".format(key_val,ref_key),"lep");
        i+=1
    
    hh.Draw("nostack,e1p")
    # hh.Draw("nostack,e2")
    # hh.Draw("nostack,box,same")
    hh.GetXaxis().SetTitle(binNameDict[binsName])
    legend.Draw()
    # gPad.BuildLegend();
    c.SaveAs("{}/Ratio_{}_{}.pdf".format(effDir,ccbar,source))


def compare_tools(ccbar, source, tool_list, binsName="PT", draw=True):
    ''' Compare PID efficiencies obtained with differend tools for the same state '''

    effDir = homeDir + "/eff/note_v2/pid/"
    # nameTxt = "{}/plots_{}/{}_{}.txt".format(homeDir,source,ccbar,source)
    nameTxt = "{}/{}_{}.txt".format(effDir,ccbar,source)
    with open(nameTxt, "w") as f:

        for tool_type in tool_list:

            filename = "{}/eff_{}_{}_{}_pid_{}.root".format(effDir,ccbar,source,binsName,tool_type)
            hist, eff_val, eff_err = pid_def_eff(ccbar,source,binsName,tool_type)
            hist.Print()
            write2file(filename, hist, eff_val, eff_err)
            # eff_val = TParameter(float)("eff_val", eff_val)
            # eff_err = TParameter(float)("eff_err", eff_err)
            # file = TFile("plots_{}/eff_{}_{}_{}_{}.root".format(source,ccbar,source,binsName,eff_type), "RECREATE")
            # hist.Write()
            # file.WriteObject(eff_val, "eff_val")
            # file.WriteObject(eff_err, "eff_err")
            # file.Close()
            # hist.SaveAs("eff_{}_{}_{}_{}.root".format(ccbar,source,binsName,eff_type))
            # f.write("{}: {} +/- {} \n".format(eff_type, eff_val.GetVal(), eff_err.GetVal()))
            f.write("{}: {} +/- {} \n".format(tool_type, eff_val, eff_err))

            if draw:
                canv = plot_hist(hist, binsName, eff_val, eff_err)
                canv.SaveAs("{}/eff_{}_{}_{}_pid_{}.pdf".format(effDir,ccbar,source,binsName,tool_type))


def compare_ratio(ccbar1, ccbar2, source, tool_list, binsName="PT"):
    ''' Compare the ratio of PID efficiency between two ccbar samples '''

    effDir = homeDir + "/eff/note_v2/pid/"
    hh = THStack("hh","PID eff distribution")
    legend = TLegend(0.6,0.2,0.88,0.5)
    h_list = []

    for i, tool_type in enumerate(tool_list):
            
            filename1 = "{}/eff_{}_{}_{}_pid_{}.root".format(effDir,ccbar1,source,binsName,tool_type)
            file_ref = TFile(filename1, "READ")
            hist1 = file_ref.Get("hist_eff").Clone("h_{}".format(tool_type))
            hist1.SetDirectory(0)
            file_ref.Close()
            filename2 = "{}/eff_{}_{}_{}_pid_{}.root".format(effDir,ccbar2,source,binsName,tool_type)
            file_ref = TFile(filename2, "READ")
            hist2 = file_ref.Get("hist_eff").Clone("h_{}".format(tool_type))
            hist2.SetDirectory(0)
            file_ref.Close()
            hist1.Divide(hist2)
            # eff_val = eff_val1/eff_val2
            # eff_err = eff_val * math.sqrt(eff_err1**2/eff_val1**2 + eff_err2**2/eff_val2**2)
            if i!=4:
                hist1.SetLineColor(i+1)
            else:
                hist1.SetLineColor(i+2)
            hist1.GetYaxis().SetLimits(0.7,1.3)
            legend.AddEntry(hist1,"{}".format(tool_type),"lep")
            h_list.append(hist1)
            hh.Add(hist1)
    canv = TCanvas("c","c",800,600)
    hh.Draw("nostack,e1p")
    # hh.Draw("nostack,e2")
    # hh.Draw("nostack,box,same")
    hh.GetXaxis().SetTitle(binNameDict[binsName])
    # hh.GetYaxis().SetLimits(0.7,1.3)
    hh.SetMaximum(1.15)
    hh.SetMinimum(0.78)
    canv.Update()
    legend.Draw()
    # gPad.BuildLegend();
    canv.SaveAs("{}/Comparison_PID_eff_{}2{}_{}_{}_pid.pdf".format(effDir,ccbar1,ccbar2,source,binsName))

def draw_all(ccbar_list, source, tool_list, binsName="PT"):
    ''' Draw all the PID efficiency plots '''

    effDir = f"{homeDir}/eff/note_v2/pid/"
    for tool_type in tool_list:
        hh = THStack("hh","PID eff distribution")
        legend = TLegend(0.5,0.2,0.88,0.55)
        for i, ccbar in enumerate(ccbar_list):
            filename = "{}/eff_{}_{}_{}_pid_{}.root".format(effDir,ccbar,source,binsName,tool_type)
            file_ref = TFile(filename, "READ")
            hist = file_ref.Get("hist_eff").Clone("h_{}".format(tool_type))
            hist.SetDirectory(0)
            file_ref.Close()
            hist.SetLineColor(i+1)
            legend.AddEntry(hist,"{} {}".format(ccbar,tool_type),"lep")
            hh.Add(hist)

        canv = TCanvas("c","c",800,600)
        hh.Draw("nostack,e1p")
        canv.SaveAs("{}/PID_eff_{}_{}_pid_all.pdf".format(effDir, source, tool_type))


if __name__ == "__main__":

    gROOT.LoadMacro("../fit/libs/lhcbStyle.C")
    kSources = ["prompt","fromB"]
    # kSources = ["prompt","sec"]
    # kSources = ["fromB"]
    # kRanges = ["Etac"]
    # binsName = "PT"
    binsList = ["PT","Y"]
    binsList = ["PT"]
    kRanges = ["etac","etac2S","jpsi","psi2S","chic0","chic1","chic2"]
    # kRanges = ["etac2S","psi2S","chic0","chic1","chic2"]
    # kRanges = ["chic0","chic1","chic2"]
    # kRanges = ["jpsi","etac"]
    # kRanges = ["etac2S","psi2S"]
    # for kRange in kRanges:
    #     for kSource in kSources:
    #         for binsName in binsList:
                # calc_PID(kRange, kSource, "all", binsName)
                # calc_PID(kRange, kSource, "ProbNN", binsName)
                # calc_PID(kRange, kSource, "DLL", binsName)
                # calc_PID(kRange, kSource, "ProbNNTight", binsName)
                # calc_PID(kRange, kSource, "DLLTight", binsName)
                # compare(kRange, kSource, ["def","5bin"], binsName,"l0")
                # compare(kRange, kSource, ["calib","5bin"], binsName,"l0")
                # compare(kRange, kSource, ["def","calib"], binsName,"l0")
                # compare(kRange, kSource, ["calib","def","5bin"], binsName,"l0")
                # compare(kRange, kSource, ["def","all"], binsName)
                # compare(kRange, kSource, ["DLL","ProbNN","all"], binsName)
                # compare_tools(kRange, kSource,["none","gen","gen_Lc","gen_Lb","corr"], binsName)
                # compare(kRange, kSource, ["none","gen","gen_Lc","gen_Lb","corr","all"], "PT")
                # compare(kRange, kSource, ["none","all"], "PT")
                # compare(kRange, kSource, ["def","all"], "PT")
                # make_ratio(kRange, kSource, "all", ["def"], binsName)
                # make_ratio(kRange, kSource, "all", ["ProbNN","DLL"], binsName)
                # make_ratio(kRange, kSource, "all", ["ProbNN","DLL"], binsName)

    for kSource in kSources:
        for binsName in binsList:
            # draw_all(["jpsi","psi2S"], kSource, ["ProbNN","DLL","all"], binsName)
            draw_all(["jpsi","psi2S"], kSource, ["ProbNN","DLL","all"], binsName)
            # draw_all(kRanges, kSource, ["ProbNN","DLL","all"], binsName)
            # compare_ratio("jpsi","psi2S",kSource,["ProbNN","ProbNNTight","DLL","DLLTight","all"],binsName)


