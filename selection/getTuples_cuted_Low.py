from ROOT import gROOT, TChain, TCut, TFile, TTree, TProof
from datetime import datetime
start=datetime.now()



optVar_list  = ['Jpsi_M', 'Jpsi_m_scaled', 'Jpsi_MM','Jpsi_ENDVERTEX_CHI2','Jpsi_ENDVERTEX_Z','Jpsi_OWNPV_Z','Jpsi_PT','Jpsi_FDCHI2_OWNPV',
                'Jpsi_PE','Jpsi_PZ','Jpsi_P',
                # 'Jpsi_ETA',
                'Jpsi_Y',
                'Jpsi_L0HadronDecision_TOS',
                'ProtonP_P', 'ProtonP_PT','ProtonM_P','ProtonM_PT',
                'ProtonP_TRACK_CHI2NDOF','ProtonM_TRACK_CHI2NDOF',
                'ProtonP_ProbNNp','ProtonM_ProbNNp',
                'ProtonP_ProbNNpi','ProtonM_ProbNNpi',
                'ProtonP_ProbNNk','ProtonM_ProbNNk',
                'ProtonP_PIDK', 'ProtonP_PIDp', 'ProtonM_PIDK', 'ProtonM_PIDp',
                'ProtonP_IPCHI2_OWNPV', 'ProtonM_IPCHI2_OWNPV',
                'ProtonP_TRACK_GhostProb', 'ProtonM_TRACK_GhostProb', 'ProtonP_TRACK_CloneDist', 'ProtonM_TRACK_CloneDist',
                'ProtonP_TRACK_GhostProb', 'ProtonM_TRACK_GhostProb', 'ProtonP_TRACK_CloneDist', 'ProtonM_TRACK_CloneDist',
                'Jpsi_M01_Subst0_p2pi','Jpsi_M01_Subst1_p2pi',
                'Jpsi_M01_Subst0_p2K','Jpsi_M01_Subst1_p2K',
                'Jpsi_M01_Subst01_pp~2KK','Jpsi_M01_Subst01_pp~2Kpi','Jpsi_M01_Subst01_pp~2piK','Jpsi_M01_Subst01_pp~2pipi',
                'ProtonP_PX','ProtonM_PX',
                'ProtonP_PY','ProtonM_PY',
                'ProtonP_PZ','ProtonM_PZ',
]



def get_cut(kRange, kSource):


    cut_L0  = TCut('Jpsi_L0HadronDecision_TOS')
    if kRange=="Low":
        cut_Hlt = TCut('Jpsi_Hlt1DiProtonDecision_TOS')
    else:
        cut_Hlt = TCut('Jpsi_Hlt1DiProtonHighDecision_TOS')


    cut_Jpsi = TCut('Jpsi_ENDVERTEX_CHI2<4.0')
    cut_Jpsi_Y = TCut('Jpsi_Y > 2 && Jpsi_Y < 4.5')

    if kSource=="prompt":
        cut_FD = TCut('(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08')
    elif kSource=="fromB":
        cut_FD = TCut('(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16')
    else:
        cut_FD = TCut('1')

    cut_Protons = TCut('ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
                        ProtonP_PT>2000 && ProtonM_PT>2000 && \
                        ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366 && \
                        ProtonP_TRACK_CHI2NDOF<2.5 && ProtonM_TRACK_CHI2NDOF<2.5 && \
                        ProtonP_TRACK_CloneDist<0 && ProtonM_TRACK_CloneDist<0 && \
                        ProtonP_PIDp>5 && ProtonM_PIDp>5 && \
                        (ProtonP_PIDp-ProtonP_PIDK)>0 && (ProtonM_PIDp-ProtonM_PIDK)>0 && \
                        ProtonP_TRACK_GhostProb<0.2 && ProtonM_TRACK_GhostProb<0.2 \
                      ')

    totCut = TCut(cut_L0 + cut_Hlt + cut_Protons + cut_Jpsi + cut_Jpsi_Y + cut_FD)
    print("Selection cut: .................... \n {} \n ".format(totCut.GetTitle()))
    return totCut

def build_map(kSource, kRange):



    #print('Tree cutted successfully')
    nt_MC = TChain("DecayTree")
    if kRange=="Low":
        optVar_list.apend('Jpsi_Hlt1DiProtonDecision_TOS')
        nt_MC.Add("{}/MC/Trigger/{}DiProton_{}_2018_eff.root".format(dataDir, "etac", kSource))
        #if kSource=="prompt":
        nt_MC.Add("{}/MC/Trigger/{}DiProton_{}_2018_eff.root".format(dataDir, "jpsi", kSource))
    else:
        optVar_list.apend('Jpsi_Hlt1DiProtonHighDecision_TOS')
        nt_MC.Add("{}/MC/Trigger/{}DiProton_{}_2018_eff.root".format(dataDir, "chic*", kSource))
        nt_MC.Add("{}/MC/Trigger/{}DiProton_{}_2018_eff.root".format(dataDir, "etac2S", kSource))
        #if kSource=="prompt":
        nt_MC.Add("{}/MC/Trigger/{}DiProton_{}_2018_eff.root".format(dataDir, "psi2S", kSource))


    print("MC tree is created .................... \n ")

    path = "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/PRODPOL/etac2s_2018/ppbar/remake/"
    chain = TChain("Jpsi2ppTuple/DecayTree")
    jobs= {
            "Low":    {152: 407,
                       153: 472},
            "High":   {150: 407,
                       151: 472}
            }

    for j in jobs[kRange]:
        for sj in range(jobs[kRange][j]):
            chain.Add("{}/{}/{}/Tuple.root".format(path, j, sj))


    chain.SetBranchStatus("*",0)
    for var in optVar_list:
        chain.SetBranchStatus("{}".format(var),1);

    print("DATA tree is created .................... \n ")

    totCut = get_cut(kRange, kSource)

    newfile = TFile("{0}/Data_{1}_eff/Etac2sDiProton_{1}_2018.root".format(dataDir, kRange),"recreate")
    newtree=TTree()
    newtree.SetMaxTreeSize(500000000)
    #newtree = cuttedtree.CloneTree();
    newtree = chain.CopyTree(totCut.GetTitle())

    newtree.Print()

    newtree.GetCurrentFile().Write()
    newtree.GetCurrentFile().Close()
    print( datetime.now()-start)


    eff_trigger   = array( "d", [0])
    eff_selection = array( "d", [0])


    f_new = TFile(dirName+'/Data_{}_eff/Tuple_w.root', "RECREATE")

    tree = nt.CloneTree(0)
    tree.Branch("eff_trigger",eff_trigger,"eff_trigger/D")
    tree.Branch("eff_selection",eff_selection,"eff_selection/D")


    #f = TFile("{}/MC/Trigger/{}DiProton_{}_2018_eff.root".format(dataDir, kRange, kSource))
    #t_MC = f.Get("DecayTree")


    for event in nt:
        x = {
            "ProtonM_PX":                       nt.ProtonM_PX,
            "ProtonP_PX":                       nt.ProtonP_PX,
            "ProtonM_PY":                       nt.ProtonM_PY,
            "ProtonP_PY":                       nt.ProtonP_PY,
            "ProtonM_PZ":                       nt.ProtonM_PZ,
            "ProtonP_PZ":                       nt.ProtonP_PZ,
            #"ProtonM_TRACK_CHI2NDOF":           nt.ProtonM_TRACK_CHI2NDOF,
            #"ProtonP_TRACK_CHI2NDOF":           nt.ProtonP_TRACK_CHI2NDOF,
            }
        cut = "1"
        for key in x:
            bin_n = np.digitize([x[key]], binning[key])[0]
            left  = binning[key][bin_n-1]
            right = binning[key][bin_n]
            cut = "{0} && ({1} > {2} && {1} < {3})".format(cut, key, left, right)

        n_sel = float(nt_MC.GetEntries("{} && eff_selection>0.0".format(cut)))
        n_tr  = float(nt_MC.GetEntries("{} && eff_trigger>0.0".format(cut)))
        n     = float(nt_MC.GetEntries(cut))
        #print(cut,n, n_cut)
        if n>1e-6:
            eff_selection[0] = n_sel/n
            eff_trigger[0]   = n_tr/n
            #print(eff_etac[0])
            #input()

        else:
            eff_selection[0] = 0.
            eff_trigger[0]   = 0.
        tree.Fill()

    tree.Write()

    f_new.Close()
    del tree, nt, nt_MC
