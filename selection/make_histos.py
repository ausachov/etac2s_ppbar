from ROOT import TChain, TH1D, TCut, TList
from ROOT import RooRealVar, RooWorkspace, RooDataHist, RooArgList
from ROOT import EnableImplicitMT, RDataFrame
from ROOT.RDF import TH1DModel

from drawModule import binN_Tot_Fit, minM_Low, maxM_High
import charmConstLib as cl

import numpy as np

# from setupPlot import *
# from setupFitModel_RunI import *

#homeDir = "/afs/cern.ch/user/v/vazhovko/private/etac2s_ppbar/"
#dataDir = "/eos/user/v/vazhovko/etac2s_ppbar/"
homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

var_list = ['Jpsi_m_scaled', 'Jpsi_MM',
            #'Jpsi_ENDVERTEX_CHI2',
            'Jpsi_ENDVERTEX_Z','Jpsi_OWNPV_Z',
            'Jpsi_PT',
            'Jpsi_Y',
            #'Jpsi_FDCHI2_OWNPV',
            'Jpsi_PZ',#'Jpsi_PE','Jpsi_P',
            'ProtonP_ProbNNp','ProtonM_ProbNNp',
            'ProtonP_ProbNNpi','ProtonM_ProbNNpi',
            'ProtonP_ProbNNk','ProtonM_ProbNNk',
            #'ProtonP_P','ProtonM_P',
            #'ProtonP_PT','ProtonM_PT',
            #'ProtonP_TRACK_CHI2NDOF','ProtonM_TRACK_CHI2NDOF',
            #'ProtonP_PIDK', 'ProtonP_PIDp', 'ProtonM_PIDK', 'ProtonM_PIDp',
            'ProtonP_IPCHI2_OWNPV', 'ProtonM_IPCHI2_OWNPV',
            #'ProtonP_TRACK_GhostProb', 'ProtonM_TRACK_GhostProb', 'ProtonP_TRACK_CloneDist', 'ProtonM_TRACK_CloneDist',
            #'Jpsi_M01_Subst0_p2pi','Jpsi_M01_Subst1_p2pi',
            #'Jpsi_M01_Subst0_p2K','Jpsi_M01_Subst1_p2K',
            #'Jpsi_M01_Subst01_pp~2KK','Jpsi_M01_Subst01_pp~2pipi',
            #'Jpsi_M01_Subst01_pp~2Kpi','Jpsi_M01_Subst01_pp~2piK',

            ]

binningDict = {
    "Jpsi_PT":                      [5000., 6500., 8000., 10000., 12000., 14000., 20000.],
    "Jpsi_Y":                       [2.0, 2.5, 3.0, 3.5, 4.0, 4.5],
}


def prepare_cut(isPrompt, pPID, nPT=0, nY=0):

    if isPrompt=="prompt":
        cut_FD = TCut('(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08')
    else:
        cut_FD = TCut('(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16')

    variable = "Jpsi_PT"
    if nPT==0:
       cut_PT = TCut("{0}>{1} && {0}<{2}".format(variable,binningDict[variable][0],binningDict[variable][-1]))
    else:
       cut_PT = TCut("{0}>{1} && {0}<{2}".format(variable,binningDict[variable][nPT-1],binningDict[variable][nPT]))

    variable = "Jpsi_Y"
    if nY==0:
       cut_Y = TCut("{0}>{1} && {0}<{2}".format(variable,binningDict[variable][0],binningDict[variable][-1]))
    else:
       cut_Y = TCut("{0}>{1} && {0}<{2}".format(variable,binningDict[variable][nPT-1],binningDict[variable][nPT]))

    cut_misID_Kpi = TCut("Jpsi_M01_Subst01_pp~2Kpi>1895 || Jpsi_M01_Subst01_pp~2Kpi<1835")
    cut_misID_piK = TCut("Jpsi_M01_Subst01_pp~2piK>1895 || Jpsi_M01_Subst01_pp~2piK<1835")

    cut_misID_pipi = TCut("(Jpsi_M01_Subst01_pp~2pipi>1805 || Jpsi_M01_Subst01_pp~2piK<1770)")
    cut_misID_KK = TCut("(Jpsi_M01_Subst01_pp~2KK>(1245 + 0.3196*Jpsi_m_scaled) || Jpsi_M01_Subst01_pp~2KK<(1225 + 0.3196*Jpsi_m_scaled))")

    #default PID cut
    cut_PID     = TCut(f"ProtonP_ProbNNp > {pPID} && ProtonM_ProbNNp > {pPID}")

    cut_total = TCut(cut_FD + cut_PT + cut_PID)
    cut_total.Print()
    # totCut_High = TCut(cut_FD + cut_PT + cut_PID + cut_misID_Kpi + cut_misID_piK)

    return cut_total

def get_data_df_h(w, kSource):

    var_int = "Jpsi_m_scaled"
    EnableImplicitMT()

    # minM_Low, maxM_High = 1000., 4000.
    # binN_Tot_Fit = int((maxM_High-minM_Low)/5)
    pPID = 0.6
    dirName_Low  = dataDir +"Data_Low_NoPID_new/"
    dirName_High = dataDir +"Data_High_NoPID_new/"

    nt_Low  =  TChain("DecayTree")
    nt_High =  TChain("DecayTree")

    nt_Low.Add(dirName_Low+"/Etac2sDiProton_Low_2018*.root")
    nt_High.Add(dirName_High+"/Etac2sDiProton_High_2018*.root")

    df_Low = RDataFrame(nt_Low)
    df_High = RDataFrame(nt_High)

    totCut = prepare_cut(kSource, pPID)

    histModelL = TH1DModel('hh_Low','hh', binN_Tot_Fit, minM_Low, maxM_High)
    histModelH = TH1DModel('hh_High','hh', binN_Tot_Fit, minM_Low, maxM_High)
    hh = TH1D("hh","hh", binN_Tot_Fit, minM_Low, maxM_High)
    print( "[INFO] TH1D is created....")

    # hh = df.Filter(totCut.GetTitle()).Histo1D(histModel,"Jpsi_m_scaled") # this fills the histogram of x in parallel, leveraging the aforementioned pool
    # hh.Draw("goff") # RResultPtr<TH1D> - vector of results
    # hh.GetPtr().Print()
    hh_Low = df_Low.Filter(totCut.GetTitle()).Histo1D(histModelL,var_int) # this fills the histogram of x in parallel, leveraging the aforementioned pool
    hh_Low.Draw("goff") # RResultPtr<TH1D> - vector of results
    hh_Low.GetPtr().Print()
    hh_High = df_High.Filter(totCut.GetTitle()).Histo1D(histModelH,var_int) # this fills the histogram of x in parallel, leveraging the aforementioned pool
    hh_High.Draw("goff") # RResultPtr<TH1D> - vector of results
    hh_High.GetPtr().Print()

    Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M", minM_Low, maxM_High)

    hlist = TList()
    hlist.Add(hh_Low.GetPtr())
    hlist.Add(hh_High.GetPtr())
    hh.Reset()
    hh.Merge(hlist)
    hh.Print()


    dh_Low = RooDataHist("dh_Low","dh_Low", RooArgList(Jpsi_M), hh_Low.GetPtr())
    dh_High = RooDataHist("dh_High","dh_High", RooArgList(Jpsi_M), hh_High.GetPtr())
    dh = RooDataHist("dh","dh", RooArgList(Jpsi_M), hh)
    getattr(w,"import")(dh_Low)
    getattr(w,"import")(dh_High)
    getattr(w,"import")(dh)

    print( "[INFO] RooDataHist is created....")

    nameWksp = f"{dataDir}/wksps/wksp_data_{kSource}_ProbNNp_0{int(10*pPID)}_df.root"
    w.writeToFile(nameWksp)
    # hh.Draw("e1")
    # input()

def get_data_h(w, kSource):

    dirName_Low  = dataDir +"Data_Low_NoPID/"
    dirName_High = dataDir +"Data_High_NoPID/"


    nt_Low  =  TChain("DecayTree")
    nt_High =  TChain("DecayTree")

    nt_Low.Add(dirName_Low+"/Etac2sDiProton_Low_2018*.root")
    nt_High.Add(dirName_High+"/Etac2sDiProton_High_2018*.root")

    #tree_Low = TTree()
    #tree_High = TTree()

    cut_FD = ""
    if kSource=="prompt":
        cut_FD = TCut("(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08")# & Jpsi_FDCHI2_OWNPV > 49"
    else:
        cut_FD = TCut("(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16")# & Jpsi_FDCHI2_OWNPV > 49"

    # cut_PT      = TCut("Jpsi_PT > 5000 && Jpsi_PT < 20000")
    cut_PT      = TCut("Jpsi_PT > 5000 && Jpsi_PT < 14000")
    # cut_PT      = TCut("Jpsi_PT > 6500 && Jpsi_PT < 8000")
    #cut_PID     = TCut("ProtonP_ProbNNk < 0.6 && ProtonM_ProbNNk < 0.6 && ProtonP_ProbNNp > 0.4 && ProtonM_ProbNNp > 0.4 && ProtonP_ProbNNpi < 0.6 && ProtonM_ProbNNpi < 0.6")
    # cut_PID     = TCut("ProtonP_ProbNNk < 0.6 && ProtonM_ProbNNk < 0.6 && ProtonP_ProbNNp > 0.6 && ProtonM_ProbNNp > 0.6 && ProtonP_ProbNNpi < 0.6 && ProtonM_ProbNNpi < 0.6 ")
    # cut_PID     = TCut("ProtonP_ProbNNp > 0.8 && ProtonM_ProbNNp > 0.8")
    cut_PID     = TCut("ProtonP_ProbNNp > 0.6 && ProtonM_ProbNNp > 0.6")

    cut_misID_Kpi = TCut("(Jpsi_M01_Subst01_pp~2Kpi>1895 || Jpsi_M01_Subst01_pp~2Kpi<1835)")
    cut_misID_piK = TCut("(Jpsi_M01_Subst01_pp~2piK>1895 || Jpsi_M01_Subst01_pp~2piK<1835)")

    cut_misID_pipi = TCut("(Jpsi_M01_Subst01_pp~2pipi>1805 || Jpsi_M01_Subst01_pp~2piK<1770)")

    cut_misID_KK = TCut("(Jpsi_M01_Subst01_pp~2KK>(1245 + 0.3196*Jpsi_m_scaled) || Jpsi_M01_Subst01_pp~2KK<(1225 + 0.3196*Jpsi_m_scaled))")

    totCut = TCut(cut_FD + cut_PT + cut_PID)
    totCut_High = TCut(cut_FD + cut_PT + cut_PID + cut_misID_Kpi + cut_misID_piK)

    # binN_Tot_Fit, minM_Low, maxM_High = 240, 2800., 4000.
    hh_Low = TH1D("hh_Low","hh_Low", binN_Tot_Fit, minM_Low, maxM_High)
    hh_High = TH1D("hh_High","hh_High", binN_Tot_Fit, minM_Low, maxM_High)
    hh = TH1D("hh","hh", binN_Tot_Fit, minM_Low, maxM_High)

    nt_Low.Draw("Jpsi_m_scaled>>hh_Low",totCut_High.GetTitle(),"goff")
    nt_High.Draw("Jpsi_m_scaled>>hh_High",totCut_High.GetTitle(),"goff")

    hlist = TList()
    hlist.Add(hh_Low)
    hlist.Add(hh_High)
    hh.Reset()
    hh.Merge(hlist)

    Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M", minM_Low, maxM_High)
    #Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M", minM_High, maxM_High)

    dh_Low = RooDataHist("dh_Low","dh_Low", RooArgList(Jpsi_M), hh_Low)
    dh_High = RooDataHist("dh_High","dh_High", RooArgList(Jpsi_M), hh_High)
    dh = RooDataHist("dh","dh", RooArgList(Jpsi_M), hh)
    getattr(w,"import")(dh_Low)
    getattr(w,"import")(dh_High)
    getattr(w,"import")(dh)

    print( "[INFO] RooDataHist is created....")

    # nameWksp = dataDir + "/wksps/wksp_data_{}_base_bin2.root".format(kSource)
    # nameWksp = dataDir + "/wksps/wksp_data_{}_ProbNNp_08.root".format(kSource)
    nameWksp = dataDir + "/wksps/wksp_data_{}_ProbNNp_06_pt5_14.root".format(kSource)
    w.writeToFile(nameWksp)


if __name__ == "__main__":
    # w = RooWorkspace("w",True)
    # get_data_h(w, "fromB")
    # w = RooWorkspace("w",True)
    # get_data_h(w, "prompt")

    w = RooWorkspace("w",True)
    get_data_df_h(w, "fromB")
    w = RooWorkspace("w",True)
    get_data_df_h(w, "prompt")

    # del w
