from genericpath import isfile
import ROOT
from ROOT import TChain, TCut, TFile
from ROOT import EnableImplicitMT, RDataFrame
from ROOT.RDF import RSnapshotOptions
from ROOT.RDF import TH1DModel

from datetime import datetime
import os

# homeDir = "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/PRODPOL/etac2s_2018/ppbar/remake/"
homeDir = "/sps/lhcb/zhovkovska/BandQ/PRODPOL/etac2s_2018/ppbar/remake/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

jobs= {
        "Low":    {152: 407,
                    153: 472},
        "High":   {150: 407,
                    151: 472}
        }

# jobs = {
#     "LowUp"     : 153,
#     "LowDown"   : 152,
#     "HihjUp"    : 150,
#     "HihjDown"  : 151,
# }

var_list = {'Jpsi_M', 'Jpsi_m_scaled', 'Jpsi_MM',
            'Jpsi_ENDVERTEX_CHI2','Jpsi_ENDVERTEX_Z','Jpsi_OWNPV_Z','Jpsi_PT','Jpsi_FDCHI2_OWNPV',
            'Jpsi_PE','Jpsi_PZ','Jpsi_P',
            'Jpsi_DOCA',
            'nSPDHits',
            'Jpsi_Y',
            'Jpsi_L0HadronDecision_TOS', 'Jpsi_Hlt1DiProtonDecision_TOS', 
            'ProtonP_P', 'ProtonP_PT','ProtonM_P','ProtonM_PT',
            'ProtonP_TRACK_CHI2NDOF','ProtonM_TRACK_CHI2NDOF',
            'ProtonP_ProbNNp','ProtonM_ProbNNp',
            'ProtonP_ProbNNpi','ProtonM_ProbNNpi',
            'ProtonP_ProbNNk','ProtonM_ProbNNk',
            'ProtonP_PIDK', 'ProtonP_PIDp', 'ProtonM_PIDK', 'ProtonM_PIDp',
            'ProtonP_IPCHI2_OWNPV', 'ProtonM_IPCHI2_OWNPV',
            'ProtonP_TRACK_GhostProb', 'ProtonM_TRACK_GhostProb', 'ProtonP_TRACK_CloneDist', 'ProtonM_TRACK_CloneDist',
            'Jpsi_M01_Subst0_p2pi','Jpsi_M01_Subst1_p2pi',
            'Jpsi_M01_Subst0_p2K','Jpsi_M01_Subst1_p2K',
            'Jpsi_M01_Subst01_pp~2KK','Jpsi_M01_Subst01_pp~2Kpi','Jpsi_M01_Subst01_pp~2piK','Jpsi_M01_Subst01_pp~2pipi',
            }


def prepare_cut(reg:str, fd:str=""):

    cut_L0 = TCut('Jpsi_L0HadronDecision_TOS')
    cut_Hlt = TCut('Jpsi_Hlt1DiProtonDecision_TOS')
    if reg=="High":
        cut_Hlt = TCut('Jpsi_Hlt1DiProtonHighDecision_TOS')


    cut_Jpsi = TCut('Jpsi_ENDVERTEX_CHI2<4.0 && nSPDHits < 300 && Jpsi_DOCA < 0.1')
    cut_Jpsi_Y = TCut('Jpsi_Y > 2 && Jpsi_Y < 4.5')

    cut_Protons = TCut('ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
        ProtonP_PT>2000 && ProtonM_PT>2000 && \
        ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366 && \
        ProtonP_TRACK_CHI2NDOF<2.5 && ProtonM_TRACK_CHI2NDOF<2.5 && \
        ProtonP_TRACK_CloneDist<0 && ProtonM_TRACK_CloneDist<0 && \
        ProtonP_TRACK_GhostProb<0.2 && ProtonM_TRACK_GhostProb<0.2  \
    ')
    cut_PID     = TCut("ProtonP_ProbNNp > 0.6 && ProtonM_ProbNNp > 0.6")

    cut_FD = TCut("")
    if fd=="pr":
        cut_FD = TCut("(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08")
    if fd=="fb":
        cut_FD  = TCut('(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16')# & Jpsi_FDCHI2_OWNPV > 49')

    cut_total = TCut(cut_L0 + cut_Hlt + cut_Protons + cut_Jpsi + cut_Jpsi_Y + cut_FD)
    # cut_total = TCut( cut_PT + cut_Y + cut_PID)
    cut_total.Print()

    return cut_total


def cut_data_df(reg="Low"):


    EnableImplicitMT()
    dirName  = f"{dataDir}/Data_{reg}_NoPID_new/"
    if not os.path.isdir(f"{dataDir}/Data_{reg}_NoPID_new/"):
        os.mkdir(f"{dataDir}/Data_{reg}_NoPID_new/")
    i = 0
    nt  =  TChain("Jpsi2ppTuple/DecayTree")
    # nt.Add(f"{homeDir}/{jobs[reg+'Up']}/0/Tuple.root")
    # nt.Add(f"{homeDir}/{jobs[reg+'Down']}/0/Tuple.root")
    for j in jobs[reg]:
        nsj = jobs[reg][j]

        for i in range(nsj):
            if i%5==0:
                nt  =  TChain("Jpsi2ppTuple/DecayTree")
                # nt.Reset()
                # nt.Print()
            localPath = f"{homeDir}/{j}/{i}/Tuple.root"
            if os.path.isfile(localPath):
                nt.Add(localPath)
                print(localPath, i)


            if i%5==4:
                nt.SetBranchStatus("*",0)
                for var in var_list:
                    nt.SetBranchStatus("{}".format(var),1);

                df = RDataFrame(nt)

                totCut = prepare_cut(reg)

                print( "[INFO] TChain is loaded....")

                df_new = df.Filter(totCut.GetTitle())
                df_new = df_new.Define("Jpsi_Tz","(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ")

                fname = f"{dataDir}/Data_{reg}_NoPID_new/Etac2sDiProton_{reg}_2018_{10*j}{i}.root"
                df_new.Snapshot(treename="DecayTree", filename=fname, columnList=var_list)#, options=rso)
    # df_new.Snapshot(treename="DecayTree", filename=fname)


def cut_hist_df(reg="Low"):


    EnableImplicitMT()
    var_int = "Jpsi_m_scaled"
    fd = "fb"
    dirName  = f"{dataDir}/Data_{reg}_NoPID_new/"

    nt  =  TChain("Jpsi2ppTuple/DecayTree")
    for x in os.walk(f"{homeDir}/{jobs[reg+'Up']}"):
        if os.path.isfile(f"{x[0]}/Tuple.root"):
            nt.Add(f"{x[0]}/Tuple.root")

    for x in os.walk(f"{homeDir}/{jobs[reg+'Down']}"):
        if os.path.isfile(f"{x[0]}/Tuple.root"):
            nt.Add(f"{x[0]}/Tuple.root")

    nt.SetBranchStatus("*",0)
    for var in var_list:
        nt.SetBranchStatus("{}".format(var),1);

    df = RDataFrame(nt)

    totCut = prepare_cut(reg, fd)

    print( "[INFO] TChain is loaded....")

    # df_new = df.Filter(totCut.GetTitle())
    # df_new = df_new.Define("Jpsi_Tz","(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ")

    minM_Low, maxM_High = 2850, 3780
    binN_Tot_Fit = int((maxM_High-minM_Low)/0.25)
    nameHist = f"{dirName}/hist_{reg}_{fd}.root"

    f = TFile(nameHist, "recreate")

    histModel = TH1DModel('hh','hh', binN_Tot_Fit, minM_Low, maxM_High)

    hh = df.Filter(totCut.GetTitle()).Histo1D(histModel,var_int) # this fills the histogram of x in parallel, leveraging the aforementioned pool
    hh.Draw("goff") # RResultPtr<TH1D> - vector of results
    hh.GetPtr().Print()

    hh.Write() 
    f.Close()   


if __name__ == "__main__":

    start=datetime.now()
    cut_data_df("High")
    # cut_hist_df("Low")
    print( datetime.now()-start)
