from ROOT import TFile, TChain, TTree, TH1D, TCanvas, TCut, TMath
#from ROOT import RooFit, RooStats
from ROOT import RooRealVar, RooWorkspace, RooDataHist, RooArgList
from ROOT import EnableImplicitMT, RDataFrame
from ROOT.RDF import TH1DModel

import sys

sys.path.insert(1, "../fit/mass_fit")
sys.path.insert(1, "../fit/libs")

# from drawModule import *
import charmConstLib as cl
minM_Low = 2850
maxM_Low = 3250

#homeDir = "/afs/cern.ch/user/v/vazhovko/private/etac2s_ppbar/"
#dataDir = "/eos/user/v/vazhovko/etac2s_ppbar/"
homeDir = "/users/LHCb/zhovkovska/scripts/etac2s_ppbar/results/"
dataDir = "/sps/lhcb/zhovkovska/etac2s_ppbar/"

var_list = ['Jpsi_m_scaled', 'Jpsi_MM',
            'Jpsi_ENDVERTEX_Z','Jpsi_OWNPV_Z',
            'Jpsi_PT',
            'Jpsi_Y',
            'Jpsi_PZ',#'Jpsi_PE','Jpsi_P',
            'ProtonP_ProbNNp','ProtonM_ProbNNp',
            'ProtonP_ProbNNpi','ProtonM_ProbNNpi',
            'ProtonP_ProbNNk','ProtonM_ProbNNk',
            'ProtonP_IPCHI2_OWNPV', 'ProtonM_IPCHI2_OWNPV',
            ]

binningDict = {
    "Jpsi_PT":                      [5000., 6500., 8000., 10000., 12000., 14000., 20000.],
    # "Jpsi_PT":                      [5000., 6500., 8000., 10000., 12000., 14000.],
    # "Jpsi_PT":                      [6500., 8000., 10000., 12000., 14000.],
    "Jpsi_Y":                       [2.0, 2.5, 3.0, 3.5, 4.0, 4.5],
}


def prepare_cut(isPrompt:bool, nPT:int=0, nY:int=0, pPID:float=0.6):

    if isPrompt:
        cut_FD = TCut('(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < 0.08')
    else:
        cut_FD = TCut('(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > 0.08 && ProtonP_IPCHI2_OWNPV>16 && ProtonM_IPCHI2_OWNPV>16')

    variable = "Jpsi_PT"
    if nPT==0:
       cut_PT = TCut("{0}>{1} && {0}<{2}".format(variable,binningDict[variable][0],binningDict[variable][-1]))
    else:
       cut_PT = TCut("{0}>{1} && {0}<{2}".format(variable,binningDict[variable][nPT-1],binningDict[variable][nPT]))

    variable = "Jpsi_Y"
    if nY==0:
       cut_Y = TCut("{0}>{1} && {0}<{2}".format(variable,binningDict[variable][0],binningDict[variable][-1]))
    else:
       cut_Y = TCut("{0}>{1} && {0}<{2}".format(variable,binningDict[variable][nPT-1],binningDict[variable][nPT]))

    #default PID cut
    # cut_PID     = TCut(f"ProtonP_ProbNNp > {pPID} && ProtonM_ProbNNp > {pPID}")
    cut_PID     = TCut("ProtonP_PIDp > 20.0 && ProtonM_PIDp > 20.0 && (ProtonP_PIDp-ProtonP_PIDK)>15 && (ProtonM_PIDp-ProtonM_PIDK)>15")    

    cut_total = TCut(cut_FD + cut_PT + cut_Y + cut_PID)
    cut_total.Print()

    return cut_total

def get_data_h(w, keyPrompt, nPT=0, nY=0, dfON=True, shift=False):

    if dfON:
        EnableImplicitMT()

    pPID = 0.6
    dirName  = f"{dataDir}/Data_Low_NoPID/"

    nt  =  TChain("DecayTree")
    nt.Add(dirName+'/Etac2sDiProton_Low_2018*.root')

    nt.SetBranchStatus("*",0)
    for var in var_list:
        nt.SetBranchStatus(f"{var}",1);
    df = RDataFrame(nt)

    kSource = ''
    if keyPrompt:
        kSource = "prompt"
    else:
        kSource = "fromB"
    histName = f"dh_PT{nPT}_{kSource[0].capitalize()}{kSource[1:]}"

    totCut = prepare_cut(keyPrompt, nPT, nY, pPID)

    Jpsi_M = RooRealVar('Jpsi_M',"Jpsi_M", minM_Low, maxM_Low)
    hh = TH1D('hh','hh', 2000, minM_Low, maxM_Low)
    print( "[INFO] TH1D is created....")
    dh = RooDataHist()

    if(shift):
        hh.SetBins(276,2705,3395)
        Jpsi_M.setRange(2855,3245)

    if dfON:
        # EnableImplicitMT()
        histModel = TH1DModel('hh','hh', 2000, minM_Low, maxM_Low)

        hh = df.Filter(totCut.GetTitle()).Histo1D(histModel,"Jpsi_m_scaled") # this fills the histogram of x in parallel, leveraging the aforementioned pool
        hh.Draw("goff") # RResultPtr<TH1D> - vector of results
        hh.GetPtr().Print()

        dh = RooDataHist(histName, histName, RooArgList(Jpsi_M), hh.GetPtr())
    else:
        nt.Draw('Jpsi_m_scaled>>hh',totCut.GetTitle(),'goff')
        dh = RooDataHist(histName,histName, RooArgList(Jpsi_M) ,hh)

    getattr(w,'import')(dh)

    dh.Print()
    print( "[INFO] RooDataHist is stored....")

    bin_idx = "PT0"
    if nPT!=0:
        bin_idx = "PT{}".format(nPT)
    if nY!=0:
        bin_idx = "Y{}".format(nY)

    # nameWksp = f"{dataDir}/wksps/wksp_data_{kSource}_Low_{bin_idx}_ProbNNp_0{int(pPID*10)}.root"
    nameWksp = f"{dataDir}/wksps/wksp_data_{kSource}_Low_{bin_idx}_oldPID.root"
    w.writeToFile(nameWksp)

if __name__ == "__main__":

    # for keyPrompt in [True, False]:
    #     w = RooWorkspace("w",True)
    #     get_data_h(w, keyPrompt, 0, dfON=True)
    #     del w
    for iPT in range(0, 7):
        for keyPrompt in [True,False]:
            w = RooWorkspace("w",True)
            get_data_h(w, keyPrompt, iPT)
            del w
    # for iY in range(1, 5):
    #     w = RooWorkspace("w",True)
    #     get_data_h(w, True, 0, iY)
    #     del w

    # del w
