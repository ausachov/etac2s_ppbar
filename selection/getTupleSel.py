from ROOT import gROOT, TChain, TCut, TFile, TTree, TProof
from datetime import datetime
start=datetime.now()

chain = TChain("DecayTree")
chain.Add("/sps/lhcb/zhovkovska/etac2s_ppbar/Data_2017/no_sel/*.root")

cut_L0 = TCut('Jpsi_L0HadronDecision_TOS')
cut_Hlt = TCut('Jpsi_Hlt1DiProtonDecision_TOS && Jpsi_Hlt2CcDiHadronDiProtonDecision_TOS')

cut_Jpsi = TCut('Jpsi_ENDVERTEX_CHI2<4.0')
cut_Jpsi_Y = TCut('Jpsi_Y > 2 && Jpsi_Y < 4.5')

cut_Protons = TCut('ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
                    ProtonP_PT>2000 && ProtonM_PT>2000 && \
                    ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366 && \
                    ProtonP_TRACK_CHI2NDOF<2.5 && ProtonM_TRACK_CHI2NDOF<2.5 && \
                    ProtonP_TRACK_CloneDist<0 && ProtonM_TRACK_CloneDist<0 && \
                    ProtonP_TRACK_GhostProb<0.2 && ProtonM_TRACK_GhostProb<0.2  \
            ')
                    # ProtonP_PIDp>5 && ProtonM_PIDp>5 && \
                    # (ProtonP_PIDp-ProtonP_PIDK)>0 && (ProtonM_PIDp-ProtonM_PIDK)>0 && \

#cut_Jpsi = TCut('Jpsi_ENDVERTEX_CHI2<3.5 && \
                        #Jpsi_PT>6500') #\
##                        Jpsi_Y>2 && Jpsi_Y<4.5')


optVar_list = ['Jpsi_M', 'Jpsi_m_scaled', 'Jpsi_MM','Jpsi_ENDVERTEX_CHI2','Jpsi_ENDVERTEX_Z','Jpsi_OWNPV_Z','Jpsi_PT','Jpsi_FDCHI2_OWNPV',
                'Jpsi_PE','Jpsi_PZ','Jpsi_P',
                # 'Jpsi_ETA',
                #'nSPDHits',
                'Jpsi_Y',
                'Jpsi_L0HadronDecision_TOS', 'Jpsi_Hlt1DiProtonDecision_TOS', 'Jpsi_Hlt2CcDiHadronDiProtonDecision_TOS',
                'ProtonP_P', 'ProtonP_PT','ProtonM_P','ProtonM_PT',
                'ProtonP_TRACK_CHI2NDOF','ProtonM_TRACK_CHI2NDOF',
                'ProtonP_ProbNNp','ProtonM_ProbNNp',
                'ProtonP_ProbNNpi','ProtonM_ProbNNpi',
                'ProtonP_ProbNNk','ProtonM_ProbNNk',
                'ProtonP_PIDK', 'ProtonP_PIDp', 'ProtonM_PIDK', 'ProtonM_PIDp',
                'ProtonP_IPCHI2_OWNPV', 'ProtonM_IPCHI2_OWNPV',
                'ProtonP_TRACK_GhostProb', 'ProtonM_TRACK_GhostProb', 'ProtonP_TRACK_CloneDist', 'ProtonM_TRACK_CloneDist',
                'Jpsi_M01_Subst0_p2pi','Jpsi_M01_Subst1_p2pi',
                'Jpsi_M01_Subst0_p2K','Jpsi_M01_Subst1_p2K',
                'Jpsi_M01_Subst01_pp~2KK','Jpsi_M01_Subst01_pp~2Kpi','Jpsi_M01_Subst01_pp~2piK','Jpsi_M01_Subst01_pp~2pipi',

]



totCut = TCut(cut_L0 + cut_Hlt + cut_Protons + cut_Jpsi + cut_Jpsi_Y)


chain.SetBranchStatus("*",0)
for var in optVar_list:
  chain.SetBranchStatus("{}".format(var),1);


#print('Tree cutted successfully')

newfile = TFile("/sps/lhcb/zhovkovska/etac2s_ppbar/Data_2017/EtacDiProton_2017.root","recreate")
newtree=TTree()
newtree.SetMaxTreeSize(500000000)
#newtree = cuttedtree.CloneTree();
newtree = chain.CopyTree(totCut.GetTitle())

newtree.Print()

newtree.GetCurrentFile().Write()
newtree.GetCurrentFile().Close()
print( datetime.now()-start)
