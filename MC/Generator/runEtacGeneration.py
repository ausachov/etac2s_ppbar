import sys
import os

file_names = {
            10132000: "incl_b=psi2S,ppbar,NoCut.dec",
            10132010: "incl_b=Jpsi,ppbar,NoCut.dec",
            10132030: "incl_b=chic0,ppbar,NoCut.dec",
            10132040: "incl_b=chic1,ppbar,NoCut.dec",
            10132050: "incl_b=chic2,ppbar,NoCut.dec",
            10132060: "incl_b=etac1S,ppbar,NoCut.dec",
            10132080: "incl_b=etac2S,ppbar,NoCut.dec",
            24102002: "incl_Jpsi,pp=NoCuts.dec",
            24102011: "incl_etac,pp=NoCut.dec",
            24102013: "incl_Jpsi,pp=NoCut.dec",
            24102402: "incl_Jpsi,pppi0=NoCut.dec",
            28102000: "incl_psi2S,pp=NoCut.dec",
            28102001: "incl_etac2S,pp=NoCut.dec",
            28102002: "incl_psi2S,pp=NoCuts.dec",
            28102033: "incl_chic0,pp=NoCut.dec",
            28102043: "incl_chic1,pp=NoCut.dec",
            28102053: "incl_chic2,pp=NoCut.dec",
            }

def prepare_gauss_job(evt_type, year, mag):

    GaussVersion = 'v49r17'
    GaussDir = "/cvmfs/lhcb.cern.ch/lib/lhcb/GAUSS/GAUSS_"+GaussVersion

    OptnsDir = GaussDir+"/Sim/Gauss/options"
    PythiDir = GaussDir+"/Gen/LbPythia8"
    CurntDir = os.getcwd()

    #if len(sys.argv) is not 2:
    #    sys.exit("Please provide an event type file.")

    GaussLocDir = '/afs/cern.ch/user/v/vazhovko/cmtuser/GaussDev_'+GaussVersion
    if os.path.exists(GaussLocDir):
        myApplication = GaudiExec()
        myApplication.directory = GaussLocDir
    else:
        myApplication = prepareGaudiExec('Gauss',GaussVersion, myPath='/afs/cern.ch/user/v/vazhovko/cmtuser/')
    myApplication.platform='x86_64-slc6-gcc49-opt'
    myApplication.options = [
                            "{}/Gauss-{}.py".format(OptnsDir, year)
                            , "{}/{}.py".format(CurntDir, evt_type)
                            , "{}/options/Pythia8.py".format(PythiDir)
                            , "{}/GenStandAlone.py".format(OptnsDir)
                            , "{}/Gauss-Job.py".format(OptnsDir)
                            ]

    #os.environ["DECFILESROOT"] = "/afs/cern.ch/user/a/ausachov/cmtuser/Gauss_v49r6/Gen/DecFiles"
    j = Job()
    j.application = myApplication
    j.name = "GenLevel_{}_Beam6500GeV-{}100-{}-nu1.6_Pythia8".format(evt_type, year, mag)
    j.backend = Dirac()
    j.splitter = GaussSplitter(numberOfJobs=500,eventsPerJob=1000)
    j.inputfiles = [LocalFile("{}/{}".format(CurntDir, file_names[evt_type]))]
    j.outputfiles = [DiracFile("*.xgen"),DiracFile("*.root"),DiracFile("*.xml")]
    j.parallel_submit = True
    j.submit()

mag_pol = ["md"]
year = "2018"
files = [24102402, 24102011, 24102013, 28102000, 28102033, 28102043, 28102053, 10132000, 10132010, 10132030, 10132040, 10132050, 10132060, 10132080]
for f in files:
    for mp in mag_pol:
        prepare_gauss_job(f, year, mp)
