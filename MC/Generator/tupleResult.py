
from Configurables import (
    DaVinci,
    EventSelector,
    PrintMCTree,
    MCDecayTreeTuple
)
from DecayTreeTuple.Configuration import *

particles = {
            10132000: "psi(2S)",
            10132010: "J/psi(1S)",
            10132030: "chi_c0(1P)",
            10132040: "chi_c1(1P)",
            10132050: "chi_c2(1P)",
            10132060: "eta_c(1S)",
            10132080: "eta_c(2S)",
            24102002: "J/psi(1S)",
            24102013: "J/psi(1S)",
            24102011: "J/psi(1S)", # prompt "eta_c(1S)",
            24102402: "J/psi(1S)", # prompt "eta_c(1S)",
            28102000: "psi(2S)",
            28102001: "psi(2S)", # prompt "eta_c(2S)",
            28102002: "psi(2S)",
            28102003: "psi(2S)",
            28102033: "chi_c0(1P)",
            28102043: "chi_c1(1P)",
            28102053: "chi_c2(1P)",
            28102063: "eta_c(1S)", # prompt "eta_c(1S)",
            }

# https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LoKiNewDecayFinders

def prepare_app(evt_type):

    year = 2018

    decay = "({} ==> ^p+ ^p~-)".format(particles[evt_type])
    decay_heads = ["J/psi(1S)"]



    branches = {
         "ProtonP"  :  "{} ==>^p+ p~-".format(particles[evt_type])
        ,"ProtonM"  :  "{} ==> p+^p~-".format(particles[evt_type])
        ,"Jpsi"     : "^({} ==> p+ p~-)".format(particles[evt_type])
    }

    datafile = ""
    with open("GenLevel_{}_Beam6500GeV-2018100-md-nu1.6_Pythia8_URLs.txt".format(evt_type)) as f:
        for line in f:
            datafile += "{}, ".format(line[:-1])


    # For a quick and dirty check, you don't need to edit anything below here.
    ##########################################################################

    # Create an MC DTT containing any candidates matching the decay descriptor
    mctuple = MCDecayTreeTuple("MCDecayTreeTuple")
    mctuple.Decay = decay
    mctuple.Branches = branches
    mctuple.ToolList = [
        "MCTupleToolHierarchy",
        "MCTupleToolAngles",
        "MCTupleToolPrompt",
        "MCTupleToolPrimaries"
    ]

    mctuple.addTupleTool("MCTupleToolKinematic").Verbose = True


    from Configurables import LoKi__Hybrid__MCTupleTool
    mctuple.addTool( LoKi__Hybrid__MCTupleTool, name = "MCLoKiHybrid" )
    mctuple.ToolList += [ "LoKi::Hybrid::MCTupleTool/MCLoKiHybrid" ]
    mctuple.MCLoKiHybrid.Preambulo = [ "from LoKiCore.functions import switch" ]
    mctuple.MCLoKiHybrid.Variables = {
        "TRUEM"   : "MCM",
        "TRUEETA" : "MCETA",
        "TRUETHETA" : "MCTHETA",
    }

    ######################################################################
    from Configurables import LoKi__Hybrid__EvtTupleTool
    mctuple.addTool( LoKi__Hybrid__EvtTupleTool, name = "ELoKiHybrid" )
    mctuple.ToolList += [ "LoKi::Hybrid::EvtTupleTool/ELoKiHybrid" ]
    mctuple.ELoKiHybrid.Preambulo = [ "from LoKiCore.basic import LHCb" ]
    mctuple.ELoKiHybrid.VOID_Variables = {
        "nSPDHits" : "RECSUMMARY( LHCb.RecSummary.nSPDhits, -1, '/Event/Rec/Summary', False )"
    }


    # Print the decay tree for any particle in decay_heads
    #printMC = PrintMCTree()*
    #printMC.ParticleNames = decay_heads

    # Name of the .xgen file produced by Gauss
    EventSelector().Input = ["DATAFILE='{0}' TYP='POOL_ROOTTREE' Opt='READ'".format(datafile)]

    # Configure DaVinci
    # DaVinci().TupleFile = "/eos/user/v/vazhovko/etac2s_ppbar/MC/Generator/Tuple-{}-Total_2.root".format(evt_type)
    DaVinci().TupleFile = "Tuple-{}-Total.root".format(evt_type)
    DaVinci().Simulation = True
    DaVinci().EvtMax = -1                          # Number of events
    DaVinci().SkipEvents = 0                       # Events to skip
    DaVinci().PrintFreq = 1000
    DaVinci().Lumi = False
    DaVinci().DataType = str(year)
    DaVinci().UserAlgorithms += [mctuple]
    #DaVinci().UserAlgorithms = [printMC, mctuple]

    from Gaudi.Configuration import appendPostConfigAction
    def doIt():
        """
        specific post-config action for (x)GEN-files
        """
        extension = "xgen"
        ext = extension.upper()

        from Configurables import DataOnDemandSvc
        dod  = DataOnDemandSvc ()
        from copy import deepcopy
        algs = deepcopy ( dod.AlgMap )
        bad  = set()
        for key in algs :
            if     0 <= key.find ( 'Rec'     )                  : bad.add ( key )
            elif   0 <= key.find ( 'Raw'     )                  : bad.add ( key )
            elif   0 <= key.find ( 'DAQ'     )                  : bad.add ( key )
            elif   0 <= key.find ( 'Trigger' )                  : bad.add ( key )
            elif   0 <= key.find ( 'Phys'    )                  : bad.add ( key )
            elif   0 <= key.find ( 'Prev/'   )                  : bad.add ( key )
            elif   0 <= key.find ( 'Next/'   )                  : bad.add ( key )
            elif   0 <= key.find ( '/MC/'    ) and 'GEN' == ext : bad.add ( key )

        for b in bad :
            del algs[b]

        dod.AlgMap = algs

        from Configurables import EventClockSvc, CondDB
        EventClockSvc ( EventTimeDecoder = "FakeEventTime" )
        CondDB  ( IgnoreHeartBeat = True )

    appendPostConfigAction( doIt )


evt_type = 24102402
prepare_app(evt_type)
