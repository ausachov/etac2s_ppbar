#!/bin/bash

StringArray=("10132000" "10132010" "10132030" "10132040" "10132050" "10132060" "10132080")
StringArray=("24102013" "28102000" "28102001" "28102002" "28102003" "28102033" "28102043" "28102053" "28102063")
for val in "${StringArray[@]}"; 
do    
  gaudirun.py $GAUSSOPTS/Gauss-Job.py $GAUSSOPTS/Gauss-2018.py $GAUSSOPTS/GenStandAlone.py   \
      $DECFILESROOT/options/$val.py $LBPYTHIA8ROOT/options/Pythia8.py; 
done
