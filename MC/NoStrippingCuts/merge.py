from ROOT import TFile, TChain, TTree

#f = TFile('DVntuple_Etac2016_MagDown_NoStrippingCuts.root', 'recreate')
f = TFile('DVntuple_EtacDiProton_NoStrippingCuts_2016.root', 'recreate')
chain = TChain("Jpsi2ppTuple/DecayTree")

for i in range(27):    
    chainName = '/afs/cern.ch/work/v/vazhovko/gangadir/workspace/vazhovko/LocalXML/8/%s/output/Tuple.root'%i
    chain.Add(chainName)

for i in range(27):    
    chainName = '/afs/cern.ch/work/v/vazhovko/gangadir/workspace/vazhovko/LocalXML/9/%s/output/Tuple.root'%i
    chain.Add(chainName)

tree = TTree()
tree = chain.CopyTree("")
tree.Write()
#f.Write()
f.Close()