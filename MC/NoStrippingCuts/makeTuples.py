def makeSingleTuple(year,polarity,mass,evtType,src):

    yearShort = str(int(year)-2000)
    low = ['Etac','etac','jpsi','pppi0']

    # if mass=='pppi0' or mass=='Etac' or ("psi" in mass):   
    if (evtType in ['24102402', '28102000', '10132010', '10132000', '10030060']):
        simV = 'Sim09j'
    else:
        simV = 'Sim09k'

    myJobName = f'MC_{mass}_{src}_Ccbar2PPbar_NoTurbo_Mag{polarity}_{year}'

    davinciVer = 'v44r7'
    davinciDir = '/users/LHCb/zhovkovska/cmtuser/DaVinciDev_'+davinciVer
    # davinciDir = '/afs/cern.ch/user/v/vazhovko/cmtuser/DaVinciDev_'+davinciVer
    import os
    if os.path.exists(davinciDir):
        myApplication = GaudiExec()
        myApplication.directory = davinciDir
    else:
        myApplication = prepareGaudiExec('DaVinci',davinciVer, myPath='/users/LHCb/zhovkovska/cmtuser/')


    myApplication.options = [f'DaVinci_Jpsi2pp_dst.py',f'conddb{polarity}.py']
    bkpath  = f'/MC/{year}/Beam6500GeV-{year}-Mag{polarity}-Nu1.6-25ns-Pythia8/{simV}/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/{evtType}/ALLSTREAMS.DST'
    bkq     = BKQuery(bkpath, dqflag=['OK','UNCHECKED'])
    data    = bkq.getDataset()

    validData   = LHCbDataset(files=['LFN:'+lfn for lfn, rep in data.getReplicas().items() if len(rep)])

    mySplitter  = SplitByFiles( filesPerJob = 5, maxFiles = -1, ignoremissing = False, bulksubmit = False )

    myBackend   = Dirac()
    j = Job (
             name         = myJobName,
             application  = myApplication,
             splitter     = mySplitter,
             outputfiles  = [ DiracFile('Tuple.root'),
                              DiracFile('DVHistos.root')],
             backend      = myBackend,
             inputdata    = validData,
             do_auto_resubmit = True,
             parallel_submit = True
             )
    j.application.platform = "x86_64+avx2+fma-centos7-gcc62-opt"
    j.backend.settings['CPUTime'] = 302400
    j.submit()


years      = ['2018']
polarities = ['Down','Up']
# polarities = ['Down']
sources    = ["prompt","fromB"]
# sources    = ["fromB"]
# masses     = ['jpsi','etac','etac2S','chic0','chic1','chic2','psi2S'] 
masses     = ['chic2'] 

evtTypes   = { 
        "prompt":
                {  
                 'etac' : '24102011',
                 'etac2S': '28102001',
                 # 'jpsi' : '24102002',
                 'jpsi' : '24102013', #
                 'chic0': '28102033',
                 'chic1': '28102043',
                 'chic2': '28102053',
                 # 'psi2S': '28102002',
                 'psi2S': '28102000',
                 'pppi0': '24102402'
                },
        "fromB":
                {
                 'Etac'  : '10030060',
                 'etac'  : '10132060',
                 'etac2S': '10132080',
                 'chic0' : '10132030',
                 'chic1' : '10132040',
                 'chic2' : '10132050',
                 'jpsi'  : '10132010',
                 'psi2S' : '10132000',
                 }
              }


for mass in masses:
    for year in years:
        for polarity in polarities:
            for src in sources:
              print('PPbar for ', year, polarity, mass, src, ' submitting below! \n')
              evtType   = evtTypes[src][mass]
              makeSingleTuple(year,polarity,mass,evtType,src)
