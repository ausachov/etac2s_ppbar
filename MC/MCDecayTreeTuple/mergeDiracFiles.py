merged_filepath_prefix = '/eos/user/v/vazhovko/etac2s_ppbar/MC/NoTrigger'

input_tree_name     = 'MCDecayTreeTuple/MCDecayTree'

import sys, re
import ROOT

def merge_root_output(merged_filepath_prefix, polarity, ccbar, source):

    access_urls = []
    thefile = open('MCGen_{}_{}_CCbar2PPbar_Turbo05_Mag{}_2018_URLs.txt'.format(ccbar, source, polarity), 'r')

    for line in thefile:
       line = line.translate(None, "\'[]")
       line = line.replace('?svcClass=lhcbUser','')
       line = line.replace('\n','')
       if "Tuple.root" in line:
          access_urls.append(line)
          print(line)
    thefile.close()

    import ROOT
    tchain = ROOT.TChain(input_tree_name)
    for url in access_urls:
       tchain.Add(url)
       print(tchain.GetEntries())

    sjDir = merged_filepath_prefix


    newfile = ROOT.TFile(sjDir+"/{}DiProton_{}_Mag{}_2018_new.root".format(ccbar, source, polarity),"recreate")
    newtree = ROOT.TTree()
    newtree.SetMaxTreeSize(700000000)

    newtree = tchain.CopyTree("")
    newtree.Print()
    newtree.GetCurrentFile().Write()
    newtree.GetCurrentFile().Close()

    del tchain, newtree, newfile

fb = True
ccbars = ["etac","etac2S","jpsi","psi2S","chic0","chic1","chic2","pppi0"]
sources = ["prompt","fromB"]
pols = ["Up","Down"]
for ccbar in ccbars:
    for pol in pols:
        for src in sources:
            print("Merging {} {} Mag{}".format(ccbar, src, pol))
            merge_root_output(merged_filepath_prefix, pol, ccbar, src)
