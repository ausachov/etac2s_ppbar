from DaVinci.Configuration import *
from DecayTreeTuple.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, DecayTreeTuple, HltSelReportsDecoder,HltVertexReportsDecoder,HltDecReportsDecoder,LoKi__Hybrid__TupleTool, FilterDesktop

from Configurables import CombineParticles
from PhysSelPython.Wrappers import (
    RebuildSelection,
    AutomaticData,
    Selection,
    SelectionSequence,
    TupleSelection
)


def fillTuple( tuple, myTriggerList ):

    tuple.ToolList = ["TupleToolAngles",
                      "TupleToolEventInfo",
                      "TupleToolGeometry",
                      "TupleToolKinematic",
                      "TupleToolPid",
                      "TupleToolPrimaries",
                      "TupleToolTrackInfo",
                      #"TupleToolRecoStats",
                      "TupleToolANNPID"
                     ]

    # RecoStats for filling SpdMult, etc
    from Configurables import TupleToolRecoStats
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True

    #from Configurables import TupleToolANNPID
    #tuple.addTupleTool(TupleToolANNPID, name = 'TupleToolANNPID')
    #tuple.TupleToolANNPID.Verbose = True

    from Configurables import TupleToolTISTOS, TupleToolDecay
    tuple.addTupleTool(TupleToolDecay, name = 'Jpsi')


    #tuple.Jpsi.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForJpsi" ]
    #tuple.Jpsi.addTool(TupleToolTISTOS, name="TupleToolTISTOSForJpsi" )

    tuple.Jpsi.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.Jpsi.TupleToolTISTOS.Verbose=True
    tuple.Jpsi.TupleToolTISTOS.TriggerList = myTriggerList


    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.Jpsi.addTupleTool(LoKi_Jpsi)


    from Configurables import TupleToolMCBackgroundInfo, TupleToolMCTruth

    tuple.ToolList += ["TupleToolMCBackgroundInfo",
                       "TupleToolMCTruth"
                       ]


    MCTruth = TupleToolMCTruth()
    MCTruth.ToolList =  ["MCTupleToolAngles"
                         , "MCTupleToolHierarchy"
                         , "MCTupleToolKinematic"
                         , "MCTupleToolReconstructed"
                         , "MCTupleToolPID"
                         , "MCTupleToolDecayType"
                         , "MCTupleToolPrompt"
                         ##, "MCTupleToolEventType"
                         ##, "MCTupleToolInteractions"
                         ##, "MCTupleToolP2VV"
                         ##, "MCTupleToolPrimaries"
                         ##, "MCTupleToolRedecay"
                         ]
    tuple.addTool(MCTruth)

    from LoKiMC.functions import MCMOTHER, MCVFASPF, MCPRIMARY

    from Configurables import LoKi__Hybrid__MCTupleTool
    MCTruth.addTool( LoKi__Hybrid__MCTupleTool, name = "MCLoKiHybrid" )
    MCTruth.ToolList += [ "LoKi::Hybrid::MCTupleTool/MCLoKiHybrid" ]
    MCTruth.MCLoKiHybrid.Preambulo = [ "from LoKiCore.functions import switch" ]
    MCTruth.MCLoKiHybrid.Variables = {
        "TRUE_Tz"   : "(3.3)*(MCVFASPF(MCVZ)-MCMOTHER(MCVFASPF(MCVZ), 0))*MCM/MCPZ",
        "TRUEM"     : "MCM",
        "TRUEETA"   : "MCETA",
        "TRUEPHI"   : "MCPHI",
        "TRUETHETA" : "MCTHETA",
        "MC_MOTHER_M"       : "MCMOTHER(MCM,   -1)",
        "MC_MOTHER_TRUEP"   : "MCMOTHER(MCP,   -9999)",
        "MC_MOTHER_TRUEETA" : "MCMOTHER(MCETA, -9999)",
        "MC_MOTHER_TRUEPHI" : "MCMOTHER(MCPHI, -9999)",
        "MC_MOTHER_TRUEV_X" : "MCMOTHER(MCVFASPF(MCVX), -9999)",
        "MC_MOTHER_TRUEV_Y" : "MCMOTHER(MCVFASPF(MCVY), -9999)",
        "MC_MOTHER_TRUEV_Z" : "MCMOTHER(MCVFASPF(MCVZ), -9999)",
        "MC_MOTHER_ISPRIMARY"  : "MCMOTHER(MCVFASPF(switch(MCPRIMARY,1,0)),0)",
        "MC_GD_MOTHER_M"       : "MCMOTHER(MCMOTHER(MCM,   -1), -1)",
        "MC_GD_MOTHER_TRUEETA" : "MCMOTHER(MCMOTHER(MCETA, -9999), -9999)",
        "MC_GD_MOTHER_TRUEPHI" : "MCMOTHER(MCMOTHER(MCPHI, -9999), -9999)",
        "MC_GD_MOTHER_TRUEP"   : "MCMOTHER(MCMOTHER(MCP,   -9999), -9999)",
        "MC_GD_MOTHER_TRUEV_X" : "MCMOTHER(MCMOTHER(MCVFASPF(MCVX), -9999), -9999)",
        "MC_GD_MOTHER_TRUEV_Y" : "MCMOTHER(MCMOTHER(MCVFASPF(MCVY), -9999), -9999)",
        "MC_GD_MOTHER_TRUEV_Z" : "MCMOTHER(MCMOTHER(MCVFASPF(MCVZ), -9999), -9999)",
        "MC_GD_MOTHER_ISPRIMARY"  : "MCMOTHER(MCMOTHER(MCVFASPF(switch(MCPRIMARY,1,0)),0),0)",
        "MC_GD_GD_MOTHER_TRUEETA" : "MCMOTHER(MCMOTHER(MCMOTHER(MCETA, -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEPHI" : "MCMOTHER(MCMOTHER(MCMOTHER(MCPHI, -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEP"   : "MCMOTHER(MCMOTHER(MCMOTHER(MCP,   -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEV_X" : "MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVX), -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEV_Y" : "MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVY), -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEV_Z" : "MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVZ), -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_ISPRIMARY"  : "MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(switch(MCPRIMARY,1,0)),0),0),0)",
        "MC_GD_GD_GD_MOTHER_TRUEETA" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCETA, -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEPHI" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCPHI, -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEP"   : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCP,   -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEV_X" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVX), -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEV_Y" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVY), -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEV_Z" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVZ), -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_ISPRIMARY"  : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(switch(MCPRIMARY,1,0)),0),0),0),0)",
        }
    tuple.addTool(MCTruth)

    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
        }
    tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
    tuple.addTool(LoKi_All)


    tuple.Jpsi.addTupleTool( 'TupleToolSubMass' )
    tuple.Jpsi.TupleToolSubMass.Substitution += ["p+ => pi+"]
    tuple.Jpsi.TupleToolSubMass.Substitution += ["p+ => K+"]

    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => pi+/pi-"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => K+/pi-"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => pi+/K-"]
    tuple.Jpsi.TupleToolSubMass.DoubleSubstitution += ["p+/p~- => K+/K-"]




myTriggerList = [
                 # L0
                 "L0ElectronDecision",
                 "L0PhotonDecision",
                 "L0HadronDecision",
                 # L0 Muon
                 "L0MuonDecision",
                 "L0MuonHighDecision",
                 "L0DiMuonDecision",

                 # Hlt1 track
                 "Hlt1B2PhiPhi_LTUNBDecision",
                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackPhotonDecision",

                 "Hlt1TrackAllL0Decision",
                 "Hlt1TrackMVADecision",
                 "Hlt1TrackMVALooseDecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1TwoTrackMVALooseDecision"
                 "Hlt1B2HH_LTUNB_KPiDecision",
                 "Hlt1B2HH_LTUNB_KKDecision",
                 "Hlt1B2HH_LTUNB_PiPiDecision",
                 "Hlt1IncPhiDecision",
                 "Hlt1DiProtonDecision",
                 "Hlt1DiProtonLowMultDecision",
                 "Hlt1LowMultVeloCut_HadronsDecision",
                 "Hlt1LowMultPassThroughDecision",

                 "Hlt1DiProtonDecision",
                 "Hlt1DiProtonHighDecision"

                 ]



from Configurables import LoKi__Hybrid__Tool as MyFactory
mf = MyFactory("HybridFactory")
mf.Modules.append ( 'LoKiPhysMC.decorators' )


stream = "AllStreams"
year = "2018"
Jpsi2ppDecay = "J/psi(1S) -> ^p+ ^p~-"
Jpsi2ppBranches = {
     "ProtonP"  :  "J/psi(1S)  ->^p+ p~-"
    ,"ProtonM"  :  "J/psi(1S)  -> p+^p~-"
    ,"Jpsi"     :  "(J/psi(1S)  -> p+ p~-)"
}


#Jpsi2ppLocation = "Hlt2CcDiHadronDiProtonHighTurbo/Particles"
#from PhysConf.Selections import AutomaticData, MomentumScaling, TupleSelection
#inputData = AutomaticData(Jpsi2ppLocation)
##inputData = MomentumScaling(inputData, Turbo  = True, Year = year)

##Jpsi2ppTuple = TupleSelection("Jpsi2ppTuple", inputData, Decay = Jpsi2ppDecay, Branches = Jpsi2ppBranches)
##fillTuple( Jpsi2ppTuple, myTriggerList )


MyPreambulo = [
    'from LoKiPhysMC.decorators import *',
    'from LoKiPhysMC.functions import mcMatch' ,
    'from LoKiCore.functions import monitor'
]

from StandardParticles import StdAllNoPIDsProtons as ProtonsForCcbar2Ppbar
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysConf.Selections import CombineSelection

Jpsi2ppbar = CombineSelection (
    "Jpsi2ppbar"  , ## name
    [ ProtonsForCcbar2Ppbar ] , ## input
    DecayDescriptor = "J/psi(1S) -> p+ p~-" ,
    Preambulo       = MyPreambulo  ,
    DaughtersCuts   = {
    'p+' : "mcMatch ( 'J/psi(1S) => ^p+  p~-' , 2 )",
    'p~-': "mcMatch ( 'J/psi(1S) =>  p+ ^p~-' , 2 )"
    } ,
    CombinationCut = 'AALL'                           ,
    MotherCut = "mcMatch('J/psi(1S) => p+  p~-' , 2 )"
    )

from PhysSelPython.Selections import SelectionSequence
selseq1 = SelectionSequence('SeqJpsi', Jpsi2ppbar)
seq1 = selseq1.sequence()



Jpsi2ppTuple = DecayTreeTuple('Jpsi2ppTuple')
Jpsi2ppTuple.Decay    = Jpsi2ppDecay
Jpsi2ppTuple.Branches = Jpsi2ppBranches
#Jpsi2ppTuple.Inputs   = [Jpsi2ppLocation]
Jpsi2ppTuple.Inputs = [ selseq1.outputLocation() ]
fillTuple( Jpsi2ppTuple, myTriggerList )


#rootInTES = '/Event/AllStreams/Turbo'

#from Configurables import DataOnDemandSvc
#dod = DataOnDemandSvc()
#from Configurables import Gaudi__DataLink as Link
##rawEvt1 = Link ( 'LinkRawEvent1', What   =  '/Event/DAQ/RawEvent' , Target = '/Event/Trigger/RawEvent' )
#if (year == '2017' or year == '2018' ):
  #rawEvt1 = Link ( 'LinkTurbopRec', What   =  rootInTES+'/pRec' , Target = rootInTES+'/Hlt2/pRec' )
#dod.AlgMap [ rawEvt1  . Target ] = rawEvt1



#from Configurables import DstConf, TurboConf
#TurboConf().PersistReco=False
#DstConf().Turbo=True




"""
Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Jpsi2ppFilters = LoKi_Filters (
     HLT2_Code = "HLT_PASS_RE('Hlt2CcDiHadronDiProtonTurboDecision')"
    )



from Configurables import CondDB
CondDB( LatestGlobalTagByDataType = year )
DaVinci().EventPreFilters = Jpsi2ppFilters.filters('Jpsi2ppFilters')
DaVinci().EvtMax         = -1                        # Number of events
DaVinci().SkipEvents     = 0                       # Events to skip
DaVinci().PrintFreq      = 1000
DaVinci().DataType       = year
DaVinci().Simulation     = True
DaVinci().HistogramFile  = "DVHistos.root"      # Histogram file
DaVinci().TupleFile      = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms = [seq1,Jpsi2ppTuple]# The algorithms

# MDST
DaVinci().InputType = "DST"
#DaVinci().RootInTES = '/Event/AllStreams/Turbo'
#DaVinci().Turbo     = True

DaVinci().Lumi = not DaVinci().Simulation

from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]

from GaudiConf import IOHelper
# Use the local input data
#IOHelper().inputFiles([
                      #'/afs/cern.ch/work/v/vazhovko/private/etac2s_ppbar/MC/etac/00091000_00000090_7.AllStreams.dst'
                      #], clear=True)
