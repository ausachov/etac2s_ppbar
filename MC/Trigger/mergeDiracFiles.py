# merged_filepath_prefix = '/eos/user/v/vazhovko/etac2s_ppbar/MC/NoTrigger'
merged_filepath_prefix = '/sps/lhcb/zhovkovska/etac2s_ppbar/MC/NoTurbo/'

input_tree_name     = 'Jpsi2ppTuple/DecayTree'

import os, sys, re
import ROOT

def merge_root_output(merged_filepath_prefix, polarity, ccbar, source):

    postfix = ""
    if source=="fromB":
        #postfix = "Detached"
        postfix = ""
    else:
        postfix = ""

    input_tree_name = "Jpsi2pp{}Tuple/DecayTree".format(postfix)

    access_urls = []
    thefile = open('MC_{}_{}_Ccbar2PPbar_NoTurbo_Mag{}_2018_URLs.txt'.format(ccbar, source, polarity), 'r')

    for line in thefile:
       # line = line.translate(None, "\'[]")
       line = line.translate({ ord(c): None for c in "\'[]" })
       line = line.replace('?svcClass=lhcbUser','')
       line = line.replace('\n','')
       if "Tuple.root" in line:
          access_urls.append(line)
          print(line)
    thefile.close()

    import ROOT
    tchain = ROOT.TChain(input_tree_name)
    for url in access_urls:
       tchain.Add(url)
       print(tchain.GetEntries())

    sjDir = merged_filepath_prefix


    newfile = ROOT.TFile(sjDir+"/{}DiProton_{}_Mag{}_2018.root".format(ccbar, source, polarity),"recreate")
    newtree = ROOT.TTree()
    newtree.SetMaxTreeSize(500000000)

    newtree = tchain.CopyTree("")
    newtree.Print()
    newtree.GetCurrentFile().Write()
    newtree.GetCurrentFile().Close()

    del tchain, newtree, newfile

if __name__ == "__main__":

  # ccbars = ["etac","etac2S","jpsi","psi2S","chic0","chic1","chic2"]
  ccbars = ["jpsi"]
  # pols = ["Up","Down"]
  pols = ["Down"]
  sources = ["fromB"]
  for ccbar in ccbars:
    for pol in pols:
      for src in sources:
        print("Merging {} {} Mag{}".format(ccbar, src, pol))
        merge_root_output(merged_filepath_prefix, pol, ccbar, src)
