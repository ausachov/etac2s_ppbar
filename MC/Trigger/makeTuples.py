def makeSingleTuple(year,polarity,mass):

    evtTypes   = {'Low'  : '10132060',
                  'High' : '10132080' }
    #evtTypes   = {'Low'  : '24102011',
                  #'High' : '28102001' }
    yearShort = str(int(year)-2000)
    evtType   = evtTypes[mass]

    myJobName = 'incl_b_Jpsi2PPbar_'+mass+'_Turbo05_Mag'+polarity+'_'+year


    davinciVer = 'v45r0'
    davinciDir = '/afs/cern.ch/user/v/vazhovko/cmtuser/DaVinciDev_'+davinciVer
    import os
    if os.path.exists(davinciDir):
        myApplication = GaudiExec()
        myApplication.directory = davinciDir
    else:
        myApplication = prepareGaudiExec('DaVinci',davinciVer, myPath='/afs/cern.ch/user/v/vazhovko/cmtuser/')


    myApplication.options = ['DaVinci_Jpsi2pp_'+mass+'.py']
    data  = BKQuery('/MC/'+year+'/Beam6500GeV-'+year+'-Mag'+polarity+'-Nu1.6-25ns-Pythia8/Sim09f/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/'+evtType+'/ALLSTREAMS.DST', dqflag=['OK','UNCHECKED']).getDataset()

    validData = LHCbDataset(files=['LFN:'+lfn for lfn, rep in data.getReplicas().iteritems() if len(rep)])

    mySplitter = SplitByFiles( filesPerJob = 20, maxFiles = -1, ignoremissing = True, bulksubmit = False )

    myBackend = Dirac()
    j = Job (
             name         = myJobName,
             application  = myApplication,
             splitter     = mySplitter,
             outputfiles  = [ DiracFile('Tuple.root'),
                              DiracFile('DVHistos.root')],
             backend      = myBackend,
             inputdata    = validData,
             do_auto_resubmit = True,
             parallel_submit = True
             )
    j.application.platform = "x86_64-centos7-gcc8-opt"
    queues.add(j.submit)


years      = ['2018']
polarities = ['Down','Up']
masses     = ['High','Low']
#masses     = ['Low']
#evtTypes   = {'Low'  : '24102011',
              #'High' : '28102001' }
evtTypes   = {'Low'  : '10132060',
              'High' : '10132080' }


for mass in masses:
    for year in years:
        for polarity in polarities:
            print('PPbar for ', year, polarity, mass, ' submitting below! \n')
            makeSingleTuple(year,polarity,mass)
